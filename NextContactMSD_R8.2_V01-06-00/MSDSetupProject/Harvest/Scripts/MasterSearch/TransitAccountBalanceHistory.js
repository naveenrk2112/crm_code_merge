﻿var TransitAccountBalanceHistory = {}

TransitAccountBalanceHistory.SearchTransitAccountBalanceHistory = function (startDate) {
    $("#AccountBalanceHistoryGrid").jsGrid({ data: [], noDataContent: "Loading..." });
    $('#AccountBalanceHistory').show();
    $("#SearchAccountBalanceHistoryButton").prop("disabled", true);
    $("#balanceStartDatepicker").datepicker({ dateFormat: "yy-mm-dd" });
    $("#balanceEndDatepicker").datepicker({ dateFormat: "yy-mm-dd" });
    if ($("#balanceStartDatepicker").val() == '') {
        $("#balanceStartDatepicker").val(startDate);
    }
    if ($("#balanceEndDatepicker").val() == '') {
        var endDate = DateFormat.format.date(new Date(), "yyyy-MM-dd");
        $("#balanceEndDatepicker").val(endDate);
    }

    var startDate = $("#balanceStartDatepicker").val();
    var endDate = $("#balanceEndDatepicker").val();
    var params = {
        "AccountId": TransitAccount._TransitAccountParams.AccountId,
        "Subsystem": TransitAccount._TransitAccountParams.Subsystem,
        "StartDate": startDate,
        "EndDate": endDate
    };
    $.ajax({
        type: 'post',
        data: params,
        dataType: 'json',
        cache: false,
        url: '/MasterSearch/GetBalanceHistory',
        success: function (response, textStatus, jqXHR) {
            TransitAccountBalanceHistory.PrepareAccountBalanceHistory(JSON.parse(response));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });
}

TransitAccountBalanceHistory.PrepareAccountBalanceHistory = function (accountBalanceHistory) {
    $("#AccountBalanceHistoryGrid").jsGrid({
        noDataContent: "No data found!",
        width: "900px",
        sorting: true,
        paging: true,
        pageSize: 3,
        pageButtonCount: 10,
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
        pagePrevText: "Prev",
        pageNextText: "Next",
        pageFirstText: "First",
        pageLastText: "Last",
        pageNavigatorNextText: "...",
        pageNavigatorPrevText: "...",
        data: accountBalanceHistory.lineItems,
        headerRowRenderer: function () {
            var $result = $("<tr>")
                            .append($("<th class='account_status_header'>").width(100).text("Date"))
                            .append($("<th class='account_status_header'>").width(120).text("Purse Type"))
                            .append($("<th class='account_status_header'>").width(120).text("Purse Restriction"))
                            .append($("<th class='account_status_header'>").width(150).text("Entry Type"))
                            .append($("<th class='account_status_header'>").width(100).text("Reversal?"))
                            .append($("<th class='account_status_header'>").width(75).text("Transaction Amount"))
                            .append($("<th class='account_status_header'>").width(75).text("Ending Balance"));
            return $result;
        },
        fields: [
            {
                title: "Date", name: "entryDateTime", type: "text", align: "center", width: 100,
                cellRenderer: function (value, item) {
                    return "<td>" + DateFormat.format.date(value, "MM/dd/yyyy") + "</td>";
                }
            },
            { title: "Purse Type", name: "purseTypeDescription", type: "text", width: 120 },
            { title: "Purse Restriction", name: "purseRestrictionDescription", type: "text", width: 120 },
            { title: "Entry Type", name: "entryTypeDescription", type: "text", width: 150 },
            {
                title: "Reversal?", name: "isReversal", type: "text", align: "center", width: 100,
                cellRenderer: function (value, item) {
                    return value == true ? "<td>Yes</td>" : "<td>No</td>";
                }
            },
            {
                title: "Transaction Amount", name: "journalEntryAmount", type: "text", width: 75, align: "right",
                cellRenderer: function (value, item) {
                    return "<td>" + value / 100 + "</td>";
                }
            },
            {
                title: "Ending Balance", name: "endingBalance", type: "text", width: 75, align: "right",
                cellRenderer: function (value, item) {
                    return "<td>" + value / 100 + "</td>";
                }
            },
        ]
    });
    $("#SearchAccountBalanceHistoryButton").prop("disabled", false);
}
