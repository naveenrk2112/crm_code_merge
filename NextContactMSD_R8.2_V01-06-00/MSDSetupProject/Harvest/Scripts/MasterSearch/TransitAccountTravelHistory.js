﻿var TransitAccountTravelHistory = {}

TransitAccountTravelHistory.SearchTransitAccountTravelHistory = function (startDate) {
    $("#AccountTravelHistoryGrid").jsGrid({ data: [], noDataContent: "Loading..." });
    $('#AccountTravelHistory').show();
    $("#SearchAccountTravelHistoryButton").prop("disabled", true);
    $("#travelStartDatepicker").datepicker({ dateFormat: "yy-mm-dd" });
    $("#travelEndDatepicker").datepicker({ dateFormat: "yy-mm-dd" });
    if ($("#travelStartDatepicker").val() == '') {
        $("#travelStartDatepicker").val(startDate);
    }
    if ($("#travelEndDatepicker").val() == '') {
        var endDate = DateFormat.format.date(new Date(), "yyyy-MM-dd");
        $("#travelEndDatepicker").val(endDate);
    }
    var startDate = $("#travelStartDatepicker").val();
    var endDate = $("#travelEndDatepicker").val();
    var params = {
        "AccountId": TransitAccount._TransitAccountParams.AccountId,
        "Subsystem": TransitAccount._TransitAccountParams.Subsystem,
        "StartDate": startDate,
        "EndDate": endDate
    };
    $.ajax({
        type: 'post',
        data: params,
        dataType: 'json',
        cache: false,
        url: '/MasterSearch/GetTravelHistory',
        success: function (response, textStatus, jqXHR) {
            TransitAccountTravelHistory.PrepareAccountTravelHistory(JSON.parse(response));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });
}

TransitAccountTravelHistory.PrepareAccountTravelHistory = function (accountTravelHistory) {
    $("#AccountTravelHistoryGrid").jsGrid({
        noDataContent: "No data found!",
        width: "1200px",
        sorting: true,
        paging: true,
        pageSize: 10,
        pageButtonCount: 10,
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
        pagePrevText: "Prev",
        pageNextText: "Next",
        pageFirstText: "First",
        pageLastText: "Last",
        pageNavigatorNextText: "...",
        pageNavigatorPrevText: "...",
        data: accountTravelHistory.lineItems,
        headerRowRenderer: function () {
            var $result = $("<tr>")
                            .append($("<th class='account_status_header'>").width(170).text("Timing"))
                            .append($("<th class='account_status_header'>").width(300).text("Location"))
                            .append($("<th class='account_status_header'>").width(120).text("Travel Mode"))
                            .append($("<th class='account_status_header'>").width(120).text("Token (Type/PAN/Exp)"))
                            .append($("<th class='account_status_header'>").width(100).text("Product"))
                            .append($("<th class='account_status_header'>").width(75).text("Fare"))
                            .append($("<th class='account_status_header'>").width(75).text("Unpaid Fare"));
            return $result;
        },
        fields: [
            {
                title: "Timing", Name: "Timing", type: "text", align: "left", width: 170,
                cellRenderer: function (value, item) {
                    return "<td><table>" +
                                "<tr><td>Start:</td><td>" + DateFormat.format.date(item.startDateTime, "MM-dd-yyyy hh:mm a") + "</td></tr>" +
                                "<tr><td>End:</td><td>" + DateFormat.format.date(item.endDateTime, "MM-dd-yyyy hh:mm a") + "</td></tr>" +
                           "</table></td>";
                }
            },
            {
                title: "Location", name: "Location", type: "text", width: 300,
                cellRenderer: function (value, item) {
                    return "<td><table>" +
                                "<tr><td>From:</td><td>" + item.startLocationDescription + '</td></tr>' +
                                "<tr><td>To:</td><td>" + item.endLocationDescription + '</td></tr>' +
                           "</table></td>";
                }
            },
            { title: "Travel Mode", name: "travelModeDescription", type: "text", width: 120 },
            {
                title: "Token", name: "Token", type: "text", width: 120,
                cellRenderer: function (value, item) {
                    return "<td>" + item.token.tokenType + '-' +
                                    item.token.maskedPAN + '-' +
                                    item.token.cardExpiryMMYY +
                           "</td>";
                }
            },
            { title: "Product", name: "productDescription", type: "text", align: "center", width: 100 },
            {
                title: "Fare", name: "totalFare", type: "text", width: 60, align: "right",
                cellRenderer: function (value, item) {
                    return "<td>" + value / 100 + "</td>";
                }
            },
            {
                title: "Unpaid Fare", name: "unpaidFare", type: "text", width: 60, align: "right",
                cellRenderer: function (value, item) {
                    return "<td>" + value / 100 + "</td>";
                }
            },
        ]
    });
    $("#SearchAccountTravelHistoryButton").prop("disabled", false);
}
