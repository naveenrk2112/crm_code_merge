﻿var TransitAccountStatus = {}

TransitAccountStatus.SearchTransitAccountStatus = function () {
    $.ajax({
        type: 'post',
        data: TransitAccount._TransitAccountParams,
        dataType: 'json',
        cache: false,
        url: '/MasterSearch/GetAccountStatus',
        success: function (response, textStatus, jqXHR) {
            accountStatusData = JSON.parse(response);
            TransitAccountStatus.PrepareUnregisteredCustomerSubsystemPage(accountStatusData);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });
}

TransitAccountStatus.PrepareUnregisteredCustomerSubsystemPage = function (accountStatusData) {
    TransitAccountStatus.PrepareAccountTokens(accountStatusData);
    TransitAccountStatus.PrepareAccountPurses(accountStatusData);
    TransitAccountStatus.PrepareAccountPasses(accountStatusData);
    $('#AccountStatus').show();
}

TransitAccountStatus.PrepareAccountTokens = function (accountStatusData) {
    var statusChanged = DateFormat.format.date(accountStatusData.accountStatusChangeDateTime, "MM/dd/yyyy");
    var accountCreation = DateFormat.format.date(accountStatusData.accountCreatedDateTime, "MM/dd/yyyy");
    var lastUded = DateFormat.format.date(accountStatusData.accountLastUsageDetails.transactionDateTime, "MM-dd-yyyy hh:mm a");
    $('#account_status').html(accountStatusData.accountStatus);
    $('#rider_class').html(accountStatusData.riderClassDescription);
    $('#status_changed').html(statusChanged);
    $('#account_creation').html(accountCreation);
    $('#last_used_date_time').html(lastUded);
    $('#last_used_device').html(accountStatusData.accountLastUsageDetails.device);
    $('#last_used_status').html(accountStatusData.accountLastUsageDetails.statusDescription);
    $('#last_used_location').html(accountStatusData.accountLastUsageDetails.location);
    $("#AccountTokens").jsGrid({
        width: "1200px",
        sorting: true,
        paging: false,
        data: accountStatusData.tokens,
        headerRowRenderer: function () {
            var $result = $("<tr>").height(0)
                          .append($("<th>").width(75))
                          .append($("<th>").width(75))
                          .append($("<th>").width(100))
                          .append($("<th>").width(75))
                          .append($("<th>").width(200))
                          .append($("<th>").width(180))
                          .append($("<th>").width(100))
                          .append($("<th>").width(140))
                          .append($("<th>").width(150));
            $result = $result.add($("<tr>").append($("<th class='account_status_header'>").attr("colspan", 9).text("TOKENS")));
            $result = $result.add($("<tr>")
                      .append($("<th class='account_status_header'>").attr("colspan", 5).text(""))
                      .append($("<th class='account_status_header'>").attr("colspan", 4).text("Last Used")))
            return $result.add($("<tr>")
                   .append($("<th class='account_status_header'>").width(75).text("Type"))
                   .append($("<th class='account_status_header'>").width(75).text("PAN"))
                   .append($("<th class='account_status_header'>").width(100).text("Expiry Date"))
                   .append($("<th class='account_status_header'>").width(75).text("Status"))
                   .append($("<th class='account_status_header'>").width(200).text("Status Changed on"))
                   .append($("<th class='account_status_header'>").width(180).text("Date/Time"))
                   .append($("<th class='account_status_header'>").width(100).text("Device"))
                   .append($("<th class='account_status_header'>").width(140).text("Status"))
                   .append($("<th class='account_status_header'>").width(150).text("Location")));
        },
        fields: [
            { title: "Token Type", name: "subsystemTokenTypeDescription", type: "text", width: 75 },
            { title: "Masked PAN", name: "tokenInfo.maskedPAN", type: "text", width: 75, align: "center" },
            { title: "Card Expiry", name: "tokenInfo.cardExpiryMMYY", type: "text", width: 100, align: "center" },
            { title: "Status", name: "statusDescription", type: "text", width: 75 },
            {
                title: "Status Changed On", name: "statusChangeDateTime", type: "text", width: 200, align: "center",
                cellRenderer: function (value, item) {
                    return "<td>" + DateFormat.format.date(value, "MM/dd/yyyy") + "</td>";
                }
            },
            {
                title: "Date/Time", name: "tokenLastUsageDetails.transactionDateTime", type: "text", width: 180, align: "center",
                cellRenderer: function (value, item) {
                    return "<td>" + DateFormat.format.date(value, "MM-dd-yyyy hh:mm a") + "</td>";
                }
            },
            { title: "Device", name: "tokenLastUsageDetails.device", type: "text", width: 100 },
            { title: "Status", name: "tokenLastUsageDetails.statusDescription", type: "text", width: 140 },
            { title: "Location", name: "tokenLastUsageDetails.location", type: "text", width: 150 },
        ]
    });
}

TransitAccountStatus.PrepareAccountPurses = function (accountStatusData) {
    var balance = 0;
    if (accountStatusData.purses[0].balance != null) {
        balance = accountStatusData.purses[0].balance / 100;
    }
    $("#account_balance").html("$" + balance);

    $("#AccountPurses").jsGrid({
        width: "450px",
        sorting: true,
        paging: false,
        data: accountStatusData.purses,
        headerRowRenderer: function () {
            var $result = $("<tr>")
                         .append($("<th class='account_status_header'>").width(150).text("Type"))
                         .append($("<th class='account_status_header'>").width(150).text("Restriction"))
                         .append($("<th class='account_status_header'>").width(100).text("Balance"));
            return $result;
        },
        fields: [
            { title: "Type", name: "purseTypeDescription", type: "text", width: 150 },
            { title: "Restriction", name: "purseRestrictionDescription", type: "text", width: 150 },
            {
                title: "Balance", name: "balance", type: "text", width: 100, align: "right",
                cellRenderer: function (value, item) {
                    return "<td>" + value / 100 + "</td>";
                }
            },
        ]
    });
}

TransitAccountStatus.PrepareAccountPasses = function (accountStatusData) {
    $("#AccountPasses").jsGrid({
        width: "1200px",
        sorting: true,
        paging: false,
        data: accountStatusData.passes,
        headerRowRenderer: function () {
            var $result = $("<tr>").height(0)
                          .append($("<th>").width(200))
                          .append($("<th>").width(100))
                          .append($("<th>").width(100))
                          .append($("<th>").width(100))
                          .append($("<th>").width(100))
                          .append($("<th>").width(100))
                          .append($("<th>").width(180))
                          .append($("<th>").width(180))
                          .append($("<th>").width(100));
            $result = $result.add($("<tr>").append($("<th class='account_status_header'>").attr("colspan", 9).text("PASSES ")));
            return $result.add($("<tr>")
                   .append($("<th class='account_status_header'>").width(200).text("Description"))
                   .append($("<th class='account_status_header'>").width(100).text("Created On"))
                   .append($("<th class='account_status_header'>").width(100).text("Start On"))
                   .append($("<th class='account_status_header'>").width(100).text("End On"))
                   .append($("<th class='account_status_header'>").width(100).text("Remaining"))
                   .append($("<th class='account_status_header'>").width(100).text("Expiration"))
                   .append($("<th class='account_status_header'>").width(180).text("Deactivated On"))
                   .append($("<th class='account_status_header'>").width(180).text("Deactivation Reason"))
                   .append($("<th class='account_status_header'>").width(100).text("Serial #")));
        },
        fields: [
            { title: "Description", name: "passDescription", type: "text", width: 200 },
            {
                title: "Created On", name: "createdDateTime", type: "text", align: "center", width: 100,
                cellRenderer: function (value, item) {
                    return "<td>" + DateFormat.format.date(value, "MM/dd/yyyy") + "</td>";
                }
            },
            {
                title: "Start On", name: "createdDateTime", type: "text", align: "center", width: 100,
                cellRenderer: function (value, item) {
                    return "<td>" + DateFormat.format.date(value, "MM/dd/yyyy") + "</td>";
                }
            },
            {
                title: "End On", name: "createdDateTime", type: "text", align: "center", width: 100,
                cellRenderer: function (value, item) {
                    return "<td>" + DateFormat.format.date(value, "MM/dd/yyyy") + "</td>";
                }
            },
            {
                title: "Remaining", type: "text", align: "center", width: 100,
                cellRenderer: function (value, item) {
                    return "<td>" + item.remainingDuration + ' ' + item.durationType + "</td>";
                }
            },
            {
                title: "Expiration", name: "expirationDateTime", type: "text", align: "center", width: 100,
                cellRenderer: function (value, item) {
                    return "<td>" + DateFormat.format.date(value, "MM/dd/yyyy") + "</td>";
                }
            },
            {
                title: "Deactivation On", name: "deactivatedDateTime", type: "text", align: "center", width: 180,
                cellRenderer: function (value, item) {
                    return "<td>" + DateFormat.format.date(value, "MM-dd-yyyy hh:mm a") + "</td>";
                }
            },
            { title: "Deactivation Reason", name: "deactivatedReason", type: "text", width: 180 },
            { title: "Serial #", name: "passSerialNbr", type: "text", align: "center", width: 100 },
        ]
    });
}