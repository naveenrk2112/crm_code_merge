﻿$(document).ready(function () {
    // jQuery validation: https://jqueryvalidation.org/
    $("#CustomerRegistrationForm").validate();
    $("#FindDuplicatesForm").validate();
});

//
// Search Contact
//
function SearchContact() {
    $("#AccountBalanceHistoryGrid").jsGrid({ data: [], noDataContent: "Loading..." });
    var params = {
        "FirstName": $("#SearchFirstName").val(),
        "LastName": $("#SearchLastName").val(),
        "Email": $("#SearchEmail").val(),
        "Phone": $("#SearchPhone").val()
    };
    $.ajax({
        type: 'post',
        data: params,
        dataType: 'json',
        cache: false,
        url: '/CustomerRegistration/SearchContact',
        success: function (response, textStatus, jqXHR) {
            PrepareSearchResultsGrid("SearchResultsGrid", JSON.parse(response));
            $("#ShowCreateNewCustomerButton").prop("disabled", false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });
}

//
// Prepare the Search Grid
//
function PrepareSearchResultsGrid(gridName, data) {
    $("#" + gridName).jsGrid({
        noDataContent: "No data found!",
        width: "1100px",
        sorting: true,
        paging: false,
        data: data,
        headerRowRenderer: function () {
            var $result = $("<tr>")
                         .append($("<th class='account_status_header'>").width(100).text("First Name"))
                         .append($("<th class='account_status_header'>").width(100).text("Last Name"))
                         .append($("<th class='account_status_header'>").width(250).text("Email"))
                         .append($("<th class='account_status_header'>").width(150).text("Phone 1"))
                         .append($("<th class='account_status_header'>").width(150).text("Phone 2"))
                         .append($("<th class='account_status_header'>").width(150).text("Phone 3"))
                         .append($("<th class='account_status_header'>").width(75).text("Actions"));
            return $result;
        },
        fields: [
            { title: "First Name", name: "FirstName", type: "text", width: 100 },
            { title: "Last Name", name: "LastName", type: "text", width: 100 },
            { title: "Email", name: "Email", type: "text", width: 250 },
            { title: "Phone1", name: "Phone1", type: "text", width: 150 },
            { title: "Phone2", name: "Phone2", type: "text", width: 150 },
            { title: "Phone3", name: "Phone3", type: "text", width: 150 },
            {
                title: "Actions", name: "ContactId", type: "text", width: 75, align: "right",
                itemTemplate: function (value, item) {
                    return $("<a>").attr("href", "javascript:void(0)").on("click", function () {
                        OpenContactInCRM(value);
                        return false;
                    }).text("Open");
                }
            },
        ]
    });
}

//
// Open a contact in CRM
//
function OpenContactInCRM(id) {
    url = CRM_URL + "/main.aspx?etc=2&id={" + id + "}&newWindow=true&pagetype=entityrecord";
    var strWindowFeatures = "location=yes,height=800,width=600,scrollbars=yes,status=yes";
    var win = window.open(url, "_blank", strWindowFeatures);
}

//
// Clear the Search Fields
//
function ClearSearch() {
    $("#SearchFirstName").val('');
    $("#SearchLastName").val('');
    $("#SearchEmail").val('');
    $("#SearchPhone").val('');
}

//
// Show the duplicated grid
//
function ShowFindDuplicates() {
    $('#FindDuplicatesFirstName').val($('#SearchFirstName').val());
    $('#FindDuplicatesLastName').val($('#SearchLastName').val());
    $('#FindDuplicatesEmail').val($('#SearchEmail').val());
    $('#ContactSearchDiv').hide();
    $('#FindDuplicatesDiv').show();
}

//
// Find duplicates
//
function FindDuplicates() {
    var isValid = $("#FindDuplicatesForm").valid();
    if (isValid) {
        var params = {
            "FirstName": $("#FindDuplicatesFirstName").val(),
            "LastName": $("#FindDuplicatesLastName").val(),
            "Email": $("#FindDuplicatesEmail").val()
        };

        $.ajax({
            type: 'post',
            data: params,
            dataType: 'json',
            cache: false,
            url: '/CustomerRegistration/FindDuplicates',
            success: function (response, textStatus, jqXHR) {
                ProcessFindDuplicates(JSON.parse(response));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error - ' + errorThrown);
            }
        });
    }
}

//
// Process duplicates
//
function ProcessFindDuplicates(data) {
    if (data.length == 0) {
        $('#FirstName').val($('#FindDuplicatesFirstName').val());
        $('#LastName').val($('#FindDuplicatesLastName').val());
        $('#Email').val($('#FindDuplicatesEmail').val());
        $('#Phone1').val($('#SearchPhone').val());
        if (USER_NAME_REQUIRE_EMAIL) {
           $("#UserName").val($("#Email").val());
        }
        $('#FindDuplicatesDiv').hide();
        $('#CreateNewCustomerDiv').show();
    } else {
        PrepareSearchResultsGrid("DuplicatedCustomersDiv", data);
    }
}

//
// Cancel Duplicates
//
function FindDuplicatesCancel() {
    $('#ContactSearchDiv').show();
    $('#FindDuplicatesDiv').hide();
}

//
// Show Create new Customer
//
function ShowCreateNewCustomer() {
    $('#FirstName').val($('#SearchFirstName').val());
    $('#LastName').val($('#SearchLastName').val());
    $('#Email').val($('#SearchEmail').val());
    $('#Phone1').val($('#SearchPhone').val());
    $('#ContactSearchDiv').hide();
    $('#CreateNewCustomerDiv').show();
}

//
// Cancel Create New Customer
//
function CancelCreateNewCustomer() {
    $('#ContactSearchDiv').show();
    $('#CreateNewCustomerDiv').hide();
}

//
// Create new Customer
//
function CreateNewCustomer() {
    var isValid = $("#CustomerRegistrationForm").valid();
    if (isValid) {
        if ($("#ZipPostalCodeId").val() == "") {
            alert("Please Validte the Zip/Postal Code");
            return;
        }
        $("#CustomerRegistrationForm").submit();
    }
}

//
// Check Zip/Postal Code
//
function CheckZip() {
    var params = {
        "code": $("#Zip").val(),
        //"state": $("#State").val(),
        //"country": $("#Country").val()
    };

    $.ajax({
        type: 'post',
        data: params,
        dataType: 'json',
        cache: false,
        url: '/CustomerRegistration/CheckZipPostalCode',
        success: function (response, textStatus, jqXHR) {
            ProcessCheckZipPostalCode(JSON.parse(response));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });
}

//
// Process Check Zup/Postal Code
//
function ProcessCheckZipPostalCode(data) {
    if (data.length == 0) {
        // todo: Show as a UI warning validation
        alert("Zip/Postal Code not found");
    } else if (data.length == 1) {
        FillFieldsFromZipPostalCode(data[0]);
    } else {
        ShowZipPostalCodeGrid(data);
    }
}

//
// Fill Fields from Zip Postal Code
//
function FillFieldsFromZipPostalCode(zipData) {
    $("#City").val(zipData.City);
    $("#Country").val(zipData.Country.Key);
    $("#State").val(zipData.State.Key);
    $("#ZipPostalCodeId").val(zipData.ID);
}

//
// Show Zip/Postal Code Grid
//
function ShowZipPostalCodeGrid(data) {
    $("#ZipPostalCodeDiv").jsGrid({
        noDataContent: "No data found!",
        width: "1200px",
        sorting: true,
        paging: false,
        data: data,
        headerRowRenderer: function () {
            var $result = $("<tr>")
                         .append($("<th class='account_status_header'>").width(100).text("Zip/postal Code"))
                         .append($("<th class='account_status_header'>").width(250).text("Country"))
                         .append($("<th class='account_status_header'>").width(150).text("State"))
                         .append($("<th class='account_status_header'>").width(250).text("City"))
                         .append($("<th class='account_status_header'>").width(100).text("Code Type"))
            return $result;
        },
        fields: [
            { name: "ZipPostal", type: "text", width: 100 },
            { name: "Country.Value", type: "text", width: 250 },
            { name: "State.Value", type: "text", width: 150 },
            { name: "City", type: "text", width: 250 },
            { name: "CodeType", type: "text", width: 100 },
            {
                title: "Actions", type: "text", width: 75, align: "right",
                itemTemplate: function (value, item) {
                    return $("<a>").attr("href", "javascript:void(0)").on("click", function () {
                        FillFieldsFromZipPostalCode(item);
                        $("#ZipPostalCodeDiv").hide();
                        return false;
                    }).text("Select");
                }
            },
        ]
    });
}

/// Check if the user name is already taken
function ValidateUserName() {
    var params = {
        "userName": $('#UserName').val()
    };
    $.ajax({
        type: 'post',
        data: params,
        dataType: 'json',
        cache: false,
        url: '/CustomerRegistration/CheckUserName',
        success: function (response, textStatus, jqXHR) {
            ProcessCheckUserName(JSON.parse(response));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });
}

function ProcessCheckUserName(data) {
    if (data.length != 0) {
        var errors = [];
        var message = "";
        for (i = 0; i < data.length; i++) {
            errors[i] = data[i].message;
            message = message + "\n" + errors[i];
        }
        alert(message);
    }
}
