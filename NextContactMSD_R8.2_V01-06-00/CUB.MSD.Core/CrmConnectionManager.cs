/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using CUB.MSD.Model;
using static CUB.MSD.Model.CUBConstants;

namespace CUB.MSD.Core
{
    public class CrmConnectionManager : IDisposable
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CrmConnectionManager"/> class. 
        /// </summary>
        public static string Base64EncryptedStringToString(string encryptedValue)
        {
            string return_string = null;

            try
            {
                byte[] decodedBytes = Convert.FromBase64String(encryptedValue);
                return_string = new ASCIIEncoding().GetString(decodedBytes);
            }
            catch (Exception e)
            {
                //log the error
            }

            return return_string;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrmConnectionManager"/> class. 
        /// </summary>
        /// <param name="callerId"></param>
        public CrmConnectionManager(Guid? callerId = null)
        {
            var url = ConfigurationManager.AppSettings["CRM.IPURL"];
            var discoveryPath = ConfigurationManager.AppSettings["CRM.DiscoveryService"];
            var organizationName = ConfigurationManager.AppSettings["CRM.OrganizationUniqueName"];

            this.Service = this.GetOrganizationService(null, url + discoveryPath, organizationName);
            if (callerId.HasValue)
            {
                this.Service.CallerId = callerId.Value;
            }
            this.Service.EnableProxyTypes();
            this.CUBOrganizationServiceContext = new CUBOrganizationServiceContext(this.Service);
        }

        public CrmConnectionManager(string url, string organizationName)
        {
            this.Service = this.GetOrganizationService(null, url, organizationName);
            this.CUBOrganizationServiceContext = new CUBOrganizationServiceContext(this.Service);
        }


        #endregion

        #region Public Properties

        /// <summary>
        /// </summary>
        public CUBOrganizationServiceContext CUBOrganizationServiceContext { get; set; }

        /// <summary>
        ///     Gets the URL of the OrganizationData service endpoint.
        /// </summary>
        public string OrganizationDataServiceEndPoint
        {
            get
            {
                string rootWebApplicationUrl = this.WebApplicationUrl;
                return string.Concat(rootWebApplicationUrl, "/XRMServices/2011/OrganizationData.svc");
            }
        }

        /// <summary>
        /// </summary>
        public OrganizationServiceProxy Service { get; set; }

        /// <summary>
        ///     Return the URL of the CRM Organization.
        /// </summary>
        public string WebApplicationUrl { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(true);
        }

        /// <summary>
        /// </summary>
        /// <returns>
        /// The <see cref="OrganizationServiceProxy"/>.
        /// </returns>
        public OrganizationServiceProxy GetOrganizationService(IOrganizationService service, string url, string organizationName)
        {
            IServiceManagement<IDiscoveryService> serviceManagement =
                ServiceConfigurationFactory.CreateManagement<IDiscoveryService>(new Uri(url));

            AuthenticationProviderType endpointType = serviceManagement.AuthenticationType;

            // Set the credentials.
            AuthenticationCredentials authCredentials = GetCredentials(serviceManagement, endpointType);

            // Get the discovery service proxy.
            using (
                DiscoveryServiceProxy discoveryProxy =
                    this.GetProxy<IDiscoveryService, DiscoveryServiceProxy>(serviceManagement, authCredentials))
            {
                // Obtain organization information from the Discovery service. 
                if (discoveryProxy != null)
                {
                    // Obtain information about the organizations that the system user belongs to.
                    OrganizationDetailCollection orgs = this.DiscoverOrganizations(discoveryProxy);

                    // Obtains the Web address (Uri) of the target organization.
                    OrganizationDetail org = this.FindOrganization(organizationName, orgs.ToArray());
                    this.WebApplicationUrl = org.Endpoints[EndpointType.WebApplication];
                    url = org.Endpoints[EndpointType.OrganizationService];
                }
            }

            if (string.IsNullOrWhiteSpace(url))
            {
                throw new ApplicationException("Organization Service URL could not be determined.");
            }

            OrganizationServiceProxy organizationProxy = null;
            IServiceManagement<IOrganizationService> orgServiceManagement =
                ServiceConfigurationFactory.CreateManagement<IOrganizationService>(new Uri(url));
             
            // Set the credentials.
            AuthenticationCredentials credentials = GetCredentials(orgServiceManagement, endpointType);

            organizationProxy = this.GetProxy<IOrganizationService, OrganizationServiceProxy>(
                orgServiceManagement,
                credentials);
            organizationProxy.EnableProxyTypes();
            organizationProxy.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Clear();
            organizationProxy.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(
                new ProxyTypesBehavior(Assembly.GetExecutingAssembly()));

            return organizationProxy;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Obtain the AuthenticationCredentials based on AuthenticationProviderType.
        /// </summary>
        /// <param name="service">A service management object.</param>
        /// <param name="endpointType">An AuthenticationProviderType of the CRM environment.</param>
        /// <returns>Get filled credentials.</returns>
        private AuthenticationCredentials GetCredentials<TService>(IServiceManagement<TService> service, AuthenticationProviderType endpointType)
        {
            AuthenticationCredentials authCredentials = new AuthenticationCredentials();
            var domain = Base64EncryptedStringToString(ConfigurationManager.AppSettings["CRM.Domain"]);
            var username = Base64EncryptedStringToString(ConfigurationManager.AppSettings["CRM.Username"]);
            var password = Base64EncryptedStringToString(ConfigurationManager.AppSettings["CRM.Password"]);

            switch (endpointType)
            {
                case AuthenticationProviderType.ActiveDirectory:
                    authCredentials.ClientCredentials.Windows.ClientCredential =
                        new System.Net.NetworkCredential(username,
                            password,
                            domain);
                    break;
                case AuthenticationProviderType.LiveId:
                    authCredentials.ClientCredentials.UserName.UserName = username;
                    authCredentials.ClientCredentials.UserName.Password = password;
                    authCredentials.SupportingCredentials = new AuthenticationCredentials();
                    authCredentials.SupportingCredentials.ClientCredentials =
                        Microsoft.Crm.Services.Utility.DeviceIdManager.LoadOrRegisterDevice();
                    break;
                default: // For Federated and OnlineFederated environments.                    
                    authCredentials.ClientCredentials.UserName.UserName = username;
                    authCredentials.ClientCredentials.UserName.Password = password;
                    // For OnlineFederated single-sign on, you could just use current UserPrincipalName instead of passing user name and password.
                    // authCredentials.UserPrincipalName = UserPrincipal.Current.UserPrincipalName;  // Windows Kerberos

                    // The service is configured for User Id authentication, but the user might provide Microsoft
                    // account credentials. If so, the supporting credentials must contain the device credentials.
                    if (endpointType == AuthenticationProviderType.OnlineFederation)
                    {
                        IdentityProvider provider = service.GetIdentityProvider(authCredentials.ClientCredentials.UserName.UserName);
                        if (provider != null && provider.IdentityProviderType == IdentityProviderType.LiveId)
                        {
                            authCredentials.SupportingCredentials = new AuthenticationCredentials();
                            authCredentials.SupportingCredentials.ClientCredentials =
                                Microsoft.Crm.Services.Utility.DeviceIdManager.LoadOrRegisterDevice();
                        }
                    }

                    break;
            }

            return authCredentials;
        }

        /// <summary>
        /// </summary>
        /// <param name="disposing">
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
        }

        /// <summary>
        /// Discovers the organizations that the calling user belongs to.
        /// </summary>
        /// <param name="service">
        /// A Discovery service proxy instance.
        /// </param>
        /// <returns>
        /// Array containing detailed information on each organization that
        ///     the user belongs to.
        /// </returns>
        private OrganizationDetailCollection DiscoverOrganizations(IDiscoveryService service)
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }

            var orgRequest = new RetrieveOrganizationsRequest();
            var orgResponse = (RetrieveOrganizationsResponse)service.Execute(orgRequest);

            return orgResponse.Details;
        }

        /// <summary>
        /// Finds a specific organization detail in the array of organization details
        ///     returned from the Discovery service.
        /// </summary>
        /// <param name="orgUniqueName">
        /// The unique name of the organization to find.
        /// </param>
        /// <param name="orgDetails">
        /// Array of organization detail object returned from the discovery service.
        /// </param>
        /// <returns>
        /// Organization details or null if the organization was not found.
        /// </returns>
        /// <seealso cref="DiscoveryOrganizations"/>
        private OrganizationDetail FindOrganization(string orgUniqueName, OrganizationDetail[] orgDetails)
        {
            if (string.IsNullOrWhiteSpace(orgUniqueName))
            {
                throw new ArgumentNullException("orgUniqueName");
            }

            if (orgDetails == null)
            {
                throw new ArgumentNullException("orgDetails");
            }

            OrganizationDetail orgDetail = null;

            foreach (OrganizationDetail detail in orgDetails)
            {
                if (string.Compare(detail.UniqueName, orgUniqueName, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    orgDetail = detail;
                    break;
                }
            }

            return orgDetail;
        }

        /// <summary>
        /// Generic method to obtain discovery/organization service proxy instance.
        /// </summary>
        /// <typeparam name="TService">
        /// Set IDiscoveryService or IOrganizationService type to request respective service proxy instance.
        /// </typeparam>
        /// <typeparam name="TProxy">
        /// Set the return type to either DiscoveryServiceProxy or OrganizationServiceProxy type based on TService type.
        /// </typeparam>
        /// <param name="serviceManagement">An instance of IServiceManagement</param>
        /// <param name="authCredentials">The user's Microsoft Dynamics CRM logon credentials.</param>
        /// <returns></returns>
        private TProxy GetProxy<TService, TProxy>(
            IServiceManagement<TService> serviceManagement,
            AuthenticationCredentials authCredentials)
            where TService : class
            where TProxy : ServiceProxy<TService>
        {
            Type classType = typeof(TProxy);

            if (serviceManagement.AuthenticationType != AuthenticationProviderType.ActiveDirectory)
            {
                AuthenticationCredentials tokenCredentials =
                    serviceManagement.Authenticate(authCredentials);
                // Obtain discovery/organization service proxy for Federated, LiveId and OnlineFederated environments. 
                // Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and SecurityTokenResponse.
                return (TProxy)classType
                    .GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(SecurityTokenResponse) })
                    .Invoke(new object[] { serviceManagement, tokenCredentials.SecurityTokenResponse });
            }

            // Obtain discovery/organization service proxy for ActiveDirectory environment.
            // Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and ClientCredentials.
            return (TProxy)classType
                .GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(ClientCredentials) })
                .Invoke(new object[] { serviceManagement, authCredentials.ClientCredentials });
        }

        #endregion
    }

}
