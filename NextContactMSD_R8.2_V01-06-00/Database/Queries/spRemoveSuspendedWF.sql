﻿ 
if object_id('spRemoveSuspendedWF') is not null
	drop procedure dbo.spRemoveSuspendedWF;
GO
/****** Object:  StoredProcedure [dbo].[spRemoveSuspendedWF]    Script Date: 1/11/11 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--This removes workflows in a wait state for the named workflow!!!
--To check counts run these
select COUNT(*) from asyncoperationbase where name = 'Email Due Date Reminder' and StateCode = 1

SELECT COUNT(*) from workflowwaitsubscriptionbase

where asyncoperationid in

(select asyncoperationid from asyncoperationbase where name = 'Email Due Date Reminder' and StateCode = 1)

SELECT COUNT(*) from workflowlogbase

where asyncoperationid in

(select asyncoperationid from asyncoperationbase where name = 'Email Due Date Reminder' and StateCode = 1)


-- to improve performance create these indexes
--IF not EXISTS (SELECT name from sys.indexes WHERE name = N'CRM_AsyncOperation_CleanupCompleted')
-- 	  CREATE NONCLUSTERED INDEX CRM_AsyncOperation_CleanupCompleted ON [dbo].[AsyncOperationBase] ([StatusCode],[StateCode],[OperationType])
--GO
--IF not EXISTS (SELECT name from sys.indexes WHERE name = N'CRM_WorkflowLog_AsyncOperationID')
--	  CREATE NONCLUSTERED INDEX CRM_WorkflowLog_AsyncOperationID ON [dbo].[WorkflowLogBase] ([AsyncOperationID])
--GO
--IF not EXISTS (SELECT name from sys.indexes WHERE name = N'CRM_DuplicateRecord_AsyncOperationID')
--	  CREATE NONCLUSTERED INDEX CRM_DuplicateRecord_AsyncOperationID ON [dbo].[DuplicateRecordBase] ([AsyncOperationID])
--GO
--IF not EXISTS (SELECT name from sys.indexes WHERE name = N'CRM_BulkDeleteOperation_AsyncOperationID')
--	  CREATE NONCLUSTERED INDEX CRM_BulkDeleteOperation_AsyncOperationID ON [dbo].[BulkDeleteOperationBase] (AsyncOperationID)
--GO

CREATE PROCEDURE [dbo].[spRemoveSuspendedWF]
AS
BEGIN


delete 
--SELECT COUNT(*)
from workflowwaitsubscriptionbase
where asyncoperationid in
 (select asyncoperationid 
  from asyncoperationbase 
  where name = 'Update Booking and Departure Status' 
    and StateCode = 1)

delete 
--SELECT COUNT(*)
from workflowlogbase
where asyncoperationid in
  (select asyncoperationid 
   from asyncoperationbase 
   where name = 'Update Booking and Departure Status' 
     and StateCode = 1)

delete 
--SELECT StateCode,StatusCode
from asyncoperationbase 
where name = 'Update Booking and Departure Status' 
  and StateCode = 1


--use these queries to remove canceling WFs
delete 
SELECT COUNT(*)
from workflowwaitsubscriptionbase
where asyncoperationid in
 (select asyncoperationid 
  from asyncoperationbase 
  where name = 'DF Welcome Home Email' 
    and StateCode = 2)

delete 
--SELECT *
from workflowlogbase
where asyncoperationid in
  (select asyncoperationid 
   from asyncoperationbase 
   where name = 'DF Welcome Home Email' 
     and StateCode = 2)

delete 
--SELECT *
from asyncoperationbase 
where name = 'DF Welcome Home Email' 
  and StateCode = 2

END