﻿ --When you run Microsoft Dynamics CRM 4.0, the AsyncOperationBase table grows to be very large. When the table contains millions of records, performance is slow.

-- show how many rows will be deleted

--Select Count(AsyncOperationId)
--from AsyncOperationBase WITH (NOLOCK)
--where OperationType in (1, 9, 12, 25, 27, 10) --10 = workflows
--AND StateCode = 3 AND StatusCode IN (30,32)  

-- cleanup - run this on every org - KB = http://support.microsoft.com/kb/968520

if object_id('spCleanupAsyncOperationsWF') is not null
	drop procedure dbo.spCleanupAsyncOperationsWF;
GO
/****** Object:  StoredProcedure [dbo].[spCleanupAsyncOperationsWF]    Script Date: 1/11/11 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- to improve performance create these indexes
IF not EXISTS (SELECT name from sys.indexes WHERE name = N'CRM_AsyncOperation_CleanupCompleted')
 	  CREATE NONCLUSTERED INDEX CRM_AsyncOperation_CleanupCompleted ON [dbo].[AsyncOperationBase] ([StatusCode],[StateCode],[OperationType])
GO
IF not EXISTS (SELECT name from sys.indexes WHERE name = N'CRM_WorkflowLog_AsyncOperationID')
	  CREATE NONCLUSTERED INDEX CRM_WorkflowLog_AsyncOperationID ON [dbo].[WorkflowLogBase] ([AsyncOperationID])
GO
IF not EXISTS (SELECT name from sys.indexes WHERE name = N'CRM_DuplicateRecord_AsyncOperationID')
	  CREATE NONCLUSTERED INDEX CRM_DuplicateRecord_AsyncOperationID ON [dbo].[DuplicateRecordBase] ([AsyncOperationID])
GO
IF not EXISTS (SELECT name from sys.indexes WHERE name = N'CRM_BulkDeleteOperation_AsyncOperationID')
	  CREATE NONCLUSTERED INDEX CRM_BulkDeleteOperation_AsyncOperationID ON [dbo].[BulkDeleteOperationBase] (AsyncOperationID)
GO

CREATE PROCEDURE [dbo].[spCleanupAsyncOperationsWF]
AS
BEGIN

declare @DeleteRowCount int
Select @DeleteRowCount = 2000
declare @DeletedAsyncRowsTable table (AsyncOperationId uniqueidentifier not null primary key)
declare @continue int, @rowCount int
select @continue = 1
while (@continue = 1)
begin
	  begin tran
	   insert into @DeletedAsyncRowsTable(AsyncOperationId)
	  -- Select top (@DeleteRowCount) AsyncOperationId
	  Select AsyncOperationId
	  from AsyncOperationBase a WITH (NOLOCK)
	  inner JOIN dbo.P15_bookings b ON a.RegardingObjectId=b.P15_bookingsId
	  where OperationType in (10) -- workflows
	  AND a.StateCode = 3 -- completed
	  AND a.StatusCode IN (30,31,32)  -- succeeded, canceled
	  AND RegardingObjectTypeCode=10017 -- booking
      AND b.statecode=1 --inactive

  
	  Select @rowCount = 0
	  Select @rowCount = count(*) from @DeletedAsyncRowsTable
	  select @continue = case when @rowCount <= 0 then 0 else 1 end     
 
		if (@continue = 1)
		begin
			delete WorkflowLogBase from WorkflowLogBase W, @DeletedAsyncRowsTable d
			where W.AsyncOperationId = d.AsyncOperationId
            
            delete BulkDeleteFailureBase From BulkDeleteFailureBase B, @DeletedAsyncRowsTable d
			where B.AsyncOperationId = d.AsyncOperationId
 
			delete AsyncOperationBase From AsyncOperationBase A, @DeletedAsyncRowsTable d
			where A.AsyncOperationId = d.AsyncOperationId            
 
			delete @DeletedAsyncRowsTable
	  end
 
	  commit
end

--Drop the Index on AsyncOperationBase

--DROP INDEX AsyncOperationBase.CRM_AsyncOperation_CleanupCompleted

END