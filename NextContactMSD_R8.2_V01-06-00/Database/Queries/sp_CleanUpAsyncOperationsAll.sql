
IF OBJECT_ID('sp_CleanUpAsyncOperationsAll') is not null
	DROP PROCEDURE dbo.sp_CleanUpAsyncOperationsAll;
GO

CREATE Procedure [dbo].[sp_CleanUpAsyncOperationsAll]
AS
DECLARE @name VARCHAR(50) -- database name
DECLARE db_cursor CURSOR FOR  
SELECT name 
FROM master.dbo.sysdatabases 
WHERE name NOT IN ('default_mscrm')  
  AND Name LIKE '%_MSCRM'
  
OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @name   

WHILE @@FETCH_STATUS = 0   
BEGIN   
	DECLARE @sql AS NVARCHAR(max)
	SET @sql = '
	if (object_id(''' + @name + '.dbo.spCleanupAsyncOperations'') IS NOT NULL )
	BEGIN
		EXEC ' + @name + '.dbo.spCleanupAsyncOperations
	END '
	
	--print (@sql)
	exec (@sql)
	
	FETCH NEXT FROM db_cursor INTO @name   
END   

CLOSE db_cursor   
DEALLOCATE db_cursor  

RETURN @@ERROR