﻿CREATE PROCEDURE [ExpandFilterAttributes]
AS


select Maxlength from MetadataSchema.Attribute
where name ='filteringattributes'
Update MetadataSchema.Attribute
SET Maxlength = 500
where name ='filteringattributes'
select [description] , * from [dbo].[SdkMessageProcessingStepBase] where [description] = 'ContactUpdateHouseholdAddress'
update [dbo].[SdkMessageProcessingStepBase] 
set filteringattributes = 'address1_line1,address1_line2,address1_city,address1_stateorprovince,address1_postalcode,address1_country,p15_contacthouseholdid' 
where [description] = 'ContactUpdateHouseholdAddress'



select [description] , * from [dbo].[SdkMessageProcessingStepBase] where [description] = 'HouseholdUpdateRelatedContactsShippingAddresses'
update [dbo].[SdkMessageProcessingStepBase] 
set filteringattributes = 'address2_line1,address2_line2,address2_city,address2_stateorprovince,address2_postalcode,address2_country' 
where [description] = 'HouseholdUpdateRelatedContactsShippingAddresses'
select [description] , * from [dbo].[SdkMessageProcessingStepBase] where [description] = 'HouseHoldUpdateRelatedContactsPrimaryAddresses'
update [dbo].[SdkMessageProcessingStepBase] 
set filteringattributes = 'address1_line1,address1_line2,address1_city,address1_stateorprovince,address1_postalcode,address1_country' 
where [description] = 'HouseHoldUpdateRelatedContactsPrimaryAddresses'

RETURN 0


RETURN 0