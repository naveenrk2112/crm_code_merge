﻿-- use this to find the longest running queries
-- try to reduce avgReadsPerExecution
SELECT TOP 50
      DB_Name(qp.dbid) as [Database] , 
      qp.number , qt.text,
	qs.total_logical_reads as [Reads], 
	qs.total_logical_reads/execution_count as [avgReadsPerExectuion],
     SUBSTRING(qt.text, (qs.statement_start_offset/2) + 1,
        ((CASE statement_end_offset 
            WHEN -1 THEN DATALENGTH(qt.text)
            ELSE qs.statement_end_offset END 
                - qs.statement_start_offset)/2) + 1) as statement_text
    FROM sys.dm_exec_query_stats as qs 
    CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle) as qp
    CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle) as qt
      ORDER BY qs.total_logical_reads    DESC
 