﻿
/****** Object:  StoredProcedure [dbo].[spBatchCleanupAsyncOperationsBase]    Script Date: 01/06/2011 10:00:59 ******/

if object_id('spBatchCleanupAsyncOperationsBase') is not null
	drop procedure dbo.spBatchCleanupAsyncOperationsBase;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spBatchCleanupAsyncOperationsBase] as
BEGIN

DECLARE @name VARCHAR(50) -- database name  
DECLARE db_cursor CURSOR FOR  
SELECT name 
FROM master.dbo.sysdatabases 
WHERE Name LIKE '%_MSCRM'

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @name   

WHILE @@FETCH_STATUS = 0   
BEGIN
-- EXEC spCleanupAsyncOperations(@name)
-- EXEC spCleanupAsyncOperationsWF(@name)
EXEC(
IF EXISTS (SELECT name from sys.indexes
				  WHERE name = N'CRM_AsyncOperation_CleanupCompleted')
	  DROP Index AsyncOperationBase.CRM_AsyncOperation_CleanupCompleted
GO
CREATE NONCLUSTERED INDEX CRM_AsyncOperation_CleanupCompleted
ON [dbo].[AsyncOperationBase] ([StatusCode],[StateCode],[OperationType])
GO

END

CLOSE db_cursor   

DEALLOCATE db_cursor  

END