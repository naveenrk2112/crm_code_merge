﻿-- This script will force Microsoft CRM 4.0 to run the deletion service 
-- clean up job right away; cleaning out all records "marked for deletion". 
-- After running this script, RESTART the Microsoft CRM Async Service 
--
UPDATE dbo.ScaleGroupOrganizationMaintenanceJobs SET 
NextRunTime = getdate() -- Now 
WHERE OperationType = 14 -- Deletion Service 
