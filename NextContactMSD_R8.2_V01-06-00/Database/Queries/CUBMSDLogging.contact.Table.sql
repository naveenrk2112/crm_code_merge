USE [CUBMSDLogging]
GO
/****** Object:  Table [dbo].[contact]    Script Date: 3/16/2017 4:19:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact](
	[Event_Id] [int] IDENTITY(1,1) NOT NULL,
	[Parent_Event_Id] [int] NULL,
	[OrgName] [nvarchar](255) NULL,
	[Create_Time] [datetime] NOT NULL,
	[Request_Time] [datetime] NULL,
	[Response_Time] [datetime] NULL,
	[Log_Level] [nvarchar](20) NULL,
	[Application] [nvarchar](100) NULL,
	[Logger] [nvarchar](250) NULL,
	[Method_Name] [nvarchar](250) NULL,
	[Sequence_Number] [tinyint] NULL,
	[Machine_Name] [nvarchar](100) NULL,
	[User_Name] [nvarchar](100) NULL,
	[Thread] [nvarchar](100) NULL,
	[Log_Message] [text] NULL,
	[Log_Exception] [text] NULL,
	[Request] [text] NULL,
	[Response] [text] NULL,
	[Run_Duration] [nvarchar](20) NULL,
	[Class_Name] [nvarchar](250) NULL,
	[Correlation_Id] [nvarchar](40) NULL,
 CONSTRAINT [PK_contact] PRIMARY KEY CLUSTERED 
(
	[Event_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
