﻿--The asyncoperationbase table stores details of system jobs (including workflows). The following table details some of the different job types that are stored in the table.

--operationtype Description 
--1 System Event 
--9 Collect SQM data 
--10* Workflow Operation 
--12 Update Match Code 
--25 Generate Full Text Catalog 
--27 Update Contract States 

--Note: For entries where the operationtype is 10, there are also related entries in the workflowlogbase table (join on the asyncoperationid field). The CRM SDK lists additional operationtypes.

--State Code   Status Code   
--3 Completed  30 Succeded 
--             31 Failed 
--             32 Canceled 
--2 Locked     20 In Progress 
--             21 Pausing 
--             22 Canceling 
--1 Suspended  10 Waiting
--0 Ready       0 Waiting for Resource

if object_id('spCleanupAsyncOperations') is not null
	drop procedure dbo.spCleanupAsyncOperations;
GO

/****** Object:  StoredProcedure [dbo].[spCleanupAsyncOperations]    Script Date: 6/4/2010 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF not EXISTS (SELECT name from sys.indexes WHERE name = N'CRM_AsyncOperation_CleanupCompleted')
	  --DROP Index AsyncOperationBase.CRM_AsyncOperation_CleanupCompleted
      --GO
	  CREATE NONCLUSTERED INDEX CRM_AsyncOperation_CleanupCompleted ON [dbo].[AsyncOperationBase] ([StatusCode],[StateCode],[OperationType])
GO

CREATE PROCEDURE [dbo].[spCleanupAsyncOperations]
AS
BEGIN

declare @DeleteRowCount int
Select @DeleteRowCount = 2000
declare @DeletedAsyncRowsTable table (AsyncOperationId uniqueidentifier not null primary key)
declare @continue int, @rowCount int
select @continue = 1
while (@continue = 1)
begin
	  begin tran
	  insert into @DeletedAsyncRowsTable(AsyncOperationId)
	 -- Select top (@DeleteRowCount) AsyncOperationId
	  Select AsyncOperationId
	  from AsyncOperationBase
	  where OperationType in (1, 9, 12, 25, 27) -- exclude workflows (10)
	    AND StateCode = 3 
	    AND StatusCode in (30, 32)     
	    --AND name NOT IN ('ALA Booking Checklist','Vaya Booking Checklist')
 
 
	  Select @rowCount = 0
	  Select @rowCount = count(*) from @DeletedAsyncRowsTable
	  select @continue = case when @rowCount <= 0 then 0 else 1 end     
 
		if (@continue = 1)
		begin
			delete WorkflowLogBase from WorkflowLogBase W, @DeletedAsyncRowsTable d
			where W.AsyncOperationId = d.AsyncOperationId
            
            delete BulkDeleteFailureBase From BulkDeleteFailureBase B, @DeletedAsyncRowsTable d
			where B.AsyncOperationId = d.AsyncOperationId
 
			delete AsyncOperationBase From AsyncOperationBase A, @DeletedAsyncRowsTable d
			where A.AsyncOperationId = d.AsyncOperationId            
 
			delete @DeletedAsyncRowsTable
	  end
 
	  commit
end

--Drop the Index on AsyncOperationBase

DROP INDEX AsyncOperationBase.CRM_AsyncOperation_CleanupCompleted

END