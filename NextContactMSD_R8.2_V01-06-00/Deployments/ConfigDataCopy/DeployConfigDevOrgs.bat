rem Deploy config data to all DEV orgs using exported files as source

set /p action=Enter Action (Insert/Upsert):

ConfigDataCopy.exe sourceorgname="file" targetorgnames="NextContactModelDev2|NextContactModelDev3|NextContactModelDev4|NextContactModelDev5|NextContactModelDev7|NextContactModelDev8|NextContactModelDev9|NextContactModelDev10|NextContactModelDev11" entitynames="cub_globals|cub_globaldetails|cub_optionsettranslation|cub_optionsettranslationdetail" action=%action%

rem ConfigDataCopy.exe sourceorgname="file" targetorgnames="NextContactModelDev2" entitynames="cub_globals|cub_globaldetails|cub_optionsettranslation|cub_optionsettranslationdetail" action=%action%
pause