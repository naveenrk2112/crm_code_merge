ConfigDataCopy.exe [options]
sourceorgname (optional): the name of a CRM connection string in app.config, if omitted defaults to NextContactModel.
targetorgnames: same as before, can be one-or-more CRM connection string names separated by pipe (�|�), or star (�*�) for all.
entitynames: same as before, can be  one-or-more entity logical names separated by pipe.
action: same as before, either Upsert or Insert.
 
Example:
ConfigDataCopy.exe sourceorgname="NextContactModelDev11" targetorgnames="NextContactModelDev9|NextContactModelDev10" entitynames="cub_globals|cub_globaldetails|cub_optionsettranslation|cub_optionsettranslationdetail" action="Upsert"
