echo off
rem set /p targetconnection=Enter Target Connection Name (Conn-NextContactOrg.xml): || set targetconnection=Conn-NextContactOrg.xml
rem set /p targetorgname=Enter Target Organization Name (NextContactTIM): || set targetorgname=NextContactTIM
rem set /p solution=Enter Solution Name (NXTCFY17Q3SP5_1_0.zip): || set solution=NXTCFY17Q3SP5_1_0.zip
rem set /p solutionsfolder=Enter Solution Location (c:\solutions\\):
rem set /p deleteplugins=Enter Plugins (true or false): || set deleteplugins=false
rem echo targetconnection = %targetconnection%
rem echo targetorgname = %targetorgname%
rem echo solution = %solution%
rem echo solutionsfolder = %solutionsfolder%
rem echo deleteplugins = %deleteplugins%
rem echo

rem Crm.Utilities.SolutionsDeploymentTool.Console.exe targetconnectionxml=Conn-NextContactTarget.xml targetorgnames=%targetorgname% solutionnames=%solution% mainoperation="importsolutionsfromfile" abortonerror="false" publishoption="publishallafterallsolutionsimported" silent="false" toemails="tim.klooster@cubic.com" deletepluginassemblies=%deleteplugins% solutionsfolder=%solutionsfolder% 
Crm.Utilities.SolutionsDeploymentTool.Console.exe targetconnectionxml=Conn-NextContactTarget.xml targetorgnames=%targetorgname% solutionnames=%solution% mainoperation="importsolutionsfromfile" abortonerror="false" publishoption="publishallafterallsolutionsimported" silent="false" toemails="tim.klooster@cubic.com" deletepluginassemblies=true 
pause

