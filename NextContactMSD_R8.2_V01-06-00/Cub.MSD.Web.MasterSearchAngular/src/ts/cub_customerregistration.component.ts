import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_CustomerRegistrationService } from './services/cub_customerregistration.service';
import { Cub_DataService } from './services/cub_data.service';
import * as lodash from 'lodash';

@Component({
    selector: 'app-landing',
    template: `<div class="content">
                   <div class="header center">
                        <h1>Create New Customer</h1>
                   </div>
                   <ul class="columns">
                        <li cub-controlscolumn class="column-width-M" 
                            [controls]="registrationControls" 
                            [controlsOrder]="registrationControlsColumn1"
                            [labelWidthClass]="'label-width-30'" 
                            (onChange)="inputChange($event)"
                            (onClick)="buttonClicked($event)"
                            (onRemove)="removedClicked($event)"></li>
                        <li cub-controlscolumn class="column-width-M" 
                            [controls]="registrationControls"
                            [controlsOrder]="registrationControlsColumn2"
                            [labelWidthClass]="'label-width-30'" 
                            (onChange)="inputChange($event)"
                            (onClick)="buttonClicked($event)"
                            (onRemove)="removedClicked($event)"></li>
                   </ul>
	               <ul class="buttons">
                        <li cub-button [control]="createCustomerButton" (onClick)="createButtonClicked($event)"></li>
                        <li cub-button [control]="cancelButton" (onClick)="cancelButtonClicked($event)"></li>
                   </ul>
                </div>`,
    providers: [Cub_CustomerRegistrationService]
})

export class Cub_CustomerRegistrationComponent {
    //For Customer Registration Form
    registrationControlsColumn1 = ['customerType', 'contactType', 'firstName', 'lastName', 'dateOfBirth', 'dateOfBirthDivider', 'email', 'addPhone'];
    registrationControlsColumn2 = ['country', 'addressline1', 'addressline2', 'state', 'zip', 'zipDivider', 'username', 'pin', 'addSecurityQuestion'];

    registrationControls: Object = {
        customerType: {
            type: "Dropdown",
            id: "customerType",
            label: "Customer Type",
            isRequired: true,
            errorMessage: 'Customer Type is required',
            fieldWidthClass: "form-field-width-75",
            spacerWidthClass: "form-field-width-25",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            options: []
        },
        contactType: {
            type: "Dropdown",
            id: "contactType",
            label: "Contact Type",
            isRequired: true,
            errorMessage: 'Contact Type is required',
            fieldWidthClass: "form-field-width-75",
            spacerWidthClass: "form-field-width-25",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            options: []
        },
        firstName: {
            type: "Textbox",
            id: "firstName",
            label: "First Name",
            isRequired: true,
            errorMessage: 'First Name is required',
            fieldWidthClass: "form-field-width-75",
            additionalControls: {
                middleInitial: {
                    type: "Textbox",
                    id: "middleInitial",
                    label: "MI",
                    fieldWidthClass: "form-field-width-100",
                    labelWidthClass: "label-width-50",
                    formFieldClass: 'form-fields-siblings'
                }
            }
        },
        lastName: {
            type: "Textbox",
            id: "lastName",
            label: "Last Name",
            isRequired: true,
            errorMessage: 'Last Name is required',
            fieldWidthClass: "form-field-width-75",
            spacerWidthClass: "form-field-width-25",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
        },
        dateOfBirth: {
            type: "Textbox", //TODO: This will be it's own control (not textbox) when we implement a date/time picker (most likely)
            id: "dateOfBirth",
            label: "Date Of Birth",
            fieldWidthClass: "form-field-width-40",
            spacerWidthClass: "form-field-width-60",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            placeholder: "MM/DD/YYYY"
        },
        dateOfBirthDivider: {
            type: "Divider"
        },
        email: {
            type: "Textbox",
            id: "email",
            label: "Email",
            isRequired: true,
            errorMessage: "Email is required",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            value: ''
        },
        phone1: { //Placeholder, intentionally left out of order array, as it will load when the phone types are loaded
        },
        addPhone: {
            type: "Button",
            id: 'addPhone',
            label: "Add Another Phone",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            class: 'button-secondary-text'
        },
        country: {
            type: "OptionSelect",
            id: "country",
            label: "Country",
            selectId: "countrySelect",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            dropdownFieldWidthClass: "form-field-width-60",
            value: '0',
            option1: {},
            option2: {},
            additionalOptions: new Array()
        },
        addressline1: {
            type: "Textbox",
            id: "addressLine1",
            label: "Address",
            isRequired: true,
            errorMessage: "Address is required",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
        },
        addressline2: {
            type: "Textbox",
            id: "addressLine2",
            label: "",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
        },
        state: {
            type: "Dropdown",
            id: "state",
            label: "State",
            isRequired: true,
            errorMessage: 'State is required',
            fieldWidthClass: "form-field-width-75",
            spacerWidthClass: "form-field-width-25",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            options: []
        },
        zip: {
            type: "Textbox",
            id: "zip",
            label: "ZIP",
            isRequired: true,
            fieldWidthClass: "form-field-width-30",
            spacerWidthClass: "form-field-width-70",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
        },
        zipDivider: {
            type: "Divider"
        },
        username: {
            type: "Textbox",
            id: "userName",
            label: "Username",
            isRequired: true,
            fieldWidthClass: "form-field-width-75",
            spacerWidthClass: "form-field-width-25",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
        },
        pin: {
            type: "Textbox",
            id: "pin",
            label: "PIN",
            isRequired: true,
            fieldWidthClass: "form-field-width-20",
            spacerWidthClass: "form-field-width-80",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
        },
        securityQuestion1: {//Placeholder, intentionally left out of order array, as it will load when the phone types are loaded
        },
        securityAnswer1: {//Placeholder, intentionally left out of order array, as it will load when the security questions are loaded''
        },
        addSecurityQuestion: {
            type: "Button",
            id: 'addSecurityQuestion',
            label: "Add Another Question",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            class: 'button-secondary-text'
        },
    }
    createCustomerButton: Object = {
        id: "CreateCustomer",
        label: "Create Customer",
        class: "button-primary"
    };
    cancelButton: Object = {
        id: "Cancel",
        label: "Cancel",
        class: "button-cancel"
    };

    public phoneCount: number = 0;
    public securityQuestionCount: number = 0;

    constructor(
        private ref: ChangeDetectorRef,
        private _cub_dataService: Cub_DataService,
        private _cub_customerRegistrationService: Cub_CustomerRegistrationService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    addPhone(isFirst: boolean = false) {
        this.phoneCount++;
        this.registrationControls['phone' + this.phoneCount] = { 
            type: "Textbox",
            id: 'phone' + this.phoneCount,
            isRequired: isFirst == true, 
            label: "Phone",
            errorMessage: "Phone is required",
            fieldWidthClass: "form-field-width-35",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            value: '',
            isRemovable: isFirst == false,
            additionalControls: new Array()
        };
        this.registrationControls['phone' + this.phoneCount].additionalControls['phoneType' + this.phoneCount] = {
            type: "OptionSelect",
            id: "phoneType" + this.phoneCount,
            label: "",
            selectId: "phoneType" + + this.phoneCount + "Select",
            fieldWidthClass: "form-field-width-65",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            dropdownFieldWidthClass: "form-field-width-40",
            value: '0',
            option1: lodash.cloneDeep(Object.assign({}, this._cub_customerRegistrationService.phoneTypeOption1, { id: 'phoneType' + this.phoneCount + 'Options' + this._cub_customerRegistrationService.phoneTypeOption1['id'] })),
            option2: lodash.cloneDeep(Object.assign({}, this._cub_customerRegistrationService.phoneTypeOption2, { id: 'phoneType' + this.phoneCount + 'Options' + this._cub_customerRegistrationService.phoneTypeOption2['id'] })),
            additionalOptions: this._cub_customerRegistrationService.phoneTypeAdditionalOptions,
        };

        this.registrationControlsColumn1.splice(this.registrationControlsColumn1.indexOf('addPhone'), 0, 'phone' + this.phoneCount);
    }

    addSecurityQuestion(isFirst: boolean = false) {
        this.securityQuestionCount++;
        this.registrationControls['securityQuestion' + this.securityQuestionCount] = {
            type: "Dropdown",
            id: 'securityQuestion' + this.securityQuestionCount,
            label: "Security Question",
            errorMessage: "Security Question is required",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            value: '0',
            isRemovable: isFirst == false,
            options: this._cub_customerRegistrationService.securityQuestionOptions
        };
        this.registrationControls['securityAnswer' + this.securityQuestionCount] = { 
            type: "Textbox",
            id: 'securityAnswer' + this.securityQuestionCount,
            label: "Security Answer",
            errorMessage: "Security Answer is required",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            value: ''
        };

        this.registrationControlsColumn2.splice(this.registrationControlsColumn2.indexOf('addSecurityQuestion'), 0, 'securityQuestion' + this.securityQuestionCount);
        this.registrationControlsColumn2.splice(this.registrationControlsColumn2.indexOf('addSecurityQuestion'), 0, 'securityAnswer' + this.securityQuestionCount);
    }

    buttonClicked(data: any): void {
        if (data) {
            var newValue = data[0];
            var control = data[1];
            if (control.id == 'addPhone') {
                if (this.phoneCount != 3) {
                    this.addPhone();
                }

                control.isHidden = this.phoneCount == 3; //TODO: Make configurable?
            }
            else if (control.id == 'addSecurityQuestion') {
                if (this.securityQuestionCount != 3) {
                    this.addSecurityQuestion();
                }

                control.isHidden = this.securityQuestionCount == 3; //TODO: Make configurable?
            }
        }
    }

    removedClicked(controlIds: any): void {
        if (controlIds) {
            if (controlIds[0].startsWith('phone')) { //Phone removable
                this.registrationControlsColumn1.splice(this.registrationControlsColumn1.indexOf(controlIds[0]), 1)
                this.phoneCount--;
                this.registrationControls['addPhone'].isHidden = this.phoneCount == 3; //TODO: Make configurable?
            }
            else if (controlIds[0].startsWith('securityQuestion')) { //Security Question/Answer removable
                //TODO: Remove Answers too
                this.registrationControlsColumn2.splice(this.registrationControlsColumn2.indexOf(controlIds[0]), 2)
                this.securityQuestionCount--;
                this.registrationControls['addSecurityQuestion'].isHidden = this.securityQuestionCount == 3; //TODO: Make configurable?
            }
        }
    }

    inputChange(data: any): void {
    }

    ngOnInit() {
        this.registrationControls['firstName'].value = this._cub_dataService.searchValues['firstName'];
        this.registrationControls['lastName'].value = this._cub_dataService.searchValues['lastName'];
        this.registrationControls['phone1'].value = this._cub_dataService.searchValues['phone'];
        this.registrationControls['email'].value = this._cub_dataService.searchValues['email'];

        this._cub_customerRegistrationService.getAccountTypes(this, function (scope: any, data: any, isError: boolean) {
            if (isError) {
                //TODO: Handle differently?
                alert("Error retrieving contact types: " + data);
                scope.registrationControls.customerType.options = [];
            }
            else {
                for (var i = 0; i < data.length; i++) {
                    var securityQuestion = data[i];
                    scope.registrationControls.customerType.options.push({
                        value: securityQuestion['Key'],
                        id: securityQuestion['Key'],
                        name: securityQuestion['Value']
                    });
                }
            }
        });

        this._cub_customerRegistrationService.getContactTypes(this, function (scope: any, data: any, isError: boolean) {
            if (isError) {
                //TODO: Handle differently?
                alert("Error retrieving contact types: " + data);
                scope.registrationControls.contactType.options = [];
            }
            else {
                for (var i = 0; i < data.length; i++) {
                    var securityQuestion = data[i];
                    scope.registrationControls.contactType.options.push({
                        value: securityQuestion['Key'],
                        id: securityQuestion['Key'],
                        name: securityQuestion['Value']
                    });
                }
            }
        });

        this._cub_customerRegistrationService.getSecurityQuestions(this, function (scope: any, data: any, isError: boolean) {
            if (isError) {
                //TODO: Handle differently?
                alert("Error retrieving security questions: " + data);
                scope._cub_customerRegistrationService.securityQuestionOptions = [];
            }
            else {
                scope._cub_customerRegistrationService.securityQuestionOptions = [{
                    value: "0",
                    id: "0",
                    name: "Select...",
                    isDisabled: true
                }];

                for (var i = 0; i < data.length; i++) {
                    var securityQuestion = data[i];
                    scope._cub_customerRegistrationService.securityQuestionOptions.push({
                        value: securityQuestion['Key'],
                        id: securityQuestion['Key'],
                        name: securityQuestion['Value']
                    });
                }
                scope.addSecurityQuestion(true);
            }
        });

        this._cub_customerRegistrationService.getCountries(this, function (scope: any, data: any, isError: boolean) {
            if (isError) {
                //TODO: Handle differently?
                alert("Error retrieving countires: " + data);
                scope.registrationControls.country.option1 = {};
                scope.registrationControls.country.option2 = {};
                scope.registrationControls.country.additionalOptions = [];
            }
            else {
                scope.registrationControls.country.additionalOptions = [{
                    value: "0",
                    name: "Select...",
                    isDisabled: true
                }];

                for (var i = 0; i < data.length; i++) {
                    var country = data[i];
                    if (i == 0) {
                        scope.registrationControls.country.option1 = {
                            value: country['Key'],
                            id: country['Key'],
                            name: country['Value'],
                            fieldWidthClass: 'form-field-width-15'
                        }
                    }
                    else if (i == 1) {
                        scope.registrationControls.country.option2 = {
                            value: country['Key'],
                            id: country['Key'],
                            name: country['Value'],
                            fieldWidthClass: 'form-field-width-20'
                        }
                    }
                    else {
                        scope.registrationControls.country.additionalOptions.push({
                            value: country['Key'],
                            id: country['Key'],
                            name: country['Value'],
                        });
                    }
                }
            }
        });

        this._cub_customerRegistrationService.getPhoneTypes(this, function (scope: any, data: any, isError: boolean) {
            if (isError) {
                //TODO: Handle differently?
                alert("Error retrieving phone types: " + data);
                scope._cub_customerRegistrationService.phoneTypeOption1 = {};
                scope._cub_customerRegistrationService.phoneTypeOption2 = {};
                scope._cub_customerRegistrationService.phoneTypeAdditionalOptions= [];
            }
            else {
                scope._cub_customerRegistrationService.phoneTypeAdditionalOptions = [{
                    value: "0",
                    name: "Select...",
                    isDisabled: true
                }];

                for (var i = 0; i < data.length; i++) {
                    var country = data[i];
                    if (i == 0) {
                        scope._cub_customerRegistrationService.phoneTypeOption1 = {
                            value: country['Key'],
                            id: country['Key'],
                            name: country['Value'],
                            fieldWidth: '30'
                        }
                    }
                    else if (i == 1) {
                        scope._cub_customerRegistrationService.phoneTypeOption2= {
                            value: country['Key'],
                            id: country['Key'],
                            name: country['Value'],
                            fieldWidth: '30'
                        }
                    }
                    else {
                        scope._cub_customerRegistrationService.phoneTypeAdditionalOptions.push({
                            value: country['Key'],
                            id: country['Key'],
                            name: country['Value'],
                            fieldWidth: '40'
                        });
                    }
                }
            }
            scope.addPhone(true);
        });
    }

    createButtonClicked(event: any) {
        alert("Create Customer Service call here!");
    }

    cancelButtonClicked(event: any) {
        this.router.navigate(['CustomerSearch']);
    }
}