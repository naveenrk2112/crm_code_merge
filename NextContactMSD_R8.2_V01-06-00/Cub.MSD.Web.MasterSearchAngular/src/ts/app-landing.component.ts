import {Component, Input} from '@angular/core';

//TODO: We can put a page header here, and this is where everything should originate
@Component({
    selector: 'app-landing',
    template: `<div class="header center" style="
                    font-size: 18px;
                ">
                    <h1>Landing Page For Development</h1><br>
                    <p>Please select a page to go to:</p>
                    <a routerLink="/CustomerSearch" routerLinkActive="active">Customer Search</a><br>
                    <a routerLink="/CustomerVerification" routerLinkActive="active">Customer Verification</a><br>
                    <a routerLink="/CustomerRegistration" routerLinkActive="active">Customer Registration</a><br>
                    <a routerLink="/CustomerDuplicate" routerLinkActive="active">Customer Duplicate</a><br>
                    <a routerLink="/MasterSearchContact" routerLinkActive="active">Master Search Contact</a><br>
               </div>`,
    styleUrls: ['/css/styles.css']
})

export class AppLandingComponent {
    ngOnInit() {
    }
}