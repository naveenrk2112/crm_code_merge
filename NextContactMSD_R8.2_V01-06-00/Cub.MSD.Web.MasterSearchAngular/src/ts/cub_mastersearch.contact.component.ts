﻿import 'rxjs/add/operator/switchMap';
import {Component, Input, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Cub_MasterSearchService } from './services/cub_mastersearch.service';
import { Cub_DataService } from './services/cub_data.service';
import { DataTableModule, SharedModule } from 'primeng/primeng';

@Component({
    selector: 'cub-mastersearchcontact',
    templateUrl: './html/cub_mastersearch.contact.component.html',
    styleUrls: ['./css/styles.css'],
    //inputs: ['controls: controls'],
    providers: [Cub_MasterSearchService]
})

export class Cub_MasterSearchContactComponent implements OnInit {
    controls: Object = {};
    firstName: Object = {};
    lastName: Object = {};
    phone: Object = {};
    email: Object = {};
    searchButton: Object = {};
    register: any;
    busyCounter: number = 0;
    displaySearchResults: boolean = false;
    rowsOnPage = 5;
    sortBy = "BestMatch";
    sortOrder = "asc";
    sortOrderOptions = [
        { value: "BestMatch", name: "Best Match" },
        { value: "AccountName", name: "Customer Name" },
        { value: "FullName", name: "Contact Full Name" },
        { value: "FirstName", name: " Contact First Name" },
        { value: "LastName", name: "Contact Last Name" },
        { value: "Email", name: "Contact Email" }
    ];
    sortOrderControl = {
        formFieldClass: 'form-fields-vertical', //Form field class to use, eg. form-fields-siblings
        fieldWidth: 'XL', //Width of field (0-100)
        labelWidth: 'M', //Width of label
        //spacerWidth: '0', //Width of space to right of field (0-100)
        //isDisabled: false, //Disable component
        //isRequired: false, //Require a value
        isTableControl: false,
        //errorMessage: '', //Error message to display when required or validation not met
        id: 'sortOrderControl', //uniquie control id
        label: 'Sort by', //Label for control
        value: "BestMatch", //Value for control
        options: [ //Array of objects: {name: "Label Text", value: "Value"}
            { value: "BestMatch", name: "Best Match" }, 
            { value: "AccountName", name: "Customer Name" },
            { value: "FullName", name: "Contact Full Name" },
            { value: "FirstName", name: " Contact First Name" },
            { value: "LastName", name: "Contact Last Name" },
            { value: "Email", name: "Contact Email" }]
    };

    constructor(
        private _cub_masterSearchService: Cub_MasterSearchService,
        private _cub_dataService: Cub_DataService,
        private route: ActivatedRoute,
        private router: Router) {}

    handleRequired(data:any, scope:any){
        var phone = this.phone;
        var email = this.email;
        var firstName = this.firstName;
        var lastName = this.lastName
        if (phone["value"] || email["value"]) {
            firstName["isRequired"] = false;
            lastName["isRequired"] = false;
            phone["isRequired"] = !!phone["value"];
            email["isRequired"] = !!email["value"];
        }
        else if (this.firstName["value"] || lastName["value"]) {
            firstName["isRequired"] = true;
            lastName["isRequired"] = true;
            phone["isRequired"] = false;
            email["isRequired"] = false;
        }
        else {
            firstName["isRequired"] = true;
            lastName["isRequired"] = true;
            phone["isRequired"] = true;
            email["isRequired"] = true;
        }

        if ((firstName["value"] && lastName["value"] ||
            phone["value"] ||
            email["value"])) {
            this.searchButton["isDisabled"] = false;
        }
        else {
            this.searchButton["isDisabled"] = true;
        }
    }

    ngOnInit() {
       this.firstName= {
            type: "Textbox",
            id: "firstName",
            class: "form-field-width-M",
            isRequired: true,
            label: "First Name",
            value: "",
            onChange: this.handleRequired
        };
        this.lastName= {
            type: "Textbox",
            id: "lastName",
            class: "form-field-width-M",
            isRequired: true,
            label: "Last Name",
            value: "",
            onChange: this.handleRequired
        };
        this.phone= {
            type: "Textbox",
            id: "phone",
            class: "form-field-width-S",
            isRequired: true,
            label: "Phone",
            value: "",
            onChange: this.handleRequired
        };
        this.email= {
            type: "Textbox",
            id: "email",
            class: "form-field-width-XL",
            isRequired: true,
            label: "Email",
            value: "",
            onChange: this.handleRequired
        };
        
        this.searchButton= {
            id: "searchButton",
            label: "Search",
            class: "button-primary",
            isDisabled: true        
        };
        this.handleRequired(null, this);
    }

    searchButtonClicked(scope: any) {
        //TODO: get the variables and then pass to the search service
        this.sortBy = "BestMatch"; //reset sorting;
        this._cub_masterSearchService.searchCustomer(this, this.searchCompleted);
    }

    searchCompleted(scope: any, data: any, isError: boolean) {
        //do initial sort of 
        scope.displaySearchResults = true;
        scope._cub_dataService.sortMasterSearchContactResults(scope.sortBy, true);
    }

    onSortDropdownChanged(event: any) {
        if (!event || event.length <= 0) {
            console.log('Missing event information from dropdown event')
            return;
        }
        var selectedValue = event[0];
        this._cub_dataService.sortMasterSearchContactResults(selectedValue, true);
    }

    buttonClicked(event: any) {
        var control = event[1];
        if(control.onClick) {
            control.onClick(this);
        }
    }

    textChanged(data: any = null, scope: any) {
        if (data) {
            var newValue = data[0];
            var textControl = data[1];
            this._cub_dataService.searchParams[textControl['id']] = newValue;
        }
        this.handleRequired(data, scope);
    }

    // special properties:

    rowClick(rowEvent: any) {
        console.log('Clicked: ' + rowEvent.row.item.FullName);
    }

    rowDoubleClick(rowEvent: any) {
        console.log('Double clicked: ' + rowEvent.row.item.FullName);
    }

    rowTooltip(item: any) {
        return item.ContactId;
    }

    verifyContactClicked(event: any) {
        console.log(event);
        this._cub_dataService.selectedContactCustomerToVerify = event;
        this.router.navigate(["/CustomerVerification"])
    }
}