﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
//import 'mdn-polyfills/Object.assign';
import { AppModule } from './app.module';
const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);