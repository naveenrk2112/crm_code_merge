﻿import 'rxjs/add/operator/switchMap';
import {Component, Input, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_CustomerSearchService } from './services/cub_customersearch.service';
import { Cub_DataService } from './services/cub_data.service';

@Component({
    selector: 'cub-customerregistration',
    template: `<div class="header center">
                    <h1>Create New Customer</h1>
               </div>
               <ul class="columns">
                    <li cub-controlscolumn class="column-width-XS" 
                        [controls]="searchControls" 
                        [controlsOrder]="searchControlsColumn"
                        [labelWidthClass]="'label-width-25'" 
                        (onChange)="onTextChange($event)"></li>
               </ul>
	           <ul class="buttons">
                    <li cub-button [control]="searchCustomerButton" (onClick)="searchButtonClicked($event)"></li>
                    <li cub-button [control]="cancelButton" (onClick)="cancelButtonClicked($event)"></li>
               </ul>`,
    providers: [ Cub_CustomerSearchService ]
})

export class Cub_CustomerSearchComponent {
        searchControls: Object = {
            firstName: {
                type: "Textbox",
                id: "firstName",
                label: "First Name",
                isRequired: true,
                onChange: "handleRequired",
                errorMessage: "First name is required",
                fieldWidthClass: "form-field-width-100",
                isTableControl: true,
                formFieldClass: 'form-fields-siblings',
                value: ''
            },
            lastName: {
                type: "Textbox",
                id: "lastName",
                label: "Last Name",
                isRequired: true,
                errorMessage: "Last name is required",
                fieldWidthClass: "form-field-width-100",
                isTableControl: true,
                formFieldClass: 'form-fields-siblings',
                value: ''
            },
            email: {
                type: "Textbox",
                id: "email",
                label: "Email",
                isRequired: true,
                errorMessage: "Email is required",
                fieldWidthClass: "form-field-width-100",
                isTableControl: true,
                formFieldClass: 'form-fields-siblings',
                value: ''
            },
            phone: {
                type: "Textbox",
                id: "phone1",
                label: "Phone",
                isRequired: true,
                errorMessage: "Phone is required",
                fieldWidthClass: "form-field-width-100",
                isTableControl: true,
                formFieldClass: 'form-fields-siblings',
                value: ''
            }
        };
        searchControlsColumn = ['firstName', 'lastName', 'email', 'phone'];

    searchCustomerButton: Object = {
        id: "CreateCustomer",
        label: "Create Customer",
        class: "button-primary"
    };
    cancelButton: Object = {
        id: "Cancel",
        label: "Cancel",
        class: "button-cancel"
    };

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_customerSearchService: Cub_CustomerSearchService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.onTextChange();
    }

    cancelButtonClicked(event: any) {
        this.router.navigate(['']);
    }

    searchButtonClicked(event: any) {
        var isValidated = this.validate(null, true);
        if (isValidated) {
            this._cub_dataService.busyScreen.busyText = "Searching for duplicates...";
            this._cub_dataService.busyScreen.buttonText = '';
            this._cub_dataService.busyScreen.busyCount++;
            this._cub_dataService.busyScreen.icon = 'loading';
            this._cub_customerSearchService.searchCustomer(this, function (scope: any, data: any, isError: boolean) {
                //TODO: Handle error?
                if (isError) {
                    alert("Error: " + data);
                    scope._cub_dataService.busyScreen.busyCount--;
                }
                else if (data && data.length > 0) {       
                    scope._cub_dataService.busyScreen.busyText = data.length + ' duplicates found!';
                    scope._cub_dataService.busyScreen.icon = '';
                    setTimeout(function () {
                        scope._cub_dataService.busyScreen.busyCount--;
                        scope.router.navigate(['CustomerDuplicate']);
                    }, 500);
                }
                else {
                    scope._cub_dataService.busyScreen.busyText = 'No duplicates found';
                    scope._cub_dataService.busyScreen.icon = 'check';
                    setTimeout(function () {
                        scope._cub_dataService.busyScreen.busyCount--;
                        scope.router.navigate(['CustomerRegistration']);
                    }, 500);
                }
            });
        }
    }

    onTextChange(data: any = null) {
        //Todo: Do another way
        if (data) {
            var newValue = data[0];
            var textControl = data[1];
            this._cub_dataService.searchValues[textControl['id']] = newValue;
        }

        var firstNameControl = this.searchControls['firstName'];
        var lastNameControl = this.searchControls['lastName'];
        var emailControl = this.searchControls['email'];
        var phoneControl = this.searchControls['phone'];

        if (!firstNameControl['value'] && !lastNameControl['value']) {
            firstNameControl.isRequired = false;
            lastNameControl.isRequired = false;
            phoneControl.isRequired = !!phoneControl['value'];
            emailControl.isRequired = !!emailControl['value'];
            this.validate();
        }
        else if (!!firstNameControl['value'] || !!lastNameControl['value']) {
            firstNameControl.isRequired = true;
            lastNameControl.isRequired = true;
            phoneControl.isRequired = false;
            emailControl.isRequired = false;
            this.validate();
        }
        else {
            firstNameControl.isRequired = true;
            lastNameControl.isRequired = true;
            phoneControl.isRequired = true;
            emailControl.isRequired = true;
            this.validate(true);
        }
    }

    validate(clear: boolean = false, showMessage: boolean = false) {
        var isValidated = true;
        var controlKeys = this.searchControlsColumn;

        for (var key in controlKeys) {
            var control = this.searchControls[controlKeys[key]];
            switch (control.type) {
                //TODO: Handle other types of controls for validation
                case "Textbox":
                    {
                        if (control.isRequired && !control.value && !clear) {
                            control.isErrored = true
                            control.showErrorMessage = showMessage;
                            isValidated = false;
                        }
                        else {
                            control.isErrored = false;
                            control.showErrorMessage = false;
                        }
                    }
            }
        }

        return isValidated;
    }
}