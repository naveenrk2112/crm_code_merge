﻿import { Component, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'cub-customerverified',
    template: `
        <div class="modal">
    	    <ul class="window contact-verified">
	    	    <li class="icon">
		    	    <div class="icon-confirmation-check">
			    	    <svg viewBox="0 0 61 61">
				    	    <circle cx="30.5" cy="30.5" r="29.5"/>
					        <polyline points="16,30.5 25.25,39.75 45,20 "/>
				        </svg>
			        </div>
		        </li>
		        <li class="header center">
		    	    <h1>Contact Verified</h1>
		        </li>
	        </ul>
        </div>`
})
export class Cub_CustomerVerifiedComponent implements AfterViewInit {
    constructor (
        private router: Router
    ) { }
    
    ngAfterViewInit(): void {
        setTimeout(() => this.router.navigateByUrl(''), 500);
    }
}