﻿import { Injectable, NgModule } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Cub_DataService } from './services/cub_data.service';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class Cub_CustomerVerificationService {
    private contactId: string
    constructor(
        private _http: Http,
        private _cub_dataService: Cub_DataService
    ) {
        this.contactId = this._cub_dataService.selectedContactCustomerToVerify['ContactId'];
    }

    _get(scope: any, callback: Function, requestName: string) {
        this._cub_dataService.incrementLoader();
        this._http.get(`http://localhost:12663/CustomerVerification/${requestName}?contactId=${this.contactId}`)
            .toPromise()
            .then(response => response.json())
            .then(data => {
                this._cub_dataService.decrementLoader();
                callback(scope, data);
            }).catch(err => {
                console.log(err);
                this._cub_dataService.decrementLoader();
                callback(scope);
            })
    }

    getSecurityQuestions(scope: any, callback: Function) {
        this._get(scope, callback, 'GetSecurityQA');
    }

    getAddress(scope: any, callback: Function) {
        this._get(scope, callback, 'GetAddress');
    }

    getBirthDate(scope: any, callback: Function) {
        this._get(scope, callback, 'GetBirthDate');
    }

    getPIN(scope: any, callback: Function) {
        this._get(scope, callback, 'GetPIN');
    }
}