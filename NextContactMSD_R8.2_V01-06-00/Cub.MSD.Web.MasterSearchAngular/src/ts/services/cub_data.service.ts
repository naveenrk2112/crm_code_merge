﻿import {Injectable, NgModule} from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
//TODO: Remove this reference from data service
import { SecurityOption } from '../cub_customerverification.component';
import 'rxjs/add/operator/toPromise';
import * as _ from 'lodash';

//Used to call service for Customer Registration
@Injectable()
export class Cub_DataService {
    busyScreen: {
        busyCount: number,
        busyText: string,
        buttonText: string,
        buttonRoute: string,
        icon: string
    } = {
        busyCount: 0,
        busyText: "Loading...",
        buttonText: '',
        buttonRoute: '',
        icon: 'loading'
    };

    public searchParams: Object = {};
    public masterSearchContactResults: Array<string> = [];
    public sortedMasterSearchContactResults: Array<string> = [];
    public selectedContactCustomerToVerify: Object = {};
    public duplicateSearchResults: Array<string> = []; //Values used for duplicate results
    public controlConfiguration: Object = {}; //Language values for labels
    public customerVerification: { //Values for customer verification
        alerts?: string[]
    };

    //Used to pass search values from Customer Search to Customer Registration
    searchValues: Object = {
        firstName: '',
        lastName: '',
        phone: '',
        email: ''
    };

    searchControls: Object = {};
    searchControlsColumn: Array<string>;

    sortMasterSearchContactResults(field: string, inAscendingOrder: boolean) {
        if (field.toLocaleLowerCase() === "BestMatch".toLocaleLowerCase()) {
            this.sortedMasterSearchContactResults = this.masterSearchContactResults;
            return;
        }
        if (!this.masterSearchContactResults ||
            !this.masterSearchContactResults[0] ||
            !this.masterSearchContactResults[0][field]) {
            console.log("Error while sorting results, master Search Contact results is null or has no field with that name.");
            return;
        }
        var sortOrder = 'desc'
        if (inAscendingOrder) {
            sortOrder = 'asc';
        }
        this.sortedMasterSearchContactResults = _.orderBy(this.masterSearchContactResults, [field], ['asc']);
    }

    incrementLoader() {
        this.busyScreen.busyText = 'Loading';
        this.busyScreen.buttonRoute = '';
        this.busyScreen.icon = 'loading';
        this.busyScreen.buttonText = '';
        this.busyScreen.busyCount++;
    }

    decrementLoader() {
        this.busyScreen.busyCount--;
    }

    constructor(private _http: Http) {
        this.searchParams = { //For Master Search
            firstName: '',
            lastName: '',
            email: '',
            phone: ''
        };

        this.customerVerification = {
            alerts: ['Customer is hard of hearing']
        }
    }
}