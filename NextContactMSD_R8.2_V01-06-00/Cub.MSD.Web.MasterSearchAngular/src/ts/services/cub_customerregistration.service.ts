﻿import {Injectable, NgModule} from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Cub_DataService } from './cub_data.service';
import 'rxjs/add/operator/toPromise';

//Used to call service for Customer Registration
@Injectable()
export class Cub_CustomerRegistrationService {

    public searchParams: Object = {};

    phoneTypeOption1: {
        name: string,
        value: string,
        id: string,
        fieldWidth: string
    } = {
        name: '',
        value: '',
        id: '',
        fieldWidth: ''
    };

    phoneTypeOption2: {
        name: string,
        value: string,
        id: string,
        fieldWidth: string
    } = {
        name: '',
        value: '',
        id: '',
        fieldWidth: ''
    };

    phoneTypeAdditionalOptions: Array<Object> = [];

    securityQuestionOptions: Array<Object> = [];

    constructor(private _http: Http,
        private _cub_dataService: Cub_DataService) {
    }

    getPhoneTypes(scope: any, callback: Function) {
        this._cub_dataService.incrementLoader();
        var headers = new Headers({ 'Content-Type': 'application/json' });

        this._http.get('http://usdced-msdyn09.cts.cubic.cub:12663/CustomerRegistration/GetPhoneTypes', { headers: headers })
            .toPromise()
            .then(response => response.json())
            .then(function (countries) {
                callback(scope, countries);
                scope._cub_dataService.decrementLoader();
            })
            .catch(function (error) {
                callback(scope, error, true);
            });
    }

    getCountries(scope: any, callback: Function) {
        this._cub_dataService.incrementLoader();
        var headers = new Headers({ 'Content-Type': 'application/json' });

        this._http.get('http://usdced-msdyn09.cts.cubic.cub:12663/CustomerRegistration/GetCountries', { headers: headers })
            .toPromise()
            .then(response => response.json())
            .then(function (countries) {
                callback(scope, countries);
                scope._cub_dataService.decrementLoader();
            })
            .catch(function (error) {
                callback(scope, error, true);
            });
    }

    getSecurityQuestions(scope: any, callback: Function) {
        this._cub_dataService.incrementLoader();
        var headers = new Headers({ 'Content-Type': 'application/json' });

        this._http.get('http://usdced-msdyn09.cts.cubic.cub:12663/CustomerRegistration/GetSecurityQuestions', { headers: headers })
            .toPromise()
            .then(response => response.json())
            .then(function (securityQuestions) {
                callback(scope, securityQuestions);
                scope._cub_dataService.decrementLoader();
            })
            .catch(function (error) {
                callback(scope, error, true);
            });
    }

    getContactTypes(scope: any, callback: Function) {
        this._cub_dataService.incrementLoader();
        var headers = new Headers({ 'Content-Type': 'application/json' });

        this._http.get('http://localhost:12663/CustomerRegistration/GetContactTypes', { headers: headers })
            .toPromise()
            .then(response => response.json())
            .then(function (countries) {
                callback(scope, countries);
                scope._cub_dataService.decrementLoader();
            })
            .catch(function (error) {
                callback(scope, error, true);
            });
    }

    getAccountTypes(scope: any, callback: Function) {
        this._cub_dataService.incrementLoader();
        var headers = new Headers({ 'Content-Type': 'application/json' });

        this._http.get('http://localhost:12663/CustomerRegistration/GetAccountTypes', { headers: headers })
            .toPromise()
            .then(response => response.json())
            .then(function (countries) {
                callback(scope, countries);
                scope._cub_dataService.decrementLoader();
            })
            .catch(function (error) {
                callback(scope, error, true);
            });
    }
}