﻿import {Injectable, NgModule} from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Cub_DataService } from './cub_data.service';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class Cub_CustomerSearchService {

    public searchParams: Object = {};

    constructor(private _http: Http,
        private _cub_dataService: Cub_DataService) {
    }

    searchCustomer(scope: any, callback: Function) {
        var params = {
            'FirstName': this._cub_dataService.searchValues['firstName'],
            'LastName': this._cub_dataService.searchValues['lastName'],
            'Email': this._cub_dataService.searchValues['email'],
            'Phone': this._cub_dataService.searchValues['phone'],
        };

        var headers = new Headers({ 'Content-Type': 'application/json' });

        this._http.post('http://usdced-msdyn09.cts.cubic.cub:12663/CustomerRegistration/SearchContact', JSON.stringify(params), { headers: headers })
            .toPromise()
            .then(response => JSON.parse(response.json()))
            .then(function (data) {
                scope._cub_dataService.duplicateSearchResults = data;
                callback(scope, data);
            })
            .catch(function (error) {
                scope._cub_dataService.duplicateSearchResults = [];
                callback(scope, error, true);
            });
    }
}