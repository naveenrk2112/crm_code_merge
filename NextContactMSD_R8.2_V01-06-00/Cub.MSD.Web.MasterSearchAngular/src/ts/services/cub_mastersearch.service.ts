﻿import {Injectable, NgModule} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import { Cub_DataService } from './cub_data.service';
import 'rxjs/add/operator/toPromise';

//Used to call service for Master Search
@Injectable()
export class Cub_MasterSearchService {
    constructor(private _http: Http, private _cub_dataservice: Cub_DataService){}

    searchCustomer(scope:any, callback:Function){
        var params = {
            'FirstName': this._cub_dataservice.searchParams["firstName"],
            'LastName': this._cub_dataservice.searchParams["lastName"],
            'Email': this._cub_dataservice.searchParams["email"],
            'Phone': this._cub_dataservice.searchParams["phone"]
        };

        var headers = new Headers({'Content-Type': 'application/json'});

        this._http.post('http://usdced-msdyn09.cts.cubic.cub:12663/MasterSearch/SearchContact', JSON.stringify(params), {headers: headers})
            .toPromise()
            .then(response => JSON.parse(response.json()))
            .then(function (data) {
                scope._cub_dataService.masterSearchContactResults = data;
                callback(scope, data);
            })
            .catch(function(error){
                console.log(error);
                callback(scope);
            });
    }
}