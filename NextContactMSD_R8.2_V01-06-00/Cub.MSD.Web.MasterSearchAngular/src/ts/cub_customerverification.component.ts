﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Cub_DataService } from './services/cub_data.service';
import { Cub_CustomerVerificationService } from './cub_customerverification.service';
import * as _ from 'lodash';

export interface SecurityOption {
    name: string,
    question: string,
    answer: string | string[],
    verified: boolean
}

interface Address {
    addressId: string,
    city: string,
    state: string,
    street1: string,
    street2?: string,
    street3?: string,
    zipCode: string
}

@Component({
    selector: 'cub-customerverification',
    templateUrl: './html/cub_customerverification.component.html',
    providers: [Cub_CustomerVerificationService]
})
export class Cub_CustomerVerificationComponent implements OnInit {
    questionsVerified: number;
    detailsVerified: number;
    alerts: string[];
    securityQuestions: SecurityOption[];
    contactDetails: SecurityOption[];

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_customerVerificationService: Cub_CustomerVerificationService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) { 
        this.questionsVerified = 0;
        this.detailsVerified = 0;
    }

    ngOnInit() {
        this._cub_customerVerificationService.getSecurityQuestions(this, this.createSecurityQuestions);
        this._cub_customerVerificationService.getAddress(this, this.addAddressDetail);
        this._cub_customerVerificationService.getBirthDate(this, this.addBirthDateDetail);
        this._cub_customerVerificationService.getPIN(this, this.addPIN);
        this.alerts = this._cub_dataService.customerVerification.alerts;
        this.createContactSecurityDetails();
        this.updateQuestionsVerified();
        this.updateDetailsVerified();
    }

    questionChanged(data: any) {
        if (!data || !data.length || data.length !== 2) { return; }
        let newValue = data[0];
        var securityQuestion = data[1];
        securityQuestion.verified = newValue;
        this.updateQuestionsVerified();
    }

    detailChanged(data: any) {
        if (!data || !data.length || data.length !== 2) { return; }
        let newValue = data[0];
        var securityQuestion = data[1];
        securityQuestion.verified = newValue;
        this.updateDetailsVerified();
    }

    updateQuestionsVerified() {
        this.questionsVerified = _.sumBy(this.securityQuestions, o => o.verified ? 1 : 0);
        if (this.questionsVerified >= 1) {
            this.router.navigateByUrl('/CustomerVerified');
        }
    }

    updateDetailsVerified() {
        this.detailsVerified = _.sumBy(this.contactDetails, o => o.verified ? 1 : 0);
        if (this.detailsVerified >= 3) {
            this.router.navigateByUrl('/CustomerVerified');
        }
    }

    createSecurityQuestions(scope: any, data: any) {
        if (!!data) {
            scope.securityQuestions = _.map(data, (qa, i) => ({
                name: `security-question-${i}-verified`,
                question: qa['securityQuestion'],
                answer: qa['securityAnswer'],
                verifed: false
            }));
        }
    }

    createContactSecurityDetails() {
        this.contactDetails = [
            {
                name: 'name-verified',
                question: 'Name',
                answer: this.getFullName() || null,
                verified: false
            },
            {
                name: 'address-verified',
                question: 'Address',
                answer: null, // value populated asynchronously
                verified: false
            },
            {
                name: 'email-verified',
                question: 'Email',
                answer: this._cub_dataService.selectedContactCustomerToVerify['Email'] || null,
                verified: false
            },
            {
                name: 'date-of-birth-verified',
                question: 'Date of Birth',
                answer: null, // value populated asynchronously
                verified: false
            },
            {
                name: 'pin-verified',
                question: 'PIN',
                answer: null, // value populated asynchronously
                verified: false
            }
        ];
    }

    addAddressDetail(scope: Cub_CustomerVerificationComponent, data: Address) {
        if (!!data) {
            let addressDetail = _.find(scope.contactDetails, d => d.name === 'address-verified');
            if (!addressDetail) {
                console.error('Could not find address detail');
            } else {
                addressDetail.answer = scope.formatAddress(data);
            }
        }
    }

    addBirthDateDetail(scope: Cub_CustomerVerificationComponent, data: any) {
        if (!!data) {
            let birthDateDetail = _.find(scope.contactDetails, d => d.name === 'date-of-birth-verified');
            if (!birthDateDetail) {
                console.error('Could not find date of birth detail');
            } else {
                birthDateDetail.answer = data;
            }
        }
    }

    addPIN(scope: Cub_CustomerVerificationComponent, data: string) {
        if (!!data) {
            let pinDetail = _.find(scope.contactDetails, d => d.name === 'pin-verified');
            if (!pinDetail) {
                console.error('Could not find PIN detail');
            } else {
                pinDetail.answer = data;
            }
        }
    }

    getFullName(): string {
        let firstAndLast = this._cub_dataService.selectedContactCustomerToVerify['FirstName']
            && this._cub_dataService.selectedContactCustomerToVerify['LastName'];
        return [
            this._cub_dataService.selectedContactCustomerToVerify['FirstName'] || '',
            firstAndLast ? ' ' : '',
            this._cub_dataService.selectedContactCustomerToVerify['LastName'] || ''
        ].join('');
    }

    formatAddress(a: Address): string[] {
        let result = [];
        result.push(a.street1);
        if (!!a.street2) {
            result.push(a.street2);
        }
        if (!!a.street3) {
            result.push(a.street3);
        }
        result.push(`${a.city}, ${a.state} ${a.zipCode}`);
        return result;
    }
}