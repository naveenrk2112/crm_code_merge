import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from './services/cub_data.service';
import { DataTableModule, SharedModule } from 'primeng/primeng';

@Component({
    selector: 'app-landing',
    template: `<div class="header center">
                    <h1>[Customer Duplicate Page]</h1>
                    <a routerLink="/" routerLinkActive="active">Back to Landing Page</a>
               </div>
                <br>
                <table style="
                        border-collapse: separate;
                        border: 1px solid black;
                        font-size: 18px;
                    ">
                  <tr>
                    <th>Contact Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone 1</th>
                    <th>Phone 2</th>
                    <th>Phone 3</th>
                  </tr>
                  <tr *ngFor="let data of _cub_dataService.duplicateSearchResults">
                    <td>{{data.ContactId}}</td>
                    <td>{{data.FirstName}}</td>
                    <td>{{data.LastName}}</td>
                    <td>{{data.Email}}</td>
                    <td>{{data.Phone1}}</td>
                    <td>{{data.Phone2}}</td>
                    <td>{{data.Phone3}}</td>
                  </tr>
                </table>

	           <ul class="buttons">
                    <li cub-button [control]="createCustomerButton" (onClick)="createButtonClicked($event)"></li>
                    <li cub-button [control]="backCustomerButton" (onClick)="backButtonClicked($event)"></li>
               </ul>`
})

export class Cub_CustomerDuplicateComponent {
    constructor(
        private _cub_dataService: Cub_DataService,
        private route: ActivatedRoute,
        public router: Router
    ) { }

    ngOnInit() {
    }

    createCustomerButton: Object = {
        id: "CreateCustomer",
        label: "Create Customer",
        class: "button-primary",
    }

    backCustomerButton: Object = {
        id: "Back",
        label: "Back",
        class: "button-cancel",
    }

    createButtonClicked(event: any) {
        this.router.navigate(['CustomerRegistration']);;
    }

    backButtonClicked(event: any) {
        this.router.navigate(['CustomerSearch']);
    }
}