﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SecurityOption } from './cub_customerverification.component';

// TODO: add class long-line-wrap to answer <li> for email (& others?)

@Component({
    selector: 'cub-securityoption',
    template: `
        <li *ngIf="securityOption.answer">
            <input type="checkbox" id="{{securityOption.name}}"
                [ngModel]="securityOption.verified"
                (ngModelChange)="inputChange($event)">
            <label for="{{securityOption.name}}">
                <ul>
    		        <li class="checkbox">
    			        <span>
    				        <div class="icon-checkmark">
    						    <svg viewBox="0 0 20 16">
    							    <polyline points="2.25,7.5 7.5,12.75 17.75,2.5 "/>
    				    	    </svg>
    			    	    </div>
    		    	    </span>
    	    	    </li>
    		        <li>
    			        <p>{{securityOption.question}}</p>
   			        </li>
   			        <li class="answer">
                        <p *ngIf="!answerIsArray()">{{securityOption.answer}}</p>
    				    <p *ngIf="answerIsArray()">
                            <cub-securityoptionanswer *ngFor="let value of securityOption.answer; let i=index"
                                [value]="value"
                                [isLast]="i === securityOption.answer.length - 1">
                            </cub-securityoptionanswer>
                        </p>
    			    </li>
    		    </ul>
            </label>
        </li>`
})
export class Cub_SecurityOptionComponent {
    @Input() securityOption: SecurityOption;
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    inputChange(newValue: boolean) {
        this.onChange.emit([newValue, this.securityOption]);
    }

    // Returns true if array, false if string
    answerIsArray(): boolean {
        return this.securityOption.answer instanceof Array;
    }
}