﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule} from '@angular/forms';
import { Cub_CustomerSearchComponent } from './cub_customersearch.component';
import { Cub_CustomerSearchService } from './services/cub_customersearch.service';
import { Cub_MasterSearchContactComponent } from './cub_mastersearch.contact.component';
import { Cub_ControlsColumnComponent } from './controls/cub_controlscolumn.component';
import { Cub_ButtonComponent } from './controls/cub_button.component';
import { Cub_TextboxComponent } from './controls/cub_textbox.component';
import { Cub_DropdownComponent } from './controls/cub_dropdown.component';
import { Cub_TextboxVerticalStackedLabelComponent } from './controls/cub_textbox_vertical_stacked_label.component';
import { Cub_ControlLabelComponent } from './controls/cub_controllabel.component';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppRouting } from './app-routing.module';
import { AppLandingComponent } from './app-landing.component';
import { Cub_CustomerRegistrationComponent } from './cub_customerregistration.component';
import { Cub_CustomerDuplicateComponent } from './cub_customerduplicate.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { Cub_DataService } from './services/cub_data.service';
import { Cub_CustomerRegistrationService } from './services/cub_customerregistration.service';
import { Cub_MasterSearchService } from './services/cub_mastersearch.service';
import { Cub_OptionSelectComponent } from './controls/cub_optionselect.component';
import { Cub_CustomerVerificationComponent } from './cub_customerverification.component';
import { Cub_SecurityOptionComponent } from './cub_securityoption.component';
import { Cub_SecurityOptionAnswerComponent } from './cub_securityoptionanswer.component';
import { Cub_CustomerVerifiedComponent } from './cub_customerverified.component';
import { Cub_AlertComponent } from './controls/cub_alert.component';
import { DataTableModule, SharedModule } from 'primeng/primeng';

const appRoutes: Routes = [
  { path: 'CustomerSearch', component: Cub_CustomerSearchComponent },
  { path: 'CustomerVerification', component: Cub_CustomerVerificationComponent },
  { path: 'CustomerVerified', component: Cub_CustomerVerifiedComponent },
  { path: 'CustomerRegistration', component: Cub_CustomerRegistrationComponent },
  { path: 'CustomerDuplicate', component: Cub_CustomerDuplicateComponent },
  { path: 'MasterSearchContact', component: Cub_MasterSearchContactComponent },
  { path: '', component: AppLandingComponent}
  //{path: '**', component: PageNotFoundComponent} //TODO: Add a 404 component
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes),
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        DataTableModule
    ],
  declarations: [
    AppRouting,
    AppLandingComponent,
    Cub_CustomerSearchComponent,
    Cub_MasterSearchContactComponent,
    Cub_CustomerRegistrationComponent, 
    Cub_CustomerDuplicateComponent,
    Cub_CustomerVerificationComponent,
    Cub_ControlsColumnComponent, 
    Cub_TextboxComponent,
    Cub_TextboxVerticalStackedLabelComponent,
    Cub_ControlLabelComponent, 
    Cub_ButtonComponent,
    Cub_DropdownComponent,
    Cub_OptionSelectComponent,
    Cub_SecurityOptionComponent,
    Cub_SecurityOptionAnswerComponent,
    Cub_CustomerVerifiedComponent,
    Cub_AlertComponent
  ],
  exports: [RouterModule],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }, Cub_DataService],
  bootstrap: [AppRouting ]
})
export class AppModule { }
