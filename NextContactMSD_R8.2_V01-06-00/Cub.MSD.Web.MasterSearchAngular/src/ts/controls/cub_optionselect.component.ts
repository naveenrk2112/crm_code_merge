﻿import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: '[cub-optionselect]',
    template: `<ul [attr.class]="control.formFieldClass">
                    <li [attr.class]="control.fieldWidthClass">
                        <div class="form-field-toggle">
                            <ul>
                            <li *ngIf="control.option1!=null" [attr.class]="control.option1.fieldWidthClass">
                                <input type="radio"
                                    (change)="dropdownChange($event.target.value)"
                                    [attr.id]="control.option1.id" 
                                    [attr.name]="control.id" 
                                    [attr.value]="control.option1.value">
                                <label [attr.for]="control.option1.id">{{control.option1.name}}</label>
                            </li>
                            <li *ngIf="control.option2!=null" [attr.class]="control.option2.fieldWidthClass">
                                <input type="radio"
                                    (change)="dropdownChange($event.target.value)" 
                                    [attr.id]="control.option2.id" 
                                    [attr.name]="control.id" 
                                    [attr.value]="control.option2.value">
                                <label [attr.for]="control.option2.id">{{control.option2.name}}</label>
                            </li>
                            <li *ngIf="control.additionalOptions && control.additionalOptions.length > 0" [attr.class]="control.dropdownFieldWidthClass">
                                <div class="form-field-drop-down-toggle">
                                    <select
                                        (change)="dropdownChange($event.target.value)"
                                        [attr.id]="control.selectId">
                                        <option *ngFor="let option of control.additionalOptions"
                                            [attr.disabled]="option.isDisabled ? '' : null"
                                            [attr.value]="option.value"
                                            [attr.selected]="option.value == 0 ? '' : null">
                                            {{option.name}}
                                        </option>
				                    </select>
                                    <div>
				                        <span class="icon-drop-down-arrow">
					                        <svg viewBox="0 0 16 11">
						                        <polyline points="2,2 8,8 14,2"></polyline>
					                        </svg>
				                        </span>
			                        </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>`
})

export class Cub_OptionSelectComponent {
    private fieldWidthClass: string;

    constructor() { }

    ngAfterViewInit() {
        //this.dropdownChange(this.control['value']);
    }

    @Input() control = {
        formFieldClass: '', //Form field class to use, eg. form-fields-siblings
        labelWidthClass: '', //Width of label field (eg. form-field-width-100) (Only used when isTableControl = false)
        fieldWidthClass: '', //Width of field (eg. form-field-width-100)
        isDisabled: false, //Disable component
        isTableControl: false, //Should we display this as a table control layout
        errorMessage: '', //Error message to display when required or validation not met
        id: '', //uniquie control id
        label: '', //Label for control
        value: '', //Value for control
        dropdownFieldWidthClass: '', //width of the additional dropdown control
        option1: {}, //option 1, object {value: '', id: '', name: '', fieldWidthClass: ''}
        option2: {}, //option 1, object {value: '', id: '', name: '', fieldWidthClass: ''}
        additionalOptions: new Array() //Array of objects, {value: '', name: '', isDisabled: ''}
    };

    @Output() onChange: EventEmitter<any> = new EventEmitter();

    dropdownChange(newValue: any): void {
        //TODO: DOM manipulation, but should be in sync with value (due to some HTML/JS limitations it seems), maybe a better way?
        if (document.getElementById(this.control['option1']['id'])) {
            if (newValue == this.control['option1']['value']) {
                document.getElementById(this.control['option1']['id'])['checked'] = true;
                document.getElementById(this.control['option2']['id'])['checked'] = false;
                if (document.getElementById(this.control['selectId'])) {
                    document.getElementById(this.control['selectId'])['selectedIndex'] = 0;
                }
            }
            else if (newValue == this.control['option2']['value']) {
                document.getElementById(this.control['option1']['id'])['checked'] = false;
                document.getElementById(this.control['option2']['id'])['checked'] = true;
                if (document.getElementById(this.control['selectId'])) {
                    document.getElementById(this.control['selectId'])['selectedIndex'] = 0;
                }
            }
            else {
                document.getElementById(this.control['option1']['id'])['checked'] = false;
                document.getElementById(this.control['option2']['id'])['checked'] = false;
                if (document.getElementById(this.control['selectId'])) {
                    document.getElementById(this.control['selectId'])['value'] = newValue;
                }
            }
        }
        this.control['value'] = newValue;
        this.onChange.emit([newValue, this.control]);
    }

    inputChange(newValue: string): void {
        //this.control.value = newValue
        this.onChange.emit([newValue, this.control]);
    }
}
