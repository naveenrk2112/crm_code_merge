﻿import {Component, Input, Output, EventEmitter} from '@angular/core';

//A column that can contain various controls
@Component({
    selector: '[cub-controlscolumn]',
    template: `<div [attr.class]="generateLabelWidth(labelWidthClass)">
                    <ng-container *ngFor="let key of controlsOrder">
                        <ul [attr.class]="controls[key].type == 'Divider' ? 'divider' : ''">
                            <li *ngIf="controls[key].type=='Divider' || controls[key].type == 'Button'"></li>
                            <li *ngIf="controls[key].type!='Divider' && controls[key].type != 'Button'" [id]="controls[key].id" cub-controllabel 
                                [label]="controls[key].label">
                            </li>    
                            <li *ngIf="controls[key].type == 'Dropdown'" cub-dropdown
                                [attr.class]="controls[key].showErrorMessage 
                                                ? 'error show-error-message' 
                                                : controls[key].isErrored 
                                                    ? 'error' 
                                                    : ''" 
                                (onChange)="inputChange($event)" 
                                [control]="controls[key]">
                            </li>
                            <li *ngIf="controls[key].type == 'Textbox'" cub-textbox
                                [attr.class]="controls[key].showErrorMessage 
                                                ? 'error show-error-message' 
                                                : controls[key].isErrored 
                                                    ? 'error' 
                                                    : ''" 
                                (onChange)="inputChange($event)" 
                                [control]="controls[key]">
                            </li>
                            <li *ngIf="controls[key].type=='OptionSelect'" cub-optionselect
                                [attr.class]="controls[key].showErrorMessage 
                                                ? 'error show-error-message' 
                                                : controls[key].isErrored 
                                                    ? 'error' 
                                                    : ''" 
                                (onChange)="inputChange($event)" 
                                [control] ="controls[key]">
                            </li>
                            <li *ngIf="controls[key].type=='Button' && !controls[key].isHidden">
                                <ul class="form-field-siblings">
                                    <li cub-button [attr.class]="form-field-width-100"
                                        [control]="controls[key]" 
                                        (onClick)="buttonClicked($event)">
                                    </li>
                                </ul>
                            </li>
			                <button *ngIf="controls[key].isRemovable == true"
                                [attr.id]="controls[key].id"
                                (click)="removedClicked($event)" 
                                type="button" class="row-x">
					            <div>
						            <span class="icon-row-x">
							            <svg viewBox="0 0 16 16">
								            <line x1="2.25" y1="2.25" x2="13.75" y2="13.75"/>
								            <line x1="13.75" y1="2.25" x2="2.25" y2="13.75"/>
							            </svg>
						            </span>
					            </div>
			                </button>
                        </ul>
                    </ng-container>
                </div>`
})

export class Cub_ControlsColumnComponent {
    @Input() controls = {};
    @Input() controlsOrder: Array<string>;
    @Input() labelWidthClass = ""; //Width of label field (eg. label-width-25) (Only used when isTableControl = false)

    ngOnInit(){
    }

    @Output() onChange: EventEmitter<any> = new EventEmitter();
    @Output() onClick: EventEmitter<any> = new EventEmitter();
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    removedClicked(event: any) {
        this.onRemove.emit([event.currentTarget.id]);
    }

    inputChange(data: any) {
        this.onChange.emit([data[0], data[1]]);
    }

    buttonClicked(data: any) {
        this.onClick.emit([data[0], data[1]]);
    }

    generateLabelWidth(width: string) {
        return 'form-fields-vertical ' + width;
    }
}

//<li *ngIf="controls[key].type == 'Textbox'" cub- textboxlabel[textControl]="controls[key]" > </li>
//   < li * ngIf="controls[key].type == 'Textbox'" cub- textbox(onChange)="textChanged($event)"[textControl] = "controls[key]" > </li>