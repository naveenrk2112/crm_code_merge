﻿import { Component, Input, Output, EventEmitter } from '@angular/core';

//Can be inserted into a control column
@Component({
    selector: '[cub-textbox]',
    template: `<ul [attr.class]="control.formFieldClass">
                    <li [attr.class]="control.fieldWidthClass">
                        <div class="form-field-text">
                            <input type="text" [attr.id]="control.id"
                                [ngModel]="control.value" 
                                (ngModelChange)="inputChange($event)" 
                                [attr.required]="control.isRequired ? '' : null"
                                [attr.disabled]="control.isDisabled ? '' : null"
                                [attr.placeholder]="generatePlaceholder(control.placeholder)" />
				            <i></i>
				            <span class="error-message">
					            <p>{{control.errorMessage}}</p>
				            </span>
                        </div>
                    </li>
                    <li *ngFor="let key of arrayOfAdditionalControlKeys" [attr.class]="control.additionalControls[key].formFieldClass">
                        <div [attr.class]="generateLabelWidth(control.additionalControls[key].labelWidthClass)">
                            <ul>
                                <li cub-controllabel *ngIf="control.additionalControls[key].label != ''" [id]="control.additionalControls[key].id" [label]="control.additionalControls[key].label"></li>
                                <li *ngIf="control.additionalControls[key].type == 'Textbox'" cub-textbox
                                    [attr.class]="control.additionalControls[key].showErrorMessage 
                                                    ? 'error show-error-message' 
                                                    : control.additionalControls[key].isErrored 
                                                        ? 'error' 
                                                        : ''" 
                                    (onChange)="additionalInputChange($event)" 
                                    [control]="control.additionalControls[key]">
                                </li>
                                <li *ngIf="control.additionalControls[key].type=='OptionSelect'" cub-optionselect
                                    [attr.class]="control.additionalControls[key].showErrorMessage 
                                                    ? 'error show-error-message' 
                                                    : control.additionalControls[key].isErrored 
                                                        ? 'error' 
                                                        : ''" 
                                    (onChange)="additionalInputChange($event)" 
                                    [control] ="control.additionalControls[key]">
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li *ngIf="!!control.spacerWidthClass" [attr.class]="spacerWidthClass">
                    </li>
                </ul>`
})

export class Cub_TextboxComponent {
    private arrayOfAdditionalControlKeys = new Array<string>();

    constructor() { }

    ngOnInit() {
        this.arrayOfAdditionalControlKeys = this.control.additionalControls ? Object.keys(this.control.additionalControls) : [];
    }

    @Input() control = {
        additionalControls: {},
        placeholder: "",

        formFieldClass: '', //Form field class to use, eg. form-fields-siblings
        labelWidthClass: '', //Width of label field (eg. form-field-width-100) (Only used when isTableControl = false)
        fieldWidthClass: '', //Width of field (eg. form-field-width-100)
        spacerWidthClass: '', //Width of space to right of field (eg. form-field-width-100)
        isDisabled: false, //Disable component
        isRequired: false, //Require a value
        isTableControl: false, //Should we display this as a table control layout
        errorMessage: '', //Error message to display when required or validation not met
        id: '', //uniquie control id
        label: '', //Label for control
        value: '', //Value for control
        options: Array() //Array of objects: {name: "Label Text", value: "Value"}
    };

    @Output() onChange: EventEmitter<any> = new EventEmitter();

    inputChange(newValue: string): void {
        this.control.value = newValue;
        this.onChange.emit([newValue, this.control]);
    }

    additionalInputChange(data: any) {
        this.control.additionalControls[data[1].id].value = data[0];
        this.onChange.emit([data[0], data[1]]);
    }

    generatePlaceholder(placeholder: string) {
        return placeholder || "";
    }

    generateLabelWidth(width: string, fieldClass: string) {
        return (fieldClass ? fieldClass : 'form-fields-vertical ') + width;
    }
}
