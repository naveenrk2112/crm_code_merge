﻿import {Component, Input} from '@angular/core';

//Used by textbox
@Component({
    selector: '[cub-controllabel]',
    template: `	
	            <label [attr.for]="id">{{label}}</label>
              `
})

export class Cub_ControlLabelComponent {
    @Input() id: string = "";
    @Input() label: string = "";
}