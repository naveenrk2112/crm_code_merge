﻿import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: '[cub-button]',
    template: `	
			<button 
                type="button" 
                [attr.disabled]="control.isDisabled ? '' : null"
                (click)="click($event)"
                class="{{control.class}}">
				<p>{{control.label}}</p>
			</button>
               `
})

export class Cub_ButtonComponent {
    @Input() control: Object = {
        id: "", //uniquie control id
        class: "button-cancel", //Button's class
        isDisabled: false, //Disable component
        label: "Button" //Label for control
    };

    @Output() onClick: EventEmitter<any> = new EventEmitter();

    click(entry: Cub_ButtonComponent): void {
        if(this.onClick) {
            this.onClick.emit([entry, this.control]);
        }
    }

    ngOnInit() {
    }
}