﻿import {Component, Input, Output, EventEmitter} from '@angular/core';

//Can be inserted into a control column
@Component({
    selector: '[cub-textbox-vertical-stacked-label]',
    template: `	
                
                    <li [attr.class]="textControlVerticalStackedLabel.class">
                        <label [attr.for]="textControlVerticalStackedLabel.id">{{textControlVerticalStackedLabel.label}}</label>
                        <div class="form-field-text">
                            <input type="text" id="textControlVerticalStackedLabel.id" [ngModel]="textControlVerticalStackedLabel.value" 
                                (ngModelChange)="valueChange($event)" 
                                [attr.required]="textControlVerticalStackedLabel.isRequired ? '' : null" />
							<i></i>
							<span class="error-message">
								<p>{{textControlVerticalStackedLabel.errorMessage}}</p>
							</span>
                        </div>
                    </li>
                `
})

export class Cub_TextboxVerticalStackedLabelComponent {
    @Input() textControlVerticalStackedLabel = {
        isRequired: false,
        label: "",
        value: "",
        errorMessage: "",
        id: "",
        class: "",
        onChange: Function
    };

    @Output() onChange: EventEmitter<any> = new EventEmitter();

    valueChange(newValue: string): void {
        this.textControlVerticalStackedLabel.value = newValue;
        if(this.onChange){
            this.onChange.emit([newValue, this.textControlVerticalStackedLabel]);
        }
    }
}