﻿import {Component, Input} from '@angular/core';

@Component({
    selector: '[cub-textboxlabel]',
    template: `	
	            <li [attr.class]="generateFieldWidth(fieldWidth)"></li>
              `
})

export class Cub_ControlSpacerComponent {
    @Input() fieldWidth: string = "25";

    generateFieldWidth(width: string) {
        return 'form-field-width-' + width;
    }
}