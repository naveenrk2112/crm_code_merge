﻿import { Component, Input, Output, EventEmitter } from '@angular/core';

//Can be inserted into a control column
@Component({
    selector: '[cub-dropdown]',
    template: `<ul [attr.class]="control.formFieldClass" *ngIf="control.isTableControl">
                    <li [attr.class]="control.fieldWidthClass">
                        <div class="form-field-drop-down">
                            <select [attr.id]="control.id" 
                                [ngModel]="control.value" 
                                (change)="dropdownChange($event)">
                                [attr.required]="control.isRequired ? '' : null"
                                [attr.disabled]="control.isDisabled ? '' : null">
                                <option *ngFor="let option of control.options"
                                    [attr.value]="option.value" 
                                    [attr.checked]="option.value == control.value ? '' : null">
                                    {{option.name}}
                                </option>
				            </select>
                            <div>
				                <span class="icon-drop-down-arrow">
					                <svg viewBox="0 0 16 11">
						                <polyline points="2,2 8,8 14,2"></polyline>
					                </svg>
				                </span>
			                </div>
                        </div>
                    </li>
                    <li *ngIf="!!control.spacerWidthClass" [attr.class]="control.spacerWidthClass">
                    </li>
                </ul>
               <ul *ngIf="!control.isTableControl"  [attr.class]="control.formFieldClass">
                   <li [id]="control.id" cub-controllabel 
                        [attr.class]="control.labelWidthClass" 
                        [label]="control.label">
                   </li>
                   <li [attr.class]="control.fieldWidthClass">
                        <div class="form-field-drop-down drop-down-small">
                            <select [attr.id]="control.id" 
                                [(ngModel)]="control.value" 
                                (change)="dropdownChange($event)">
                                [attr.required]="control.isRequired ? '' : null"
                                [attr.disabled]="control.isDisabled ? '' : null">
                                <option *ngFor="let option of control.options"
                                    [attr.value]="option.value" 
                                    [attr.checked]="option.value == control.value ? '' : null">
                                    {{option.name}}
                                </option>
				            </select>
                            <div>
				                <span class="icon-drop-down-arrow">
					                <svg viewBox="0 0 16 11">
						                <polyline points="2,2 8,8 14,2"></polyline>
					                </svg>
				                </span>
			                </div>
                        </div>                           
                    </li>
                 </ul>`
})

export class Cub_DropdownComponent {
    private fieldWidthClass: string;

    constructor() { }

    ngOnInit() {
    }

    @Input() control = {
        formFieldClass: '', //Form field class to use, eg. form-fields-siblings
        labelWidthClass: '', //Width of label field (eg. form-field-width-100) (Only used when isTableControl = false)
        fieldWidthClass: '', //Width of field (eg. form-field-width-100)
        spacerWidthClass: '', //Width of space to right of field (eg. form-field-width-100)
        isDisabled: false, //Disable component
        isRequired: false, //Require a value
        isTableControl: false, //Should we display this as a table control layout
        errorMessage: '', //Error message to display when required or validation not met
        id: '', //uniquie control id
        label:'', //Label for control
        value: '', //Value for control
        options: Array() //Array of objects: {name: "Label Text", value: "Value"}
    };

    @Output() onChange: EventEmitter<any> = new EventEmitter();

    dropdownChange(newValue: any): void {
        this.control.value = newValue.target.value;
        this.onChange.emit([newValue.target.value, this.control]);
    }

    inputChange(newValue: string): void {
        this.control.value = newValue;
        this.onChange.emit([newValue, this.control]);
    }
}
