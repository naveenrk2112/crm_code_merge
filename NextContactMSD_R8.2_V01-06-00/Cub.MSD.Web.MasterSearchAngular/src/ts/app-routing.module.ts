import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from './services/cub_data.service';

//TODO: We can put a page header here, and this is where everything should originate
@Component({
    selector: 'app',
    template: `<div class="wrapper no-scroll">
                    <div class="customer-bar">
		                <div>
			                <ul>
				                <li></li>
			                </ul>
		                </div>
	               </div>
                   <router-outlet></router-outlet>
               </div>
               <div *ngIf="_cub_dataService.busyScreen.busyCount > 0" class="modal">
                    <ul class="window checking-for-duplicates">

	                    <li class="icon">
		                    <div *ngIf="_cub_dataService.busyScreen.icon == 'loading'" class="loading hidden-">
			                    <span class="spinner">
				                    <i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i>
			                    </span>
		                    </div>
                            <div *ngIf="_cub_dataService.busyScreen.icon == 'check'" class="icon-confirmation-check">
				                <svg viewBox="0 0 61 61">
					                <circle cx="30.5" cy="30.5" r="29.5"></circle>
					                <polyline points="16,30.5 25.25,39.75 45,20 "></polyline>
				                </svg>
			                </div>
                            <div *ngIf="_cub_dataService.busyScreen.icon == 'caution'" class="icon-confirmation-check">
                                <svg viewBox="0 0 25 25">
							        <circle cx="12.5" cy="12.5" r="11.5"></circle>
							        <path d="M11,5.997C11,5.446,11.443,5,11.999,5h1.002 C13.553,5,14,5.446,14,5.997v7.006C14,13.554,13.557,14,13.001,14h-1.002C11.447,14,11,13.554,11,13.003V5.997z"></path>
							        <path d="M11.003,16.495c0-0.273,0.216-0.495,0.496-0.495h2.009 c0.274,0,0.496,0.216,0.496,0.495v2.01c0,0.273-0.216,0.495-0.496,0.495h-2.009c-0.274,0-0.496-0.216-0.496-0.495V16.495z"></path>
						        </svg>
			                </div>
	                    </li>
	                    <li class="header center">
		                    <h1>{{_cub_dataService.busyScreen.busyText}}</h1>
	                    </li>
	                    <li class="footer">
		                    <ul *ngIf="_cub_dataService.busyScreen.buttonText != ''" class="buttons">
			                    <li>
				                    <button type="button" class="button-cancel"
                                        (click)="buttonClicked($event)">
					                    <p>{{_cub_dataService.busyScreen.buttonText}}</p>
				                    </button>
			                    </li>
		                    </ul>
	                    </li>
                    </ul>
               </div>`
})

export class AppRouting {
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _cub_dataService: Cub_DataService
    )
    { };

    ngOnInit() {
    }

    buttonClicked(entry: any): void {
        this._cub_dataService.busyScreen.busyCount--;
        this.router.navigate([this._cub_dataService.busyScreen.buttonRoute]);
    }
}