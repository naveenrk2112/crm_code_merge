﻿import { Component, Input } from '@angular/core';

@Component({
    selector: 'cub-securityoptionanswer',
    template: `{{value}}<br *ngIf="!isLast"/>`
})
export class Cub_SecurityOptionAnswerComponent {
    @Input() value: string;
    @Input() isLast: boolean;
}