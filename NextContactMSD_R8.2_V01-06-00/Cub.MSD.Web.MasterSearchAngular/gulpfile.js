﻿'use strict';
var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    ts = require('gulp-typescript'),
    merge = require('merge2'),
    //sass = require('gulp-sass'),
    clean = require('gulp-clean');
var destPath = './build/libs/';

// Delete the dist directory
gulp.task('clean', function () {
   console.log('Starting Clean Command')
  return gulp.src(destPath)
      .pipe(clean());
});

gulp.task("scriptsAndStyles", function () {
  gulp.src([
          'core-js/client/*.js',
          'systemjs/dist/*.js',
          'reflect-metadata/*.js',
          'rxjs/**',
          'zone.js/dist/*.js',
          '@angular/**/bundles/*.js',
          'bootstrap/dist/js/*.js',
          'angular-in-memory-web-api/bundles/*.js',
          'mdn-polyfills/*.js',
          'primeng/*.js',
          'primeng/components/**/*.js',
          'lodash/**'
  ], {
    cwd: "node_modules/**"
  })
      .pipe(gulp.dest(destPath));
});

var tsProject = ts.createProject('src/tsconfig.json', {
  typescript: require('typescript')
});

/*
* Copies all indexhtml file into the build folder.
*/
gulp.task('copy_indexhtml', function () {
    console.log('Copying index files from the source directory to build folder.');
    gulp.src('src/index.html')
        .pipe(gulp.dest('build/'));
});

/*
* Compiles required starting files into the build folder.
*/
gulp.task('copy_systemjs', function () {
    console.log('Copying systemjs files from the source directory to build folder.');
    gulp.src('src/*.js')
            .pipe(gulp.dest('build/'));
});

/*
* Compiles all typescript files into the build/app folder.
* The 'watch_source' task will use this task to do actual compilation of typescript files.
*/
gulp.task('compile_ts', function () {
    console.log('Compiling TS files...');
    var tsResult = gulp.src('src/ts/**/*.ts').pipe(tsProject());
    return merge([tsResult.js.pipe(gulp.dest('build/app'))]);
});

/*
* Copies all map files into the build/app/ folder.
*/
gulp.task('copy_map', function () {
    console.log('Copying map files from the source directory to build/app folder.');
    gulp.src('src/ts/**/*.map')
        .pipe(gulp.dest('build/app'));
});

/*
* Copies all html files into the build/app/html build folder.
*/
gulp.task('copy_html', function () {
    console.log('Copying html files from the source directory to build/app/html folder.');
    gulp.src('src/html/**/*.html')
        .pipe(gulp.dest('build/app/html'));
});

/*
* Compiles all css files into the static/resources package.
*/
gulp.task('copy_css', function () {
    console.log('Copying css files from the source directory to build/app/css.');
    gulp.src('src/css/**/*.css')
        .pipe(gulp.dest('build/app/css'));
});

/*
* Compiles all image files into the static/resources package.
*/
gulp.task('copy_image', function () {
    console.log('Copying image files from the source directory to build/app/css.');
    gulp.src('src/images/**/*.jpg')
        .pipe(gulp.dest('build/app/images'));
});

///*
//* Compiles all sass files into the static/resources package.
//* The 'watch_source' task will use this task to do actual compilation of typescript files.
//*/
//gulp.task('process_sass', function () {
//    console.log('Processing scss files and deploying css to static/resources.');
//    return gulp.src('src/scss/**/*.scss')
//        .pipe(sass().on('error', sass.logError))
//        .pipe(gulp.dest('build'));
//});

var reloadTimeout;
gulp.task('watch_build', function () {
    gulp.watch('build/**/*.*').on('change', function () {
        if (reloadTimeout) {
            clearTimeout(reloadTimeout);
        }
        reloadTimeout = setTimeout(browserSync.reload, 2000);
    });
    console.log('Watching build files for changes to reload browser.');
});

/*
* This gulp task will watch the ts files and call the ts-compile_ts task if there are any change in the files.
* This will also watch the html files for changes to copy them to the static/resources directory.
* This will also watch the scss files for changes to process them and output css to the static/resources directory.
* As soon as compilation done 'watch' task will be called because there is a change in the newly generated js files.
*/
//', process_sass'
gulp.task('watch_source',
    ['copy_indexhtml', 'copy_systemjs', 'compile_ts',
        'copy_map', 'copy_html', 'copy_css', 'copy_image'], function () {
    gulp.watch('src/ts/**/*.ts', ['compile_ts']);
    gulp.watch('src/html/**/*.html', ['copy_html']);
    gulp.watch('src/ts/**/*.map', ['copy_map']);
    gulp.watch('src/index.html', ['copy_indexhtml']);
    gulp.watch('src/*.js', ['copy_systemjs']);
    gulp.watch('src/css/**/*.css', ['copy_css']);
    
    console.log('Watching ts, html and scss files for changes to rebuild/deploy to static resources.');
});

//gulp.watch('src/scss/**/*.scss', ['process_sass']);

// Static server
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "build/",
            index: "index.html"
        }
    });
});

/*
* Tasks to run.
* Both 'default' and 'start' will run the 'serve' task.
* Serve will start the app up and start the 'watch_static_resources' and 'watch_source' tasks.
* 'watch_source' will watch for changes to typescript, html and scss files to rebuild if necessary.
* 'watch_static_resources' will watch for changes to the static/resources package to reload the browser.
*/
gulp.task('default',
    ['scriptsAndStyles', 'copy_indexhtml', 'copy_systemjs',
        'copy_html', 'copy_map', 'copy_css', 'copy_image',
        'compile_ts', 'watch_source', 'watch_build', 'browser-sync']); //'process_sass',
gulp.task('start', ['browser-sync']);
