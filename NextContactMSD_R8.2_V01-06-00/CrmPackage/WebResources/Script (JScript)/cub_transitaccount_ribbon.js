﻿(function (global) {
    'use strict';
    global.Cubic = global.Cubic || {};
    global.Cubic.cub_transitaccount = global.Cubic.cub_transitaccount || {};
    global.Cubic.cub_transitaccount.ribbon = (function () {

        // Only displayed/enabled on UPDATE form
        function createCase() {
            var customerControl = Xrm.Page.getAttribute('cub_customer'),
                params = {},
                customerRef;

            params['cub_transitaccount'] = Xrm.Page.data.entity.getId();
            params['cub_transitaccountname'] = Xrm.Page.data.entity.getPrimaryAttributeValue();
            if (!!customerControl
                && !!customerControl.getValue()
                && !!customerControl.getValue()[0]) {
                customerRef = customerControl.getValue()[0];
                params['customerid'] = customerRef.id;
                params['customeridname'] = customerRef.name;
                params['customeridtype'] = customerRef.entityType;
            }

            Xrm.Utility.openEntityForm('incident', null, params);
        }

        // Only displayed/enabled on UPDATE form
        function linkToCustomer() {
            var transitAccountId = Xrm.Page.data.entity.getPrimaryAttributeValue();
            var subsystemControl = Xrm.Page.getAttribute('cub_subsystemid');
            var url = Xrm.Page.context.getClientUrl();
            var data = encodeURIComponent(JSON.stringify({
                route: 'MasterSearchContact',
                queryParams: [
                    'subsystemAccountReference=', transitAccountId,
                    '&subsystemId=', subsystemControl.getValue()
                ].join('')
            }));

            var webResourceCacheKey = Cubic.utility.getWebResourceUriCacheKey();
            if (webResourceCacheKey) {
                url += '/' + webResourceCacheKey;
            }

            url += '/WebResources/cub_/angular/index.html?pagemode=iframe&Data=' + data;
            window.location.href = url;
        }

        function blockCustomer() {
            
            var dialogOptions = new Xrm.DialogOptions(),
                customerControl = Xrm.Page.getAttribute('cub_customer'),  // customerId
                subsystemControl = Xrm.Page.getAttribute('cub_subsystemid'), // ABP
                subsystemAccountReference = Xrm.Page.data.entity.getPrimaryAttributeValue(), // subsystemAccountId
                modalUrl = Xrm.Page.context.getClientUrl();

            var data = encodeURIComponent(JSON.stringify({
                route: 'SubsystemAccountsBlockUnblock',
                queryParams: [
                    'subsystemId=', subsystemControl ? subsystemControl.getValue() : '',
                    '&subsystemAccount=', subsystemAccountReference,
                    '&block=true'
                ].join('')
            }));

            var webResourceCacheKey = Cubic.utility.getWebResourceUriCacheKey();
            if (webResourceCacheKey) {
                modalUrl += '/' + webResourceCacheKey;
            }

            modalUrl += '/WebResources/cub_/angular/index.html?Data=' + data;

            dialogOptions.width = 425;
            dialogOptions.height = 395;
            Xrm.Internal.openDialog(modalUrl, dialogOptions, null, null, onModalClose);
        }

        function unBlockCustomer() {

            var dialogOptions = new Xrm.DialogOptions(),
                customerControl = Xrm.Page.getAttribute('cub_customer'),  // customerId
                subsystemControl = Xrm.Page.getAttribute('cub_subsystemid'), // ABP
                subsystemAccountReference = Xrm.Page.data.entity.getPrimaryAttributeValue(), // subsystemAccountId
                modalUrl = Xrm.Page.context.getClientUrl();

            var data = encodeURIComponent(JSON.stringify({
                route: 'SubsystemAccountsBlockUnblock',
                queryParams: [
                    'subsystemId=', subsystemControl ? subsystemControl.getValue() : '',
                    '&subsystemAccount=', subsystemAccountReference,
                    '&block=false'
                ].join('')
            }));

            var webResourceCacheKey = Cubic.utility.getWebResourceUriCacheKey();
            if (webResourceCacheKey) {
                modalUrl += '/' + webResourceCacheKey;
            }

            modalUrl += '/WebResources/cub_/angular/index.html?Data=' + data;

            dialogOptions.width = 425;
            dialogOptions.height = 395;
            Xrm.Internal.openDialog(modalUrl, dialogOptions, null, null, onModalClose);
        }

        // Only displayed/enabled on UPDATE form
        function delinkFromCustomer() {
            var dialogOptions = new Xrm.DialogOptions(),
                customerControl = Xrm.Page.getAttribute('cub_customer'),  // customerId
                subsystemControl = Xrm.Page.getAttribute('cub_subsystemid'), // ABP
                subsystemAccountReference = Xrm.Page.data.entity.getPrimaryAttributeValue(), // subsystemAccountId
                modalUrl = Xrm.Page.context.getClientUrl();

            var data = encodeURIComponent(JSON.stringify({
                route: 'SubsystemAccountsDelink',
                queryParams: [
                    'subsystemId=', subsystemControl ? subsystemControl.getValue() : '',
                    '&subsystemAccount=', subsystemAccountReference,
                    '&customerId=', customerControl && customerControl.getValue() ? customerControl.getValue()[0].id : ''
                ].join('')
            }));

            var webResourceCacheKey = Cubic.utility.getWebResourceUriCacheKey();
            if (webResourceCacheKey) {
                modalUrl += '/' + webResourceCacheKey;
            }

            modalUrl += '/WebResources/cub_/angular/index.html?Data=' + data;

            dialogOptions.width = 400;
            dialogOptions.height = 296;
            Xrm.Internal.openDialog(modalUrl, dialogOptions, null, null, onModalClose);
        }

        function isNotLinked() {
            var customerControl = Xrm.Page.getAttribute('cub_customer');
            return !customerControl || !customerControl.getValue() || !customerControl.getValue()[0];
        }

        function isLinked() {
            var customerControl = Xrm.Page.getAttribute('cub_customer');
            if (!customerControl) {
                return false;
            }
            else if (!customerControl.getValue()) {
                return false;
            }
            else if (!customerControl.getValue()[0].id) {
                return false;
            }
            return true;
        }

        // debtCollectionInfo is made available by the Angular component Cub_SubsystemComponent,
        // which also refreshes the ribbon once it is set
        function showResolveBalance() {
            var info = global.Cubic.cub_transitaccount.debtCollectionInfo;
            return info && (info.action === 'Available' || info.action === 'AvailableWait');
        }

        // accountStatus is made available by the Angular component Cub_SubsystemComponent,
        // which also refreshes the ribbon once it is set
        function showBlockAccount() {
            var info = global.Cubic.cub_transitaccount.accountStatus;
            return info && (info === 'Active' || info === 'Suspended');
        }

        // accountStatus is made available by the Angular component Cub_SubsystemComponent,
        // which also refreshes the ribbon once it is set
        function showUnBlockAccount() {
            var info = global.Cubic.cub_transitaccount.accountStatus;
            return info && (info != 'Active');
        }

        function resolveBalance() {
            var dialogOptions = new Xrm.DialogOptions(),
                customerControl = Xrm.Page.getAttribute('cub_customer'),
                subsystemControl = Xrm.Page.getAttribute('cub_subsystemid'),
                modalUrl = Xrm.Page.context.getClientUrl();

            var requestBody = {
                subsystem: subsystemControl ? subsystemControl.getValue() : null,
                subsystemAccountReference: Xrm.Page.data.entity.getPrimaryAttributeValue(),
                customerId: customerControl && customerControl.getValue() ? customerControl.getValue()[0].id : null
            };

            var data = encodeURIComponent(JSON.stringify({
                route: 'ResolveBalance',
                queryParams: [
                    'debtCollectionInfo=', JSON.stringify(global.Cubic.cub_transitaccount.debtCollectionInfo),
                    '&requestBody=', JSON.stringify(requestBody)
                ].join('')
            }));

            var webResourceCacheKey = Cubic.utility.getWebResourceUriCacheKey();
            if (webResourceCacheKey) {
                modalUrl += '/' + webResourceCacheKey;
            }

            modalUrl += '/WebResources/cub_/angular/index.html?Data=' + data;

            dialogOptions.width = 400;
            dialogOptions.height = 296;
            Xrm.Internal.openDialog(modalUrl, dialogOptions, null, null, onModalClose);
        }

        function onModalClose(response) {
            if (!response) {
                return;
            }
            if (response['refresh']) {
                issueDataRefresh();
            }
            if (response['orderId'] != null) {
                showOrderDetail(response['orderId']);
            }
        }

        function issueDataRefresh() {
            Xrm.Page.data.refresh().then(function () {
                // for some reason the updated fields aren't immediately available within this callback.
                setTimeout(function () {
                    if (global.Cubic.cub_transitaccount.subsystem) {
                        global.Cubic.cub_transitaccount.subsystem.zone.run(function () {
                            global.Cubic.cub_transitaccount.subsystem.component.refresh();
                        });
                    }
                    if (global.Cubic.session) {
                        global.Cubic.session.zone.run(function () {
                            global.Cubic.session.component.refresh(true);
                        });
                    }
                });
            });
        }

        // TODO: make orderId available to Order History once the latter is implemented
        function showOrderDetail(orderId) {
            var tabControl = Xrm.Page.ui.tabs.get('order_history_tab');
            if (!tabControl || !tabControl.getVisible()) {
                console.warn('Unable to navigate to Order Detail, could not access Order History tab');
                return;
            }
            tabControl.setDisplayState('expanded');
            tabControl.setFocus();
        }

        return {
            createCase: createCase,
            linkToCustomer: linkToCustomer,
            delinkFromCustomer: delinkFromCustomer,
            isNotLinked: isNotLinked,
            isLinked: isLinked,
            showResolveBalance: showResolveBalance,
            resolveBalance: resolveBalance,
            blockCustomer: blockCustomer,
            showBlockAccount: showBlockAccount,
            showUnBlockAccount: showUnBlockAccount,
            unBlockCustomer: unBlockCustomer
        };
    }());
}(this));