﻿/// <reference path="./externlib/jquery/jquery-3.1.1.min.js" />
/// <reference path="./MSXRMTOOLS.Xrm.Page.2016.js" />

(function (global) {
    'use strict';
    global.Cubic = global.Cubic || {};
    global.Cubic.cub_transitaccount = global.Cubic.cub_transitaccount || {};
    global.Cubic.cub_transitaccount.form = (function () {

        function executeOnLoad() {
            hideHeader();
            showRibbon();
        }

        function hideHeader() {
            var headerContainer = $(".ms-crm-inlineheader-content.ms-crm-Form-HeaderPosition");
            if (headerContainer) {
                headerContainer.hide();
            }
        }

        //Make sure native ribbon is brought back on page load (due to potential back navigation)
        function showRibbon() {
            if (window.top) {
                $('#crmTopBar', window.top.document).show();
                $('#crmContentPanel', window.top.document).css('top', 112);
            }
        }

        return {
            executeOnLoad: executeOnLoad
        };
    }());

    global.Cubic.cub_transitaccount.grids = (function () {

        /**
         * Example custom grid action function
         */
        function openRRN(journalEntry) {
            var message = [
                'Journal entry ID: ', journalEntry.journalEntryId, '\n',
                'Retrieval Reference #: ', journalEntry.retrievalRefNbr
            ].join('');

            var grid = this;

            Xrm.Utility.alertDialog(message, function () {
                grid.onRefreshClicked();
            });
        }

        function openAggregationDetails(journalEntry) {
            var grid = this;
            grid.onViewDetailsActionClicked(journalEntry.retrievalRefNbr);
        }

        function openVoidTrip(journalEntry) {
            var grid = this;
            grid.onVoidTripActionClicked(journalEntry.tripId);
        }

        function openCorrectTap(tapEntry) {
            var grid = this;
            grid.onCorrectTapActionClicked(tapEntry);

        }

        function openVoidTap(tapEntry) {
            var grid = this;
            grid.onVoidTapActionClicked(tapEntry);
        }

        return {
            openRRN: openRRN,
            openAggregationDetails: openAggregationDetails,
            openVoidTrip: openVoidTrip,
            openCorrectTap: openCorrectTap,
            openVoidTap: openVoidTap
        };
    }());
}(this));