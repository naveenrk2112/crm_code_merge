﻿/**
 * ATTENTION:
 * When adding new functions to this file, please add them to the 
 * window.Cubic.utility namespace.
 */

/// <reference path="MSXRMTOOLS.Xrm.Page.2016.js" />

(function (global) {
    global.Cubic = global.Cubic || {};
    global.Cubic.utility = (function () {

        // unsupported - some native MSD script tags have id="web resource path"
        // but this may change in the future
        // https://community.dynamics.com/crm/b/develop1/archive/2011/05/18/crm-developer-must-know-2-web-resource-caching
        function getWebResourceUriCacheKey() {
            var cacheKey = '';
            var scripts = document.getElementsByTagName('script');
            var len = scripts.length;
            var i, matchResult;
            for (i = 0; i < len; i++) {
                // look for {000000000000000000} in web resource script IDs
                matchResult = scripts[i].id.match(/(\%7b|\{)[0-9]{18}(\%7d|\})/i);
                if (matchResult && matchResult[0]) {
                    cacheKey = matchResult[0];
                    break;
                }
            }
            if (!cacheKey) {
                console.warn('Unable to retrieve MSD web resource cache key. The resulting web resource may not be current.');
            }
            return cacheKey;
        }

        return {
            getWebResourceUriCacheKey: getWebResourceUriCacheKey
        };
    }());
}(this));

//CONSTANTS:
var CRM_FORM_TYPE_CREATE = 1;
var CRM_FORM_TYPE_UPDATE = 2;
var CRM_FORM_TYPE_READONLY = 3;
var CRM_FORM_TYPE_DISABLED = 4;
var CRM_FORM_TYPE_BULKEDIT = 6;
var CRM_FORM_TYPE_UNDEFINED = 0;

if (typeof (CubUtility) === 'undefined') {
    var CubUtility = {};
}

/// &lt;summary&gt;
/// TranslatePhoneMask() will step through each character of an 
/// input string and pass that character to the 
/// TranslatePhoneLetter() helper method
/// &lt;/summary&gt;
/// &lt;param name="s"&gt;Input string to translate&lt;/param&gt;
function TranslatePhoneMask(s) {
    var ret = "";

    //loop through each char, and pass it to the translation method
    for (var i = 0; i < s.length; i++) {
        ret += TranslatePhoneLetter(s.charAt(i))
    }

    return ret;
}

///// &lt;summary&gt;
///// TranslatePhoneLetter() takes a character and returns the 
///// equivalent phone number digit if it is alphanumeric
///// &lt;/summary&gt;
///// &lt;param name="s"&gt;Character to translate&lt;/param&gt;
function TranslatePhoneLetter(s) {
    var sTmp = s.toUpperCase();
    var ret = s;

    switch (sTmp) {
        case "A":
        case "B":
        case "C":
            ret = 2;
            break;
        case "D":
        case "E":
        case "F":
            ret = 3;
            break;
        case "G":
        case "H":
        case "I":
            ret = 4;
            break;
        case "J":
        case "K":
        case "L":
            ret = 5;
            break;
        case "M":
        case "N":
        case "O":
            ret = 6;
            break;
        case "P":
        case "Q":
        case "R":
        case "S":
            ret = 7;
            break;
        case "T":
        case "U":
        case "V":
            ret = 8;
            break;
        case "W":
        case "X":
        case "Y":
        case "Z":
            ret = 9;
            break;
        default:
            ret = s;
            break;
    }

    return ret;
}

function formatPhoneNumber(phoneString) {
    var returnString;
    if (phoneString == null) return;

    // Remove any special characters
    var sTmp = phoneString.replace(/[^0-9,A-Z,a-z]/g, "");

    // Translate any letters to the equivalent phone number, if method is included
    try {
          sTmp = TranslatePhoneMask(sTmp);
    }
    catch (e) {
    }

    // If the number is a length we expect and support, 
    // format the translated number
    switch (sTmp.length) {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 8:
        case 9:
            break;
        case 7:
            returnString = sTmp.substr(0, 3) + "-" + sTmp.substr(3, 4);
            break;
        case 10:
            returnString = "(" + sTmp.substr(0, 3) + ") " + sTmp.substr(3, 3) + "-" + sTmp.substr(6, 4);
            break;
        default:
            returnString = "(" + sTmp.substr(0, 3) + ") " + sTmp.substr(3, 3) + "-" + sTmp.substr(6, 4) + " " + sTmp.substr(10, sTmp.length);
            break;

    }

    return returnString;
}

// Set a lookup field value to null
// Input: Field Name
function NullFieldValue(lookupAttribute) {
    var lookupObject = Xrm.Page.getAttribute(lookupAttribute);
    if (lookupObject != null) {
        Xrm.Page.getAttribute(lookupAttribute).setValue(null);
    }
}

// Convert string to camel case
// Input: String
// Return: Converted string
function ToCamelCase(str) {
    return str.toLowerCase()
    	.replace(/\s(.)/g, function ($1) { return $1.toUpperCase(); })
        .replace(/^(.)/, function ($1) { return $1.toUpperCase(); }); //.replace(/\s/g, '')
}

// Set a lookup field value
// Input:
//	fieldName - for e.g. cub_countryid
//	entityType - for e.g. cub_country
//	guid - record id
//	name - value of the field
function SetLookUp(fieldName, entityType, guid, name) {
    try {
        var object = new Array();
        object[0] = new Object();
        object[0].id = guid;
        object[0].name = name;
        object[0].entityType = entityType;
        Xrm.Page.getAttribute(fieldName).setValue(object);
    }
    catch (e) {
        alert("Error in SetLookUp: fieldName = " + fieldName + " fieldType = " + fieldType + " fieldId = " + fieldId + " value = " + value + " error = " + e);
    }
}

function GetGlobalsAttributeValue(globalName, detailName) {
    var actionname = "cub_GlobalGetGlobalsAttributeValue";
    var actionParameters = {
        "Global": globalName,
        "GlobalDetail": detailName,
    };

    var clientURL = Xrm.Page.context.getClientUrl();
    var cubODataPath = clientURL + "/api/data/v8.2/";
    var request = new XMLHttpRequest();
    request.open("POST", cubODataPath + actionname, false);
    request.setRequestHeader("Accept", "application/json");
    request.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    request.setRequestHeader("OData-MaxVersion", "4.0");
    request.setRequestHeader("OData-Version", "4.0");

    request.send(window.JSON.stringify(actionParameters));

    if (request.status === 200) {
        var result = JSON.parse(request.response);
        return result.AttributeValue;
    } else {
        return null;
    }
}


function DeleteGridAddress(SelectedControl, SelectedControlSelectedItemReferences, SelectedEntityTypeCode) {

    var deletePromptMsg = 'This will mark the address as inactive. Do you want to continue?';
    var softDelete = GetGlobalsAttributeValue('AppInfo', 'SoftDeleteAddress');

    if (softDelete.toLowerCase() != 'yes') {
        deletePromptMsg = 'Delete this address?';
    }
    else {
        //not on the address form, need to figure out how to get the statecode value of the address record in the grid.
        //for now call org service
        var statecode = GetAddressStateCode(SelectedControlSelectedItemReferences[0].Id); //0 = active, 1=inactive
        if (statecode == 1) {
            Xrm.Page.ui.setFormNotification("This address is already Inactive", 'ERROR', '1');
            return;
        }
    }

    var retval = window.confirm(deletePromptMsg);

    if (retval == true)
    {
        CallGlobalDeleteAction(SelectedControlSelectedItemReferences[0].Id,
                            SelectedControlSelectedItemReferences[0].TypeName, 'AddressGrid', CallGlobalDeleteActionResponse);
    }

}

function GetAddressStateCode(addressId)
{
    addressId = addressId.replace(/[{}]/g, ""); // remove { }; unrelated to NIS formatting, do not delete

    var getUrl = Xrm.Page.context.getClientUrl() + "/api/data/v8.2/cub_addresses(" + addressId + ")?$select=statecode";

    var returnValue = 0;

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: getUrl,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
            XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
        },
        async: false,
        success: function (data, textStatus, xhr) {
            var result = data;
            var statecode = result["statecode"];
            var statecode_formatted = result["statecode@OData.Community.Display.V1.FormattedValue"];
            returnValue = statecode;
        },
        error: function (xhr, textStatus, errorThrown) {
            Xrm.Utility.alertDialog(textStatus + " " + errorThrown);
        }
    });

    return returnValue;
}

function CallGlobalDeleteAction(FirstPrimaryItemId, PrimaryEntityTypeName, MethodCalledFrom, unableToDeleteCallback)
{

    var result = '';
    var actionname = "cub_GlobalDeleteAction";
    var actionParameters = {
        "RecordId": FirstPrimaryItemId,
        "EntityName": PrimaryEntityTypeName
    };

    var clientURL = Xrm.Page.context.getClientUrl();
    var cubODataPath = clientURL + "/api/data/v8.2/";
    var req = new XMLHttpRequest();
    req.open("POST", cubODataPath + actionname, false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.setRequestHeader("OData-MaxVersion", "4.0");
    req.setRequestHeader("OData-Version", "4.0");
    req.onreadystatechange = null;

    req.onreadystatechange = function () {
        if (this.readyState == 4) {
            // handle success
            if (this.status == 200) {
                result = JSON.parse(this.response);
                ProcessDeleteAddressResponse(result, MethodCalledFrom, unableToDeleteCallback);
            }
            else {
                var error = JSON.parse(this.response);
                Xrm.Utility.alertDialog(error.error.message, null)
            }
        }
    };

    req.send(window.JSON.stringify(actionParameters));


}

function CallGlobalDeleteActionResponse(responseObject) {
    var data = [];

    for (var i = 0; i < responseObject.contactDependencies.length; i++) {
        var contactDependency = responseObject.contactDependencies[i];
        data.push({
            id: contactDependency.contactId,
            type: 'contact',
            title: (!contactDependency.name || (!contactDependency.name.firstName && !contactDependency.name.lastName))
                        ? '(no name)'
                        : contactDependency.name.firstName + ' ' + contactDependency.name.lastName,
            allowUpdate: '1'
        });
    }

    for (var i = 0; i < responseObject.fundingSourceDependencies.length; i++) {
        var fundingSourceDependency = responseObject.fundingSourceDependencies[i];

        var val = 'UNDEFINED';
        if (!!fundingSourceDependency.creditCard && !!fundingSourceDependency.creditCard.maskedPan) {
            val = fundingSourceDependency.creditCard.creditCardType + ' ' + fundingSourceDependency.creditCard.maskedPan;
        } else if (!!fundingSourceDependency.directDebit && !!fundingSourceDependency.directDebit.bankAccountNumber) {
            val = fundingSourceDependency.directDebit.accountType + ' ' + fundingSourceDependency.directDebit.bankAccountNumber;
        }

        data.push({
            id: fundingSourceDependency.fundingSourceId,
            type: 'fundingsource',
            title: val,
            allowUpdate: '0'
        });
    }

    var dataString = "";
    for (var i = 0; i < data.length; i++) {
        var record = data[i];
        dataString += ((i == 0 ? "" : "&") + "record" + (i + 1) + "=" + record.type + "|" + record.id + "|" + record.title + "|" + record.allowUpdate);
    }

    var dialogOptions = new Xrm.DialogOptions;
    dialogOptions.width = 380;
    dialogOptions.height = 380;
    Xrm.Internal.openDialog(Xrm.Page.context.getClientUrl() + '/WebResources/cub_/Dialogs/unableToDeleteAddress.html?Data=' + encodeURIComponent(dataString), dialogOptions, null, null, function () {
    });
}

function ProcessDeleteAddressResponse(data, MethodCalledFrom, unableToDeleteCallback) {
    var CubResponse = JSON.parse(data.CubResponse);

    if (CubResponse.Success)        // process ran without exceptions
    {
        var responseObject = CubResponse.ResponseObject;

        if (responseObject.hdr.result == "Successful") {
            //refresh if inactivated, go to account if deleted
            var softDelete = GetGlobalsAttributeValue('AppInfo', 'SoftDeleteAddress');
            if (softDelete.toLowerCase() != 'yes') {
                if (MethodCalledFrom != 'AddressGrid')
                    OpenAccount();
                else
                {
                    //refresh account page data
                    Xrm.Page.data.refresh();
                    Xrm.Page.ui.setFormNotification("Address Deleted", 'INFO', '1');
                }
            }
            else {  //soft delete
                if (MethodCalledFrom != 'AddressGrid') {
                    // on address form, refresh it
                    Xrm.Page.data.refresh();
                    //Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(), Xrm.Page.data.entity.getId());
                }
                else {
                    Xrm.Page.data.refresh();
                    Xrm.Page.ui.setFormNotification("Address Inactivated", 'INFO', '1');
                }
            }
        }
        else {
            if (unableToDeleteCallback) {
                unableToDeleteCallback(responseObject);
            }
            else {
                Xrm.Page.ui.setFormNotification(responseObject.hdr.errorMessage, 'ERROR', '1');
                Xrm.Page.ui.setFormNotification("Total Funding Source Dependencies: " + responseObject.fundingSourceDependencies.length, 'INFO', '2');
                Xrm.Page.ui.setFormNotification("Total Contact Dependencies: " + responseObject.contactDependencies.length, 'INFO', '3');
            }
        }
    }
    else {
        Xrm.Page.ui.setFormNotification(CubResponse.Errors[0].errorMessage, 'ERROR', '1');
    }

}

function OpenAccount(onAccountForm) {

    var accountId;

    if (onAccountForm == true)  //could be used to reload an account record while on account page
        Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(), Xrm.Page.data.entity.getId());
    else{
        if (Xrm.Page.data.entity.getEntityName() == "cub_address")
            accountId = Xrm.Page.getAttribute("cub_accountaddressid").getValue()[0].id;
        else
            return;
    }
    
    Xrm.Utility.openEntityForm("account", accountId);
}

function FindQueryParam(key) {
    var queryParams = window.location.search,
        pairs, pair, i;
    if (!queryParams) {
        return null;
    }
    if (queryParams.startsWith('?')) {
        queryParams = queryParams.substr(1);
    }
    pairs = queryParams.split('&');
    for (i = 0; i < pairs.length; ++i) {
        pair = pairs[i].split('=');
        if (pair[0] === key) {
            return pair[1];
        }
    }
    return null;
}
