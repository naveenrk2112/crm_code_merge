﻿/// <reference path="MSXRMTOOLS.Xrm.Page.2016.js" />
/// <reference path="CubUtilityjs" />

(function (global) {
    "use strict";
    global.Cubic = global.Cubic || {};
    global.Cubic.cub_account_ribbon = (function createNamespace() {

        function linkToken() {
            var oneAccountId = Xrm.Page.getAttribute('cub_oneaccountid').getValue();
            var data = encodeURIComponent(JSON.stringify({
                route: 'MasterSearchToken',
                queryParams: [
                    'customerId=', Xrm.Page.data.entity.getId(),
                    '&customerName=', Xrm.Page.data.entity.getPrimaryAttributeValue(),
                    '&oneAccountId=', oneAccountId
                ].join('')
            }));
            var url = Xrm.Page.context.getClientUrl();
            var cacheKey = Cubic.utility.getWebResourceUriCacheKey();
            if (cacheKey) {
                url += '/' + cacheKey;
            }
            url += '/WebResources/cub_/angular/index.html?pagemode=iframe&Data=' + data;
            window.location.href = url;
        }

        function showLinkButton() {
            var oneAccountControl = Xrm.Page.getAttribute('cub_oneaccountid');
            return !!oneAccountControl && !!oneAccountControl.getValue();
        }

        return {
            linkToken: linkToken,
            showLinkButton: showLinkButton
        };
    }());
}(this));