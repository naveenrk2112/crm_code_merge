/// <reference path="MSXRMTOOLS.Xrm.Page.2016.js" />
/// <reference path="notify.js" />
/// <reference path="CubUtility.js" />

// JavaScript Document
//CONSTANTS:
var CRM_FORM_TYPE_CREATE = 1;
var CRM_FORM_TYPE_UPDATE = 2;
var CRM_FORM_TYPE_READONLY = 3;
var CRM_FORM_TYPE_DISABLED = 4;
var CRM_FORM_TYPE_BULKEDIT = 6;
var CRM_FORM_TYPE_UNDEFINED = 0;
var Sdk = window.Sdk || {};

function displayError(controlName, eventName, errorDescription) {
    alert("There was an error with this field\x27s customized event.\n\nField\x3a" + controlName + "\n\nEvent\x3a" + eventName + "\n\nError\x3a" + errorDescription);
}

function Form_OnLoad() {

    Xrm.Page.getAttribute("subjectid").addOnChange(subject_onchange);
    if (window.top.opener != null && window.top.opener.Cubic != null) {
        var cubSessionId = Xrm.Page.data.entity.attributes.get('cub_sessionid');
        if (cubSessionId.getValue() == null && window.top.opener.Cubic.session != null) {
            var sessionId = '{' + window.top.opener.Cubic.session.component.session.sessionId + '}';
            SetLookUp("cub_sessionid", "cub_csrsession", sessionId, window.top.opener.Cubic.session.component.session.sessionNumber);
        }

        //var cubGroupId = Xrm.Page.data.entity.attributes.get('cub_groupid');
        //if (cubGroupId.getValue() == null)
        //    cubGroupId.setValue(window.top.opener.Cubic.session.component.session.sessionId.toUpperCase());

    }
}

function subject_onchange(objExecution) {
   var formType = Xrm.Page.ui.getFormType();

    switch (formType) {
        case CRM_FORM_TYPE_CREATE:
        case CRM_FORM_TYPE_UPDATE:
            {
                Xrm.Page.data.entity.save();
            }
    }
}

