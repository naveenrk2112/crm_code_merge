/// <reference path="MSXRMTOOLS.Xrm.Page.2016.js" />
/// <reference path="notify.js" />
/// <reference path="WebAPI.js" />
/// <reference path="CubUtility.js" />

(function (global) {
    'use strict';
    global.Cubic = global.Cubic || {};
    global.Cubic.contact = (function () {
        var VERIFICATION_MESSAGE = 'VERIFICATION_MESSAGE';
        var contactId;
        var webApi;

        function executeOnLoad() {
            var formType = Xrm.Page.ui.getFormType();
            var oneAccountControl = Xrm.Page.getAttribute('cub_oneaccountid');
            if (formType === 2) {
                if (oneAccountControl && oneAccountControl.getValue() == null) {
                    getOneAccount();
                }
            }
        }

        function getOneAccount() {
            var customerControl = Xrm.Page.getAttribute('parentcustomerid');
            if (!customerControl || !customerControl.getValue()) {
                return;
            }

            var customerId = customerControl.getValue()[0].id.replace(/[{}]/g, '');

            if (!webApi) {
                webApi = new WebAPI();
            }

            webApi.get('accounts(' + customerId + ')?$select=cub_oneaccountid')
                .then(function onSuccess(response) {
                    var customer = JSON.parse(response.responseText);
                    if (customer && customer['cub_oneaccountid']) {
                        var oneAccountControl = Xrm.Page.getAttribute('cub_oneaccountid');
                        oneAccountControl.setValue(customer['cub_oneaccountid']);
                        oneAccountControl.setSubmitMode('always');
                        Xrm.Page.data.save().then(function (){
                            Xrm.Page.ui.refreshRibbon();
                        });
                    }
                })
                .catch(function onFailure(err) {
                    console.log('Error retrieving OneAccount ID from parent customer:', err);
                });
        }

        // Triggered by session bar
        function authenticateContact() {
            if (Xrm.Page.ui.getFormType() === 2) {
                Xrm.Page.ui.clearFormNotification(VERIFICATION_MESSAGE);
                contactId = Xrm.Page.data.entity.getId();
                getCurrentVerificationStatus();
            }
        }

        // Get this contact's current authentication status.
        // Called by authenticateContact()
        function getCurrentVerificationStatus() {
            var webServiceUrl = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL');
            var currentStatusPath = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL.VERIFICATION.GETCURRENTSTATUS');
            var userId = Xrm.Page.context.getUserId();
            var url = [
                webServiceUrl, currentStatusPath,
                '?userId=', userId,
                '&contactId=', contactId
            ].join('');

            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.setRequestHeader('Cache-Control', 'no-cache');
            xhr.setRequestHeader('Pragma', 'no-cache');
            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200 && xhr.responseText) {
                        checkRecentActivity(xhr.responseText);
                    } else {
                        Xrm.Page.ui.setFormNotification('Error retrieving contact authentication status', 'ERROR', VERIFICATION_MESSAGE);
                        console.log(xhr.responseText);
                    }
                }
            };
            xhr.send();
        }

        // Determine whether or not to perform the contact authentication process.
        // Called by getCurrentVerificationStatus()
        function checkRecentActivity(response) {
            var verificationStatus = JSON.parse(response);
            if (!verificationStatus.hasRecentActivity) {
                showContactVerification();
            } else {
                Xrm.Page.ui.setFormNotification('Contact has recently completed the authentication process', 'INFO', VERIFICATION_MESSAGE);
                setTimeout(function () {
                    Xrm.Page.ui.clearFormNotification(VERIFICATION_MESSAGE);
                }, 5000);
            }
        }

        // Display the contact verification modal window to begin contact authentication process
        // Called by checkRecentActivity()
        function showContactVerification() {
            var url = Xrm.Page.context.getClientUrl();
            var webResourceCacheKey = Cubic.utility.getWebResourceUriCacheKey();
            var dialogOptions = new Xrm.DialogOptions();
            var data = encodeURIComponent(JSON.stringify({
                route: 'Verification',
                queryParams: 'contact=' + JSON.stringify(getContactDataForVerification())
            }));

            if (webResourceCacheKey) {
                url += '/' + webResourceCacheKey;
            }
            url += '/WebResources/cub_/angular/index.html?Data=' + data;

            dialogOptions.width = 768;
            dialogOptions.height = 388;
            Xrm.Internal.openDialog(url, dialogOptions, null, null, onContactVerificationClose);
        }

        // If contact is now verified, update the session bar
        // Called by showContactVerification()
        function onContactVerificationClose(returnValue) {
            if (returnValue) {
                global.Cubic.session.zone.run(function () {
                    global.Cubic.session.component.update({
                        cub_isverified: true
                    });
                });
            }
        }

        // Get all contact data needed for verification from the form
        // Called by showContactVerification()
        function getContactDataForVerification() {
            var data = {
                ContactId: contactId,
                FullName: Xrm.Page.data.entity.getPrimaryAttributeValue()
            };
            var control = Xrm.Page.getAttribute('telephone1');
            if (control) {
                data.Phone1 = control.getValue();
            }
            control = Xrm.Page.getAttribute('cub_phone1type');
            if (control && control.getSelectedOption()) {
                data.Phone1Type = control.getSelectedOption().text;
            }
            control = Xrm.Page.getAttribute('telephone2');
            if (control) {
                data.Phone2 = control.getValue();
            }
            control = Xrm.Page.getAttribute('cub_phone2type');
            if (control && control.getSelectedOption()) {
                data.Phone2Type = control.getSelectedOption().text;
            }
            control = Xrm.Page.getAttribute('telephone3');
            if (control) {
                data.Phone3 = control.getValue();
            }
            control = Xrm.Page.getAttribute('cub_phone3type');
            if (control && control.getSelectedOption()) {
                data.Phone3Type = control.getSelectedOption().text;
            }
            control = Xrm.Page.getAttribute('birthdate');
            if (control && control.getValue()) {
                data.BirthDate = control.getValue().toISOString();
            }
            control = Xrm.Page.getAttribute('emailaddress1');
            if (control) {
                data.Email = control.getValue();
            }
            return data;
        }

        return {
            executeOnLoad: executeOnLoad,
            authenticateContact: authenticateContact
        };
    }());
}(this));

// JavaScript Document
//CONSTANTS:
var CRM_FORM_TYPE_CREATE = 1;
var CRM_FORM_TYPE_UPDATE = 2;
var CRM_FORM_TYPE_READONLY = 3;
var CRM_FORM_TYPE_DISABLED = 4;
var CRM_FORM_TYPE_BULKEDIT = 6;
var CRM_FORM_TYPE_UNDEFINED = 0;
var Sdk = window.Sdk || {};

function displayError(controlName, eventName, errorDescription) {
    alert("There was an error with this field\x27s customized event.\n\nField\x3a" + controlName + "\n\nEvent\x3a" + eventName + "\n\nError\x3a" + errorDescription);
}

function emailaddressCheck() {
    var contactID = "";
    var errorMessage = "";
    var results = "";
    var actionname = "cub_GlobalCheckEmailAction";
    var email = Xrm.Page.getAttribute("emailaddress1").getValue();
    var recordId = Xrm.Page.data.entity.getId();
    var actionParameters = {
        "EmailAddress": email,
                "ID": recordId
    };

    var clientURL = Xrm.Page.context.getClientUrl();
    var cubODataPath = clientURL + "/api/data/v8.0/";
    var req = new XMLHttpRequest();
    req.open("POST", cubODataPath + actionname, false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.setRequestHeader("OData-MaxVersion", "4.0");
    req.setRequestHeader("OData-Version", "4.0");
    req.onreadystatechange = null;

    req.onreadystatechange = function () {
        if (this.readyState == 4) {
            // handle success
            if (this.status == 200) {
                result = JSON.parse(this.response);
                Xrm.Page.ui.clearFormNotification('1');
                return "";
            }
            else {
                var error = JSON.parse(this.response);
                Xrm.Page.ui.setFormNotification(result.DuplicateID, 'ERROR', '1')
                return result.DuplicateID;
            }
        }
    };

    req.send(window.JSON.stringify(actionParameters));
   
    if (result.DuplicateID != "") {
        if (result.DuplicateID.substr(0, 3) == "ID:") {
            contactID = result.DuplicateID.substr(3, 36);
            errorMessage = result.DuplicateID.substr(40, result.DuplicateID.length - 40);
        }
        Notify.add(errorMessage, "ERROR", "emailError",
         [{
             type: "Button",
             text: "View Duplicate",
             callback: function () {
                 //  Notify.add("Contact Opened.", "SUCCESS", "emailError", null, 3);
                 //                     var windowOptions = {
                 //                         openInNewWindow: true
                 //                     };
                 // openEntityForm forces an onSave which throws the dirty warning so can't use
                 //Xrm.Utility.openEntityForm("contact", contactID, windowOptions);
                 var url = Xrm.Page.context.getClientUrl() + "/main.aspx?etn=contact&pagetype=entityrecord&id=" + contactID;
                 window.open(url, '_blank', 'width=750,height=900,status=1,resizable=1,location=no,toolbar=no');
             }
         },
         {
             type: "link",
             text: "Close",
             callback: function () {
                 Notify.remove("emailError");
             }
         }]);

        Xrm.Page.getControl("emailaddress1").setFocus();
        return true;
    }
    else {
        Notify.remove("emailError");
    }
    return false;

}

function Form_OnLoad() {
    realignQuickForms();
    hideHeader();
    showRibbon();

    var formType = Xrm.Page.ui.getFormType();

    switch (formType) {
        case CRM_FORM_TYPE_UPDATE:
            {
                var phoneNumber = Xrm.Page.getAttribute("telephone1");
                var phoneNumber2 = Xrm.Page.getAttribute("telephone2");
                var phoneNumber3 = Xrm.Page.getAttribute("telephone3");

                if (phoneNumber.getValue() != null) {
                      phoneNumber.setSubmitMode("never"); // avoid the isdirty flag 
                      phoneNumber.setValue(formatPhoneNumber(phoneNumber.getValue()));
                  //  phoneNumber.setSubmitMode("always"); 
                }

                if (phoneNumber2.getValue() != null) {
                    phoneNumber2.setSubmitMode("never"); // avoid the isdirty flag 
                    phoneNumber2.setValue(formatPhoneNumber(phoneNumber2.getValue()));
               //     phoneNumber2.setSubmitMode("always");
                }

                if (phoneNumber3.getValue() != null) {
                    phoneNumber3.setSubmitMode("never"); // avoid the isdirty flag 
                    phoneNumber3.setValue(formatPhoneNumber(phoneNumber3.getValue()));
             //       phoneNumber3.setSubmitMode("always");
                }
            }
    }

    function emailaddress1_onchange() {
       var ret = emailaddressCheck();
    }     

    //REGISTER ONCHANGE EVENTS:
    Xrm.Page.getAttribute("emailaddress1").addOnChange(emailaddress1_onchange);
}


function Form_onsave(objExecution) {
    // Using Notify instead of this
    //Xrm.Page.ui.clearFormNotification('1');
    // Xrm.Page.data.refresh(true);
    if (Xrm.Page.getAttribute('emailaddress1').getIsDirty()) {
        var ret = emailaddressCheck();
        if (ret) {
            objExecution.getEventArgs().preventDefault();
        }
    }
}

function formatChangedPhone(phoneNumber) {
    if (typeof (phoneNumber) != "undefined" && phoneNumber != null) {
        if (phoneNumber.getValue() != null) {
          phoneNumber.setValue(formatPhoneNumber(phoneNumber.getValue()));
        }
    }
}


function telephone1_onchange(objExecution) {
    var phoneNumber = Xrm.Page.getAttribute("telephone1");
    if (phoneNumber.getIsDirty() == true) {
            phoneNumber.setSubmitMode("always");
            phoneNumber.setValue(formatPhoneNumber(phoneNumber.getValue()));
    }
}

function telephone2_onchange(objExecution) {
    var phoneNumber = Xrm.Page.getAttribute("telephone2");
    if (phoneNumber.getIsDirty() == true) {
        phoneNumber.setSubmitMode("always");
        phoneNumber.setValue(formatPhoneNumber(phoneNumber.getValue()));
    }
}

function telephone3_onchange(objExecution) {
    var phoneNumber = Xrm.Page.getAttribute("telephone3");
    if (phoneNumber.getIsDirty() == true) {
        phoneNumber.setSubmitMode("always");
        phoneNumber.setValue(formatPhoneNumber(phoneNumber.getValue()));
    }
}

function realignQuickForms() {
    try{
        var quickViewFormSections = document.querySelectorAll('div.ms-crm-QuickForm table.ms-crm-FormSection');
        for (var i = 0; i < quickViewFormSections.length; ++i) {
            quickViewFormSections[i].style.padding = '0';
            quickViewFormSections[i].style.marginLeft = '-5px';
        }
    } catch (e) {
        console.error(e);
    }
}

function resetPassword() {

    var sendForgotTokenPrompt = 'This will email the patron a password reset token. \n Do you want to continue?';
    
    var retval = window.confirm(sendForgotTokenPrompt);

    if (retval == true) {
       
        var websiteUrl = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL');
        var websiteForgotPasswordUrl = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL.FORGOTPASSWORD');

        var customerId = Xrm.Page.getAttribute("parentcustomerid").getValue()[0].id;

        var params = {
            "contactID": Xrm.Page.data.entity.getId(),
            "customerId": customerId,
            "userId": Xrm.Page.context.getUserId()
        };
        
        //url: 'http://localhost:12665/Contact/ForgotPassword?contactId=2B9634F0-3F29-E711-80EA-005056813E0D'

        $.ajax({
            async: false,
            type: 'POST',
            data: params,
            dataType: 'JSON',
            cache: false,
            url: websiteUrl + websiteForgotPasswordUrl,
            success: function (response, textStatus, jqXHR) {
                if (response != null && response.Success) {
                   
                        Xrm.Utility.alertDialog('New password token has been successfully sent to patron');
                    }
                    else {
                        Xrm.Utility.alertDialog('Error occurred while sending password token to patron:\n' + reqResults.Error);
                    }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                Xrm.Utility.alertDialog(this.textStatus + '\n' + errorThrown);
            }
        });
    }
}

function hideHeader() {
    var headerContainer = $(".ms-crm-inlineheader-content.ms-crm-Form-HeaderPosition");
    if (headerContainer) {
        headerContainer.hide();
    }
}

function showRibbon() {
    $('#crmTopBar', window.top.document).show();
    $('#crmContentPanel', window.top.document).css('top', 112);
}
