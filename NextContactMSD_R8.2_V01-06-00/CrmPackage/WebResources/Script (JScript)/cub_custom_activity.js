﻿function Form_OnLoad()
{
    if (window.top.opener != null && window.top.opener.Cubic != null)
    {
        var cubSessionId = Xrm.Page.data.entity.attributes.get('cub_session');
        if (cubSessionId.getValue() == null && window.top.opener.Cubic.session != null)
        {
            var sessionId = '{' + window.top.opener.Cubic.session.component.session.sessionId + '}';
            SetLookUp("cub_session", "cub_csrsession", sessionId, window.top.opener.Cubic.session.component.session.sessionNumber);
        }

        var cubGroupId = Xrm.Page.data.entity.attributes.get('cub_groupid');
        if (cubGroupId.getValue() == null)
            cubGroupId.setValue(window.top.opener.Cubic.session.component.session.sessionId.toUpperCase());

    }


}