﻿/// <reference path="MSXRMTOOLS.Xrm.Page.2016.js" />
/// <reference path="CubUtility.js" />

(function (global) {
    "use strict";
    global.Cubic = global.Cubic || {};
    global.Cubic.cub_contact_ribbon = (function createNamespace() {

        function linkToken() {
            var customer = Xrm.Page.getAttribute('parentcustomerid').getValue()[0];
            var oneAccountId = Xrm.Page.getAttribute('cub_oneaccountid').getValue();
            var data = encodeURIComponent(JSON.stringify({
                route: 'MasterSearchToken',
                queryParams: [
                    'customerId=', customer.id,
                    '&customerName=', customer.name,
                    '&oneAccountId=', oneAccountId
                ].join('')
            }));
            var url = Xrm.Page.context.getClientUrl();
            var webResourceCacheKey = Cubic.utility.getWebResourceUriCacheKey();
            if (webResourceCacheKey) {
                url += '/' + webResourceCacheKey;
            }
            url += '/WebResources/cub_/angular/index.html?pagemode=iframe&Data=' + data;
            window.location.href = url;
        }

        function showLinkButton() {
            var customerControl = Xrm.Page.getAttribute('parentcustomerid');
            var oneAccountControl = Xrm.Page.getAttribute('cub_oneaccountid');
            return !!customerControl && !!customerControl.getValue()
                && !!oneAccountControl && !!oneAccountControl.getValue();
        }

        return {
            linkToken: linkToken,
            showLinkButton: showLinkButton
        };
    }());
}(this));