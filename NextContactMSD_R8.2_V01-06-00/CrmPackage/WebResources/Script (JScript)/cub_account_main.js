﻿/// <reference path="MSXRMTOOLS.Xrm.Page.2016.js" />
/// <reference path="CubUtility.js" />

(function (global) {
    "use strict";
    global.Cubic = global.Cubic || {};
    global.Cubic.account = (function createNamespace() {
        var VERIFICATION_MESSAGE = 'VERIFICATION_MESSAGE';
        var _primaryContactJson;
        var _webServiceUrl;

        function executeOnLoad() {

            // SS - NXTC-3069 - The 'New' button on the customer form will be replaced with a custom
            // new button to open the customer registration form but adding code here to redirect in case
            // we end up here from some other place.
            if (Xrm.Page.data.entity.getId() == "")
            {
                return RedirectToCustomerRegistration();
            }
            hideHeader();
            showRibbon();

            var oneAccountControl = Xrm.Page.getAttribute('cub_oneaccountid');
            var formType = Xrm.Page.ui.getFormType();

            if (formType === 2
                && oneAccountControl
                && oneAccountControl.getValue() == null
            ) {
                completeRegistration();
            }
        }
        
        function hideHeader() {
            var headerContainer = $(".ms-crm-inlineheader-content.ms-crm-Form-HeaderPosition");
            if (headerContainer) {
                headerContainer.hide();
            }
        }

        //Make sure native ribbon is brought back on page load (due to potential back navigation)
        function showRibbon() {
            if (window.top) {
                $('#crmTopBar', window.top.document).show();
                $('#crmContentPanel', window.top.document).css('top', 112);
            }
        }

        // Call NIS to complete registration and get back OneAccountId
        function completeRegistration() {
            var customerId = Xrm.Page.data.entity.getId(),
            path = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL.CUSTOMER.COMPLETEREGISTRATION'),
            xhr = new XMLHttpRequest(),
            currentUserId = Xrm.Page.context.getUserId(),
            queryParams = '?accountId=' + customerId + '&userId=' + currentUserId;

            if (!_webServiceUrl) {
                _webServiceUrl = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL');
            }

            xhr.open('POST', _webServiceUrl + path + queryParams, true);
            xhr.setRequestHeader('Cache-Control', 'no-cache');
            xhr.setRequestHeader('Pragma', 'no-cache');
            xhr.onreadystatechange = function () {
                var myResponse;
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    myResponse = xhr.responseText;
                    try {
                        myResponse = JSON.parse(myResponse);
                        Xrm.Page.data.refresh();
                        executeOnLoad(); // execute again to check verification status
                    } catch (e) { }
                }
            };
            xhr.send();
        }

        // Triggered by Angular session bar
        function authenticateContact() {
            if (Xrm.Page.ui.getFormType() === 2) {
                Xrm.Page.ui.clearFormNotification(VERIFICATION_MESSAGE);
                getPrimaryContact();
            }
        }

        // Check this customer's primary contact authentication status by finding the contact.
        // Called by authenticateContact()
        function getPrimaryContact() {
            var getContactPath = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL.CUSTOMER.GETPRIMARYCONTACT');
            var xhr = new XMLHttpRequest();

            if (!_webServiceUrl) {
                _webServiceUrl = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL');
            }

            var url = [
                _webServiceUrl, getContactPath,
                '?customerId=', Xrm.Page.data.entity.getId()
            ].join('');

            xhr.open('GET', url, true);
            xhr.setRequestHeader('Cache-Control', 'no-cache');
            xhr.setRequestHeader('Pragma', 'no-cache');
            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        _primaryContactJson = xhr.responseText;
                        getCurrentVerificationStatus();
                    } else {
                        Xrm.Page.ui.setFormNotification('Cannot check authentication status, unable to retrieve primary contact data', 'ERROR', VERIFICATION_MESSAGE);
                        console.log(xhr.responseText);
                    }
                }
            };
            xhr.send();
        }

        // Get this customer's primary contact's current authentication status.
        // Called by getPrimaryContact()
        function getCurrentVerificationStatus() {
            var contact = JSON.parse(_primaryContactJson);
            if (!contact || !contact.ContactId) {
                Xrm.Page.ui.setFormNotification('Cannot check authentication status, no primary contact found', 'ERROR', VERIFICATION_MESSAGE);
                return;
            }

            var currentStatusPath = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL.VERIFICATION.GETCURRENTSTATUS');
            var xhr = new XMLHttpRequest();
            var userId = Xrm.Page.context.getUserId();
            if (!_webServiceUrl) {
                _webServiceUrl = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL');
            }

            var url = [
                _webServiceUrl, currentStatusPath,
                '?userId=', userId,
                '&contactId=', contact.ContactId
            ].join('');

            xhr.open('GET', url, true);
            xhr.setRequestHeader('Cache-Control', 'no-cache');
            xhr.setRequestHeader('Pragma', 'no-cache');
            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200 && xhr.responseText) {
                        checkRecentActivity(xhr.responseText);
                    } else {
                        Xrm.Page.ui.setFormNotification('Error retrieving contact verification status', 'ERROR', VERIFICATION_MESSAGE);
                        console.log(xhr.responseText);
                    }
                }
            };
            xhr.send();
        }

        // Determine whether or not to perform the contact authentication process.
        // Called by getCurrentVerificationStatus()
        function checkRecentActivity(response) {
            var verificationStatus = JSON.parse(response);
            if (!verificationStatus.hasRecentActivity) {
                showContactVerification();
            } else {
                Xrm.Page.ui.setFormNotification('Primary contact has recently completed the authentication process', 'INFO', VERIFICATION_MESSAGE);
                setTimeout(function () {
                    Xrm.Page.ui.clearFormNotification(VERIFICATION_MESSAGE);
                }, 5000);
            }
        }

        // Display the contact verification modal window to begin contact authentication process
        // Called by checkRecentActivity()
        function showContactVerification() {
            var url = Xrm.Page.context.getClientUrl();
            var webResourceCacheKey = Cubic.utility.getWebResourceUriCacheKey();
            var dialogOptions = new Xrm.DialogOptions();
            var data = encodeURIComponent(JSON.stringify({
                route: 'Verification',
                queryParams: 'contact=' + _primaryContactJson
            }));

            if (webResourceCacheKey) {
                url += '/' + webResourceCacheKey;
            }
            url += '/WebResources/cub_/angular/index.html?Data=' + data;

            dialogOptions.width = 768;
            dialogOptions.height = 388;
            Xrm.Internal.openDialog(url, dialogOptions, null, null, onContactVerificationClose);
        }

        // If contact is now verified, update the session bar
        // Called by showContactVerification()
        function onContactVerificationClose(returnValue) {
            if (returnValue) {
                global.Cubic.session.zone.run(function () {
                    global.Cubic.session.component.update({
                        cub_isverified: true
                    });
                });
            }
        }

        return {
            executeOnLoad: executeOnLoad,
            authenticateContact: authenticateContact
        };
    }());
}(this));

function RedirectToCustomerRegistration() {

    var data = encodeURIComponent(JSON.stringify({
        route: 'CustomerRegistration',
        queryParams: [

        ].join('')
    }));
    var url = Xrm.Page.context.getClientUrl();
    var cacheKey = Cubic.utility.getWebResourceUriCacheKey();
    if (cacheKey) {
        url += '/' + cacheKey;
    }
    url += '/WebResources/cub_/angular/index.html?Data=' + data;

    window.location.href = url;
}