// JavaScript Document

var Sdk = window.Sdk || {};

//CONSTANTS:
var CRM_FORM_TYPE_CREATE = 1;
var CRM_FORM_TYPE_UPDATE = 2;
var CRM_FORM_TYPE_READONLY = 3;
var CRM_FORM_TYPE_DISABLED = 4;
var CRM_FORM_TYPE_BULKEDIT = 6;
var CRM_FORM_TYPE_UNDEFINED = 0;

function Form_OnLoad() {
    //REGISTER ONCHANGE EVENTS:
    Xrm.Page.getAttribute("cub_postalcodeid").addOnChange(zip_onchange);
    Xrm.Page.getAttribute("cub_stateprovinceid").addOnChange(state_onchange);

    // Register onSave event; keep this here and NOT specified in the form editor
    // because certain conditions require this event to be unregistered
    Xrm.Page.data.entity.addOnSave(Form_OnSave);
}

/// Fires each time the form is saved
/// <param name="objExecution" type="object">The execution context of this save event</param>
function Form_OnSave(objExecution) {
    var formType = Xrm.Page.ui.getFormType();
    if (formType === CRM_FORM_TYPE_UPDATE) {
        objExecution.getEventArgs().preventDefault();
        //Notify.remove();
        addressPreUpdateValidate();
    }
}

function getAddressFieldsFromForm() {
    var customerId = getGuid('cub_accountaddressid'),
        street1 = Xrm.Page.getAttribute('cub_street1').getValue(),
        street2 = Xrm.Page.getAttribute('cub_street2').getValue(),
        city = Xrm.Page.getAttribute('cub_city').getValue(),
        stateId = getGuid('cub_stateprovinceid'),
        zipId = getGuid('cub_postalcodeid'),
        countryId = getGuid('cub_countryid'),
        addressId = Xrm.Page.data.entity.getId();

    return {
        customerId: customerId,
        address: {
            addressId: addressId,
            address1: street1,
            address2: street2,
            city: city,
            stateId: stateId,
            postalCodeId: zipId,
            countryId: countryId
        }
    };
}

function getGuid(logicalName) {
    return Xrm.Page.getAttribute(logicalName).getValue()[0].id;
}

// Make an HTTP request to the CUB.MSD.Web service to check whether or not this
// address being saved already exists for its customer
// @param context:object The execution context of this save event
// @param callback:function Executes with results from successful HTTP response
function addressPreUpdateValidate() {
    var webServiceUrl = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL'),
        webServicePath = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL.ADDRESS.PREUPDATEVALIDATE'),
        formData = getAddressFieldsFromForm(),
        xhr = new XMLHttpRequest(),
        queryString = [
            '?street1=', formData.address.address1,
            '&street2=', formData.address.address2,
            '&street3=',
            '&city=', formData.address.city,
            '&stateId=', formData.address.stateId,
            '&zipId=', formData.address.postalCodeId,
            '&countryId=', formData.address.countryId,
            '&customerId=', formData.customerId,
            '&addressId=', formData.address.addressId
        ].join('');

    // This GET request is synchronous
    xhr.open('GET', webServiceUrl + webServicePath + queryString, false);
    xhr.send();
    if (xhr.status === 200) {
        postAddressPreUpdateValidate(JSON.parse(xhr.responseText), webServiceUrl);
    } else {
        Notify.add('CUB.MSD.Web service AddressPreUpdateValidate error ' + xhr.status, 'ERROR');
    }
}

// Callback function that interprets results from CUB.MSD.Web service call;
// if response is not a Success, display the errors as notifications and exit;
// if there is only one (or none) address dependency, save and close the form;
// if there are more than one, open the Update Address modal window.
// @param context:object The execution context of this save event
// @param responseBody:object CUB.MSD.Web service response object
function postAddressPreUpdateValidate(responseBody, webServiceUrl) {
    var numDependencies, responseError, i;

    if (!responseBody.Success) {
        for (i = 0; i < responseBody.Errors.length; ++i) {
            responseError = responseBody.Errors[i];
            Notify.add(responseError.errorMessage, 'ERROR');
        }
        return;
    }

    numDependencies = responseBody.ResponseObject.contactDependencies.length
        + responseBody.ResponseObject.fundingSourceDependencies.length;

    if (numDependencies > 0) {
        openUpdateAddressModal(responseBody.ResponseObject);
    } else {
        updateAddressNoDependencies(webServiceUrl);
    }
}

// Parse address data to be passed to Angular component 
// and open Update Address modal window
// @param responseObject:object dependency data from CUB.MSD.Web service response
function openUpdateAddressModal(responseObject) {
    var dialogOptions = new Xrm.DialogOptions,
        modalUrl = Xrm.Page.context.getClientUrl() + '/WebResources/cub_/Dialogs/updateAddress.html?Data=',
        formData = getAddressFieldsFromForm();

    responseObject.customerId = formData.customerId;
    responseObject.address = formData.address;
    responseObject.currentUserId = Xrm.Page.context.getUserId();
    modalUrl += encodeURIComponent(JSON.stringify(responseObject));
    dialogOptions.width = 380;
    dialogOptions.height = 380;

    Xrm.Internal.openDialog(modalUrl, dialogOptions, null, null, onDialogClose);
}

function updateAddressNoDependencies(webServiceUrl) {
    var webServicePath = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL.ADDRESS.UPDATE'),
        currentUserId = Xrm.Page.context.getUserId(),
        formData = getAddressFieldsFromForm(),
        xhr = new XMLHttpRequest(),
        responseBody, i;

    xhr.open('POST', webServiceUrl + webServicePath, false);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
    xhr.send(JSON.stringify({
        userId: currentUserId,
        customerId: formData.customerId,
        addressId: formData.address.addressId,
        requestBody: {
            address: formData.address,
            contactsUpdated: [],
            fundingSourcesUpdated: []
        }
    }));

    if (xhr.status === 200 && (responseBody = JSON.parse(xhr.responseText)).Success) {
        Notify.add('Address updated', 'SUCCESS');
        setTimeout(function () {
            closeWithoutSave();
        }, 1000);
    } else if (responseBody) {
        for (i = 0; i < responseBody.Errors.length; ++i) {
            Notify.add(responseBody.Errors[i].errorMessage, 'ERROR');
        }
    } else {
        Notify.add('CUB.MSD.Web service UpdateAddress error ' + xhr.status, 'ERROR');
    }
}

// Callback function that executes when the Update Address modal window closes
// @param returnValue:object set by the modal window, contains an action and optional data
function onDialogClose(returnValue) {
    var data;
    if (!returnValue) {
        return;
    }

    data = JSON.parse(returnValue);
    if (data.action === 'error') {
        if (data.error instanceof Array) {
            for (i = 0; i < data.error.length; ++i) {
                Notify.add(data.error[i].errorMessage, 'ERROR');
            }
        } else {
            Notify.add(data.error, 'ERROR', null, null, 10);
        }
    }
    else if (data.action === 'success') {
        Notify.add('Address updated', 'SUCCESS');
        setTimeout(function () {
            closeWithoutSave();
        }, 1000);
    }
}

// Prevents unsaved field warning dialog
function closeWithoutSave() {
    Xrm.Page.data.entity.attributes.get().forEach(function modifyFieldSubmitMode(attr) {
        attr.setSubmitMode('never');
    });
    Xrm.Page.ui.close();
}

Sdk.filterZip = function () {

    var country = Xrm.Page.getAttribute("cub_countryid").getValue();
    var state = Xrm.Page.getAttribute("cub_stateprovinceid").getValue();

    //if Country or State has a value, proceed
    if (country != null || state != null) {

        var countryTextValue = null;
        var countryID = null;
        var stateTextValue = null;
        var stateID = null;

        if (country != null) {
            countryTextValue = country[0].name;
            countryID = country[0].id;
        }
        if (state != null) {
            stateTextValue = state[0].name;
            stateID = state[0].id;
        }

        var filter = '<filter type="and" ><condition attribute="cub_country" operator="eq" value=' + countryID + ' /></filter>';
        //  "<filter type='and'><condition attribute='cub_country' operator='eq' value='" + countryID + "'/></filter>"
        Xrm.Page.getControl("cub_postalcodeid").addCustomFilter(filter, "cub_postalcode");

    }
}

function zip_onchange() {

    var zip = Xrm.Page.getAttribute("cub_postalcodeid").getValue();
    var zipID = null;
    if (zip != null) {

        zipID = zip[0].id.replace(/[^a-z0-9A-Z-]/g, "");
        var clientURL = Xrm.Page.context.getClientUrl();
        var cubODataPath = clientURL + "/api/data/v8.0/";
        var cubODataReq = "cub_ziporpostalcodes(" + zipID + ")?$select=cub_code,cub_city,_cub_state_value,_cub_country_value";
        var req = new XMLHttpRequest()
        req.open("GET", encodeURI(cubODataPath + cubODataReq), true);
        req.setRequestHeader("Acccept", "application/json")
        req.setRequestHeader("Content-Type", "application/json; charset=utf=8");

        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 200) {
                    var reqResults = JSON.parse(this.response);
                    if (reqResults != null) {
                        //Update City
                        if (reqResults.cub_city != null) {

                            var cityval = Xrm.Page.getAttribute("cub_city").getValue();
                            if (cityval = null || cityval != reqResults.cub_city)
                            { Xrm.Page.getAttribute("cub_city").setValue(ToCamelCase(reqResults.cub_city)); }
                        }

                        //Update State
                        if (reqResults._cub_state_value != null) {

                            var custStateVal = Xrm.Page.getAttribute("cub_stateprovinceid").getValue();
                            if (custStateVal = null || custStateVal != reqResults._cub_state_value) {
                                var stateName = reqResults["_cub_state_value@OData.Community.Display.V1.FormattedValue"];
                                SetLookUp("cub_stateprovinceid", "cub_stateorprovince", reqResults._cub_state_value, stateName);
                            }
                        }

                        //Update Country
                        if (reqResults._cub_country_value != null) {
                            var custCntryVal = Xrm.Page.getAttribute("cub_countryid").getValue();
                            if (custCntryVal = null || custCntryVal != reqResults._cub_country_value) {
                                var countryName = reqResults["_cub_country_value@OData.Community.Display.V1.FormattedValue"];
                                SetLookUp("cub_countryid", "cub_country", reqResults._cub_country_value, countryName);
                            }
                        }

                        Sdk.filterZip();
                    }
                    else {
                        Xrm.Utility.alertDialog(this.statusText);
                    }
                }
            }
        };
        req.send();
    }
    else {
        //clear all fields set
        NullFieldValue("cub_countryid");
        NullFieldValue("cub_stateprovinceid");
        NullFieldValue("cub_city");

    }
}

function state_onchange() {
    // clear zip/postal code field
    NullFieldValue("cub_postalcodeid");
}

function CheckDuplicateAddress() {
    var returnValue = false;
    var websiteUrl = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL');
    var websiteAddressCheckUrl = GetGlobalsAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL.ADDRESS.CHECK');

    var accountId = Xrm.Page.getAttribute("cub_accountaddressid").getValue()[0].id;
    var street1 = Xrm.Page.getAttribute("cub_street1").getValue();
    var street2 = Xrm.Page.getAttribute("cub_street2").getValue();
    var city = Xrm.Page.getAttribute("cub_city").getValue();
    var stateId = Xrm.Page.getAttribute("cub_stateprovinceid").getValue()[0].id;
    var zipId = Xrm.Page.getAttribute("cub_postalcodeid").getValue()[0].id;
    var countryId = Xrm.Page.getAttribute("cub_countryid").getValue()[0].id;


    var params = {
        "accountId": accountId,
        "street1": street1,
        "street2": street2,
        "city": city,
        "stateId": stateId,
        "zipId": zipId,
        "countryId": countryId
    };

    //url: 'http://usdced-msdyn04:8080/CustomerAddress/CheckDuplicate'

    $.ajax({
        async: false,
        type: 'GET',
        data: params,
        dataType: 'JSON',
        cache: false,
        url: websiteUrl + websiteAddressCheckUrl,
        success: function (response, textStatus, jqXHR) {
            returnValue = ProcessDuplicate(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });

    return returnValue;
}

function ProcessDuplicate(data) {
    if (data.Success != true) {
        Xrm.Utility.alertDialog(data.Errors[0].errorMessage, onCloseCallback)
        return true;
    }

    return false;
}

function onCloseCallback() {
    parent.Mscrm.GlobalQuickCreate.GlobalQuickCreateBehavior.closeAllGlobalQuickCreateForms();
}


function DeleteAddress(FirstPrimaryItemId, PrimaryEntityTypeName) {

    var deletePromptMsg = 'This will mark the address as inactive. Do you want to continue?';
    var softDelete = GetGlobalsAttributeValue('AppInfo', 'SoftDeleteAddress');

    if (softDelete.toLowerCase() != 'yes') {
        deletePromptMsg = 'Delete this address?';
    }
    else {
        var statecode = Xrm.Page.getAttribute("statecode").getValue(); //0 = active, 1=inactive
        if (statecode == 1) {
            Xrm.Page.ui.setFormNotification("This record is already Inactive", 'ERROR', '1');
            return;
        }
    }

    var retval = window.confirm(deletePromptMsg);

    if (retval == true) {
        CallGlobalDeleteAction(FirstPrimaryItemId, PrimaryEntityTypeName, 'AddressForm', CallGlobalDeleteActionResponse);
    }
    else {
        OpenAccount(false);
    }
}
