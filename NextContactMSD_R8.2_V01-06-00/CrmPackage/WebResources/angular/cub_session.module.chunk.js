webpackJsonp(["cub_session.module"],{

/***/ "../../../../../src/app/cub-session/cub-session.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"hybrid\">\r\n    <!-- NOTE: Class \"session-in-web-resource\" is used when the session UI is used in a standalone web resource. -->\r\n    <div class=\"session\"\r\n         [class.session-in-web-resource]=\"isStandalone\"\r\n         [class.session-master-search]=\"!isStandalone\">\r\n        <!-- NOTE: These <input> checkboxes trigger the hiding/showing of the corresponding tiles in the session bar when \"View All\" / \"Close\" is clicked. Only applies to the IVR History, Messages, and Alerts. -->\r\n        <input type=\"checkbox\"\r\n               id=\"ivr-history\"\r\n               class=\"session-tiles-toggle\"\r\n               [(ngModel)]=\"ivrHistoryShowAll\" />\r\n        <input type=\"checkbox\"\r\n               id=\"messages\"\r\n               class=\"session-tiles-toggle\"\r\n               [(ngModel)]=\"messagesShowAll\" />\r\n        <input type=\"checkbox\"\r\n               id=\"alerts\"\r\n               class=\"session-tiles-toggle\"\r\n               [(ngModel)]=\"alertsShowAll\" />\r\n\r\n        <ul class=\"session-groups\">\r\n            <!--SESSION INFORMATION-->\r\n            <!-- CLASSES: Use class \"session-active\" on <li> if session is active. Use class \"session-inactive\" if there is no active session. -->\r\n            <li class=\"session-information\"\r\n                [class.session-active]=\"isActive\"\r\n                [class.session-inactive]=\"!isActive\">\r\n                <div class=\"session-group-header\">\r\n                    <p>Session Information</p>\r\n                </div>\r\n                <ul class=\"session-tiles\">\r\n                    <li>\r\n                        <div>\r\n                            <ng-container *ngIf=\"isActive; else isInactive\">\r\n                                <!--<p class=\"small\">-->\r\n                                    <!-- NOTE: The ID# length is currently 10 characters. If this changes, styling should be adjusted to account for more/less tile width. -->\r\n                                <!--<span class=\"session-id\">-->\r\n                                    <div class=\"dotted-link-container\">\r\n                                        <button type=\"button\" class=\"dotted-link\" (click)=\"showSessionDetailsClicked(session?.sessionId)\">\r\n                                            <div class=\"tooltip-left tooltip-width-M\">\r\n                                                <p class=\"small\">ID# {{session?.sessionNumber}}</p>\r\n                                                <span>Click to view Session Details</span>\r\n                                            </div>\r\n                                        </button>\r\n                                        <span></span> <!-- Blank span necessary for right-side spacing -->\r\n                                    </div>\r\n                                    <span class=\"session-date\">{{session?.startDateTime | momentDate | async}}</span>\r\n                               <!-- </span>-->\r\n                                <!--/p>-->\r\n                                <div class=\"buttons\">\r\n                                    <button type=\"button\"\r\n                                            class=\"button-secondary\"\r\n                                            (click)=\"endSession()\">\r\n                                        <p>End</p>\r\n                                    </button>\r\n                                    <!-- NOTE: Transferring the session with this <button> will allow the user to select a different user to pass along the active session. As of 11/16/2017, this functionality has not been defined, and there is no corresponding UI to display upon click of this button. -->\r\n                                    <button type=\"button\"\r\n                                            class=\"button-secondary\"\r\n                                            (click)=\"unimplemented($event)\">\r\n                                        <p>Transfer</p>\r\n                                    </button>\r\n                                </div>\r\n                            </ng-container>\r\n                            <ng-template #isInactive>\r\n                                <p class=\"small\">No active session</p>\r\n                                <div class=\"buttons\">\r\n                                    <button type=\"button\"\r\n                                            class=\"button-secondary\"\r\n                                            (click)=\"startSession()\">\r\n                                        <p>Start Session</p>\r\n                                    </button>\r\n                                </div>\r\n                            </ng-template>\r\n                        </div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <!--CUSTOMER-->\r\n            <!-- CLASSES: Use class \"different-contact\" on the <li> if the person the CSR is currently interacting with is NOT the primary contact/customer on the account. If the CSR is interacting with the primary contact, do not use this class. -->\r\n            <li *ngIf=\"session?.isRegistered || session?.subsystemAccountReference\"\r\n                class=\"customer\"\r\n                [class.different-contact]=\"session?.isRegistered && !session?.isPrimaryContact\">\r\n                <div class=\"session-group-header\">\r\n                    <p>Customer</p>\r\n                </div>\r\n                <ul class=\"session-tiles\">\r\n                    <li *ngIf=\"!session?.isRegistered\">\r\n                        <!-- NOTE: This <a> links to the create new customer page. -->\r\n                        <a href=\"javascript:void(0)\" (click)=\"registerClicked()\">\r\n                            <p>Unregistered</p>\r\n                            <p class=\"link\">Register</p>\r\n                        </a>\r\n                    </li>\r\n                    <li *ngIf=\"session?.isRegistered\">\r\n                        <!-- NOTE: This <a> links to the customer record. -->\r\n                        <a href=\"javascript:void(0)\" (click)=\"customerClicked()\">\r\n                            <!-- CLASSES: Use class \"verified\" on the <div> if the contact is verified. Use class \"unverified\" if the contact is not verified. -->\r\n                            <div *ngIf=\"session?.isPrimaryContact\"\r\n                                 class=\"verification-status\"\r\n                                 [class.verified]=\"session?.isVerified\"\r\n                                 [class.unverified]=\"!session?.isVerified\">\r\n                                <span>\r\n                                    <div class=\"icon-verified\">\r\n                                        <svg viewBox=\"0 0 14 12\">\r\n                                            <polyline points=\"2.75,5.75 5.5,8.5 11.25,2.75\" />\r\n                                        </svg>\r\n                                    </div>\r\n                                    <div class=\"icon-unverified\">\r\n                                        <svg viewBox=\"0 0 12 12\">\r\n                                            <line x1=\"2.75\" y1=\"9.25\" x2=\"9.25\" y2=\"2.75\" />\r\n                                            <line x1=\"9.25\" y1=\"9.25\" x2=\"2.75\" y2=\"2.75\" />\r\n                                        </svg>\r\n                                    </div>\r\n                                </span>\r\n                                <p>{{session?.isVerified ? 'Verified' : 'Unverified'}}</p>\r\n                            </div>\r\n                            <p class=\"large\">{{session?.customerFirstName}}</p>\r\n                            <p class=\"small\">{{session?.customerLastName}}</p>\r\n                        </a>\r\n                    </li>\r\n                    <li *ngIf=\"isActive && (session?.isPrimaryContact || !session?.isRegistered)\">\r\n                        <div>\r\n                            <p class=\"small\">Call From</p>\r\n                            <p class=\"nowrap\">312-555-1234</p>\r\n                        </div>\r\n                    </li>\r\n                    <li *ngIf=\"session?.isRegistered && !session?.isPrimaryContact\">\r\n                        <!-- NOTE: This <a> links to the contact record. -->\r\n                        <a href=\"javascript:void(0)\" (click)=\"contactClicked()\">\r\n                            <!-- CLASSES: Use class \"verified\" on the <div> if the contact is verified. Use class \"unverified\" if the contact is not verified. -->\r\n                            <div class=\"verification-status\"\r\n                                 [class.verified]=\"session?.isVerified\"\r\n                                 [class.unverified]=\"!session?.isVerified\">\r\n                                <span>\r\n                                    <div class=\"icon-verified\">\r\n                                        <svg viewBox=\"0 0 14 12\">\r\n                                            <polyline points=\"2.75,5.75 5.5,8.5 11.25,2.75\" />\r\n                                        </svg>\r\n                                    </div>\r\n                                    <div class=\"icon-unverified\">\r\n                                        <svg viewBox=\"0 0 12 12\">\r\n                                            <line x1=\"2.75\" y1=\"9.25\" x2=\"9.25\" y2=\"2.75\" />\r\n                                            <line x1=\"9.25\" y1=\"9.25\" x2=\"2.75\" y2=\"2.75\" />\r\n                                        </svg>\r\n                                    </div>\r\n                                </span>\r\n                                <p>{{session?.isVerified ? 'Verified' : 'Unverified'}}</p>\r\n                            </div>\r\n                            <ul class=\"label-values\">\r\n                                <li *ngIf=\"isActive\">\r\n                                    <div>\r\n                                        <p class=\"small\">Call From</p>\r\n                                    </div>\r\n                                    <div>\r\n                                        <p>312-555-1234</p>\r\n                                    </div>\r\n                                </li>\r\n                                <li>\r\n                                    <div>\r\n                                        <p class=\"small\">Contact</p>\r\n                                    </div>\r\n                                    <div>\r\n                                        <p>{{session?.contactFirstName}} {{session?.contactLastName}}</p>\r\n                                    </div>\r\n                                </li>\r\n                            </ul>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <!--LINKED ACCOUNTS-->\r\n            <li class=\"linked-accounts\" *ngIf=\"session?.isRegistered && oneAccountData?.linkedAccounts?.length\">\r\n                <div class=\"session-group-header\">\r\n                    <p>{{oneAccountData.linkedAccounts.length}} Linked Account{{oneAccountData.linkedAccounts.length > 1 ? 's' : ''}}</p>\r\n                    <!-- NOTE: This <a> links to the customer record where Subsystem Accounts / Tokens allows the user to view all. -->\r\n                    <a href=\"javascript:void(0)\"\r\n                       *ngIf=\"oneAccountData.linkedAccounts.length > 1\"\r\n                       (click)=\"customerClicked()\">View All</a>\r\n                </div>\r\n                <ul class=\"session-tiles session-tiles-even\">\r\n                    <li *ngFor=\"let account of oneAccountData.linkedAccounts\">\r\n                        <!-- NOTE: This <a> links to the corresponding Subsystem Account with the token and corresponding passes displayed (ideally). -->\r\n                        <a href=\"javascript:void(0)\" (click)=\"subsystemAccountClicked(account.subsystemInfo.subsystem, account.subsystemAccountReference)\">\r\n                            <p>{{account.nickname || account.displayToken?.subsystemTokenTypeDescription + ' ' + (account.displayToken | travelTokenId)}}</p>\r\n                            <p class=\"small\">{{account.subsystemInfo?.subsystem}} &ndash; {{account.subsystemAccountReference}}</p>\r\n                            <p class=\"small\">Last activity {{account.displayToken?.lastUseDTM | momentDate:'MM/DD/YYYY' | async}}</p>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <!--UNREGISTERED ACCOCUNT-->\r\n            <li class=\"linked-accounts\" *ngIf=\"!session?.isRegistered && !!subsystemData?.displayToken\">\r\n                <div class=\"session-group-header\">\r\n                    <p>Subsystem Account</p>\r\n                </div>\r\n                <ul class=\"session-tiles session-tiles-even\">\r\n                    <li>\r\n                        <!-- NOTE: This <a> links to the corresponding Subsystem Account with the token and corresponding passes displayed (ideally). -->\r\n                        <a href=\"javascript:void(0)\" (click)=\"subsystemAccountClicked(session?.subsystem, session?.subsystemAccountReference)\">\r\n                            <p>{{subsystemData.displayToken?.subsystemTokenTypeDescription}} {{subsystemData.displayToken | travelTokenId}}</p>\r\n                            <p class=\"small\">{{session?.subsystem}} &ndash; {{session?.subsystemAccountReference}}</p>\r\n                            <p class=\"small\">Last activity {{subsystemData.displayToken?.tokenLastUsageDetails?.transactionDateTime | momentDate:'MM/DD/YYYY' | async}}</p>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <!--IVR-->\r\n            <!-- CLASSES: Depending on the number of IVR options selected, use one of the following classes on the <li>:\r\n                • \"tiles-1\" = 0 or 1 option selected.\r\n                • \"tiles-2\" = 2 options selected.\r\n                • \"tiles-3\" = 3 options selected.\r\n                • \"tiles-4-plus\" = 4 or more options selected.\r\n            -->\r\n            <li class=\"ivr-history tiles-4-plus\" *ngIf=\"false\">\r\n                <div class=\"session-group-header\">\r\n                    <p>IVR History</p>\r\n                    <!-- NOTE: Change \"View All\" text to \"Close\" when corresponding <input> checkbox is checked. -->\r\n                    <label for=\"ivr-history\">{{ivrHistoryShowAll ? 'Close' : 'View All'}}</label>\r\n                </div>\r\n                <ul class=\"session-tiles session-tiles-even session-tiles-fixed\">\r\n                    <li>\r\n                        <div>\r\n                            <p class=\"small\">1<sup>st</sup> Selection</p>\r\n                            <p>This is the first option selected by the caller.</p>\r\n                        </div>\r\n                    </li>\r\n                    <li>\r\n                        <div>\r\n                            <p class=\"small\">2<sup>nd</sup> Selection</p>\r\n                            <p>This is the second option selected by the caller.</p>\r\n                        </div>\r\n                    </li>\r\n                    <li>\r\n                        <div>\r\n                            <p class=\"small\">3<sup>rd</sup> Selection</p>\r\n                            <p>Third option selected.</p>\r\n                        </div>\r\n                    </li>\r\n                    <li>\r\n                        <!-- NOTE: By default, only the last selection tile displays. The other tiles are revealed when the user clicks the corresponding \"View All\" link. -->\r\n                        <div>\r\n                            <!-- NOTE: If this is the ONLY selection the user made, or if the user selected no options, change \"Last Selection\" text to \"Option Selected\". -->\r\n                            <p class=\"small\">Last Selection</p>\r\n                            <!-- NOTE: If the user did not select any options, use the text \"None\" in the <p>. -->\r\n                            <p>This is the most recent option that was selected by the caller.</p>\r\n                        </div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <!--MESSAGES-->\r\n            <!-- CLASSES: Depending on the number of messages, use one of the following classes on the <li>:\r\n                • \"tiles-1\" = 0 or 1 option selected.\r\n                • \"tiles-2\" = 2 options selected.\r\n                • \"tiles-3\" = 3 options selected.\r\n                • \"tiles-4-plus\" = 4 or more options selected.\r\n            -->\r\n            <li class=\"messages tiles-4-plus\" *ngIf=\"false\">\r\n                <div class=\"session-group-header\">\r\n                    <p>5 Messages</p>\r\n                    <!-- NOTE: Change \"View All\" text to \"Close\" when corresponding <input> checkbox is checked. -->\r\n                    <label for=\"messages\">{{messagesShowAll ? 'Close' : 'View All'}}</label>\r\n                </div>\r\n                <ul class=\"session-tiles session-tiles-even session-tiles-fixed\">\r\n                    <li>\r\n                        <!-- NOTE: This <a> links to the corresponding activity record. -->\r\n                        <a href=\"javascript:void(0)\" (click)=\"unimplemented($event)\">\r\n                            <p class=\"small\">10/17/2017</p>\r\n                            <p>This is a shorter message.</p>\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\" (click)=\"unimplemented($event)\">\r\n                            <p class=\"small\">10/17/2017</p>\r\n                            <p>This is a longer message that wraps to the next line and eventually disappears from view because it's ridiculously long.</p>\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\" (click)=\"unimplemented($event)\">\r\n                            <p class=\"small\">9/7/2017</p>\r\n                            <p>This is a message. Read it or else. Or else what? Exactly.</p>\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\" (click)=\"unimplemented($event)\">\r\n                            <p class=\"small\">9/1/2017</p>\r\n                            <p>Short message here.</p>\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\" (click)=\"unimplemented($event)\">\r\n                            <p class=\"small\">8/30/2017</p>\r\n                            <p>This is the message. It goes here. I love it because I can read it.</p>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <!--ALERTS-->\r\n            <!-- CLASSES: Depending on the number of alerts, use one of the following classes on the <li>:\r\n                • \"tiles-1\" = 0 or 1 option selected.\r\n                • \"tiles-2\" = 2 options selected.\r\n                • \"tiles-3\" = 3 options selected.\r\n                • \"tiles-4-plus\" = 4 or more options selected.\r\n            -->\r\n            <li class=\"alerts tiles-4-plus\" *ngIf=\"false\">\r\n                <div class=\"session-group-header\">\r\n                    <p>4 Alerts</p>\r\n                    <!-- NOTE: Change \"View All\" text to \"Close\" when corresponding <input> checkbox is checked. -->\r\n                    <label for=\"alerts\">{{alertsShowAll ? 'Close' : 'View All'}}</label>\r\n                </div>\r\n                <ul class=\"session-tiles session-tiles-even session-tiles-fixed\">\r\n                    <li>\r\n                        <!-- NOTE: This <a> links to the corresponding activity record. -->\r\n                        <a href=\"javascript:void(0)\" (click)=\"unimplemented($event)\">\r\n                            <p class=\"small\">10/27/2017</p>\r\n                            <p>Funding source Visa xxxx-3210 has expired.</p>\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\" (click)=\"unimplemented($event)\">\r\n                            <p class=\"small\">10/24/2017</p>\r\n                            <p>This is the title of another alert.</p>\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\" (click)=\"unimplemented($event)\">\r\n                            <p class=\"small\">10/10/2017</p>\r\n                            <p>This is an alert. Read it or else. Or else what? Exactly.</p>\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\" (click)=\"unimplemented($event)\">\r\n                            <p class=\"small\">10/9/2017</p>\r\n                            <p>Short alert here.</p>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n        </ul>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/cub-session/cub-session.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Cub_SessionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_cub_data_service__ = __webpack_require__("../../../../../src/app/services/cub_data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_cub_session_service__ = __webpack_require__("../../../../../src/app/services/cub-session.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_cub_oneaccount_service__ = __webpack_require__("../../../../../src/app/services/cub_oneaccount.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_cub_subsystem_service__ = __webpack_require__("../../../../../src/app/services/cub_subsystem.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_cub_transitaccount_service__ = __webpack_require__("../../../../../src/app/services/cub_transitaccount.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_loading_screen_app_loading_screen_service__ = __webpack_require__("../../../../../src/app/app-loading-screen/app-loading-screen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__model__ = __webpack_require__("../../../../../src/app/model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var Cub_SessionComponent = (function () {
    function Cub_SessionComponent(_cub_dataService, _cub_sessionService, _cub_oneAccountService, _cub_subsystemService, _cub_transitAccountService, _loadingScreen, _zone) {
        this._cub_dataService = _cub_dataService;
        this._cub_sessionService = _cub_sessionService;
        this._cub_oneAccountService = _cub_oneAccountService;
        this._cub_subsystemService = _cub_subsystemService;
        this._cub_transitAccountService = _cub_transitAccountService;
        this._loadingScreen = _loadingScreen;
        this._zone = _zone;
        this.isStandalone = true; // false if the session bar is loaded within a component like Master Search
        this.ivrHistoryShowAll = false;
        this.messagesShowAll = false;
        this.alertsShowAll = false;
        this.sessionOutOfContextMessage = 'This page is outside the context of the current session. Click OK to start a new session. Click Cancel to stay on this page or return to the previous page without ending the current session.';
        parent['Cubic'] = parent['Cubic'] || {};
        parent['Cubic'].session = {
            component: this,
            zone: this._zone
        };
    }
    Object.defineProperty(Cub_SessionComponent.prototype, "isActive", {
        get: function () { return this._cub_sessionService.isActive; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Cub_SessionComponent.prototype, "session", {
        get: function () { return this._cub_sessionService.session; },
        set: function (s) { this._cub_sessionService.session = s; },
        enumerable: true,
        configurable: true
    });
    Cub_SessionComponent.prototype.ngOnInit = function () {
        if (!parent.Xrm.Page.data || !parent.Xrm.Page.data.entity) {
            this.isStandalone = false;
            this.finishInit(false);
            return;
        }
        this.isStandalone = true;
        this.currentEntityName = parent.Xrm.Page.data.entity.getEntityName();
        this.currentRecordId = parent.Xrm.Page.data.entity.getId();
        if (this.checkSessionContextIsRelevant()) {
            this.finishInit(true);
            return;
        }
        // If the current record is unrelated to the session context, prompt
        // the user to end the session and begin a new one, otherwise finish
        // initialization as usual.
        var message = this.sessionOutOfContextMessage;
        parent.Xrm.Utility.confirmDialog(message, function () { return parent['Cubic'].session.zone.run(function () {
            // This callback has no reference to `this` so we use comp
            var comp = parent['Cubic'].session.component;
            var done = comp.tryAddLoadingMessage('Ending session');
            comp._cub_sessionService.endSession()
                .then(function () { return comp.startSession(); })
                .then(function () { return comp.authenticateContact(); })
                .catch(function (err) { return comp._cub_dataService.onApplicationError(err); })
                .then(function () { return done(); });
        }); }, function () { return parent['Cubic'].session.zone.run(function () {
            return parent['Cubic'].session.component.finishInit(false);
        }); });
    };
    Cub_SessionComponent.prototype.ngOnDestroy = function () {
        try {
            parent['Cubic'].session = null;
        }
        catch (e) { }
    };
    /**
     * Refresh the session bar
     * @param forceRefresh if true, make HTTP requests for data, otherwise check cache first
     */
    Cub_SessionComponent.prototype.refresh = function (forceRefresh) {
        var _this = this;
        var done = this.tryAddLoadingMessage('Refreshing');
        var getSession = forceRefresh
            ? this._cub_sessionService.refreshSession()
            : this._cub_sessionService.getCachedSession();
        getSession
            .then(function () { return _this.extractFormData(); })
            .then(function () { return _this.loadData(forceRefresh); })
            .catch(function (err) { return _this._cub_dataService.onApplicationError(err); })
            .then(function () { return done(); });
    };
    /**
     * Determine the current record. If no active session, gather as much data
     * as possible from the record's form.
     */
    Cub_SessionComponent.prototype.extractFormData = function () {
        if (!parent.Xrm.Page.data || !parent.Xrm.Page.data.entity) {
            this.isStandalone = false;
            return;
        }
        this.isStandalone = true;
        this.currentEntityName = parent.Xrm.Page.data.entity.getEntityName();
        this.currentRecordId = parent.Xrm.Page.data.entity.getId();
        if (this.isActive) {
            return;
        }
        if (this.currentEntityName === 'account') {
            this.session.customerId = this.currentRecordId;
            var fullName = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
            this.session.customerFirstName = fullName.trim().split(' ')[0].trim();
            this.session.customerLastName = fullName.trim().split(' ')[1].trim();
            this.session.isRegistered = true;
            this.session.isPrimaryContact = true;
        }
        else if (this.currentEntityName === 'contact') {
            this.session.contactFirstName = parent.Xrm.Page.getAttribute('firstname').getValue();
            this.session.contactLastName = parent.Xrm.Page.getAttribute('lastname').getValue();
            this.session.contactId = this.currentRecordId;
            this.session.customerId = parent.Xrm.Page.getAttribute('parentcustomerid').getValue()[0].id;
            var fullName = parent.Xrm.Page.getAttribute('parentcustomerid').getValue()[0].name;
            this.session.customerFirstName = fullName.trim().split(' ')[0].trim();
            this.session.customerLastName = fullName.trim().split(' ')[1].trim();
            this.session.isRegistered = true;
            this.session.isPrimaryContact = false;
        }
        else if (this.currentEntityName === 'cub_transitaccount') {
            this.session.transitAccountId = this.currentRecordId;
            if (parent.Xrm.Page.getAttribute('cub_customer').getValue()) {
                this.session.customerId = parent.Xrm.Page.getAttribute('cub_customer').getValue()[0].id;
                var fullName = parent.Xrm.Page.getAttribute('cub_customer').getValue()[0].name;
                this.session.customerFirstName = fullName.trim().split(' ')[0].trim();
                this.session.customerLastName = fullName.trim().split(' ')[1].trim();
                this.session.isRegistered = true;
                this.session.isPrimaryContact = true;
            }
            else {
                this.session.subsystem = parent.Xrm.Page.getAttribute('cub_subsystemid').getValue();
                this.session.subsystemAccountReference = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
            }
        }
    };
    /**
     * If the session bar has either a registered customer or an unregistered
     * subsystem account in context, populate the session bar with its summary.
     * @param forceRefresh if true, make HTTP requests for data, otherwise check cache first
     */
    Cub_SessionComponent.prototype.loadData = function (forceRefresh) {
        var _this = this;
        var loadMessage = forceRefresh ? 'Refreshing' : 'Loading';
        loadMessage += this.session.isRegistered ? ' OneAccount' : ' Subsystem';
        loadMessage += ' data';
        var done = this.tryAddLoadingMessage(loadMessage);
        // If the session context is a registered customer or unregistered subsystem account,
        // we request & process either OneAccount summary or Subsystem status, respectively.
        var queryPromise;
        if (this.session.isRegistered && this.session.customerId) {
            queryPromise = forceRefresh
                ? this._cub_oneAccountService.refreshOneAccountSummary(this.session.customerId)
                : this._cub_oneAccountService.getCachedOneAccountSummary(this.session.customerId);
            queryPromise = queryPromise.then(function (data) { return _this.extractOneAccountData(data); });
        }
        else if (!this.session.isRegistered && this.session.subsystem && this.session.subsystemAccountReference) {
            queryPromise = forceRefresh
                ? this._cub_subsystemService.refreshSubsystemStatus(this.session.subsystem, this.session.subsystemAccountReference)
                : this._cub_subsystemService.getCachedSubsystemStatus(this.session.subsystem, this.session.subsystemAccountReference);
            queryPromise = queryPromise.then(function (data) { return _this.extractSubsystemData(data); });
        }
        else {
            done();
            return;
        }
        queryPromise
            .catch(function (err) { return _this._cub_dataService.onApplicationError(err); })
            .then(function () { return done(); });
    };
    /**
     * Assign a display token for each Linked Account, and sort Linked Accounts
     * @param data
     */
    Cub_SessionComponent.prototype.extractOneAccountData = function (data) {
        this.oneAccountData = data;
        if (!this.oneAccountData.linkedAccounts) {
            return;
        }
        var len = this.oneAccountData.linkedAccounts.length;
        for (var i = 0; i < len; i++) {
            var tokens = this.oneAccountData.linkedAccounts[i].subsystemAccountDetailedInfo.tokens;
            this.oneAccountData.linkedAccounts[i].displayToken = this._cub_oneAccountService.getDisplayToken(tokens);
        }
        // Sort accounts by status (Active, Closed, Suspended, Terminated) and
        // by most-recently used token.
        this.oneAccountData.linkedAccounts = __WEBPACK_IMPORTED_MODULE_8_lodash__["orderBy"](this.oneAccountData.linkedAccounts, [function (a) { return a.subsystemAccountDetailedInfo.accountStatus; }, function (a) { return a.displayToken.tokenInfo.lastUseDTM; }], ['asc', 'desc']);
    };
    /**
     * Assign a display token and determine last usage details
     * @param data
     */
    Cub_SessionComponent.prototype.extractSubsystemData = function (data) {
        this.subsystemData = data;
        if (!this.subsystemData.accountLastUsageDetails && !this.subsystemData.tokens) {
            return;
        }
        this.subsystemData.displayToken = this._cub_oneAccountService.getDisplayToken(this.subsystemData.tokens);
        // The subsystem account and its related tokens all have last usage details.
        // Display whichever is most recent.
        if (this.subsystemData.accountLastUsageDetails && this.subsystemData.displayToken) {
            var tokenLastUse = this.subsystemData.displayToken.tokenLastUsageDetails.transactionDateTime;
            var accountLastUse = this.subsystemData.accountLastUsageDetails.transactionDateTime;
            if (__WEBPACK_IMPORTED_MODULE_9_moment__(accountLastUse).isAfter(tokenLastUse)) {
                this.subsystemData.displayToken.tokenLastUsageDetails = this.subsystemData.accountLastUsageDetails;
            }
        }
    };
    /**
     * TODO: remove once everything is implemented.
     * Placeholder method to use until something is implemented
     * @param event
     */
    Cub_SessionComponent.prototype.unimplemented = function (event) {
        event.preventDefault();
        alert('This function is unimplemented');
    };
    /**
     * Create a session record and load additional data based on its context
     */
    Cub_SessionComponent.prototype.startSession = function () {
        var _this = this;
        var done = this.tryAddLoadingMessage('Starting new session');
        return this._cub_sessionService.createSession(this.currentEntityName, this.currentRecordId)
            .then(function () { return _this.loadData(false); })
            .catch(function (err) { return _this._cub_dataService.onApplicationError(err); })
            .then(function () { return done(); });
    };
    /**
     * End a session and update the session bar context to match the current page
     */
    Cub_SessionComponent.prototype.endSession = function () {
        var _this = this;
        var done = this.tryAddLoadingMessage('Ending session');
        this._cub_sessionService.endSession()
            .then(function () {
            _this.extractFormData();
            _this.loadData(true);
        })
            .catch(function (err) { return _this._cub_dataService.onApplicationError(err); })
            .then(function () { return done(); });
    };
    /**
     * Navigate to the customer record if not already there
     */
    Cub_SessionComponent.prototype.customerClicked = function () {
        if (!this.session.customerId) {
            this._cub_dataService.onApplicationError('Cannot navigate to customer record, no customer ID found!');
            return;
        }
        if (this._cub_dataService.guidsAreEqual(this.session.customerId, this.currentRecordId)) {
            return;
        }
        this._cub_dataService.openMsdForm('account', this.session.customerId);
    };
    /**
     * Navigate to the contact record if not already there
     */
    Cub_SessionComponent.prototype.contactClicked = function () {
        if (!this.session.contactId) {
            this._cub_dataService.onApplicationError('Cannot navigate to contact record, no contact ID found!');
            return;
        }
        if (this._cub_dataService.guidsAreEqual(this.session.contactId, this.currentRecordId)) {
            return;
        }
        this._cub_dataService.openMsdForm('contact', this.session.contactId);
    };
    /**
     * Navigate to the subsystem account record if not already there
     * @param subsystem
     * @param subsystemAccountReference
     */
    Cub_SessionComponent.prototype.subsystemAccountClicked = function (subsystem, subsystemAccountReference) {
        var _this = this;
        if (parent.Xrm.Page.data && this.currentEntityName === __WEBPACK_IMPORTED_MODULE_7__model__["cub_TransitAccountLogicalName"]) {
            var currentAccountRef = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
            var currentSubsystem = parent.Xrm.Page.getAttribute('cub_subsystemid').getValue();
            if (currentAccountRef === subsystemAccountReference
                && currentSubsystem === subsystem) {
                return;
            }
        }
        var done = this.tryAddLoadingMessage('Retrieving account');
        this._cub_transitAccountService.getTransitAccount(subsystemAccountReference, subsystem, this.session.customerId)
            .then(function (transitAccount) {
            if (transitAccount && !_this._cub_dataService.guidsAreEqual(transitAccount.Id, _this.currentRecordId)) {
                _this._cub_dataService.openMsdForm(__WEBPACK_IMPORTED_MODULE_7__model__["cub_TransitAccountLogicalName"], transitAccount.Id);
            }
        })
            .catch(function (err) { return _this._cub_dataService.onApplicationError(err); })
            .then(function () { return done(); });
    };
    /**
     * Navigate to Customer Registration if not already there or on Master Search
     */
    Cub_SessionComponent.prototype.registerClicked = function () {
        if (!this.isStandalone) {
            return;
        }
        var data = encodeURIComponent(JSON.stringify({
            route: 'CustomerRegistration',
            queryParams: encodeURIComponent([
                'subsystem=', this.session.subsystem,
                '&subsystemAccountReference=', this.session.subsystemAccountReference
            ].join(''))
        }));
        var url = window.location.origin + window.location.pathname + '?pagemode=iframe&Data=' + data;
        parent.location.href = url;
    };
    /**
     * Determine whether the session's context is relevant to the current MSD
     * page by checking the session and related records against the displaying record.
     * @returns false if the current page is unrelated to the current session, true otherwise.
     */
    Cub_SessionComponent.prototype.checkSessionContextIsRelevant = function () {
        // Return early if within another component (like Master Search),
        // there is no active session, or the active session is not yet associated
        // with a registered customer or unregistered subsystem account.
        if (!this.isStandalone
            || !this.isActive
            || (!this.session.customerId
                && !this.session.transitAccountId)) {
            return true;
        }
        if (this.currentEntityName === 'account') {
            return this.session.isRegistered
                && this._cub_dataService.guidsAreEqual(this.session.customerId, this.currentRecordId);
        }
        else if (this.currentEntityName === 'contact') {
            if (!this.session.isRegistered) {
                return false;
            }
            var customerControl = parent.Xrm.Page.getAttribute('parentcustomerid');
            if (!customerControl || !customerControl.getValue()) {
                return false;
            }
            var customerId = customerControl.getValue()[0].id;
            return this._cub_dataService.guidsAreEqual(this.session.customerId, customerId);
        }
        else if (this.currentEntityName = 'cub_transitaccount') {
            if (!this.session.isRegistered) {
                return this._cub_dataService.guidsAreEqual(this.session.transitAccountId, this.currentRecordId);
            }
            else {
                var customerControl = parent.Xrm.Page.getAttribute('cub_customer');
                if (!customerControl || !customerControl.getValue()) {
                    return false;
                }
                var customerId = customerControl.getValue()[0].id;
                return this._cub_dataService.guidsAreEqual(this.session.customerId, customerId);
            }
        }
        return true;
    };
    /**
     * If there is an active session, load data from session context.
     * If there is not an active session, try to start a new session
     * with the current record as context.
     * @param shouldAuthenticate if true, attempt to trigger contact authentication process
     */
    Cub_SessionComponent.prototype.finishInit = function (shouldAuthenticate) {
        var _this = this;
        if (this.isActive) {
            this.loadData(false);
            if (shouldAuthenticate) {
                this.authenticateContact();
            }
            return;
        }
        if (this.currentEntityName === 'account'
            || this.currentEntityName === 'contact'
            || this.currentEntityName === 'cub_transitaccount') {
            this.startSession().then(function () {
                if (shouldAuthenticate) {
                    _this.authenticateContact();
                }
            });
        }
    };
    /**
     * If the session bar is attached to another component like Master Search,
     * don't display any loading messages to reduce screen clutter.
     * @param message
     * @returns a function to remove the loading message or a no-op
     */
    Cub_SessionComponent.prototype.tryAddLoadingMessage = function (message) {
        if (!this.isStandalone) {
            return function () { }; // no-op
        }
        else {
            return this._loadingScreen.add(message);
        }
    };
    /**
     * Update the current session.
     * Invoked by MSD form scripts for contact and account.
     * @param session an object containing any MSD attributes to be updated
     */
    Cub_SessionComponent.prototype.update = function (session) {
        var _this = this;
        session.Id = this.session.sessionId;
        var done = this.tryAddLoadingMessage('Updating');
        this._cub_sessionService.updateSession(session)
            .catch(function (err) { return _this._cub_dataService.onApplicationError(err); })
            .then(function () { return done(); });
    };
    /**
     * Call a form script function to check if the current contact needs to be verified.
     */
    Cub_SessionComponent.prototype.authenticateContact = function () {
        if (this.currentEntityName === 'account') {
            parent['Cubic'].account.authenticateContact();
        }
        else if (this.currentEntityName === 'contact') {
            parent['Cubic'].contact.authenticateContact();
        }
    };
    Cub_SessionComponent.prototype.showSessionDetailsClicked = function (sessionId) {
        var dialogOptions = new parent.Xrm['DialogOptions']();
        var data = encodeURIComponent(JSON.stringify({
            route: 'SessionDetails',
            queryParams: [
                'sessionId=', sessionId
                //'sessionId=DAEB341B-292D-E811-80F9-005056814569'
            ].join('')
        }));
        var modalUrl = window.location.origin + window.location.pathname + '?Data=' + data;
        dialogOptions.width = 1280;
        dialogOptions.height = 780;
        parent.Xrm['Internal'].openDialog(modalUrl, dialogOptions, null, null, function (response) {
            if (response == null) {
                return;
            }
            try {
                parent['Cubic'].account.subsystemAccounts.zone.run(function () {
                    parent['Cubic'].account.subsystemAccounts.component.onModalClose(response);
                });
            }
            catch (e) {
                console.warn('Unable to handle modal close callback', e);
            }
        });
    };
    return Cub_SessionComponent;
}());
Cub_SessionComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'cub-session',
        template: __webpack_require__("../../../../../src/app/cub-session/cub-session.component.html"),
        providers: [
            __WEBPACK_IMPORTED_MODULE_3__services_cub_oneaccount_service__["a" /* Cub_OneAccountService */],
            __WEBPACK_IMPORTED_MODULE_4__services_cub_subsystem_service__["a" /* Cub_SubsystemService */],
            __WEBPACK_IMPORTED_MODULE_5__services_cub_transitaccount_service__["a" /* Cub_TransitAccountService */]
        ]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_cub_data_service__["a" /* Cub_DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_cub_data_service__["a" /* Cub_DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_cub_session_service__["a" /* Cub_SessionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_cub_session_service__["a" /* Cub_SessionService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_cub_oneaccount_service__["a" /* Cub_OneAccountService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_cub_oneaccount_service__["a" /* Cub_OneAccountService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_cub_subsystem_service__["a" /* Cub_SubsystemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_cub_subsystem_service__["a" /* Cub_SubsystemService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_cub_transitaccount_service__["a" /* Cub_TransitAccountService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_cub_transitaccount_service__["a" /* Cub_TransitAccountService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__app_loading_screen_app_loading_screen_service__["a" /* AppLoadingScreenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__app_loading_screen_app_loading_screen_service__["a" /* AppLoadingScreenService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _g || Object])
], Cub_SessionComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=cub-session.component.js.map

/***/ }),

/***/ "../../../../../src/app/cub-session/cub_session.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cub_SessionModule", function() { return Cub_SessionModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cub_session_component__ = __webpack_require__("../../../../../src/app/cub-session/cub-session.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_moment_date_pipe__ = __webpack_require__("../../../../../src/app/pipes/moment-date.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pipes_travel_token_id_pipe__ = __webpack_require__("../../../../../src/app/pipes/travel-token-id.pipe.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var Cub_SessionModule = (function () {
    function Cub_SessionModule() {
    }
    return Cub_SessionModule;
}());
Cub_SessionModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["RouterModule"].forChild([
                { path: '', component: __WEBPACK_IMPORTED_MODULE_4__cub_session_component__["a" /* Cub_SessionComponent */] }
            ]),
            __WEBPACK_IMPORTED_MODULE_5__pipes_moment_date_pipe__["a" /* MomentDatePipeModule */],
            __WEBPACK_IMPORTED_MODULE_6__pipes_travel_token_id_pipe__["a" /* TravelTokenIdPipeModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_4__cub_session_component__["a" /* Cub_SessionComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_4__cub_session_component__["a" /* Cub_SessionComponent */]]
    })
], Cub_SessionModule);

//# sourceMappingURL=cub_session.module.js.map

/***/ })

});
//# sourceMappingURL=cub_session.module.chunk.js.map