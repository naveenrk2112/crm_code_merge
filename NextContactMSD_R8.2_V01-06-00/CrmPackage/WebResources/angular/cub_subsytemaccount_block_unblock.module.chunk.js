webpackJsonp(["cub_subsytemaccount_block_unblock.module"],{

/***/ "../../../../../src/app/cub-subsystem-acct-block-unblock/cub_subsytemaccount_block_unblock.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"requestBody?.block && !showSaveModal\" class=\"hybrid modal-small unsupported\">\r\n    <ul class=\"window\">\r\n        <li class=\"header center\">\r\n            <h1>Block Transit Account</h1>\r\n        </li>\r\n        <li class=\"body\">\r\n            <div>\r\n                <div class=\"form-fields-vertical label-width-20\">\r\n                    <ul>\r\n                        <li>\r\n                            <label for=\"{{reasonCodeControl.id}}\">Reason</label>\r\n                        </li>\r\n                        <li cub-dropdown [control]=\"reasonCodeControl\" (onChange)=\"onReasonCodeChanged()\">\r\n                        </li>\r\n                    </ul>\r\n                    <ul>\r\n                        <li>\r\n                            <label for=\"Notes\">Notes</label>\r\n                        </li>\r\n                        <li>\r\n                            <ul class=\"form-fields-siblings\">\r\n                                <li  class=\"form-field-width-100\">\r\n                                    <div class=\"form-field-textarea show-error-message\"  [class.error]=\"showNotesError\">\r\n                                        <textarea [(ngModel)]=\"notes\" [id]=\"notes\" [rows]=\"5\" [required]=\"reasonCodeSelected?.notesRequired\">{{reasonCodeControl?.notesRequired}}</textarea>\r\n                                        <i></i>\r\n                                        <span *ngIf=\"showNotesError\" class=\"error-message\">\r\n                                            <p>Notes are required.</p>\r\n                                        </span>\r\n                                    </div>\r\n                                </li>\r\n                            </ul>\r\n                        </li>\r\n                    </ul>\r\n                </div>                    \r\n            </div>\r\n        </li>\r\n        <li class=\"footer\">\r\n            <ul class=\"buttons\">\r\n                <li>\r\n                    <button type=\"button\" class=\"button-primary\" (click)=\"submitBlockClicked()\">\r\n                        <p>Block Account</p>\r\n                    </button>\r\n                </li>\r\n                <li>\r\n                    <button type=\"button\" class=\"button-cancel\" (click)=\"cancelClicked()\">\r\n                        <p>Cancel</p>\r\n                    </button>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n    </ul>\r\n</div>\r\n<div *ngIf=\"!requestBody?.block && !showSaveModal\" class=\"hybrid modal-small unsupported\">\r\n    <ul class=\"window\">\r\n        <li class=\"header center\">\r\n            <h1>Unblock Transit Account</h1>\r\n        </li>\r\n        <li class=\"body\">\r\n            <div>\r\n                <div class=\"form-fields-vertical label-width-20\">\r\n                    <ul>\r\n                        <li>\r\n                            <label for=\"{{reasonCodeControl.id}}\">Reason</label>\r\n                        </li>\r\n                        <li cub-dropdown [control]=\"reasonCodeControl\" (onChange)=\"onReasonCodeChanged()\">\r\n                        </li>\r\n                    </ul>\r\n                    <ul>\r\n                        <li>\r\n                            <label for=\"Notes\">Notes</label>\r\n                        </li>\r\n                        <li>\r\n                            <ul class=\"form-fields-siblings\">\r\n                                <li class=\"form-field-width-100\">\r\n                                    <div class=\"form-field-textarea show-error-message\" [class.error]=\"showNotesError\" >\r\n                                        <textarea [(ngModel)]=\"notes\" [id]=\"notes\" [rows]=\"5\" [required]=\"reasonCodeSelected?.notesRequired\">{{reasonCodeControl?.notesRequired}}</textarea>\r\n                                        <i></i>\r\n                                        <span *ngIf=\"showNotesError\" class=\"error-message\">\r\n                                            <p>Notes are required.</p>\r\n                                        </span>\r\n                                    </div>\r\n                                </li>\r\n                            </ul>\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n            </div>\r\n        </li>\r\n        <li class=\"footer\">\r\n            <ul class=\"buttons\">\r\n                <li>\r\n                    <button type=\"button\" class=\"button-primary\" (click)=\"submitUnBlockClicked()\">\r\n                        <p>Unblock Account</p>\r\n                    </button>\r\n                </li>\r\n                <li>\r\n                    <button type=\"button\" class=\"button-cancel\" (click)=\"cancelClicked()\">\r\n                        <p>Cancel</p>\r\n                    </button>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n    </ul>\r\n</div>\r\n\r\n<!-- Add class \"visible\" to modal <div> to display modal window. -->\r\n<div *ngIf=\"showSaveModal\" class=\"hybrid modal-small unsupported\">\r\n    <ul class=\"window\">\r\n        <li class=\"icon\">\r\n            <div class=\"icon-confirmation-check\">\r\n                <svg viewBox=\"0 0 51 51\">\r\n                    <circle cx=\"25.5\" cy=\"25.5\" r=\"24.5\" />\r\n                    <polyline points=\"13,25.5 21.25,33.75 38,17\" />\r\n                </svg>\r\n            </div>\r\n        </li>\r\n        <li class=\"body center\">\r\n            <div>\r\n                <h1>Transit Account {{requestBody.block ? 'Blocked' : 'Unblocked'}}</h1>\r\n                <p> Account ID: {{requestBody.subsystemAccount}}</p>\r\n            </div>\r\n        </li>\r\n        <li class=\"footer\">\r\n            <ul class=\"buttons\">\r\n                <li>\r\n                    <button type=\"button\" class=\"button-primary\" (click)=\"okClicked()\">\r\n                        <p>OK</p>\r\n                    </button>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n    </ul>\r\n</div>\r\n\r\n<!-- Add class \"visible\" to modal <div> to display modal window. -->\r\n<div *ngIf=\"showErrorsModal\" class=\"hybrid modal-small visible\">\r\n    <ul class=\"window modal-width-S\">\r\n        <li class=\"icon\">\r\n            <div class=\"icon-warning-hybrid\">\r\n                <svg viewBox=\"0 0 45 42\">\r\n                    <path d=\"M22.5,2.009c-1.467,0-2.891,1.102-4.011,3.102 L3.006,32.766c-1.123,2.006-1.313,3.842-0.534,5.17C3.252,39.267,4.952,40,7.258,40h30.485c2.306,0,4.005-0.733,4.785-2.064 c0.779-1.329,0.59-3.165-0.533-5.17L26.511,5.111C25.391,3.11,23.967,2.009,22.5,2.009z\" />\r\n                    <line x1=\"22.5\" y1=\"12.5\" x2=\"22.5\" y2=\"24.5\" />\r\n                    <circle cx=\"22.5\" cy=\"31.5\" r=\"2.5\" />\r\n                </svg>\r\n            </div>\r\n        </li>\r\n        <li class=\"body center\">\r\n            <div>\r\n                <p>Transit Account {{subsystemAccount}} could not be {{requestBody.block ? 'Blocked' : 'Unblocked'}} because following error:</p>\r\n            </div>\r\n        </li>\r\n        <li class=\"body center\" *ngFor=\"let e of responseBody?.errors\">\r\n            <div>\r\n                - {{e.errorKey}}: {{e.errorMessage}}\r\n            </div>\r\n        </li>\r\n\r\n        <li class=\"footer\">\r\n            <ul class=\"buttons\">\r\n                <li>\r\n                    <button type=\"button\" class=\"button-cancel\" (click)=\"okErrors()\">\r\n                        <p>OK</p>\r\n                    </button>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n    </ul>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/cub-subsystem-acct-block-unblock/cub_subsytemaccount_block_unblock.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Cub_SubsystemAccountBlockUnblockComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_cub_data_service__ = __webpack_require__("../../../../../src/app/services/cub_data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_cub_subsystem_service__ = __webpack_require__("../../../../../src/app/services/cub_subsystem.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_loading_screen_app_loading_screen_service__ = __webpack_require__("../../../../../src/app/app-loading-screen/app-loading-screen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__model__ = __webpack_require__("../../../../../src/app/model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Cub_SubsystemAccountBlockUnblockComponent = (function () {
    function Cub_SubsystemAccountBlockUnblockComponent(_cub_dataService, _cub_subsystemService, _loadingScreen, _route) {
        this._cub_dataService = _cub_dataService;
        this._cub_subsystemService = _cub_subsystemService;
        this._loadingScreen = _loadingScreen;
        this._route = _route;
        this.notes = "";
        this.showNotesError = false;
        this.showSaveModal = false;
        this.showErrorsModal = false;
        this.blockReasonCodeDesc = "";
        this.unblockReasonCodeDesc = "";
    }
    Cub_SubsystemAccountBlockUnblockComponent.prototype.ngOnInit = function () {
        var _this = this;
        var queryParams = this._route.snapshot.queryParamMap;
        if (!queryParams.has('subsystemId')
            || !queryParams.has('subsystemAccount')
            || !queryParams.has('block')) {
            this._cub_dataService.onApplicationError('No data passed to Block\Unblock modal');
            return;
        }
        this.requestBody = {
            subsystemId: queryParams.get('subsystemId'),
            subsystemAccount: queryParams.get('subsystemAccount'),
            block: JSON.parse(queryParams.get('block'))
        };
        this._cub_dataService.Globals
            .first(function (globals) { return !!globals['CUB.MSD.Web.Angular']; })
            .subscribe(function (globals) {
            _this.blockReasonCodeDesc = globals['CUB.MSD.Web.Angular']['ReasonCode_BlockAccount'];
            _this.unblockReasonCodeDesc = globals['CUB.MSD.Web.Angular']['ReasonCode_UnblockAccount'];
            if (!_this.blockReasonCodeDesc) {
                _this.blockReasonCodeDesc = "BlockAccount";
            }
            if (!_this.unblockReasonCodeDesc) {
                _this.unblockReasonCodeDesc = "UnblockAccount";
            }
            _this.reasonCodeDescription = _this.requestBody.block ? _this.blockReasonCodeDesc : _this.unblockReasonCodeDesc;
            _this.getReasonCodeTypes();
        });
        //"Block" : "Unblock";
        this.reasonCodeControl = {
            id: 'reasonCodeControl',
            formFieldClass: 'form-field-drop-down',
            fieldWidthClass: 'form-field-width-L',
            errorMessage: 'Reason Code is required.',
            isRequired: true,
            value: '',
            options: []
        };
    };
    Cub_SubsystemAccountBlockUnblockComponent.prototype.getReasonCodeTypes = function () {
        var _this = this;
        this._cub_subsystemService.getReasonCodes()
            .then(function (data) {
            if (data) {
                _this.reasonCodeTypes = JSON.parse(data.Body);
            }
        })
            .then(function () {
            _this.reasonCodeControl.options = [{
                    value: "0",
                    id: "0",
                    name: "Select...",
                    isDisabled: true,
                    notesRequired: false
                }];
            _this.reasonCodeControl.options = _this.reasonCodeControl.options.concat(__WEBPACK_IMPORTED_MODULE_6_lodash__["find"](_this.reasonCodeTypes.reasonCodeTypes, function (rct) { return rct['name'] === _this.reasonCodeDescription; }).reasonCodes.map(function (rc) { return ({
                value: rc.reasonCodeId,
                name: rc.description,
                notesRequired: rc.notesMandatoryFlag
            }); }));
            //if (this.reasonCodeControl.options.length > 0) {
            //    this.reasonCodeSelected = this.reasonCodeControl.options[0];}
        })
            .catch(function (error) {
            _this._cub_dataService.prependCurrentError('Error getting reason Codes');
        });
    };
    Cub_SubsystemAccountBlockUnblockComponent.prototype.onReasonCodeChanged = function () {
        this.reasonCodeSelected = __WEBPACK_IMPORTED_MODULE_6_lodash__["find"](this.reasonCodeControl.options, ['value', parseInt(this.reasonCodeControl.value)]);
        this.reasonCodeControl.isErrored = false;
    };
    Cub_SubsystemAccountBlockUnblockComponent.prototype.submitBlockClicked = function () {
        var _this = this;
        if (!this.validateSubmit())
            return;
        this._cub_subsystemService.blockSubsystemTransitAccount(this.requestBody.subsystemId, this.requestBody.subsystemAccount, this.reasonCodeSelected.value, this.notes)
            .then(function (data) {
            if (data != null) {
                if (data.Header.errorMessage) {
                    _this.showErrorsModal = true;
                }
                else {
                    _this.showSaveModal = true;
                }
            }
        })
            .catch(function (err) {
            if (err instanceof __WEBPACK_IMPORTED_MODULE_5__model__["CubError"] && err.handled) {
                return;
            }
            _this._cub_dataService.onApplicationError(err);
        });
    };
    Cub_SubsystemAccountBlockUnblockComponent.prototype.validateSubmit = function () {
        if (this.reasonCodeSelected == null) {
            this.reasonCodeControl.showErrorMessage = true;
            this.reasonCodeControl.isErrored = true;
            return false;
        }
        if (this.reasonCodeSelected.notesRequired && !this.notes) {
            this.showNotesError = true;
            return false;
        }
        else {
            this.showNotesError = false;
            return true;
        }
    };
    Cub_SubsystemAccountBlockUnblockComponent.prototype.submitUnBlockClicked = function () {
        var _this = this;
        if (!this.validateSubmit())
            return;
        this._cub_subsystemService.unblockSubsystemTransitAccount(this.requestBody.subsystemId, this.requestBody.subsystemAccount, this.reasonCodeSelected.value, this.notes)
            .then(function (data) {
            if (data != null) {
                _this.responseBody = data;
                if (data.Header.errorMessage) {
                    _this.showErrorsModal = true;
                }
                else {
                    _this.showSaveModal = true;
                }
            }
        })
            .catch(function (err) {
            if (err instanceof __WEBPACK_IMPORTED_MODULE_5__model__["CubError"] && err.handled) {
                return;
            }
            _this._cub_dataService.onApplicationError(err);
        });
    };
    Cub_SubsystemAccountBlockUnblockComponent.prototype.cancelClicked = function () {
        this.showSaveModal = false;
        var shouldRefresh = false;
        window['Mscrm'].Utilities.setReturnValue({
            refresh: shouldRefresh
        });
        window['closeWindow']();
    };
    Cub_SubsystemAccountBlockUnblockComponent.prototype.okErrors = function () {
        this.showErrorsModal = false;
        var shouldRefresh = false;
        window['Mscrm'].Utilities.setReturnValue({
            refresh: shouldRefresh
        });
        window['closeWindow']();
    };
    Cub_SubsystemAccountBlockUnblockComponent.prototype.okClicked = function () {
        this.processResponse();
    };
    Cub_SubsystemAccountBlockUnblockComponent.prototype.processResponse = function () {
        if (this.responseBody == null) {
            var error = ['Something went wrong while attempting to ', this.reasonCodeDescription, ' transit account ', this.requestBody.subsystemAccount, '.'].join('');
            this._cub_dataService.onApplicationError(error);
            return;
        }
        if (this.responseBody.Header.result === 'Successful') {
            window['Mscrm'].Utilities.setReturnValue({
                refresh: true
            });
            window['closeWindow']();
        }
    };
    return Cub_SubsystemAccountBlockUnblockComponent;
}());
Cub_SubsystemAccountBlockUnblockComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'cub-subsystem-acct-block-unblock',
        template: __webpack_require__("../../../../../src/app/cub-subsystem-acct-block-unblock/cub_subsytemaccount_block_unblock.component.html"),
        providers: [__WEBPACK_IMPORTED_MODULE_3__services_cub_subsystem_service__["a" /* Cub_SubsystemService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_cub_data_service__["a" /* Cub_DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_cub_data_service__["a" /* Cub_DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_cub_subsystem_service__["a" /* Cub_SubsystemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_cub_subsystem_service__["a" /* Cub_SubsystemService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__app_loading_screen_app_loading_screen_service__["a" /* AppLoadingScreenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__app_loading_screen_app_loading_screen_service__["a" /* AppLoadingScreenService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"]) === "function" && _d || Object])
], Cub_SubsystemAccountBlockUnblockComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=cub_subsytemaccount_block_unblock.component.js.map

/***/ }),

/***/ "../../../../../src/app/cub-subsystem-acct-block-unblock/cub_subsytemaccount_block_unblock.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cub_SubsystemAcctBlockUnblockModule", function() { return Cub_SubsystemAcctBlockUnblockModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cub_subsytemaccount_block_unblock_component__ = __webpack_require__("../../../../../src/app/cub-subsystem-acct-block-unblock/cub_subsytemaccount_block_unblock.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__controls_cub_dropdown_cub_dropdown_component__ = __webpack_require__("../../../../../src/app/controls/cub-dropdown/cub_dropdown.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pipes_money_pipe__ = __webpack_require__("../../../../../src/app/pipes/money.pipe.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var Cub_SubsystemAcctBlockUnblockModule = (function () {
    function Cub_SubsystemAcctBlockUnblockModule() {
    }
    return Cub_SubsystemAcctBlockUnblockModule;
}());
Cub_SubsystemAcctBlockUnblockModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_5__controls_cub_dropdown_cub_dropdown_component__["a" /* Cub_DropdownModule */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild([
                { path: '', component: __WEBPACK_IMPORTED_MODULE_4__cub_subsytemaccount_block_unblock_component__["a" /* Cub_SubsystemAccountBlockUnblockComponent */] }
            ]),
            __WEBPACK_IMPORTED_MODULE_6__pipes_money_pipe__["a" /* MoneyPipeModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_4__cub_subsytemaccount_block_unblock_component__["a" /* Cub_SubsystemAccountBlockUnblockComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_4__cub_subsytemaccount_block_unblock_component__["a" /* Cub_SubsystemAccountBlockUnblockComponent */]]
    })
], Cub_SubsystemAcctBlockUnblockModule);

//# sourceMappingURL=cub_subsytemaccount_block_unblock.module.js.map

/***/ })

});
//# sourceMappingURL=cub_subsytemaccount_block_unblock.module.chunk.js.map