webpackJsonp(["cub_updateaddress.module"],{

/***/ "../../../../../src/app/cub-updateaddress/cub_updateaddress.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"hybrid modal-small unsupported\">\r\n    <ul class=\"window modal-width-S update-address\">\r\n        <li class=\"header center\">\r\n            <h1>Update Address</h1>\r\n        </li>\r\n        <li class=\"body center\">\r\n            <div *ngIf=\"!!options && options.length > 0\">\r\n                <p>Do you wish to apply the updated address to the following contacts and/or funding sources?</p>\r\n                <div class=\"list-selector\">\r\n                    <button type=\"button\" \r\n                            class=\"button-text\" \r\n                            id=\"option-all\"\r\n                            (click)=\"onChangeAll()\">\r\n                        <p>{{allSelected ? 'Deselect All' : 'Select All'}}</p>\r\n                    </button>\r\n                </div>\r\n\r\n                <ul class=\"checklist-small\">\r\n                    <li *ngFor=\"let option of options\">\r\n                        <input type=\"checkbox\" id=\"{{'option-' + option.key}}\"\r\n                               [checked]=\"option.update\"\r\n                               (change)=\"onChange(option)\" />\r\n                        <label for=\"{{'option-' + option.key}}\">\r\n                            <ul>\r\n                                <li class=\"checkbox\">\r\n                                    <span>\r\n                                        <div class=\"icon-checkmark\">\r\n                                            <svg viewBox=\"0 0 16 13\">\r\n                                                <polyline points=\"2.25,5.75 6.5,10 14.25,2.25\" />\r\n                                            </svg>\r\n                                        </div>\r\n                                    </span>\r\n                                </li>\r\n                                <li>\r\n                                    <p>{{option.value}}</p>\r\n                                </li>\r\n                            </ul>\r\n                        </label>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n        </li>\r\n        <li class=\"footer\">\r\n            <ul class=\"buttons\">\r\n                <li>\r\n                    <button type=\"button\" class=\"button-primary\"\r\n                            [attr.disabled]=\"updateIsDisabled() ? '' : null\"\r\n                            (click)=\"updateClicked()\">\r\n                        <p>Update</p>\r\n                    </button>\r\n                </li>\r\n                <li>\r\n                    <button type=\"button\" class=\"button-cancel\" (click)=\"cancelClicked()\">\r\n                        <p>Cancel</p>\r\n                    </button>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n    </ul>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/cub-updateaddress/cub_updateaddress.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Cub_UpdateAddressComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_loading_screen_app_loading_screen_service__ = __webpack_require__("../../../../../src/app/app-loading-screen/app-loading-screen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_cub_data_service__ = __webpack_require__("../../../../../src/app/services/cub_data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_cub_updateaddress_service__ = __webpack_require__("../../../../../src/app/services/cub_updateaddress.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Cub_UpdateAddressComponent = (function () {
    function Cub_UpdateAddressComponent(_cub_dataService, _loadingScreen, _cub_updateAddressService, _route) {
        this._cub_dataService = _cub_dataService;
        this._loadingScreen = _loadingScreen;
        this._cub_updateAddressService = _cub_updateAddressService;
        this._route = _route;
        this.options = [];
        this.allSelected = true;
    }
    /**
     * Checks the query params for data and parses a list of dependencies
     * and the address fields.
     */
    Cub_UpdateAddressComponent.prototype.ngOnInit = function () {
        var queryParams = this._route.snapshot.queryParams;
        if (!queryParams['Data']) {
            this._cub_updateAddressService.postToParent({
                action: 'error',
                error: 'No data passed to update address component'
            });
            return;
        }
        var data = JSON.parse(decodeURIComponent(queryParams['Data']));
        this.customerId = data.customerId;
        this.address = data.address;
        this._cub_dataService.currentUserId = data.currentUserId;
        // sort contacts by name and then map to checkbox controls
        var contacts = __WEBPACK_IMPORTED_MODULE_5_lodash__["chain"](data.contactDependencies)
            .sortBy([
            function (c) { return c.name.lastName; },
            function (c) { return c.name.firstName; }
        ])
            .map(function (c) { return ({
            key: c.contactId,
            value: c.name.firstName + ' ' + c.name.lastName,
            type: 'contact',
            update: true
        }); })
            .value();
        // map funding sources to checkbox controls and then sort by display value
        var fundingSources = __WEBPACK_IMPORTED_MODULE_5_lodash__["chain"](data.fundingSourceDependencies)
            .map(function (f) {
            var val = 'UNDEFINED';
            if (!!f.creditCard && !!f.creditCard.maskedPan) {
                val = f.creditCard.creditCardType + ' ' + f.creditCard.maskedPan;
            }
            else if (!!f.directDebit && !!f.directDebit.bankAccountNumber) {
                val = f.directDebit.accountType + ' ' + f.directDebit.bankAccountNumber;
            }
            return {
                key: f.fundingSourceId,
                value: val,
                type: 'fundingSource',
                update: true
            };
        })
            .sortBy(function (f) { return f.value; })
            .value();
        this.options = contacts.concat(fundingSources);
    };
    /**
     * Fires when a dependency checkbox is clicked; flip the dependency's update
     * field and recheck if all dependencies are selected
     */
    Cub_UpdateAddressComponent.prototype.onChange = function (option) {
        option.update = !option.update;
        if (!option.update) {
            this.allSelected = false;
        }
        else if (__WEBPACK_IMPORTED_MODULE_5_lodash__["findIndex"](this.options, function (o) { return !o.update; }) === -1) {
            this.allSelected = true;
        }
    };
    /**
     * Fires when the Select All checkbox is clicked; flip its value and
     * set each dependency to the new value
     */
    Cub_UpdateAddressComponent.prototype.onChangeAll = function () {
        var _this = this;
        this.allSelected = !this.allSelected;
        this.options.forEach(function (o) { return o.update = _this.allSelected; });
    };
    /**
     * the user should not be able to click the UPDATE button if none of the
     * dependencies are selected
     */
    Cub_UpdateAddressComponent.prototype.updateIsDisabled = function () {
        return __WEBPACK_IMPORTED_MODULE_5_lodash__["findIndex"](this.options, function (o) { return o.update; }) === -1;
    };
    /**
     * Tell the parent to close
     */
    Cub_UpdateAddressComponent.prototype.cancelClicked = function () {
        this._cub_updateAddressService.postToParent({
            action: 'cancel'
        });
    };
    /**
     * Pulls all dependency keys selected and calls update. If all dependencies
     * are selected, empty lists are sent instead (per NIS specification). The
     * result is relayed up to the parent HTML web resource within MSD.
     */
    Cub_UpdateAddressComponent.prototype.updateClicked = function () {
        var contacts = [], fundingSources = [];
        if (!this.allSelected) {
            this.options.forEach(function (o) {
                if (o.update) {
                    (o.type === 'contact') ? contacts.push(o.key) : fundingSources.push(o.key);
                }
            });
        }
        this._loadingScreen.add('Updating...');
        this._cub_updateAddressService.update(this.customerId, this.address, contacts, fundingSources);
    };
    return Cub_UpdateAddressComponent;
}());
Cub_UpdateAddressComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'cub-updateaddress',
        template: __webpack_require__("../../../../../src/app/cub-updateaddress/cub_updateaddress.component.html"),
        providers: [__WEBPACK_IMPORTED_MODULE_4__services_cub_updateaddress_service__["a" /* Cub_UpdateAddressService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_cub_data_service__["a" /* Cub_DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_cub_data_service__["a" /* Cub_DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__app_loading_screen_app_loading_screen_service__["a" /* AppLoadingScreenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__app_loading_screen_app_loading_screen_service__["a" /* AppLoadingScreenService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_cub_updateaddress_service__["a" /* Cub_UpdateAddressService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_cub_updateaddress_service__["a" /* Cub_UpdateAddressService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"]) === "function" && _d || Object])
], Cub_UpdateAddressComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=cub_updateaddress.component.js.map

/***/ }),

/***/ "../../../../../src/app/cub-updateaddress/cub_updateaddress.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cub_UpdateAddressModule", function() { return Cub_UpdateAddressModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cub_updateaddress_component__ = __webpack_require__("../../../../../src/app/cub-updateaddress/cub_updateaddress.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var Cub_UpdateAddressModule = (function () {
    function Cub_UpdateAddressModule() {
    }
    return Cub_UpdateAddressModule;
}());
Cub_UpdateAddressModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild([
                { path: '', component: __WEBPACK_IMPORTED_MODULE_3__cub_updateaddress_component__["a" /* Cub_UpdateAddressComponent */] }
            ])
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__cub_updateaddress_component__["a" /* Cub_UpdateAddressComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]]
    })
], Cub_UpdateAddressModule);

//# sourceMappingURL=cub_updateaddress.module.js.map

/***/ }),

/***/ "../../../../../src/app/services/cub_updateaddress.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Cub_UpdateAddressService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cub_data_service__ = __webpack_require__("../../../../../src/app/services/cub_data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Cub_UpdateAddressService = (function () {
    function Cub_UpdateAddressService(_http, _cub_dataService) {
        this._http = _http;
        this._cub_dataService = _cub_dataService;
    }
    /**
     * Since this component is intended to render in an IFrame within a modal,
     * use this to communicate with the IFrame's parent's message listener.
     * @param data must contain an action string: {
     *     action: 'some action defined in updateAddress.html web resource',
     *     ...optional data to be passed to parent
     * }
     */
    Cub_UpdateAddressService.prototype.postToParent = function (data) {
        parent.postMessage(JSON.stringify(data), '*');
    };
    /**
     * Send a POST request to the web controller to update this address for the
     * provided dependencies.
     * @param customerId
     * @param address
     * @param contacts
     * @param fundingSources
     */
    Cub_UpdateAddressService.prototype.update = function (customerId, address, contacts, fundingSources) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["e" /* HttpHeaders */]({ 'Content-Type': 'application/json' }), body = {
            customerId: customerId,
            addressId: address.addressId,
            requestBody: {
                address: address,
                contactsUpdated: contacts,
                fundingSourcesUpdated: fundingSources
            },
            userId: this._cub_dataService.currentUserId
        };
        this._http.post(this._cub_dataService.apiServerUrl + '/CustomerAddress/AddressUpdate', body, { headers: headers })
            .toPromise()
            .then(function (data) { return ({
            action: 'success'
        }); })
            .catch(function (err) { return ({
            action: 'error',
            error: err
        }); })
            .then(function (returnVal) { return _this.postToParent(returnVal); });
    };
    return Cub_UpdateAddressService;
}());
Cub_UpdateAddressService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__cub_data_service__["a" /* Cub_DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__cub_data_service__["a" /* Cub_DataService */]) === "function" && _b || Object])
], Cub_UpdateAddressService);

var _a, _b;
//# sourceMappingURL=cub_updateaddress.service.js.map

/***/ })

});
//# sourceMappingURL=cub_updateaddress.module.chunk.js.map