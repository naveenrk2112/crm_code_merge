-- First create database called Cub_Logging

Use Cub_Logging
go

CREATE TABLE [dbo].[Logs](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Level] [nvarchar](20) NOT NULL,
	[Correlation_ID] [nvarchar](36) NULL,
	[Machine_Name] [nvarchar](250) NULL,
	[User] [nvarchar](150) NULL,
	[Class] [nvarchar](250) NULL,
	[Method] [nvarchar](250) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[Exception] [nvarchar](max) NULL,
	[Request] [nvarchar](max) NULL,
	[Response] [nvarchar](max) NULL,
	[RequestTime] [datetime2](7) NULL,
	[ResponseTime] [datetime2](7) NULL,
	[Run_Duration] [int] NULL,
 CONSTRAINT [pk_logs] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary Key (Identity)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'LogId'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Date'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry level' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Level'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry correlation ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Correlation_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Caller machine name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Machine_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Caller user name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'User'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Caller class name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Class'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Caller method name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Method'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Message'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry data (JSON)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Data'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry exception. Plus inner exceptions and stack trace' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Exception'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry request' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Request'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry response' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Response'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry request time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'RequestTime'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry response time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'ResponseTime'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log entry run duration in Milliseconds' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Logs', @level2type=N'COLUMN',@level2name=N'Run_Duration'
GO

CREATE NONCLUSTERED INDEX [LogsDateIdx] ON [dbo].[Logs]
(
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE procedure [dbo].[InsertLog] 
(
	@date datetime2,
	@level nvarchar(10),
	@correlation_id nvarchar(50),
	@machine_name nvarchar(250),
	@user nvarchar(150),
	@class nvarchar(250),
	@method nvarchar(150),
	@message nvarchar(max),
	@data nvarchar(max),
	@exception nvarchar(max),
	@request nvarchar(max),
	@response nvarchar(max),
	@request_time datetime2,
	@response_time datetime2	
)
as

/*
 * Hacking the default values of request/response times passed by NLog
 */
if Year(@request_time) = 1900 or Year(@request_time) = 1
  set @request_time = null
if Year(@response_time) = 1900 or Year(@response_time) = 1
  set @response_time = null

/*
 * Calculating the Run_Duration
 */
declare @run_duration int = null
if @request_time is not null and
   @response_time is not null
   set @run_duration = DATEDIFF(ms, @request_time, @response_time)

/*
 * Make sure that empty string are saved as NULL
 */
if (@data = '') set @data = null
if (@exception = '') set @exception = null
if (@request = '') set @request = null
if (@response = '') set @response = null

insert into dbo.Logs
(
	[Date],
	[Level],
	Correlation_ID,
	Machine_Name,
	[User],
	Class,
	Method,
	[Message],
	[Data],
	[exception],
	Request,
	Response,
	RequestTime,
	ResponseTime,
	Run_Duration
)
values
(
	@date,
	@level,
	@correlation_id,
	@machine_name,
	@user,
	@class,
	@method,
	@message,
	@data,
	@exception,
	@request,
	@response,
	@request_time,
	@response_time,
	@run_duration
)

CREATE LOGIN [LoggerUser] WITH PASSWORD=N'd$fault1', DEFAULT_DATABASE=[Cub_Logging], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

--ALTER LOGIN [LoggerUser] DISABLE
--GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [LoggerUser]
GO

