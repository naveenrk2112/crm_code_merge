/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace CUB.MSD.WCF.Library
{
    [ConfigurationCollection(typeof(SecurityTokenElement),
            CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class SecurityTokenElementCollection : ConfigurationElementCollection
    {
        #region Constructors
        static SecurityTokenElementCollection()
        {
            _properties = new ConfigurationPropertyCollection();
        }

        public SecurityTokenElementCollection()
        {
        }
        #endregion

        #region Fields
        private static ConfigurationPropertyCollection _properties;
        #endregion

        #region Properties
        protected override ConfigurationPropertyCollection Properties
        {
            get { return _properties; }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get
            {
                return "securityToken";
            }
        }
        #endregion

        #region Indexers
        //public SecurityTokenElement this[int index]
        //{
        //    get { return (SecurityTokenElement)BaseGet(index); }
        //    set
        //    {
        //        if (BaseGet(index) != null)
        //        {
        //            BaseRemoveAt(index);
        //        }
        //        BaseAdd(index, value);
        //    }
        //}

        new public SecurityTokenElement this[string token]
        {
            get { return BaseGet(token) as SecurityTokenElement; }
        }
        #endregion

        #region Methods
        public void Add(SecurityTokenElement securityToken)
        {
            BaseAdd(securityToken);
        }

        public void Remove(string token)
        {
            BaseRemove(token);
        }

        public void Remove(SecurityTokenElement securityToken)
        {
            BaseRemove(GetElementKey(securityToken));
        }

        public void Clear()
        {
            BaseClear();
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public string GetKey(int index)
        {
            return (string)BaseGetKey(index);
        }
        #endregion

        #region Overrides
        protected override ConfigurationElement CreateNewElement()
        {
            return new SecurityTokenElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as SecurityTokenElement).Value;
        }

        #endregion
    }
}
