/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.WCF.Library
{
    public class JsonModels
    {
        #region GUID Helper
        private static string FormatGUID(string id)
        {
            return String.IsNullOrWhiteSpace(id) ? null : id.ToUpper();
        }
        #endregion

        #region Customer Contracts

        public class WSResponseHeader
        {
            public string result { get; set; }
            public string uid { get; set; }
        }

        public class WSDataValidationResponseHeader
        {
            public string result { get; set; }
            public string uid { get; set; }
            public string fieldName { get; set; }
            public string errorKey { get; set; }
            public string errorMessage { get; set; }
        }

        public class WSErrorResponseHeader
        {
            public string result { get; set; }
            public string uid { get; set; }
            public string errorMessage { get; set; }
        }

        public class WSMSDFault
        {
            public string fieldName { get; set; }
            public string errorKey { get; set; }
            public string errorMessage { get; set; }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="fieldName"></param>
            /// <param name="errorKey"></param>
            /// <param name="errorMessage"></param>
            public WSMSDFault(string field_Name, string error_Key, string error_Message)
            {
                fieldName = field_Name;
                errorKey = error_Key;
                errorMessage = error_Message;
            }
        }

        public class WSError
        {
            public WSError()
            {
                key = string.Empty;
                message = string.Empty;
            }

            public WSError(string error_Key, string error_Message)
            {
                key = error_Key;
                message = error_Message;
            }

            public string key { get; set; }
            public string message { get; set; }
        }

        public class WSPhone
        {
            public string number { get; set; }
            public string type { get; set; }
        }

        public class WSName
        {
            public string title { get; set; }
            public string firstName { get; set; }
            public string middleInitial { get; set; }
            public string lastName { get; set; }
            public string nameSuffix { get; set; }
        }

        public class WSCredentials
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        //public class WSAddress
        //{
        //    public string address1 { get; set; }
        //    public string address2 { get; set; }
        //    public string city { get; set; }
        //    public string country { get; set; }
        //    public string state { get; set; }
        //    public string postalCode { get; set; }
        //}

        public class WSAddressExt
        {
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string state { get; set; }
            public string postalCode { get; set; }
            private string _addressId;
            public string addressId
            {
                get
                {
                    return _addressId;
                }
                set
                {
                    _addressId = FormatGUID(value);
                }
            }
        }

        public class WSAddressDeleteResponse
        {
            public List<WSCustomerContactInfo> contactDependencies { get; set; }
        }

        public class WSAddressCreationResponse
        {
            private string _addressId;
            public string addressId
            {
                get
                {
                    return _addressId;
                }
                set
                {
                    _addressId = FormatGUID(value);
                }
            }
            public bool isExistingAddress { get; set; }
        }

        public class WSSecurityQuestionsRetreiveRequest
        {
            public string username { get; set; }
        }

        public class WSPasswordForgotRequest
        {
            public string username { get; set; }
            public List<WSSecurityQA> securityQuestionAnswers { get; set; }
        }

        public class WSPasswordForgotResponse
        {
            public string verificationToken { get; set; }
            public string tokenExpiryDateTime { get; set; }
            private string _customerId;
            public string customerId
            {
                get
                {
                    return _customerId;
                }
                set
                {
                    _customerId = FormatGUID(value);
                }
            }
            private string _contactId;
            public string contactId
            {
                get
                {
                    return _contactId;
                }
                set
                {
                    _contactId = FormatGUID(value);
                }
            }
        }

        public class WSGenerateVerificationTokenRequest
        {
            public WSVerificationToken verificationToken { get; set; }
        }

        public class WSCredentialsValidate
        {
            public string pin { get; set; }
        }

        public class WSVerificationToken
        {
            public string verificationType { get; set; }
            public string mobileToken { get; set; }
        }

        public class WSVerifyTokenRequest
        {
            public string username { get; set; }
            public string verificationToken { get; set; }
            public string newPassword { get; set; }
        }

        public class WSVerifyMobileTokenRequest
        {
            private string _customerId;
            public string customerId
            {
                get
                {
                    return _customerId;
                }
                set
                {
                    _customerId = FormatGUID(value);
                }
            }
            private string _contactId;
            public string contactId
            {
                get
                {
                    return _contactId;
                }
                set
                {
                    _contactId = FormatGUID(value);
                }
            }
            public string verificationToken { get; set; }
        }

        public class WSVerifyMobileTokenResponse
        {
            public string mobileToken { get; set; }
        }

        public class WSGenerateTokenResponse
        {
            public string verificationToken { get; set; }
            public string tokenExpiryDateTime { get; set; }
        }

        public class WSSecurityQA
        {
            public string securityQuestion { get; set; }
            public string securityAnswer { get; set; }
        }

        public class WSSecurityQuestionsWithStrings
        {
            public List<string> securityQuestions { get; set; }
        }

        public class WSSecurityQuestions
        {
            public List<WSNameValue> securityQuestions { get; set; }
        }

        public class WSNameValue
        {
            public int value { get; set; }
            public string name { get; set; }
        }

        public class WSKeyValue
        {
            public string key { get; set; }
            public string value { get; set; }
        }

        public class WSNameSuffixTypesResponse
        {
            public List<WSKeyValue> nameSuffixes { get; set; }
        }

        public class WSPhoneTypesResponse
        {
            public List<WSKeyValue> phoneTypes { get; set; }
        }

        public class WSNameTitleTypesResponse
        {
            public List<WSKeyValue> nameTitles { get; set; }
        }

        public class WSCountryListResponse
        {
            public List<WSCountryInfo> countries { get; set; }
        }

        /// <summary>
        /// client customer statuses
        /// </summary>
        public class WSCustomerStatuses
        {
            /// <summary>
            /// list of client customer statuses
            /// </summary>
            public List<WSCustomerStatus> customerStatuses { get; set; }
        }

        /// <summary>
        /// client customer status items
        /// </summary>
        public class WSCustomerStatus
        {
            /// <summary>
            /// empty WSCustomerStatus constructor
            /// </summary>
            public WSCustomerStatus()
            {
                customerStatusId = -1;
                customerStatusEnum = string.Empty;
                description = string.Empty;
                shortDesc = string.Empty;
            }

            /// <summary>
            /// WSCustomer Status constructor
            /// </summary>
            /// <param name="ID"></param>
            /// <param name="StatusEnum"></param>
            /// <param name="Description"></param>
            /// <param name="ShortDescription"></param>
            public WSCustomerStatus(int ID, string StatusEnum, string Description, string ShortDescription)
            {
                customerStatusId = ID;
                customerStatusEnum = StatusEnum;
                description = Description;
                shortDesc = ShortDescription;
            }

            /// <summary>
            /// customer status identifier
            /// </summary>
            public int customerStatusId { get; set; }

            /// <summary>
            /// customer status enum name
            /// </summary>
            public string customerStatusEnum { get; set; }

            /// <summary>
            /// full description of the status
            /// </summary>
            public string description { get; set; }

            /// <summary>
            /// short description of the status
            /// </summary>
            public string shortDesc { get; set; }
        }

        /// <summary>
        /// client customer settings
        /// </summary>
        public class WSClientCustomerSettings
        {
            /// <summary>
            /// list of client customer settings
            /// </summary>
            public List<WSCustomerSettings> customerSettings { get; set; }
        }

        /// <summary>
        /// Client customer settings
        /// </summary>
        public class WSCustomerSettings
        {
            /// <summary>
            /// customer type
            /// </summary>
            public string customerType { get; set; }

            /// <summary>
            /// set of client password rules
            /// </summary>
            public WSPasswordRules passwordRules { get; set; }

            /// <summary>
            /// set of client username rules
            /// </summary>
            public WSUsernameRules usernameRules { get; set; }
        }

        public class WSCountryInfo
        {
            public string country { get; set; }
            public string description { get; set; }
            public string dialingCode { get; set; }
            public List<WSKeyValue> states { get; set; }
            public WSCountryConfig config { get; set; }
        }

        public class WSCountryConfig
        {
            public string address2 { get; set; } //REQ or OPT
            public string address3 { get; set; } //REQ, OPT, or NA
            public string city { get; set; } //REQ or OPT
            public string state { get; set; } //REQ or OPT
            public string postalCode { get; set; } //REQ, OPT, or NA
        }

        public class WSForgotUsername
        {
            public string email { get; set; }
            public string phone { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string pin { get; set; }
            public string postalCode { get; set; }
        }

        public class WSPasswordRequest
        {
            public string oldpassword { get; set; }
            public string newpassword { get; set; }
        }

        public class WSPersonalIdentifier
        {
            public string personalIdentifier { get; set; }
            public string personalIdentifierType { get; set; }
        }

        public class WSAppInfo
        {
            public string NextContactMSD { get; set; }
            public bool status { get; set; }
        }

        /// <summary>
        /// data to search for a customer by
        /// </summary>
        public class WSCustomerSearchCriteria
        {
            public string customerType { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string email { get; set; }
            public string phoneNumber { get; set; }
            public string postalCode { get; set; }
            public string personalIdentifierType { get; set; }
            public string personalIdentifier { get; set; }
            public string username { get; set; }
            public string customerStatus { get; set; }
            public string customerId { get; set; }
            public string businessName { get; set; }
            public string dob { get; set; }
            public string pin { get; set; }
            public string sortBy { get; set; }
            public int offset { get; set; }
            public int limit { get; set; }
        }

        // WSCustomerContactInfo - 2.5.7
        /// <summary>
        /// Customer Info
        /// </summary>
        public class WSCustomerContactInfo
        {
            /// <summary>
            /// Contact ID
            /// </summary>
            private string _contactId;
            public string contactId
            {
                get
                {
                    return _contactId;
                }
                set
                {
                    _contactId = FormatGUID(value);
                }
            }

            /// <summary>
            /// Contact Type
            /// </summary>
            public string contactType { get; set; }

            /// <summary>
            /// Name
            /// </summary>
            public WSName name { get; set; }

            /// <summary>
            /// Address
            /// </summary>
            public WSAddressExt address { get; set; }

            /// <summary>
            /// Phone
            /// </summary>
            public WSPhoneExt[] phone { get; set; }

            /// <summary>
            /// Email
            /// </summary>
            public string email { get; set; }

            /// <summary>
            /// Date Of Birth
            /// </summary>
            public string dateOfBirth { get; set; }

            /// <summary>
            /// Peronsal IdentifierInfo
            /// </summary>
            public WSPersonalIdentifier personalIdentifierInfo { get; set; }

            /// <summary>
            /// Username
            /// </summary>
            public string username { get; set; }
            
            /// <summary>
            /// PIN
            /// </summary>
            public string pin { get; set; }

            /// <summary>
            /// Security QA
            /// </summary>
            public WSSecurityQA[] securityQAs { get; set; }

            /// <summary>
            /// Contact Status
            /// </summary>
            public string contactStatus { get; set; }

            /// <summary>
            /// Credential Status
            /// </summary>
            public string credentialStatus { get; set; }
        }

        /// <summary>
        /// WSCustomerContact
        /// </summary>
        public class WSCustomerContact
        {
            /// <summary>
            /// Contact Info
            /// </summary>
            public WSCustomerContactInfo contact;
        }

        public class WSCreateCustomerRequest
        {
            public string customerType { get; set; }
            public Model.MSDApi.WSCustomerContact contact { get; set; }
            public Model.MSDApi.WSBusiness business { get; set; }
        }

        public class WSCreateCustomerResponse
        {
            private string _customerId;
            public string customerId
            {
                get
                {
                    return _customerId;
                }
                set
                {
                    _customerId = FormatGUID(value);
                }
            }
            private string _contactId;
            public string contactId
            {
                get
                {
                    return _contactId;
                }
                set
                {
                    _contactId = FormatGUID(value);
                }
            }
            public string tempPassword { get; set; }
            public string tempPasswordExpirationDate { get; set; }
        }

        //WSCustomer - 2.5.6
        /// <summary>
        /// WS Customer
        /// </summary>
        public class WSCustomer
        {
            /// <summary>
            /// (Required) Unique identifier of this customer.
            /// </summary>
            private string _customerId;
            public string customerId
            {
                get
                {
                    return _customerId;
                }
                set
                {
                    _customerId = FormatGUID(value);
                }
            }

            /// <summary>
            /// (Required) The customer type.
            /// </summary>
            public string customerType { get; set; }

            /// <summary>
            /// (Required) The current account status identifier as the key and the account status description as the value.
            /// </summary>
            public WSKeyValue customerStatus { get; set; }

            /// <summary>
            /// (Optional) The date and time when the customer status was last modified.
            /// </summary>
            public string lastStatusDtm { get; set; }

            /// <summary>
            /// (Required) The date and time when the customer was created.
            /// </summary>
            public string insertedDtm { get; set; }

            /// <summary>
            /// (Optional) The close account order ID associated with the customer, if it exists.
            /// </summary>
            private string _closeAccountOrderId;
            public string closeAccountOrderId
            {
                get
                {
                    return _closeAccountOrderId;
                }
                set
                {
                    _closeAccountOrderId = FormatGUID(value);
                }
            }

            /// <summary>
            /// Contacts
            /// </summary>
            public List<WSCustomerContactInfo> contacts { get; set; }

            /// <summary>
            /// Address
            /// </summary>
            public List<WSAddressExt> addresses { get; set; }

            /// <summary>
            /// (Conditionally Required) Business customer information. Required when customerType is Retailer.
            /// </summary>
            public Model.MSDApi.WSBusiness business { get; set; }
        }


        public class WSCredentailsPrevalidateResults
        {
            public bool isUsernameValid { get; set; }
            public List<Model.MSDApi.WSError> usernameErrors { get; set; }
            public bool isPasswordValid { get; set; }
            public List<Model.MSDApi.WSError> passwordErrors { get; set; }
        }

        public class WSAuthenticateRequest
        {
            public string username { get; set; }
            public string password { get; set; }
            public string customerType { get; set; }
        }

        public class WSAuthenticateResponse
        {
            public string authCode { get; set; }
            public List<WSError> authErrors { get; set; }
            private string _customerId;
            public string customerId
            {
                get
                {
                    return _customerId;
                }
                set
                {
                    _customerId = FormatGUID(value);
                }
            }
            public string customerType { get; set; }
            private string _contactId;
            public string contactId
            {
                get
                {
                    return _contactId;
                }
                set
                {
                    _contactId = FormatGUID(value);
                }
            }
        }

        public class WSCustomerSearchByIdRequest
        {
            private List<string> _customerIds = new List<string>();
            public List<string> customerIds
            {
                get
                {
                    return _customerIds;
                }
            }
            public void AddCustomerId(string id)
            {
                _customerIds.Add(FormatGUID(id));
            }
            public string sortBy { get; set; }
            public int offset { get; set; }
            public int limit { get; set; }
        }

        public class WSMSDOrganizationInfo
        {
            private string _organzationId;
            public string OrganzationId
            {
                get
                {
                    return _organzationId;
                }
                set
                {
                    _organzationId = FormatGUID(value);
                }
            }
            public string OrganizationName { get; set; }
            public string Version { get; set; }
        }

        /// <summary>
        /// 2-136. customer/feedback POST API Request Fields
        /// </summary>
        public class WSCustomerFeedbackRequest
        {
            /// <summary>
            /// string(20)
            /// (Required) Type of customer feedback.Allowable value is:
            /// - ApplicationFeedback
            /// - GeneralFeedback
            /// - Question
            /// - Suggestion
            /// - Complaint
            /// - Compliment
            /// - Refund
            /// </summary>
            public string feedbackType { get; set; }
            
            /// <summary>
            /// (Required) The text message explaining customer feedback.
            /// </summary>
            public string feedbackMessage { get; set; }
            
            /// <summary>
            /// (Optional) Unique identifier for the customer.
            /// </summary>
            public string customerId { get; set; }
            
            /// <summary>
            /// (Optional) Unique identifier for the contact within the customer.
            /// </summary>
            public string contactId { get; set; }
            
            /// <summary>
            /// (Optional) If the customer is not registered (i.e. customerId is not provided), the email address to send notifications.
            /// If customerId is specified, this field will be ignored.
            /// </summary>
            public string unregisteredEmail { get; set; }
            
            /// <summary>
            /// (Optional) If the customer is not registered, the first name to use. If customerId is specified, this field will be ignored.
            /// </summary>
            public string firstName { get; set; }
            
            /// <summary>
            /// (Optional) If the customer is not registered, the last name to use. If customerId is specified, this field will be ignored.
            /// </summary>
            public string lastName { get; set; }
            
            /// <summary>
            /// (Optional) If the customer is not registered, the phone number to use. If customerId is specified, this field will be ignored.
            /// </summary>
            public WSPhone phone { get; set; }

            /// <summary>
            /// (Optional) The type of the item customer is referring this feedback about. E.g. card, order, case etc.
            /// No validation is done on referenceType field.
            /// </summary>
            public string referenceType { get; set; }

            /// <summary>
            /// (Optional) The value of the item customer is referring this feedback about. E.g. card id, order id, case id etc.
            /// </summary>
            public string referenceValue { get; set; }

            /// <summary>
            /// Channel
            /// </summary>
            public string channel { get; set; }
        }

        /// <summary>
        /// 2-80 customer/feedback POST API Response Fields
        /// </summary>
        public class WSCustomerFeedbackResponse
        {
            /// <summary>
            /// (Required) The unique identifier to which this feedback is referred against.
            /// </summary>
            public string referenceNumber { get; set; }
            
            /// <summary>
            /// MSD case ID
            /// </summary>
            public string caseId { get; set; }
        }

        #endregion

    }


}
