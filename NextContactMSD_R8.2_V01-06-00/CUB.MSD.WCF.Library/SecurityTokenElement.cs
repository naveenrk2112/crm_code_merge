/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace CUB.MSD.WCF.Library
{
    public class SecurityTokenElement : ConfigurationElement
    {
        private static ConfigurationProperty _value;
        private static ConfigurationProperty _userName;
        private static ConfigurationProperty _password;
        private static ConfigurationProperty _domain;
        private static ConfigurationProperty _orgName;
        private static ConfigurationPropertyCollection _properties;
        private static ConfigurationProperty _CUBMSDServer;

        static SecurityTokenElement()
        {
            _properties = new ConfigurationPropertyCollection();

            _value = new ConfigurationProperty(
                "value",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_value);

            _userName = new ConfigurationProperty(
                "userName",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_userName);

            _password = new ConfigurationProperty(
                "password",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_password);

            _domain = new ConfigurationProperty(
                "domain",
                typeof(string),
                null,
                ConfigurationPropertyOptions.None);
            _properties.Add(_domain);

            _orgName = new ConfigurationProperty(
                "orgName",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_orgName);

            _CUBMSDServer = new ConfigurationProperty(
                "CUBMSDServer",
                typeof(string),
                null,
                ConfigurationPropertyOptions.None);
            _properties.Add(_CUBMSDServer);
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get { return (string)this[_value]; }
        }

        [ConfigurationProperty("userName", IsRequired = true)]
        public string UserName
        {
            get { return (string)this[_userName]; }
        }

        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get { return (string)this[_password]; }
        }

        [ConfigurationProperty("domain", IsRequired = false)]
        public string Domain
        {
            get { return (string)this[_domain]; }
        }

        [ConfigurationProperty("orgName", IsRequired = false)]
        public string OrgName
        {
            get { return (string)this[_orgName]; }
        }

        [ConfigurationProperty("CUBMSDServer", IsRequired = false)]
        public string CUBMSDServer
        {
            get { return (string)this[_CUBMSDServer]; }
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                return _properties;
            }
        }
    }
}
