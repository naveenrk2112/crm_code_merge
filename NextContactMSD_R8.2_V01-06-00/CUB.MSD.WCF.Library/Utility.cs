/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.IO;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Runtime.Caching;
using System.Globalization;
using System.ServiceModel;
using CUB.MSD.Model;
using System.Security.Cryptography;
using System.Runtime.InteropServices;


namespace CUB.MSD.WCF.Library
{
    public class CubdObjectCache<T> : MemoryCache where T : class
    {
        private CacheItemPolicy HardDefaultCacheItemPolicy = new CacheItemPolicy()
        {
            SlidingExpiration = new TimeSpan(0, 15, 0)
        };

        private CacheItemPolicy defaultCacheItemPolicy;

        public CubdObjectCache(string name, NameValueCollection nvc = null, CacheItemPolicy policy = null) : base(name, nvc)
        {
            defaultCacheItemPolicy = policy ?? HardDefaultCacheItemPolicy;
        }

        public void Set(string cacheKey, T cacheItem, CacheItemPolicy policy = null)
        {
            policy = policy ?? defaultCacheItemPolicy;
            if (cacheItem != null)
                base.Set(cacheKey, cacheItem, policy);
        }

        public bool TryGetAndSet(string cacheKey, Func<T> getData, out T returnData, CacheItemPolicy policy = null)
        {
            if (TryGet(cacheKey, out returnData))
                return true;


            returnData = getData();
            Set(cacheKey, returnData, policy);

            if (TryGet(cacheKey, out returnData))
                return true;

            return false;
        }

        public bool TryGet(string cacheKey, out T returnItem)
        {
            //returnItem = this[cacheKey] as T
            returnItem = (T)this[cacheKey];
            return returnItem != null;
        }
    }

    public class Utility
    {
        public static cub_verificationtokentype getTokenType(string verificationType)
        {
            cub_verificationtokentype type;

            if (!Enum.IsDefined(typeof(cub_verificationtokentype), verificationType))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Verification Token", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, "Invalid token type"));

            if (!Enum.TryParse<cub_verificationtokentype>(verificationType, out type))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Verification Token", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, "Invalid token type"));

            return type;
        }

        public static string Base64EncryptedStringToString(string encryptedValue)
        {
            string return_string = null;

            try
            {
                byte[] decodedBytes = Convert.FromBase64String(encryptedValue);
                return_string = new ASCIIEncoding().GetString(decodedBytes);
            }
            catch (Exception e)
            {
                //log the error
            }

            return return_string;
        }

        public static string Base64EncryptString(string s)
        {
            return Convert.ToBase64String(StringToByteArray(s));
        }

        public static string ByteArrayToString(byte[] byteArray)
        {
            return Encoding.ASCII.GetString(byteArray);
        }

        public static byte[] StringToByteArray(string str)
        {
            if (string.IsNullOrEmpty(str))
                return null;
            return Encoding.ASCII.GetBytes(str);
        }

        public static bool IsGUID(string expression)
        {
            if (expression != null)
            {
                Regex guidRegEx = new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");

                return guidRegEx.IsMatch(expression);
            }
            return false;
        }

        public static string[] ParseTokenAndExpiry(string input)
        {
            string[] output = null;

            if (!string.IsNullOrEmpty(input))
            {
                output = new string[2];
                string[] Items = input.Split(new char[] { ',' });
                string token = Items[0];
                string expiry = Items[1];
                output[0] = token.Trim();
                output[1] = expiry.Trim();
            }

            return output;
        }
    }
}
