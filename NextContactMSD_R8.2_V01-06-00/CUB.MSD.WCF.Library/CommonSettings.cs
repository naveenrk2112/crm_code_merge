/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.WCF.Library
{
    public class CommonSettings
    {
        private static CubdObjectCache<object> restfulCache = new CubdObjectCache<object>("MSDRestful");
        private static string _UserName;
        private static string _Password;
        private static string _Domain;
        private static string _OrgName;
        private static string _LogTable;
        private static int _FirstNameMaxLength = -1;
        private static int _LastNameMaxLength = -1;
        private static int _AddressStreetMaxLength = -1;
        private static int _AddressCityMaxLength = -1;
        private static int _PhoneNumberMaxLength = -1;


        private static int _UsernameMinLength = -1;
        private static int _UsernameMaxLength = -1;
        private static int _PasswordMinLength = -1;
        private static int _PasswordMaxLength = -1;
        private static int _PasswordUpperAlpha = -1;
        private static int _PasswordLowerAlpha = -1;
        private static int _PasswordNumeric = -1;
        private static int _PasswordNonAlphanumeric = -1;
        private static int _UsernameRequireEmail = -1;

        public static string UserName
        {
            get
            {
                if (string.IsNullOrEmpty(_UserName))
                    return _UserName = Utility.Base64EncryptedStringToString(Convert.ToString(GetCacheSetting("userName")));
                else
                    return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }

        public static string Password
        {
            get
            {
                if (string.IsNullOrEmpty(_Password))
                    return _Password = Utility.Base64EncryptedStringToString(Convert.ToString(GetCacheSetting("password")));
                else
                    return _Password;
            }
            set
            {
                _Password = value;
            }
        }

        public static string Domain
        {
            get
            {
                if (string.IsNullOrEmpty(_Domain))
                    return _Domain = Utility.Base64EncryptedStringToString(Convert.ToString(GetCacheSetting("domain")));
                else
                    return _Domain;
            }
            set
            {
                _Domain = value;
            }
        }

        public static string OrgName
        {
            get
            {
                if (string.IsNullOrEmpty(_OrgName))
                    return _OrgName = Convert.ToString(GetCacheSetting("orgName"));
                else
                    return _OrgName;
            }
            set
            {
                _OrgName = value;
            }
        }

        public static string LogTable
        {
            get
            {
                if (string.IsNullOrEmpty(_LogTable))
                    return _LogTable = Convert.ToString(GetCacheSetting("LogTable"));
                else
                    return _LogTable;
            }
            set
            {
                _LogTable = value;
            }
        }

        public static int UsernameRequireEmail
        {
            get
            {
                if (_UsernameRequireEmail < 0)
                    return _UsernameRequireEmail = Convert.ToInt32(GetCacheSetting("UsernameRequireEmail"));
                else
                    return _UsernameRequireEmail;
            }
            set
            {
                _UsernameRequireEmail = value;
            }
        }

        public static int UsernameMinLength
        {
            get
            {
                if (_UsernameMinLength < 0)
                    return _UsernameMinLength = Convert.ToInt32(GetCacheSetting("UsernameMinLength"));
                else
                    return _UsernameMinLength;
            }
            set
            {
                _UsernameMinLength = value;
            }
        }

        public static int UsernameMaxLength
        {
            get
            {
                if (_UsernameMaxLength < 0)
                    return _UsernameMaxLength = Convert.ToInt32(GetCacheSetting("UsernameMaxLength"));
                else
                    return _UsernameMaxLength;
            }
            set
            {
                _UsernameMaxLength = value;
            }
        }

        public static int PasswordMinLength
        {
            get
            {
                if (_PasswordMinLength < 0)
                    return _PasswordMinLength = Convert.ToInt32(GetCacheSetting("PasswordMinLength"));
                else
                    return _PasswordMinLength;
            }
            set
            {
                _PasswordMinLength = value;
            }
        }

        public static int PasswordMaxLength
        {
            get
            {
                if (_PasswordMaxLength < 0)
                    return _PasswordMaxLength = Convert.ToInt32(GetCacheSetting("PasswordMaxLength"));
                else
                    return _PasswordMaxLength;
            }
            set
            {
                _PasswordMaxLength = value;
            }
        }

        public static int PasswordUpperAlpha
        {
            get
            {
                if (_PasswordUpperAlpha < 0)
                    return _PasswordUpperAlpha = Convert.ToInt32(GetCacheSetting("PasswordUpperAlpha"));
                else
                    return _PasswordUpperAlpha;
            }
            set
            {
                _PasswordUpperAlpha = value;
            }
        }

        public static int PasswordLowerAlpha
        {
            get
            {
                if (_PasswordLowerAlpha < 0)
                    return _PasswordLowerAlpha = Convert.ToInt32(GetCacheSetting("PasswordLowerAlpha"));
                else
                    return _PasswordLowerAlpha;
            }
            set
            {
                _PasswordLowerAlpha = value;
            }
        }

        public static int PasswordNumeric
        {
            get
            {
                if (_PasswordNumeric < 0)
                    return _PasswordNumeric = Convert.ToInt32(GetCacheSetting("PasswordNumeric"));
                else
                    return _PasswordNumeric;
            }
            set
            {
                _PasswordNumeric = value;
            }
        }
        public static int PasswordNonAlphanumeric
        {
            get
            {
                if (_PasswordNonAlphanumeric < 0)
                    return _PasswordNonAlphanumeric = Convert.ToInt32(GetCacheSetting("PasswordNonAlphanumeric"));
                else
                    return _PasswordNonAlphanumeric;
            }
            set
            {
                _PasswordNonAlphanumeric = value;
            }
        }

        private static object GetCacheSetting(String settingName)
        {
            object returnValue;

            if (!restfulCache.TryGetAndSet(settingName, () => GetSetting(settingName), out returnValue))
            {
                return null;
            }

            return returnValue;
        }

        private static string GetSetting(string name)
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains(name))
                return ConfigurationManager.AppSettings[name];
            else
                return null;
        }
    }
}
