﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.ServiceModel;
//using System.ServiceModel.Configuration;
using System.Configuration;
//using System.Xml.Serialization;
using System.ServiceModel.Description;
//using System.ServiceModel.Security;
//using System.IdentityModel.Claims;
using System.Data;
using CUB.MSD.Logic;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Xrm.Tooling.Connector;

//using Newtonsoft.Json;
using EntityReference = Microsoft.Xrm.Sdk.EntityReference;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using CUB.MSD.Model.MSDApi;

namespace CUB.MSD.WCF.Library
{
    /// <summary>
    /// Internal response
    /// </summary>
    public class InternalResponse
    {
        public int _errorCode = 0;
        public string _id = "";
        public string _id2 = "";
        public string _message = "";
        public InternalResponse() { }
        public InternalResponse(int errorCode, string message)
        {
            _errorCode = errorCode;
            _message = message;
        }
        public InternalResponse(int errorCode, string message, string id)
        {
            _errorCode = errorCode;
            _message = message;
            _id = id;
        }
    }

    /// <summary>
    /// connection information for talking to MSD
    /// </summary>
    public class ConnectionInformation
    {
        public ConnectionInformation()
        { CrmService = null; Connection = null; }
        public IOrganizationService CrmService { get; set; }
        public CrmServiceClient Connection { get; set; }
    }

    public class Globals
    {
        #region
        /// <summary>
        /// create a connection when we create the class
        /// this makes unit testing easier
        /// </summary>
        /// <param name="connInfo"></param>
        public Globals(ConnectionInformation connInfo = null)
        {
            if (connInfo == null)
                CreateCrmService();
            else ConnectionInfo = connInfo;
        }

        private const String _password = "<password>";
        public static int DEFAULT_ATTEMPTS = 4;
        public static int DEFAULT_SLEEP_MILLI = 5000;
        public ConnectionInformation ConnectionInfo = new ConnectionInformation();

        /// <summary>
        /// certificaiton policies
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        private static bool AcceptAllCertificatePolicy(
           Object sender,
           X509Certificate certificate,
           X509Chain chain,
           SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// Create CRM Service validates communicating with the CRM and returns the connection information
        /// </summary>
        /// <returns></returns>
        public ConnectionInformation CreateCrmService()
        {
            try
            {
                // The genericEntity function does not use a token but the user entered logon (entered as user@tripsync.com)

                string crmServer = ConfigurationManager.AppSettings["crmserver"];

                if (string.IsNullOrEmpty(crmServer)) throw new SystemException("crm server name is null");

                // validate the orgname using discovery services
                var url = new Uri("http://" + crmServer + "/XRMServices/2011/Discovery.svc");

                //to ignore certificates errors
                ServicePointManager.ServerCertificateValidationCallback =
                    new RemoteCertificateValidationCallback(AcceptAllCertificatePolicy);

                var credentials = getCredentials();

                var discoveryServiceProxy = new DiscoveryServiceProxy(url, null, credentials, null);
                var discoveryService = discoveryServiceProxy;

                var orgRequest = new RetrieveOrganizationsRequest();
                string orgName = "";

                try
                {
                    var orgResponse = (RetrieveOrganizationsResponse)discoveryService.Execute(orgRequest);

                    if (orgResponse == null) //todo error cleanup
                        throw new Exception("CreateCrmService: Unable to retrieve organization");

                    orgName = GetOrgName();

                    OrganizationDetail orgInfo =
                        orgResponse.Details.FirstOrDefault(
                            orgDetail => orgDetail.UniqueName.ToLower().Equals(orgName.ToLower()));

                    if (orgInfo == null)
                        throw new Exception("The organization name is invalid.");
                }
                catch (Exception e)
                {
                    CubLogger.Error("CreateCrmService", e);
                    throw e;
                    //return new ConnectionInformation();
                }

                // Establish a connection to the organization web service using CrmConnection.
                string connectionString = "";

                connectionString = "AuthType=AD;Url=http://" + crmServer + "/" + orgName + "; Domain=" + CommonSettings.Domain + "; Username=" +
                                       credentials.Windows.ClientCredential.UserName + "; Password=" + credentials.Windows.ClientCredential.Password +
                                       ";";

                ConnectionInfo.Connection = new CrmServiceClient(connectionString);
                ConnectionInfo.CrmService = (IOrganizationService)ConnectionInfo.Connection.OrganizationWebProxyClient ?? (IOrganizationService)ConnectionInfo.Connection.OrganizationServiceProxy;

                return ConnectionInfo;
            }
            catch (Exception ex)
            {
                CubLogger.Error("CreateCrmService", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Get Credentials fills in and returns a client credentials object
        /// </summary>
        /// <returns></returns>
        public ClientCredentials getCredentials()
        {
            ClientCredentials creds = new ClientCredentials();

            string username = CommonSettings.UserName;
            string password = CommonSettings.Password;
            string domain = CommonSettings.Domain;

            //NetworkCredential credentials;
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(domain))
            {
                creds.Windows.ClientCredential = new NetworkCredential(username, password, domain);
                creds.UserName.UserName = username;
                creds.UserName.Password = password;
                creds.Windows.ClientCredential.Domain = domain;
            }
            else
            {
                creds.Windows.ClientCredential = new NetworkCredential(username, password);
            }

            return creds;
        }

        /// <summary>
        /// Get Organization Name returns that Org name this instance of the web service is connected to
        /// </summary>
        /// <returns></returns>
        public string GetOrgName()
        {
            return CommonSettings.OrgName;
        }

        #endregion

        #region Rest-related
        /// <summary>
        /// returns the verison data for the web service
        /// </summary>
        /// <returns></returns>
        public JsonModels.WSAppInfo IsAvailable()
        {
            JsonModels.WSAppInfo appInfo = new JsonModels.WSAppInfo();

            try
            {
                // get the version from the dll
                string client_name = string.Empty;
                string version = typeof(CUB.MSD.WCF.Library.Globals).Assembly.GetName().Version.ToString();
                int at = version.LastIndexOf('.');
                string first = string.Empty;
                string second = string.Empty;
                string third = string.Empty;
                string fourth = string.Empty;

                // parse out the version items
                fourth = version.Substring(at + 1, version.Length - (at + 1));
                version = version.Substring(0, at);
                first = version.Substring(0, version.IndexOf('.'));
                second = version.Substring(version.IndexOf('.') + 1, version.LastIndexOf('.') - (version.IndexOf('.') + 1));
                third = version.Substring(version.LastIndexOf('.') + 1, version.Length - (version.LastIndexOf('.') + 1));

                // add leading zeros where necessary
                if (first.Length < 2) first = "0" + first;
                if (second.Length < 2) second = "0" + second;
                if (third.Length < 2) third = "0" + third;
                if (fourth.Length < 2) fourth = "0" + fourth;

                // assemble all the data into one version string
                client_name = "1." + first + "." + second + "." + third + "." + fourth;

                // put the version string within the Part Number
                appInfo.NextContactMSD = "PN.8500-99285-" + client_name + ".NextContact.MSDSVC";
                appInfo.status = true;

                // add logging for debug just to check
                CubLogger.Debug(JsonConvert.SerializeObject(appInfo));
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return appInfo;
        }

        /// <summary>
        /// returns MSDg Organziation data
        /// </summary>
        /// <returns></returns>
        public JsonModels.WSMSDOrganizationInfo GetOrganziationInfo()
        {
            JsonModels.WSMSDOrganizationInfo orgInfo = new JsonModels.WSMSDOrganizationInfo();


            try
            {
                OrganizationLogic orgLogic = new OrganizationLogic(ConnectionInfo.CrmService);

                CubResponse<WSOrganizationInfo> orgInfoResponse = orgLogic.GetOrganziationInfo();
                if (orgInfoResponse.Success)
                {
                    orgInfo.OrganzationId = orgInfoResponse.ResponseObject.OrganizationId.Value.ToString().ToUpper();
                    orgInfo.OrganizationName = orgInfoResponse.ResponseObject.OrganizationName;
                    orgInfo.Version = orgInfoResponse.ResponseObject.Version;
                }
                else
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault(orgInfoResponse.Errors[0].fieldName, orgInfoResponse.Errors[0].errorKey, orgInfoResponse.Errors[0].errorMessage));
                }


            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }


            return orgInfo;
        }

        /// <summary>
        /// retrieve the security questions available for this client
        /// </summary>
        /// <returns></returns>
        public JsonModels.WSSecurityQuestions GetClientSecurityQuestions()
        {
            JsonModels.WSSecurityQuestions squestions = new JsonModels.WSSecurityQuestions();

            try
            {
                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                {
                    // request the questions from the database
                    var qlist = (from quests in osc.cub_SecurityQuestionSet
                                 select quests.cub_SecurityQuestionDesc);

                    if (qlist != null)
                    {
                        int i = 0;
                        squestions.securityQuestions = new List<JsonModels.WSNameValue>();
                        foreach (string s in qlist)
                        {
                            JsonModels.WSNameValue nv = new JsonModels.WSNameValue(); 
                            nv.value = ++i;
                            nv.name = s;
                            squestions.securityQuestions.Add(nv);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return squestions;
        }

        /// <summary>
        /// retrieve the name suffixes available for this client
        /// </summary>
        /// <returns></returns>
        public JsonModels.WSNameSuffixTypesResponse GetClientSuffixes()
        {
            JsonModels.WSNameSuffixTypesResponse suffixList = new JsonModels.WSNameSuffixTypesResponse();

            try
            {
                UtilityLogic utilLogic = new UtilityLogic();
                List<KeyValuePair<int, string>> optionSet = utilLogic.GetOptionSetMetaData(typeof(cub_contactsuffix));
                OptionSetTranslationLogic logic = new OptionSetTranslationLogic(ConnectionInfo.CrmService);
                // Fill in the list of Suffixes available for this client
                suffixList.nameSuffixes = new List<JsonModels.WSKeyValue>();
                foreach (KeyValuePair<int, string> item in optionSet)
                {
                    JsonModels.WSKeyValue kv = new JsonModels.WSKeyValue();
                    kv.key = logic.GetText(null, "cub_contactsuffix", item.Key);
                    kv.value = item.Value;
                    suffixList.nameSuffixes.Add(kv);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return suffixList;
        }

        /// <summary>
        /// retrieve the phone types available for this client
        /// </summary>
        /// <returns></returns>
        public JsonModels.WSPhoneTypesResponse GetClientPhoneTypes()
        {
            JsonModels.WSPhoneTypesResponse phoneTypeList = new JsonModels.WSPhoneTypesResponse();
            try
            {
                UtilityLogic utilLogic = new UtilityLogic();
                List<KeyValuePair<int, string>> optionSet = utilLogic.GetOptionSetMetaData(typeof(cub_phonetype));
                OptionSetTranslationLogic logic = new OptionSetTranslationLogic(ConnectionInfo.CrmService);
                // Fill in the list of Phone Types available for this client
                phoneTypeList.phoneTypes = new List<JsonModels.WSKeyValue>();
                foreach (KeyValuePair<int, string> item in optionSet)
                {
                    JsonModels.WSKeyValue kv = new JsonModels.WSKeyValue();
                    kv.key = logic.GetText(null, "cub_phonetype", item.Key);
                    kv.value = item.Value;
                    phoneTypeList.phoneTypes.Add(kv);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return phoneTypeList;
        }

        /// <summary>
        /// retrieve the prefixes (name titles) available for this client
        /// </summary>
        /// <returns></returns>
        public JsonModels.WSNameTitleTypesResponse GetClientNameTitles()
        {
            JsonModels.WSNameTitleTypesResponse nameTitleList = new JsonModels.WSNameTitleTypesResponse();

            try
            {
                UtilityLogic utilLogic = new UtilityLogic();
                List<KeyValuePair<int, string>> optionSet = utilLogic.GetOptionSetMetaData(typeof(cub_nametitles));
                OptionSetTranslationLogic logic = new OptionSetTranslationLogic(ConnectionInfo.CrmService);
                nameTitleList.nameTitles = new List<JsonModels.WSKeyValue>();
                foreach (KeyValuePair<int, string> item in optionSet)
                {
                    JsonModels.WSKeyValue kv = new JsonModels.WSKeyValue();
                    kv.key = logic.GetText(null, "cub_nametitles", item.Key);
                    kv.value = item.Value;
                    nameTitleList.nameTitles.Add(kv);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return nameTitleList;
        }

        /// <summary>
        /// retrieve the countries available for this client with their address requirements
        /// </summary>
        /// <returns>Countries</returns>
        public JsonModels.WSCountryListResponse GetClientCountries()
        {
            JsonModels.WSCountryListResponse response = new JsonModels.WSCountryListResponse();
            try
            {
                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                {
                    // Look up the Active countries for this client and also return their address requirements
                    var countries = (from c in osc.cub_CountrySet
                                     where c.statecode.Equals(cub_CountryState.Active)
                                     select new JsonModels.WSCountryInfo
                                     {
                                         country = c.cub_Alpha2,
                                         description = c.cub_CountryName,
                                         dialingCode = c.cub_DialingCode,
                                         config = new JsonModels.WSCountryConfig
                                         {
                                             address2 = c.cub_Address2 == null ?
                                                null :
                                                Enum.GetName(typeof(cub_Countrycub_Address2), ((OptionSetValue)c.cub_Address2).Value),
                                             address3 = c.cub_Address3 == null ?
                                                null :
                                                Enum.GetName(typeof(cub_Countrycub_Address3), ((OptionSetValue)c.cub_Address3).Value),
                                             city = c.cub_City == null ?
                                                null :
                                                Enum.GetName(typeof(cub_Countrycub_City), ((OptionSetValue)c.cub_City).Value),
                                             state = c.cub_State == null ?
                                                null :
                                                Enum.GetName(typeof(cub_Countrycub_State), ((OptionSetValue)c.cub_State).Value),
                                             postalCode = c.cub_PostalCode == null ?
                                                null :
                                                Enum.GetName(typeof(cub_Countrycub_PostalCode), ((OptionSetValue)c.cub_PostalCode).Value)
                                         },
                                         states = (from st in osc.cub_StateOrProvinceSet
                                                   where st.cub_Country.Id.Equals(c.cub_CountryId)
                                                   select new JsonModels.WSKeyValue
                                                   {
                                                       key = st.cub_Abbreviation,
                                                       value = st.cub_StateName
                                                   }).ToList()
                                     }).ToList();
                    response.countries = countries;
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
            return response;
        }

        /// <summary>
        /// retreive the client customer statuses
        /// </summary>
        /// <returns></returns>
        public JsonModels.WSCustomerStatuses GetClientCustomerStatuses()
        {
            try
            {
                JsonModels.WSCustomerStatuses statusList = new JsonModels.WSCustomerStatuses();

                UtilityLogic utilLogic = new UtilityLogic();
                List<KeyValuePair<int, string>> optionSet = utilLogic.GetOptionSetMetaData(typeof(account_statuscode));
                
                // Fill in the list of Statuses available for this client
                statusList.customerStatuses = new List<JsonModels.WSCustomerStatus>();

                foreach (KeyValuePair<int, string> item in optionSet)
                {
                    JsonModels.WSCustomerStatus cs = new JsonModels.WSCustomerStatus();
                    cs.customerStatusId = item.Key;
                    cs.customerStatusEnum = item.Value;
                    cs.description = item.Value;
                    cs.shortDesc = item.Value;
                    statusList.customerStatuses.Add(cs);
                }

                return statusList;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
        }

        /// <summary>
        /// retreive the client customer settings
        /// </summary>
        /// <returns></returns>
        public JsonModels.WSClientCustomerSettings GetClientCustomerSettings()
        {
            try
            {
                JsonModels.WSClientCustomerSettings settings = new JsonModels.WSClientCustomerSettings();

                //get an array of Customer Types
                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                {
                    // Look up the Active countries for this client and also return their address requirements
                    var types = (from c in osc.cub_AccountTypeSet
                                 where c.statuscode.Equals(cub_accounttype_statuscode.Active)
                                 select c.cub_AccountName).ToList();
                 
                    // Call MSD
                    ContactLogic contactLogic = new ContactLogic(ConnectionInfo.CrmService);
                    WSUsernameRules usrnamerules = contactLogic.GetClientUsernameRules();
                    WSPasswordRules pwdrules = contactLogic.GetClientPasswordRules();

                    settings.customerSettings = new List<JsonModels.WSCustomerSettings>();

                    foreach (string t in types)
                    {
                        JsonModels.WSCustomerSettings item = new JsonModels.WSCustomerSettings();
                        item.customerType = t;
                        item.passwordRules = pwdrules;
                        item.usernameRules = usrnamerules;
                        settings.customerSettings.Add(item);
                    }
                }

                return settings;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
        }

        /// <summary>
        /// retrieve the security questions for a given username
        /// </summary>
        /// <param name="usernameData"></param>
        /// <returns></returns>
        public JsonModels.WSSecurityQuestionsWithStrings LookupSecurityQAs(JsonModels.WSSecurityQuestionsRetreiveRequest usernameData)
        {
            JsonModels.WSSecurityQuestionsWithStrings response = null;

            try
            {
                // validate the input data
                if (string.IsNullOrEmpty(usernameData.username))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

                response = new JsonModels.WSSecurityQuestionsWithStrings();

                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                {
                    // find the username credentials
                    var credentials = (from creds in osc.cub_CredentialSet where creds.cub_Username.Equals(usernameData.username) select new { contactId = creds.cub_ContactId, cred_status = creds.statuscode }).SingleOrDefault();

                    if (credentials != null)
                    {
                        //check for account locked, account inactive, and account pending activation and return such if found
                        if (credentials.cred_status != null)
                        {
                            if (credentials.cred_status.Equals(cub_credential_statuscode.Inactive))
                                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_ACCNT_INACTIVE, CUBConstants.Error.MSG_LOGIN_INACTIVE));
                            if (credentials.cred_status.Equals(cub_credential_statuscode.Locked))
                                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_ACCNT_LOCKED, CUBConstants.Error.MSG_LOGIN_LOCKED));
                            if (credentials.cred_status.Equals(cub_credential_statuscode.PendingActivation))
                                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_ACCNT_PENDING_ACTIVATION, CUBConstants.Error.MSG_LOGIN_PENDING_ACTIVATION));
                        }
                        else
                            throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_USERNAME_NOT_FOUND));

                        // find the security questions for the found credentials
                        var securityQA = (from sec in osc.cub_SecurityAnswerSet
                                          join secQues in osc.cub_SecurityQuestionSet on sec.cub_securityquestion_securityanswer.cub_SecurityQuestionId equals secQues.cub_SecurityQuestionId
                                          where sec.cub_ContactId.Equals(credentials.contactId)
                                          select new { sec.cub_SecurityAnswerDesc, secQues.cub_SecurityQuestionDesc }).ToList();

                        if (securityQA != null && securityQA.Count > 0)
                        {
                            // if we find security questions then assemble them into a name/value list for return
                            response.securityQuestions = new List<string>();

                            foreach (var sqa in securityQA)
                            {
                                response.securityQuestions.Add(sqa.cub_SecurityQuestionDesc);
                            }
                        }
                        else
                            throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_ACCNT_HAS_NO_SECURITY_QUESTIONS_ON_FILE, CUBConstants.Error.MSG_ACCOUNT_SECURITY_QA_MISSING));
                    }
                    else
                        throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_USERNAME_NOT_FOUND));
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return response;
        }

        //2.5.15 customer/password/forgot
        /// <summary>
        /// This API is used to answer security questions associated with the user's account
        /// </summary>
        /// <param name="validateSecurityQAData"></param>
        /// <returns></returns>
        public JsonModels.WSPasswordForgotResponse validateSecurityQAs(JsonModels.WSPasswordForgotRequest validateSecurityQAData)
        {
            JsonModels.WSPasswordForgotResponse response = null;

            try
            {
                // Validate the input data
                if (string.IsNullOrEmpty(validateSecurityQAData.username))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (validateSecurityQAData.securityQuestionAnswers == null || validateSecurityQAData.securityQuestionAnswers.Count == 0)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("SecurityQuestionAnswers", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                {
                    // Get the credentials
                    var credentials = (from creds in osc.cub_CredentialSet where creds.cub_Username.Equals(validateSecurityQAData.username) select new { contactId = creds.cub_ContactId }).SingleOrDefault();

                    if (credentials != null)
                    {
                        //TODO: before proceding to security QAs check for account locked, account inactive, and account pending activation and return such if found

                        // Found credentials so see if there are any security questions associated with this contact
                        var securityQA = (from sec in osc.cub_SecurityAnswerSet
                                          join secQues in osc.cub_SecurityQuestionSet on sec.cub_securityquestion_securityanswer.cub_SecurityQuestionId equals secQues.cub_SecurityQuestionId
                                          where sec.cub_ContactId.Equals(credentials.contactId)
                                          select new { sec.cub_SecurityAnswerDesc, secQues.cub_SecurityQuestionDesc }).ToList();

                        if (securityQA != null && securityQA.Count > 0)
                        {
                            // Security questions found so validate the QA pairs sent in
                            foreach (var inputqa in validateSecurityQAData.securityQuestionAnswers)
                            {
                                bool questionFound = false;
                                int i = 0;

                                while (!questionFound && i < securityQA.Count)
                                {
                                    if (string.Compare(securityQA[i].cub_SecurityQuestionDesc, inputqa.securityQuestion, true) == 0)
                                    {
                                        questionFound = true;

                                        if (string.Compare(securityQA[i].cub_SecurityAnswerDesc, inputqa.securityAnswer, true) != 0)
                                        {
                                            // Passed in answer for this question does not match
                                            // TODO: call the logic code to increment the number of failed attempts(login attempt failure count)...if the number of failed attempts reaches the configured
                                            // number then lock the account and return a fault exception to the caller of (error.account.is.locked) then notify
                                            // CustomerServiceApi ReportLoginAccountLocked (POST Operation).
                                            throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("SecurityQuestionAnswers", "errors.security.answer.mismatch", "One or more of the provided security answers did not match the answers on the account"));
                                        }
                                    }

                                    i++;
                                }

                                // if a question was passed in but we didn't find it
                                if (!questionFound)
                                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("SecurityQuestionAnswers", "errors.security.answer.mismatch", "One or more of the provided security answers did not match the answers on the account"));
                            }

                            // If we find no errors in validating the security questions then look for the contact and find the customer Id on the contact
                            //TODO: reset the number of failed attempts to zero
                            var Customer = (from cs in osc.ContactSet where cs.ContactId.Equals(credentials.contactId.Id) select cs.ParentCustomerId).ToList();

                            if (Customer != null && Customer[0] != null)
                            {
                                // We found the contact so generate a password token and a password token expiry time  
                                response = new JsonModels.WSPasswordForgotResponse();
                                if (credentials.contactId != null && credentials.contactId.Id != null) response.contactId = credentials.contactId.Id.ToString().ToUpper();
                                response.customerId = Customer[0].Id.ToString().ToUpper();
                                
                                //need to call the model dll to generate the token and set the data in the db
                                ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);

                                CubResponse<WSGenerateTokenResponse> cubResponse = new CubResponse<WSGenerateTokenResponse>();
                                cubResponse = conLogic.GenerateToken(Customer[0].Id, credentials.contactId.Id, cub_verificationtokentype.Password);

                                if (cubResponse.Success)
                                {
                                    if (cubResponse.ResponseObject != null)
                                    {
                                        response.verificationToken = cubResponse.ResponseObject.token;
                                        DateTime tokenExpiry = cubResponse.ResponseObject.tokenExpiryTime.Value;
                                        tokenExpiry = DateTime.SpecifyKind(tokenExpiry, DateTimeKind.Utc);
                                        response.tokenExpiryDateTime = tokenExpiry.ToString("o");

                                        //report asynchronously to CustomerServiceApi ReportVerificationToken (POST Operation).
                                        conLogic.RequestSendingOfVerificationToken(Customer[0].Id, credentials.contactId.Id, cubResponse.ResponseObject.token, tokenExpiry, cub_verificationtokentype.Password);
                                    }
                                }
                                else
                                {
                                    // Could not find the contact
                                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault(cubResponse.Errors[0].fieldName, cubResponse.Errors[0].errorKey, cubResponse.Errors[0].errorMessage));
                                }
                            }
                            else
                            {
                                // Could not find the contact
                                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Username not found"));
                            }
                        }
                        else
                        {
                            // Could not find any security QAs
                            throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("SecurityQuestionAnswers", "errors.security.not.possible", "Account has no security question answer pairs on file"));
                        }
                    }
                    else
                    {
                        // Could not find any credentials for this username
                        throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Username", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Username not found"));
                    }
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return response;
        }

        public Guid CreateCustomerContact(string customerId, WSCustomerContactRequest contactData)
        {
            try
            {
                Guid Customerguid = Guid.Empty;

                // Validate the input data
                if (string.IsNullOrEmpty(customerId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (!Guid.TryParse(customerId.ToUpper(), out Customerguid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND));
                if (contactData == null)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Contact", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (contactData.contact == null)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Contact", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

                DateTime tempTime;
                if (!string.IsNullOrEmpty(contactData.contact.dateOfBirth) && !DateTime.TryParse(contactData.contact.dateOfBirth, out tempTime))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("dateOfBirth", "errors.gereral.invalid.dateofbirth", "Invalid date of birth"));

                contactData.contact.customerId = Customerguid;

                // Call MSD to add the data
                ContactLogic contactLogic = new ContactLogic(ConnectionInfo.CrmService);
                var contact = contactLogic.CreateContact(contactData.contact);

                if (contact.ResponseObject.contactId.HasValue)
                {
                    Guid contactID = contact.ResponseObject.contactId.Value;
                    return contactID;
                }
                else
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(contact.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
        }

        /// <summary>
        /// Create a new customer and any contacts sent in to be atached to this customer
        /// </summary>
        /// <param name="customerData"></param>
        /// <returns></returns>
        public JsonModels.WSCreateCustomerResponse CreateCustomerContact(JsonModels.WSCreateCustomerRequest customerData)
        {
            JsonModels.WSCreateCustomerResponse response = null;

            try
            {
                // Validate the input data
                if (customerData == null)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Contact", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (customerData.contact == null)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Contact", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

                DateTime tempTime;
                if (!string.IsNullOrEmpty(customerData.contact.dateOfBirth) && !DateTime.TryParse(customerData.contact.dateOfBirth, out tempTime))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("dateOfBirth", "errors.gereral.invalid.dateofbirth", "Invalid date of birth"));

                WSCustomer cust = new WSCustomer();
                cust.contact = new WSCustomerContact();
                cust.contact = customerData.contact;
                cust.customerId = null;
                cust.customerType = customerData.customerType;
                cust.customerTypeId = null;

                // Call MSD to add the data
                AccountLogic acctLogic = new AccountLogic(ConnectionInfo.CrmService);
                CubResponse<WSCustomer> msdResponse = acctLogic.RegisterCustomer(cust);

                if (msdResponse.Success)
                {
                    response = new JsonModels.WSCreateCustomerResponse();
                    response.contactId = msdResponse.ResponseObject.contact.contactId.Value.ToString().ToUpper();
                    response.customerId = msdResponse.ResponseObject.customerId.Value.ToString().ToUpper();
                }
                else
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return response;
        }

        /// <summary>
        /// Get Customer information
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonModels.WSCustomer GetCustomer(string customerId, bool returnPrimaryContactsOnly = false)
        {
            Guid Customerguid;
            JsonModels.WSCustomer customer = null;

            try
            {
                // Validate the input
                if (string.IsNullOrEmpty(customerId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (!Guid.TryParse(customerId.ToUpper(), out Customerguid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND));

                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                {
                    // find the customer data
                    var Customer = (from cust in osc.AccountSet
                                    where cust.AccountId.Equals(Customerguid)
                                    select new
                                    {
                                        customerId = cust.AccountId,
                                        customerType = cust.cub_AccountTypeId,
                                        customerStatus = cust.StatusCode,
                                        insertedDtm = cust.CreatedOn,
                                        lastStatusDtm = cust.ModifiedOn,
                                        contacts = from cont in osc.ContactSet
                                                   where cont.ParentCustomerId.Equals(cust.AccountId)
                                                   select new
                                                   {
                                                       contactId = cont.ContactId.Value,
                                                       contactStatus = cont.StatusCode,
                                                       contactType = cont.cub_ContactTypeId,
                                                       title = cont.Salutation,
                                                       firstName = cont.FirstName,
                                                       lastName = cont.LastName,
                                                       middleInitial = cont.MiddleName != null ? cont.MiddleName.Substring(0, 1) : "",
                                                       phone1 = cont.Telephone1,
                                                       phone2 = cont.Telephone2,
                                                       phone3 = cont.Telephone3,
                                                       phone1Type = cont.cub_Phone1Type,
                                                       phone2Type = cont.cub_Phone2Type,
                                                       phone3Type = cont.cub_Phone3Type,
                                                       address = cont.cub_AddressId != null ? 
                                                                 from addr in osc.cub_AddressSet
                                                                 where addr.Id == cont.cub_AddressId.Id 
                                                                 select new
                                                                 {
                                                                     addressId = addr.cub_AddressId,
                                                                     address1 = addr.cub_Street1,
                                                                     address2 = addr.cub_Street2, //address3 = addr.cub_street3,
                                                                     city = addr.cub_City,
                                                                     state = addr.cub_StateProvinceId.Name,
                                                                     country = from cnt in osc.cub_CountrySet where cnt.cub_CountryId.Value.Equals(addr.cub_CountryId.Id) select new { country = cnt.cub_Alpha2 },
                                                                     postalCode = addr.cub_PostalCodeId.Name
                                                                 }
                                                                 : null,
                                                       customerId = cont.AccountId,
                                                       nameSuffixId = cont.Suffix,
                                                       email = cont.EMailAddress1,
                                                       dateOfBirth = cont.BirthDate,
                                                       personalIdentifier = "", //cont.cub_personalidentifier,
                                                       personalIdentifierType = "", //(Int32 or string 20?)cont.cub_personalidentifiertype,
                                                       credentials = from creds in osc.cub_CredentialSet where creds.cub_ContactId.Id.Equals(cont.ContactId) select new { creds.cub_Username, creds.cub_Pin, creds.statuscode },
                                                       securityQA = from sec in osc.cub_SecurityAnswerSet join secQues in osc.cub_SecurityQuestionSet on sec.cub_securityquestion_securityanswer.cub_SecurityQuestionId equals secQues.cub_SecurityQuestionId where sec.cub_ContactId.Id.Equals(cont.ContactId) select new { sec.cub_SecurityAnswerDesc, secQues.cub_SecurityQuestionDesc }
                                                   },
                                        addresses = from addr in osc.cub_AddressSet
                                                     where addr.cub_AccountAddressId.Id.Equals(cust.AccountId)
                                                     select new
                                                     {
                                                         addressId = addr.cub_AddressId,
                                                         address1 = addr.cub_Street1,
                                                         address2 = addr.cub_Street2, //address3 = addr.cub_street3,
                                                         city = addr.cub_City,
                                                         state = addr.cub_StateProvinceId.Name,
                                                         country = from cnt in osc.cub_CountrySet where cnt.cub_CountryId.Value.Equals(addr.cub_CountryId.Id) select new { country = cnt.cub_Alpha2 },
                                                         postalCode = addr.cub_PostalCodeId.Name
                                                     }
                                    });

                    customer = new JsonModels.WSCustomer();

                    if (Customer == null)
                    {
                        // no customer found
                        throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer not found"));
                    }

                    // if we found the customer then find the addresses and contacts associated with the customer
                    customer.addresses = new List<JsonModels.WSAddressExt>();
                    customer.contacts = new List<JsonModels.WSCustomerContactInfo>();

                    foreach (var q in Customer)
                    {
                        customer.customerId = q.customerId.Value.ToString().ToUpper();
                        customer.customerStatus = new JsonModels.WSKeyValue();
                        customer.customerStatus.key = (((OptionSetValue)q.customerStatus).Value).ToString();
                        customer.customerStatus.value = ((contact_statuscode)((OptionSetValue)q.customerStatus).Value).ToString("G");
                        customer.customerType = q.customerType != null ? q.customerType.Name : "";

                        if (q.insertedDtm != null)
                        {
                            DateTime iDtm = DateTime.SpecifyKind((DateTime)q.insertedDtm, DateTimeKind.Utc);
                            customer.insertedDtm = iDtm.ToString("o");
                        }

                        if (q.lastStatusDtm != null)
                        {
                            DateTime lSDtm = DateTime.SpecifyKind((DateTime)q.lastStatusDtm, DateTimeKind.Utc);
                            customer.lastStatusDtm = lSDtm.ToString("o");
                        }

                        if (q.contacts != null)
                        {
                            // if there are contacts for this customer then return the credentials for the contact
                            foreach (var i in q.contacts)
                            {
                                if (i.contactId != null)
                                {
                                    if (!returnPrimaryContactsOnly || (returnPrimaryContactsOnly && i.contactType != null && i.contactType.Name == "Primary"))
                                    {
                                        string pin = string.Empty;
                                        string username = string.Empty;
                                        string credential_status = string.Empty;

                                        // get the pin and username for this contact
                                        if (i.credentials != null)
                                        {
                                            foreach (var set in i.credentials)
                                            {
                                                pin = set.cub_Pin;
                                                username = set.cub_Username;
                                                credential_status = ((cub_credential_statuscode)((OptionSetValue)set.statuscode).Value).ToString("G");
                                            }
                                        }

                                        // get the security questions for this contact
                                        JsonModels.WSSecurityQA[] qas = null;

                                        if (i.securityQA != null)
                                        {
                                            qas = new JsonModels.WSSecurityQA[i.securityQA.AsEnumerable().Count()];
                                            int index = 0;

                                            foreach (var set in i.securityQA)
                                            {
                                                qas[index] = new JsonModels.WSSecurityQA();
                                                qas[index].securityAnswer = set.cub_SecurityAnswerDesc;
                                                qas[index++].securityQuestion = set.cub_SecurityQuestionDesc;
                                            }
                                        }

                                        JsonModels.WSAddressExt addressExt = null;

                                        // get the main address for this contact
                                        if (i.address != null)
                                        {
                                            addressExt = new JsonModels.WSAddressExt();
                                            addressExt.address1 = i.address.FirstOrDefault().address1;
                                            addressExt.address2 = i.address.FirstOrDefault().address2;
                                            //addressExt.address3 = i.address.FirstOrDefault().address3;
                                            addressExt.city = i.address.FirstOrDefault().city;
                                            if (i.address.FirstOrDefault().country != null)
                                                addressExt.country = i.address.FirstOrDefault().country.FirstOrDefault().country;
                                            addressExt.state = i.address.FirstOrDefault().state;
                                            addressExt.postalCode = i.address.FirstOrDefault().postalCode;
                                            addressExt.addressId = i.address.FirstOrDefault().addressId.Value.ToString().ToUpper();
                                        }

                                        // fill in the contact info and then add this contact to our return structure
                                        customer.contacts.Add(FillInContactInfo(i.phone1, i.phone2, i.phone3, i.phone1Type, i.phone2Type, i.phone3Type, addressExt, i.customerId,
                                            i.contactId, i.contactType, i.title, i.firstName, i.lastName, i.middleInitial, i.nameSuffixId, i.email, i.dateOfBirth, i.personalIdentifier, i.personalIdentifierType,
                                            pin, username, qas, i.contactStatus, credential_status));
                                    }
                                }
                            }
                        }

                        // add all the addresses attached to this customer
                        foreach (var a in q.addresses)
                        {
                            JsonModels.WSAddressExt addressE = new JsonModels.WSAddressExt();
                            addressE.address1 = a.address1;
                            addressE.address2 = a.address2;
                            //addressE.address3 = a.address3;
                            addressE.city = a.city;
                            if (a.country != null)
                                addressE.country = a.country.FirstOrDefault().country;
                            addressE.state = a.state;
                            addressE.postalCode = a.postalCode;
                            addressE.addressId = a.addressId.Value.ToString().ToUpper();
                            customer.addresses.Add(addressE);
                        }
                    }
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return customer;
        }

        /// <summary>
        /// Get Customer Contact information
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public JsonModels.WSCustomerContactInfo GetCustomerContact(string customerId, string contactId)
        {
            Guid Customerguid;
            Guid Contactguid;
            JsonModels.WSCustomerContactInfo returnInfo = null;

            try
            {
                CubLogger.Info("In GetCustomerContact");

                // validate intput
                if (string.IsNullOrEmpty(customerId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer ID is blank"));
                if (!Guid.TryParse(customerId.ToUpper(), out Customerguid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer ID is not a guid"));
                if (string.IsNullOrEmpty(contactId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The contact ID is blank"));
                if (!Guid.TryParse(contactId.ToUpper(), out Contactguid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The contact ID is not a guid"));

                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                {
                    string pin = string.Empty;
                    string username = string.Empty;
                    string credentials_status = string.Empty;

                    // get the contact information

                    var Contact =
                        (from c in osc.ContactSet
                         where c.ContactId.Value.Equals(Contactguid) && c.ParentCustomerId.Equals(Customerguid)
                         select new
                         {
                             phone1 = c.Telephone1,
                             phone2 = c.Telephone2,
                             phone3 = c.Telephone3,
                             phone1Type = c.cub_Phone1Type,
                             phone2Type = c.cub_Phone2Type,
                             phone3Type = c.cub_Phone3Type,
                             address = c.cub_AddressId != null ? 
                                    from addr in osc.cub_AddressSet
                                    where c.cub_AddressId != null && addr.Id == c.cub_AddressId.Id
                                    select new
                                    {
                                        addressId = addr.cub_AddressId,
                                        address1 = addr.cub_Street1,
                                        address2 = addr.cub_Street2,
                                        //address3 = addr.cub_street3,
                                        city = addr.cub_City,
                                        state = addr.cub_StateProvinceId.Name,
                                        country = from cnt in osc.cub_CountrySet where cnt.cub_CountryId.Value.Equals(addr.cub_CountryId.Id) select new { country = cnt.cub_Alpha2 },
                                        postalCode = addr.cub_PostalCodeId.Name
                                    } : null,
                             customerId = c.AccountId,
                             contactId = c.ContactId.Value,
                             contactType = c.cub_ContactTypeId,
                             title = c.Salutation,
                             firstName = c.FirstName,
                             lastName = c.LastName,
                             middleInitial = c.MiddleName != null ? c.MiddleName.Substring(0, 1) : "",
                             nameSuffixId = c.Suffix,
                             email = c.EMailAddress1,
                             dateOfBirth = c.BirthDate,
                             contactStatus = c.StatusCode,
                             personalIdentifier = "", //c.cub_personalidentifier != null ? c.cub_personalidentifier : "",
                             personalIdentifierType = "", //(Int32 or string 20?)c.cub_personalidentifiertype,
                             credentials = from creds in osc.cub_CredentialSet where creds.cub_ContactId.Equals(c.ContactId) select new { creds.cub_Username, creds.cub_Pin, creds.statuscode },
                             securityQA = from sec in osc.cub_SecurityAnswerSet join secQues in osc.cub_SecurityQuestionSet on sec.cub_securityquestion_securityanswer.cub_SecurityQuestionId equals secQues.cub_SecurityQuestionId where sec.cub_ContactId.Equals(c.ContactId) select new { sec.cub_SecurityAnswerDesc, secQues.cub_SecurityQuestionDesc }
                         })
                         .SingleOrDefault();

                    if (Contact == null)
                    {
                        // no contact found
                        CubLogger.Error("GetCustomerContact - contact not found");
                        throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The contact not found"));
                    }

                    if (Contact.credentials != null)
                    {
                        CubLogger.Info("GetCustomerContact - credentials");

                        // get the data from the credentials
                        foreach (var set in Contact.credentials)
                        {
                            pin = set.cub_Pin;
                            username = set.cub_Username;
                            credentials_status = ((cub_credential_statuscode)((OptionSetValue)set.statuscode).Value).ToString("G");
                        }
                    }

                    // get the security questions for this contact
                    JsonModels.WSSecurityQA[] qas = null;

                    if (Contact.securityQA != null)
                    {
                        CubLogger.Info("GetCustomerContact - get security questions");
                        qas = new JsonModels.WSSecurityQA[Contact.securityQA.AsEnumerable().Count()];
                        int index = 0;

                        foreach (var set in Contact.securityQA)
                        {
                            qas[index] = new JsonModels.WSSecurityQA();
                            qas[index].securityAnswer = set.cub_SecurityAnswerDesc;
                            qas[index++].securityQuestion = set.cub_SecurityQuestionDesc;
                        }
                    }

                    JsonModels.WSAddressExt addressExt = null;

                    // get the main address for this contact
                    if (Contact.address != null)
                    {
                        CubLogger.Info("GetCustomerContact - get address");
                        addressExt = new JsonModels.WSAddressExt();

                        addressExt.address1 = Contact.address.FirstOrDefault().address1;
                        addressExt.address2 = Contact.address.FirstOrDefault().address2;
                        //addressExt.address3 = Contact.address.FirstOrDefault().address3;
                        addressExt.city = Contact.address.FirstOrDefault().city;
                        if (Contact.address.FirstOrDefault().country != null)
                            addressExt.country = Contact.address.FirstOrDefault().country.FirstOrDefault().country;
                        addressExt.state = Contact.address.FirstOrDefault().state;
                        addressExt.postalCode = Contact.address.FirstOrDefault().postalCode;
                        addressExt.addressId = Contact.address.FirstOrDefault().addressId.Value.ToString().ToUpper();
                    }

                    if (Contact.contactStatus != null && Contact.contactStatus.Equals(contact_statuscode.Terminated))
                    {
                        CubLogger.Error("GetCustomerContact - contact status blank or terminated");
                        throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_ACCNT_CLOSED_OR_PENDING_CLOSURE, CUBConstants.Error.MSG_ACCNT_CLOSED_OR_PENDING));
                    }

                    // call a function to fill in the return object
                    returnInfo = FillInContactInfo( Contact.phone1, Contact.phone2, Contact.phone3, Contact.phone1Type, Contact.phone2Type, Contact.phone3Type, addressExt, Contact.customerId,
                        Contact.contactId, Contact.contactType, Contact.title, Contact.firstName, Contact.lastName, Contact.middleInitial, Contact.nameSuffixId, Contact.email, Contact.dateOfBirth,
                        Contact.personalIdentifier, Contact.personalIdentifierType, pin, username, qas, Contact.contactStatus, credentials_status);
                }

            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return returnInfo;
        }

        /// <summary>
        /// Fill in the return object with the passed in information
        /// </summary>
        /// <param name="phone1"></param>
        /// <param name="phone2"></param>
        /// <param name="phone3"></param>
        /// <param name="phone1Type"></param>
        /// <param name="phone2Type"></param>
        /// <param name="phone3Type"></param>
        /// <param name="address"></param>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="contactType"></param>
        /// <param name="title"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="middleInitial"></param>
        /// <param name="nameSuffix"></param>
        /// <param name="email"></param>
        /// <param name="dateOfBirth"></param>
        /// <param name="personalIdentifier"></param>
        /// <param name="personalIdentifierType"></param>
        /// <param name="pin"></param>
        /// <param name="username"></param>
        /// <param name="securityQAs"></param>
        /// <param name="status_code"></param>
        /// <param name="credentials_status"></param>
        /// <returns></returns>
        private JsonModels.WSCustomerContactInfo FillInContactInfo(string phone1, string phone2, string phone3, object phone1Type, object phone2Type, object phone3Type, JsonModels.WSAddressExt address,
            EntityReference customerId, Guid contactId, EntityReference contactType, string title, string firstName, string lastName, string middleInitial, string nameSuffix, string email, DateTime? dateOfBirth,
            string personalIdentifier, string personalIdentifierType, string pin, string username, JsonModels.WSSecurityQA[] securityQAs, object status_code, string credentials_status)
        {
            JsonModels.WSCustomerContactInfo contact = null;

            try
            {
                // copy in the name and address id
                contact = new JsonModels.WSCustomerContactInfo();
                int phoneCount = 0;
                contact.contactId = contactId.ToString().ToUpper();
                contact.contactType = contactType != null ? contactType.Name : "";
                contact.name = new JsonModels.WSName();
                contact.name.title = title;
                contact.name.firstName = firstName;
                contact.name.middleInitial = middleInitial;
                contact.name.lastName = lastName;
                contact.name.nameSuffix = nameSuffix;

                if (status_code != null) contact.contactStatus = ((contact_statuscode)((OptionSetValue)status_code).Value).ToString("G");

                contact.credentialStatus = credentials_status;

                //see if there is any address data and if so fill it in
                if (address != null)
                {
                    contact.address = new JsonModels.WSAddressExt();
                    contact.address = address;
                }

                // see if there is any phone data and if so fill it in
                if (!string.IsNullOrEmpty(phone1) || !string.IsNullOrEmpty(phone2) || !string.IsNullOrEmpty(phone3))
                {
                    if (!string.IsNullOrEmpty(phone1)) phoneCount++;
                    if (!string.IsNullOrEmpty(phone2)) phoneCount++;
                    if (!string.IsNullOrEmpty(phone3)) phoneCount++;

                    contact.phone = new Model.NIS.WSPhoneExt[phoneCount];
                    int phone = 0;

                    // fill in phone 1
                    if (!string.IsNullOrEmpty(phone1))
                    {
                        contact.phone[phone] = new Model.NIS.WSPhoneExt();
                        contact.phone[phone].number = phone1;
                        contact.phone[phone].country = string.Empty;
                        contact.phone[phone].displayNumber = string.Empty;

                        if (phone1Type != null) contact.phone[phone].type = ((cub_phonetype)((OptionSetValue)phone1Type).Value).ToString("G");

                        phone++;
                    }

                    // fill in phone 2
                    if (!string.IsNullOrEmpty(phone2))
                    {
                        contact.phone[phone] = new Model.NIS.WSPhoneExt();
                        contact.phone[phone].number = phone2;
                        contact.phone[phone].country = string.Empty;
                        contact.phone[phone].displayNumber = string.Empty;

                        if (phone2Type != null) contact.phone[phone].type = ((cub_phonetype)((OptionSetValue)phone2Type).Value).ToString("G");

                        phone++;
                    }

                    // fill in phone 3
                    if (!string.IsNullOrEmpty(phone3))
                    {
                        contact.phone[phone] = new Model.NIS.WSPhoneExt();
                        contact.phone[phone].number = phone3;
                        contact.phone[phone].country = string.Empty;
                        contact.phone[phone].displayNumber = string.Empty;

                        if (phone3Type != null) contact.phone[phone].type = ((cub_phonetype)((OptionSetValue)phone3Type).Value).ToString("G");

                        phone++;
                    }
                }

                contact.email = email;

                // see if the date of birth is given and either fill it in or return null for it
                if (dateOfBirth != null)
                {
                    DateTime dob = DateTime.SpecifyKind((DateTime)dateOfBirth, DateTimeKind.Utc);
                    contact.dateOfBirth = dob.ToString("o");
                }
                else
                {
                    contact.dateOfBirth = null;
                }

                // fill in personal identifier information
                contact.personalIdentifierInfo = new JsonModels.WSPersonalIdentifier();
                contact.personalIdentifierInfo.personalIdentifier = personalIdentifier;

                if (personalIdentifierType == null)
                    contact.personalIdentifierInfo.personalIdentifierType = string.Empty;
                else
                    contact.personalIdentifierInfo.personalIdentifierType = personalIdentifierType.ToString();

                // fill in username and pin 
                contact.username = username;
                contact.pin = pin;

                // if hte user has security questions and answers on file then fill those in on the return
                if (securityQAs != null && securityQAs.Count() > 0)
                {
                    if (contact.securityQAs == null) contact.securityQAs = new JsonModels.WSSecurityQA[securityQAs.Count()];
                    int index = 0;

                    foreach (JsonModels.WSSecurityQA s in securityQAs)
                    {
                        contact.securityQAs[index] = new JsonModels.WSSecurityQA();
                        contact.securityQAs[index].securityQuestion = s.securityQuestion;
                        contact.securityQAs[index++].securityAnswer = s.securityAnswer;
                    }
                }
                else
                {
                    // no security questions and answers on file
                    contact.securityQAs = null;
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return contact;
        }

        /// <summary>
        /// Find contact by data: Forgot username so look up the contact by other data
        /// </summary>
        /// <param name="contactData"></param>
        public void FindContactByData(JsonModels.WSForgotUsername contactData)
        {
            // validate the input data
            if (contactData == null)
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Email/Phone", CUBConstants.Error.ERR_EMAIL_OR_PHONE_REQUIRED, CUBConstants.Error.MSG_EMAIL_OR_PHONE_REQUIRED));
            if (string.IsNullOrEmpty(contactData.email))
            {
                if (string.IsNullOrEmpty(contactData.phone))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Email/Phone", CUBConstants.Error.ERR_EMAIL_OR_PHONE_REQUIRED, CUBConstants.Error.MSG_EMAIL_OR_PHONE_REQUIRED));
            }
            if (!string.IsNullOrEmpty(contactData.firstName) && contactData.firstName.Length > 60)
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT));
            if (!string.IsNullOrEmpty(contactData.lastName) && contactData.lastName.Length > 60)
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT));
            if (!string.IsNullOrEmpty(contactData.pin) && contactData.pin.Length > 9)
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT));
            if (!string.IsNullOrEmpty(contactData.postalCode) && contactData.postalCode.Length > 10)
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT));

            try
            {
                // call MSD to find the contact
                ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);
                CubResponse<WSForgotUsernameResponse> msdResponse = conLogic.ForgotUsername(contactData.firstName, contactData.lastName, contactData.email, contactData.phone, contactData.pin, contactData.postalCode);

                if (!msdResponse.Success)
                {
                    throw new FaultException<WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                // log any faults
                CubLogger.Error(ex.Message, ex);
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
        }

        /// <summary>
        /// Update a contact with new data
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="contactData"></param>
        public void UpdateCustomerContact(string customerId, string contactId, Model.MSDApi.WSCustomerContact contactData)
        {
            Guid customerGuid;
            Guid contactGuid;

            try
            {
                // validate the input data
                if (string.IsNullOrEmpty(customerId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer not found"));
                if (!Guid.TryParse(customerId.ToUpper(), out customerGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer not found"));
                if (string.IsNullOrEmpty(contactId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The contact not found"));
                if (!Guid.TryParse(contactId.ToUpper(), out contactGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The contact not found"));

                // call MSD to further validate the data and update the contact
                ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);
                contactData.contactId = contactGuid;
                contactData.customerId = customerGuid;
                CubResponse<WSCustomerContact> msdResponse = conLogic.UpdateContact(contactData);

                if (!msdResponse.Success)
                {
                    throw new FaultException<WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return;
        }

        /// <summary>
        /// Return the address information for the matching customer's address
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public Model.NIS.WSAddressExt GetCustomerAddress(string customerId, string addressId)
        {
            Guid customerGuid;
            Guid addressGuid;
            Model.NIS.WSAddressExt address = null;

            try
            {
                // validate the input
                if (string.IsNullOrEmpty(customerId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, "CustomerId is null"));
                if (!Guid.TryParse(customerId.ToUpper(), out customerGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Customer not found"));
                if (string.IsNullOrEmpty(addressId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("AddressId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, "AddressId is null"));
                if (!Guid.TryParse(addressId.ToUpper(), out addressGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("AddressId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Address not found"));

                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                {
                    // find the address date
                    var Address =
                        (from c in osc.cub_AddressSet
                         join cnt in osc.cub_CountrySet on c.cub_CountryId.Id equals cnt.cub_CountryId into joined
                         from j in joined.DefaultIfEmpty()
                         where c.cub_AccountAddressId.Equals(customerGuid) && c.cub_AddressId.Value.Equals(addressGuid)
                         select new
                         {
                             addressId = c.cub_AddressId,
                             address1 = c.cub_Street1,
                             address2 = c.cub_Street2,
                             address3 = c.cub_Street3,
                             city = c.cub_City,
                             state = c.cub_StateProvinceId.Name,
                             country = j.cub_Alpha2,
                             postalCode = c.cub_PostalCodeId.Name
                         })
                         .SingleOrDefault();

                    if (Address == null)
                    {
                        // address not found
                        throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("AddressId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_ADDRESS_NOT_FOUND));
                    }

                    // fill in the return address information
                    address = new Model.NIS.WSAddressExt();
                    address.addressId = Address.addressId.Value.ToString().ToUpper();
                    address.address1 = Address.address1;
                    address.address2 = Address.address2;
                    address.address3 = Address.address3;
                    address.city = Address.city;
                    address.state = Address.state;
                    address.country = Address.country;
                    address.postalCode = Address.postalCode;
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return address;
        }

        /// <summary>
        /// Customer Address Update to update address information for an existing address
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <param name="addressUpdateData"></param>
        public void CustomerAddressUpdate(string customerId, string addressId, Model.MSDApi.WSAddress addressUpdateData)
        {
            Guid customerGuid;
            Guid addressGuid;

            //validate the input
            if (string.IsNullOrEmpty(customerId))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, "CustomerId is null"));
            if (!Guid.TryParse(customerId.ToUpper(), out customerGuid))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Customer not found"));
            if (string.IsNullOrEmpty(addressId))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("AddressId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, "AddressId is null"));
            if (!Guid.TryParse(addressId.ToUpper(), out addressGuid))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("AddressId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "AddressId not found"));
            if (addressUpdateData == null)
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Address", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

            try
            {
                // call MSD to update the information
                AddressLogic al = new AddressLogic(ConnectionInfo.CrmService);
                CubResponse<WSAddress> ret_val = al.UpdateAddress(addressUpdateData, customerGuid, addressGuid);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
        }

        /// <summary>
        /// Customer Address Delete to remove an address associated with a customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public JsonModels.WSAddressDeleteResponse CustomerAddressDelete(string customerId, string addressId)
        {
            Guid customerGuid;
            Guid addressGuid;

            JsonModels.WSAddressDeleteResponse resp = null;

            //validate the input
            if (string.IsNullOrEmpty(customerId))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, "CustomerId is null"));
            if (!Guid.TryParse(customerId.ToUpper(), out customerGuid))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Customer not found"));
            if (string.IsNullOrEmpty(addressId))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, "CustomerId is null"));
            if (!Guid.TryParse(addressId.ToUpper(), out addressGuid))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Customer not found"));

            try
            {
                // call MSD to remove the address
                AddressLogic al = new AddressLogic(ConnectionInfo.CrmService);
                CubResponse<WSAddressDeleteResponse> msdResponse = al.DeleteAddress(customerGuid, addressGuid);

                if (msdResponse.Success)
                {
                    WSAddressDeleteResponse response = msdResponse.ResponseObject;

                    if (response != null) 
                    {
                        //if there are contact dependencies on this address then send them back to the caller
                        resp = new JsonModels.WSAddressDeleteResponse();
                        resp.contactDependencies = new List<JsonModels.WSCustomerContactInfo>();

                        foreach (var r in response.contactDependencies)
                        {
                            // since the objects are not the same we need to copy the items we need one at a time
                            JsonModels.WSCustomerContactInfo info = new JsonModels.WSCustomerContactInfo();
                            info.contactId = r.contactId.ToUpper();
                            info.address = null;
                            info.contactType = r.contactType;
                            info.dateOfBirth = r.dateOfBirth;
                            info.email = r.email;
                            if (r.name != null)
                            {
                                info.name = new JsonModels.WSName();
                                info.name.firstName = r.name.firstName;
                                info.name.lastName = r.name.lastName;
                                info.name.middleInitial = r.name.middleInitial;
                                info.name.nameSuffix = r.name.nameSuffix;
                                info.name.title = r.name.title;
                            }
                            info.personalIdentifierInfo = null;
                            info.phone = null;
                            info.pin = r.pin;
                            info.securityQAs = null;
                            info.username = r.username;

                            resp.contactDependencies.Add(info);
                        }
                    }
                }
                else
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return resp;
        }

        /// <summary>
        /// Customer Address Add to add a new address to a customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressAddData"></param>
        /// <returns></returns>
        public Model.MSDApi.WSAddressCreationResponse CustomerAddressAdd(string customerId, Model.MSDApi.WSAddress addressAddData)
        {
            Guid customerGuid;

            Model.MSDApi.WSAddressCreationResponse resp = null;

            if (string.IsNullOrEmpty(customerId))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, "CustomerId is null"));
            if (!Guid.TryParse(customerId.ToUpper(), out customerGuid))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Customer not found"));
            if (addressAddData == null)
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Address", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

            try
            {
                AddressLogic al = new AddressLogic(ConnectionInfo.CrmService);
                CubResponse<WSAddress> msdResponse = al.ValidateAndCreateAddress(addressAddData, customerGuid);

                if (msdResponse.Success)
                {
                    resp = new WSAddressCreationResponse
                    {
                        addressId = ((WSAddress)msdResponse.ResponseObject).addressId.Value.ToString().ToUpper(),
                        isExistingAddress = ((WSAddress)msdResponse.ResponseObject).isExistingAddress
                    };
                }
                else
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }

            return resp;
        }

        /// <summary>
        /// Prevalidate username and password 
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public JsonModels.WSCredentailsPrevalidateResults CustomerPrevalidate(JsonModels.WSCredentials credentials)
        {
            JsonModels.WSCredentailsPrevalidateResults results = new JsonModels.WSCredentailsPrevalidateResults();

            if (credentials != null)
            {
                try
                {
                    // validate the username
                    ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);
                    CubResponse<WSCredentailsPrevalidateResults> preValidateResults = conLogic.ValidateUsernameAndPassword(credentials.username, credentials.password);
                    if (preValidateResults.ResponseObject != null)
                    {
                        if (preValidateResults.ResponseObject.usernameErrors != null && preValidateResults.ResponseObject.usernameErrors.Count > 0)
                            results.usernameErrors = preValidateResults.ResponseObject.usernameErrors;
                        else
                            results.usernameErrors = null;

                        results.isUsernameValid = preValidateResults.ResponseObject.isUsernameValid;

                        if (preValidateResults.ResponseObject.passwordErrors != null && preValidateResults.ResponseObject.passwordErrors.Count > 0)
                            results.passwordErrors = preValidateResults.ResponseObject.passwordErrors;
                        else
                            results.passwordErrors = null;

                        results.isPasswordValid = preValidateResults.ResponseObject.isPasswordValid;

                        //username already being used by someone
                        if (!preValidateResults.ResponseObject.isUsernameAvailable)
                        {
                            results.usernameErrors = new List<WSError>();
                            WSError error = new WSError();
                            error.key = "errors.general.value.duplicate";
                            error.message = "Value already exists";
                            results.usernameErrors.Add(error);
                            results.isUsernameValid = false;
                        }
                    }
                }
                catch (FaultException<WSMSDFault> ex)
                {
                    throw ex;
                }
                catch (Exception e)
                {
                    // log any exceptions
                    CubLogger.Error(e.Message,e);
                    throw new FaultException<WSMSDFault>(new WSMSDFault("MSD", e.Source, e.Message));
                }
            }
            else
            {
                // we were given null data
                results.isUsernameValid = false;
                results.usernameErrors = new List<WSError>();
                WSError username_error = new WSError();
                username_error.key = CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL;
                username_error.message = CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET;
                results.usernameErrors.Add(username_error);
                results.isPasswordValid = false;
                results.passwordErrors = new List<WSError>();
                WSError password_error = new WSError();
                password_error.key = CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL;
                password_error.message = CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET;
                results.passwordErrors.Add(password_error);
            }

            return results;
        }

        public void CredentialsValidate(string customerId, string contactId, string pin)
        {
            if (string.IsNullOrWhiteSpace(customerId))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("customerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
            if (string.IsNullOrWhiteSpace(contactId))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("contactId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
            if (string.IsNullOrWhiteSpace(pin))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("pin", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
            ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);
            conLogic.CredentialsValidate(customerId, contactId, pin);
        }

        /// <summary>
        /// Authenticate the submitted credentials
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public JsonModels.WSAuthenticateResponse CustomerAuthenticate(JsonModels.WSAuthenticateRequest credentials)
        {
            JsonModels.WSAuthenticateResponse results = new JsonModels.WSAuthenticateResponse();

            if (credentials != null && !string.IsNullOrEmpty(credentials.username))
            {
                string encptedPwd = string.Empty;

                if (!string.IsNullOrEmpty(credentials.password))
                    encptedPwd = UtilityLogic.EncryptTripleDES(credentials.password);

                try
                {
                    ContactLogic clogic = new ContactLogic(ConnectionInfo.CrmService);
                    CubResponse<WSForgotUsernameResponse> FindUserCredentialsResponse = clogic.GetCustomerCreds(credentials.username);

                    if (FindUserCredentialsResponse.ResponseObject != null && FindUserCredentialsResponse.ResponseObject.credStatus.Equals(cub_credential_statuscode.Locked))
                    {
                        FindUserCredentialsResponse.Errors.Add(new WSMSDFault(string.Empty, CUBConstants.Error.ERR_ACCNT_LOCKED,
                                                    CUBConstants.Error.MSG_LOGIN_LOCKED));
                    }

                    if (FindUserCredentialsResponse.ResponseObject != null && FindUserCredentialsResponse.ResponseObject.credStatus.Equals(cub_credential_statuscode.PendingActivation))
                    {
                        FindUserCredentialsResponse.Errors.Add(new WSMSDFault(string.Empty, CUBConstants.Error.ERR_ACCNT_PENDING_ACTIVATION,
                                                    CUBConstants.Error.MSG_LOGIN_PENDING_ACTIVATION));
                        CheckTemporaryPasswordExpired(credentials, FindUserCredentialsResponse);
                    }

                    Guid customerGuid = Guid.Empty;
                    if (FindUserCredentialsResponse.ResponseObject != null && FindUserCredentialsResponse.ResponseObject.contactId != null && !Guid.TryParse(FindUserCredentialsResponse.ResponseObject.customerId.ToUpper(), out customerGuid))
                        throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer not found"));

                    if (FindUserCredentialsResponse.Success && customerGuid != Guid.Empty)
                    {
                        if (FindUserCredentialsResponse.ResponseObject != null && FindUserCredentialsResponse.ResponseObject.contactId != null && FindUserCredentialsResponse.ResponseObject.credentialId != null)
                        {
                            // found the username so check the password on the account with what was passed in
                            if (string.Compare(encptedPwd, FindUserCredentialsResponse.ResponseObject.existingPassword, false) == 0)
                            {
                                //password is good so find the account and account information
                                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                                {
                                    var AccountInfo =
                                    (from act in osc.AccountSet
                                     join acctType in osc.cub_AccountTypeSet on act.cub_AccountTypeId.Id equals acctType.cub_AccountTypeId
                                     where act.AccountId.Value.Equals(customerGuid)
                                     select new { accountTypeName = acctType.cub_AccountName }).SingleOrDefault();

                                    if (AccountInfo != null)
                                    {
                                        if (string.IsNullOrEmpty(credentials.customerType) || (!string.IsNullOrEmpty(credentials.customerType) && (string.Compare(credentials.customerType, AccountInfo.accountTypeName, true) == 0)))
                                        {
                                            if (FindUserCredentialsResponse.ResponseObject.forceChangePwd == true)
                                            {
                                                results.authCode = CUBConstants.Error.SUCCESS_CHANGE_PASSWORD;
                                            }
                                            else
                                            {
                                                results.authCode = CUBConstants.Error.SUCCESS;
                                            }

                                            results.contactId = FindUserCredentialsResponse.ResponseObject.contactId.ToString().ToUpper();
                                            results.customerId = FindUserCredentialsResponse.ResponseObject.customerId.ToString().ToUpper();
                                            results.customerType = AccountInfo.accountTypeName;
                                            clogic.CallResetFailedLoginAttempts(FindUserCredentialsResponse.ResponseObject.credentialId.ToUpper(), CUBConstants.GlobalCallingFromConstants.CALLING_FROM_WEB);
                                        }
                                        else
                                        {
                                            //credential status found but passed in account type does not match
                                            results.authCode = CUBConstants.Error.FAILURE;
                                            results.authErrors = new List<JsonModels.WSError>();
                                            JsonModels.WSError auth_error = new JsonModels.WSError();
                                            auth_error.key = CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD;
                                            auth_error.message = CUBConstants.Error.MSG_USERNAME_AND_PASSWORD_INVALID;
                                            results.authErrors.Add(auth_error);
                                        }
                                    }
                                    else
                                    {
                                        // no account information found
                                        if (string.IsNullOrEmpty(credentials.customerType))
                                        {
                                            if (FindUserCredentialsResponse.ResponseObject.forceChangePwd == true)
                                            {
                                                results.authCode = CUBConstants.Error.SUCCESS_CHANGE_PASSWORD;
                                            }
                                            else
                                            {
                                                results.authCode = CUBConstants.Error.SUCCESS;
                                            }

                                            results.contactId = FindUserCredentialsResponse.ResponseObject.contactId.ToString().ToUpper();
                                            results.customerId = FindUserCredentialsResponse.ResponseObject.customerId.ToString().ToUpper();
                                            results.customerType = null;
                                        }
                                        else
                                        {
                                            //credential status found but passed in account type cannot be mathcd since account was not found
                                            results.authCode = CUBConstants.Error.FAILURE;
                                            results.authErrors = new List<JsonModels.WSError>();
                                            JsonModels.WSError auth_error = new JsonModels.WSError();
                                            auth_error.key = CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD;
                                            auth_error.message = CUBConstants.Error.MSG_USERNAME_AND_PASSWORD_INVALID;
                                            results.authErrors.Add(auth_error);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // wrong password so bump the failed attempt and return
                                CubResponse<WSEmptyClass> BumpFailedResponse = clogic.CallIsFailedLoginAttemptsLimitReached(FindUserCredentialsResponse.ResponseObject.credentialId.ToUpper(), true);
                                
                                if (!BumpFailedResponse.Success && string.Compare(BumpFailedResponse.Errors[0].errorKey, CUBConstants.Error.ERR_ACCNT_LOCKED) == 0)
                                {
                                    //credential record is locked
                                    results.authCode = CUBConstants.Error.FAILURE;
                                    results.authErrors = new List<JsonModels.WSError>();
                                    JsonModels.WSError auth_error = new JsonModels.WSError();
                                    auth_error.key = CUBConstants.Error.ERR_ACCOUNT_LOCKED;
                                    auth_error.message = CUBConstants.Error.MSG_LOGIN_LOCKED;
                                    results.authErrors.Add(auth_error);

                                    try
                                    {
                                        Guid customer;
                                        Guid contact;
                                        Guid.TryParse(FindUserCredentialsResponse.ResponseObject.customerId, out customer);
                                        Guid.TryParse(FindUserCredentialsResponse.ResponseObject.contactId, out contact);

                                        clogic.RequestSendingCustomerLockoutNotification(customer, contact);
                                    }
                                    catch (FaultException<Model.MSDApi.WSMSDFault> ex)
                                    {
                                        throw ex;
                                    }
                                }
                                else
                                {
                                    results.authCode = CUBConstants.Error.FAILURE;
                                    results.authErrors = new List<JsonModels.WSError>();
                                    JsonModels.WSError auth_error = new JsonModels.WSError();
                                    auth_error.key = CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD;
                                    auth_error.message = CUBConstants.Error.MSG_USERNAME_AND_PASSWORD_INVALID;
                                    results.authErrors.Add(auth_error);
                                }
                            }
                        }
                        else
                        {
                            //response object is null or contact id is null
                            results.authCode = CUBConstants.Error.FAILURE;
                            results.authErrors = new List<JsonModels.WSError>();
                            JsonModels.WSError auth_error = new JsonModels.WSError();
                            auth_error.key = CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD;
                            auth_error.message = CUBConstants.Error.MSG_USERNAME_AND_PASSWORD_INVALID;
                            results.authErrors.Add(auth_error);
                        }
                    }
                    else
                    {
                        // failed ... send back why
                        results.authCode = CUBConstants.Error.FAILURE;
                        results.authErrors = new List<JsonModels.WSError>();

                        if (string.Compare(CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND, FindUserCredentialsResponse.Errors[0].errorKey) == 0)
                        {
                            results.authErrors.Add(new JsonModels.WSError(CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD, CUBConstants.Error.MSG_USERNAME_AND_PASSWORD_INVALID));
                        }
                        else
                        {
                            // Adding all the authentication error to the reponse
                            foreach (WSMSDFault fault in FindUserCredentialsResponse.Errors)
                            {
                                results.authErrors.Add(new JsonModels.WSError(fault.errorKey, fault.errorMessage));
                            }
                        }
                    }
                }
                catch (FaultException<Model.MSDApi.WSMSDFault> ex)
                {
                    throw ex;
                }
                catch (Exception e)
                {
                    // log any exceptions
                    CubLogger.Error(e.Message, e);
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
                }
            }
            else
            {
                // null data was passed in
                results.authCode = CUBConstants.Error.FAILURE;
                results.authErrors = new List<JsonModels.WSError>();
                JsonModels.WSError auth_error = new JsonModels.WSError();
                auth_error.key = CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD;
                auth_error.message = CUBConstants.Error.MSG_USERNAME_AND_PASSWORD_INVALID;
                results.authErrors.Add(auth_error);
            }

            return results;
        }

        private void CheckTemporaryPasswordExpired(JsonModels.WSAuthenticateRequest credentials, CubResponse<WSForgotUsernameResponse> FindUserCredentialsResponse)
        {
            if (FindUserCredentialsResponse.ResponseObject.credStatus.Equals(cub_credential_statuscode.PendingActivation))
            {
                using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                {
                    /*
                     * Retrieving the latest active verification token
                     */
                    var passwordVerification = (from vt in osc.cub_VerificationTokenSet
                                                join c in osc.cub_CredentialSet
                                                  on vt.cub_CredentialsId.Id equals c.cub_CredentialId
                                                where c.cub_Username.Equals(credentials.username)
                                                orderby vt.cub_TokenExpirationDate descending
                                                select new
                                                {
                                                    ExpirationDate = vt.cub_TokenExpirationDate
                                                }
                                           ).ToList();
                    if (passwordVerification != null &&
                        passwordVerification.Count != 0)
                    {
                        if (passwordVerification[0].ExpirationDate.HasValue &&
                            passwordVerification[0].ExpirationDate.Value.ToUniversalTime().CompareTo(DateTime.Today.ToUniversalTime()) <= 0)
                        {
                            FindUserCredentialsResponse.Errors.Add(new WSMSDFault(string.Empty,
                                                                   CUBConstants.Error.ERR_ACCNT_IS_EXPIRED,
                                                                   CUBConstants.Error.MSG_TEMPORARY_PASSWORD_IS_EXPIRED));
                        }
                    }
                }
            }
        }

        public void CustomerContactStatus(string customerId, string contactId, string status)
        {
            try
            {
                contact_statuscode statusCode;
                status = status.ToLower();
                if (status.Equals("activate"))
                {
                    statusCode = contact_statuscode.Active;
                }
                else if (status.Equals("suspend"))
                {
                    statusCode = contact_statuscode.Suspended;
                }
                else if (status.Equals("terminate"))
                {
                    statusCode = contact_statuscode.Terminated;
                }
                else
                {
                    throw new Exception($"Invalid Status: {status}");
                }

                LogicBase logic = new LogicBase(ConnectionInfo.CrmService);
                logic.SetState2(new Guid(contactId), Contact.EntityLogicalName, (int) ContactState.Active, (int) statusCode);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);

                throw e;
            }
        }

        /// <summary>
        /// Generate a password token: this API os for generating a password token to reset a password
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <returns>JasonModels.WSGenerateTokenResponse</returns>
        public JsonModels.WSGenerateTokenResponse GenerateToken(JsonModels.WSVerificationToken tokenData, string customerId, string contactId)
        {
            Guid customerGuid;
            Guid contactGuid;
            JsonModels.WSGenerateTokenResponse response = null;

            try
            {
                // validate passed in ids
                if (string.IsNullOrEmpty(customerId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer not found"));
                if (!Guid.TryParse(customerId.ToUpper(), out customerGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer not found"));

                if (string.IsNullOrEmpty(contactId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CONTACT_NOT_FOUND));
                if (!Guid.TryParse(contactId.ToUpper(), out contactGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CONTACT_NOT_FOUND));

                if (tokenData == null)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Verification Token", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (string.IsNullOrEmpty(tokenData.verificationType))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Verification Token", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

                cub_verificationtokentype tokenType = Utility.getTokenType(tokenData.verificationType);

                string mobileDeviceToken = null;

                if (tokenType == cub_verificationtokentype.Mobile && !string.IsNullOrEmpty(tokenData.mobileToken))
                {
                    mobileDeviceToken = tokenData.mobileToken;
                }

                //call MSD to generate the password token 
                ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);
                CubResponse<WSGenerateTokenResponse> msdResponse = conLogic.GenerateToken(customerGuid, contactGuid, tokenType, mobileDeviceToken);

                //check for key == "success" and value to have the token and the expiry date for the token
                if (msdResponse.Success)
                {
                    if (msdResponse.ResponseObject != null)
                    {
                        // fill in the token and the expiry for the returned data
                        response = new JsonModels.WSGenerateTokenResponse();
                        response.verificationToken = msdResponse.ResponseObject.token;
                        DateTime expiry = msdResponse.ResponseObject.tokenExpiryTime.Value;
                        expiry = DateTime.SpecifyKind(expiry, DateTimeKind.Utc);
                        response.tokenExpiryDateTime = expiry.ToString("o");

                        //do not report for NewCustomerPassword as NIS already sends a new contact email with new customer password token information
                        if (tokenType != cub_verificationtokentype.NewCustomerPassword)
                            conLogic.RequestSendingOfVerificationToken(customerGuid, contactGuid, msdResponse.ResponseObject.token, expiry, tokenType);
                    }
                }
                else
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);

                throw e;
            }

            return response;
        }

        /// <summary>
        /// Change the password for a contact 
        /// </summary>
        /// <param name="changePasswordData"></param>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        public void ContactChangePassword(JsonModels.WSPasswordRequest changePasswordData, string customerId, string contactId)
        {
            Guid customerGuid;
            Guid contactGuid;

            try
            {
                // Validate the incoming data
                if (changePasswordData == null)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer not found"));
                if (string.IsNullOrEmpty(customerId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer not found"));
                if (!Guid.TryParse(customerId.ToUpper(), out customerGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "The customer not found"));
                if (string.IsNullOrEmpty(contactId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CONTACT_NOT_FOUND));
                if (!Guid.TryParse(contactId.ToUpper(), out contactGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CONTACT_NOT_FOUND));
                if (string.IsNullOrEmpty(changePasswordData.oldpassword))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Old", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (string.IsNullOrEmpty(changePasswordData.newpassword))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("New", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

                //call MSD to change password or return errors
                ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);

                CubResponse<WSPasswordPrevalidateResults> msdResponse = conLogic.ChangePassword(customerGuid, contactGuid, changePasswordData.oldpassword, changePasswordData.newpassword);

                if (!msdResponse.Success)
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
        }

        /// <summary>
        /// Validate Password Token is used in the reset password flow
        /// </summary>
        /// <param name="tokenData"></param>
        public void ValidatePasswordToken(JsonModels.WSVerifyTokenRequest tokenData)
        {
            try
            {
                if (tokenData == null)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("username", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (string.IsNullOrEmpty(tokenData.username))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("username", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (string.IsNullOrEmpty(tokenData.verificationToken))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("verificationToken", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

                //if the username is valid
                //    error.general.value.required (no token passed in)
                //    error.general.code.expired
                //    error.account.is.locked 
                //    but the verificaiton token is not correct the system will increment the number of failed attempts to redeem the verificaiton token
                //    if the number of failed attempts reaches the limit then the account is locked
                //if token validates and no password is present then return response with code 204 (and username?)
                //if token validates and there is a password then we:
                //validate password
                //if password validates then set new password and reset failed attempts to zero, then invalidate (activate? mark as used? delete?) the password token and respond with code 204 (username?)
                //Report Contact Password Change to CustomerServiceAPI

                //call MSD to change password or return errors
                ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);

                CubResponse<WSForgotUsernameResponse> msdResponse = conLogic.ValidateToken(tokenData.username, tokenData.verificationToken, tokenData.newPassword, cub_verificationtokentype.Password);

                if (!msdResponse.Success)
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
        }

        /// <summary>
        /// Validate Mobile Token used for registering a mobile account 
        /// </summary>
        /// <param name="tokenData"></param>
        /// <returns></returns>
        public JsonModels.WSVerifyMobileTokenResponse ValidateMobileToken(JsonModels.WSVerifyMobileTokenRequest tokenData)
        {
            Guid customerGuid;
            Guid contactGuid;

            try
            {
                // Validate the incoming data
                if (tokenData == null)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("customerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND));
                if (string.IsNullOrEmpty(tokenData.customerId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("customerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (!Guid.TryParse(tokenData.customerId.ToUpper(), out customerGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("customerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND));
                if (string.IsNullOrEmpty(tokenData.contactId))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("contactId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (!Guid.TryParse(tokenData.contactId.ToUpper(), out contactGuid))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("contactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CONTACT_NOT_FOUND));
                if (string.IsNullOrEmpty(tokenData.verificationToken))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("verificationToken", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);

                CubResponse<WSMobileToken> msdResponse = conLogic.ValidateMobileToken(customerGuid, contactGuid, tokenData.verificationToken);

                if (!msdResponse.Success || msdResponse.ResponseObject == null)
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }

                JsonModels.WSVerifyMobileTokenResponse response = new JsonModels.WSVerifyMobileTokenResponse();

                response.mobileToken = msdResponse.ResponseObject.mobileToken;

                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
        }

        /// <summary>
        /// Validate Credential Token used for unlock account flow
        /// </summary>
        /// <param name="tokenData"></param>
        public void ValidateCredentialToken(JsonModels.WSVerifyTokenRequest tokenData)
        {
            try
            {
                if (tokenData == null)
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("username", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (string.IsNullOrEmpty(tokenData.username))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("username", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                if (string.IsNullOrEmpty(tokenData.verificationToken))
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("verificationToken", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

                //If the username is correct but the token is invalid, increment the number of attempts to redeem the token
                // errors returned can be tokenerror.general.value.not.found, tokenerror.general.attempts.exceeded, error.general.value.required, error.general.code.expired
                // if successful return code 204
                // second use of this API
                //if the password is filled in then update the password (if valid), unlock the account, reset the number of failed attempts, mark the token as having been used
                //call MSD to change password or return errors
                //send async call to CustomerServiceApi to Report Contact Password Change customer/<customer-id>/contact/<contact-id>/password/reportchange POST

                ContactLogic conLogic = new ContactLogic(ConnectionInfo.CrmService);

                CubResponse<WSForgotUsernameResponse> msdResponse = conLogic.ValidateToken(tokenData.username, tokenData.verificationToken, tokenData.newPassword, cub_verificationtokentype.UnlockAccount);

                if (!msdResponse.Success)
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                // log any exceptions
                CubLogger.Error(e.Message, e);
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("MSD", e.Source, e.Message));
            }
        }

        /// <summary>
        /// Authenticate the caller with username and passord
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool AuthenticateCaller(string caller, string username, string password)
        {
            bool RET_VAL = true;

            // look up the caller in the API authorization table
            if (!string.IsNullOrEmpty(caller) && !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                try
                {
                    using (var osc = new CUBOrganizationServiceContext(ConnectionInfo.CrmService))
                    {
                        var ApiAuthSet =
                        (from aas in osc.cub_ApiAuthorizationSet
                         where aas.cub_Department.Equals(caller) && aas.cub_Username.Equals(username) && aas.cub_Password.Equals(password)
                            && aas.statuscode.Equals(contact_statuscode.Active)
                         select new
                         {
                             ApiAuthId = aas.cub_ApiAuthorizationId
                         })
                         .SingleOrDefault();

                        // if null then no caller registered with those credentials
                        if (ApiAuthSet == null || ApiAuthSet.ApiAuthId == null) RET_VAL = false;
                    }
                }
                catch (FaultException<Model.MSDApi.WSMSDFault> ex)
                {
                    throw ex;
                }
                catch (Exception e)
                {
                    // log any exceptions that occur
                    RET_VAL = false;
                    CubLogger.Error(e.Message, e);
                }
            }
            else
            {
                RET_VAL = false;
            }

            return RET_VAL;
        }

        /// <summary>
        /// Customer search by IDs where a list if customer Ids can be supplied in the request
        /// </summary>
        /// <param name="searchData"></param>
        /// <returns></returns>
        public Model.NIS.WSCustomerSearchResponse CustomerSearchById(JsonModels.WSCustomerSearchByIdRequest searchData)
        {
            if (searchData == null || searchData.customerIds == null)
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Customer Ids", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

            try
            {
                AccountLogic al = new AccountLogic(ConnectionInfo.CrmService);
                Model.MSDApi.CubResponse<Model.NIS.WSCustomerSearchResponse> msdResponse = al.SearchCustomerById(searchData.customerIds, searchData.sortBy, searchData.offset, searchData.limit);

                if (msdResponse.Success)
                {
                    return msdResponse.ResponseObject;
                }
                else
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (Exception e)
            {
                // log any exceptions that occur
                CubLogger.Error(e.Message, e);
                throw (e);
            }
        }

        /// <summary>
        /// Customer search by customer type and/or by the customer's contact's characteristics
        /// </summary>
        /// <returns></returns>
        public Model.NIS.WSCustomerSearchResponse CustomerSearch(string customerType, string firstname, string lastname, string email, string phone, string postalCode, 
            string personalIdentifierType, string personalIdentifier, string username, string customerStatus, string customerId, string businessName, string dob,
            string pin, string sortBy, int offset, int limit)
        {
            if (string.IsNullOrEmpty(customerType))
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Customer Type", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));

            try
            {
                AccountLogic al = new AccountLogic(ConnectionInfo.CrmService);
                Model.MSDApi.CubResponse<Model.NIS.WSCustomerSearchResponse> msdResponse = al.SearchCustomer(customerType, firstname, lastname, email, phone, postalCode, personalIdentifierType, 
                    personalIdentifier, username, customerStatus, customerId, businessName, dob, pin, sortBy, offset, limit);

                if (msdResponse.Success)
                {
                    return msdResponse.ResponseObject;
                }
                else
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(msdResponse.Errors[0]);
                }
            }
            catch (Exception e)
            {
                // log any exceptions that occur
                CubLogger.Error(e.Message, e);
                throw (e);
            }
        }

        /// <summary>
        /// Create case
        /// </summary>
        /// <param name="request">Request</param>
        /// <returns>Response</returns>
        public JsonModels.WSCustomerFeedbackResponse createCase(JsonModels.WSCustomerFeedbackRequest request)
        {
            try
            {
                CaseLogic logic = new CaseLogic(ConnectionInfo.CrmService);
                string caseResponse = 
                    logic.CreateCase(request.feedbackType, request.feedbackMessage, request.customerId, request.contactId, 
                                     request.unregisteredEmail, request.firstName, request.lastName, request?.phone?.type, request?.phone?.number, 
                                     request.referenceType, request.referenceValue, request.channel);
                JsonModels.WSCustomerFeedbackResponse response = JsonConvert.DeserializeObject<JsonModels.WSCustomerFeedbackResponse>(caseResponse);
                return response;
            }
            catch (Exception e)
            {
                CubLogger.Error(e.Message, e);
                throw (e);
            }
        }

        #endregion
    }
}
