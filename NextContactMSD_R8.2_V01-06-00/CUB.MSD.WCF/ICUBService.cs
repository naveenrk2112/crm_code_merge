/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml;
using CUB.MSD.WCF.Library;

namespace CUB.MSD.WCF
{

    // V1
    [ServiceContract]
    public interface IRestService
    {
        // MSD REST API Contracts

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "isAvailable")]
        JsonModels.WSAppInfo IsAvailable();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "msdorginfo")]
        JsonModels.WSMSDOrganizationInfo GetOrganizationInfo();

        /// <summary>
        /// 2.5.1
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/credentials/prevalidate")]
        JsonModels.WSCredentailsPrevalidateResults customerprevalidate(JsonModels.WSCredentials credentials);

        /// <summary>
        /// 2.5.2
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/credentials/authenticate")]
        JsonModels.WSAuthenticateResponse customerauthenticate(JsonModels.WSAuthenticateRequest request);

        /// <summary>
        /// 2.5.3
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "customer/search")]
        Model.NIS.WSCustomerSearchResponse customersearch();

        /// <summary>
        /// 2.5.3 (alternate)
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "customer/search/get")]
        Model.NIS.WSCustomerSearchResponse customersearchget(JsonModels.WSCustomerSearchCriteria searchData);

        /// <summary>
        /// 2.5.4
        /// </summary>
        /// <param name="searchData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "customer/searchbyid")]
        Model.NIS.WSCustomerSearchResponse customersearchbyid(JsonModels.WSCustomerSearchByIdRequest searchData);

        /// <summary>
        /// 2.5.5
        /// </summary>
        /// <param name="customerData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer")]
        JsonModels.WSCreateCustomerResponse customercreate(JsonModels.WSCreateCustomerRequest customerData);

        /// <summary>
        /// 2.5.6
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}")]
        JsonModels.WSCustomer customer(string customerId);

        /// <summary>
        /// 2.5.7
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/contact/{contactId}")]
        JsonModels.WSCustomerContact customercontact(string customerId, string contactId);

        /// <summary>
        /// 2.5.8 customer/<customer-id>/contact POST
        /// </summary>
        /// <param name="contactData">Contact ID</param>
        /// <param name="customerId">Customer ID</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/contact")]
        Model.NIS.WSCustomerContactCreateReponse CustomerContactCreate(Model.MSDApi.WSCustomerContactRequest contactData, string customerId);

        /// <summary>
        /// 2.5.9 PATCH contains just the changes to the object
        /// </summary>
        /// <param name="contactData"></param>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        [OperationContract]
        [WebInvoke(Method = "PATCH", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/contact/{contactId}")]
        void customercontactupdate(Model.MSDApi.WSCustomerContact contactData, string customerId, string contactId);

        /// <summary>
        /// 2.5.10 customer/<customer-id>/contact/<contact-id>/status/<statusaction> PUT
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="status">(Required) The status action of the user to be updated to.
        /// Possible values are:
        ///     - Activate
        ///     - Suspend
        ///     - Terminate
        ///</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/contact/{contactId}/status/{status}")]
        void CustomerContactStatus(string customerId, string contactId, string status);

        /// <summary>
        /// 2.5.11
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/address/{addressId}")]
        Model.NIS.WSAddressExtResponse address(string customerId, string addressId);

        /// <summary>
        /// 2.5.12 Change the full object
        /// </summary>
        /// <param name="addressUpdateData"></param>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/address/{addressId}")]
        void addressupdate(CUB.MSD.Model.NIS.WSUpdateAddress addressUpdateData, string customerId, string addressId);

        /// <summary>
        /// 2.5.13 Remove the full object
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/address/{addressId}")]
        JsonModels.WSAddressDeleteResponse addressdelete(string customerId, string addressId);

        /// <summary>
        /// 2.5.14 POST to create a new object
        /// </summary>
        /// <param name="addressAddData"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/address")]
        Model.MSDApi.WSAddressCreationResponse addressadd(Model.MSDApi.WSAddress addressAddData, string customerId);

        /// <summary>
        /// 2.5.15
        /// </summary>
        /// <param name="changePasswordData"></param>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/contact/{contactId}/password")]
        void passwordchange(JsonModels.WSPasswordRequest changePasswordData, string customerId, string contactId);

        /// <summary>
        /// 2.5.16
        /// </summary>
        /// <param name="usernameData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/securityquestion")]
        JsonModels.WSSecurityQuestionsWithStrings securityquestionsretrieve(JsonModels.WSSecurityQuestionsRetreiveRequest usernameData);

        /// <summary>
        /// 2.5.17
        /// </summary>
        /// <param name="validateSecurityQAData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/password/forgot")]
        JsonModels.WSPasswordForgotResponse securityquestionsvalidate(JsonModels.WSPasswordForgotRequest validateSecurityQAData);

        /// <summary>
        /// 2.5.18
        /// </summary>
        /// <param name="request"></param>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/contact/{contactId}/verificationtoken/generate")]
        JsonModels.WSGenerateTokenResponse generatetoken(JsonModels.WSGenerateVerificationTokenRequest request, string customerId, string contactId);

        /// <summary>
        /// 2.5.19
        /// </summary>
        /// <param name="tokenData"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/password/verifytoken")]
        void passwordtokenvarification(JsonModels.WSVerifyTokenRequest tokenData);

        /// <summary>
        /// 2.5.18
        /// </summary>
        /// <param name="tokenData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/mobile/verifytoken")]
        JsonModels.WSVerifyMobileTokenResponse mobiletokenvarification(JsonModels.WSVerifyMobileTokenRequest tokenData);

        /// <summary>
        /// 2.5.21
        /// </summary>
        /// <param name="tokenData"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/credentials/verifytoken")]
        void credentialtokenvarification(JsonModels.WSVerifyTokenRequest tokenData);

        /// <summary>
        /// 2.5.22
        /// </summary>
        /// <param name="userData"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/username/forgot")]
        void findcontactbydata(JsonModels.WSForgotUsername userData);

        /// <summary>
        /// 2.5.32 customer/<customer-id>/contact/<contact-id>/credentials/validate POST
        /// This API provides ability to validate the pin associated with a contact.
        /// </summary>
        /// <param name="pin">(Required)  A numeric PIN associated with the contact for identification purposes.
        /// Most projects require the PIN to be exactly 4 digits, but the minimum and maximum number of digits may be customized for each project.
        ///</param>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customer/{customerId}/contact/{contactId}/credentials/validate")]
        void credentialsValidate(string customerId, string contactId, JsonModels.WSCredentialsValidate request);

        /// <summary>
        /// 2.6.1
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customerservice/securityquestions")]
        JsonModels.WSSecurityQuestions clientsecurityquestionsretrieve();

        /// <summary>
        /// 2.6.2
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customerservice/namesuffixes")]
        JsonModels.WSNameSuffixTypesResponse clientnamesuffixesretrieve();

        /// <summary>
        /// 2.6.3
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customerservice/phonetypes")]
        JsonModels.WSPhoneTypesResponse clientphonetypesretrieve();

        /// <summary>
        /// 2.6.4
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customerservice/nametitles")]
        JsonModels.WSNameTitleTypesResponse clientnametitlesretrieve();

        /// <summary>
        /// 2.6.5
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customerservice/countries")]
        JsonModels.WSCountryListResponse clientcountriesretrieve();

        /// <summary>
        /// 2.6.7 customerservice/customerstatuses GET
        /// This method returns a list of all customer statuses
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customerservice/customerstatuses")]
        JsonModels.WSCustomerStatuses clientcustomerstatusesretrieve();

        /// <summary>
        /// 2.6.8 customerservice/customersettings
        /// This API returns a list of all customer settings related to username/password validation rules
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customerservice/customersettings")]
        JsonModels.WSClientCustomerSettings clientcustomersettingsretreive();

        /// <summary>
        /// 2.6.11 customerservice/feedback POST
        /// This method is used to record a customerís feedback (such as complaint, compliment etc.) coming from website/mobile into the system.
        /// </summary>
        /// <returns>(Required) The unique identifier to which this feedback is referred against.</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "customerservice/feedback")]
        JsonModels.WSCustomerFeedbackResponse clientFeedback(JsonModels.WSCustomerFeedbackRequest request);

    }
}
