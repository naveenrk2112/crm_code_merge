/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CUB.MSD.WCF
{
    /// <summary>  
    /// A <see cref="WebHttpBehavior"/> that does not attempt to display friendly error messages when an exception occurs  
    /// locating or invoking a service.  
    /// </summary>  
    public class FaultingWebHttpBehavior : WebHttpBehavior
    {
        protected override void AddServerErrorHandlers(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.ChannelDispatcher.ErrorHandlers.Clear();
            endpointDispatcher.ChannelDispatcher.ErrorHandlers.Add(new ErrorHandler());
        }
        public class ErrorHandler : IErrorHandler
        {
            #region IErrorHandler Members
            public bool HandleError(Exception error)
            {
                return true;
            }
            public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
            {
                FaultCode faultCode = FaultCode.CreateSenderFaultCode(error.GetType().Name, "http://tempuri.org/net/exceptions");
                fault = Message.CreateMessage(version, faultCode, error.Message, null);
            }
            #endregion
        }
    }

    /// <summary>
    /// The configuration element for a <see cref="FaultingWebHttpBehavior"/>.
    /// </summary>  
    public class FaultingWebHttpBehaviorElement : BehaviorExtensionElement
    {
        /// <summary>
        /// Gets the type of behavior.  
        /// </summary>  
        /// <value></value>  
        /// <returns>A <see cref="T:System.Type"/>.</returns>  
        public override Type BehaviorType
        {
            get { return typeof(FaultingWebHttpBehavior); }
        }
        /// <summary>  
        /// Creates a behavior extension based on the current configuration settings.  
        /// </summary>  
        /// <returns>The behavior extension.</returns>  
        protected override object CreateBehavior()
        {
            return new FaultingWebHttpBehavior();
        }
    }
}
