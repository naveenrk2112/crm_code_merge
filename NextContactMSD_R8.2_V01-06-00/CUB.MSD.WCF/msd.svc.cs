/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Net;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.ServiceModel.Activation;
using System.Diagnostics;
using CUB.MSD.WCF.Library;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using CUB.MSD.Logic;
using CUB.MSD.Model.NIS;
using System.Text.RegularExpressions;

namespace CUB.MSD.WCF
{

    /// <summary>
    /// x-cub-audit
    /// </summary>
    public class X_Cub_Audit
    {
        /// <summary>
        /// sourceIp
        /// </summary>
        public string sourceIp { get; set; }

        /// <summary>
        /// userId
        /// </summary>
        public string userId { get; set; }

        /// <summary>
        /// channel
        /// </summary>
        public string channel { get; set; }

        /// <summary>
        /// location
        /// </summary>
        public string location { get; set; }
    }

    /// <summary>
    /// Rest Service
    /// </summary>
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RestService : IRestService
    {
        public RestService()
        {
            //System.Web.Http.GlobalConfiguration.Configuration.MessageHandlers.Add(new LogRequestAndResponseHandler());
            //CubLogger.Init()
        }

        /// <summary>
        /// GetRequestInfo
        /// </summary>
        /// <param name="dictForm">dictForm</param>
        /// <param name="requestGuid">requestGuid</param>
        /// <param name="requestDevice">requestDevice</param>
        private void GetRequestInfo(ref Dictionary<string, string> dictForm, ref string requestGuid, ref string requestDevice)
        {
            try
            {
                string[] cubHdr = GetCUBHdrInfo();
                requestGuid = cubHdr[0];
                requestDevice = cubHdr[1];
                dictForm.Add("uid", requestGuid);
                dictForm.Add("device", requestDevice);
            }
            catch (Exception e)
            {
                JsonModels.WSDataValidationResponseHeader rh = new JsonModels.WSDataValidationResponseHeader();
                rh.result = "DataValidationError";
                rh.uid = requestGuid;
                rh.errorMessage = e.Message;
                rh.fieldName = "header";
                rh.errorKey = "errors.invalid.header";
                rh.errorMessage = "header is in invalid format";
                string jsonString = JsonConvert.SerializeObject(rh);

                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                }
                else
                {
                    throw e;
                }
            }
        }

        /// <summary>
        /// Get the x-cub-audit section from the request header
        /// </summary>
        /// <returns></returns>
        public static X_Cub_Audit getXCubAuditFromHeader()
        {
            string header = System.Web.HttpContext.Current.Request.Headers["x-cub-audit"];
            if (header == null)
            {
                return null;
            }
            else
            {
                return JsonConvert.DeserializeObject<X_Cub_Audit>(header);
            }
        }

        /// <summary>
        /// GetCUBHdrInfo
        /// </summary>
        /// <returns>CUBHdrInfo</returns>
        public static string[] GetCUBHdrInfo()
        {
            string cubHdr = System.Web.HttpContext.Current.Request.Headers["x-cub-hdr"];

            return ParseCUBHeader(cubHdr);
        }

        /// <summary>
        /// ParseCUBHeader
        /// </summary>
        /// <param name="cubHdr">cubHdr</param>
        /// <returns>CUBHeader</returns>
        public static string[] ParseCUBHeader(string cubHdr)
        {
            string[] headerInfo = new string[2];

            if (!string.IsNullOrEmpty(cubHdr))
            {
                cubHdr = cubHdr.Trim();
                if ((cubHdr = RemoveFromString(cubHdr, "{", "}")) == null) throw new Exception("Invalid Header");

                string[] hdrItems = cubHdr.Split(new char[] { ',' });
                string hdrGuidMain = hdrItems[0];
                string hdrDeviceMain = hdrItems[1];

                string[] hdrGuid = hdrGuidMain.Split(new char[] { ':' });
                string guid = hdrGuid[0].Trim();
                if (string.IsNullOrEmpty(guid)) throw new Exception("Invalid Header - uid missing");
                if ((guid = RemoveFromString(guid, "\"", "\"")) == null) throw new Exception("Invalid Header - uid missing");
                if (string.Compare(guid, "uid", true) != 0) throw new Exception("Invalid Header - uid missing");
                headerInfo[0] = hdrGuid[1].Trim();
                if (string.IsNullOrEmpty(headerInfo[0])) throw new Exception("Invalid Header - uid missing");
                if ((headerInfo[0] = RemoveFromString(headerInfo[0], "\"", "\"")) == null) throw new Exception("Invalid Header - uid missing");
                if (headerInfo[0].Length > 40) throw new Exception("Invalid Header - uid too long");

                string[] hdrDevice = hdrDeviceMain.Split(new char[] { ':' });
                string device = hdrDevice[0].Trim();
                if (string.IsNullOrEmpty(device)) throw new Exception("Invalid Header - device missing");
                if ((device = RemoveFromString(device, "\"", "\"")) == null) throw new Exception("Invalid Header - device missing");
                if (string.Compare(device, "device", true) != 0) throw new Exception("Invalid Header - device missing");
                headerInfo[1] = hdrDevice[1].Trim();
                if (string.IsNullOrEmpty(headerInfo[1])) throw new Exception("Invalid Header - device missing");
                if ((headerInfo[1] = RemoveFromString(headerInfo[1], "\"", "\"")) == null) throw new Exception("Invalid Header - device missing");
                if (headerInfo[1].Length > 40) throw new Exception("Invalid Header - device too long");
            }

            return headerInfo;
        }

        /// <summary>
        /// RemoveFromString
        /// </summary>
        /// <param name="input">input</param>
        /// <param name="removeFront">removeFront</param>
        /// <param name="removeRear">removeRear</param>
        /// <returns>String removed</returns>
        public static string RemoveFromString(string input, string removeFront, string removeRear)
        {
            string returnString = null;

            if ((string.Compare(input, 0, removeFront, 0, 1) == 0) && (string.Compare(input, input.Length - 1, removeRear, 0, 1) == 0))
            {
                returnString = input.Substring(1, input.Length - 2);
            }

            return returnString;
        }

        /// <summary>
        /// AddSuccessHeader
        /// </summary>
        /// <param name="requestGuid">requestGuid</param>
        public void AddSuccessHeader(string requestGuid)
        {
            if (string.IsNullOrEmpty(requestGuid)) requestGuid = "unknown";
            JsonModels.WSResponseHeader rh = new JsonModels.WSResponseHeader();
            rh.result = "Successful";
            rh.uid = requestGuid;
            string jsonString = JsonConvert.SerializeObject(rh);

            if (System.ServiceModel.Web.WebOperationContext.Current != null)
            {
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
            }
            else
                throw new InvalidProgramException();
        }

        /// <summary>
        /// GetRequestInfo
        /// </summary>
        /// <returns>RequestInfo</returns>
        private static string[] GetRequestInfo()
        {
            List<string> ret_string = new List<string>();

            ret_string.Add("Request Content-Length: " + System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.ContentLength.ToString());
            ret_string.Add("Request Content-Type: " + System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.ContentType);
            ret_string.Add("Request Method Action: " + System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Method);

            return ret_string.ToArray();
        }

        /// <summary>
        /// IsAvailable
        /// </summary>
        /// <returns>Is Available</returns>
        public JsonModels.WSAppInfo IsAvailable()
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSAppInfo Info = null;
            DateTime startTime = DateTime.UtcNow;

            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);

                Dictionary<string, string> dictForm = new Dictionary<string, string>();

                CubLogger.Info("Entering isAvailable",
                               JsonConvert.SerializeObject(GetRequestInfo()) + ", HTTP Headers, " + JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                               startTime);

                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);


                Info = globals.IsAvailable();

                AddSuccessHeader(requestGuid);

                return Info;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));

                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }

                throw e;
            }
            finally
            {
                CubLogger.Info("Exiting isAvailable", 
                               JsonConvert.SerializeObject(GetRequestInfo()) + ", HTTP Headers, " + JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()), 
                               startTime,
                               JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString() + JsonConvert.SerializeObject(Info)), 
                               DateTime.UtcNow);
            }
        }

        /// <summary>
        /// GetOrganizationInfo
        /// </summary>
        /// <returns>Organization Info</returns>
        public JsonModels.WSMSDOrganizationInfo GetOrganizationInfo()
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSMSDOrganizationInfo Info = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                CubLogger.Info("Entering GetOrganizationInfo", 
                               JsonConvert.SerializeObject(GetRequestInfo()) + ", HTTP Headers, " + JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                               startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                Info = globals.GetOrganziationInfo();
                AddSuccessHeader(requestGuid);
                return Info;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
            finally
            {
                CubLogger.Info("Exiting isAvailable",
                               JsonConvert.SerializeObject(GetRequestInfo()) + ", HTTP Headers, " + JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()), 
                               startTime,
                               JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString() + JsonConvert.SerializeObject(Info)), 
                               DateTime.UtcNow);
            }
        }

        /// <summary>
        /// customersearch
        /// </summary>
        /// <returns>Customer search</returns>
        public Model.NIS.WSCustomerSearchResponse customersearch()
        {
            int offset = 0;
            int limit = 10;
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            Model.NIS.WSCustomerSearchResponse response = null;
            Uri passedInUri = HttpContext.Current.Request.Url;
            String customerType = HttpUtility.ParseQueryString(passedInUri.Query).Get("customerType");
            string firstname = HttpUtility.ParseQueryString(passedInUri.Query).Get("firstName");
            string lastname = HttpUtility.ParseQueryString(passedInUri.Query).Get("lastName");
            string email = HttpUtility.ParseQueryString(passedInUri.Query).Get("email");
            string phone = HttpUtility.ParseQueryString(passedInUri.Query).Get("phoneNumber");
            string postalCode = HttpUtility.ParseQueryString(passedInUri.Query).Get("postalCode");
            string personalIdentifierType = HttpUtility.ParseQueryString(passedInUri.Query).Get("personalIdentifierType");
            string personalIdentifier = HttpUtility.ParseQueryString(passedInUri.Query).Get("personalIdentifier");
            string username = string.Empty;
            string customerStatus = string.Empty;
            string customerId = string.Empty;
            string businessName = string.Empty;
            string dob = string.Empty; ;
            string pin = string.Empty;
            string sortBy = HttpUtility.ParseQueryString(passedInUri.Query).Get("sortBy");
            string offsetString = HttpUtility.ParseQueryString(passedInUri.Query).Get("offset");
            string limitString = HttpUtility.ParseQueryString(passedInUri.Query).Get("limit");
            if (!string.IsNullOrEmpty(offsetString)) try { offset = Convert.ToInt32(offsetString); } catch{ offset = 0; }
            if (!string.IsNullOrEmpty(limitString)) try { limit = Convert.ToInt32(limitString); } catch { limit = 10; }
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                response = globals.CustomerSearch(customerType, firstname, lastname, email, phone, postalCode, personalIdentifierType, personalIdentifier,
                    username, customerStatus, customerId, businessName, dob, pin, sortBy, offset, limit);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(response));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// customersearch
        /// </summary>
        /// <returns>Customer search</returns>
        public Model.NIS.WSCustomerSearchResponse customersearchget(JsonModels.WSCustomerSearchCriteria searchData)
        {
            int offset = 0;
            int limit = 10;
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            Model.NIS.WSCustomerSearchResponse response = null;
            if (searchData.offset < 0)  offset = 0; else offset = searchData.offset;
            if (searchData.limit < 0) limit = 10; else limit = searchData.limit;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                response = globals.CustomerSearch(searchData.customerType, searchData.firstName, searchData.lastName, searchData.email, searchData.phoneNumber, searchData.postalCode, 
                    searchData.personalIdentifierType, searchData.personalIdentifier, 
                    searchData.username, searchData.customerStatus, searchData.customerId, searchData.businessName, searchData.dob, searchData.pin, 
                    searchData.sortBy, offset, limit);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(response));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// customersearchbyid
        /// </summary>
        /// <param name="searchData">searchData</param>
        /// <returns>Customer</returns>
        public Model.NIS.WSCustomerSearchResponse customersearchbyid(JsonModels.WSCustomerSearchByIdRequest searchData)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            Model.NIS.WSCustomerSearchResponse response = null;
            Uri passedInUri = HttpContext.Current.Request.Url;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(searchData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                response = globals.CustomerSearchById(searchData);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(response));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// customercreate
        /// </summary>
        /// <param name="customerData">customerData</param>
        /// <returns>Customer Create Response</returns>
        public JsonModels.WSCreateCustomerResponse customercreate(JsonModels.WSCreateCustomerRequest customerData)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSCreateCustomerResponse response = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(customerData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                response = globals.CreateCustomerContact(customerData);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(response));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// customer
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <returns>WSCustomer</returns>
        public JsonModels.WSCustomer customer(string customerId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSCustomer customerInfo = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Uri passedInUri = HttpContext.Current.Request.Url;
                string returnPrimaryContactsOnlyParam = HttpUtility.ParseQueryString(passedInUri.Query).Get("returnPrimaryContactsOnly");
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                bool validParam;
                bool returnPrimaryContactsOnly = Boolean.TryParse(returnPrimaryContactsOnlyParam, out validParam);
                if (validParam)
                {
                    customerInfo = globals.GetCustomer(customerId, returnPrimaryContactsOnly);
                }
                else
                {
                    customerInfo = globals.GetCustomer(customerId);
                }
                AddSuccessHeader(requestGuid);
                CubLogger.Debug("GetCustomer <" + customerId + "> Response " + JsonConvert.SerializeObject(customerInfo));
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(customerInfo));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return customerInfo;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// customercontact
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="contactId">contactId</param>
        /// <returns>WSCustomerContactInfo</returns>
        public JsonModels.WSCustomerContact customercontact(string customerId, string contactId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSCustomerContactInfo customerInfo = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                customerInfo = globals.GetCustomerContact(customerId, contactId);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(customerInfo));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                JsonModels.WSCustomerContact customer = new JsonModels.WSCustomerContact();
                customer.contact = customerInfo;
                return customer;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// Customer Contact Create
        /// </summary>
        /// <param name="contactData">Contact</param>
        /// <param name="customerId">Customer ID</param>
        /// <returns>WSCustomerContactCreateReponse</returns>
        public Model.NIS.WSCustomerContactCreateReponse CustomerContactCreate(Model.MSDApi.WSCustomerContactRequest contactData, string customerId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;
            WSCustomerContactCreateReponse response = new Model.NIS.WSCustomerContactCreateReponse();
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(contactData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                Guid? contactId = globals.CreateCustomerContact(customerId, contactData);
                if (contactId.HasValue)
                {
                    response.contactId = contactId.Value.ToString().ToUpper();
                }
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(response));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                // Return the current contact id
                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";

                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));

                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return response; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return response; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// customercontactupdate
        /// </summary>
        /// <param name="contactData">contactData</param>
        /// <param name="customerId">customerId</param>
        /// <param name="contactId"contactId></param>
        public void customercontactupdate(Model.MSDApi.WSCustomerContact contactData, string customerId, string contactId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(contactData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                globals.UpdateCustomerContact(customerId, contactId, contactData);
                AddSuccessHeader(requestGuid);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                string responseInfo = JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString());
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// address
        /// </summary>
        /// <param name="customerId"customerId></param>
        /// <param name="addressId">addressId</param>
        /// <returns>WSAddress</returns>
        public Model.NIS.WSAddressExtResponse address(string customerId, string addressId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            Model.NIS.WSAddressExt addressInfo = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                addressInfo = globals.GetCustomerAddress(customerId, addressId);
                AddSuccessHeader(requestGuid);
                WSAddressExtResponse addressResponse = new WSAddressExtResponse();
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(addressResponse));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                addressResponse.address = addressInfo;
                return addressResponse;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// addressupdate
        /// </summary>
        /// <param name="addressUpdateData">addressUpdateData</param>
        /// <param name="customerId">customerId</param>
        /// <param name="addressId">addressId</param>
        public void addressupdate(CUB.MSD.Model.NIS.WSUpdateAddress addressUpdateData, string customerId, string addressId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(addressUpdateData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);

                Model.MSDApi.WSAddress address = new Model.MSDApi.WSAddress(addressUpdateData);

                globals.CustomerAddressUpdate(customerId, addressId, address);
                AddSuccessHeader(requestGuid);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                string responseInfo = JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString());
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// addressdelete
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="addressId">addressId</param>
        /// <returns>WSAddressDeleteResponse</returns>
        public JsonModels.WSAddressDeleteResponse addressdelete(string customerId, string addressId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSAddressDeleteResponse addressDeleteResults = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                addressDeleteResults = globals.CustomerAddressDelete(customerId, addressId);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(addressDeleteResults));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return addressDeleteResults;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// addressadd
        /// </summary>
        /// <param name="addressAddData">addressAddData</param>
        /// <param name="customerId">customerId</param>
        /// <returns>WSAddressCreationResponse</returns>
        public Model.MSDApi.WSAddressCreationResponse addressadd(Model.MSDApi.WSAddress addressAddData, string customerId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            Model.MSDApi.WSAddressCreationResponse addressAddResults = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(addressAddData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                addressAddResults = globals.CustomerAddressAdd(customerId, addressAddData);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(addressAddResults));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return addressAddResults;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }


        /// <summary>
        /// customerprevalidate
        /// </summary>
        /// <param name="credentials">credentials</param>
        /// <returns>WSCredentailsPrevalidateResults</returns>
        public JsonModels.WSCredentailsPrevalidateResults customerprevalidate(JsonModels.WSCredentials credentials)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSCredentailsPrevalidateResults CredentialsResults = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(credentials));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                CredentialsResults = globals.CustomerPrevalidate(credentials);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(CredentialsResults));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return CredentialsResults;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// customerauthenticate
        /// </summary>
        /// <param name="authRequest">authRequest</param>
        /// <returns>WSAuthenticateResponse</returns>
        public JsonModels.WSAuthenticateResponse customerauthenticate(JsonModels.WSAuthenticateRequest authRequest)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSAuthenticateResponse AuthenticateResponse = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(authRequest));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                AuthenticateResponse = globals.CustomerAuthenticate(authRequest);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(AuthenticateResponse));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return AuthenticateResponse;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";

                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));

                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// passwordchange
        /// </summary>
        /// <param name="changePasswordData">changePasswordData</param>
        /// <param name="customerId">customerId</param>
        /// <param name="contactId">contactId</param>
        public void passwordchange(JsonModels.WSPasswordRequest changePasswordData, string customerId, string contactId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(changePasswordData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                globals.ContactChangePassword(changePasswordData, customerId, contactId);
                AddSuccessHeader(requestGuid);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                string responseInfo = JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString());
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// generatetoken
        /// </summary>
        /// <param name="tokenData">tokenData</param>
        /// <param name="customerId">customerId</param>
        /// <param name="contactId">contactId</param>
        /// <returns>WSGenerateTokenResponse</returns>
        public JsonModels.WSGenerateTokenResponse generatetoken(JsonModels.WSGenerateVerificationTokenRequest tokenData, string customerId, string contactId)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSGenerateTokenResponse response = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(tokenData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                response = globals.GenerateToken(tokenData.verificationToken, customerId, contactId);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(response));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// passwordtokenvarification
        /// </summary>
        /// <param name="tokenData">tokenData</param>
        public void passwordtokenvarification(JsonModels.WSVerifyTokenRequest tokenData)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(tokenData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                globals.ValidatePasswordToken(tokenData);
                AddSuccessHeader(requestGuid);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                string responseInfo = JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString());
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// mobiletokenvarification
        /// </summary>
        /// <param name="tokenData">tokenData</param>
        /// <returns>WSVerifyMobileTokenResponse</returns>
        public JsonModels.WSVerifyMobileTokenResponse mobiletokenvarification(JsonModels.WSVerifyMobileTokenRequest tokenData)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSVerifyMobileTokenResponse response = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(tokenData));
                CubLogger.Info("Request", requestInfo, startTime);
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                response = globals.ValidateMobileToken(tokenData);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(response));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// credentialtokenvarification
        /// </summary>
        /// <param name="tokenData">tokenData</param>
        public void credentialtokenvarification(JsonModels.WSVerifyTokenRequest tokenData)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(tokenData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                globals.ValidateCredentialToken(tokenData);
                AddSuccessHeader(requestGuid);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                string responseInfo = JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString());
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow); return;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// securityquestionsvalidate
        /// </summary>
        /// <param name="validateSecurityQAData">validateSecurityQAData</param>
        /// <returns>WSPasswordForgotResponse</returns>
        public JsonModels.WSPasswordForgotResponse securityquestionsvalidate(JsonModels.WSPasswordForgotRequest validateSecurityQAData)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSPasswordForgotResponse response = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(validateSecurityQAData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                response = globals.validateSecurityQAs(validateSecurityQAData);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(response));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// securityquestionsretrieve
        /// </summary>
        /// <param name="usernameData">usernameData</param>
        /// <returns>WSSecurityQuestions</returns>
        public JsonModels.WSSecurityQuestionsWithStrings securityquestionsretrieve(JsonModels.WSSecurityQuestionsRetreiveRequest usernameData)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSSecurityQuestionsWithStrings response = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(usernameData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                response = globals.LookupSecurityQAs(usernameData);
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(response));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return response;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// findcontactbydata
        /// </summary>
        /// <param name="userData">userData</param>
        public void findcontactbydata(JsonModels.WSForgotUsername userData)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(userData));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                globals.FindContactByData(userData);
                AddSuccessHeader(requestGuid);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                string responseInfo = JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString());
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));

                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// 2.5.32 customer/<customer-id>/contact/<contact-id>/credentials/validate POST
        /// This API provides ability to validate the pin associated with a contact.
        /// </summary>
        /// <param name="pin">(Required) A numeric PIN associated with the contact for identification purposes.
        /// Most projects require the PIN to be exactly 4 digits, but the minimum and maximum number of digits may be customized for each project.
        ///</param>
        public void credentialsValidate(string customerId, string contactId, JsonModels.WSCredentialsValidate request)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(request));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                globals.CredentialsValidate(customerId, contactId, request.pin);
                AddSuccessHeader(requestGuid);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                string responseInfo = JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString());
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// 2.5.32 customer/feedback POST
        /// This method is used to record a customerís feedback (such as complaint, compliment etc.) coming from website/mobile into the system.
        /// </summary>
        /// <returns>(Required) The unique identifier to which this feedback is referred against.</returns>
        public JsonModels.WSCustomerFeedbackResponse clientFeedback(JsonModels.WSCustomerFeedbackRequest request)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;
            JsonModels.WSCustomerFeedbackResponse response = new JsonModels.WSCustomerFeedbackResponse();
            try
            {
                response = clientFeedbackValidateFields(request);
                if (response != null)
                {
                    return response;
                }
                X_Cub_Audit xCubAudit = getXCubAuditFromHeader();
                if (xCubAudit != null)
                {
                    request.channel = xCubAudit.channel;
                }
                else
                {
                    throw new Exception("x-cub-audit section not found on request header.");
                }
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}, Body = {2}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()),
                                                   JsonConvert.SerializeObject(request));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                response = globals.createCase(request);
                string responseInfo = JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString());
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
            }
            catch (Exception e)
            {
                if (e.Message.Equals("Subject not found"))
                {
                    JsonModels.WSDataValidationResponseHeader rh = new JsonModels.WSDataValidationResponseHeader();
                    rh.result = "DataValidationError";
                    rh.errorMessage = "The feedback type is invalid.";
                    rh.fieldName = "feedbackType";
                    rh.errorKey = "errors.general.value.invalid";
                    string jsonString = JsonConvert.SerializeObject(rh);
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                }
                else
                {
                    string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                    if (System.ServiceModel.Web.WebOperationContext.Current != null)
                    {
                        System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                        return null; // todo error handling
                    }
                    throw e;
                }
            }
            return response;
        }

        /// <summary>
        /// clientFeedbackValidateFields
        /// </summary>
        /// <param name="request">Request</param>
        /// <returns>Response</returns>
        private JsonModels.WSCustomerFeedbackResponse clientFeedbackValidateFields(JsonModels.WSCustomerFeedbackRequest request)
        {
            JsonModels.WSCustomerFeedbackResponse response = new JsonModels.WSCustomerFeedbackResponse();
            /*
             * Check feedbackType required
             */
            if (string.IsNullOrWhiteSpace(request.feedbackType))
            {
                JsonModels.WSDataValidationResponseHeader rh = new JsonModels.WSDataValidationResponseHeader();
                rh.result = "DataValidationError";
                rh.errorMessage = "Field is blank.";
                rh.fieldName = "feedbackType";
                rh.errorKey = "errors.general.value.required";
                string jsonString = JsonConvert.SerializeObject(rh);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                return response;
            }
            /*
             * Check feedbackMessage length
             */
            if (request.feedbackMessage.Length > 2000)
            {
                JsonModels.WSDataValidationResponseHeader rh = new JsonModels.WSDataValidationResponseHeader();
                rh.result = "DataValidationError";
                rh.errorMessage = "Maximum length exceeded.";
                rh.fieldName = "feedbackMessage";
                rh.errorKey = "errors.general.value.toolong";
                string jsonString = JsonConvert.SerializeObject(rh);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                return response;
            }
            return null;
        }

        /// <summary>
        /// clientsecurityquestionsretrieve
        /// </summary>
        /// <returns>WSSecurityQuestions</returns>
        public JsonModels.WSSecurityQuestions clientsecurityquestionsretrieve()
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            JsonModels.WSSecurityQuestions questionList = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                questionList = globals.GetClientSecurityQuestions();
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(questionList));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return questionList;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// clientnamesuffixesretrieve
        /// </summary>
        /// <returns>WSNameSuffixTypesResponse</returns>
        public JsonModels.WSNameSuffixTypesResponse clientnamesuffixesretrieve()
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            CUB.MSD.WCF.Library.JsonModels.WSNameSuffixTypesResponse nameSuffixesList = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                nameSuffixesList = globals.GetClientSuffixes();
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(nameSuffixesList));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return nameSuffixesList;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// clientphonetypesretrieve
        /// </summary>
        /// <returns>WSPhoneTypesResponse</returns>
        public JsonModels.WSPhoneTypesResponse clientphonetypesretrieve()
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            CUB.MSD.WCF.Library.JsonModels.WSPhoneTypesResponse phoneTypeList = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                phoneTypeList = globals.GetClientPhoneTypes();
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(phoneTypeList));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return phoneTypeList;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// clientnametitlesretrieve
        /// </summary>
        /// <returns>WSNameTitleTypesResponse</returns>
        public JsonModels.WSNameTitleTypesResponse clientnametitlesretrieve()
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            CUB.MSD.WCF.Library.JsonModels.WSNameTitleTypesResponse nameTitleList = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                nameTitleList = globals.GetClientNameTitles();
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(nameTitleList));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return nameTitleList;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// clientcountriesretrieve
        /// </summary>
        /// <returns>WSCountryListResponse</returns>
        public JsonModels.WSCountryListResponse clientcountriesretrieve()
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            CUB.MSD.WCF.Library.JsonModels.WSCountryListResponse countriesList = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                countriesList = globals.GetClientCountries();
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(countriesList));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return countriesList;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        public JsonModels.WSCustomerStatuses clientcustomerstatusesretrieve()
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            CUB.MSD.WCF.Library.JsonModels.WSCustomerStatuses customerstatusesList = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                customerstatusesList = globals.GetClientCustomerStatuses();
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(customerstatusesList));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return customerstatusesList;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        public JsonModels.WSClientCustomerSettings clientcustomersettingsretreive()
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            CUB.MSD.WCF.Library.JsonModels.WSClientCustomerSettings customersettingsList = null;
            DateTime startTime = DateTime.UtcNow;
            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                customersettingsList = globals.GetClientCustomerSettings();
                AddSuccessHeader(requestGuid);
                string responseInfo = string.Concat(JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString()),
                                                    JsonConvert.SerializeObject(customersettingsList));
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
                return customersettingsList;
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";
                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));
                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                    return null; // todo error handling
                }
                throw e;
            }
        }

        /// <summary>
        /// DataValidationError
        /// </summary>
        /// <param name="guid">guid</param>
        /// <param name="fieldname">fieldname</param>
        /// <param name="key">key</param>
        /// <param name="message">message</param>
        /// <returns>WSDataValidationResponseHeader</returns>
        private JsonModels.WSDataValidationResponseHeader DataValidationError(string guid, string fieldname, string key, string message)
        {
            JsonModels.WSDataValidationResponseHeader rh = new JsonModels.WSDataValidationResponseHeader();
            rh.result = "DataValidationError";
            rh.uid = guid;
            rh.fieldName = fieldname;
            rh.errorKey = key;
            rh.errorMessage = message;
            return rh;
        }

        /// <summary>
        /// UnexpectedSystemError
        /// </summary>
        /// <param name="guid">guid</param>
        /// <param name="message">message</param>
        /// <returns>WSErrorResponseHeader</returns>
        private JsonModels.WSErrorResponseHeader UnexpectedSystemError(string guid, Exception e)
        {
            CubLogger.Error(guid, e, e.Message);
            string msg = null;

            if (e is WebException)
            {
                WebException ex = e as WebException;
                var response = ex.Response as System.Net.HttpWebResponse;
                if (response != null && response.StatusCode == HttpStatusCode.NotFound)
                {
                    msg = "Web service endpoint not found";
                }
            }

            if (string.IsNullOrEmpty(msg))
            {
                msg = e.Message;
                Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                msg = rgx.Replace(msg, "");
            }

            JsonModels.WSErrorResponseHeader rh = new JsonModels.WSErrorResponseHeader();
            rh.result = RestConstants.UNEXPECTED_SYSTEM_ERROR;
            rh.uid = guid;
            rh.errorMessage = msg;
            return rh;
        }

        /// <summary>
        /// Customer Contact Status
        /// </summary>
        /// <param name="customerId">Contact ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="status">Status</param>
        /// <returns></returns>
        public void CustomerContactStatus(string customerId, string contactId, string status)
        {
            string requestGuid = string.Empty;
            string requestDevice = string.Empty;
            DateTime startTime = DateTime.UtcNow;

            try
            {
                Globals globals = new Globals();
                CubLogger.Init(globals.ConnectionInfo.CrmService);
                string requestInfo = string.Format("{0},  HTTP Headers = {1}",
                                                   JsonConvert.SerializeObject(GetRequestInfo()),
                                                   JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Headers.ToString()));
                CubLogger.Info("Request", requestInfo, startTime);
                Dictionary<string, string> dictForm = new Dictionary<string, string>();
                GetRequestInfo(ref dictForm, ref requestGuid, ref requestDevice);
                globals.CustomerContactStatus(customerId, contactId, status);
                AddSuccessHeader(requestGuid);
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                string responseInfo = JsonConvert.SerializeObject(System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.ToString());
                CubLogger.Info("Response", requestInfo, startTime, responseInfo, DateTime.UtcNow);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exWSMSDFault)
            {
                if (string.IsNullOrEmpty(exWSMSDFault.Detail.fieldName)) exWSMSDFault.Detail.fieldName = "MSD";

                string jsonString = JsonConvert.SerializeObject(DataValidationError(requestGuid, exWSMSDFault.Detail.fieldName, exWSMSDFault.Detail.errorKey, exWSMSDFault.Detail.errorMessage));

                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                }

                throw exWSMSDFault;
            }
            catch (Exception e)
            {
                string jsonString = JsonConvert.SerializeObject(UnexpectedSystemError(requestGuid, e));

                if (System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("x-cub-hdr", jsonString);
                }

                throw e;
            }
        }

        //private NameValueCollection StandardizeParams(NameValueCollection nvc)
        //{
        //    try
        //    {
        //        NameValueCollection nvcNew = new NameValueCollection();
        //        //LogEvent(null, "standardizeparms keys.count=" + nvc.Keys.Count.ToString(), EventLogEntryType.Information, 15001);
        //        foreach (string key in nvc.AllKeys)
        //        {
        //            if (key != null)
        //            {
        //              nvcNew.Add(key.ToLower(), nvc[key]);
        //            }
        //        }
        //        return nvcNew;

        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}

        //public List<JsonModels.Customer> GETCustomerSearch()
        //{
        //  //  List<JsonModels.GETCustomerSearch> results = new List<JsonModels.GETCustomerSearch>();

        //    try
        //    {
        //        NameValueCollection nvc = StandardizeParams(SSW.WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters);

        //        Dictionary<string, string> dictForm = new Dictionary<string, string>();

        //        foreach (string s in nvc.Keys)
        //        {
        //            dictForm.Add(s, nvc[s]);
        //        }

        //        string token = "";
        //        if (dictForm.ContainsKey("token"))
        //            token = dictForm["token"];
        //        else
        //        {
        //          // error here
        //        }

        //        //if (complexName == null)
        //        //{
        //        //  // error here
        //        //}

        //       // if (complexName.ToLower() == "getcustomerinfo")
        //       // {
        //            return Globals.GetCustomerSearch(dictForm);
        //       // }

        //    }
        //    catch (Exception e)
        //    {
        //        return null; // todo error handling
        //    }
        //}

        //        public List<JsonModels.GetCustomer> GetComplexEntity(string complexName)
        //        {
        //            List<JsonModels.GetCustomer> results = new List<JsonModels.GetCustomer>();

        //            try
        //            {
        //                //Stream rStream = null;
        //                //InternalResponse ir = new InternalResponse(0, "");

        //                NameValueCollection nvc = StandardizeParams(SSW.WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters);

        //                Dictionary<string, string> dictForm = new Dictionary<string, string>();

        //                foreach (string s in nvc.Keys)
        //                {
        //                    dictForm.Add(s, nvc[s]);
        //                }

        //                string token = "";
        //                if (dictForm.ContainsKey("token"))
        //                    token = dictForm["token"];
        //                else
        //                {
        ////                    doc.InnerXml = "<?xml version='1.0' ?><error>" + "A token is required" + "</error>";
        ////                    return doc;
        //                }

        //                if (complexName == null)
        //                {
        // //                   doc.InnerXml = "<?xml version='1.0' ?><error>" + "Multiple name cannot be empty" + "</error>";
        // //                   return doc;
        //                }

        //                if (complexName.ToLower() == "getcustomerinfo")
        //                {
        //                   return Globals.GetCustomerInfo(dictForm);
        //                }

        //            }
        //            catch (Exception e)
        //            {
        //                return results; // todo error handling
        //            }

        //            return results;
        //        }
    }

    /// <summary>
    /// BasicAuthHttpModule
    /// </summary>
    public class BasicAuthHttpModule : IHttpModule
    {
        /// <summary>
        /// Init
        /// </summary>
        /// <param name="context">context</param>
        public void Init(HttpApplication context)
        {
            // Register event handlers
            context.AuthenticateRequest += AuthenticateUser;
        }

        // TODO: Here is where you would validate the username and password.
        /// <summary>
        /// context
        /// </summary>
        /// <param name="caller">caller</param>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns></returns>
        private static bool CheckHeaderAuthentication(string caller, string username, string password)
        {
            Globals globals = new Globals();

            return globals.AuthenticateCaller(caller, username, password);
        }

        /// <summary>
        /// AuthenticateUser
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="eventArgs">eventArgs</param>
        public void AuthenticateUser(object source, EventArgs eventArgs)
        {
            if (source == null) return;

            HttpApplication app = (HttpApplication)source;

            try
            {
                string authHeader = app.Request.Headers["Authorization"];

                HandleAuthHeader(authHeader, app);
            }
            catch (Exception e)
            {
                DenyAccess(app);
            }
        }

        /// <summary>
        /// HandleAuthHeader
        /// </summary>
        /// <param name="authHeader">authHeader</param>
        /// <param name="app">app</param>
        private void HandleAuthHeader(string authHeader, HttpApplication app)
        {
            if (!string.IsNullOrEmpty(authHeader))
            {
                authHeader = authHeader.Trim();
                if (authHeader.IndexOf("Basic", 0) != 0)
                {
                    return;
                }

                authHeader = authHeader.Trim();

                string encodedCredentials = authHeader.Substring(6);

                byte[] decodedBytes =
                Convert.FromBase64String(encodedCredentials);
                string s = new ASCIIEncoding().GetString(decodedBytes);

                string[] userPass = s.Split(new char[] { ':' });
                string username = userPass[0];
                string password = userPass[1];

                string[] cubHdr = RestService.GetCUBHdrInfo();

                if (cubHdr == null || cubHdr.Length < 2)
                {
                    DenyAccess(app);
                    return;
                }

                string caller = cubHdr[1];

                if (!CheckHeaderAuthentication(caller, username, password))
                {
                    DenyAccess(app);
                    return;
                }
            }
            else
            {
                DenyAccess(app);
                return;
            }
        }

        // If the request was unauthorized, add the WWW-Authenticate header 
        // to the response.
        /// <summary>
        /// OnApplicationEndRequest
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private static void OnApplicationEndRequest(object sender, EventArgs e)
        {
            var response = HttpContext.Current.Response;
            if (response.StatusCode == 401)
            {
                response.Headers.Add("WWW-Authenticate",
                    string.Format("Basic realm=\"NextContact\\msd\\\""));
            }
        }

        /// <summary>
        /// DenyAccess
        /// </summary>
        /// <param name="app">app</param>
        private void DenyAccess(HttpApplication app)
        {
            try
            {
                if (app == null || app.Response == null) return;

                app.Response.StatusCode = 401;
                app.Response.StatusDescription = "Access Denied";
                app.Response.Write("401 Access Denied oh yeah, not gettin' in");
                app.CompleteRequest();
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        /// <summary>
        /// RejectAccess
        /// </summary>
        /// <param name="app">app</param>
        private void RejectAccess(HttpApplication app)
        {
            if (app == null) return;

            try
            {
                app.Response.StatusCode = 401;
                app.Response.StatusDescription = "Access Denied";
                app.Response.Write("401 Incomplete Request");
                app.CompleteRequest();
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
        }
    }

    /// <summary>
    /// LogRequestAndResponseHandler
    /// </summary>
    public class LogRequestAndResponseHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(
               HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // log request body
            string requestBody = await request.Content.ReadAsStringAsync();
            Trace.WriteLine(requestBody);

            // let other handlers process the request
            var result = await base.SendAsync(request, cancellationToken);

            if (result.Content != null)
            {
                // once response body is ready, log it
                var responseBody = await result.Content.ReadAsStringAsync();
                Trace.WriteLine(responseBody);
            }

            return result;
        }
    }
}
