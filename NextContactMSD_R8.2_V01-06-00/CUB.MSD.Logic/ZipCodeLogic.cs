/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using System.Runtime.Serialization;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// Zip Code Logic
    /// </summary>
    public class ZipCodeLogic : LogicBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public ZipCodeLogic(IOrganizationService service) : base(service)
        {
        }

        /// <summary>
        /// Get by ID
        /// </summary>
        /// <param name="zipcodeId">Zipcode</param>
        /// <returns>Zipcode record</returns>
        public cub_ZipOrPostalCode GetById(Guid zipcodeId)
        {
            return
                (from zip in CubOrgSvc.cub_ZipOrPostalCodeSet
                 where zip.cub_ZipOrPostalCodeId.Value == zipcodeId
                 select zip).FirstOrDefault();
        }

        /// <summary>
        /// Get By Zip Code
        /// </summary>
        /// <param name="zipcode">zipcode</param>
        /// <returns>Zipcode list</returns>
        public IEnumerable<cub_ZipOrPostalCode> GetByZipCode(string zipcode)
        {
            IEnumerable<cub_ZipOrPostalCode> zipcodes = null;
            zipcodes = (from zips in CubOrgSvc.cub_ZipOrPostalCodeSet
                        where zips.cub_Code == zipcode
                        select
                            new cub_ZipOrPostalCode()
                            {
                                cub_City = zips.cub_City,
                                cub_Country = zips.cub_Country,
                                cub_State = zips.cub_State
                            }).ToList();
            return zipcodes;
        }
    }
}