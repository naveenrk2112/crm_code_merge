﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// Account Logic
    /// </summary>
    public class AccountLogic : LogicBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public AccountLogic(IOrganizationService service) : base(service)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <param name="executionContext">MSD plugin execution contect</param>
        public AccountLogic(IOrganizationService service, IPluginExecutionContext executionContext) : base(service, executionContext)
        {
        }

        /// <summary>
        /// Get Account by ID
        /// </summary>
        /// <param name="accountId"Account ID></param>
        /// <returns>Account</returns>
        public Account GetById(Guid accountId)
        {
            return
                (from acc in CubOrgSvc.AccountSet
                 where acc.AccountId.Value == accountId
                 select acc).FirstOrDefault();
        }

        /// <summary>
        /// Get Contacts by account ID
        /// </summary>
        /// <param name="accountId">Account ID</param>
        /// <returns>WS Contact List</returns>
        public IEnumerable<WSContact> GetByAccountId(Guid accountId)
        {
            IEnumerable<WSContact> contacts = null;
            contacts = (from con in CubOrgSvc.ContactSet
                        where con.AccountId.Id == accountId
                        select
                            new WSContact()
                            {
                                contactId = con.ContactId.Value,
                                firstName = con.FirstName,
                                lastName = con.LastName
                            }).ToList();
            return contacts;
        }

        /// <summary>
        /// Get Primary Contacy
        /// </summary>
        /// <param name="accountId">Account ID</param>
        /// <returns>Primary Contact</returns>
        public object GetPrimaryContact(Guid accountId)
        {
            var contact = (from c in CubOrgSvc.ContactSet
                           join type in CubOrgSvc.cub_ContactTypeSet
                               on c.cub_ContactTypeId.Id equals type.cub_ContactTypeId
                           where c.AccountId.Id == accountId
                               && type.cub_Name == "Primary"
                           select new Contact
                           {
                               FirstName = c.FirstName,
                               LastName = c.LastName,
                               ContactId = c.ContactId,
                               BirthDate = c.BirthDate,
                               cub_Phone1Type = c.cub_Phone1Type,
                               cub_Phone2Type = c.cub_Phone2Type,
                               cub_Phone3Type = c.cub_Phone3Type,
                               Telephone1 = c.Telephone1,
                               Telephone2 = c.Telephone2,
                               Telephone3 = c.Telephone3,
                               EMailAddress1 = c.EMailAddress1
                           }).FirstOrDefault();
            return new
            {
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                ContactId = contact.ContactId,
                BirthDate = contact.BirthDate?.ToShortDateString(),
                Phone1Type = contact.cub_Phone1Type != null
                    ? GetAttributeOptionSetText(contact, Contact.cub_phone1typeAttribute, typeof(cub_phonetype))
                    : null,
                Phone2Type = contact.cub_Phone2Type != null
                    ? GetAttributeOptionSetText(contact, Contact.cub_phone2typeAttribute, typeof(cub_phonetype))
                    : null,
                Phone3Type = contact.cub_Phone3Type != null
                    ? GetAttributeOptionSetText(contact, Contact.cub_phone3typeAttribute, typeof(cub_phonetype))
                    : null,
                Phone1 = contact.Telephone1,
                Phone2 = contact.Telephone2,
                Phone3 = contact.Telephone3,
                Email = contact.EMailAddress1
            };
        }

        /// <summary>
        /// Get primary contact EntityReference
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns>EntityReference to primary contact</returns>
        public EntityReference GetPrimaryContactReference(Guid accountId)
        {
            return (from contact in CubOrgSvc.ContactSet
                    join contactType in CubOrgSvc.cub_ContactTypeSet
                       on contact.cub_ContactTypeId.Id equals contactType.Id
                    where contact.AccountId.Id == accountId
                    where contactType.cub_Name == "Primary"
                    select new EntityReference(Contact.EntityLogicalName, contact.Id)).FirstOrDefault();
        }

        /// <summary>
        /// Complete Customer Registration
        /// </summary>
        /// <param name="accountId">Account ID</param>
        public string CompleteCustomerRegistration(Guid accountId)
        {


            try
            {
                CubLogger.Debug("Entered method");
                CubLogger.DebugFormat("Account Id = {0}", accountId);
                CubLogger.Debug("Creating nisApi object");
                //SS - NXTC-768 - NIS Complete Registration
                NISApiLogic nisApi = new NISApiLogic(_organizationService, _executionContext);
                CubLogger.Debug("Before calling nisApi.CompleteCustomerRegistration");
          
                long oneAccountId = nisApi.CompleteCustomerRegistration(accountId);
                CubLogger.Debug("After calling nisApi.CompleteCustomerRegistration");
                nisApi = null;

                CubLogger.Debug(string.Format("oneAccountId = {0}", oneAccountId.ToString()));

                if (oneAccountId != 0)
                {
                    //Update Account
                    Account acc = new Account();
                    acc.AccountId = accountId;
                    acc.Attributes.Add(Account.cub_oneaccountidAttribute, oneAccountId.ToString());
                    _organizationService.Update(acc);
                    return oneAccountId.ToString();
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error("", ex);
                throw;
            }

            return "";
        }

        /// <summary>
        /// Pugin call to save the Customer Registratiuon data
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <returns>Customer ID (MSD Account ID)</returns>
        private void RegisterAccountPluginCall(WSCustomer customer, out Guid customerId, out Guid contactID)
        {
            OrganizationRequest request = new OrganizationRequest("cub_GlobalCustomerRegistration");
            request["Input"] = JsonConvert.SerializeObject(customer);
            OrganizationResponse response = _organizationService.Execute(request);
            customerId = new Guid(response.Results[CUB.MSD.Model.CUBConstants.GlobalCustomerRegistrationConstants.OUTPUT_PARAMETER_CUSTOMER_ID_NAME].ToString());
            contactID = new Guid(response.Results[CUB.MSD.Model.CUBConstants.GlobalCustomerRegistrationConstants.OUTPUT_PARAMETER_CONTACT_ID_NAME].ToString());
        }

        /// <summary>
        /// Save all customer registration data into MSD.
        /// As this method is invoked from the GlobalCustomerRegistration Plugin it act as a transaction
        /// So if any MSD creation operation fail all operation are rolled-back.
        /// WARNING: This was the only way we found to execute MSD operations as an ACID transaction
        /// </summary>
        /// <param name="customer">Customer</param>
        public void RegisterCusomterSaveData(WSCustomer customer)
        {
            CreateAccount(customer);
            if (customer.contact != null && customer.contact.address != null)
            {
                AddressLogic addressLogic = new AddressLogic(_organizationService);
                addressLogic.CreateAddress(customer.contact.address, customer.customerId.Value);
            }
            ContactLogic contactLogic = new ContactLogic(_organizationService);
            contactLogic.CreateContact(customer.contact, "CreateAccount");
        }


        /// <summary>
        /// method will first validate input paramaters then will create customer and contact record in the system
        /// </summary>
        /// <param name="customer">Customer Data</param>
        /// <returns>WSMSDResponse</returns>
        /// <exception cref="System.Exception">General exceptions</exception>
        public CubResponse<WSCustomer> RegisterCustomer(WSCustomer customer)
        {
            CubResponse<WSCustomer> response = new CubResponse<WSCustomer>();
            const string methodName = "CreateAccount";
            try
            {
                CubLogger.Info("In RegisterCustomer");
                // If there are errors validating customer data, exit the function and send errors back to caller
                response = ValidateCustomer(customer);
                if (!response.Success)
                    return response;

                if (GlobalsCache.CustomerRegistrationIsAddressRequired(_organizationService) || customer.contact.address != null)
                {
                    AddressLogic addressLogic = new AddressLogic(_organizationService);
                    CubResponse<WSAddress> addressResponse = addressLogic.IsAddressValid(customer.contact.address);
                    if (!addressResponse.Success)
                    {
                        response.Errors.AddRange(addressResponse.Errors);
                        return response;
                    }
                }

                try
                {
                    Guid customerID;
                    Guid contactID;
                    RegisterAccountPluginCall(customer, out customerID, out contactID);
                    customer.customerId = customerID;
                    customer.contact.contactId = contactID;
                }
                catch (Exception ex)
                {
                    response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                    return response;
                }

                //call nis 
                if (response.Success)
                    CompleteCustomerRegistration(customer.customerId.Value);
                response.ResponseObject = customer;
            }
            catch (Exception ex) {
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                CubLogger.Error(methodName, ex);
            }
            return response;
        }


        /// <summary>
        /// method will first validate input paramaters then will update customer and contact record in the system
        /// </summary>
        /// <param name="customer">Customer Data</param>
        /// <returns>WSMSDResponse</returns>
        /// <exception cref="System.Exception">General exceptions</exception>
        public CubResponse<WSCustomer> UpdateAccount(WSCustomer customer)
        {
            CubResponse<WSCustomer> response = new CubResponse<WSCustomer>();

            const string methodName = "UpdateAccount";
            try
            {
                // If there are errors validating customer data, exit the function and send errors back to caller
                response = ValidateCustomer(customer);
                if (!response.Success)
                    return response;


                response = UpdateCustomer(customer);

                //if there are errors adding customer record to MSD, exit the function and return the error
                if (!response.Success)
                    return response;

                if (customer != null && customer.contact != null && customer.contact.address != null)
                {
                    AddressLogic addressLogic = new AddressLogic(_organizationService);
                    CubResponse<WSAddress> addressResponse = addressLogic.ValidateAndCreateAddress(customer.contact.address, customer.customerId.Value);


                    if (!addressResponse.Success)
                    {
                        response.Errors.AddRange(addressResponse.Errors);
                        return response;
                    }

                    //assign address to contact object so later we can link the address to contact
                    customer.contact.address.addressId = addressResponse.ResponseObject.addressId;
                }

                ContactLogic conLogic = new ContactLogic(_organizationService);
                CubResponse<WSCustomerContact> custContactResponse = conLogic.UpdateContact(customer.contact);

                //If there are errors adding contact, add errors to response and exit the function
                if (!custContactResponse.Success)
                {
                    response.Errors.AddRange(custContactResponse.Errors);
                    return response;
                }

                response.ResponseObject = customer;
            }

            catch (Exception ex)
            {
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                CubLogger.Error(methodName, ex);
            }

            return response;
        }

        /// <summary>
        /// Get a random Account Number
        /// </summary>
        /// <returns></returns>
        public string GetAccountNumber()
        {
            Random rndgen = new Random();
            return rndgen.ToString();
        }

        /// <summary>
        /// Add customer record to database
        /// </summary>
        /// <param name="customer">WSCustomer</param>
        /// <returns>WSMSDResponse</returns>
        private void CreateAccount(WSCustomer customer)
        {
            CubLogger.Debug("Method Entered");
            CubResponse<WSCustomer> response = new CubResponse<WSCustomer>();
            Account newAccount = new Account();
            newAccount.Name = $"{customer.contact.name.firstName} { customer.contact.name.lastName}";
            //if customer type exists in passed WSCustomer class, add Entity reference using customer type Id (better performance since adding using primary key) otherwise use customer type to query entity and add as reference
            newAccount.cub_AccountTypeId = (customer.customerTypeId.HasValue ? new EntityReference(cub_AccountType.EntityLogicalName, customer.customerTypeId.Value) : new EntityReference(cub_AccountType.EntityLogicalName, cub_AccountType.cub_accountnameAttribute, customer.customerType));
            CubLogger.Debug($"Adding new Account to the system: Name {newAccount.Name}");
            Guid? newAccountId = _organizationService.Create(newAccount);
            customer.customerId = newAccountId.Value;
            customer.contact.customerId = newAccountId.Value;
            CubLogger.Debug($"Account successfully created Id: { newAccountId.Value}");
            CubLogger.Debug("Method Exit");
        }

        /// <summary>
        /// Update Customer
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <returns>Update customer response</returns>
        private CubResponse<WSCustomer> UpdateCustomer(WSCustomer customer)
        {
            string methodName = "UpdateCustomer";
            CubLogger.Debug("Method Entered");
            CubResponse<WSCustomer> response = new CubResponse<WSCustomer>();

            Entity accountEntity = _organizationService.Retrieve(Account.EntityLogicalName, customer.customerId.Value, new Microsoft.Xrm.Sdk.Query.ColumnSet(new string[] { }));
            Account account;
            if (accountEntity == null)
            {
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT));
                return response;
            }
            account = (Account)accountEntity;


            account.Name = $"{customer.contact.name.firstName} { customer.contact.name.lastName}";

            //if customer type exists in passed WSCustomer class, add Entity reference using customer type Id (better performance since adding using primary key) otherwise use customer type to query entity and add as reference
            account.cub_AccountTypeId = (customer.customerTypeId.HasValue ? new EntityReference(cub_AccountType.EntityLogicalName, customer.customerTypeId.Value) : new EntityReference(cub_AccountType.EntityLogicalName, cub_AccountType.cub_accountnameAttribute, customer.customerType));
            CubLogger.Debug($"Updating Account in the system: Name {account.Name}");

            _organizationService.Update(account);


            CubLogger.Debug($"Account successfully updated Id: { customer.customerId.Value}");


            CubLogger.Debug("Method Exit");
            response.ResponseObject = customer;
            return response;

        }

        /// <summary>
        /// Validate customer fields. Exit the fnction on first failed validation
        /// </summary>
        /// <param name="customer">WSCustomer</param>
        /// <returns>WSMSDResponse</returns>
        private CubResponse<WSCustomer> ValidateCustomer(WSCustomer customer)
        {
            CubResponse<WSCustomer> response = new CubResponse<WSCustomer>();


            GlobalsLogic globalLogic = new GlobalsLogic(_organizationService);

            #region verify required fields

            //customer type is required
            if (string.IsNullOrEmpty(customer.customerType))
            {
                response.Errors.Add(new WSMSDFault(CUB.MSD.Model.Account.cub_accounttypeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                return response;
            }

            //check if customer type is valid
            Guid? customerTypeId = globalLogic.GetLookupRecord(cub_AccountType.EntityLogicalName, cub_AccountType.cub_accountnameAttribute, customer.customerType);
            if (!customerTypeId.HasValue)
            {

                response.Errors.Add(new WSMSDFault(CUB.MSD.Model.Account.cub_accounttypeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_CUSTOMER_TYPE));
                return response;
            }
            else
                customer.customerTypeId = customerTypeId.Value;

            //contact information is required
            if (customer.contact == null)
            {
                response.Errors.Add(new WSMSDFault(CUB.MSD.Model.Contact.EntityLogicalName, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                return response;
            }
            #endregion

            //Validate contact
            ContactLogic conLogic = new ContactLogic(_organizationService);
            CubResponse<WSCustomerContact> contactResponse = conLogic.ValidateContactForUpdate(customer.contact);
            if (!contactResponse.Success)
                response.Errors.AddRange(contactResponse.Errors);

            return response;
        }

        /// <summary>
        /// Customer Search
        /// </summary>
        /// <param name="customerType">Customer Type (Required)</param>
        /// <param name="firstname">First name</param>
        /// <param name="lastname">Last Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">phone</param>
        /// <param name="postalCode">postal Code</param>
        /// <param name="personalIdentifierType">Personal Identifier Type</param>
        /// <param name="personalIdentifier">Personal Identifier</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="offset">offset</param>
        /// <param name="limit">limit</param>
        /// <returns>WSCustomerSearchResponse</returns>
        public CubResponse<Model.NIS.WSCustomerSearchResponse> SearchCustomer(string customerType, string firstname, string lastname, string email, string phone, string postalCode, 
            string personalIdentifierType, string personalIdentifier, string username, string customerStatus, string parentCustomerId, string businessName, string dob,
            string pin, string sortBy, int offset, int limit)
        {
            CubLogger.Debug("Method Entered");

            CubResponse<Model.NIS.WSCustomerSearchResponse> response = new CubResponse<Model.NIS.WSCustomerSearchResponse>();
            try
            {
                #region Setup and Validate
                CubLogger.Debug("Entering setup and validating");

                //If no search parameters supplied, return error
                if (string.IsNullOrEmpty(customerType))
                {
                    response.Errors.Add(new WSMSDFault("CustomerType", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }
                else
                {
                    GlobalsLogic globalLogic = new GlobalsLogic(_organizationService);
                    Guid? customerTypeId = globalLogic.GetLookupRecord(cub_AccountType.EntityLogicalName, cub_AccountType.cub_accountnameAttribute, customerType);
                    if (!customerTypeId.HasValue)
                    {
                        response.Errors.Add(new WSMSDFault(Account.cub_accounttypeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_CUSTOMER_TYPE));
                        return response;
                    }
                }

                if (!string.IsNullOrEmpty(personalIdentifier) && string.IsNullOrEmpty(personalIdentifierType))
                {
                    response.Errors.Add(new WSMSDFault(Contact.cub_personalidentifiertypeAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_PERSONAL_IDENTIFIER_TYPE));
                    return response;
                }

                if (string.IsNullOrEmpty(customerType) && string.IsNullOrEmpty(firstname) && string.IsNullOrEmpty(lastname) && string.IsNullOrEmpty(postalCode) &&
                    string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(postalCode) && string.IsNullOrEmpty(personalIdentifierType) &&
                    string.IsNullOrEmpty(personalIdentifier) && string.IsNullOrEmpty(username) && string.IsNullOrEmpty(parentCustomerId) && string.IsNullOrEmpty(businessName))
                    return null;

                //trim all white spaces
                if (!string.IsNullOrEmpty(customerType)) customerType = customerType.Trim();
                if (!string.IsNullOrEmpty(firstname)) firstname = firstname.Trim();
                if (!string.IsNullOrEmpty(lastname)) lastname = lastname.Trim();
                if (!string.IsNullOrEmpty(email)) email = email.Trim();
                if (!string.IsNullOrEmpty(phone)) phone = phone.Trim();
                if (!string.IsNullOrEmpty(postalCode)) postalCode = postalCode.Trim();
                if (!string.IsNullOrEmpty(personalIdentifierType)) personalIdentifierType = personalIdentifierType.Trim();
                if (!string.IsNullOrEmpty(personalIdentifier)) personalIdentifier = personalIdentifier.Trim();
                if (!string.IsNullOrEmpty(username)) username = username.Trim();
                if (!string.IsNullOrEmpty(customerStatus)) customerStatus = customerStatus.Trim();
                if (!string.IsNullOrEmpty(parentCustomerId)) parentCustomerId = parentCustomerId.Trim();
                if (!string.IsNullOrEmpty(businessName)) businessName = businessName.Trim();
                if (!string.IsNullOrEmpty(pin)) pin = pin.Trim();
                if (!string.IsNullOrEmpty(dob)) dob = dob.Trim();
                CubLogger.Debug("Exiting setup and validating");
                #endregion Validate

                CubLogger.Debug("Getting ready to get the list of accounts");
                EntityCollection res = null;

                if (!string.IsNullOrEmpty(parentCustomerId))
                {
                    List<string> customerId = new List<string>(1);
                    customerId.Add(parentCustomerId);
                    res = GetFilteredAndSortedCustomers(customerId, customerType, firstname, lastname, email, phone, postalCode, personalIdentifierType, personalIdentifier, username, customerStatus,
                        businessName, dob, pin, sortBy, offset, limit);
                }
                else
                {
                    res = GetFilteredAndSortedCustomers(null, customerType, firstname, lastname, email, phone, postalCode, personalIdentifierType, personalIdentifier, username, customerStatus,
                        businessName, dob, pin, sortBy, offset, limit);
                }
                CubLogger.Debug("Got the list of accounts");

                if (res != null && res.Entities.Count > 0)
                {
                    Guid[] accountGuids = (from c in res.Entities select c.Id).Distinct().ToArray();
                    List<Model.NIS.WSCustomerSearchResult> searchListResults = new List<Model.NIS.WSCustomerSearchResult>();

                    CubLogger.Debug("Getting ready to build the result set");
                    foreach (Guid g in accountGuids)
                    {
                        Model.NIS.WSCustomerSearchResult searchListResult = new Model.NIS.WSCustomerSearchResult();
                        searchListResult = GetCustomerSearchResult(g, firstname, lastname, email, phone, postalCode, personalIdentifierType, personalIdentifier, sortBy);
                        searchListResults.Add(searchListResult);
                    }
                    Model.NIS.WSCustomerSearchResponse ret = new Model.NIS.WSCustomerSearchResponse();
                    ret.customers = searchListResults;
                    ret.totalCount = searchListResults.Count();
                    response.ResponseObject = ret;
                    CubLogger.Debug("Result set built");
                }
                else
                {
                    CubLogger.Error(CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT);
                    //response.Errors.Add(new WSMSDFault("SearchCustomer", CUBConstants.Error.ERR_GENERAL_EXCEPTION, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT));
                    Model.NIS.WSCustomerSearchResponse ret = new Model.NIS.WSCustomerSearchResponse();
                    ret.customers = new List<Model.NIS.WSCustomerSearchResult>();
                    ret.totalCount = ret.customers.Count();
                    response.ResponseObject = ret;
                    CubLogger.Debug("Empty result set built");
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("SearchCustomer", CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            CubLogger.Debug("Method Exit");
            return response;
        }

        /// <summary>
        /// Customer Search By Id
        /// </summary>
        /// <param name="customerIds">required-List</param>
        /// <param name="sortBy"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns>WSCustomerSearchResponse</returns>
        public CubResponse<Model.NIS.WSCustomerSearchResponse> SearchCustomerById(List<string> customerIds, string sortBy, int offset, int limit)
        {
            CubLogger.Debug("Method Entered");
            CubResponse<Model.NIS.WSCustomerSearchResponse> response = new CubResponse<Model.NIS.WSCustomerSearchResponse>();

            try
            {
                if (customerIds.Count == 0)
                {
                    response.Errors.Add(new WSMSDFault("CustomerIds", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }

                CubLogger.Debug("Getting ready to get the list of accounts sorted and filtered");
                EntityCollection res = GetFilteredAndSortedCustomers(customerIds, null, null, null, null, null, null, null, null, null, null, null, null, null, sortBy, offset, limit);
                CubLogger.Debug("Got the list of sorted and filtered accounts");

                CubLogger.Debug("Getting ready to build the result set");
                List<Model.NIS.WSCustomerSearchResult> searchListResults = new List<Model.NIS.WSCustomerSearchResult>();
                if (res != null && res.Entities.Count > 0)
                {
                    Guid[] accountGuids = (from c in res.Entities select c.Id).Distinct().ToArray();
                    
                    CubLogger.Debug("Getting ready to build the result set");
                    foreach (Guid g in accountGuids)
                    {
                        Model.NIS.WSCustomerSearchResult searchListResult = new Model.NIS.WSCustomerSearchResult();
                        searchListResult = GetCustomerSearchResult(g, null, null, null, null, null, null, null, sortBy);
                        searchListResults.Add(searchListResult);
                    }

                    Model.NIS.WSCustomerSearchResponse custRet = new Model.NIS.WSCustomerSearchResponse();
                    custRet.customers = searchListResults;
                    custRet.totalCount = searchListResults.Count();
                    response.ResponseObject = custRet;
                    CubLogger.Debug("Result set built");
                }
                else
                {
                    CubLogger.Error(CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT);
                    //response.Errors.Add(new WSMSDFault("SearchCustomerById", CUBConstants.Error.ERR_GENERAL_EXCEPTION, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT));
                    Model.NIS.WSCustomerSearchResponse ret = new Model.NIS.WSCustomerSearchResponse();
                    ret.customers = new List<Model.NIS.WSCustomerSearchResult>();
                    ret.totalCount = ret.customers.Count();
                    response.ResponseObject = ret;
                    CubLogger.Debug("Empty result set built");
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("SearchCustomerById", CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            CubLogger.Debug("Method Exit");
            return response;
        }

        /// <summary>
        /// GetFilteredAndSortedCustomers
        /// </summary>
        /// <param name="customerIds"></param>
        /// <param name="customerType"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="postalCode"></param>
        /// <param name="personalIdentifierType"></param>
        /// <param name="personalIdentifier"></param>
        /// <param name="sortBy"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns>EntityCollection</returns>
        private EntityCollection GetFilteredAndSortedCustomers(List<string> customerIds, string customerType, string firstname, string lastname, string email, string phone, 
            string postalCode, string personalIdentifierType, string personalIdentifier, string username, string customerStatus, string businessName, string dob,
            string pin, string sortBy, int offset, int limit)
        {
            CubLogger.Debug("Method Entered");
            EntityCollection ec = new EntityCollection();
            int skip = 0;
            try
            {
                #region Validate and Setup
                JoinOperator contactJoin = new JoinOperator();
                JoinOperator addyJoin = new JoinOperator();
                JoinOperator credsJoin = new JoinOperator();
                Dictionary<string, OrderType> searchSort = new Dictionary<string, OrderType>();

                if (string.IsNullOrEmpty(firstname) && string.IsNullOrEmpty(lastname) &&
                    string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(personalIdentifierType) &&
                    string.IsNullOrEmpty(personalIdentifier) && string.IsNullOrEmpty(businessName) && string.IsNullOrEmpty(dob))
                    contactJoin = JoinOperator.LeftOuter;
                else
                    contactJoin = JoinOperator.Inner;

                if (string.IsNullOrEmpty(postalCode))
                    addyJoin = JoinOperator.LeftOuter;
                else
                    addyJoin = JoinOperator.Inner;

                if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(pin) && string.IsNullOrEmpty(customerStatus))
                    credsJoin = JoinOperator.LeftOuter;
                else
                    credsJoin = JoinOperator.Inner;

                //trim all white spaces
                if (!string.IsNullOrEmpty(customerType)) customerType = customerType.Trim();
                if (!string.IsNullOrEmpty(firstname)) firstname = firstname.Trim();
                if (!string.IsNullOrEmpty(lastname)) lastname = lastname.Trim();
                if (!string.IsNullOrEmpty(email)) email = email.Trim();
                if (!string.IsNullOrEmpty(phone)) phone = phone.Trim();
                if (!string.IsNullOrEmpty(postalCode)) postalCode = postalCode.Trim();
                if (!string.IsNullOrEmpty(personalIdentifierType)) personalIdentifierType = personalIdentifierType.Trim();
                if (!string.IsNullOrEmpty(personalIdentifier)) personalIdentifier = personalIdentifier.Trim();
                if (!string.IsNullOrEmpty(username)) username = username.Trim();
                if (!string.IsNullOrEmpty(customerStatus)) customerStatus = customerStatus.Trim();
                if (!string.IsNullOrEmpty(businessName)) businessName = businessName.Trim();
                if (!string.IsNullOrEmpty(dob)) dob = dob.Trim();
                if (!string.IsNullOrEmpty(pin)) pin = pin.Trim();
                if (!string.IsNullOrEmpty(sortBy)) sortBy = sortBy.Trim();

                //offset is the record number not the page number.  With that in mind
                //we need to mimic the skip and take functionality of IEnumerator...

                int pageNumber = 0;
                int numOfRecordsToRetrieve = 0;
                
                //validate max not exceeded
                limit = (limit <= 0) ? numOfRecordsToRetrieve = 10 : limit;

                if (offset <= 0)
                {
                    pageNumber = 1;
                    numOfRecordsToRetrieve = limit;
                }
                else
                {
                    if (offset < limit)
                    {
                        numOfRecordsToRetrieve = (offset + limit);
                        pageNumber = 1;
                        skip = offset;
                    }
                    else if (offset == limit)
                    {
                        numOfRecordsToRetrieve = limit;
                        pageNumber = 2;
                    }
                    else if (offset > limit)
                    {
                        double val = Math.IEEERemainder((double)offset, (double)limit);
                        if (val == 0)
                        {
                            numOfRecordsToRetrieve = limit;
                            pageNumber = offset/limit;
                        }
                        else
                        {
                            numOfRecordsToRetrieve = offset;
                            pageNumber = 2;
                        }
                    }
                }
                #endregion Validate and Setup

                #region Setup QueryExpression
                var queryExpression = new QueryExpression(Account.EntityLogicalName);
                queryExpression.NoLock = true;
                queryExpression.Distinct = true;
                queryExpression.ColumnSet = new ColumnSet(Account.accountidAttribute,
                                                        Account.nameAttribute,
                                                        Account.statuscodeAttribute,
                                                        Account.cub_accounttypeidAttribute);
                FilterExpression accountFilter = new FilterExpression(LogicalOperator.And);
                if (customerIds != null && customerIds.Count > 0)
                {               
                    object[] values = customerIds.ToArray();
                    accountFilter.AddCondition(Account.accountidAttribute, ConditionOperator.In, values);
                }
                if (!string.IsNullOrEmpty(businessName))
                {
                    if (SearchAsBeginsWith(businessName))
                        accountFilter.AddCondition(Account.nameAttribute, ConditionOperator.BeginsWith, businessName.Remove(businessName.Length - 1));
                    //else if (SearchAsContains(businessName))
                    //    accountFilter.AddCondition(Account.nameAttribute, ConditionOperator.Contains, businessName.Substring(1, businessName.Length - 2));
                    else if (SearchAsEndsWith(businessName))
                        accountFilter.AddCondition(Account.nameAttribute, ConditionOperator.EndsWith, businessName.Remove(0, 1));
                    else
                        accountFilter.AddCondition(Account.nameAttribute, ConditionOperator.Equal, businessName);
                }
                if (accountFilter.Conditions.Count > 0) queryExpression.Criteria = accountFilter;
                queryExpression.PageInfo = new PagingInfo() { PageNumber = pageNumber, Count = numOfRecordsToRetrieve };
                #endregion Setup QueryExpression

                #region Link Contact to Account
                queryExpression.LinkEntities.Add(new LinkEntity(Account.EntityLogicalName, Contact.EntityLogicalName, Account.accountidAttribute,
                                                                Contact.parentcustomeridAttribute, contactJoin));
                FilterExpression contactFilter = new FilterExpression(LogicalOperator.And);
                FilterExpression phoneFilter = new FilterExpression(LogicalOperator.Or);

                if (!string.IsNullOrEmpty(firstname))
                {
                    if (SearchAsBeginsWith(firstname))
                        contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.BeginsWith, firstname.Remove(firstname.Length - 1));
                    //else if (SearchAsContains(firstname))
                    //    contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.Contains, firstname.Substring(1, firstname.Length - 2));
                    else if (SearchAsEndsWith(firstname))
                        contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.EndsWith, firstname.Remove(0, 1));
                    else
                        contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.Equal, firstname);
                }
                if (!string.IsNullOrEmpty(lastname))
                {
                    if (SearchAsBeginsWith(lastname))
                        contactFilter.AddCondition(Contact.lastnameAttribute, ConditionOperator.BeginsWith, lastname.Remove(lastname.Length - 1));
                    //else if (SearchAsContains(lastname))
                    //    contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.Contains, lastname.Substring(1, lastname.Length - 2));
                    else if (SearchAsEndsWith(lastname))
                        contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.EndsWith, lastname.Remove(0, 1));
                    else
                        contactFilter.AddCondition(Contact.lastnameAttribute, ConditionOperator.Equal, lastname);
                }
                if (!string.IsNullOrEmpty(email))
                {
                    if (SearchAsBeginsWith(email))
                        contactFilter.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.BeginsWith, email.Remove(email.Length - 1));
                    //else if (SearchAsContains(email))
                    //    contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.Contains, email.Substring(1, email.Length - 2));
                    else if (SearchAsEndsWith(email))
                        contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.EndsWith, email.Remove(0, 1));
                    else
                        contactFilter.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.Equal, email);
                }
                if (!string.IsNullOrEmpty(dob))
                {
                    contactFilter.AddCondition(Contact.birthdateAttribute, ConditionOperator.Equal, dob);
                }
                if (!string.IsNullOrEmpty(phone))
                {
                    if (SearchAsBeginsWith(phone))
                    {
                        phone = phone.Remove(phone.Length - 1);

                        if (!string.IsNullOrEmpty(phone))
                        {
                            phoneFilter.AddCondition(Contact.telephone1Attribute, ConditionOperator.BeginsWith, phone.Trim());
                            phoneFilter.AddCondition(Contact.telephone2Attribute, ConditionOperator.BeginsWith, phone.Trim());
                            phoneFilter.AddCondition(Contact.telephone3Attribute, ConditionOperator.BeginsWith, phone.Trim());
                        }
                    }
                    //else if (SearchAsContains(phone))
                    //{
                    //    phone = phone.Substring(1, phone.Length - 2);

                    //    if (!string.IsNullOrEmpty(phone))
                    //    {
                    //        phoneFilter.AddCondition(Contact.telephone1Attribute, ConditionOperator.Contains, phone.Trim());
                    //        phoneFilter.AddCondition(Contact.telephone2Attribute, ConditionOperator.Contains, phone.Trim());
                    //        phoneFilter.AddCondition(Contact.telephone3Attribute, ConditionOperator.Contains, phone.Trim());
                    //    }
                    //}
                    else if (SearchAsEndsWith(phone))
                    {
                        phone = phone.Remove(0, 1);

                        if (!string.IsNullOrEmpty(phone))
                        {
                            phoneFilter.AddCondition(Contact.telephone1Attribute, ConditionOperator.EndsWith, phone.Trim());
                            phoneFilter.AddCondition(Contact.telephone2Attribute, ConditionOperator.EndsWith, phone.Trim());
                            phoneFilter.AddCondition(Contact.telephone3Attribute, ConditionOperator.EndsWith, phone.Trim());
                        }
                    }
                    else
                    {
                        phoneFilter.AddCondition(Contact.telephone1Attribute, ConditionOperator.Equal, phone.Trim());
                        phoneFilter.AddCondition(Contact.telephone2Attribute, ConditionOperator.Equal, phone.Trim());
                        phoneFilter.AddCondition(Contact.telephone3Attribute, ConditionOperator.Equal, phone.Trim());
                    }
                }

                if (contactFilter.Conditions.Count > 0) queryExpression.LinkEntities[0].LinkCriteria = contactFilter;
                if (phoneFilter.Conditions.Count > 0) queryExpression.LinkEntities[0].LinkCriteria.AddFilter(phoneFilter);
                queryExpression.LinkEntities[0].EntityAlias = "contact";
                #endregion Link Contact to Account

                #region Link Account Type To Account
                queryExpression.LinkEntities.Add(new LinkEntity(Account.EntityLogicalName, cub_AccountType.EntityLogicalName,
                                                                Account.cub_accounttypeidAttribute, cub_AccountType.cub_accounttypeidAttribute,
                                                                JoinOperator.Inner));
                FilterExpression accTypeFilter = new FilterExpression(LogicalOperator.And);
                if (!string.IsNullOrEmpty(customerType))
                {
                    accTypeFilter.AddCondition(cub_AccountType.cub_accountnameAttribute, ConditionOperator.Equal, customerType);
                }
                if (accTypeFilter.Conditions.Count > 0) queryExpression.LinkEntities[1].LinkCriteria = accTypeFilter;

                queryExpression.LinkEntities[1].EntityAlias = "accountType";
                #endregion Link Account Type

                #region Link Address to Contact
                queryExpression.LinkEntities[0].LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_Address.EntityLogicalName, Contact.cub_addressidAttribute,
                                                                cub_Address.cub_addressidAttribute, addyJoin));
                queryExpression.LinkEntities[0].LinkEntities[0].EntityAlias = "address";
                #endregion Link Address to Contact

                #region Link Postal Code to Address
                queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities.Add(new LinkEntity(cub_Address.EntityLogicalName, cub_ZipOrPostalCode.EntityLogicalName,
                                                                cub_Address.cub_postalcodeidAttribute, cub_ZipOrPostalCode.cub_ziporpostalcodeidAttribute, addyJoin));
                FilterExpression postCodeFilter = new FilterExpression(LogicalOperator.And);
                if (!string.IsNullOrEmpty(postalCode))
                {
                    postCodeFilter.AddCondition(cub_ZipOrPostalCode.cub_codeAttribute, ConditionOperator.Equal, postalCode);
                }
                if (postCodeFilter.Conditions.Count > 0) queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities[0].LinkCriteria = postCodeFilter;
                queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities[0].EntityAlias = "postalCode";
                #endregion Link Postal Code to Address

                #region Link Security Answers to Contact
                queryExpression.LinkEntities[0].LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_SecurityAnswer.EntityLogicalName, Contact.contactidAttribute, 
                                                                cub_SecurityAnswer.cub_contactidAttribute, JoinOperator.LeftOuter));
                queryExpression.LinkEntities[0].LinkEntities[1].EntityAlias = "contactQAs";
                #endregion Link Security Answers to Contact

                #region Link Credentials to Contact
                queryExpression.LinkEntities[0].LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_Credential.EntityLogicalName, Contact.contactidAttribute, 
                                                                cub_Credential.cub_contactidAttribute, credsJoin));
                FilterExpression credentialsFilter = new FilterExpression(LogicalOperator.And);
                if (!string.IsNullOrEmpty(username))
                {
                    if (SearchAsBeginsWith(username))
                        credentialsFilter.AddCondition(cub_Credential.cub_usernameAttribute, ConditionOperator.BeginsWith, username.Remove(username.Length - 1));
                    //else if (SearchAsContains(username))
                    //    credentialsFilter.AddCondition(cub_Credential.cub_usernameAttribute, ConditionOperator.Contains, username.Substring(1, username.Length - 2));
                    else if (SearchAsEndsWith(username))
                        credentialsFilter.AddCondition(cub_Credential.cub_usernameAttribute, ConditionOperator.EndsWith, username.Remove(0, 1));
                    else
                        credentialsFilter.AddCondition(cub_Credential.cub_usernameAttribute, ConditionOperator.Equal, username);
                }
                if (!string.IsNullOrEmpty(pin))
                {
                    credentialsFilter.AddCondition(cub_Credential.cub_pinAttribute, ConditionOperator.Equal, pin);
                }
                if (!string.IsNullOrEmpty(customerStatus))
                {
                    credentialsFilter.AddCondition(cub_Credential.statecodeAttribute, ConditionOperator.Equal, customerStatus);
                }
                if (credentialsFilter.Conditions.Count > 0) queryExpression.LinkEntities[0].LinkEntities[2].LinkCriteria = credentialsFilter;
                queryExpression.LinkEntities[0].LinkEntities[2].EntityAlias = "contactCreds";
                #endregion Link Credentials to Contact

                #region Sort
                if (!string.IsNullOrEmpty(sortBy))
                {
                    sortBy = sortBy.Trim();

                    string[] sortCriteria = sortBy.Split(',');
                    foreach (string s in sortCriteria)
                    {
                        string[] foundSort = s.Split('.');
                        if (foundSort.Length != 2 && (!foundSort[1].Equals("asc", StringComparison.OrdinalIgnoreCase) || !foundSort[1].Equals("desc", StringComparison.OrdinalIgnoreCase)))
                        {
                            throw new FaultException<WSMSDFault>(new WSMSDFault("Invalid sort expression, expression should be <field_name>.<sort_order(asc/desc)>", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND));
                        }
                        searchSort.Add(foundSort[0].ToLower(), foundSort[1].Equals("asc", StringComparison.OrdinalIgnoreCase) ? OrderType.Ascending : OrderType.Descending);
                    }

                    foreach (KeyValuePair<string, OrderType> kvp in searchSort)
                    {
                        switch (kvp.Key)
                        {
                            case Account.nameAttribute:
                                queryExpression.Orders.Add(new OrderExpression(Account.nameAttribute, kvp.Value));
                                break;
                            //case cub_AccountType.cub_accountnameAttribute:
                            //case "customerType":
                            //    queryExpression.LinkEntities[1].Orders.Add(new OrderExpression(cub_AccountType.cub_accountnameAttribute, kvp.Value));
                            //    break;
                            //case Contact.cub_contacttypeidAttribute:
                            //    orderList.add(sortFied.isAsc() ? contact.contactType.contactTypeEnum.asc() : contact.contactType.contactTypeEnum.desc());
                            //    break;
                            case Contact.firstnameAttribute:
                                queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.firstnameAttribute, kvp.Value));
                                break;
                            case Contact.lastnameAttribute:
                                queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.lastnameAttribute, kvp.Value));
                                break;
                            case Contact.emailaddress1Attribute:
                            case "email":
                                queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.emailaddress1Attribute, kvp.Value));
                                break;
                            case Contact.telephone1Attribute:
                                queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.telephone1Attribute, kvp.Value));
                                break;
                            case Contact.birthdateAttribute:
                            case "dateofbirth":
                                queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.birthdateAttribute, kvp.Value));
                                break;
                            case cub_Address.cub_street1Attribute:
                            case "address1":
                                queryExpression.LinkEntities[0].LinkEntities[0].Orders.Add(new OrderExpression(cub_Address.cub_street1Attribute, kvp.Value));
                                break;
                            case cub_Address.cub_street2Attribute:
                            case "address2":
                                queryExpression.LinkEntities[0].LinkEntities[0].Orders.Add(new OrderExpression(cub_Address.cub_street2Attribute, kvp.Value));
                                break;
                            case cub_Address.cub_cityAttribute:
                            case "city":
                                queryExpression.LinkEntities[0].LinkEntities[0].Orders.Add(new OrderExpression(cub_Address.cub_cityAttribute, kvp.Value));
                                break;
                        }
                    }
                }
                #endregion Sort

                ec =  this._organizationService.RetrieveMultiple(queryExpression);
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                return null;
            }

            IEnumerable<Entity> dc =  ec.Entities.Skip(skip).Take(limit);
            EntityCollection newEc = new EntityCollection();
            foreach (var i in dc)
            {
                newEc.Entities.Add(i);
            }

            CubLogger.Debug("Method Exit");
            return newEc;
        }

        /// <summary>
        /// is there a wildcard at the end of the text to search
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private bool SearchAsBeginsWith(string text)
        {
            bool RET_VAL = false;

            if (!string.IsNullOrEmpty(text))
            {
                if (text.Substring(text.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                {
                    text = text.Remove(text.Length - 1);
                    RET_VAL = true;

                    if (!string.IsNullOrEmpty(text) && text.Substring(0, 1) == CUBConstants.Common.SearchWildCharacter) RET_VAL = false;
                }
            }

            return RET_VAL;
        }

        /// <summary>
        /// is there text that begins and ends with a wildcard (right not Dymanics 356 does not support this)
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private bool SearchAsContains(string text)
        {
            bool RET_VAL = false;

            if (!string.IsNullOrEmpty(text))
            {
                if (text.Substring(0, 1) == CUBConstants.Common.SearchWildCharacter)
                {
                    text = text.Remove(0, 1);

                    if (!string.IsNullOrEmpty(text) && text.Substring(text.Length - 1) == CUBConstants.Common.SearchWildCharacter) RET_VAL = true;
                }
            }

            return RET_VAL;
        }

        /// <summary>
        /// is there a wildcard at the beginning of the text to search
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private bool SearchAsEndsWith(string text)
        {
            bool RET_VAL = false;

            if (!string.IsNullOrEmpty(text))
            {
                if (text.Substring(0, 1) == CUBConstants.Common.SearchWildCharacter)
                {
                    text = text.Remove(0, 1);
                    RET_VAL = true;

                    if (!string.IsNullOrEmpty(text) && text.Substring(text.Length - 1) == CUBConstants.Common.SearchWildCharacter) RET_VAL = false;
                }
            }

            return RET_VAL;
        }

        /// <summary>
        /// GetCustomerSearchResult
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="firstname">firstname</param>
        /// <param name="lastname">lastname</param>
        /// <param name="email">email</param>
        /// <param name="phone">phone</param>
        /// <param name="postalCode">postalCode</param>
        /// <param name="personalIdentifierType">personalIdentifierType</param>
        /// <param name="personalIdentifier">personalIdentifier</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="offset">offset</param>
        /// <param name="limit">limit</param>
        /// <returns>WSCustomerSearchResult</returns>
        private Model.NIS.WSCustomerSearchResult GetCustomerSearchResult(Guid customerId, string firstname, string lastname, string email, string phone, string postalCode, string personalIdentifierType, string personalIdentifier, string sortBy)
        {
            CubLogger.Debug("Method Entered");
            try
            {
                JoinOperator contactJoin = new JoinOperator();
                JoinOperator addyJoin = new JoinOperator();
                Dictionary<string, OrderType> searchSort = new Dictionary<string, OrderType>();

                sortBy = PrepareSortBy(firstname, lastname, email, phone, postalCode, personalIdentifierType, personalIdentifier, sortBy, out contactJoin, out addyJoin, searchSort);

                QueryExpression queryExpression = CreateQueryExpression(customerId);

                #region Link Contact to Account
                LinkContactToAccount(firstname, lastname, email, phone, contactJoin, queryExpression);
                #endregion Link Contact to Account

                #region Link Account Type To Account
                LinkAccountTypeToAccount(queryExpression);
                #endregion Link Account Type

                #region Link Address to Contact
                LinkAddressToContact(addyJoin, queryExpression);
                LinkCountryToAddress(addyJoin, queryExpression);
                #endregion Link Address to Contact

                #region Link Postal Code to Address
                LinkPostalCodeToAddress(postalCode, addyJoin, queryExpression);
                #endregion Link Postal Code to Address

                #region Link Credentials to Contact
                LinkCredentialsToContact(queryExpression);
                #endregion Link Credentials to Contact

                #region Sort
                AdjustSorting(sortBy, searchSort, queryExpression);
                #endregion Sort

                EntityCollection res = this._organizationService.RetrieveMultiple(queryExpression);

                Model.NIS.WSCustomerSearchResult searchListResult = new Model.NIS.WSCustomerSearchResult();

                searchListResult.customerId = customerId.ToString().ToUpper();
                searchListResult.customerType = (from c in res.Entities
                                                 where c.Id.Equals(customerId)
                                                 select ReturnStringValue(GetAttributeValue(c, "accountType." + cub_AccountType.cub_accountnameAttribute))
                                                ).FirstOrDefault();

                string accountstatus = (from c in res.Entities
                                where c.Id.Equals(customerId)
                                select (GetAttributeOptionSetText(c, Account.statuscodeAttribute, typeof(account_statuscode)))
                                                ).FirstOrDefault();

                account_statuscode choice;
                int value = -1;

                if (Enum.TryParse(accountstatus, out choice))
                {
                    value = (int)choice;
                }

                if (value > -1)
                {
                    searchListResult.customerStatus = new Model.NIS.WSKeyValue();
                    searchListResult.customerStatus.key = value.ToString();
                    searchListResult.customerStatus.value = accountstatus;
                }

                int contactCount = (from c in res.Entities
                                    where c.Id.Equals(customerId) && !string.IsNullOrEmpty(ReturnStringValue(GetAttributeValue(c, "contact." + Contact.contactidAttribute)))
                                    select "contact." + Contact.contactidAttribute
                                    ).Count();

                if (contactCount > 0)
                {
                    FillSearchResultWithContacts(customerId, res, searchListResult);
                }
                else
                    searchListResult.contacts = null;

                return searchListResult;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// FillSearchResultWithContacts
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="res">res</param>
        /// <param name="searchListResult">searchListResult</param>
        private void FillSearchResultWithContacts(Guid customerId, EntityCollection res, Model.NIS.WSCustomerSearchResult searchListResult)
        {
            searchListResult.contacts = (from c in res.Entities
                                         where c.Id.Equals(customerId)
                                         select new Model.NIS.WSCustomerContactInfo
                                         {
                                             contactId = ReturnStringValue(GetAttributeValue(c, "contact." + Contact.contactidAttribute)),
                                             contactType = ReturnStringValue(GetAttributeValue(c, "contact." + Contact.cub_contacttypeidAttribute)),
                                             contactStatus = GetAttributeOptionSetText(c, "contact." + Contact.statuscodeAttribute, typeof(contact_statuscode)),
                                             name = new Model.NIS.WSName
                                             {
                                                 firstName = ReturnStringValue(GetAttributeValue(c, "contact." + Contact.firstnameAttribute)),
                                                 lastName = ReturnStringValue(GetAttributeValue(c, "contact." + Contact.lastnameAttribute)),
                                                 middleInitial = ReturnStringValue(GetAttributeValue(c, "contact." + Contact.middlenameAttribute)),
                                                 nameSuffix = GetAttributeOptionSetText(c, "contact." + Contact.cub_suffixAttribute, typeof(cub_contactsuffix)),
                                                 title = GetAttributeOptionSetText(c, "contact." + Contact.cub_nametitleAttribute, typeof(cub_nametitles))
                                             },
                                             address = new Model.NIS.WSAddressExt
                                             {
                                                 addressId = ReturnStringValue(GetAttributeValue(c, "address." + cub_Address.cub_addressidAttribute)),
                                                 address1 = ReturnStringValue(GetAttributeValue(c, "address." + cub_Address.cub_street1Attribute)),
                                                 address2 = ReturnStringValue(GetAttributeValue(c, "address." + cub_Address.cub_street2Attribute)),
                                                 city = ReturnStringValue(GetAttributeValue(c, "address." + cub_Address.cub_cityAttribute)),
                                                 state = ReturnStringValue(GetAttributeValue(c, "address." + cub_Address.cub_stateprovinceidAttribute)),
                                                 country = ReturnStringValue(GetAttributeValue(c, "country." + cub_Country.cub_alpha2Attribute)),
                                                 postalCode = ReturnStringValue(GetAttributeValue(c, "address." + cub_Address.cub_postalcodeidAttribute))
                                             },
                                             phone = BuildPhoneList(c),
                                             email = ReturnStringValue(GetAttributeValue(c, "contact." + Contact.emailaddress1Attribute)),
                                             dateOfBirth = ReturnStringValue((GetAttributeValue(c, "contact." + Contact.birthdateAttribute)), "bday"),
                                             personalIdentifierInfo = new Model.NIS.WSPersonalIdentifier
                                             {
                                                 personalIdentifier = ReturnStringValue(GetAttributeValue(c, "contact." + Contact.cub_personalidentifierAttribute)),
                                                 personalIdentifierType = GetAttributeOptionSetText(c, "contact." + Contact.cub_personalidentifiertypeAttribute, typeof(Contactcub_PersonalIdentifierType))
                                             },
                                             username = ReturnStringValue(GetAttributeValue(c, "contactCreds." + cub_Credential.cub_usernameAttribute)),
                                             pin = ReturnStringValue(GetAttributeValue(c, "contactCreds." + cub_Credential.cub_pinAttribute)),
                                             securityQAs = BuildSecurityQAs(customerId.ToString(), ReturnStringValue(GetAttributeValue(c, "contact." + Contact.contactidAttribute))),
                                             credentialStatus = GetAttributeOptionSetText(c, "contactCreds." + cub_Credential.statuscodeAttribute, typeof(cub_credential_statuscode))
                                         }).Distinct().ToArray();
        }

        /// <summary>
        /// AdjustSorting
        /// </summary>
        /// <param name="sortBy">sortBy</param>
        /// <param name="searchSort">searchSort</param>
        /// <param name="queryExpression">queryExpression</param>
        private static void AdjustSorting(string sortBy, Dictionary<string, OrderType> searchSort, QueryExpression queryExpression)
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                foreach (KeyValuePair<string, OrderType> kvp in searchSort)
                {
                    switch (kvp.Key)
                    {
                        case Account.nameAttribute:
                            queryExpression.Orders.Add(new OrderExpression(Account.nameAttribute, kvp.Value));
                            break;
                        //case cub_AccountType.cub_accountnameAttribute:
                        //case "customerType":
                        //    queryExpression.LinkEntities[1].Orders.Add(new OrderExpression(cub_AccountType.cub_accountnameAttribute, kvp.Value));
                        //    break;
                        //case Contact.cub_contacttypeidAttribute:
                        //    orderList.add(sortFied.isAsc() ? contact.contactType.contactTypeEnum.asc() : contact.contactType.contactTypeEnum.desc());
                        //    break;
                        case Contact.firstnameAttribute:
                            queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.firstnameAttribute, kvp.Value));
                            break;
                        case Contact.lastnameAttribute:
                            queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.lastnameAttribute, kvp.Value));
                            break;
                        case Contact.emailaddress1Attribute:
                        case "email":
                            queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.emailaddress1Attribute, kvp.Value));
                            break;
                        case Contact.telephone1Attribute:
                            queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.telephone1Attribute, kvp.Value));
                            break;
                        case Contact.birthdateAttribute:
                        case "dateofbirth":
                            queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Contact.birthdateAttribute, kvp.Value));
                            break;
                        case cub_Address.cub_street1Attribute:
                        case "address1":
                            queryExpression.LinkEntities[0].LinkEntities[0].Orders.Add(new OrderExpression(cub_Address.cub_street1Attribute, kvp.Value));
                            break;
                        case cub_Address.cub_street2Attribute:
                        case "address2":
                            queryExpression.LinkEntities[0].LinkEntities[0].Orders.Add(new OrderExpression(cub_Address.cub_street2Attribute, kvp.Value));
                            break;
                        case cub_Address.cub_cityAttribute:
                        case "city":
                            queryExpression.LinkEntities[0].LinkEntities[0].Orders.Add(new OrderExpression(cub_Address.cub_cityAttribute, kvp.Value));
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// LinkCredentialsToContact
        /// </summary>
        /// <param name="queryExpression">queryExpression</param>
        private static void LinkCredentialsToContact(QueryExpression queryExpression)
        {
            queryExpression.LinkEntities[0].LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_Credential.EntityLogicalName, Contact.contactidAttribute, cub_Credential.cub_contactidAttribute, JoinOperator.LeftOuter));
            queryExpression.LinkEntities[0].LinkEntities[1].Columns.AddColumns(cub_Credential.cub_usernameAttribute, cub_Credential.cub_pinAttribute, cub_Credential.statuscodeAttribute);
            queryExpression.LinkEntities[0].LinkEntities[1].EntityAlias = "contactCreds";
        }

        /// <summary>
        /// LinkPostalCodeToAddress
        /// </summary>
        /// <param name="postalCode">postalCode</param>
        /// <param name="addyJoin">addyJoin</param>
        /// <param name="queryExpression">queryExpression</param>
        private static void LinkPostalCodeToAddress(string postalCode, JoinOperator addyJoin, QueryExpression queryExpression)
        {
            queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities.Add(new LinkEntity(cub_Address.EntityLogicalName, cub_ZipOrPostalCode.EntityLogicalName,
                                                            cub_Address.cub_postalcodeidAttribute, cub_ZipOrPostalCode.cub_ziporpostalcodeidAttribute, addyJoin));
            queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities[1].Columns.AddColumns(cub_ZipOrPostalCode.cub_codeAttribute);
            FilterExpression postCodeFilter = new FilterExpression(LogicalOperator.And);
            if (!string.IsNullOrEmpty(postalCode))
            {
                postCodeFilter.AddCondition(cub_ZipOrPostalCode.cub_codeAttribute, ConditionOperator.Equal, postalCode);
            }
            if (postCodeFilter.Conditions.Count > 0) queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities[1].LinkCriteria = postCodeFilter;
            queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities[1].EntityAlias = "postalCode";
        }

        /// <summary>
        /// LinkAddressToContact
        /// </summary>
        /// <param name="addyJoin">addyJoin</param>
        /// <param name="queryExpression">queryExpression</param>
        private static void LinkAddressToContact(JoinOperator addyJoin, QueryExpression queryExpression)
        {
            queryExpression.LinkEntities[0].LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_Address.EntityLogicalName, Contact.cub_addressidAttribute,
                                                            cub_Address.cub_addressidAttribute, addyJoin));
            queryExpression.LinkEntities[0].LinkEntities[0].Columns.AddColumns(cub_Address.cub_addressidAttribute,
                                                                                cub_Address.cub_street1Attribute,
                                                                                cub_Address.cub_street2Attribute,
                                                                                cub_Address.cub_cityAttribute,
                                                                                cub_Address.cub_stateprovinceidAttribute,
                                                                                cub_Address.cub_countryidAttribute,
                                                                                cub_Address.cub_postalcodeidAttribute);
            queryExpression.LinkEntities[0].LinkEntities[0].EntityAlias = "address";
        }

        /// <summary>
        /// LinkCredentialsToContact
        /// </summary>
        /// <param name="queryExpression">queryExpression</param>
        private static void LinkCountryToAddress(JoinOperator addyJoin, QueryExpression queryExpression)
        {
            queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities.Add(new LinkEntity(cub_Address.EntityLogicalName, cub_Country.EntityLogicalName, 
                                                                            cub_Address.cub_countryidAttribute, cub_Country.cub_countryidAttribute, addyJoin));
            queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities[0].Columns.AddColumns(cub_Country.cub_alpha2Attribute, cub_Country.cub_alpha3Attribute, cub_Country.cub_countrynameAttribute, cub_Country.cub_postalcodeAttribute);
            queryExpression.LinkEntities[0].LinkEntities[0].LinkEntities[0].EntityAlias = "country";
        }

        /// <summary>
        /// LinkAccountTypeToAccount
        /// </summary>
        /// <param name="queryExpression">queryExpression</param>
        private static void LinkAccountTypeToAccount(QueryExpression queryExpression)
        {
            queryExpression.LinkEntities.Add(new LinkEntity(Account.EntityLogicalName, cub_AccountType.EntityLogicalName,
                                                            Account.cub_accounttypeidAttribute, cub_AccountType.cub_accounttypeidAttribute,
                                                            JoinOperator.Inner));
            queryExpression.LinkEntities[1].Columns.AddColumns(cub_AccountType.cub_accountnameAttribute);
            queryExpression.LinkEntities[1].EntityAlias = "accountType";
        }

        /// <summary>
        /// LinkContactToAccount
        /// </summary>
        /// <param name="firstname">firstname</param>
        /// <param name="lastname">lastname</param>
        /// <param name="email">email</param>
        /// <param name="phone">phone</param>
        /// <param name="contactJoin">contactJoin</param>
        /// <param name="queryExpression">queryExpression</param>
        private static void LinkContactToAccount(string firstname, string lastname, string email, string phone, JoinOperator contactJoin, QueryExpression queryExpression)
        {
            queryExpression.LinkEntities.Add(new LinkEntity(Account.EntityLogicalName, Contact.EntityLogicalName, Account.accountidAttribute,
                                                            Contact.parentcustomeridAttribute, contactJoin));
            queryExpression.LinkEntities[0].Columns.AddColumns(Contact.contactidAttribute,
                                                      Contact.cub_contacttypeidAttribute,
                                                      Contact.fullnameAttribute,
                                                      Contact.firstnameAttribute,
                                                      Contact.lastnameAttribute,
                                                      Contact.emailaddress1Attribute,
                                                      Contact.telephone1Attribute,
                                                      Contact.telephone2Attribute,
                                                      Contact.telephone3Attribute,
                                                      Contact.cub_phone1typeAttribute,
                                                      Contact.cub_phone2typeAttribute,
                                                      Contact.cub_phone3typeAttribute,
                                                      Contact.birthdateAttribute,
                                                      Contact.cub_personalidentifierAttribute,
                                                      Contact.cub_personalidentifiertypeAttribute,
                                                      Contact.middlenameAttribute,
                                                      Contact.cub_suffixAttribute,
                                                      Contact.cub_nametitleAttribute,
                                                      Contact.statuscodeAttribute);
            FilterExpression contactFilter = new FilterExpression(LogicalOperator.And);
            FilterExpression phoneFilter = new FilterExpression(LogicalOperator.Or);
            if (!string.IsNullOrEmpty(firstname))
            {
                if (firstname.Substring(firstname.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                    contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.BeginsWith, firstname.Remove(firstname.Length - 1));
                else
                    contactFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.Equal, firstname);
            }
            if (!string.IsNullOrEmpty(lastname))
            {
                if (lastname.Substring(lastname.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                    contactFilter.AddCondition(Contact.lastnameAttribute, ConditionOperator.BeginsWith, lastname.Remove(lastname.Length - 1));
                else
                    contactFilter.AddCondition(Contact.lastnameAttribute, ConditionOperator.Equal, lastname);
            }
            if (!string.IsNullOrEmpty(email))
            {
                if (email.Substring(email.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                    contactFilter.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.BeginsWith, email.Remove(email.Length - 1));
                else
                    contactFilter.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.Equal, email);
            }
            if (!string.IsNullOrEmpty(phone))
            {
                if (phone.Substring(phone.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                {
                    phone = phone.Remove(phone.Length - 1);

                    if (!string.IsNullOrEmpty(phone))
                    {
                        phoneFilter.AddCondition(Contact.telephone1Attribute, ConditionOperator.BeginsWith, phone.Trim());
                        phoneFilter.AddCondition(Contact.telephone2Attribute, ConditionOperator.BeginsWith, phone.Trim());
                        phoneFilter.AddCondition(Contact.telephone3Attribute, ConditionOperator.BeginsWith, phone.Trim());
                    }
                }
                else
                {
                    phoneFilter.AddCondition(Contact.telephone1Attribute, ConditionOperator.Equal, phone.Trim());
                    phoneFilter.AddCondition(Contact.telephone2Attribute, ConditionOperator.Equal, phone.Trim());
                    phoneFilter.AddCondition(Contact.telephone3Attribute, ConditionOperator.Equal, phone.Trim());
                }
            }
            if (contactFilter.Conditions.Count > 0) queryExpression.LinkEntities[0].LinkCriteria = contactFilter;
            if (phoneFilter.Conditions.Count > 0) queryExpression.LinkEntities[0].LinkCriteria.AddFilter(phoneFilter);
            queryExpression.LinkEntities[0].EntityAlias = "contact";
        }

        /// <summary>
        /// CreateQueryExpression
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <returns></returns>
        private static QueryExpression CreateQueryExpression(Guid customerId)
        {
            var queryExpression = new QueryExpression(Account.EntityLogicalName);
            queryExpression.NoLock = true;
            queryExpression.Distinct = true;
            queryExpression.ColumnSet = new ColumnSet(Account.accountidAttribute,
                                                    Account.nameAttribute,
                                                    Account.statuscodeAttribute,
                                                    Account.cub_accounttypeidAttribute);
            FilterExpression accountFilter = new FilterExpression(LogicalOperator.And);
            accountFilter.AddCondition(Account.accountidAttribute, ConditionOperator.Equal, customerId);
            queryExpression.Criteria = accountFilter;
            return queryExpression;
        }

        /// <summary>
        /// PrepareSortNy
        /// </summary>
        /// <param name="firstname">firstname</param>
        /// <param name="lastname">lastname</param>
        /// <param name="email">email</param>
        /// <param name="phone">phone</param>
        /// <param name="postalCode">postalCode</param>
        /// <param name="personalIdentifierType">personalIdentifierType</param>
        /// <param name="personalIdentifier">personalIdentifier</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="contactJoin">contactJoin</param>
        /// <param name="addyJoin">addyJoin</param>
        /// <param name="searchSort">searchSort</param>
        /// <returns>SortBy</returns>
        private static string PrepareSortBy(string firstname, string lastname, string email, string phone, string postalCode, string personalIdentifierType, string personalIdentifier, string sortBy, out JoinOperator contactJoin, out JoinOperator addyJoin, Dictionary<string, OrderType> searchSort)
        {
            if (string.IsNullOrEmpty(firstname) && string.IsNullOrEmpty(lastname) && string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone)
                && string.IsNullOrEmpty(personalIdentifierType) && string.IsNullOrEmpty(personalIdentifier))
                contactJoin = JoinOperator.LeftOuter;
            else
                contactJoin = JoinOperator.Inner;

            if (string.IsNullOrEmpty(postalCode))
                addyJoin = JoinOperator.LeftOuter;
            else
                addyJoin = JoinOperator.Inner;

            if (!string.IsNullOrEmpty(sortBy))
            {
                sortBy = sortBy.Trim();

                string[] sortCriteria = sortBy.Split(',');
                foreach (string s in sortCriteria)
                {
                    string[] foundSort = s.Split('.');
                    if (foundSort.Length != 2 && (!foundSort[1].Equals("asc", StringComparison.OrdinalIgnoreCase) || !foundSort[1].Equals("desc", StringComparison.OrdinalIgnoreCase)))
                    {
                        throw new FaultException<WSMSDFault>(new WSMSDFault("Invalid sort expression, expression should be <field_name>.<sort_order(asc/desc)>", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND));
                    }
                    searchSort.Add(foundSort[0].ToLower(), foundSort[1].Equals("asc", StringComparison.OrdinalIgnoreCase) ? OrderType.Ascending : OrderType.Descending);
                }
            }

            return sortBy;
        }

        /// <summary>
        /// Build Phone List
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <returns>Phone List</returns>
        private Model.NIS.WSPhoneExt[] BuildPhoneList(Entity targetEntity)
        {
            string p1 = ReturnStringValue(GetAttributeValue(targetEntity, "contact." + Contact.telephone1Attribute));
            string p2 = ReturnStringValue(GetAttributeValue(targetEntity, "contact." + Contact.telephone2Attribute));
            string p3 = ReturnStringValue(GetAttributeValue(targetEntity, "contact." + Contact.telephone3Attribute));

            int count = 0;
            if (!string.IsNullOrEmpty(p1))
            {
                if (!string.IsNullOrEmpty(p2))
                {
                    if (!string.IsNullOrEmpty(p3))
                        count = 3;
                    else
                        count = 2;
                }
                else
                {
                    if (!string.IsNullOrEmpty(p3))
                        count = 2;
                    else
                        count = 1;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(p2))
                {
                    if (!string.IsNullOrEmpty(p3))
                        count = 2;
                    else
                        count = 1;
                }
                else
                {
                    if (!string.IsNullOrEmpty(p3))
                        count = 2;
                    else
                        count = 1;
                }
            }

            Model.NIS.WSPhoneExt[] phoneList = new Model.NIS.WSPhoneExt[count];
            Model.NIS.WSPhoneExt ph1 = new Model.NIS.WSPhoneExt();
            Model.NIS.WSPhoneExt ph2 = new Model.NIS.WSPhoneExt();
            Model.NIS.WSPhoneExt ph3 = new Model.NIS.WSPhoneExt();

            if (!string.IsNullOrEmpty(p1))
            {
                ph1.number = p1;
                ph1.type = null;
                ph1.type = GetAttributeOptionSetText(targetEntity, "contact." + Contact.cub_phone1typeAttribute, typeof(cub_phonetype), true);
            };

            if (!string.IsNullOrEmpty(p2))
            {
                ph2.number = p2;
                ph2.type = null;
                ph2.type = GetAttributeOptionSetText(targetEntity, "contact." + Contact.cub_phone1typeAttribute, typeof(cub_phonetype), true);
            };

            if (!string.IsNullOrEmpty(p3))
            {
                ph3.number = p3;
                ph3.type = null;
                ph3.type = GetAttributeOptionSetText(targetEntity, "contact." + Contact.cub_phone1typeAttribute, typeof(cub_phonetype), true);
            };

            if (count == 1)
            {
                if (!string.IsNullOrEmpty(p1))
                    phoneList[count - 1] = ph1;
                else
                {
                    if (!string.IsNullOrEmpty(p2))
                        phoneList[count - 1] = ph2;
                    else
                        phoneList[count - 1] = ph3;
                }
            }
            else
            {
                if (count == 2)
                {
                    if (!string.IsNullOrEmpty(p1))
                    {
                        if (!string.IsNullOrEmpty(p2))
                        {
                            phoneList[count - 2] = ph1;
                            phoneList[count - 1] = ph2;
                        }
                        else
                        {
                            phoneList[count - 2] = ph1;
                            phoneList[count - 1] = ph3;
                        }
                    }
                    else
                    {
                        phoneList[count - 2] = ph2;
                        phoneList[count - 1] = ph3;
                    }
                }
                else
                {
                    phoneList[count - 3] = ph1;
                    phoneList[count - 2] = ph2;
                    phoneList[count - 1] = ph3;
                }
            }
            return phoneList;
        }

        /// <summary>
        /// Build Security Quest/Asnwers
        /// </summary>
        /// <param name="accountId">Account ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <returns>WSSecurityQA</returns>
        private Model.NIS.WSSecurityQA[] BuildSecurityQAs(string accountId, string contactId)
        {
            Model.NIS.WSSecurityQA[] secusityQAList = null;

            Guid filterGuid;
            if (Guid.TryParse(contactId, out filterGuid))
            {

                secusityQAList = (from sq in CubOrgSvc.cub_SecurityAnswerSet
                                  where sq.cub_ContactId.Id == filterGuid
                                  select
                                      new Model.NIS.WSSecurityQA
                                      {
                                          securityQuestion = sq.cub_SecurityQuestionId.Name,
                                          securityAnswer = sq.cub_SecurityAnswerDesc
                                      }).ToArray();

            }

            return secusityQAList;
        }

        /// <summary>
        /// Return a JSON object with all addresses of the given One Account ID prepared for a dropdown list
        /// ID + (Adress 2 + Address 1 + City + State + Zipcode)
        /// </summary>
        /// <param name="oneAccountId"></param>
        /// <returns>JSON object with address for a dropdownlist</returns>
        public string GetAddressForDropdownList(Guid accountID)
        {
            var addresses = 
            (from a in CubOrgSvc.cub_AddressSet
             join c in CubOrgSvc.AccountSet on a.cub_AccountAddressId.Id equals c.AccountId
             where c.AccountId.Equals(accountID) && a.statecode.Equals(cub_AddressState.Active)
             select new
             {
                 id = a.cub_AddressId,
                 text = $"{a.cub_Street2} {a.cub_Street1}, {a.cub_City}, {a.cub_StateProvinceId.Name} {a.cub_PostalCodeId.Name}" 
             }).ToList();
            return JsonConvert.SerializeObject(addresses);
        }

        public string GetAccountOneAccountId(string accountID)
        {
            var oneaccountId =
            (from a in CubOrgSvc.AccountSet
             where a.AccountId.Equals(accountID) 
             select new
             {
                 id = a.cub_OneAccountId
             }).FirstOrDefault();
            return oneaccountId.id.ToString();
        }

        /// <summary>
        /// Get contacts of a Customer for the Void Trip component
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <returns>A list of contacts as a JSON object: Fields: ID, Name, Email, Phone</returns>
        public string getContactsForVoidTrip(Guid customerId)
        {
            ContactLogic contactLogic = new ContactLogic(this._organizationService);
            var contacts =
            (from c in CubOrgSvc.ContactSet
             where c.AccountId.Id.Equals(customerId) && c.StateCode.Equals(ContactState.Active)
             select new
             {
                 ID = c.ContactId,
                 Name = c.FirstName + " " + c.LastName,
                 Email = c.EMailAddress1,
                 Phone = contactLogic.GetSMSFormattedPhone(c.Telephone1, c.cub_Phone1Type) ?? 
                         contactLogic.GetSMSFormattedPhone(c.Telephone2, c.cub_Phone2Type) ??
                         contactLogic.GetSMSFormattedPhone(c.Telephone3, c.cub_Phone3Type)
             }).ToList();
            return JsonConvert.SerializeObject(contacts);
        }


    }
}
