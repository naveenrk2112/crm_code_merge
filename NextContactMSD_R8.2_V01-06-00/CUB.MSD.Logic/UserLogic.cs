/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace CUB.MSD.Logic
{

    /// <summary>
    /// User Info
    /// </summary>
    public class UserInfo
    {
        public string UserId;
        public string UserName;
        public string AppVersion;
        public string ClientName;
        public string SystemName;
        public string Roles;
        public string DomainName;
        public string BusinessUnit;
        public string RecordID;
        public string Currency;
        public string ServiceVersion;
        public string FirstName;
        public string LastName;
    }

    /// <summary>
    /// User Logic
    /// </summary>
    public class UserLogic : LogicBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public UserLogic(IOrganizationService service) : base(service)
        {
            _organizationService = service;
        }

        /// <summary>
        /// Get User Info
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>User Info</returns>
        public UserInfo GetUserInfo(string userId)
        {
            UserInfo ui = new UserInfo();
            ColumnSet cols = new ColumnSet(SystemUser.fullnameAttribute, SystemUser.businessunitidAttribute, SystemUser.domainnameAttribute, SystemUser.transactioncurrencyidAttribute);

            Entity su = _organizationService.Retrieve(SystemUser.EntityLogicalName, new Guid(userId), cols);
            ui.UserName = ((SystemUser)su).FullName;
            ui.BusinessUnit = ((SystemUser)su).BusinessUnitId.Name;
            ui.DomainName = ((SystemUser)su).DomainName;
            ui.Currency = ((SystemUser)su).TransactionCurrencyId.Name;

            var roles = (from r in CubOrgSvc.RoleSet
                         join s in CubOrgSvc.SystemUserRolesSet on r.RoleId equals s.RoleId
                         where s.SystemUserId.Value.Equals(new Guid(userId))
                         select new Role() { Name = r.Name }).ToList();

            foreach (Role userRole in roles)
            {
                if (ui.Roles == null)
                    ui.Roles = userRole.Name;
                else
                    ui.Roles += ", " + userRole.Name;
            }
            try
            {
                ui.AppVersion = GlobalsCache.AppInfoAppVersion(_organizationService);
                ui.ClientName = GlobalsCache.AppInfoClientName(_organizationService);
            }
            catch (Exception ex)
            {
                ui.AppVersion = "ERROR: " + ex.Message;
            }

            return ui;

        }

        /// <summary>
        /// GetCrmUserId
        /// </summary>
        /// <param name="userName">User name</param>
        /// <returns>User ID</returns>
        public Guid GetCrmUserId(string userName)
        {
            var userId = (from u in CubOrgSvc.SystemUserSet
                          where u.DomainName == userName
                          select u.SystemUserId).SingleOrDefault();
            if (userId.HasValue)
            {
                return userId.Value;
            }
            else
            {
                throw new ApplicationException("User not found in CRM");
            }
        }

        /// <summary>
        /// Return the User ID, First and Last Name
        /// </summary>
        /// <param name="domainUserName"></param>
        /// <returns>User Info</returns>
        public UserInfo GetCrmUserIdFirstLastName(string domainUserName)
        {
            var userInfo = (from u 
                            in CubOrgSvc.SystemUserSet
                            where u.DomainName == domainUserName
                            select new UserInfo {
                                UserId = u.SystemUserId.Value.ToString(),
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                                UserName = domainUserName
                            }).SingleOrDefault();
            if (userInfo != null)
            {
                return userInfo;
            }
            else
            {
                throw new ApplicationException("User not found in CRM");
            }
        }

        /// <summary>
        /// Get Recently Verified Contact Ids
        /// </summary>
        /// <returns>Recently Verified Contact Ids</returns>
        public IEnumerable<string> GetRecentlyVerifiedContactIds(Guid userId)
        {
            var logic = new GlobalsLogic(_organizationService);
            var offset = (string)GlobalsCache.GetFromCache("RecentVerificationsOffsetInMinutes");
            if (offset == null)
            {
                offset = logic.GetAttributeValue("CUB.MSD.Web.Angular", "RecentVerificationsOffsetInMinutes");
                GlobalsCache.AddToCache("RecentVerificationsOffsetInMinutes", offset);
            }
            var subjectVerified = (string)GlobalsCache.GetFromCache("SubjectVerified");
            if (subjectVerified == null)
            {
                subjectVerified = logic.GetAttributeValue("CUB.MSD.Web.Angular", "SubjectVerified");
                GlobalsCache.AddToCache("SubjectVerified", subjectVerified);
            }

            var cutOffTime = DateTime.Now.ToUniversalTime().AddMinutes(-double.Parse(offset));

            return (from v in CubOrgSvc.cub_VerificationSet
                    join n in CubOrgSvc.AnnotationSet on v.ActivityId equals n.ObjectId.Id
                    where v.ModifiedBy.Id == userId
                    where v.ModifiedOn > cutOffTime
                    orderby v.ModifiedOn descending
                    select new
                    {
                        LogicalName = v.RegardingObjectId.LogicalName,
                        ContactId = v.RegardingObjectId.Id,
                        Subject = n.Subject.ToLower()
                    })
                .ToList() // Necessary to filter by regarding object logical name
                .Where(v => v.LogicalName == "contact")
                .GroupBy(v => v.ContactId)
                .Select(g => g.First())
                .Where(v => v.Subject.Equals(subjectVerified, StringComparison.InvariantCultureIgnoreCase))
                .Select(v => v.ContactId.ToString())
                .ToList();
        }
    }
}
