/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json.Linq;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// NIS API Logic
    /// </summary>
    public class NISApiLogic : RestApiBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public NISApiLogic(IOrganizationService service) : base(service)
        {
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <param name="executionContext">MSD plugin execution contect</param>
        public NISApiLogic(IOrganizationService service, IPluginExecutionContext executionContext) : base(service, executionContext)
        {
        }

        /// <summary>
        /// Format a GUID as a string resembling "AAAABBBB-CCCC-DDDD-EEEE-FFFF11112222"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private string formatGuid(Guid id)
        {
            return id.ToString().Replace("{", "").Replace("}", "").ToUpper();
        }

        /// <summary>
        /// If id is a valid GUID, format it as a string, otherwise return it unmodified.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private string formatGuid(string id)
        {
            Guid guid;
            if (Guid.TryParse(id, out guid))
            {
                return formatGuid(guid);
            }
            else
            {
                return id;
            }
        }

        /// <summary>
        /// Complete Customer Registration
        /// </summary>
        /// <param name="accountId">Account ID</param>
        /// <returns>One Account ID</returns>
        public long CompleteCustomerRegistration(Guid accountId)
        {
            CubLogger.Debug("Entered");
            long returnValue = 0;

            WSCompleteCustomerRegistrationResponse response = new WSCompleteCustomerRegistrationResponse();
            WSCompleteCustomerRegistrationRequest request = new WSCompleteCustomerRegistrationRequest();

            request.contactId = formatGuid(accountId);

            #region TO DO: Comment this demo code

            //request.contactId = "10000";

            #endregion TO DO: Comment this demo code

            string requestUri = GlobalsCache.NISApiBaseURL(_organizationService) +
                                string.Format(GlobalsCache.NISCompleteRegistrationPath(_organizationService), request.contactId);

            CubLogger.DebugFormat("CompleteCustomerRegistration requestUri = {0}", requestUri);

            System.Net.Http.StringContent httpContent = new System.Net.Http.StringContent(string.Empty);

            RestResponse resp = HttpClientPost(requestUri, httpContent);

            if (!string.IsNullOrEmpty(resp.responseBody))
            {
                response = JsonConvert.DeserializeObject<WSCompleteCustomerRegistrationResponse>(resp.responseBody);
                returnValue = response.oneAccountId;
            }

            #region Sample calls using WebRequest and WebClient libraries
            //RestResponse resp = WebRequestPost(requestUri);
            //if (resp.webException.result == RestConstants.SUCCESSFUL)
            //{
            //    response = JsonConvert.DeserializeObject<WSCompleteCustomerRegistrationResponse>(resp.responseBody);
            //}
            //else 
            //{
            //    //TO DO: Do something with the error
            //}


            //RestResponse resp = WebClientPost(requestUri);
            //if if (resp.webException.result == RestConstants.SUCCESSFUL)
            //{
            //    response = JsonConvert.DeserializeObject<WSCompleteCustomerRegistrationResponse>(resp.responseBody);
            //}
            //else 
            //{
            //    //TO DO: Do something with the error
            //}
            #endregion Sample calls using WebRequest and WebClient libraries

            CubLogger.Debug("Exit");

            return returnValue;
        }

        /// <summary>
        /// Search Token
        /// </summary>
        /// <param name="subSystem">SubSystem</param>
        /// <param name="tokenType">Token Type</param>
        /// <param name="cardNumber">Card number</param>
        /// <param name="expDate">Expiration Date</param>
        /// <returns>Token data as JSON</returns>
        public string SearchToken(string subSystem, string tokenType, string cardNumber, string expDate)
        {

            if (string.Compare(tokenType, RestConstants.TokenTypes.bankCard, true) == 0)
                return SearchBankCard(subSystem, cardNumber, expDate);
            else
                return SearchSmartCard(subSystem, cardNumber, expDate);


        }

        /// <summary>
        /// Search bank card
        /// </summary>
        /// <param name="subSystem">subSystem</param>
        /// <param name="cardNumber">Card Number</param>
        /// <param name="expDate">Expiration Date</param>
        /// <returns>Bank card data as JSON</returns>
        private string SearchBankCard(string subSystem, string cardNumber, string expDate)
        {
            WSBankcardTravelTokenSearchRequest request = new WSBankcardTravelTokenSearchRequest();
            request.tokenType = RestConstants.TokenTypes.bankCard;
            request.pan = cardNumber;
            request.cardExpiryMMYY = expDate;

            string requestUri = GlobalsCache.NISApiBaseURL(_organizationService)
                + string.Format(GlobalsCache.NISSearchTokenPath(_organizationService), subSystem);



            string restBody = string.Format("{{\"travelToken\" : {{\"tokenType\" : \"{0}\",\"PAN\" : \"{1}\",\"cardExpiryMMYY\" :\"{2}\"}} }}", request.tokenType, request.pan, request.cardExpiryMMYY);

            System.Net.Http.StringContent httpContent = new System.Net.Http.StringContent(restBody, Encoding.UTF8, "application/json");

            RestResponse resp = HttpClientPost(requestUri, httpContent);

            if (resp.hdr.result == RestConstants.SUCCESSFUL)
            {
                return resp.responseBody;
            }
            else
            {
                return resp.hdr.errorMessage;
            }

        }

        public string SearchBankCardEntrypted(string subSystem, string encryptedToken, string nonce)
        {
            string requestUri = GlobalsCache.NISApiBaseURL(_organizationService) + 
                                string.Format(GlobalsCache.NISSearchTokenPath(_organizationService), subSystem);

            var fullEncryptedToken = new
            {
                nonce = nonce,
                payments = new
                {
                    creditCard = new
                    {
                        jweEnryptedCardData = encryptedToken
                    }
                }
            };

            CubLogger.InfoFormat("fullEncryptedToken: {0}", JsonConvert.SerializeObject(fullEncryptedToken));

            // Converting to Base64
            string fullEncryptedTokenAsString = JsonConvert.SerializeObject(fullEncryptedToken);
            byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(fullEncryptedTokenAsString);
            string fullEncryptedTokenAsBase64 = Convert.ToBase64String(toEncodeAsBytes);

            var requestBody = new
            {
                travelToken = new
                {
                    tokenType = "Bankcard",
                    encryptedToken = fullEncryptedTokenAsBase64,
                    keyName = GlobalsCache.AngularGetJweKeyName(_organizationService)
                }
            };

            CubLogger.InfoFormat("requestBody: {0}", JsonConvert.SerializeObject(requestBody));

            StringContent httpContent = new StringContent(JsonConvert.SerializeObject(requestBody), Encoding.UTF8, "application/json");
            RestResponse resp = HttpClientPost(requestUri, httpContent);
            if (resp.hdr.result == RestConstants.SUCCESSFUL)
            {
                CubLogger.InfoFormat("resp.responseBody: {0}", resp.responseBody);
                return resp.responseBody;
            }
            else
            {
                CubLogger.InfoFormat("errorMessage: {0}", resp.hdr.errorMessage);
                return resp.hdr.errorMessage;
            }
        }

        /// <summary>
        /// Search Smart Card
        /// </summary>
        /// <param name="subSystem">subSystem</param>
        /// <param name="cardNumber">cardNumber</param>
        /// <param name="expDate">Expiration Date</param>
        /// <returns></returns>
        private string SearchSmartCard(string subSystem, string cardNumber, string expDate)
        {
            WSSmartcardTravelTokenSearchRequest request = new WSSmartcardTravelTokenSearchRequest();
            request.tokenType = RestConstants.TokenTypes.smartCard;
            request.serialNumber = cardNumber;
            request.cardExpiryMMYY = expDate;
            return null;
        }

        /// <summary>
        /// Retrieve the status of an existing transit account associated to a subsystem
        /// </summary>
        /// <param name="accountID">Transit account ID</param>
        /// <param name="subSystem">Subssystem</param>
        /// <returns>Transit account status</returns>
        public WSTransitAccountSubsystemStatusResponse GetTransitAccountStatusByID(string accountID, string subSystem)
        {
            try
            {
                CubLogger.Debug("Entered");
                WSTransitAccountSubsystemStatusResponse wsresp = new WSTransitAccountSubsystemStatusResponse();
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetAccountStatusPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, accountID, subSystem);
                RestResponse resp = HttpClientGet(url);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    wsresp = resp.responseBody.DeserializeObject<WSTransitAccountSubsystemStatusResponse>();
                    wsresp.hdr = resp.hdr;
                    return wsresp;
                }
                else
                {
                    //TO DO: Do something with the error
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }
            finally
            {
                CubLogger.Debug("Exit");
            }
            return null;
        }

        /// <summary>
        /// Get Transit Account Balance History
        /// </summary>
        /// <param name="accountID">Account ID</param>
        /// <param name="subSystem">subSystem</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <returns>Transit Account Balance History as JSON</returns>
        public string GetTransitAccountBalanceHistory(string accountID, string subSystem,
                                                      string startDate, string endDate)
        {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetAccountBalanceHistoryPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, accountID, subSystem, startDate, endDate);
                RestResponse resp = HttpClientGet(url);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    return resp.responseBody;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
          
            return null;
        }

        /// <summary>
        /// Get the balance history for Transit Account. This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// NIS API call: 2.15.2 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/balancehistory GET
        /// </summary>
        /// <param name="accountId">(Required) Unique identifier for the Transit Account. </param>
        /// <param name="subSystem">(Required)  Unique identifier for the SubSystem</param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the transactions are required for, defaults to 7 days ago.  Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.  Required when startDateTime is given.</param>
        /// <param name="entryType">(Optional) The unique identifier of the entry type. </param>
        /// <param name="entrySubType">(Optional)  The unique identifier of the entry sub type (for charge it is the charge type, for loads it is the load type). </param>
        /// <param name="purseType">(Optional) Type of purse it is.</param>
        /// <param name="reversal">(Optional) Flag showing if the transaction is a reversal.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10.</param>
        /// <returns>One Account Travel History as JSON</returns>
        public MvcGenericResponse GetTransitAccountBalanceHistory(string accountId, string subSystemId, string startDateTime, string endDateTime,
                                                                 string entryType, string entrySubType, string purseType, bool? reversal,
                                                                 string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();

            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);

                string actionURL = GlobalsCache.NISGetAccountBalanceHistoryPath(_organizationService).ToString();
                actionURL = string.Format(actionURL, accountId, subSystemId);

                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (!string.IsNullOrWhiteSpace(startDateTime)) url.Append(string.Format("&startDateTime={0}", startDateTime));
                if (!string.IsNullOrWhiteSpace(endDateTime)) url.Append(string.Format("&endDateTime={0}", endDateTime));
                if (!string.IsNullOrWhiteSpace(entryType)) url.Append(prepareListParamForNIS("entryType", entryType));
                if (!string.IsNullOrWhiteSpace(entrySubType)) url.Append(prepareListParamForNIS("entrySubType", entrySubType));
                if (!string.IsNullOrWhiteSpace(purseType)) url.Append(prepareListParamForNIS("purseType", purseType));
                if (reversal.HasValue) url.Append(string.Format("&reversal={0}", reversal));
                if (!string.IsNullOrWhiteSpace(sortBy)) url.Append(string.Format("&sortBy={0}", sortBy));
                if (offset.HasValue) url.Append(string.Format("&offset={0}", offset));
                if (limit.HasValue) url.Append(string.Format("&limit={0}", limit));

                CubLogger.Info("GetTransitAccountBalanceHistory: url=" + url);

                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info("GetTransitBalanceHistory: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }

            return wsresp;
        }

        /// <summary>
        /// Get the travel history for Transit Account. This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// NIS API call: 2.12.3 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/travelhistory GET
        /// </summary>
        /// <param name="accountId">(Required) Unique identifier for the Transit Account. </param>
        /// <param name="subSystem">(Required)  Unique identifier for the SubSystem</param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the transactions are required for, defaults to 7 days ago.  Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.  Required when startDateTime is given.</param>
        /// <param name="travelMode">(Optional) Comma separated list of the travel mode IDs to return the history of, defaults to all travel modes. See section 2.6.9 for allowable values.</param>
        /// <param name="tripCategory">(Optional)  Comma separated list of the trip category enums to return the history of, defaults to all categories.
        ///                            Allowable values:
        ///                            - All
        ///                            - Correctable
        ///                            - Finalized
        ///                            - Incomplete
        ///                            - Auto-Filled
        ///                            - Manual-Filled
        ///                            - Complete
        ///    </param>
        /// <param name="viewType">(Optional) View type enum to return the history of, defaults to Simple type.</param>
        /// <param name="tripStatus">(Optional) The unique identifier of the trip status.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10.</param>
        /// <returns>One Account Travel History as JSON</returns>
        public MvcGenericResponse GetTransitAccountTravelHistory(string accountId, string subSystemId, string startDateTime, string endDateTime,
                                                                 string travelMode, string tripCategory, string viewType, string tripStatus,
                                                                 string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();

            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);

                string actionURL = GlobalsCache.NISGetAccountTravelHistoryPath(_organizationService).ToString();
                       actionURL = string.Format(actionURL, accountId, subSystemId);

                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (!string.IsNullOrWhiteSpace(startDateTime)) url.Append(string.Format("&startDateTime={0}", startDateTime));
                if (!string.IsNullOrWhiteSpace(endDateTime)) url.Append(string.Format("&endDateTime={0}", endDateTime));
                if (!string.IsNullOrWhiteSpace(viewType)) url.Append(string.Format("&viewType={0}", viewType));
                if (!string.IsNullOrWhiteSpace(travelMode)) url.Append(prepareListParamForNIS("travelMode", travelMode));
                if (!string.IsNullOrWhiteSpace(tripCategory)) url.Append(prepareListParamForNIS("tripCategory", tripCategory));
                if (!string.IsNullOrWhiteSpace(tripStatus)) url.Append(prepareListParamForNIS("tripStatus", tripStatus));
                if (!string.IsNullOrWhiteSpace(sortBy)) url.Append(string.Format("&sortBy={0}", sortBy));
                if (offset.HasValue) url.Append(string.Format("&offset={0}", offset));
                if (limit.HasValue) url.Append(string.Format("&limit={0}", limit));

                CubLogger.Info("GetTransitAccountTravelHistory: url=" + url);

                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info("GetTransitAccountTravelHistory: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }

            return wsresp;
        }

        /// <summary>
        /// 2.14.5 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/travelhistory/taps GET
        /// The API to get the tap history for a transitaccount. This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// </summary>
        /// <param name="accountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystemId">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="startDateTime">(Conditionally-Required)  Starting date and time for the period the transactions are required for, defaults to 7 days ago.
        ///                             Required when endDateTime is given. Time when tap occurred.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.
        ///                           Required when startDateTime is given. Time when tap occurred.</param>
        /// <param name="travelMode">(Optional) Comma separated list of the travel mode IDs to return the history of, defaults to all travel modes. See section 2.6.9 for allowable values.</param>
        /// <param name="status">(Optional) Comma separated list of the status IDs to return the travel history of, defaults to all statuses.</param>
        /// <param name="eventList">(Optional) Comma separated list of the event IDs to return the travel history of, defaults to all events.</param>
        /// <param name="operatorDesc">(Required) Unique identifier of the operator.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: 
        ///                      �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime. </param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10.</param>
        /// <returns>Travel History Taps as JSON</returns>

        public TransitAccountTravelHistoryTapResponse GetTransitAccountTravelHistoryTaps(string accountId, string subSystem, string startDateTime, string endDateTime,
                                                                                      string travelMode, string status, string eventList, string operatorDesc,
                                                                                      string sortBy, int? offset, int? limit)
        {
            TransitAccountTravelHistoryTapResponse wsresp = new TransitAccountTravelHistoryTapResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetAccountTravelHistoryTapsPath(_organizationService).ToString();
                actionURL = string.Format(actionURL, accountId, subSystem);
                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (!string.IsNullOrWhiteSpace(startDateTime)) url.Append(string.Format("&startDateTime={0}", startDateTime));
                if (!string.IsNullOrWhiteSpace(endDateTime)) url.Append(string.Format("&endDateTime={0}", endDateTime));
                if (!string.IsNullOrWhiteSpace(travelMode)) url.Append(prepareListParamForNIS("travelMode", travelMode));
                if (!string.IsNullOrWhiteSpace(status)) url.Append(prepareListParamForNIS("status", status));
                if (!string.IsNullOrWhiteSpace(eventList)) url.Append(prepareListParamForNIS("event", eventList));
                if (!string.IsNullOrWhiteSpace(operatorDesc)) url.Append(prepareListParamForNIS("operator", operatorDesc));
                if (!string.IsNullOrWhiteSpace(sortBy)) url.Append(string.Format("&sortBy={0}", sortBy));
                if (offset.HasValue) url.Append(string.Format("&offset={0}", offset));
                if (limit.HasValue) url.Append(string.Format("&limit={0}", limit));
                CubLogger.Info("GetTransitAccountTravelHistoryTaps: url=" + url);
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info("GetTransitAccountTravelHistoryTaps: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                    {
                        wsresp.Body = resp.responseBody;
                    }
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// Call te NIS API Method: BASE_URL/order/debtcollect POST
        /// This API is used to submit(create) a debt collection order for a given registered or unregistered customer.
        /// This type of order supports requests for subsystem bankcard token debt recovery and, in the future, it might support OneAccount debt recovery
        /// </summary>
        /// <param name="customerId">Unique identifier for the customer (Required)</param>
        /// <param name="unregisteredEmail">email for unregistered customer (Optional)</param>
        /// <param name="clientRefId">The client's unique identifier specified as <device-id>:<unique-id> (Required)</param>
        /// <param name="collectionLineItems">Line items for the debt recovery requests (Required)</param>
        public MvcGenericResponse GetOrderDebtCollect(string customerId, string unregisteredEmail, string clientRefId, string subsystem, string subsystemAccountReference)
        {
           // var response = new WSOrderDebtCollectResponse();
            MvcGenericResponse wsresp = new MvcGenericResponse();

            try
            {
                var request = new WSOrderDebtCollectRequest();
                request.customerId = customerId;
                request.unregisteredEmail = unregisteredEmail;
                request.clientRefId = clientRefId;
                // future expand to passing array from controller - only 1 line item passed for now
                WSDebtCollectLineItemBankcard bankcard = new WSDebtCollectLineItemBankcard();
                bankcard.collection = new WSSubsystemDebtCollectionBankcard();
                bankcard.collection.collectionLineItemType = "SubsystemDebtCollectionBankcard";  // future may add more types
                bankcard.collection.subsystem = subsystem;
                bankcard.collection.subsystemAccountReference = subsystemAccountReference;
                request.collectionLineItems.Add(bankcard);

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string methodURL = GlobalsCache.NISOrderDebtCollectPath(_organizationService).ToString();
                string url = baseURL + methodURL;
                var httpContent = new StringContent(JsonConvert.SerializeObject(request));
                RestResponse resp = HttpClientPost(url, httpContent);

                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                //response.hdr.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// Update Dependencies
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="addressId">Adddress ID</param>
        /// <param name="address">Address</param>
        public void UpdateDependencies(Guid customerId, Guid addressId, Model.NIS.WSAddress address)
        {
            CubLogger.Debug("Method Entered");

            WSUpdateDependenciesRequest request = new WSUpdateDependenciesRequest();
            request.customerId = formatGuid(customerId);
            request.addressId = formatGuid(addressId);
            request.address = address;

            request.contactsUpdated = CubOrgSvc.ContactSet.Where(a => a.cub_AddressId.Equals(addressId)).Select(s=> formatGuid(s.Id)).ToList();

            string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
            string addressChangeURL = GlobalsCache.NISUpdateDependenciesPath(_organizationService).ToString();
            string url = baseURL + addressChangeURL;
            url = string.Format(url, formatGuid(customerId), formatGuid(addressId));
            string requestSerialized = request.SerializeObject();
            System.Net.Http.StringContent httpContent = new System.Net.Http.StringContent(requestSerialized);
            CubLogger.Info($"UpdateDependencies Request: url={url}   Body={requestSerialized}");
            RestResponse resp = HttpClientPost(url, httpContent);
            CubLogger.Info($"UpdateDependencies Response: {resp.SerializeObject()}");

            CubLogger.Debug("Method Exit");
        }

        public MvcGenericResponse GetTravelModesBySubsystem(string subsystem)
        {
            var response = new MvcGenericResponse();
            var baseUrl = GlobalsCache.NISApiBaseURL(_organizationService);
            var path = GlobalsCache.NISSubsystemTravelModesPath(_organizationService);
            var url = baseUrl + string.Format(path, subsystem);

            var nisResponse = HttpClientGet(url);

            if(nisResponse.hdr.result == RestConstants.SUCCESSFUL)
            {
                response.Header = nisResponse.hdr;
                response.Body = nisResponse.responseBody;
            }
            return response;
        }

        public object GetTravelOperatorsBySubsystem(string subsystem)
        {
            var response = new MvcGenericResponse();
            var baseUrl = GlobalsCache.NISApiBaseURL(_organizationService);
            var path = GlobalsCache.NISSubsystemTravelOperatorsPath(_organizationService);
            var url = baseUrl + string.Format(path, subsystem);

            var nisResponse = HttpClientGet(url);

            if (nisResponse.hdr.result == RestConstants.SUCCESSFUL)
            {
                response.Header = nisResponse.hdr;
                response.Body = nisResponse.responseBody;
            }
            return response;
        }

        public object GetTapTypesBySubsystem(string subsystem)
        {
            var response = new MvcGenericResponse();
            var baseUrl = GlobalsCache.NISApiBaseURL(_organizationService);
            var path = GlobalsCache.NISSubsystemTapTypesPath(_organizationService);
            var url = baseUrl + string.Format(path, subsystem);

            var nisResponse = HttpClientGet(url);

            if (nisResponse.hdr.result == RestConstants.SUCCESSFUL)
            {
                response.Header = nisResponse.hdr;
                response.Body = nisResponse.responseBody;
            }
            return response;
        }

        public object GetTapStatusesBySubsystem(string subsystem)
        {
            var response = new MvcGenericResponse();
            var baseUrl = GlobalsCache.NISApiBaseURL(_organizationService);
            var path = GlobalsCache.NISSubsystemTapStatusesPath(_organizationService);
            var url = baseUrl + string.Format(path, subsystem);

            var nisResponse = HttpClientGet(url);

            if (nisResponse.hdr.result == RestConstants.SUCCESSFUL)
            {
                response.Header = nisResponse.hdr;
                response.Body = nisResponse.responseBody;
            }
            return response;
        }

        /// <summary>
        /// Return the location list for the given subsystem
        /// </summary>
        /// <param name="subsystem">subsystem</param>
        /// <param name="serviceOperator">(Optional) Fare collection system assigned identifier or code for the operator of the travel service.
        /// It is subsystem configured value and not the global configuration.
        /// Case Insensitive
        ///</param>
        /// <returns>List of locations</returns>
        public MvcGenericResponse GetLocationsBySubsystem(string subsystem, string serviceOperator)
        {
            var response = new MvcGenericResponse();
            var baseUrl = GlobalsCache.NISApiBaseURL(_organizationService);
            var path = GlobalsCache.NISSubsystemLocationsPath(_organizationService);
            var url = baseUrl + string.Format(path, subsystem);
            if (!string.IsNullOrWhiteSpace(serviceOperator))
                url = url + "?operator=" + serviceOperator;

            var nisResponse = HttpClientGet(url);

            if (nisResponse.hdr.result == RestConstants.SUCCESSFUL)
            {
                response.Header = nisResponse.hdr;
                response.Body = nisResponse.responseBody;
            }
            return response;
        }


        /// <summary>
        /// This API is used to retrieve the order history for a subsystem transit account.
        /// </summary>
        /// <param name="transitAccountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystemId">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the orders are required for.
        ///                             Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required)  Ending date and time for the period the orders are required for.
        ///                           Required when startDateTime is given.</param>
        /// <param name="orderType">(Optional)  Filters by the order type.
        /// Valid types are:
        ///  - Sale
        ///  - Adjustment
        ///  - AutoloadEnrollment
        ///  - CloseAccount
        ///  - Autoload
        /// </param>
        /// <param name="orderStatus">(Optional)  Filters by the status of the order:
        /// Valid statuses are:
        ///	 - OrderAccepted
        ///	 - PaymentFailed
        ///  - FulfillmentFailed
        ///  - Completed
        ///  - CompletedWithErrors
        ///  - PaymentAccepted
        /// </param>
        /// <param name="paymentStatus">(Optional)  Filters by the payment status.
        /// Valid statuses are:
        ///  - Pending
        ///  - Authorized
        ///  - FailedAuthorization
        ///  - Confirmed
        ///  - FailedConfirmation
        ///  - PendingConfirmation
        ///  - ConfirmationExpired
        /// </param>
        /// <param name="orderId">(Optional) Filter by the order id entered.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: 
        /// �orderDateTime.asc,orderType.desc� indicates sort by orderDateTime ascending and orderType descending.
        /// Default is descending by orderDateTime.
        /// Supported sort fields: 
        ///  - orderDateTime
        ///  - orderType
        ///  - orderStatus
        ///  - paymentStatus
        ///  - orderId
        /// </param>
        /// <param name="offset">(Optional)  The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional)  The number of records per page, defaults to 10. Value should be between 1 and 9999, inclusive.</param>
        /// <returns>Order history</returns>
        public MvcGenericResponse GetTransitAccountOrderHistory(string transitAccountId, string subSystemId,
                                                                string startDateTime, string endDateTime,
                                                                string orderType, string orderStatus, string paymentStatus, 
                                                                int? orderId, bool? realtimeResults,
                                                                string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetAccountoOrderHistoryPath(_organizationService).ToString();
                actionURL = string.Format(actionURL, transitAccountId, subSystemId);
                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (!string.IsNullOrWhiteSpace(startDateTime)) url.Append(string.Format("&startDateTime={0}", startDateTime));
                if (!string.IsNullOrWhiteSpace(endDateTime)) url.Append(string.Format("&endDateTime={0}", endDateTime));
                if (!string.IsNullOrWhiteSpace(orderType)) url.Append(prepareListParamForNIS("orderType", orderType));
                if (!string.IsNullOrWhiteSpace(orderStatus)) url.Append(prepareListParamForNIS("orderStatus", orderStatus));
                if (!string.IsNullOrWhiteSpace(paymentStatus)) url.Append(string.Format("&paymentStatus={0}", paymentStatus));
                if (orderId.HasValue) url.Append(string.Format("&orderId={0}", orderId));
                if (realtimeResults.HasValue) url.Append(string.Format("&realtimeResults={0}", realtimeResults.ToString().ToLower()));
                if (!string.IsNullOrWhiteSpace(sortBy)) url.Append(string.Format("&sortBy={0}", sortBy));
                if (offset.HasValue) url.Append(string.Format("&offset={0}", offset));
                if (limit.HasValue) url.Append(string.Format("&limit={0}", limit));
                CubLogger.Info(url.ToString());
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info(resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        private string prepareListParamForNIS(string paramName, string data)
        {
            if (string.IsNullOrWhiteSpace(paramName) ||
                string.IsNullOrWhiteSpace(data))
                return null;
            string[] list = data.Split(',');
            if (list.Length == 0)
                return null;
            return $"&{paramName}=" + string.Join($"&{paramName}=",  list);
        }

        /// <summary>
        /// This API is used to request the the order history details for an order. This search provides an option to search in the realtime data source 
        /// by setting the optional filter realtimeResults to true. 
        /// This is to offer customer service the ability to troubleshoot orders that are not completely processed yet. 
        /// However, this option should be used in rare cases and some information will not be available in the search results.  
        /// </summary>
        /// <param name="transitAccountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystemId">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="orderid">(Required) Unique id for the order
        /// <param name="realtimeResults">(Optional)  Indicates if the results should be retrieved from a realtime source (i.e: OMS). By default this filter is set to false.
        public MvcGenericResponse GetTransitAccountOrderDetail(string transitAccountId, string subSystemId,
                                                                string orderid, bool? realtimeResults)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetAccountoOrderHistoryPath(_organizationService).ToString();
                actionURL = string.Format(actionURL, transitAccountId, subSystemId,orderid);
                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (realtimeResults.HasValue) url.Append(string.Format("realtimeResults={0}", realtimeResults.Value));
                
                CubLogger.Info(url.ToString());
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info(resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }


        /// <summary>
        /// call NIS to request a notification be emailed to the patron that their account has been locked due to too many failed login attempts
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        public void CustomerLockoutNotificationReport(Guid CustomerId, Guid ContactId)
        {
            try
            {
                CubLogger.Debug("Method CustomerLockoutNotificationReport Entered");

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string contactReportLockoutURL = GlobalsCache.NISApiNotificationLockoutReportPath(_organizationService).ToString();
                string url = string.Format(baseURL + contactReportLockoutURL, formatGuid(CustomerId), formatGuid(ContactId));

                // Serialize our class into a JSON String
                System.Net.Http.StringContent httpContent = new System.Net.Http.StringContent(string.Empty);
                RestResponse resp = HttpClientPost(url, httpContent);
                CubLogger.DebugFormat("{0} response {1}", url, resp.hdr.result);
            }
            finally
            {
                CubLogger.Debug("Method CustomerLockoutNotificationReport Exit");
            }
        }

        /// <summary>
        /// call NIS to request a token be emailed to the patron
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="verificationToken"></param>
        /// <param name="tokenExpiry"></param>
        /// <param name="tokenType"></param>
        public void VerificationTokenReport(Guid CustomerId, Guid ContactId, string verificationToken, DateTime tokenExpiry, cub_verificationtokentype tokenType)
        {
            try
            {
                CubLogger.Debug("Entered Verification Token Report");
                WSReportVerificationToken request = new WSReportVerificationToken();
                request.verificationToken = verificationToken;
                request.verificationType = tokenType.ToString();
                request.tokenExpiryDateTime = DateTime.SpecifyKind(tokenExpiry, DateTimeKind.Utc);

                string customerId = formatGuid(CustomerId);
                string contactId = formatGuid(ContactId);

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string contactReportTokenURL = GlobalsCache.NISApiVerificationTokenReportPath(_organizationService).ToString();
                string url = baseURL + contactReportTokenURL;
                url = string.Format(url, customerId, contactId);

                // Serialize our class into a JSON String
                var stringTokenData = JsonConvert.SerializeObject(request);
                System.Net.Http.StringContent httpContent = new System.Net.Http.StringContent(stringTokenData, Encoding.UTF8, "application/json");
                CubLogger.DebugFormat("Before HttpClientPost. Content {0}", stringTokenData);
                RestResponse resp = HttpClientPost(url, httpContent);
                CubLogger.DebugFormat("{0} response {1}", url, resp.hdr.result);
            }
            finally
            {
                CubLogger.Debug("Exit Verification Token Report");
            }
        }

        /// <summary>
        /// Report Change
        /// </summary>
        /// <param name="cnt">Contact</param>
        public void ReportChange(Contact cnt)
        {
            try
            {
                CubLogger.Debug("Entered");
                WSReportChangeRequest request = new WSReportChangeRequest();
                request.contactEmail = cnt.EMailAddress1;
                request.contactSmsPhone = cnt.Telephone1;
                Model.NIS.WSName conName = new Model.NIS.WSName();
                conName.firstName = cnt.FirstName;
                conName.middleInitial = cnt.MiddleName;
                conName.lastName = cnt.LastName;
                //@TODO ****  conName.nameSuffixId = 
                request.contactName = conName;

                string customerId = formatGuid(cnt.ParentCustomerId.Id);
                string contactId = formatGuid(cnt.ContactId.Value);

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string contactChangeURL = GlobalsCache.NISReportChangePath(_organizationService).ToString();
                string url = baseURL + contactChangeURL;
                url = string.Format(url, customerId, contactId);

                // Serialize our concrete class into a JSON String
                var stringPayload = JsonConvert.SerializeObject(request);
                System.Net.Http.StringContent httpContent = new System.Net.Http.StringContent(stringPayload, Encoding.UTF8, "application/json");
                CubLogger.DebugFormat("Before HttpClientPost. Content {0}", stringPayload);
                RestResponse resp = HttpClientPost(url, httpContent);
                CubLogger.DebugFormat("{0} response {1}", url, resp.hdr.result);
            }
            finally
            {
                CubLogger.Debug("Exit");
            }
        }

        /// <summary>
        /// Send Username Reminder Notification
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="username">USername</param>
        public void SendUsernameReminderNotification(Guid customerId, Guid contactId, string username)
        {
            string methodName = string.Format("SendUsernameReminderNotification customer[{0}] - contact[{1}]", customerId, contactId);

            try
            {
                CubLogger.Debug("Entered" + methodName);
                WSSendUsernameReminder request = new WSSendUsernameReminder();
                request.username = username;

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string sendUsernameURL = GlobalsCache.NISSendForgottenUsernamePath(_organizationService).ToString();
                string url = baseURL + sendUsernameURL;
                url = string.Format(url, formatGuid(customerId), formatGuid(contactId));

                // Serialize our concrete class into a JSON String
                var body = JsonConvert.SerializeObject(request);
                System.Net.Http.StringContent httpContent = new System.Net.Http.StringContent(body, Encoding.UTF8, "application/json");
                CubLogger.DebugFormat("Before HttpClientPost. Content {0}", body);
                RestResponse resp = HttpClientPost(url, httpContent);
                CubLogger.DebugFormat("{0} response {1}", url, resp.hdr.result);
            }
            finally
            {
                CubLogger.Debug("Exit");
            }
        }

        /// <summary>
        /// Delete Address
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="addressId">Address ID</param>
        /// <returns></returns>
        public Model.MSDApi.WSAddressDeleteResponse DeleteAddress(Guid customerId, Guid addressId)
        {
            Model.MSDApi.WSAddressDeleteResponse addressDeleteResp = new Model.MSDApi.WSAddressDeleteResponse();

            string requestUri = GlobalsCache.NISApiBaseURL(_organizationService) +
                                string.Format(GlobalsCache.NISDeleteAddressPath(_organizationService), formatGuid(customerId), formatGuid(addressId));

            RestResponse resp = HttpClientDelete(requestUri);

            if (!string.IsNullOrEmpty(resp.responseBody))
                addressDeleteResp = JsonConvert.DeserializeObject<Model.MSDApi.WSAddressDeleteResponse>(resp.responseBody);

            addressDeleteResp.hdr = resp.hdr; //do this after the deserialize line above otherwise the deserialize will overwrite the hdr in addressDeleteResp

            return addressDeleteResp;
        }

        /// <summary>
        /// Update Address - NIS 2.8.13 Address PUT
        /// NOTE: Make sure the string fields country, state and zip of WSAddress inside WSAddressUpdateRequest are present. There is no validation
        /// done at this point.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <param name="requestBody"></param>
        /// <returns>header no body</returns>
        public WSWebResponseHeader UpdateAddress(string customerId, string addressId, WSAddressUpdateRequest requestBody)
        {
            WSWebResponseHeader addressUpdateResp = new WSWebResponseHeader();

            string requestUri = GlobalsCache.NISApiBaseURL(_organizationService) +
                                string.Format(GlobalsCache.NISUpdateAddressPath(_organizationService), formatGuid(customerId), formatGuid(addressId));

            System.Net.Http.StringContent httpContent = new System.Net.Http.StringContent(requestBody.SerializeObject());

            RestResponse resp = HttpClientPut(requestUri, httpContent);

            addressUpdateResp = resp.hdr;

            return addressUpdateResp;
        }


        /// <summary>
        /// Get Customer Notification
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="type">Type</param>
        /// <param name="status">Status</param>
        /// <param name="channel">Channel</param>
        /// <param name="recipient">recipient</param>
        /// <param name="notificationReference">notificationReference</param>
        /// <param name="sortBy">sortBy</param>
        /// <param name="offset">offset</param>
        /// <param name="limit">limit</param>
        /// <returns>Customer Notification Response</returns>
        public WSGetCustomerNotificationResponse GetCustomerNotification(string customerId, string contactId, string startDate, string endDate, string type, 
                                              string status, string channel, string recipient, string notificationReference, string sortBy, int? offset, int? limit)
        {
            CubLogger.Debug("Method Entered");
            WSGetCustomerNotificationResponse wsresp = new WSGetCustomerNotificationResponse();

            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetCustomerNotificationPath(_organizationService).ToString();
                string url = baseURL + actionURL;

                url = string.Format(url, formatGuid(customerId));

                var request = new
                {
                    contactId = contactId,
                    startDateTime = startDate,
                    endDateTime = endDate,
                    type = type,
                    status = status,
                    channel = channel,
                    recipient = recipient,
                    notificationReference = notificationReference,
                    sortBy = sortBy,
                    offset = offset,
                    limit = limit
                };

                string requestSerialized = JsonConvert.SerializeObject(request);
                var httpContent = new StringContent(requestSerialized);
                CubLogger.Info($"GetCustomerNotification: url={url}   Body={requestSerialized}");
                RestResponse resp = HttpClientPost(url, httpContent);
                CubLogger.Info("GetCustomerNotification: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = JsonConvert.DeserializeObject<WSGetCustomerNotificationResponse>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return wsresp;
        }

        /// <summary>
        /// Get Customer Notification Detail
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="notificationId">Notifcation ID</param>
        /// <returns>Customer Notification Detail</returns>
        public WSGetCustomerNotificationDetailResponse GetCustomerNotificationDetail(string customerId, string notificationId)
        {
            CubLogger.Debug("Method Entered");
            WSGetCustomerNotificationDetailResponse wsresp = new WSGetCustomerNotificationDetailResponse();

            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetCustomerNotificationDetailPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId), notificationId);
                RestResponse resp = HttpClientGet(url);
                if (!string.IsNullOrEmpty(resp.responseBody))
                    wsresp = JsonConvert.DeserializeObject<WSGetCustomerNotificationDetailResponse>(resp.responseBody);
                wsresp.hdr = resp.hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return wsresp;
        }

        /// <summary>
        /// Resend Customer Notification
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="notificationId">Notification ID</param>
        /// <param name="notificationType">Notification type</param>
        /// <param name="allowReformat">Allow format</param>
        /// <param name="channel">Channel ID</param>
        /// <param name="clientRefId">Client Reference ID</param>
        /// <returns> CubResponse<WSCustomerNotificationResendResponse></returns>
        public CubResponse<WSCustomerNotificationResendResponse> CustomerNotificationResend(string customerId, string contactId, string notificationId, string notificationType,
                                                 bool allowReformat, string channel, string clientRefId)
        {
            CubResponse<WSCustomerNotificationResendResponse> response = new CubResponse<WSCustomerNotificationResendResponse>();
            response.Errors = new List<WSMSDFault>();

            try
            {
                WSCustomerNotificationResendRequest request = new WSCustomerNotificationResendRequest();
                request.notificationId = notificationId;
                request.notificationType = notificationType;
                request.allowReformat = allowReformat;
                request.channel = channel;
                request.clientRefId = clientRefId;

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerNotificationResendPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId), formatGuid(contactId));

                string requestSerialized = request.SerializeObject();
                StringContent httpContent = new StringContent(requestSerialized);
                CubLogger.Debug($"CustomerNotificationResend: url={url}   Body={requestSerialized}");
                RestResponse httpResponse = HttpClientPost(url, httpContent);
                CubLogger.Debug($"CustomerNotificationResend: result={httpResponse.SerializeObject()}");
                if (httpResponse.hdr.result != RestConstants.SUCCESSFUL)
                {
                    response.Errors.Add(new WSMSDFault(httpResponse.hdr.fieldName, httpResponse.hdr.errorKey, httpResponse.hdr.errorMessage));
                }
                else
                {
                    response.ResponseObject.result = httpResponse.hdr.result;
                    response.ResponseObject.uid = httpResponse.hdr.uid;
                    return response;
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("CustomerNotificationResend", CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return response;
        }

        /// <summary>
        /// CustomerFundingSourcePostCCRef
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="pgCardId"></param>
        /// <param name="maskedPan"></param>
        /// <param name="cardExpiryMMYY"></param>
        /// <param name="nameOnCard"></param>
        /// <param name="creditCardType"></param>
        /// <param name="billingAddressId"></param>
        /// <param name="setAsPrimary"></param>
        /// <returns>WSCustomerFundingSourcePostResponse - fundingSourceId</returns>
        public CubResponse<WSCustomerFundingSourcePostResponse> CustomerFundingSourcePostCCRef(string customerId, string pgCardId, string maskedPan, string cardExpiryMMYY, string nameOnCard, string creditCardType,
                                                                                               string billingAddressId, bool? setAsPrimary)
        {
            CubResponse<WSCustomerFundingSourcePostResponse> response = new CubResponse<WSCustomerFundingSourcePostResponse>();
            response.Errors = new List<WSMSDFault>();
     
            try
            {
                WSFundingSourceRequest request = new WSFundingSourceRequest();
                request.fundingSource = new WSFundingSource();
                request.fundingSource.creditCard = new WSCreditCardReference();
                request.fundingSource.creditCard.pgCardId = pgCardId;
                ((WSCreditCardReference)request.fundingSource.creditCard).maskedPan = maskedPan;
                request.fundingSource.creditCard.cardExpiryMMYY = cardExpiryMMYY;
                request.fundingSource.creditCard.nameOnCard = nameOnCard;
                request.fundingSource.creditCard.creditCardType = creditCardType;
                request.fundingSource.setAsPrimary = setAsPrimary.HasValue ? setAsPrimary.Value : false;
                request.fundingSource.billingAddressId = formatGuid(billingAddressId);
               
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerFundingSourcePath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId));
                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(request));

                RestResponse httpResponse = HttpClientPost(url, httpContent);
                if (httpResponse.hdr.result != RestConstants.SUCCESSFUL)
                {
                    response.Errors.Add(new WSMSDFault(httpResponse.hdr.fieldName, httpResponse.hdr.errorKey, httpResponse.hdr.errorMessage));
                }
                else
                {
                    response.ResponseObject = JsonConvert.DeserializeObject<WSCustomerFundingSourcePostResponse>(httpResponse.responseBody);
                    return response;
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("CustomerFundingSourcePost", CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return response;
        }

        /// <summary>
        /// CustomerFundingSourcePostCCClear
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="pan"></param>
        /// <param name="cardExpiryMMYY"></param>
        /// <param name="nameOnCard"></param>
        /// <param name="postalcode"></param>
        /// <param name="address1"></param>
        /// <param name="country"></param>
        /// <param name="billingAddressId"></param>
        /// <param name="setAsPrimary"></param>
        /// <returns>WSCustomerFundingSourcePostResponse</returns>
        public CubResponse<WSCustomerFundingSourcePostResponse> CustomerFundingSourcePostCCClear(string customerId, string cardType, string pan, string cardExpiryMMYY, string nameOnCard, string postalcode, string address1, string country,
                                                                                                 string billingAddressId, bool? setAsPrimary)
        {
            CubResponse<WSCustomerFundingSourcePostResponse> response = new CubResponse<WSCustomerFundingSourcePostResponse>();
            response.Errors = new List<WSMSDFault>();

            try
            {
                WSFundingSourceRequest request = new WSFundingSourceRequest();
                request.fundingSource = new WSFundingSource();
                request.fundingSource.creditCard = new WSCreditCardClear();
                ((WSCreditCardClear)request.fundingSource.creditCard).PAN = pan;
                request.fundingSource.creditCard.creditCardType = cardType;
                request.fundingSource.creditCard.cardExpiryMMYY = cardExpiryMMYY;
                request.fundingSource.creditCard.nameOnCard = nameOnCard;
                ((WSCreditCardClear)request.fundingSource.creditCard).avsData = new WSAvsData();
                ((WSCreditCardClear)request.fundingSource.creditCard).avsData.postalCode = postalcode;
                ((WSCreditCardClear)request.fundingSource.creditCard).avsData.address1 = address1;
                ((WSCreditCardClear)request.fundingSource.creditCard).avsData.country = country;
                request.fundingSource.setAsPrimary = setAsPrimary.HasValue ? setAsPrimary.Value:false;
                request.fundingSource.billingAddressId = formatGuid(billingAddressId);

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerFundingSourcePath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId));
                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(request));

                RestResponse httpResponse = HttpClientPost(url, httpContent);
                if (httpResponse.hdr.result != RestConstants.SUCCESSFUL)
                {
                    response.Errors.Add(new WSMSDFault(httpResponse.hdr.fieldName, httpResponse.hdr.errorKey, httpResponse.hdr.errorMessage));
                }
                else
                {
                    response.ResponseObject = JsonConvert.DeserializeObject<WSCustomerFundingSourcePostResponse>(httpResponse.responseBody);
                    return response;
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("CustomerFundingSourcePostCCClear", CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return response;
        }

        // todo finish below
        public CubResponse<WSCustomerFundingSourcePostResponse> CustomerFundingSourcePostDDRef(string customerId, string pgCardId, string maskedPan, string cvv, int pointOfEntryMode, string merchantId, string transactionType,
                                                                                               string billingAddressId, bool? setAsPrimary)
        {
            CubResponse<WSCustomerFundingSourcePostResponse> response = new CubResponse<WSCustomerFundingSourcePostResponse>();
            response.Errors = new List<WSMSDFault>();

            try
            {
                WSFundingSource request = new WSFundingSource();

                request.directDebit = new WSDirectDebitReference();

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerFundingSourcePath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId));
                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(request));

                RestResponse httpResponse = HttpClientPost(url, httpContent);
                if (httpResponse.hdr.result != RestConstants.SUCCESSFUL)
                {
                    response.Errors.Add(new WSMSDFault(httpResponse.hdr.fieldName, httpResponse.hdr.errorKey, httpResponse.hdr.errorMessage));
                }
                else
                {
                    response.ResponseObject = JsonConvert.DeserializeObject<WSCustomerFundingSourcePostResponse>(httpResponse.responseBody);
                    return response;
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("CustomerFundingSourcePost", CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return response;
        }

        public CubResponse<WSCustomerFundingSourcePostResponse> CustomerFundingSourcePostDDClear(string customerId, string nameOnAccount, string bankAccountNumber, string bankRoutingNumber, string accountType, string financialInstitutionName,
                                                                                                 string billingAddressId, bool? setAsPrimary)
        {
            CubResponse<WSCustomerFundingSourcePostResponse> response = new CubResponse<WSCustomerFundingSourcePostResponse>();
            response.Errors = new List<WSMSDFault>();

            try
            {
                WSFundingSource request = new WSFundingSource();

                request.directDebit = new WSDirectDebitClear();
                request.directDebit.nameOnAccount = nameOnAccount;
                request.directDebit.accountType = accountType;
                request.directDebit.financialInstitutionName = financialInstitutionName;
                request.setAsPrimary = setAsPrimary.HasValue ? setAsPrimary.Value : false;
                request.billingAddressId = formatGuid(billingAddressId);
                ((WSDirectDebitClear)request.directDebit).bankAccountNumber = bankAccountNumber;
                ((WSDirectDebitClear)request.directDebit).bankRoutingNumber = bankRoutingNumber;


                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerFundingSourcePath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId));
                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(request));

                RestResponse httpResponse = HttpClientPost(url, httpContent);
                if (httpResponse.hdr.result != RestConstants.SUCCESSFUL)
                {
                    response.Errors.Add(new WSMSDFault(httpResponse.hdr.fieldName, httpResponse.hdr.errorKey, httpResponse.hdr.errorMessage));
                }
                else
                {
                    response.ResponseObject = JsonConvert.DeserializeObject<WSCustomerFundingSourcePostResponse>(httpResponse.responseBody);
                    return response;
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("CustomerFundingSourcePost", CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return response;
        }


        /// <summary>
        /// CustomerFundingSourceGet
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="inactive"></param>
        /// <returns>WSFundingSourcesResponse</returns>
        public string CustomerFundingSourceGet(string customerId, bool? inactive)
        {
            CubLogger.Debug("Method Entered");
            WSGetFundingSourcesResponse wsresp = new WSGetFundingSourcesResponse();
            
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerFundingSourcePath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId));

                if (inactive.HasValue) 
                    url += "?inactive=" + inactive;

                RestResponse resp = HttpClientGet(url);
                return resp.responseBody;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
        }

        /// <summary>
        /// CustomerFundingSourceGetById
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="fundingSourceId"></param>
        /// <returns>WSGetFundingSourceResponse</returns>
        public WSGetFundingSourceResponse CustomerFundingSourceGetById(string customerId, string fundingSourceId)
        {
            CubLogger.Debug("Method Entered");
            WSGetFundingSourceResponse wsresp = new WSGetFundingSourceResponse();
            wsresp.hdr = new WSWebResponseHeader();
            
            try
            {
                if (String.IsNullOrEmpty(fundingSourceId))
                {
                    CubLogger.Error("Error - Missing fundingSourceId");
                    wsresp.hdr.errorMessage = "Error - Missing fundingSourceId";
                    return wsresp;
                }
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerFundingSourcePath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId));
                url += "/" + fundingSourceId;

                RestResponse resp = HttpClientGet(url);
                if (!string.IsNullOrEmpty(resp.responseBody))
                    wsresp = JsonConvert.DeserializeObject<WSGetFundingSourceResponse>(resp.responseBody);
                wsresp.hdr = resp.hdr;

            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return wsresp;
        }

        /// <summary>
        /// CustomerFundingSourceDelete
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="fundingSourceId"></param>
        /// <returns>WSGetFundingSourceResponse</returns>
        public WSWebResponseHeader CustomerFundingSourceDelete(string customerId, string fundingSourceId)
        {
            WSWebResponseHeader response = new WSWebResponseHeader();
            try
            {
                if (String.IsNullOrEmpty(fundingSourceId))
                {
                    CubLogger.Error("Error - Missing fundingSourceId");
                    response.errorMessage = "Error - Missing fundingSourceId";
                    return response;
                }
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerFundingSourcePath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId));
                url += "/" + fundingSourceId;

                RestResponse httpResponse = HttpClientDelete(url);
                if (httpResponse.hdr.result != RestConstants.SUCCESSFUL)
                {
                    response.errorMessage = httpResponse.hdr.errorMessage;
                    response.fieldName = httpResponse.hdr.fieldName;
                    response.errorKey = httpResponse.hdr.errorKey;
                    return response;
                }

            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.errorMessage = ex.Message;
                return response;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return null;
        }

        /// <summary>
        /// CustomerFundingSourcePatchCC
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="fundingSourceId"></param>
        /// <param name="pgCardId"></param>
        /// <param name="cardExpiryMMYY"></param>
        /// <param name="nameOnCard"></param>
        /// <param name="address1"></param>
        /// <param name="country"></param>
        /// <param name="postalCode"></param>
        /// <param name="billingAddressId"></param>
        /// <param name="setAsPrimary"></param>
        /// <returns>WSWebResponseHeader - no data</returns>
        public WSWebResponseHeader CustomerFundingSourcePatchCC(string customerId, string fundingSourceId, string pgCardId, string cardExpiryMMYY, string nameOnCard, 
                                                                string address1, string country, string postalCode,
                                                                string billingAddressId, bool? setAsPrimary)
        {
            WSWebResponseHeader response = new WSWebResponseHeader();
        
            try
            {
                if (String.IsNullOrEmpty(fundingSourceId))
                {
                    CubLogger.Error("Error - Missing fundingSourceId");
                    response.errorMessage = "Error - Missing fundingSourceId";
                    return response;
                }

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerFundingSourcePath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId));
                url += "/" + fundingSourceId;
                string request = "";

                StringContent httpContent = new StringContent("");

                // Todo: NIS Patch request can only send the changed fields in the request. 

                if (pgCardId != null) // edit credit card request 
                {
                   // WSFundingSource request = new WSFundingSource();

                    request = "{ \"creditCard\": { \"pgCardId\": " + pgCardId;
                    if (cardExpiryMMYY != null)
                    {
                        request += ", \"cardExpiryMMYY\": \"" + cardExpiryMMYY + "\"";
                    }
                    if (nameOnCard != null)
                    {
                        request += ", \"nameOnCard\": \"" + nameOnCard + "\"";
                    }
                    request += " }";
                    if (setAsPrimary != null)
                    {
                        request += ", \"setAsPrimary\": " + ((bool)setAsPrimary).ToString().ToLower();
                    }
                    if (billingAddressId != null)
                    {
                        request += ", \"billingAddressId\": \"" + formatGuid(billingAddressId )+ "\"";
                    }
                    request += " }";

                    httpContent = new StringContent(request);
                }
                else if(setAsPrimary != null && (bool)setAsPrimary) // setAsPrimary update only request
                {
                    request = "{ \"setAsPrimary\": " + ((bool)setAsPrimary).ToString().ToLower() + " }";
                    httpContent = new StringContent(request);
                }

                RestResponse httpResponse = HttpClientPatch(url, httpContent);
                if (httpResponse.hdr.result != RestConstants.SUCCESSFUL)
                {
                    response.errorMessage = httpResponse.hdr.errorMessage;
                    response.fieldName = httpResponse.hdr.fieldName;
                    response.errorKey = httpResponse.hdr.errorKey;
                    response.result = httpResponse.hdr.result;
                    return response;
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.errorMessage = ex.Message;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            
            return null;
        }

        /// <summary>
        /// CustomerFundingSourcePatchDD
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="fundingSourceId"></param>
        /// <param name="pgCardId"></param>
        /// <param name="cardExpiryMMYY"></param>
        /// <param name="nameOnCard"></param>
        /// <param name="address1"></param>
        /// <param name="country"></param>
        /// <param name="postalCode"></param>
        /// <param name="billingAddressId"></param>
        /// <param name="setAsPrimary"></param>
        /// <returns>WSWebResponseHeader</returns>
        // finish below when NIS adds support for direct debit - convert from cc to dd
        public WSWebResponseHeader CustomerFundingSourcePatchDD(string customerId, string fundingSourceId, string pgCardId, string cardExpiryMMYY, string nameOnCard, string address1, string country, string postalCode,
                                                                string billingAddressId, bool? setAsPrimary)
        {
            WSWebResponseHeader response = new WSWebResponseHeader();

            try
            {
                if (String.IsNullOrEmpty(fundingSourceId))
                {
                    CubLogger.Error("Error - Missing fundingSourceId");
                    response.errorMessage = "Error - Missing fundingSourceId";
                    return response;
                }

                WSFundingSource request = new WSFundingSource();
                request.creditCard = new WSCreditCardClear();
                request.creditCard.pgCardId = pgCardId;
                request.creditCard.cardExpiryMMYY = cardExpiryMMYY;
                request.creditCard.nameOnCard = nameOnCard;
                ((WSCreditCardClear)request.creditCard).avsData = new WSAvsData();
                ((WSCreditCardClear)request.creditCard).avsData.postalCode = postalCode;
                ((WSCreditCardClear)request.creditCard).avsData.address1 = address1;
                ((WSCreditCardClear)request.creditCard).avsData.country = country;
                request.setAsPrimary = setAsPrimary.HasValue ? setAsPrimary.Value : false;
                request.billingAddressId = formatGuid(billingAddressId);

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISCustomerFundingSourcePath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId));
                url += "/" + fundingSourceId;
                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(request));
                RestResponse httpResponse = HttpClientPatch(url, httpContent);
                if (httpResponse.hdr.result != RestConstants.SUCCESSFUL)
                {
                    response.errorMessage = httpResponse.hdr.errorMessage;
                    response.fieldName = httpResponse.hdr.fieldName;
                    response.errorKey = httpResponse.hdr.errorKey;
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.errorMessage = ex.Message;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return null;
        }

        /// <summary>
        /// Get one Account Summary
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="returnSubsystemAccountDetailedInfo">Return Subsystem Account Detailed Info</param>
        /// <returns>WSOneAccountSummaryResponse</returns>
        public WSOneAccountSummaryResponse GetOneAccountSummary(string customerId, bool? returnSubsystemAccountDetailedInfo)
        {
            CubLogger.Debug("Method Entered");
            WSOneAccountSummaryResponse wsresp = new WSOneAccountSummaryResponse();

            try
            {

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                
                string actionURL = GlobalsCache.NISGetOneAccountSummaryPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, formatGuid(customerId), returnSubsystemAccountDetailedInfo);
                if (returnSubsystemAccountDetailedInfo.HasValue)
                {
                    url += "?returnSubsystemAccountDetailedInfo=" + returnSubsystemAccountDetailedInfo.Value;
                }

                RestResponse resp = HttpClientGet(url);

                if (!string.IsNullOrEmpty(resp.responseBody))
                    wsresp = JsonConvert.DeserializeObject<WSOneAccountSummaryResponse>(resp.responseBody);
                wsresp.hdr = resp.hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return wsresp;
        }

        /// <summary>
        /// Call te NIS API Method: BASE_URL/oneaccount/<oneaccount-id>/subsystem/<subsystem-id>/subsystemaccount/<account-ref>/link POST
        /// This API is used to link a Subsystem Account to a customer�s account.  The customer Id and account Id are required to validate that this 
        /// account belongs to this customer.  The subsystem Id is required to identify the target subsystem and the externalSubsystemAccountId is 
        /// required to link the subsystem account. If no nickname is provided, a default nickname will be set.
        /// </summary>
        /// <param name="oneaccountId">Unique identifier for the one account (Required)</param>
        /// <param name="subsystemId">Unique identifier for the subsystem where the travel token is registered (Required)</param>
        /// <param name="accountRef">Account number of the travel token associated within the given subsystem (Required)</param>
        /// <param name="subsystemAccountNickname">Specifies a nickname to be associated with the subsystem account (Required)</param>
        public WSLinkSubssystemToCustomerAccountResponse LinkSubssystemToCustomerAccount(string oneaccountId, string subsystemId, 
                                                                                         string accountRef, string subsystemAccountNickname)
        {
            var response = new WSLinkSubssystemToCustomerAccountResponse();
            try
            {
                var request = new WSLinkSubssystemToCustomerAccountRequest();
                request.subsystemAccountNickname = subsystemAccountNickname;
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string methodURL = GlobalsCache.NISLinkSubssystemToCustomerAccountPath(_organizationService).ToString();
                methodURL = string.Format(methodURL, oneaccountId, subsystemId, accountRef);
                string url = baseURL + methodURL;
                var httpContent = new StringContent(JsonConvert.SerializeObject(request));
                RestResponse httpResponse = HttpClientPost(url, httpContent);
                if (httpResponse.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(httpResponse.responseBody))
                    {
                        response = httpResponse.responseBody.DeserializeObject<WSLinkSubssystemToCustomerAccountResponse>();
                    }
                }
                else
                {
                    string msg = string.Format("{0} - {1}{2}", 
                                               httpResponse.hdr.fieldName, 
                                               httpResponse.hdr.errorKey, 
                                               string.IsNullOrWhiteSpace(httpResponse.hdr.errorMessage) ? "" : $" - {httpResponse.hdr.errorMessage}");
                    CubLogger.Error(msg);
                }
                response.hdr = httpResponse.hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.hdr.errorMessage = ex.Message;
                //throw;
            }
            return response;
        }

        /// <summary>
        /// Call te NIS API Method: BASE_URL/oneaccount/<oneaccount-id>/subsystem/<subsystem-id>/subsystemaccount/<account-ref>/delink POST
        /// This API is used to delink a Subsystem Account from a customer�s account.  The customer Id and account Id are required to validate that this 
        /// account belongs to this customer.  The subsystem Id is required to identify the target subsystem and the externalSubsystemAccountId is 
        /// required to delink the subsystem account. 
        /// </summary>
        /// <param name="oneaccountId">Unique identifier for the one account (Required)</param>
        /// <param name="subsystemId">Unique identifier for the subsystem where the travel token is registered (Required)</param>
        /// <param name="accountRef">Account number of the travel token associated within the given subsystem (Required)</param>
        /// <param name="subsystemAccount">Account number of the travel token associated within the given subsystem (Required)</param>
        public WSCanBeDelinkedSubssystemFromCustomerAccountResponse CanBeDeLinkedCustomerAccountFromSubsystem(string oneaccountId, string subsystemId, string subsystemAccount)
        {
            var response = new WSCanBeDelinkedSubssystemFromCustomerAccountResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string methodURL = GlobalsCache.NISCanBeDelinkedSubssystemFromCustomerAccountPath(_organizationService).ToString();
                methodURL = string.Format(methodURL, oneaccountId, subsystemId, subsystemAccount);
                string url = baseURL + methodURL;
                RestResponse httpResponse = HttpClientGet(url);
                if (httpResponse.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(httpResponse.responseBody))
                    {
                        response = httpResponse.responseBody.DeserializeObject<WSCanBeDelinkedSubssystemFromCustomerAccountResponse>();
                    }
                }
                else
                {
                    string msg = string.Format("{0} - {1}{2}",
                                               httpResponse.hdr.fieldName,
                                               httpResponse.hdr.errorKey,
                                               string.IsNullOrWhiteSpace(httpResponse.hdr.errorMessage) ? "" : $" - {httpResponse.hdr.errorMessage}");
                    CubLogger.Error(msg);
                }
                response.hdr = httpResponse.hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                //response.hdr.errorMessage = ex.Message;
                throw;
            }
            return response;
        }

        /// <summary>
        /// Call te NIS API Method: BASE_URL/oneaccount/<oneaccount-id>/subsystem/<subsystem-id>/subsystemaccount/<account-ref>/delink POST
        /// This API is used to delink a Subsystem Account from a customer�s account.  The customer Id and account Id are required to validate that this 
        /// account belongs to this customer.  The subsystem Id is required to identify the target subsystem and the externalSubsystemAccountId is 
        /// required to delink the subsystem account. 
        /// </summary>
        /// <param name="oneaccountId">Unique identifier for the one account (Required)</param>
        /// <param name="subsystemId">Unique identifier for the subsystem where the travel token is registered (Required)</param>
        /// <param name="subsystemAccount">Account number of the travel token associated within the given subsystem (Required)</param>
        public WSDelinkSubssystemFromCustomerAccountResponse DeLinkSubssystemFromCustomerAccount(string oneaccountId, string subsystemId, string subsystemAccount)
        {
            var response = new WSDelinkSubssystemFromCustomerAccountResponse();
            try
            {
                var request = new WSDelinkSubssystemFromCustomerAccountRequest();
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string methodURL = GlobalsCache.NISDelinkSubssystemFromCustomerAccountPath(_organizationService).ToString();
                methodURL = string.Format(methodURL, oneaccountId, subsystemId, subsystemAccount);
                string url = baseURL + methodURL;
                var httpContent = new StringContent(JsonConvert.SerializeObject(request));
                RestResponse httpResponse = HttpClientPost(url, httpContent);
                if (httpResponse.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(httpResponse.responseBody))
                    {
                        response = httpResponse.responseBody.DeserializeObject<WSDelinkSubssystemFromCustomerAccountResponse>();
                    }
                }
                else
                {
                    string msg = string.Format("{0} - {1}{2}",
                                               httpResponse.hdr.fieldName,
                                               httpResponse.hdr.errorKey,
                                               string.IsNullOrWhiteSpace(httpResponse.hdr.errorMessage) ? "" : $" - {httpResponse.hdr.errorMessage}");
                    CubLogger.Error(msg);
                }
                response.hdr = httpResponse.hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.hdr.errorMessage = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// This API is used to retrieve notification preferences for a customer�s contact.
        /// This method accepts optional filters by:
        /// contactId: to narrow down the results to a specific contact or if this filter is not provided, all contacts are returned.
        /// reqType: to narrow down the results to only show Mandatory or NotMandatory notification types and channels.
        /// channel: to narrow down the results to only show notification types that are set to be sent by a given channel.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID (Optional)</param>
        /// <param name="reqType">Required Type (Optional): M</param>
        /// <param name="channel">Channel</param>
        /// <param name="type">Type</param>
        /// <returns>Notification Preferences</returns>
        public CustomerNotificationPreferencesResponse GetCustomerNotificationPreferences(string customerId, string contactId, string reqType, string channel, string type)
        {
            CustomerNotificationPreferencesResponse wsresp = new CustomerNotificationPreferencesResponse();

            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                // Fix this
                string actionURL = GlobalsCache.NISGetCustomerNotificationPreferencesPath(_organizationService).ToString();
                string url = baseURL +
                             string.Format(actionURL, formatGuid(customerId), formatGuid(contactId), reqType, channel, type);
                RestResponse resp = HttpClientGet(url);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = resp.responseBody.DeserializeObject<CustomerNotificationPreferencesResponse>();
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
                wsresp.hdr = resp.hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return wsresp;
        }

        /// <summary>
        /// This API is used to retrieve config notification channels
        /// This method accepts optional filters by:
        /// enabled: to narrow down the results to a enabled/disabled notifications
        /// </summary>
        /// <returns>Config Notification Channel Response</returns>
        public WSGetConfigNotificationChannelResponse GetConfigNotificationChannel(bool? enabled)
        {
            WSGetConfigNotificationChannelResponse wsresp = new WSGetConfigNotificationChannelResponse();

            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetConfigNotificationChannelPath(_organizationService).ToString();
                if (enabled != null)
                {
                    actionURL += "?enabled=" + enabled;
                }
                string url = baseURL + actionURL;
                RestResponse resp = HttpClientGet(url);
                if (!string.IsNullOrEmpty(resp.responseBody))
                    wsresp = JsonConvert.DeserializeObject<WSGetConfigNotificationChannelResponse>(resp.responseBody);
                wsresp.hdr = resp.hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return wsresp;
        }

        /// <summary>
        /// Calls NIS Password Report Change. Call this method on a separate thread if you don't want the main thread to wait for the response.
        /// In this case there is no return
        /// </summary>
        /// <param name="accountId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        public void PasswordReportChange(Guid accountId, Guid contactId)
        {
            try
            {
                CubLogger.Debug("Method PasswordReportChange Entered");
                string requestUri = GlobalsCache.NISApiBaseURL(_organizationService) +
                                    string.Format(GlobalsCache.NISPasswordReportChangePath(_organizationService),
                                    formatGuid(accountId), formatGuid(contactId));

                System.Net.Http.StringContent httpContent = new System.Net.Http.StringContent(string.Empty);

                RestResponse resp = HttpClientPost(requestUri, httpContent);

                CubLogger.DebugFormat("{0} response {1}", requestUri, resp.hdr.result);
            }
            finally
            {
                CubLogger.Debug("Method PasswordReportChange Exit");
            }
        }

        /// <summary>
        /// Get Transit Account Status
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="reqType">Request type</param>
        /// <param name="channel">Channel</param>
        /// <param name="type">Type</param>
        /// <returns>Transit Account Status as JSON</returns>
        public string GetTransitAccountStatus(string customerId, string contactId, string reqType, string channel, string type)
        {
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                // Fix this
                string actionURL = GlobalsCache.NISGetCustomerNotificationPreferencesPath(_organizationService).ToString();
                string url = baseURL +
                             string.Format(actionURL, formatGuid(customerId), formatGuid(contactId), reqType, channel, type);
                RestResponse resp = HttpClientGet(url);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    return resp.responseBody;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return null;
        }

        /// <summary>
        /// update contact information and notification preferences for a list of customer�s contacts.  
        /// It accepts partial updates for the notification preferences or for the contact information with the values in the request.
        /// If in the request, a preference by notification type and channel is disabling a mandatory notification type, all other updates in the request are processed but the disabling of the mandatory notification type is not applied and no errors are returned.
        /// </summary>
        /// <param name="requestData">Customer Notification Preference Patch Data</param>
        /// <returns>Customer Notification Preference Patch Response</returns>
        public CubResponse<WSCustomerNotificationPreferencesPatchResponse> CustomerNotificationPreferencesPatch(CustomerNotificationPreferencesRequest requestData)
        {
            CubResponse<WSCustomerNotificationPreferencesPatchResponse> response = new CubResponse<WSCustomerNotificationPreferencesPatchResponse>();
            response.Errors = new List<WSMSDFault>();
            try
            {
                CubLogger.Debug("Method Entered");
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string methodURL = GlobalsCache.NISCustomerNotificationPreferencesPatchPath(_organizationService).ToString();
                methodURL = string.Format(methodURL, formatGuid(requestData.customerId));
                string url = baseURL + methodURL;
                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(requestData));
                RestResponse httpResponse = HttpClientPatch(url, httpContent);
                if (httpResponse.hdr.result != RestConstants.SUCCESSFUL)
                {
                    response.Errors.Add(new WSMSDFault(httpResponse.hdr.fieldName, httpResponse.hdr.errorKey, httpResponse.hdr.errorMessage));
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("CustomerNotificationPreferencesPatch", CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return response;
        }

        /// <summary>
        /// Get Subsystem Token Type Mappings
        /// </summary>
        /// <returns>Subsystem Token Type Mappings as JSON</returns>
        public string GetSubsystemTokenTypeMappings()
        {
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                // Fix this
                string actionURL = GlobalsCache.SubsystemTokenTypeMappingsPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                RestResponse resp = HttpClientGet(url);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    return resp.responseBody;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
            }
            finally
            {
                CubLogger.Debug("Method Exit");
            }
            return null;
        }

        /// <summary>
        /// Get Address Dependencies
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="addressId">Address ID</param>
        /// <returns></returns>
        internal WSAddressDependenciesResponse GetAddressDependencies(Guid customerId, Guid addressId)
        {
            Model.MSDApi.WSAddressDependenciesResponse addressUpdateResp = new Model.MSDApi.WSAddressDependenciesResponse();

            string requestUri = GlobalsCache.NISApiBaseURL(_organizationService) +
                                string.Format(GlobalsCache.NISAddressDependenciesPath(_organizationService), formatGuid(customerId), formatGuid(addressId));

            RestResponse resp = HttpClientGet(requestUri);

            CubLogger.Debug(resp.SerializeObject());

            addressUpdateResp.hdr = resp.hdr;

            if (!string.IsNullOrEmpty(resp.responseBody))
            {
                WSAddressDependencies dependencies = JsonConvert.DeserializeObject<WSAddressDependencies>(resp.responseBody);
                addressUpdateResp.contactDependencies = dependencies.contacts;
                addressUpdateResp.fundingSourceDependencies = dependencies.fundingSources;
            }

            return addressUpdateResp;
        }

        /// http://localhost:12665/OneAccount/GetBalanceHistory?oneAccountId=2B9634F0-3F29-E711-80EA-005056813E0D
        /// <summary>
        /// Get the balance history of an one account. This API supports pagination so that it always returns the data of a given page. 
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// NIS API call: 2.10.2 oneaccount/<oneaccount-id>/balancehistory GET
        /// </summary>
        /// <param name="oneAccountId">(Required) Unique identifier for the OneAccount account. </param>
        /// <param name="startDateTime">(Conditionally Required) Starting date and time for the period the transactions are required for. Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally Required) Ending date and time for the period the transactions are required for. Required when startDateTime is given.</param>
        /// <param name="viewType">(Optional) This is to control filter of the journal entries that are details related auth, confirm, auto-reversal:
        /// Supported types are: Simple, Complete</param>
        /// <param name="purseId">(Optional) The unique identifier of the purse to get transactions for.</param>
        /// <param name="financialTxnType">(Optional) Financial Transaction type related journal entries to fetch.  If not specified all types are returned.
        /// Supported types are: PurseLoad, PurseLoadReversal, Charge(includes Charge, ChargeAuth, ChargeConfirm, ChargeAutoReversal), ChargeReversal, PurseAdjustment</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �transactionAmount.asc,transactionDateTime.desc� indicates sort by transactionAmount 
        /// ascending and trasactionDateTime descending. Default is descending by journal entry id.</param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10. Value should be between 1 and 9999, inclusive.</param>
        /// <returns>Once Account transctions as JSON</returns>
        public OneAccountBalanceHistoryResponse GetOneAccountBalanceHistory(string oneAccountId, string startDateTime, string endDateTime, string viewType, 
                                               string purseId, string financialTxnType, string sortBy, int? offset, int? limit)
        {
            OneAccountBalanceHistoryResponse wsresp = new OneAccountBalanceHistoryResponse();

            try
            {      
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);

                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API, 
                                                                CUBConstants.Globals.ONE_ACCOUNT_BALANCE_HISTORY_PATH).ToString();
                actionURL = string.Format(actionURL, oneAccountId);
                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (!string.IsNullOrWhiteSpace(startDateTime)) url.Append(string.Format("&startDateTime={0}", startDateTime));
                if (!string.IsNullOrWhiteSpace(endDateTime)) url.Append(string.Format("&endDateTime={0}", endDateTime));
                if (!string.IsNullOrWhiteSpace(viewType)) url.Append(string.Format("&viewType={0}", viewType));
                if (!string.IsNullOrWhiteSpace(purseId)) url.Append(string.Format("&purseId={0}", purseId));
                if (!string.IsNullOrWhiteSpace(financialTxnType)) url.Append(string.Format("&financialTxnType={0}", financialTxnType));
                if (!string.IsNullOrWhiteSpace(sortBy)) url.Append(string.Format("&sortBy={0}", sortBy));
                if (offset.HasValue) url.Append(string.Format("&offset={0}", offset));
                if (limit.HasValue) url.Append(string.Format("&limit={0}", limit));

                RestResponse resp = HttpClientGet(url.ToString());
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = JsonConvert.DeserializeObject<OneAccountBalanceHistoryResponse>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }

            return wsresp;
        }

        /// <summary>
        /// Get the balance history details of an account based on a  journal entry ID. 
        /// NIS API call: 2.10.3 oneaccount/<oneaccount-id>/journalentry/<journalentry-id>/balancehistorydetail GET
        /// </summary>
        /// <param name="oneAccountId">One account id</param>
        /// <param name="journalEntryId">Journal entry id</param>
        /// <returns>Balance history details as JSON</returns>
        public OneAccountBalanceHistoryDetailResponse GetOneAccountBalanceHistoryDetail(string oneAccountId, string journalEntryId)
        {
            OneAccountBalanceHistoryDetailResponse wsresp = new OneAccountBalanceHistoryDetailResponse();

            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API,
                                                                CUBConstants.Globals.ONE_ACCOUNT_BALANCE_HISTORY_DETAIL_PATH).ToString();
                string url = string.Format(baseURL + actionURL, oneAccountId, journalEntryId);

                RestResponse resp = HttpClientGet(url);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = JsonConvert.DeserializeObject<OneAccountBalanceHistoryDetailResponse>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }

            return wsresp;
        }

        /// <summary>
        /// Get the travel history for an account. This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// NIS API call: 2.10.4 oneaccount/<oneaccount-id>/travelhistory GET
        /// </summary>
        /// <param name="oneAccountId">(Required) Unique identifier for the OneAccount account. </param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the transactions are required for, defaults to 7 days ago.  Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.  Required when startDateTime is given.</param>
        /// <param name="authorityId">(Optional) Comma separated list of the authority IDs to get the travel history of, defaults to all authorities.</param>
        /// <param name="travelMode">(Optional) Comma separated list of the travel mode IDs to return the history of, defaults to all travel modes. See section 2.6.9 for allowable values.</param>
        /// <param name="correctable">(Optional) Flag to indicate if only correctable trips should be returned or not.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10.</param>
        /// <returns>One Account Travel History as JSON</returns>
        public OneAccountTravelHistoryResponse GetOneAccountTravelHistory(string oneAccountId, string startDateTime, string endDateTime, string authorityId, 
                                                 string travelMode, string tripCategory, string viewType, string sortBy, int? offset, int? limit)
        {
            OneAccountTravelHistoryResponse wsresp = new OneAccountTravelHistoryResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API,
                                                                CUBConstants.Globals.ONE_ACCOUNT_TRAVEL_HISTORY_PATH).ToString();
                actionURL = string.Format(actionURL, oneAccountId);
                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (!string.IsNullOrWhiteSpace(startDateTime)) url.Append(string.Format("&startDateTime={0}", startDateTime));
                if (!string.IsNullOrWhiteSpace(endDateTime)) url.Append(string.Format("&endDateTime={0}", endDateTime));
                if (!string.IsNullOrWhiteSpace(authorityId)) url.Append(prepareListParamForNIS("authorityId", authorityId));
                if (!string.IsNullOrWhiteSpace(travelMode)) url.Append(prepareListParamForNIS("travelMode", travelMode));
                if (!string.IsNullOrWhiteSpace(tripCategory)) url.Append(prepareListParamForNIS("tripCategory", tripCategory));
                if (!string.IsNullOrWhiteSpace(viewType)) url.Append(string.Format("&viewType={0}", viewType));
                if (!string.IsNullOrWhiteSpace(sortBy)) url.Append(string.Format("&sortBy={0}", sortBy));
                if (offset.HasValue) url.Append(string.Format("&offset={0}", offset));
                if (limit.HasValue) url.Append(string.Format("&limit={0}", limit));

                CubLogger.Debug(string.Format("GetOneAccountTravelHistory url = {0}", url.ToString()));

                RestResponse resp = HttpClientGet(url.ToString());
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = JsonConvert.DeserializeObject<OneAccountTravelHistoryResponse>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
            }
            return wsresp;
        }

        /// <summary>
        /// Get the travel history detail information for an account by transaction ID.
        /// NIS API call: 2.10.5 oneaccount/<oneaccount-id>/transaction/<transaction-id>/travelhistorydetail GET
        /// </summary>
        /// <param name="oneAccountId">One account id</param>
        /// <param name="transactId">Transaction ID</param>
        /// <returns>One account travel history detail as JSON</returns>
        public OneAccountTravelHistoryDetailResponse GetOneAccountTravelHistoryDetail(string oneAccountId, string transactId)
        {
            OneAccountTravelHistoryDetailResponse wsresp = new OneAccountTravelHistoryDetailResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API,
                                                                CUBConstants.Globals.ONE_ACCOUNT_TRAVEL_HISTORY_DETAIL_PATH).ToString();
                string url = string.Format(baseURL + actionURL, oneAccountId, transactId);

                RestResponse resp = HttpClientGet(url);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = JsonConvert.DeserializeObject<OneAccountTravelHistoryDetailResponse>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// Get the travel history detail information for an account by transaction ID. 
        /// NIS API call: 2.12.4 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/transaction/<transaction-id>/travelhistorydetail GET
        /// </summary>
        /// <param name="accountId">(Required) Unique identifier for the transit account.</param>
        /// <param name="subsystemId">(Required) Subsystem ID</param>
        /// <param name="tripId">(Required) Trip ID to return the details for</param>
        /// <returns>Transit Account Travel History Detail as JSON</returns>
        public MvcGenericResponse GetTransitAccountTravelHistoryDetail(string transitaccountId, string subsystemId, string tripId)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API,
                                                                CUBConstants.Globals.TRANSIT_ACCOUNT_TRAVEL_HISTORY_DETAIL_PATH).ToString();
                string url = string.Format(baseURL + actionURL, transitaccountId, subsystemId, tripId);
                CubLogger.Error("GetTransitAccountTravelHistoryDetail: url=" + url);

                RestResponse resp = HttpClientGet(url);
                CubLogger.Error("GetTransitAccountTravelHistoryDetail: resp.hdr.result=" + resp.hdr.result);

                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// Get the status history information for an account
        /// NIS API call: 2.12.8 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/statushistory GET
        /// </summary>
        /// <param name="accountId">(Required) Unique identifier for the transit account.</param>
        /// <param name="subsystemId">(Required) Subsystem ID</param>
        /// <returns>Transit Account Status History as JSON</returns>
        public MvcGenericResponse GetTransitAccountStatusHistory(string transitaccountId, string subsystemId, string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API,
                                                                CUBConstants.Globals.TRANSIT_ACCOUNT_STATUS_HISTORY_PATH).ToString();
                actionURL = string.Format(actionURL, transitaccountId, subsystemId);

                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (!string.IsNullOrWhiteSpace(sortBy)) url.Append(string.Format("&sortBy={0}", sortBy));
                if (offset.HasValue) url.Append(string.Format("&offset={0}", offset));
                if (limit.HasValue) url.Append(string.Format("&limit={0}", limit));

                CubLogger.Info("GetTransitAccountStatusHistory: url=" + url);
                RestResponse resp = HttpClientGet(url.ToString());
                
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// Get the status history information for an account
        /// NIS API call: 2.12.8 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/statushistory GET
        /// </summary>
        /// <param name="accountId">(Required) Unique identifier for the transit account.</param>
        /// <param name="subsystemId">(Required) Subsystem ID</param>
        /// <returns>Transit Account Status History as JSON</returns>
        public MvcGenericResponse GetTokenStatusHistory(string subsystemId, string tokenId, string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API,
                                                                CUBConstants.Globals.TOKEN_STATUS_HISTORY_PATH).ToString();
                actionURL = string.Format(actionURL, subsystemId, tokenId);

                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (!string.IsNullOrWhiteSpace(sortBy)) url.Append(string.Format("&sortBy={0}", sortBy));
                if (offset.HasValue) url.Append(string.Format("&offset={0}", offset));
                if (limit.HasValue) url.Append(string.Format("&limit={0}", limit));

                CubLogger.Info("GetTokenStatusHistory: url=" + url);
                RestResponse resp = HttpClientGet(url.ToString());

                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }


        /// <summary>
        /// Take an action on the Token - NIS 2.16.20 TokenAction PUT
        /// This API is used to taken an action (block/unblock/terminate) on a travel token for a transit account.
        /// </summary>
        /// <param name="transitAccount"></param>
        /// <param name="subsystem"></param>
        /// <param name="traveltoken"></param>
        /// <param name="action"></param>
        /// <returns>No data is returned by this method.Common headers and http response codes are returned.</returns>
        public MvcGenericResponse TokenAction(string transitAccountId, string subsystemId, string traveltoken, string customerId, string reasonCode, string notes, string tokenAction)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISSubssytemTokenActionPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, transitAccountId, subsystemId, traveltoken, tokenAction);
                var request = new
                {
                    customerId = customerId,
                    reasonCode = reasonCode,
                    notes = notes
                };

                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(request));
                RestResponse resp = HttpClientPut(url, httpContent);

                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    wsresp.Header = resp.hdr;

                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;

                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }
            return wsresp;
        }

    /// <summary>
    /// 2.8.26 customer/<customer-id>/order/history GET
    /// This API is used to request the the order history for a customer.  
    /// This search provides an option to search in the realtime data source by setting the optional filter realtimeResults to true. 
    /// This is to offer customer service the ability to troubleshoot orders that are not completely processed yet. 
    /// However, this option should be used in rare cases and some information will not be available in the search results.
    /// </summary>
    /// <param name="customerId">(Required)  Unique identifier for the customer.</param>
    /// <param name="startDateTime">(Conditionally-Required)  Starting date and time for the period the orders are required for. Required when endDateTime is given</param>
    /// <param name="endDateTime">(Conditionally-Required)  Ending date and time for the period the orders are required for. Required when startDateTime is given.</param>
    /// <param name="orderType">(Optional)  Filters by the order type.  Valid types are:
    ///                         � Adjustment
    ///                         � Autoload 
    ///                         � AutoloadEnrollment
    ///                         � CloseAccount
    ///                         � DebtCollection
    ///                         � Sale
    ///                         � TravelCorrection
    ///                         � Others will be added as they are implemented
    ///</param>
    /// <param name="orderStatus">(Optional)  Filters by the status of the order:  Valid statuses are:
    ///                            � OrderAccepted
    ///                            � PaymentFailed
    ///                            � FulfillmentFailed
    ///                            � Completed
    ///                            � CompletedWithErrors
    ///                            � PaymentAccepted
    ///                            � PreAccepted
    ///                            � Waiting
    ///                            � Failed
    ///                            � PaymentRequested
    ///                            � RefundIssued
    ///                            � ReadyToClose
    ///                            � ManualReview
    ///                            � QueuedForAging
    ///                            � RefundPending
    ///                            � Rejected
    /// </param>
    /// <param name="paymentStatus">(Optional)  Filters by the payment status. Valid statuses are:
    ///                             �	Pending
    ///                             �	Authorized
    ///                             �	FailedAuthorization
    ///                             �	Confirmed
    ///                             �	FailedConfirmation
    ///                             �	PendingConfirmation
    ///                             �	ConfirmationExpired
    /// </param>
    /// <param name="orderId">(Optional) Filter by the order id entered.</param>
    /// <param name="realtimeResults">(Optional)  Indicates if the results should be retrieved from a realtime source (i.e: OMS). By default this filter is set to false.</param>
    /// <param name="sortBy">(Optional)  The comma separated list of fields with sort order e.g: �orderDateTime.asc,orderType.desc� indicates sort by orderDateTime ascending and orderType descending. Default is descending by orderDateTime.
    ///                         � orderDateTime         
    ///                         � orderType
    ///                         � orderStatus
    ///                         � paymentStatus      
    ///                         � orderId
    /// </param>
    /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
    /// <param name="limit">(Optional) The number of records per page, defaults to 10. Value should be between 1 and 9999, inclusive</param>
    /// <returns>Order History</returns>
    public MvcGenericResponse GetCustomerOrderHistory(string customerId, string startDateTime, string endDateTime,
                                                          string orderType, string orderStatus, string paymentStatus, int? orderId,
                                                          bool? realtimeResults, string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API,
                                                                CUBConstants.Globals.CUSTOMER_ORDER_HISTORY_PATH).ToString();
                string url = string.Format(baseURL + actionURL, formatGuid(customerId));
                url = GetGustomerOrderHistoryPrepareUrlParam(url, startDateTime, endDateTime, orderType, orderStatus, 
                                                             paymentStatus, orderId,realtimeResults, sortBy, offset, limit, realtimeResults);
                CubLogger.Info($"GetCustomerOrderHistory: url= {url}");
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info($"GetCustomerOrderHistory: result={resp.hdr.result}");
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        private string GetGustomerOrderHistoryPrepareUrlParam(string baseUrl, string startDateTime, string endDateTime,
                                                                         string orderType, string orderStatus, string paymentStatus, int? orderId,
                                                                         bool? realtimeResults, string sortBy, int? offset, int? limit, bool? realTime)
        {
            StringBuilder url = new StringBuilder(baseUrl);
            url.Append("?");
            if (startDateTime != null)
            {
                url.Append($"&startDateTime={startDateTime}");
            }
            if (endDateTime != null)
            {
                url.Append($"&endDateTime={endDateTime}");
            }
            if (!string.IsNullOrEmpty(orderType))
            {
                url.Append(prepareListParamForNIS("orderType", orderType));
            }
            if (!string.IsNullOrEmpty(orderStatus))
            {
                url.Append(prepareListParamForNIS("orderStatus", orderStatus));
            }
            if (!string.IsNullOrEmpty(paymentStatus))
            {
                url.Append(prepareListParamForNIS("paymentStatus", paymentStatus));
                    
            }
            if(orderId.HasValue)
            {
                url.Append($"&orderId={orderId.Value}");
            }
            if (realtimeResults.HasValue)
            {
                url.Append($"&realtimeResults={realtimeResults.Value}");
            }
            if (sortBy != null)
            {
                url.Append($"&sortBy={sortBy}");
            }
            if (offset.HasValue)
            {
                url.Append($"&offset={offset.Value}");
            }
            if (limit.HasValue)
            {
                url.Append($"&limit={limit.Value}");
            }
            if (realTime.HasValue)
            {
                url.Append($"&realtimeResults={realTime.Value}");
            }
            return url.ToString();
        }

        /// <summary>
        /// Get Customer Order History Detail
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="orderId">Order ID</param>
        /// <returns>Order History Detail</returns>
        public MvcGenericResponse GetCustomerOrderHistoryDetail(string customerId, string orderId, bool? realtimeResults)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API,
                                                                CUBConstants.Globals.CUSTOMER_ORDER_HISTORY_DETAIL_PATH).ToString();
                string url = string.Format(baseURL + actionURL, formatGuid(customerId), orderId);
                if(realtimeResults.HasValue && realtimeResults.Value)
                {
                    url += "?realtimeResults=true";
                }

                RestResponse resp = HttpClientGet(url.ToString());
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        public WSGetCustomerOrderResponse GetCustomerOrder(string customerId, string orderId, string closeAccountRefresh)
        {
            WSGetCustomerOrderResponse wsresp = new WSGetCustomerOrderResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API,
                                                                CUBConstants.Globals.CUSTOMER_ORDER_PATH).ToString();
                string url = string.Format(baseURL + actionURL, orderId);
                if (closeAccountRefresh != null)
                {
                    url += $"?closeAccountRefresh={closeAccountRefresh}";
                }
                RestResponse resp = HttpClientGet(url.ToString());
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = JsonConvert.DeserializeObject<WSGetCustomerOrderResponse>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }


        /// <summary>
        /// Get Tolling Transponders
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public WSTollingGetTranspondersResponse GetTollingTransponders(string customerId)
        {
            WSTollingGetTranspondersResponse wsresp = new WSTollingGetTranspondersResponse();
            try
            {
                customerId = formatGuid(customerId);
                string baseURL = GlobalsCache.TollingApiBaseURL(_organizationService);
                //baseURL = "https://app.avatardemo.com.au/CUBIC-API-0.1-SNAPSHOT";
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.Tolling_API,
                                                                CUBConstants.Globals.TOLLING_GET_TRANSPONDERS_PATH).ToString();
                string url = string.Format(baseURL + actionURL, customerId);
                RestResponse resp = HttpClientGet(url.ToString());
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = JsonConvert.DeserializeObject<WSTollingGetTranspondersResponse>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// Get Tolling Vehicles
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public WSTollingGetVehiclesResponse GetTollingVehicles(string customerId)
        {
            WSTollingGetVehiclesResponse wsresp = new WSTollingGetVehiclesResponse();
            try
            {
                customerId = formatGuid(customerId);
                string baseURL = GlobalsCache.TollingApiBaseURL(_organizationService);

                //baseURL = "https://imgrev.avatardemo.com/CUBIC-API-0.1-SNAPSHOT";
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.Tolling_API,
                                                                CUBConstants.Globals.TOLLING_GET_VEHICLES_PATH).ToString();
                string url = string.Format(baseURL + actionURL, customerId);
                RestResponse resp = HttpClientGet(url.ToString());
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = JsonConvert.DeserializeObject<WSTollingGetVehiclesResponse>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// Get Tolling Transaction History
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public WSTollingGetTransactionHistoryResponse GetTollingTransactionHistory(string customerId)
        {
            WSTollingGetTransactionHistoryResponse wsresp = new WSTollingGetTransactionHistoryResponse();
            try
            {
                customerId = formatGuid(customerId);
                string baseURL = GlobalsCache.TollingApiBaseURL(_organizationService);

                //baseURL = "https://imgrev.avatardemo.com/CUBIC-API-0.1-SNAPSHOT";
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.Tolling_API,
                                                                CUBConstants.Globals.TOLLING_GET_TRANSACTION_HISTORY_PATH).ToString();
                string url = string.Format(baseURL + actionURL, customerId);
                RestResponse resp = HttpClientGet(url.ToString());
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp = JsonConvert.DeserializeObject<WSTollingGetTransactionHistoryResponse>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// Get Tolling Account Summary
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public WSTollingGetAccountSummaryResponse GetTollingAccountSummary(string customerId)
        {
            WSTollingGetAccountSummaryResponse wsresp = new WSTollingGetAccountSummaryResponse();
            try
            {
                customerId = formatGuid(customerId);
                string baseURL = GlobalsCache.TollingApiBaseURL(_organizationService);

                //baseURL = "https://imgrev.avatardemo.com/CUBIC-API-0.1-SNAPSHOT";
                string actionURL = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.Tolling_API,
                                                                CUBConstants.Globals.TOLLING_GET_ACCOUNT_SUMMARY_PATH).ToString();
                string url = string.Format(baseURL + actionURL, customerId);
                RestResponse resp = HttpClientGet(url.ToString());
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.accountSummary = JsonConvert.DeserializeObject<WsTollingAccountSummary>(resp.responseBody);
                    wsresp.hdr = resp.hdr;
                }
                else
                {
                    string msg = string.Format("{0} - {1} - {2}", resp.hdr.fieldName, resp.hdr.errorKey, resp.hdr.errorMessage);
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.hdr.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// This API is used to retrieve activity history that was associated with the customer based upon filtering parameters.
        /// 2.8.22 customer/<customer-id>/activity GET
        /// </summary>
        /// <param name="customerId">Customer MSD ID</param>
        /// <param name="contactId">(Optional) The contact Id for which the activity should be shown.</param>
        /// <param name="startDateTime">(Optional) The date and time that should be used to filter the starting date of the range. 
        ///  If not provided then the default should be configurable with a default value of now � 30 days.</param>
        /// <param name="endDateTime">(Optional)  The date and time that should be used to filter the ending date of the range.
        /// If not provided then the default should be the beginning of next day. 
        /// The maximum end date should be as number of days from the start date.</param>
        /// <param name="type">(Optional)  An identifier for the type of activity to return in the results.
        /// The default is to return all activity types if no value is provided.</param>
        /// <param name="activitySubject">(Optional)  An identifier for the subject of activity to return in the results. 
        ///     Activity subject is a logical grouping of activity types. The default is to return all activity subject if no value is provided.</param>
        /// <param name="activityCategory">(Optional)  An identifier for the category of activity to return in the results. 
        ///     Category is used to distinguish between event and audit. The default is to return all activity category if no value is provided.</param>
        /// <param name="channel">(Optional) Used to filter the activities by channel.</param>
        /// <param name="origin">(Optional)  An identifier for the origin of activity to return in the results. 
        ///     Origin tells the component which initiated the action which caused the activities to be generated. The default is to return all activity origin if no value is provided.</param>
        /// <param name="globalTxnId">(Optional)  An identifier for the category of activity to return in the results. 
        ///     Global transaction id is a mechanism to tie multiple method being called as part of a single action. The default is to return all activity category if no value is provided.</param>
        /// <param name="userName">(Optional) Used to filter the activities by the username that performed the activity.
        /// Allow for partial string with wild card �*�.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �type.asc,channel.desc� indicates sort by type ascending and status descending.
        /// Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) Used to identify the starting index that should be provided within the page in the search result.
        /// The default is to start at the first row of data (0) if no value is provided.</param>
        /// <param name="limit">(Optional) Used to identify the number of rows per page in the search result in the response.
        /// The default is to return 20 rows if no value is provided.</param>
        /// <returns>Customer Activities</returns>
        public MvcGenericResponse GetCustomerActivities(string customerId, string contactId, string startDateTime, string endDateTime,
                                          string type, string activityCategory, string activitySubject, string channel,
                                          string origin, string globalTxnId, string userName, string crmSessionId,
                                          string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                customerId = formatGuid(customerId);
                contactId = formatGuid(contactId);
                crmSessionId = formatGuid(crmSessionId);

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetCustomerActivityPath(_organizationService).ToString();
                actionURL = string.Format(actionURL, customerId);
                string url = baseURL + actionURL;

                var request = new
                {
                    contactId = contactId,
                    startDateTime = startDateTime,
                    endDateTime = endDateTime,
                    type = type,
                    activityCategory = activityCategory,
                    activitySubject = activitySubject,
                    channel = channel,
                    origin = origin,
                    globalTxnId = globalTxnId,
                    userName = userName,
                    crmSessionId = crmSessionId,
                    sortBy = sortBy,
                    offset = offset,
                    limit = limit
                };

                string requestSerialized = JsonConvert.SerializeObject(request);
                var httpContent = new StringContent(requestSerialized);
                CubLogger.Info($"GetCustomerActivities: url={url}   Body={requestSerialized}");
                RestResponse resp = HttpClientPost(url, httpContent);
                CubLogger.Info("GetCustomerActivities: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (NISException nisEx)
            {
                CubLogger.Error(string.Empty, nisEx);
                wsresp.Header = nisEx.Hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// This method get activity details based on the parameters passed in.
        /// If customerId is available it will call the customer activity detail api otherwise it will call the transit account activity details api
        /// </summary>
        /// <param name="customerId">Required if getting customer activities</param>
        /// <param name="transitAccountId">Required if getting transit account subsystem activities</param>
        /// <param name="subsystemId">Required if getting transit account subsystem activities</param>
        /// <param name="activityId">Required</param>
        /// <param name="contactId"></param>
        /// <param name="contact"></param>
        /// <param name="actionType"></param>
        /// <param name="location"></param>
        /// <param name="device"></param>
        /// <param name="sourceIpAddress"></param>
        /// <param name="type"></param>
        /// <param name="activitySubject"></param>
        /// <param name="activityCategory"></param>
        /// <param name="createDateTime"></param>
        /// <param name="channel"></param>
        /// <param name="origin"></param>
        /// <param name="globalTxnId"></param>
        /// <param name="subsystemDescription"></param>
        /// <param name="trackingInfo"></param>
        /// <param name="userName"></param>
        /// <param name="activityData"></param>
        /// <returns>Activity Details Data</returns>
        public MvcGenericResponse GetActivityDetails(string customerId, string transitAccountId, string subsystemId, string activityId,
                    string contactId, string contact, string actionType, string location, string device, string sourceIpAddress, string type,
                    string activitySubject, string activityCategory, string createDateTime, string channel, string origin, string globalTxnId,
                    string subsystemDescription, string trackingInfo, string userName, string activityData, string getActivityDetailsFor)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                customerId = formatGuid(customerId);
                transitAccountId = formatGuid(transitAccountId);
                subsystemId = formatGuid(subsystemId);
                activityId = formatGuid(activityId);
                contactId = formatGuid(contactId);

                if (string.IsNullOrEmpty(activityId))
                {
                    throw new ArgumentException("Missing activityId parameter");
                }

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = string.Empty;
                if (getActivityDetailsFor == Account.EntityLogicalName)
                {
                    actionURL = GlobalsCache.NISGetCustomerActivityDetailPath(_organizationService).ToString();
                    actionURL = string.Format(actionURL, customerId, activityId);
                }
                else if (getActivityDetailsFor == cub_TransitAccount.EntityLogicalName)
                {
                    actionURL = GlobalsCache.NISTASubsystemActivityDetailPath(_organizationService).ToString();
                    actionURL = string.Format(actionURL, transitAccountId, subsystemId, activityId);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(customerId))
                    {
                        getActivityDetailsFor = Account.EntityLogicalName;
                        actionURL = GlobalsCache.NISGetCustomerActivityDetailPath(_organizationService).ToString();
                        actionURL = string.Format(actionURL, customerId, activityId);
                    }
                    else if (!string.IsNullOrWhiteSpace(transitAccountId) && !string.IsNullOrWhiteSpace(subsystemId))
                    {
                        getActivityDetailsFor = cub_TransitAccount.EntityLogicalName;
                        actionURL = GlobalsCache.NISTASubsystemActivityDetailPath(_organizationService).ToString();
                        actionURL = string.Format(actionURL, transitAccountId, subsystemId, activityId);
                    }
                }

                if (string.IsNullOrEmpty(actionURL))
                {
                    throw new ArgumentException("Either customerId is required or both transitAccountId and subsystemId are required to get activity details");
                }

                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");

                if (getActivityDetailsFor == Account.EntityLogicalName) // if calling the customer activity details, append the optional trasitaccount and subsystem fields
                {
                    if (!string.IsNullOrWhiteSpace(transitAccountId)) url.Append($"&transitaccountId={transitAccountId}");
                    if (!string.IsNullOrWhiteSpace(subsystemId)) url.Append($"&subsystemId={subsystemId}");
                }
                
                if (!string.IsNullOrWhiteSpace(contactId)) url.Append($"&contactId={contactId}");
                if (!string.IsNullOrWhiteSpace(actionType)) url.Append($"&actionType={actionType}");
                if (!string.IsNullOrWhiteSpace(location)) url.Append($"&location={location}");
                if (!string.IsNullOrWhiteSpace(device)) url.Append($"&device={device}");
                if (!string.IsNullOrWhiteSpace(sourceIpAddress)) url.Append($"&sourceIpAddress={sourceIpAddress}");
                if (!string.IsNullOrWhiteSpace(type)) url.Append($"&type={type}");
                if (!string.IsNullOrWhiteSpace(activitySubject)) url.Append($"&activitySubject={activitySubject}");
                if (!string.IsNullOrWhiteSpace(activityCategory)) url.Append($"&activityCategory={activityCategory}");
                if (!string.IsNullOrWhiteSpace(createDateTime)) url.Append($"&createDateTime={createDateTime}");
                if (!string.IsNullOrWhiteSpace(channel)) url.Append($"&channel={channel}");
                if (!string.IsNullOrWhiteSpace(origin)) url.Append($"&origin={origin}");
                if (!string.IsNullOrWhiteSpace(globalTxnId)) url.Append($"&globalTxnId={globalTxnId}");
                if (!string.IsNullOrWhiteSpace(subsystemDescription)) url.Append($"&subsystemDescription={subsystemDescription}");
                if (!string.IsNullOrWhiteSpace(trackingInfo)) url.Append($"&trackingInfo={trackingInfo}");
                if (!string.IsNullOrWhiteSpace(userName)) url.Append($"&userName={userName}");
                if (!string.IsNullOrWhiteSpace(activityData)) url.Append($"&activityData={activityData}"); 
                CubLogger.Info("GetActivityDetails: url=" + url);
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info("GetActivityDetails: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }


        // http://localhost:12665/TransitAccount/GetTASubsystemActivities?transitAccountId=4C167FB1-085F-4547-82F7-4C87B693F09F&subsystemId=4C167FB1-085F-4547-82F7-4C87B693F09F
        /// <summary>
        /// This API is used to retrieve activity history that was associated with the a transit account and a subsystem based upon filtering parameters.
        /// 2.8.21 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/activity GET
        /// </summary>
        /// <param name="transitAccountId">Transit Account Id</param>
        /// <param name="subsystemId">Subsystem Id</param>
        /// <param name="customerId">Customer Id - passed in but not used</param>
        /// <param name="contactId">(Optional) The contact Id for which the activity should be shown.</param>
        /// <param name="startDateTime">(Optional) The date and time that should be used to filter the starting date of the range. 
        ///     If not provided then the default should be configurable with a default value of now � 30 days.</param>
        /// <param name="endDateTime">(Optional)  The date and time that should be used to filter the ending date of the range.
        ///     If not provided then the default should be the beginning of next day. 
        ///     The maximum end date should be as number of days from the start date.</param>
        /// <param name="type">(Optional)  An identifier for the type of activity to return in the results.
        ///     The default is to return all activity types if no value is provided.</param>
        /// <param name="activitySubject">(Optional)  An identifier for the subject of activity to return in the results. 
        ///     Activity subject is a logical grouping of activity types. The default is to return all activity subject if no value is provided.</param>
        /// <param name="activityCategory">(Optional)  An identifier for the category of activity to return in the results. 
        ///     Category is used to distinguish between event and audit. The default is to return all activity category if no value is provided.</param>
        /// <param name="channel">(Optional) Used to filter the activities by channel.</param>
        /// <param name="origin">(Optional)  An identifier for the origin of activity to return in the results. 
        ///     Origin tells the component which initiated the action which caused the activities to be generated. The default is to return all activity origin if no value is provided.</param>
        /// <param name="globalTxnId">(Optional)  An identifier for the category of activity to return in the results. 
        ///     Global transaction id is a mechanism to tie multiple method being called as part of a single action. The default is to return all activity category if no value is provided.</param>
        /// <param name="userName">(Optional) Used to filter the activities by the username that performed the activity.
        ///     Allow for partial string with wild card �*�.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �type.asc,channel.desc� indicates sort by type ascending and status descending.
        ///     Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) Used to identify the starting index that should be provided within the page in the search result.
        ///     The default is to start at the first row of data (0) if no value is provided.</param>
        /// <param name="limit">(Optional) Used to identify the number of rows per page in the search result in the response.
        ///     The default is to return 20 rows if no value is provided.</param>
        /// <returns>Transit Account Subsystem Activities</returns>
        public MvcGenericResponse GetTASubsystemActivities(string transitAccountId, string subsystemId,
                                            string customerId, string contactId, string startDateTime, string endDateTime,
                                            string type, string activitySubject, string activityCategory, string channel,
                                            string origin, string globalTxnId, string userName, string crmSessionId,
                                            string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                transitAccountId = formatGuid(transitAccountId);
                subsystemId = formatGuid(subsystemId);
                customerId = formatGuid(customerId);
                contactId = formatGuid(contactId);
                crmSessionId = formatGuid(crmSessionId);


                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISTASubsystemActivityPath(_organizationService).ToString();
                actionURL = string.Format(actionURL, transitAccountId, subsystemId);
                string url = baseURL + actionURL;

                var request = new
                {
                    contactId = contactId,
                    startDateTime = startDateTime,
                    endDateTime = endDateTime,
                    type = type,
                    activityCategory = activityCategory,
                    activitySubject = activitySubject,
                    channel = channel,
                    origin = origin,
                    globalTxnId = globalTxnId,
                    userName = userName,
                    crmSessionId = crmSessionId,
                    sortBy = sortBy,
                    offset = offset,
                    limit = limit
                };

                string requestSerialized = JsonConvert.SerializeObject(request);
                var httpContent = new StringContent(requestSerialized);
                CubLogger.Info($"GetTASubsystemActivities: url={url}   Body={requestSerialized}");
                RestResponse resp = HttpClientPost(url, httpContent);
                CubLogger.Info("GetCustomerActivities: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    CubLogger.Error($"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}");
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }


        /// <summary>
        /// The API to get the bankcard charge payment history for a transitaccount. 
        /// This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// </summary>
        /// <param name="accountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystem">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the transactions are required for, defaults to 7 days ago.  Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.  Required when startDateTime is given.</param>
        /// <param name="dateType">(Optional) Type of date filter to apply on. Possible values are dateOpened and dateSettled. Defaults to dateOpened.</param>
        /// <param name="response">(Optional) Comma separated list of the response IDs, Defaults to all responses.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10.</param>
        /// <returns>Card Charge History JSON response</returns>
        public MvcGenericResponse GetTransitAccountCardChargeHistory(string accountId, string subSystem, string startDateTime, string endDateTime, 
                                                                     string dateType, string status, string response, 
                                                                     string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetTransitAccountBankCardChargeHistoryPath(_organizationService).ToString();
                actionURL = string.Format(actionURL, accountId, subSystem);
                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (!string.IsNullOrWhiteSpace(startDateTime)) url.Append(string.Format("&startDateTime={0}", startDateTime));
                if (!string.IsNullOrWhiteSpace(endDateTime)) url.Append(string.Format("&endDateTime={0}", endDateTime));
                if (!string.IsNullOrWhiteSpace(dateType)) url.Append(string.Format("&dateType={0}", dateType));
                if (!string.IsNullOrWhiteSpace(response)) url.Append(prepareListParamForNIS("response", response));
                if (!string.IsNullOrWhiteSpace(status)) url.Append(prepareListParamForNIS("status", status));
                if (!string.IsNullOrWhiteSpace(sortBy)) url.Append(string.Format("&sortBy={0}", sortBy));
                if (offset.HasValue) url.Append(string.Format("&offset={0}", offset));
                if (limit.HasValue) url.Append(string.Format("&limit={0}", limit));
                CubLogger.Info("GetTransitAccountCardChargeHistory: url=" + url);
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info("GetTransitAccountCardChargeHistory: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// 2.14.4 order/adjustment POST
        /// This API is used to submit (create) an adjustment order for a given registered or unregistered customer.
        /// The order is validated and accepted.Adjustment orders may require approval before being processed, if the order is approved, it is then processed immediately.
        /// If not, it stays in OrderAccepted status until the order is approved or rejected.
        /// When the adjustment is for a registered customer, the customerId in the request is required and if an anonymous email address is provided,
        /// it is ignored and the customer notification preferences are used to send notifications.
        ///This method assumes that the client has validated and provided the appropriate reason codes and operator financially responsible for the adjustment.
        /// </summary>
        /// <param name="customerId">(Conditionally-Required) Unique identifier for the customer. Required for adjustments on One Account and all registered customers.</param>
        /// <param name="unregisteredEmail">(Optional)  If the customer is not registered (i.e. customerId is not provided), the email address to send order notifications.
        /// If customerId is specified, this field will be ignored.
        /// </param>
        /// <param name="clientRefId">(Required)  The client�s unique identifier for sending this notification.
        /// This field will be used for detecting duplicate requests and therefore must be globally unique.
        /// This value should be specified as: "<device-id>:<unique-id>"
        ///</param>
        /// <param name="productLineItems">(Required) Line items to adjust product in the one account or subsystem account.
        /// When other types are are added, this will become Conditionally-Required.
        /// </param>
        /// <param name="reasonCode">(Required) Reason code for the adjustment.</param>
        /// <param name="notes">(Optional)  Notes for the adjustment.</param>
        /// <param name="financiallyResponsibleOperatorId">(Required) Operator impacted by the adjustment financially.</param>
        /// <param name="isApproved">(Optional) Specifies if the order is approved to be processed. Default is true. </param>
        /// <param name="userId">User ID</param>
        /// <returns>Order Adjustment response</returns>
        public MvcGenericResponse PostOrderAdjustment(string customerId, string unregisteredEmail, string clientRefId,
                                                      List<WSAdjustProductLineItem> productLineItems, string reasonCode,
                                                      string notes, int financiallyResponsibleOperatorId, bool? isApproved,
                                                      Guid? userId)
        {
            if (!string.IsNullOrWhiteSpace(customerId)) {
                customerId = customerId.Replace("{", "").Replace("}", "");
            }
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string methodURL = GlobalsCache.NISOrderAdjustmentPath(_organizationService).ToString();
                string url = baseURL + methodURL;
                var request = new
                {
                    customerId = customerId,
                    unregisteredEmail = unregisteredEmail,
                    clientRefId = clientRefId,
                    productLineItems = productLineItems,
                    reasonCode = reasonCode,
                    notes = notes,
                    financiallyResponsibleOperatorId = financiallyResponsibleOperatorId,
                    isApproved = isApproved.HasValue ? isApproved.Value : true
                };
                string requestSerialized = JsonConvert.SerializeObject(request);
                var httpContent = new StringContent(requestSerialized);
                //Removing the ':' from clientRefId because NIS was falling with it
                RestResponse resp = HttpClientPost(url, httpContent, null, clientRefId.Replace(":", ""));
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// 2.15.11 : order/<order-id>/notification/g\ POST
        ///�	startDateTime: the start date time to search for notifications and it is based on when the notification was processed by the system.This value is configurable to allow the number of days in the history to start, but the default is current day minus 30 days.
        ///�	endDateTime: to allow historical filtering on the selected range type, The end date allows the user to limit the range to search.  If not provided, the end date defaults to beginning of next day.The maximum end date should be set to no more than a configurable number of days from the start date.
        ///�	type: to narrow down the results to only show a selected notification type.
        ///�	status: to narrow down the results based on the final status of a processed notification.  Valid values are:
        ///         o All
        ///         o Sent
        ///         o Failed
        ///�	channel: to narrow down the results to show notifications processed for a selected notification channel.
        ///�	recipient: to narrow down the results to show notifications processed for a specific recipient (email address, SMS phone number or mobile device nickname).
        ///�	notificationReference: used to narrow down the customer notifications in the result by the notification reference displayed in the outgoing message.
        ///�	sortBy: sort expression to be applied to the results.
        ///�	offset: to allow for pagination provides the starting point that should be pulled from the results. Defaults to 0.
        ///�limit: to allow for pagination provides the number of records that should be returned per page. Defaults to 20.
        /// it is ignored and the customer notification preferences are used to send notifications.
        /// </summary>
        /// <param name="orderId">(Required) The order the notification is related to.</param>
        /// <param name="startDateTime">Optional)  The date and time that should be used to filter the starting date of the range.  If not provided then the default should be configurable with a default value of now � 30 days.</param>
        ///<param name="endDateTime">(Optional)  The date and time that should be used to filter the ending date of the range.  If not provided then the default should be the beginning of next day.  The maximum end date should be as number of days from the start date.</param>
        ///<param name="type">(Optional)  An identifier for the type of notification to return in the results.  The default is to return all notification types if no value is provided.</param>
        ///<param name="status">(Required)  The final notification status should be returned.  The default is to return all notifications in final status if no value is provided.
        ///     � All
        ///     � Sent
        ///     � Failed</param>
        ///<param name="channel">(Optional)  Used to filter the notification types by channel. 
        ///        Valid values are:
        ///         � Email
        ///         � PushNotification
        ///         � SMS
        ///         These values are not case sensitive.The default is to return all channels if no value is provided.
        ///</param>
        ///<param name="recipient">(Optional)  Used to filter the notification types by recipient (email address, SMS phone number or mobile device nickname). </param>
        ///<param name="notificationReference">(Optional)  Used to filter the notification types by the notification reference displayed in the outgoing message. Allow for partial string with wild card �*�.</param>
        ///<param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �type.asc,status.desc� indicates sort by type ascending and status  descending. Default is descending by startDateTime.</param>
        ///<param name="offset">(Optional) Used to identify the starting index that should be provided within the page in the search result.  The default is to start at the first row of data (0) if no value is provided.</param>
        ///<param name="limit">(Optional) Used to identify the number of rows per page in the search result in the response.  The default is to return 20 rows if no value is provided.</param>
        /// <returns>NIS Response List<WSOrderNotificationInfo> as JSON</returns>
        public MvcGenericResponse GetOrderNotification(string orderId, DateTime? startDateTime, DateTime? endDateTime, string type,string status,
                                                        string channel,string recipient,string notificationReference, string sortBy, int? offset, int? limit)
        {
            
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
               
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = string.Format(GlobalsCache.NISGetOrderNotificationsPath(_organizationService).ToString(),orderId);
                actionURL = string.Format(actionURL, orderId);

                var request = new
                {                   
                    startDateTime = startDateTime,
                    endDateTime = endDateTime,
                    type = type,
                    status = status,
                    channel = channel,
                    recipient = recipient,
                    notificationReference = notificationReference,
                    sortBy = sortBy,
                    offset = offset,
                    limit = limit
                };

                string requestSerialized = JsonConvert.SerializeObject(request);
                var httpContent = new StringContent(requestSerialized);
                RestResponse resp = HttpClientPost(baseURL + actionURL, httpContent);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// 2.13.1. POST notification/type/<notification-type>/resend POST
        /// This API method is used to resend a notification based on the provided notification type and identifier, the specified notification channels, email address and SMS phone. Only notification id and notification type are required. 
        /// If an original notification with the same notification id and notification type is not found, an error is returned. 
        /// If the original notification with the same notification id and notification type is found and notifications channel, email address or SMS phone are not specified, the notification is resent with the values from the original request.
        /// If Email is specified in the notification methods, an email address needs to be provided.
        /// If SMS is specified in the notification methods, SMS phone needs to be provided.
        /// If PushNotification is specified in the notification methods, no other information is required and the currently active push notification tokens for the patron account Id in the original request will be used. 
        /// </summary>
        /// <param name="notificationType">(Required)  An identifier for the type of notification to send.  Notification types are project specific. </param>
        /// <param name="notificationType">(Required)  An identifier for the type of notification to send.  Notification types are project specific. </param>
        /// <param name="clientRefId">(Required)  The client�s unique identifier for sending this notification.
        ///       This field will be used for detecting duplicate requests and therefore must be globally unique.This value should be specified as:
        ///        <device-id>:<unique-id>".
        ///</param>
        /// <param name="notificationId">(Required) The notification identifier for the original notification that was sent. (Note this is NOT the notificationReference. This is the notificationId returned by notification history calls)</param>
        /// <param name="channels">(Optional)  A list of notification channels (Email, SMS, Push) that should be used for resending the notification.</param>
        /// <param name="email">(Optional)  If one of the channels is Email and if a value is provided, this is the email address to be used to resend the notification.</param>
        /// <param name="smsPhone">(Optional)  If one of the channels is SMS and if a value is provided, this is the phone number to be used to resend the notification.</param>
        /// <returns>NIS Response as JSON</returns>
        public MvcGenericResponse ResendNotification(string notificationType, string clientRefId,int notificationId, string channels, string email,
                                                       string smsPhone)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetResendNotificationsPath(_organizationService).ToString();
                actionURL = string.Format(baseURL + actionURL, notificationType);

                var request = new
                {
                    notificationId = notificationId,
                    channels = new string[] { channels },
                    email = email,
                    smsPhone = smsPhone,
                    clientRefId = clientRefId.Replace(":", "")
                };

                string requestSerialized = JsonConvert.SerializeObject(request);
                var httpContent = new StringContent(requestSerialized);
                RestResponse resp = HttpClientPost(actionURL, httpContent, null, clientRefId.Replace(":", ""));
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }
            return wsresp;
        }



        /// <summary>
        /// 2.15.12 order/<order-id>/paymentreceipt POST
        /// This API is used to generate and send the payment receipt for a given order. This is to be used when a payment receipt was not sent during initial order processing due to lack of an email address (unregistered customers), customer was opted out to receive payment receipts at the time of order processing or a system malfunction.
        /// Besides the order id, the request allows for an email address to be provided.The email is only used if the order was for an unregistered customer.In this case the order gets updated with the email address for future
        /// notifications. If the order was for a registered customer and an email was provided, the email is ignored and the customer primary contact email address is used.
        /// Payment receipts will be generated only if the order type supports payment receipts (Sale, Autoload, Debt Collection) and if payment was successful.
        /// If the customer needs to receive a notification with the current order status, another method is to be used.
        /// This method returns an empty response body in case of success or failure.The response header contains the result.
        /// </summary>
        /// <param name="orderid">(Required) The order the payment receipt should be created.</param>
        /// <param name="unregisteredEmail">(Conditionally-Required)  If order is for an unregistered customer, the email address to send order payment receipt.
        ///                                 If the order is for a registered customer, this field will be ignored and the notification will be sent to the registered contact information.
        ///                                 Required if order is for unregistered customer.
        ///</param>
        /// <returns>NIS Response  as JSON</returns>
        public MvcGenericResponse OrderPaymentReceipt(string orderid, string unregisteredEmail)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetOrderPaymentReceiptPath(_organizationService).ToString();
                actionURL = string.Format(baseURL + actionURL, orderid);

                var request = new
                {
                    unregisteredEmail = unregisteredEmail
                };

                string requestSerialized = JsonConvert.SerializeObject(request);
                var httpContent = new StringContent(requestSerialized);
                RestResponse resp = HttpClientPost(actionURL, httpContent);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorKey = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// 2.15.15 order/travelcorrection POST
        /// This API is used to submit (create) a travel correction order for a given registered or unregistered customer. 
        /// A travel correction order may include multiple line items and supports tap/trip corrections and voids. 
        ///The order is validated before it is accepted.Travel correction orders may require approval before being processed.
        ///If the order is approved, all the trip corrections and trip voids are immediately processed.
        ///The response contains information about the order and processing results.
        /// When the order is for a registered customer, the customerId in the request is required and if an anonymous email address is provided, 
        /// it is ignored and the customer notification preferences are used to send notifications.
        /// </summary>
        /// <param name="request">Request</param>
        /// <param name="userId">User ID</param>
        /// <returns>NIS Response as JSON</returns>
        public MvcGenericResponse PostTravelCorrection(WSTripCorrection request, Guid? userId)
        {
            if (!string.IsNullOrWhiteSpace(request.customerId))
            {
                request.customerId = request.customerId.Replace("{", "").Replace("}", "").ToUpper();
            }
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string methodURL = GlobalsCache.NISOrderTravelCorrectionPath(_organizationService).ToString();
                string url = baseURL + methodURL;
                string requestSerialized = JsonConvert.SerializeObject(request);
                // Hacking because operator is a C# reserved word and the WSTripCorrection can not have operator
                // So Operator is been replaced by operator after the cast to a serialized JSON.
                //requestSerialized = requestSerialized.Replace("Operator", "operator");
                var httpContent = new StringContent(requestSerialized);
                //Removing the ':' from clientRefId because NIS was falling with it
                RestResponse resp = HttpClientPost(url, httpContent, null, request.clientRefId.Replace(":", ""));
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// 2.15.7 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/bankcardcharge/<payment-id>/aggregation GET
        /// The endpoint is to get the aggregated details such as bankcard charge details, journal entries and the trip details.
        /// This information is all clubbed together to help client get all the information they need to help answer questions related to 
        /// charge, payment and trip.
        /// </summary>
        /// <param name="accountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystem">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="paymentId">(Required)  Unique id for the bankcard charge payment transaction.</param>
        /// <returns></returns>
        public MvcGenericResponse getBankChargeAggregationDetails(string accountId, string subSystem, string paymentId)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetTransitBankChargeAggregationDetailsPath(_organizationService).ToString();
                actionURL = string.Format(actionURL, accountId, subSystem, paymentId);
                string url = baseURL + actionURL;
                CubLogger.Info("getBankChargeAggregationDetails: url=" + url);
                RestResponse resp = HttpClientGet(url, null);
                CubLogger.Info("getBankChargeAggregationDetails: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// This API is used to get all configured order data types such as list of order types, list of order statuses and payment statuses.
        /// Optional filters indicate which order data types should be returned.
        /// </summary>
        /// <param name="returnOrderTypes">(Optional)  Flag to indicate whether to retrieve order types Default value is �true�. </param>
        /// <param name="returnOrderStatuses">(Optional)  Flag to indicate whether to retrieve order statuses Default value is �true. </param>
        /// <param name="returnPaymentStatuses">(Optional)  Flag to indicate whether to retrieve order payment types Default value is �true. </param>
        /// <param name="userId">User ID</param>
        /// <returns>Order data types</returns>
        public MvcGenericResponse GetOrderConfigDataTypes(bool? returnOrderTypes, bool? returnOrderStatuses, bool? returnPaymentStatuses)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetOrderConfigDataTypesPath(_organizationService).ToString();
                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (returnOrderTypes.HasValue) url.Append(string.Format("&returnOrderTypes={0}", returnOrderTypes));
                if (returnOrderStatuses.HasValue) url.Append(string.Format("&returnOrderStatuses={0}", returnOrderStatuses));
                if (returnPaymentStatuses.HasValue) url.Append(string.Format("&returnPaymentStatuses={0}", returnPaymentStatuses));
                CubLogger.Info(url.ToString());
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info(resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;

        }

        /// <summary>
        /// This API returns a mapping ofconfigured reason code types and reason codes that can be used by customer service.
        /// The mapping defines the type of codes and then set of reason codes associated with that type for use within the CSR interface.
        /// Depending on the reason code type, the mapping might include a list of financially responsible operators that are associated with
        /// specific reason codes.  If the reason codes are to be used for financial transactions, minimum and maximum amounts might be included.
        /// This method accepts optional filters by:
        ///  - typeId: to narrow down the results to only show a selected reason code type.
        ///  - active: to narrow down the results to only those reason codes that are active.
        /// </summary>
        /// <param name="typeId">
        /// (Optional) An identifier for the reason code types to return in the results.
        /// The default is to return all reason code types if no value is provided.
        /// </param>
        /// <param name="active">
        /// (Optional)  An identifier for the active indicator of reason codes to return in the results.
        /// The default is to return all active reason codes if no value is provided.
        /// </param>
        /// <returns>Reason Codes</returns>
        public MvcGenericResponse GetCustomerServiceReasonCodes(int? typeId, bool? active)
        {
            string CUSTOMER_SERVICE_REASON_CODES_CACHE_NAME = "CUSTOMER_SERVICE_REASON_CODES";
            MvcGenericResponse wsresp = null;
            try
            {
                /*
                 * Trying to get from the cache first
                 */
                wsresp = (MvcGenericResponse)GlobalsCache.GetFromCache(CUSTOMER_SERVICE_REASON_CODES_CACHE_NAME);
                if (wsresp != null)
                {
                    return wsresp;
                } else
                {
                    wsresp = new MvcGenericResponse();
                }

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetCustomerServiceReasonCodesPath(_organizationService).ToString();
                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                if (typeId.HasValue) url.Append(string.Format("&typeId={0}", typeId));
                if (active.HasValue) url.Append(string.Format("&active={0}", active));
                CubLogger.Info(url.ToString());
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info(resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            GlobalsCache.AddToCache(CUSTOMER_SERVICE_REASON_CODES_CACHE_NAME, wsresp);
            return wsresp;
        }

        /// <summary>
        /// This API is used to retrieve a list of notifications based on a filtering criteria. 
        /// This method is to be used when customer, order or subsystem account is not known for the notifications being searched. 
        /// For cases when a customer, subsystem account or order is known, the respective Customer or Order methods should be used. 
        /// </summary>
        /// <param name="recipient">to narrow down the results to show  notifications processed for a specific recipient (email address, SMS phone number or mobile device nickname).</param>
        /// <param name="notificationReference">used to narrow down the customer notifications in the result by the notification reference displayed in the outgoing message.</param>
        /// <param name="globalTxnId">An identifier of the original request and all related subsequent requests in the transaction that triggered the notification. 
        ///     This filter can be used to relate the notification to other notifications or activities. This is an internal ID not usually available for input in the user interface. 
        ///     It is passed internally based on the context from where this method is being called.</param>
        /// <param name="startDateTime">the start date time to search for notifications and it is based on when the notification was processed by the system. 
        ///     This value is configurable to allow the number of days in the history to start, but the default is current day minus 30 days.</param>
        /// <param name="endDateTime">to allow historical filtering on the selected range type,The end date allows the user to limit the range to search.  
        ///     If not provided, the end date defaults to beginning of next day.  The maximum end date should be set to no more than a configurable number of days from the start date</param>
        /// <param name="type">to narrow down the results to only show a selected notification type.</param>
        /// <param name="status">to narrow down the results based on the final status of a processed notification.  Valid values are:
        ///     o All
        ///     o Sent
        ///     o Failed
        /// </param>
        /// <param name="channel">to narrow down the results to show  notifications processed for a selected notification channel.</param>
        /// <param name="sortBy">sort expression to be applied to the results.</param>
        /// <param name="offset">to allow for pagination provides the starting point that should be pulled from the results. Defaults to 0.</param>
        /// <param name="limit">to allow for pagination provides the number of records that should be returned per page. Defaults to 20.</param>
        /// <returns></returns>
        public MvcGenericResponse GetNotifications(string recipient, string notificationReference, string globalTxnId, string startDateTime, string endDateTime,
                                          string type, string status, string channel, string sortBy, int? offset, int? limit)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                globalTxnId = formatGuid(globalTxnId);

                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetNotificationsPath(_organizationService).ToString();

                string url = baseURL + actionURL;

                var request = new
                {
                    recipient = recipient,
                    notificationReference = notificationReference,
                    globalTxnId = globalTxnId,
                    startDateTime = startDateTime,
                    endDateTime = endDateTime,
                    type = type,
                    status = status,
                    channel = channel,
                    sortBy = sortBy,
                    offset = offset,
                    limit = limit
                };

                string requestSerialized = JsonConvert.SerializeObject(request);
                var httpContent = new StringContent(requestSerialized);
                CubLogger.Info($"GetNotifications: url={url}   Body={requestSerialized}");
                RestResponse resp = HttpClientPost(url, httpContent);
                CubLogger.Info("GetNotifications: result=" + resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (NISException nisEx)
            {
                CubLogger.Error(string.Empty, nisEx);
                wsresp.Header = nisEx.Hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// 2.13.2 notification/<notification-id> GET
        /// This API is used to retrieve notification details for a given notification.  
        /// </summary>
        /// <param name="notificationId">(Required)  The unique identifier for the notification.</param>
        /// <returns></returns>
        public MvcGenericResponse GetNotificationDetails(string notificationId)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                                
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetNotificationDetailsPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, notificationId);
                CubLogger.Info(url.ToString());
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info(resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }

            }
            catch (NISException nisEx)
            {
                CubLogger.Error(string.Empty, nisEx);
                wsresp.Header = nisEx.Hdr;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// 2.18.1 payment/repository/nonce GET
        /// This API gets a repository nonce for handling a credit card in a PCI compliant manner.
        /// The API returns a unique value that acts as a security token for repository requests. 
        /// </summary>
        /// <returns>Nonce</returns>
        public MvcGenericResponse GetPaymentRepositoryNonce()
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISGetPaymentRepositoryNoncePath(_organizationService).ToString();
                StringBuilder url = new StringBuilder(baseURL + actionURL + "?");
                CubLogger.Info(url.ToString());
                RestResponse resp = HttpClientGet(url.ToString());
                CubLogger.Info(resp.hdr.result);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody)) {
                        /*
                         * Hacking because the nonce as number was been trucked by javaScript
                         * Addingint the " around the returned nonce
                         */
                        wsresp.Body = resp.responseBody.Replace(":", ":\"").Replace("}", "\"}");
                    }
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }
    }
}
