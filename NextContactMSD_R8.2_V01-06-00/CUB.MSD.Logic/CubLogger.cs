﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using NLog.Targets;
using NLog.Config;
using NLog;
using Microsoft.Xrm.Sdk;
using NLog.Targets.Wrappers;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Caching;
using CUB.MSD.Model;
using System.Linq;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// This class offer Aync and Database logging capability.
    /// This is a singleto class and requires the CubLogger.Init(IOrganizationService) method to be called before any other method
    /// The database string connection has to be defined as a MSD Global called: AppInfo -> LoggerDatabaseStringConnection
    /// </summary>
    public static class CubLogger
    {
        /// <summary>
        /// NLog logger
        /// </summary>
        private static Logger _Logger;

        /// <summary>
        /// Database string connection
        /// </summary>
        private static string _DatabaseConnectionString;

        private static LogLevel _LogLevel;

        /// <summary>
        /// Raise Exception indicator
        /// </summary>
        private static bool _RaiseException;

        /// <summary>
        /// 
        /// </summary>
        private static MemoryCache memoryCache = MemoryCache.Default;

        private const string EVENT_VIEWER_SOURCE_NAME = "Cubic.MSD.AsyncDbLogger";
        private const string EVENT_VIEWER_SECTION_NAME = "Application";

        /// <summary>
        /// Initialize the Logger
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <param name="raiseException">The raise Exception should only be use for test/debug NEVER in PROD code</param>
        public static Guid? Init(IOrganizationService service, bool raiseException = false)
        {
            try
            {
                _RaiseException = raiseException;
                if (_Logger == null)
                {
                    _DatabaseConnectionString = GetLogDatabaseConnectionString(service);
                    _LogLevel = GetLogLevel(service);
                    InitLogger();
                }
                var correlationId = Guid.NewGuid();
                SetCorrelationID(correlationId);
                return correlationId;
            }
            catch (Exception ex)
            {
                if (_RaiseException)
                {
                    string msg = string.Format("Error Initializing CubLogger: Error = {0} \nStackTrace = {1}",
                                               ex.Message, ex.StackTrace);
                    WriteToEventLog(msg, EventLogEntryType.Error);
                    throw;
                }
                return null;
            }
        }

        /// <summary>
        /// Get the Log level
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <returns>NLog log level</returns>
        private static LogLevel GetLogLevel(IOrganizationService service)
        {
            string level = GetKeyFromCache(service, "AppInfo", "LoggerLevel ").ToString().ToLower();
            if (string.Equals("debug", level, StringComparison.CurrentCultureIgnoreCase))
                return LogLevel.Debug;
            else if (string.Equals("info", level, StringComparison.CurrentCultureIgnoreCase))
                return LogLevel.Info;
            else if (string.Equals("warn", level, StringComparison.CurrentCultureIgnoreCase))
                return LogLevel.Warn;
            else return LogLevel.Error;
        }

        /// <summary>
        /// Get the logging database connection string
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <returns>Logging database connection string</returns>
        private static string GetLogDatabaseConnectionString(IOrganizationService service)
        {
            return GetKeyFromCache(service, "AppInfo", "LoggerDatabaseStringConnection ").ToString();
        }

        /// <summary>
        /// Get Global key value from Cache
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <param name="keyName">Key Name</param>
        /// <param name="attributeName">Attribute Name</param>
        /// <returns>Global Key value</returns>
        public static object GetKeyFromCache(IOrganizationService service, string keyName, string attributeName)
        {
            string cacheKeyName = string.Format("{0}.{1}", keyName, attributeName);
            if (memoryCache.Get(cacheKeyName) == null)
            {
                string attribValue = GetAttributeValue(service, keyName, attributeName);
                if (attribValue == null)
                {
                    throw new NullReferenceException(CUBConstants.Exceptions.UNABLE_TO_GET_GLOBALS_VALUE);
                }
                memoryCache.Add(cacheKeyName, attribValue, new DateTimeOffset(DateTime.Now.AddHours(1)));
            }
            return memoryCache.Get(cacheKeyName);
        }

        /// <summary>
        /// Get attribute value
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <param name="name">Global key name</param>
        /// <param name="attribute">Global attribute name</param>
        /// <returns></returns>
        public static string GetAttributeValue(IOrganizationService service, string name, string attribute)
        {
            using (var osc = new CUBOrganizationServiceContext(service))
            {

                return (from g in osc.cub_GlobalsSet
                        join d in osc.cub_GlobalDetailsSet
                        on g.cub_GlobalsId.Value equals d.cub_GlobalId.Id
                        where g.cub_Name.Equals(name) &&
                              d.cub_Name.Equals(attribute) &&
                              g.cub_Enabled.Value.Equals(true) &&
                              d.cub_Enabled.Value.Equals(true)
                        select d.cub_AttributeValue).FirstOrDefault();
            }
        }

        /// <summary>
        /// Set correlation ID
        /// </summary>
        /// <param name="correlationId">Correlation ID</param>
        public static void SetCorrelationID(Guid correlationId)
        {
            CheckLogger();
            Trace.CorrelationManager.ActivityId = correlationId;
        }

        /// <summary>
        /// Log Info with string formating (interpolation) parameters
        /// </summary>
        /// <param name="message">Log message</param>
        /// <param name="messageParameters">message interpolation parameters</param>
        public static void InfoFormat(string message, params object[] messageParameters)
        {
            WriteLog(LogLevel.Info, message, null,  null, null, null, null, null, messageParameters);
        }

        /// <summary>
        /// Log Info with data
        /// </summary>
        /// <param name="message">Log message</param>
        /// <param name="data">Log data as JSON</param>
        public static void Info(string message, string data = null)
        {
            WriteLog(LogLevel.Info, message, data, null, null);
        }

        /// <summary>
        /// Log Info with request and response
        /// </summary>
        /// <param name="message">Log message</param>
        /// <param name="request">Log request</param>
        /// <param name="requestTime">Log request time (UTC)</param>
        /// <param name="response">Log response</param>
        /// <param name="responseTime">Log response time (UTC)</param>
        public static void Info(string message,
                                string request, DateTime? requestTime,
                                string response = null, DateTime? responseTime = null)
        {
            //if (!responseTime.HasValue)
            //{
            //    responseTime = DateTime.UtcNow;
            //}
            //if (!requestTime.HasValue)
            //{
            //    requestTime = DateTime.UtcNow;
            //}
            WriteLog(LogLevel.Info, message, null, null, request, requestTime, response, responseTime, null);
        }

        /// <summary>
        /// Log Debug with string formating (interpolation) parameters
        /// </summary>
        /// <param name="message">Log message</param>
        /// <param name="messageParameters">message interpolation parameters</param>
        public static void DebugFormat(string message, params object[] messageParameters)
        {
            WriteLog(LogLevel.Debug, message, null, null, null, null, null, null, messageParameters);
        }

        /// <summary>
        /// Log Debug with data
        /// </summary>
        /// <param name="message">Log message</param>
        /// <param name="data">Log data as JSON</param>
        public static void Debug(string message, string data = null)
        {
            WriteLog(LogLevel.Debug, message, data, null, null);
        }

        /// <summary>
        /// Log Waning with string formating (interpolation) parameters
        /// </summary>
        /// <param name="message">Log message</param>
        /// <param name="messageParameters">message interpolation parameters</param>
        public static void WarnFormat(string message, params object[] messageParameters)
        {
            WriteLog(LogLevel.Warn, message, null, null, null, null, null, null, messageParameters);
        }

        /// <summary>
        /// Log Warning with data
        /// </summary>
        /// <param name="message">Log message</param>
        /// <param name="data">Log data as JSON</param>
        public static void Warn(string message, string data = null)
        {
            WriteLog(LogLevel.Warn, message, data, null, null);
        }

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="message">Log error message</param>
        /// <param name="exception">Exception</param>
        /// <param name="data">Log error data</param>
        public static void Error(string message, Exception exception = null, string data = null)
        {
            WriteLog(LogLevel.Error, message, data, exception, null);
        }

        #region Private Methods

        /// <summary>
        /// Check if the class is initialized
        /// </summary>
        private static void CheckLogger()
        {
            if (_Logger == null)
            {
                throw new Exception("CubLogger.Init(IOrganizationService) method has to be called prior to Log method");
            }
        }

        /// <summary>
        /// Write Log
        /// </summary>
        /// <param name="level">Level</param>
        /// <param name="message">Message</param>
        /// <param name="data">Data</param>
        /// <param name="exception">Exception</param>
        /// <param name="request">Request</param>
        /// <param name="requestTime">Request Time</param>
        /// <param name="response">Response</param>
        /// <param name="responseTime">Response Time</param>
        /// <param name="messageParameters">Message interpoloation parameters</param>
        private static void WriteLog(LogLevel level, string message, string data,
                                     Exception exception = null,
                                     string request = null, DateTime? requestTime = null,
                                     string response = null, DateTime? responseTime = null,
                                     params object[] messageParameters)
        {
            try
            {
                CheckLogger();
                if (string.IsNullOrWhiteSpace(message) && exception != null)
                    message = exception.Message;
                LogEventInfo log = new LogEventInfo(level, _Logger.Name, null, message, messageParameters, exception);
                AddData(data, log);
                AddClassMethodNames(log);
                AddRequestResponse(request, requestTime, response, responseTime, log);
                _Logger.Log(log);
            }
            catch(Exception ex)
            {
                if (_RaiseException)
                {
                    string msg = string.Format("Error logging using CubLogger: Error = {0} \nStackTrace = {1}",
                                               ex.Message, ex.StackTrace);
                    WriteToEventLog(msg, EventLogEntryType.Error);
                    throw;
                }
            }
        }

        /// <summary>
        /// Add the caller Class and Method names to the log entry
        /// </summary>
        /// <param name="log">Log event info</param>
        private static void AddClassMethodNames(LogEventInfo log)
        {
            MethodBase method = null;
            try
            {
                var stacktrace = new StackTrace();
                method = stacktrace.GetFrame(3).GetMethod();
                log.Properties["Class"] = method.ReflectedType.FullName;
                log.Properties["Method"] = method.Name;
            }
            /*
             * Just in case it is not possible to retrieve the class/method names
             * i.e anonymous class/method
             */
            catch (Exception)
            {
                if (_RaiseException)
                    throw;
            }
        }

        /// <summary>
        /// Add the request/response data to the log entry
        /// </summary>
        /// <param name="request">Request</param>
        /// <param name="requestTime">Request time</param>
        /// <param name="response">Response</param>
        /// <param name="responseTime">Response time</param>
        /// <param name="logEventInfo">Log event info</param>
        private static void AddRequestResponse(string request, DateTime? requestTime, string response, DateTime? responseTime, LogEventInfo logEventInfo)
        {
            if (request != null)
            {
                logEventInfo.Properties["Request"] = request;
            }
            if (requestTime.HasValue)
            {
                logEventInfo.Properties["RequestTime"] = requestTime.Value.ToString("yyyy-MM-dd hh:mm:ss.ffffff"); ;
            }
            if (response != null)
            {
                logEventInfo.Properties["Response"] = response;
            }
            if (responseTime.HasValue)
            {
                logEventInfo.Properties["ResponseTime"] = responseTime.Value.ToString("yyyy-MM-dd hh:mm:ss.ffffff");
            }
        }

        /// <summary>
        /// Add data to the log entry
        /// </summary>
        /// <param name="data">Log data</param>
        /// <param name="log">Log event info</param>
        private static void AddData(string data, LogEventInfo log)
        {
            if (!string.IsNullOrWhiteSpace(data))
                log.Properties["LogData"] = data;
        }

        /// <summary>
        /// Initialize the Logger
        /// </summary>
        private static void InitLogger()
        {
            var config = new LoggingConfiguration();
            var databaseTarget = GetDatabaseTarget();
            SetupRaiseException(config, databaseTarget);
            LogManager.Configuration = config;
            LogManager.ThrowExceptions = _RaiseException;
            LogManager.ThrowConfigExceptions = _RaiseException;
            _Logger = LogManager.GetLogger("CubLogger");
        }

        /// <summary>
        /// Setup the Target based on the RaiseException attribute.
        /// If Raise Exception is true the logging will be Async so it is possible to catch exception otherwise it will be Sync
        /// The raise Exception should only be use for test/debug NEVER in PROD code
        /// </summary>
        /// <param name="config">Logging Configuration</param>
        /// <param name="databaseTarget">Database target</param>
        private static void SetupRaiseException(LoggingConfiguration config, DatabaseTarget databaseTarget)
        {
            if (_RaiseException)
            {
                config.AddTarget(databaseTarget);
                var rule = new LoggingRule("*", _LogLevel, databaseTarget);
                config.LoggingRules.Add(rule);
            }
            else
            {
                var AsyncTarget = new AsyncTargetWrapper("AsyncTarget", databaseTarget);
                config.AddTarget(AsyncTarget);
                var rule = new LoggingRule("*", _LogLevel, AsyncTarget);
                config.LoggingRules.Add(rule);
            }
        }

        /// <summary>
        /// Create the NLog Database Target
        /// </summary>
        /// <returns>Database target</returns>
        private static DatabaseTarget GetDatabaseTarget()
        {
            var target = new DatabaseTarget("Database");
            target.KeepConnection = true;
            target.DBProvider = "System.Data.SqlClient";
            target.ConnectionString = _DatabaseConnectionString;
            target.CommandText =
                "exec dbo.InsertLog @date, @level, @correlation_id, @machine_name, @user, @Class, @method, @message, @data, @exception, @request, @response, @request_time, @response_time";
            target.Parameters.Add(new DatabaseParameterInfo("@date", "${longdate}"));
            target.Parameters.Add(new DatabaseParameterInfo("@level", "${level}"));
            target.Parameters.Add(new DatabaseParameterInfo("@correlation_id", "${activityid}"));
            target.Parameters.Add(new DatabaseParameterInfo("@machine_name", "${machinename}"));
            target.Parameters.Add(new DatabaseParameterInfo("@user", "${windows-identity:userName=true:domain=true}"));
            target.Parameters.Add(new DatabaseParameterInfo("@class", "${event-properties:Class}"));
            target.Parameters.Add(new DatabaseParameterInfo("@method", "${event-properties:Method}"));
            target.Parameters.Add(new DatabaseParameterInfo("@message", "${message}"));
            target.Parameters.Add(new DatabaseParameterInfo("@data", "${event-properties:LogData}"));
            target.Parameters.Add(new DatabaseParameterInfo("@exception", "${exception:format=toString,Data:maxInnerExceptionLevel=10}"));
            target.Parameters.Add(new DatabaseParameterInfo("@request", "${event-properties:Request}"));
            target.Parameters.Add(new DatabaseParameterInfo("@response", "${event-properties:Response}"));
            target.Parameters.Add(new DatabaseParameterInfo("@request_time", "${event-properties:RequestTime}"));
            target.Parameters.Add(new DatabaseParameterInfo("@response_time", "${event-properties:ResponseTime}"));
            return target;
        }

        /// <summary>
        /// Write a log entry to Windows Event Viewer
        /// </summary>
        /// <param name="message"></param>
        /// <param name="entryType"></param>
        public static void WriteToEventLog(string message, EventLogEntryType entryType = EventLogEntryType.Information)
        {
            try
            {
                if (!EventLog.SourceExists(EVENT_VIEWER_SOURCE_NAME))
                {
                    EventLog.CreateEventSource(EVENT_VIEWER_SOURCE_NAME, EVENT_VIEWER_SECTION_NAME);
                }
                EventLog.WriteEntry(EVENT_VIEWER_SOURCE_NAME, message, entryType);
            }
            catch
            {
            }
        }
        #endregion
    }
}
