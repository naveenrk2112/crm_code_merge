/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// CaseSearchReturnResult
    /// </summary>
    public class CaseSearchReturnResult
    {
        public string caseId { get; set; }
        public string caseNumber { get; set; }
        public string caseTitle { get; set; }

        public string customerName { get; set; }

        public string customerId { get; set; }

        public string contactName { get; set; }
        public string contactId { get; set; }
        public string status { get; set; }

        public string caseOrigin { get; set; }

        public string priority { get; set; }

        public DateTime createdOn { get; set; }

    }
    /// <summary>
    /// Case Logic
    /// </summary>
    public class CaseLogic : LogicBase
    {
        IOrganizationService _service;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public CaseLogic(IOrganizationService service) : base(service)
        {
            _service = service;
        }

        /// <summary>
        /// seacrh the sytem for cases (incidents) by case Id (tikcet number
        /// </summary>
        /// <param name="caseId">Incident ticket number</param>
        /// <returns></returns>
        public virtual EntityCollection SearchByCaseId(string caseId)
        {

            //If not case Id parameter is supplied, return null
            if (string.IsNullOrEmpty(caseId))
                return null;

            try {

                //trim imput parameter
                caseId = caseId.Trim();

                var queryExpression = new QueryExpression(Incident.EntityLogicalName);

                queryExpression.NoLock = true;
                queryExpression.Distinct = true;
                queryExpression.ColumnSet = new ColumnSet(Incident.ticketnumberAttribute,
                                                          Incident.incidentidAttribute, 
                                                          Incident.titleAttribute,
                                                          Incident.prioritycodeAttribute, 
                                                          Incident.caseorigincodeAttribute, 
                                                          Incident.statuscodeAttribute, 
                                                          Incident.createdonAttribute, 
                                                          Incident.customeridAttribute,
                                                          Incident.accountidAttribute,
                                                          Incident.contactidAttribute);
                //Adding Customer (Account) to the resultset
                queryExpression.LinkEntities.Add(new LinkEntity(Incident.EntityLogicalName, 
                                                                Account.EntityLogicalName, 
                                                                Incident.customeridAttribute, 
                                                                Account.accountidAttribute, 
                                                                JoinOperator.LeftOuter));
                queryExpression.LinkEntities[0].Columns.AddColumns(Account.accountidAttribute, 
                                                                   Account.nameAttribute, 
                                                                   Account.cub_accounttypeidAttribute);
                queryExpression.LinkEntities[0].EntityAlias = "customer";
                queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Account.nameAttribute, OrderType.Ascending));
                //Add account(customer) type to the resultset
                queryExpression.LinkEntities[0].LinkEntities.Add(new LinkEntity(Account.EntityLogicalName, 
                                                                                cub_AccountType.EntityLogicalName, 
                                                                                Account.cub_accounttypeidAttribute, 
                                                                                cub_AccountType.cub_accounttypeidAttribute, 
                                                                                JoinOperator.LeftOuter));
                queryExpression.LinkEntities[0].LinkEntities[0].Columns.AddColumns(cub_AccountType.cub_accountnameAttribute);
                queryExpression.LinkEntities[0].LinkEntities[0].EntityAlias = "accountType";
                //Adding Customer (Contact) to the resultset
                //queryExpression.LinkEntities.Add(new LinkEntity(Incident.EntityLogicalName,
                //                                                Contact.EntityLogicalName,
                //                                                Incident.customeridAttribute,
                //                                                Contact.contactidAttribute,
                //                                                JoinOperator.LeftOuter));
                //queryExpression.LinkEntities[1].Columns.AddColumns(Contact.contactidAttribute,
                //                                                   Contact.fullnameAttribute,
                //                                                   Contact.cub_contacttypeidAttribute);
                //queryExpression.LinkEntities[1].EntityAlias = "contact";
                //queryExpression.LinkEntities[1].Orders.Add(new OrderExpression(Contact.fullnameAttribute, OrderType.Ascending));
                // Adding the Case Order and Criteria
                queryExpression.Orders.Add(new OrderExpression(Incident.createdonAttribute, OrderType.Descending));
                queryExpression.Criteria.AddCondition(Incident.ticketnumberAttribute, ConditionOperator.Equal, caseId);
                return this._organizationService.RetrieveMultiple(queryExpression);
            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (FaultException<OrganizationServiceFault>)
            {
                //To-DO log 
                throw;
            }
        }

        /// <summary>
        /// Cast Entity Collection to String
        /// </summary>
        /// <param name="entCol">Entity Collection</param>
        /// <returns>Entity Collection as string</returns>
        public virtual string CastEntityCollectionToString (EntityCollection entCol)
        {
            if (entCol == null || entCol.Entities.Count < 1)
                return null;

            List<CaseSearchReturnResult> ret = new List<CaseSearchReturnResult>();
            CaseSearchReturnResult c;

            foreach (Entity e in entCol.Entities)
            {
                c = new CaseSearchReturnResult();

                if (e.Attributes.Contains(Incident.incidentidAttribute)
                    && e[Incident.incidentidAttribute] != null)
                {
                    c.caseId = Convert.ToString(e[Incident.incidentidAttribute]);

                }

                if (e.Attributes.Contains(Incident.ticketnumberAttribute)
                    && e[Incident.ticketnumberAttribute] != null)
                {
                    c.caseNumber = Convert.ToString(e[Incident.ticketnumberAttribute]);

                }

                if (e.Attributes.Contains(Incident.titleAttribute)
                   && e[Incident.titleAttribute] != null)
                {
                    c.caseTitle = Convert.ToString(e[Incident.titleAttribute]);

                }

                if (e.Attributes.Contains(Incident.prioritycodeAttribute)
                    && e[Incident.prioritycodeAttribute] != null)
                {
                    c.priority = Convert.ToString(e.FormattedValues[Incident.prioritycodeAttribute]);

                }

                if (e.Attributes.Contains(Incident.caseorigincodeAttribute)
                     && e[Incident.caseorigincodeAttribute] != null)
                {
                    c.caseOrigin = Convert.ToString(e.FormattedValues[Incident.caseorigincodeAttribute]);

                }

                if (e.Attributes.Contains(Incident.statuscodeAttribute)
                     && e[Incident.statuscodeAttribute] != null)
                {
                    c.status = Convert.ToString(e.FormattedValues[Incident.statuscodeAttribute]);

                }

                if (e.Attributes.Contains(Incident.createdonAttribute)
                    && e[Incident.createdonAttribute] != null)
                {
                    c.createdOn = Convert.ToDateTime(e[Incident.createdonAttribute]).ToLocalTime();

                }

                if (e.Attributes.Contains("customer.accountid")
                   && e["customer.accountid"] != null)
                {
                    if (e["customer.accountid"].GetType() == typeof(AliasedValue))
                        c.customerId = Convert.ToString(((AliasedValue)(e["customer.accountid"])).Value);
                  

                }

                if (e.Attributes.Contains("customer.name")
                   && e["customer.name"] != null)
                {
                    if (e["customer.name"].GetType() == typeof(AliasedValue))
                        c.customerName = Convert.ToString(((AliasedValue)(e["customer.name"])).Value);
                   
                }
                if (e.Attributes.Contains(Incident.customeridAttribute))
                {
                    EntityReference contactRef = (EntityReference)e[Incident.customeridAttribute];
                    if (contactRef.LogicalName.Equals(Contact.EntityLogicalName))
                    {
                        c.contactId = contactRef.Id.ToString();
                        c.contactName = contactRef.Name;
                    }
                }

                ret.Add(c);
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(ret,Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Create a case
        /// </summary>
        /// <param name="feedbackType">feedbackType</param>
        /// <param name="feedbackMessage">feedbackMessage</param>
        /// <param name="customerId">customerId</param>
        /// <param name="contactId">contactId</param>
        /// <param name="unregisteredEmail">unregisteredEmail</param>
        /// <param name="firstName">firstName</param>
        /// <param name="lastName">lastName</param>
        /// <param name="phoneType">phoneType</param>
        /// <param name="phoneNumber">phoneNumber</param>
        /// <returns>JSON string with Case Number and Case Id</returns>
        public string CreateCase(string feedbackType, string feedbackMessage, string customerId, string contactId, 
                                 string unregisteredEmail, string firstName, string lastName, string phoneType, string phoneNumber, 
                                 string referenceType, string referenceValue, string channel)
        {
            Incident incident = new Incident();
            /* Separating the feedbackType words on Uppercase letter: "GeneralFeedback" into ['General', 'Feedback']*/
            string[] feedbackTypeWords = Regex.Split(feedbackType, @"(?<!^)(?=[A-Z])");
            /* Combining the feedbackType words adding one space between each word ['General', 'Feedback'] into "General Feedback" */
            feedbackType = String.Join(" ", feedbackTypeWords);
            string customerCommunicationCaseSubjectID = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.AppInfo, "CustomerCommunicationCaseSubjectID").ToString();
            Guid? subjectId = (from s in CubOrgSvc.SubjectSet
                               where s.Title == feedbackType &&
                                     s.ParentSubject == new EntityReference(Incident.EntityLogicalName, new Guid(customerCommunicationCaseSubjectID))
                               select s.Id).SingleOrDefault();
            if ((!subjectId.HasValue) || subjectId.Equals(Guid.Empty))
            {
                throw new Exception("Subject not found");
            }
            else
            {
                incident.SubjectId = new EntityReference(Subject.EntityLogicalName, subjectId.Value);
            }
            if (string.IsNullOrWhiteSpace(customerId))
            {
                string titleTemplate = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.AppInfo, CUBConstants.Globals.CustomerFeedbackTitleTemplate).ToString();
                titleTemplate = titleTemplate.Replace("{firstName}", firstName).Replace("{lastName}", lastName).Replace("{unregisteredEmail}", unregisteredEmail);
                titleTemplate = titleTemplate.Replace("{phoneType}", phoneType).Replace("{phoneNumber}", phoneNumber).Replace("{cardNumber}", referenceType + ": " + referenceValue);
                feedbackMessage = titleTemplate + "\n" + feedbackMessage;
                customerId = GlobalsCache.GetKeyFromCache(_organizationService, CUBConstants.Globals.AppInfo, CUBConstants.Globals.UnregisteredCustomerID).ToString();
            }
            incident.Title = feedbackType;
            if (!string.IsNullOrWhiteSpace(contactId))
            {
                incident.PrimaryContactId = new EntityReference(Contact.EntityLogicalName, new Guid(contactId));
            }
            incident.CustomerId = new EntityReference(Account.EntityLogicalName, new Guid(customerId));
            incident.Description = feedbackMessage;
            OptionSetTranslationLogic optionSetTranslation = new OptionSetTranslationLogic(_organizationService);
            incident.CaseOriginCode = new OptionSetValue(optionSetTranslation.GetValue("case", "CaseOriginCode", channel));
            Guid incidentId =_organizationService.Create(incident);
            string incidentNumber = (from i in CubOrgSvc.IncidentSet
                                     where i.Id == incidentId
                                     select i.TicketNumber).SingleOrDefault();
            var response = new
            {
                referenceNumber = incidentNumber,
                caseId = incidentId.ToString().ToUpper()
            };
            return JsonConvert.SerializeObject(response);
        }
    }
}
