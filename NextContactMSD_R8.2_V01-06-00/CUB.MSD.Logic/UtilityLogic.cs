/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;
using System.Reflection;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// Utility Logic
    /// </summary>
    public class UtilityLogic
    {
        private static byte[] KEY_192 = { 42, 16, 93, 156, 78, 4, 218, 32, 15, 167, 44, 80, 26, 250, 155, 112, 2, 94, 11, 204, 119, 35, 184, 197 };
        private static byte[] IV_192 = { 55, 103, 246, 79, 36, 99, 167, 3, 42, 5, 62, 83, 184, 7, 209, 13, 145, 23, 200, 58, 173, 10, 121, 222 };
        private const string OPTIONSET_CACHE_NAME_TEMPLATE = "OPTIONSET_{0}";


        /// <summary>
        /// Upper Case Count
        /// </summary>
        /// <param name="Password">password</param>
        /// <returns>Upper Case Count</returns>
        public static int UpperCaseCount(string Password)
        {
            return Regex.Matches(Password, "[A-Z]").Count;
        }

        /// <summary>
        /// Lower Case Count
        /// </summary>
        /// <param name="Password">password</param>
        /// <returns>Lower Case Count</returns>
        public static int LowerCaseCount(string Password)
        {
            return Regex.Matches(Password, "[a-z]").Count;
        }

        /// <summary>
        /// Numeric Count
        /// </summary>
        /// <param name="Password">Password</param>
        /// <returns>Numeric Count</returns>
        public static int NumericCount(string Password)
        {
            return Regex.Matches(Password, "[0-9]").Count;
        }

        /// <summary>
        /// Is all numeric
        /// </summary>
        /// <param name="pin">PIN</param>
        /// <returns>If is all numeric or not</returns>
        public static bool IsAllNumeric(string pin)
        {
            if (string.IsNullOrEmpty(pin)) return false;

            return pin.All(char.IsDigit);
        }

        /// <summary>
        /// Non alpha count
        /// </summary>
        /// <param name="Password">password</param>
        /// <returns>Non alpha count</returns>
        public static int NonAlphaCount(string Password)
        {
            return Regex.Matches(Password, @"[^0-9a-zA-Z\._]").Count;
        }

        /// <summary>
        /// Password Contains Dictionary Word
        /// </summary>
        /// <param name="source">Source</param>
        /// <returns>If password Contains Dictionary Word</returns>
        public static bool PasswordContainsDictionaryWord(string source)
        {
            // from where do we get the dictionary of words?
            return false;
        }

        /// <summary>
        /// Has Contiguous Chars
        /// </summary>
        /// <param name="source">Source</param>
        /// <returns>If Has Contiguous Chars</returns>
        public static bool HasContiguousChars(string source)
        {
            int size = 2;
            int n = source.Length;

            if (n < 3) return false;

            for (int i = 0; i < n - 1; i++)
            {
                int start = 0;

                for (int x = 0; x < n - size; x++)
                {
                    string checkstring = source.Substring(start++, size);

                    if (source.Contains(checkstring + checkstring))
                    {
                        return true;
                    }
                }

                size++;
            }

            return false;
        }

        /// <summary>
        /// Has Consecutive Chars
        /// </summary>
        /// <param name="source">Source</param>
        /// <returns>IF Has Consecutive Chars</returns>
        public static bool HasConsecutiveChars(string source)
        {
            var charEnumerator = StringInfo.GetTextElementEnumerator(source);
            var currentElement = string.Empty;
            int count = 1;

            while (charEnumerator.MoveNext())
            {
                if (currentElement == charEnumerator.GetTextElement())
                {
                    if (++count >= 2)
                    {
                        return true;
                    }
                }
                else
                {
                    count = 1;
                    currentElement = charEnumerator.GetTextElement();
                }
            }

            return false;
        }

        /// <summary>
        /// Is Valid Email Format
        /// </summary>
        /// <param name="source">Source</param>
        /// <returns>Is Valid Email Format</returns>
        public static bool isValidEmailFormat(string source)
        {
            source = NulltoString(ref source);
            source = source.Trim();

            Regex re = new Regex("^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#" +
                "\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?" +
                "(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|" +
                "(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?" +
                "(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])" +
                "([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)" +
                "+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])" +
                "*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?$", RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture | RegexOptions.Compiled);

            return (re.IsMatch(source));
        }

        /// <summary>
        /// Null to String
        /// </summary>
        /// <param name="str">String</param>
        /// <returns>string</returns>
        public static string NulltoString(ref string str)
        {
            if (str == null)
            {
                str = "";
            }

            return str;
        }

        /// <summary>
        /// Is String Too Short
        /// </summary>
        /// <param name="str">String</param>
        /// <param name="min_length">Minimum length</param>
        /// <returns>If Is String Too Short</returns>
        public static bool IsStringTooShort(string str, int min_length)
        {
            bool RET_VAL = false;

            if (str.Length < min_length) RET_VAL = true;

            return RET_VAL;
        }

        /// <summary>
        /// Is String Length Valid
        /// </summary>
        /// <param name="str">string</param>
        /// <param name="max_length">max length</param>
        /// <returns>If is String Length Valid</returns>
        public static bool IsStringLengthValid(string str, int max_length)
        {
            bool RET_VAL = true;

            if (!string.IsNullOrEmpty(str) && str.Length > max_length) RET_VAL = false;

            return RET_VAL;
        }

        /// <summary>
        /// TRIPLE DES encryption
        /// </summary>
        /// <history>
        /// Revision# Date      Author  Description  
        /// </history>
        public static string EncryptTripleDES(string val)
        {
            if (val != null && val.Length > 0)
            {
                TripleDESCryptoServiceProvider cryptoProvider = new TripleDESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, cryptoProvider.CreateEncryptor(KEY_192, IV_192), CryptoStreamMode.Write);
                StreamWriter sw = new StreamWriter(cs);
                sw.Write(val);
                sw.Flush();
                cs.FlushFinalBlock();
                ms.Flush();
                //convert back to a string
                val = Convert.ToBase64String(ms.GetBuffer(), 0, (int)ms.Length);
            }
            return val;
        }

        /// <summary>
        /// TRIPLE DES decryption
        /// </summary>
        /// <history>
        /// Revision# Date      Author  Description 
        /// </history>
        public static string DecryptTripleDES(string val)
        {
            if (val != null && val.Length > 0)
            {
                TripleDESCryptoServiceProvider cryptoProvider = new TripleDESCryptoServiceProvider();
                //convert from string to byte array
                byte[] buffer = Convert.FromBase64String(val);
                MemoryStream ms = new MemoryStream(buffer);
                CryptoStream cs = new CryptoStream(ms, cryptoProvider.CreateDecryptor(KEY_192, IV_192), CryptoStreamMode.Read);
                StreamReader sr = new StreamReader(cs);
                val = sr.ReadToEnd();
            }
            return val;
        }

        /// <summary>
        /// Return the metadadata of a MSD OptionSet using reflection on the generated early bound Enums.
        /// The idea of this method is to assist in the UI implementation by return the list of key+value of MSD optionsets.
        /// All OptionSetMetada will be cached
        /// </summary>
        /// <param name="optionSetType">OptionSet Type. ex: typeof(cub_phonetype)</param>
        /// <param name="assemblyName">Assembly name. The default value is the Cubic model dll: CUB.MSD.Model.dll</param>
        /// <returns>List of KeyValuePair(int, string) with the OptionSet Metadata.</returns>
        public List<KeyValuePair<int, string>> GetOptionSetMetaData(Type optionSetType, string assemblyName = "CUB.MSD.Model.dll")
        {
            string cacheName = string.Format(OPTIONSET_CACHE_NAME_TEMPLATE, optionSetType.Name);
            List<KeyValuePair<int, string>> optionSetData = (List<KeyValuePair<int, string>>) GlobalsCache.GetFromCache(cacheName);
            if (optionSetData != null)
            {
                return optionSetData;
            }
            var fullPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "") + "\\" + assemblyName;
            optionSetData = new List<KeyValuePair<int, string>>();
            Assembly assembly = Assembly.ReflectionOnlyLoadFrom(fullPath);
            Type type = assembly.GetType(optionSetType.Name);
            if (!type.IsEnum)
            {
                throw new Exception(string.Format("{0} is not a Enum on {1}", optionSetType.Name, assemblyName));
            }
            FieldInfo[] fields = type.GetFields();
            foreach (FieldInfo field in fields)
            {
                if (!field.IsLiteral) // Ignore the non literals fields
                    continue;
                string name = field.Name;
                int value = int.Parse(field.GetRawConstantValue().ToString());
                optionSetData.Add(new KeyValuePair<int, string>(value, name));
            }
            GlobalsCache.AddToCache(cacheName, optionSetData);
            return optionSetData;
        }

        /// <summary>
        /// Convert OptionSet Object To Enum
        /// </summary>
        /// <typeparam name="T">Type OptionSet</typeparam>
        /// <param name="objectField">OptionSet</param>
        /// <returns>optionSet as ENUM</returns>
        public static T ConvertOptionSetObjectToEnum<T>(object objectField)
        {
            OptionSetValue osv = (OptionSetValue)objectField;
            T ret_code = (T)System.Enum.Parse(typeof(T), osv.Value.ToString());
            return ret_code;
        }

        /// <summary>
        /// Convert OptionSet ObjectTo String Name
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="objectField">Optionset</param>
        /// <returns></returns>
        public static string ConvertOptionSetObjectToStringName<T>(object objectField)
        {
            OptionSetValue osv = (OptionSetValue)objectField;
            T ret_code = (T)System.Enum.Parse(typeof(T), osv.Value.ToString());
            return System.Enum.GetName(typeof(T), ret_code);
        }
    }
}
