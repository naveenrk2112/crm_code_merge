/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUB.MSD.Web.Models;
using CUB.MSD.Model.MSDApi;
using System.Text.RegularExpressions;
using Microsoft.Xrm.Sdk.Messages;
using System.ServiceModel;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// Customer Registration Logic
    /// </summary>
    public class CustomerRegistrationLogic : LogicBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">Organization Service</param>
        public CustomerRegistrationLogic(IOrganizationService service) : base(service)
        {

        }

        /// <summary>
        /// Seach for a contact using the First Name, Last Name, Email and Phone
        /// The search rules are:
        /// First Name AND Last Name
        /// OR Email
        /// OR Phone (Phone will be compared with the three MSD phone fields: telephone1, telephone2 and telephone3)
        /// * All search is full match (Equals) not partial or wilcards
        /// * The name search is only executed with both first name and last name is provided
        /// </summary>
        /// <param name="firstName">First Name</param>
        /// <param name="lastName">Last Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">Phone</param>
        /// <param name="customerTypeId">Customer Type ID</param>
        /// <returns>JSON with the list of found records or empty if not found.
        /// The returned field list is: ContactId, FirstName, LastName, EMailAddress1, Telephone1, Telephone2, Telephone3
        /// </returns>
        public virtual string SearchContact(string firstName, string lastName, string email, string phone, Guid? customerTypeId)
        {
            if (string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName) &&
                string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone) && !customerTypeId.HasValue)
                return "{}";
            if (!string.IsNullOrEmpty(firstName)) firstName = firstName.Trim();
            if (!string.IsNullOrEmpty(lastName)) lastName = lastName.Trim();
            if (!string.IsNullOrEmpty(email)) email = email.Trim();
            if (!string.IsNullOrEmpty(phone))
            {
                var digitsOnly = new Regex(@"[^\d]");
                phone = digitsOnly.Replace(phone, "");
                phone = phone.Trim();
            }
            var queryExpression = new QueryExpression(Contact.EntityLogicalName);
            queryExpression.NoLock = true;
            queryExpression.Distinct = true;
            queryExpression.ColumnSet = new ColumnSet(Contact.contactidAttribute,
                                                      Contact.firstnameAttribute,
                                                      Contact.lastnameAttribute,
                                                      Contact.emailaddress1Attribute,
                                                      Contact.telephone1Attribute,
                                                      Contact.telephone2Attribute,
                                                      Contact.telephone3Attribute,
                                                      Contact.cub_phone1typeAttribute,
                                                      Contact.cub_phone2typeAttribute,
                                                      Contact.cub_phone3typeAttribute,
                                                      Contact.cub_contacttypeidAttribute);
            queryExpression.Orders.Add(new OrderExpression(Contact.fullnameAttribute, OrderType.Ascending));
            FilterExpression filter = new FilterExpression(LogicalOperator.Or);
            //Name filter
            FilterExpression nameFilter = new FilterExpression(LogicalOperator.And);
            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            {
                nameFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.Equal, firstName);
                nameFilter.AddCondition(Contact.lastnameAttribute, ConditionOperator.Equal, lastName);
            }
            // Email filter
            FilterExpression emailFilter = new FilterExpression();
            if (!string.IsNullOrEmpty(email))
                emailFilter.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.Equal, email);
            // Phone filter
            FilterExpression phoneFilter = new FilterExpression(LogicalOperator.Or);
            if (!string.IsNullOrEmpty(phone))
            {
                phoneFilter.AddCondition(Contact.telephone1Attribute, ConditionOperator.Equal, phone.Trim());
                phoneFilter.AddCondition(Contact.telephone2Attribute, ConditionOperator.Equal, phone.Trim());
                phoneFilter.AddCondition(Contact.telephone3Attribute, ConditionOperator.Equal, phone.Trim());
            }

            // Adding the filters
            if (nameFilter.Conditions.Count > 0)
                filter.AddFilter(nameFilter);
            if (emailFilter.Conditions.Count > 0)
                filter.AddFilter(emailFilter);
            if (phoneFilter.Conditions.Count > 0)
                filter.AddFilter(phoneFilter);
            // No condition. This is protection when we have only First Name or Last Name and no other condition
            // We should always search for First Name and Last Name
            if (filter.Filters.Count == 0)
            {
                return "{}";
            }
            queryExpression.Criteria = filter;

            //Added attributes from Account and Address
            var customerLink = new LinkEntity(Contact.EntityLogicalName, Account.EntityLogicalName, Contact.accountidAttribute,
                                                Account.accountidAttribute, JoinOperator.LeftOuter);
            customerLink.EntityAlias = "acc";
            customerLink.Columns.AddColumns(Account.cub_accounttypeidAttribute, Account.nameAttribute, Account.accountidAttribute);

            // Add Customer Type link & filter to Account link
            if (customerTypeId.HasValue)
            {
                var customerTypeLink = new LinkEntity(Account.EntityLogicalName, cub_AccountType.EntityLogicalName,
                    Account.cub_accounttypeidAttribute, cub_AccountType.cub_accounttypeidAttribute, JoinOperator.Inner);
                var customerTypeFilter = new FilterExpression();
                customerTypeFilter.AddCondition(cub_AccountType.cub_accounttypeidAttribute, ConditionOperator.Equal, customerTypeId.Value);
                customerTypeLink.LinkCriteria = customerTypeFilter;
                customerLink.LinkEntities.Add(customerTypeLink);
            }
            queryExpression.LinkEntities.Add(customerLink);

            queryExpression.LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_Address.EntityLogicalName, Contact.cub_addressidAttribute,
                                    cub_Address.cub_addressidAttribute, JoinOperator.LeftOuter));
            queryExpression.LinkEntities[1].Columns.AddColumns(cub_Address.cub_cityAttribute);
            queryExpression.LinkEntities[1].EntityAlias = "addr";

            EntityCollection res = this._organizationService.RetrieveMultiple(queryExpression);
            List<Contact> contacts = res.Entities.Select(c => c.ToEntity<Contact>()).ToList();
            var reducedContact = (from c in contacts
                                  select new
                                  {
                                      ContactId = c.Id.ToString(),
                                      ContactType = (c.cub_ContactTypeId != null ? c.cub_ContactTypeId.Name : ""),
                                      AccountId = c.Attributes.Contains("acc.accountid") ? ((Guid)c.GetAttributeValue<AliasedValue>("acc.accountid").Value).ToString() : "",
                                      AccountType = c.Attributes.Contains("acc.cub_accounttypeid") ? ((EntityReference)c.GetAttributeValue<AliasedValue>("acc.cub_accounttypeid").Value).Name : "",
                                      FirstName = c.FirstName,
                                      LastName = c.LastName,
                                      FullName = String.Format("{0} {1}", c.FirstName, c.LastName),
                                      CustomerName = c.Attributes.Contains("acc.name") ? (string)c.GetAttributeValue<AliasedValue>("acc.name").Value : "",
                                      Email = c.EMailAddress1 ?? string.Empty,
                                      City = c.Attributes.Contains("addr.cub_city") ? (string)c.GetAttributeValue<AliasedValue>("addr.cub_city").Value : "",
                                      Phone1 = c.Telephone1 ?? string.Empty,
                                      Phone2 = c.Telephone2 ?? string.Empty,
                                      Phone3 = c.Telephone3 ?? string.Empty,
                                      Phone1Type = GetAttributeOptionSetText(c, Contact.cub_phone1typeAttribute, typeof(cub_phonetype)),
                                      Phone2Type = GetAttributeOptionSetText(c, Contact.cub_phone2typeAttribute, typeof(cub_phonetype)),
                                      Phone3Type = GetAttributeOptionSetText(c, Contact.cub_phone3typeAttribute, typeof(cub_phonetype)),
                                      SecurityQA = (from sa in CubOrgSvc.cub_SecurityAnswerSet
                                                    where sa.cub_ContactId == c.ToEntityReference()
                                                    select new
                                                    {
                                                        Question = sa.cub_SecurityQuestionId != null ? sa.cub_SecurityQuestionId.Name : "",
                                                        Answer = sa != null ? sa.cub_SecurityAnswerDesc : ""
                                                    }).FirstOrDefault()
                                  });
            return Newtonsoft.Json.JsonConvert.SerializeObject(reducedContact);
        }

        /// <summary>
        /// Search contact. 
        /// </summary>
        /// <param name="firstName">First Name</param>
        /// <param name="lastName">Last Name</param>
        /// <param name="email">Email</param>
        /// <returns>Contacts with exactly First, Last Name and Email</returns>
        public string SearchContactExactMatch(string firstName, string lastName, string email)
        {
            var query = from c
                        in CubOrgSvc.ContactSet
                        orderby c.FirstName, c.LastName
                        where c.FirstName.Equals(firstName) &&
                              c.LastName.Equals(lastName) &&
                              c.EMailAddress1.Equals(email)
                        select new
                        {
                            ContactId = c.ContactId.ToString(),
                            FirstName = c.FirstName,
                            LastName = c.LastName,
                            Email = c.EMailAddress1,
                            Phone1 = c.Telephone1,
                            Phone2 = c.Telephone2,
                            Phone3 = c.Telephone3
                        };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }

        /// <summary>
        /// Register Costumer
        /// One record will be created in the following entities: Account, Address, Contact, SecurityQuestionsAnswers, Credentials
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Account ID</returns>
        public Guid RegisterCustomer(CustomerRegistrationModel model)
        {
            var requestToCreateRecords = new ExecuteTransactionRequest()
            {
                Requests = new OrganizationRequestCollection(),
                ReturnResponses = true
            };

            EntityCollection operations = new EntityCollection();

            Account account = new Account();
            account.AccountId = Guid.NewGuid();
            account.Name = string.Format("{0} {1}", model.FirstName, model.LastName);
            account.cub_AccountTypeId = new EntityReference(cub_AccountType.EntityLogicalName, model.AccountType);
            operations.Entities.Add(account);
            Guid addressId = Guid.Empty;

            if (GlobalsCache.CustomerRegistrationIsAddressRequired(_organizationService))
            {
                var address = CreateAddress(model, account.AccountId.Value);
                operations.Entities.Add(address);
                addressId = address.cub_AddressId.Value;
            }

            Contact contact = CreateContact(model, account.AccountId.Value, addressId);
            operations.Entities.Add(contact);
            CreateSecurityQuestionAnswer(model, contact.ContactId.Value, operations);
            var credentials = CreateCredentials(model, contact.ContactId.Value);
            operations.Entities.Add(credentials);

            foreach (var entity in operations.Entities)
            {
                CreateRequest createRequest = new CreateRequest { Target = entity };
                requestToCreateRecords.Requests.Add(createRequest);
            }

            try
            {
                var responseForCreateRecords = (ExecuteTransactionResponse) _organizationService.Execute(requestToCreateRecords);
                // Log results of each response
                int i = 0;
                // Display the results returned in the responses.
                foreach (var responseItem in responseForCreateRecords.Responses)
                {
                    if (responseItem != null)
                    {
                        CubLogger.Info("Register Customer - Entity = ", ((Entity)requestToCreateRecords.Requests[i].Parameters["Target"]).LogicalName);
                        CubLogger.Info("Register Customer - New ID = ", responseItem.Results["id"].ToString());
                        i++;
                    }
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                CubLogger.Error("Error register customer=", ex.InnerException);
                throw;
            }


            return account.AccountId.Value;
        }

        /// <summary>
        /// Create Address
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="accountId">Account ID</param>
        /// <returns>Address ID</returns>
        public cub_Address CreateAddress(CustomerRegistrationModel model, Guid accountId)
        {
            cub_Address address = new cub_Address();
            address.cub_AccountAddressId = new EntityReference(Account.EntityLogicalName, accountId);
            address.cub_Name = model.Address1 + " " + model.Address2;
            address.cub_Street1 = model.Address1;
            address.cub_Street2 = model.Address2;
            address.cub_City = model.City;
            if (model.State.HasValue) {
                address.cub_StateProvinceId = new EntityReference(cub_StateOrProvince.EntityLogicalName, model.State.Value);
            }
            if (model.ZipPostalCodeId.HasValue)
            {
                address.cub_PostalCodeId = new EntityReference(cub_ZipOrPostalCode.EntityLogicalName, model.ZipPostalCodeId.Value);
            }
            if (model.Country.HasValue)
            {
                address.cub_CountryId = new EntityReference(cub_Country.EntityLogicalName, model.Country.Value);

            }
            address.cub_AddressId = Guid.NewGuid();
            return address;
        }

        /// <summary>
        /// Validate fields from UI
        /// </summary>
        /// <param name="model">Customer Registration Model</param>
        /// <returns>Validation Errors</returns>
        public List<KeyValuePair<string, string>> ValidateFieldsNewCustomerRegistration(CustomerRegistrationModel model)
        {
            //Key = Field Name; Value = Error Message
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            ValidateStringField(result, "First Name", model.FirstName, true, Convert.ToInt32(GlobalsCache.CustomerRegistrationFirstNameMaxLength(_organizationService)));
            ValidateStringField(result, "Last Name", model.LastName, true, Convert.ToInt32(GlobalsCache.CustomerRegistrationLastNameMaxLength(_organizationService)));
            ValidateStringField(result, "Middle Name Initial", model.MiddleNameInitial, false, 1);
            ValidateStringField(result, "City", model.City, GlobalsCache.CustomerRegistrationIsAddressRequired(_organizationService), Convert.ToInt32(GlobalsCache.CustomerRegistrationCityMaxLength(_organizationService)));
            ValidateStringField(result, "Address 1", model.Address1, GlobalsCache.CustomerRegistrationIsAddressRequired(_organizationService), Convert.ToInt32(GlobalsCache.CustomerRegistrationAddressLine1MaxLength(_organizationService)));
            ValidateStringField(result, "Address 2", model.Address2, false, Convert.ToInt32(GlobalsCache.CustomerRegistrationAddressLine2MaxLength(_organizationService)));
            ValidateStringField(result, "PIN", model.PIN, false, Convert.ToInt32(GlobalsCache.CustomerRegistrationPinMaxLength(_organizationService)));
            
            if (GlobalsCache.CustomerRegistrationIsAddressRequired(_organizationService) && !model.State.HasValue)
                result.Add(new KeyValuePair<string, string>("State", "Field required"));
            if (GlobalsCache.CustomerRegistrationIsAddressRequired(_organizationService) && !model.Country.HasValue)
                result.Add(new KeyValuePair<string, string>("Country", "Field required"));
            if (GlobalsCache.CustomerRegistrationIsAddressRequired(_organizationService) && !model.ZipPostalCodeId.HasValue)
                result.Add(new KeyValuePair<string, string>("Zip/Postal Code", "Field required"));

            // Security Question/Answer validation
            var minimumSQs = int.Parse(GlobalsCache.CustomerRegistrationSecurityQuestionsMinimum(_organizationService));
            if (model.SecurityQAs?.Count < minimumSQs)
            {
                result.Add(new KeyValuePair<string, string>("Security Question", $"Must specify {minimumSQs}"));
            }
            foreach (var qa in model.SecurityQAs)
            {
                ValidateStringField(result, "Security Question Answer", qa.Value, true, Convert.ToInt32(GlobalsCache.CustomerRegistrationSecurityAnswerMaxLength(_organizationService)));
            }

            ContactLogic contactLogic = new ContactLogic(_organizationService);
            var userNameValidation = contactLogic.ValidateUsername(model.UserName);
            if (userNameValidation.Errors.Count != 0)
            {
                foreach (var err in userNameValidation.Errors)
                {
                    result.Add(new KeyValuePair<string, string>("UserName", err.errorMessage));
                }
            }
            ValidateTelephone(contactLogic, result, "Phone 1", model.Phone1, model.Phone1Type);
            ValidateTelephone(contactLogic, result, "Phone 2", model.Phone2, model.Phone2Type);
            ValidateTelephone(contactLogic, result, "Phone 3", model.Phone3, model.Phone3Type);
            EmailLogic emailLogic = new EmailLogic(_organizationService);
            bool emailValidation = emailLogic.ValidateEmailAddress(model.Email);
            if (!emailValidation)
            {
                result.Add(new KeyValuePair<string, string>("Email", "Invalid Email"));
            }
            return result;
        }

        /// <summary>
        /// Validate a string field
        /// </summary>
        /// <param name="errors">List of errors</param>
        /// <param name="fieldName">Field Nme</param>
        /// <param name="fieldValue">Field Value</param>
        /// <param name="isRequired">Is Required</param>
        /// <param name="maxLength">Max length. Null if doesn't require check</param>
        private void ValidateStringField(List<KeyValuePair<string, string>> errors,
                                         string fieldName,
                                         string fieldValue,
                                         bool isRequired,
                                         int? maxLength)
        {
            if (isRequired && string.IsNullOrWhiteSpace(fieldValue))
            {
                errors.Add(new KeyValuePair<string, string>(fieldName, "Field required"));
            }
            if (maxLength.HasValue && !string.IsNullOrWhiteSpace(fieldValue) && fieldValue.Length > maxLength)
            {
                errors.Add(new KeyValuePair<string, string>(fieldName, "Field maximum length is " + maxLength));
            }
        }

        /// <summary>
        /// Validat e a phone field.
        /// The errors wil be added tgo the result collection
        /// </summary>
        /// <param name="contactLogic">Contact Logic</param>
        /// <param name="result">List of errors</param>
        /// <param name="name">name of the field</param>
        /// <param name="value">value of the field</param>
        /// <param name="type">type of the phone field</param>
        private void ValidateTelephone(ContactLogic contactLogic, 
                                       List<KeyValuePair<string, string>> result, 
                                       string name, 
                                       string value, 
                                       int type)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                var logic = new OptionSetTranslationLogic(_organizationService);

                WSPhone phone = new WSPhone();
                phone.number = value;
                phone.type = logic.GetText(null, "cub_phonetype", type);

                var phoneValidation = contactLogic.IsPhoneValid(phone);
                if (phoneValidation.Errors.Count != 0)
                {
                    foreach (var err in phoneValidation.Errors)
                    {
                        result.Add(new KeyValuePair<string, string>(name, err.errorMessage));
                    }
                }
            }
        }

        /// <summary>
        /// Create Contact
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="accountId">Account ID</param>
        /// <param name="addressId">Address ID</param>
        /// <returns>Contact ID</returns>
        public Contact CreateContact(CustomerRegistrationModel model, Guid accountId, Guid addressId)
        {
            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            contact.ParentCustomerId = new EntityReference(Account.EntityLogicalName, accountId);
            if (addressId != Guid.Empty)
                contact.cub_AddressId = new EntityReference(cub_Address.EntityLogicalName, addressId);
            contact.cub_ContactTypeId = new EntityReference(cub_ContactType.EntityLogicalName, model.ContactType);
            contact.FirstName = model.FirstName;
            contact.LastName = model.LastName;
            contact.MiddleName = model.MiddleNameInitial;
            contact.BirthDate = model.BirthDate;
            contact.EMailAddress1 = model.Email;
            contact.cub_Phone1Type = new OptionSetValue(model.Phone1Type);
            contact.Telephone1 = model.Phone1;
            if (!string.IsNullOrWhiteSpace(model.Phone2))
            {
                contact.cub_Phone2Type = new OptionSetValue(model.Phone2Type);
                contact.Telephone2 = model.Phone2;
            }
            if (!string.IsNullOrWhiteSpace(model.Phone3))
            {
                contact.cub_Phone3Type = new OptionSetValue(model.Phone3Type);
                contact.Telephone3 = model.Phone3;
            }
            return contact;
        }

        /// <summary>
        /// Create Security Questions Answers
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="ContactId">Contact ID</param>
        public void CreateSecurityQuestionAnswer(CustomerRegistrationModel model, Guid ContactId, EntityCollection operations)
        {
            foreach (var qa in model.SecurityQAs)
            {
                cub_SecurityAnswer securityAnswer = new cub_SecurityAnswer();
                securityAnswer.cub_ContactId = new EntityReference(Contact.EntityLogicalName, ContactId);
                securityAnswer.cub_SecurityQuestionId = new EntityReference(cub_SecurityQuestion.EntityLogicalName, qa.Key);
                securityAnswer.cub_SecurityAnswerDesc = qa.Value;
                operations.Entities.Add(securityAnswer);
            }
        }

        /// <summary>
        /// Create Credentials
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="ContactId">Contact ID</param>
        public cub_Credential CreateCredentials(CustomerRegistrationModel model, Guid ContactId)
        {
            cub_Credential credentials = new cub_Credential();
            credentials.cub_ContactId = new EntityReference(Contact.EntityLogicalName, ContactId);
            credentials.cub_Username = model.UserName;
            credentials.cub_Pin = model.PIN;
            return credentials;
        }

        /// <summary>
        /// Get Zip/Postal Code
        /// </summary>
        /// <param name="zipPostalCode">zip/postal code</param>
        /// <param name="country">Country ID</param>
        /// <param name="state">State ID</param>
        /// <returns>Zip/Postal Codes with Country, State and City data</returns>
        public List<ZipPostalCodeModel> GetZipPostalCode(string zipPostalCode, Guid? country, Guid? state)
        {
            if (string.IsNullOrEmpty(zipPostalCode))
                return null;
            if (!string.IsNullOrEmpty(zipPostalCode)) zipPostalCode = zipPostalCode.Trim();
            var queryExpression = new QueryExpression(cub_ZipOrPostalCode.EntityLogicalName);
            queryExpression.NoLock = true;
            queryExpression.Distinct = true;
            queryExpression.ColumnSet = new ColumnSet(cub_ZipOrPostalCode.cub_ziporpostalcodeidAttribute,
                                                      cub_ZipOrPostalCode.cub_codeAttribute,
                                                      cub_ZipOrPostalCode.cub_stateAttribute,
                                                      cub_ZipOrPostalCode.cub_countryAttribute,
                                                      cub_ZipOrPostalCode.cub_cityAttribute,
                                                      cub_ZipOrPostalCode.cub_codetypeAttribute);
            queryExpression.Orders.Add(new OrderExpression(cub_ZipOrPostalCode.cub_countryAttribute, OrderType.Ascending));
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            //Code filter
            FilterExpression zipCodeFilter = new FilterExpression(LogicalOperator.And);
            zipCodeFilter.AddCondition(cub_ZipOrPostalCode.cub_codeAttribute, ConditionOperator.Equal, zipPostalCode);
            // State filter
            FilterExpression stateFilter = new FilterExpression();
            if (state.HasValue)
                stateFilter.AddCondition(cub_ZipOrPostalCode.cub_stateAttribute, ConditionOperator.Equal, state.Value);
            // Country filter
            FilterExpression countryFilter = new FilterExpression(LogicalOperator.And);
            if (country.HasValue)
            {
                countryFilter.AddCondition(cub_ZipOrPostalCode.cub_countryAttribute, ConditionOperator.Equal, country.Value);
            }
            // Adding the filters
            filter.AddFilter(zipCodeFilter);
            if (stateFilter.Conditions.Count > 0)
                filter.AddFilter(stateFilter);
            if (countryFilter.Conditions.Count > 0)
                filter.AddFilter(countryFilter);
            queryExpression.Criteria = filter;
            EntityCollection res = this._organizationService.RetrieveMultiple(queryExpression);
            List<cub_ZipOrPostalCode> zips = res.Entities.Select(c => c.ToEntity<cub_ZipOrPostalCode>()).ToList();
            var query = (from z in zips
                         select new ZipPostalCodeModel
                         {
                             ID = z.cub_ZipOrPostalCodeId.Value,
                             ZipPostal = z.cub_Code,
                             Country = new KeyValuePair<Guid, string>(z.cub_Country.Id, z.cub_Country.Name),
                             State = new KeyValuePair<Guid, string>(z.cub_State.Id, z.cub_State.Name),
                             City = z.cub_City,
                             CodeType = z.cub_CodeType
                         }).ToList();

            return query;
        }
    }
}
