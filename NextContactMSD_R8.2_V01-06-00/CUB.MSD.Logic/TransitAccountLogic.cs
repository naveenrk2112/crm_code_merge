﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using Microsoft.Xrm.Sdk;
using System.Linq;

namespace CUB.MSD.Logic
{
    public class TransitAccountLogic : LogicBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="orgService">MSD Organization Service instance</param>
        public TransitAccountLogic(IOrganizationService orgService) : base(orgService)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="orgService">MSD Organization Service instance</param>
        /// <param name="context">MSD Plugin Execution Context object</param>
        public TransitAccountLogic(IOrganizationService orgService, IPluginExecutionContext context) : base(orgService, context)
        {
        }

        /// <summary>
        /// Within MSD, associate a Transit Account record with a Customer record
        /// </summary>
        /// <param name="transitAccountId">Transit account ID</param>
        /// <param name="oneAccountId">OneAccount ID</param>
        public void AssociateWithCustomer(string transitAccountId, string subsystemId, string oneAccountId)
        {
            var customerRef = (from c in CubOrgSvc.AccountSet
                               where c.cub_OneAccountId == oneAccountId
                               select new Account
                               {
                                   Id = c.Id
                               }).FirstOrDefault()?.ToEntityReference();
            UpsertTransitAccount(transitAccountId, subsystemId, customerRef);
        }

        /// <summary>
        /// Within MSD, upsert a Transit Account record, assigning or updating
        /// a related Customer record.
        /// </summary>
        /// <param name="transitAccountId">Transit account ID</param>
        /// <param name="customer">Reference to a Customer. If not provided, removes the current Customer (if any)</param>
        public void UpsertTransitAccount(string transitAccountId, string subsystemId, EntityReference customer = null)
        {
            var transitAccount = new cub_TransitAccount
            {
                cub_Customer = customer,
                cub_Contact = null
            };
            if(customer != null)
            {
                using (var accountLogic = new AccountLogic(_organizationService))
                {
                    transitAccount.cub_Contact = accountLogic.GetPrimaryContactReference(customer.Id);
                }
            }

            var existingRef = GetTransitAccountRef(transitAccountId, subsystemId);

            if (existingRef == null)
            {
                transitAccount.cub_Name = transitAccountId;
                transitAccount.cub_SubsystemId = subsystemId;
                _organizationService.Create(transitAccount);
            }
            else
            {
                transitAccount.Id = existingRef.Id;
                _organizationService.Update(transitAccount);
            }
        }

        /// <summary>
        /// Attempt to retrieve an existing Transit Account entity reference from MSD.
        /// </summary>
        /// <param name="transitAccountId">Transit account ID</param>
        /// <returns>An entity reference, or null if no match</returns>
        public EntityReference GetTransitAccountRef(string transitAccountId, string subsystemId)
        {
            return (from t in CubOrgSvc.cub_TransitAccountSet
                    where t.cub_Name == transitAccountId
                       && t.cub_SubsystemId == subsystemId
                    select new cub_TransitAccount
                    {
                        Id = t.Id
                    }).FirstOrDefault()?.ToEntityReference();
        }

        public WSCubTransitAccount GetTransitAccount(string transitaccountId, string subsystemId)
        {
            var ta = (from t in CubOrgSvc.cub_TransitAccountSet
                    where t.cub_Name == transitaccountId
                        && t.cub_SubsystemId == subsystemId
                    select new cub_TransitAccount
                    {
                        Id = t.Id,
                        cub_Name = t.cub_Name,
                        cub_SubsystemId = t.cub_SubsystemId,
                        cub_Customer = t.cub_Customer,
                        cub_Contact = t.cub_Contact
                    }).FirstOrDefault();

            return ta == null ? null : MapTransitAccount(ta);
        }

        /// <summary>
        /// Create a Transit Account record in MSD
        /// </summary>
        /// <param name="transitAccountId">Transit account ID</param>
        /// <param name="customer">Customer reference if Transit Account is linked, or null if unlinked</param>
        /// <returns>An entity reference of the newly-created transit account</returns>
        public WSCubTransitAccount CreateTransitAccount(string transitAccountId, string subsystemId, EntityReference customer = null)
        {
            var transitAccount = new cub_TransitAccount
            {
                cub_Name = transitAccountId,
                cub_SubsystemId = subsystemId,
                cub_Customer = customer
            };
            if(customer != null)
            {
                using(var accountLogic = new AccountLogic(_organizationService))
                {
                    transitAccount.cub_Contact = accountLogic.GetPrimaryContactReference(customer.Id);
                }
            }
            var id = _organizationService.Create(transitAccount);
            transitAccount.Id = id;
            return MapTransitAccount(transitAccount);
        }

        private WSCubTransitAccount MapTransitAccount(cub_TransitAccount ta)
        {
            return new WSCubTransitAccount
            {
                Id = ta.Id,
                TransitAccountId = ta.cub_Name,
                SubsystemId = ta.cub_SubsystemId,
                Customer = ta.cub_Customer,
                Contact = ta.cub_Contact
            };
        }
    }
}
