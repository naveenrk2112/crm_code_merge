﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Data;
using System.Linq;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using System.Text.RegularExpressions;
using System.Text;
using CUB.MSD.Model.MSDApi;

namespace CUB.MSD.Logic
{

    /// <summary>
    /// Email Logic
    /// </summary>
    public class EmailLogic : LogicBase
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public EmailLogic(IOrganizationService service) : base(service)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <param name="executionContext">MSD plugin execution contect</param>
        public EmailLogic(IOrganizationService service, IPluginExecutionContext executionContext) : base(service, executionContext)
        {
        }

        /// <summary>
        /// Validate Email Address
        /// </summary>
        /// <param name="emailAddress">Email address</param>
        /// <returns>Email is valid or not</returns>
        public bool ValidateEmailAddress(String emailAddress)
        {
            const string methodName = "ValidateEmailAddress";
            try
            {
                if ((String.IsNullOrEmpty(emailAddress.Trim())))
                {
                    return true;
                }


                //string eml_exp = "^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?$";
                //var glogic = new GlobalsLogic(_organizationService);
                //string eml_exp = glogic.GetAttributeValue("ValidateEmailFormat", "EmailMatchRegex");
                //string eml_exp=GlobalsCache.GetKeyFromCache(_organizationService, "ValidateEmailFormat", "EmailMatchRegex");
                Regex regex = new Regex(GlobalsCache.EmailMatchRegEX(_organizationService), RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture | RegexOptions.Compiled);

                return regex.IsMatch(emailAddress.Trim());
            }
            catch (Exception Ex)
            {
                CubLogger.Error(methodName, Ex);
                return false;
            }
        }

        /// <summary>
        /// Is email valid
        /// </summary>
        /// <param name="contact">Contact</param>
        /// <returns>Email is valid or not</returns>
        public CubResponse<WSCustomerContact> IsEmailValid(WSCustomerContact contact)
        {
            const string methodName = "IsEmailValid";
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            try
            {
                if (String.IsNullOrEmpty(contact.email.Trim()))
                {
                    response.Errors.Add(new WSMSDFault("Email", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }

                if (!UtilityLogic.IsStringLengthValid(contact.contactType, 100))
                {
                    response.Errors.Add(new WSMSDFault("Email", CUBConstants.Error.ERR_GENERAL_MUST_BE_STD_EMAIL_FRMT, CUBConstants.Error.MSG_MUST_BE_STD_EMAIL_FRMT));
                    return response;
                }


                if (!UtilityLogic.isValidEmailFormat(contact.email.Trim()))
                {
                    response.Errors.Add(new WSMSDFault("Email", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                response.ResponseObject = contact;
            }
            catch (Exception Ex)
            {
                CubLogger.Error(methodName, Ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, Ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Clean Up Gmail Address
        /// </summary>
        /// <param name="emailAddress">Email Address</param>
        /// <returns>Gmail address cleared</returns>
        public string CleanUpGmailAddress(string emailAddress)
        {
            string[] arrEmailAddy = emailAddress.Split('@');
            string userName = arrEmailAddy.ElementAtOrDefault(0);
            string domain = arrEmailAddy.ElementAtOrDefault(1);

            if (userName.Contains('+'))
            {
                string[] arrUserName = userName.Split('+');
                userName = arrUserName.ElementAtOrDefault(0);
            }

            if (userName.Contains('.'))
                userName = userName.Replace(".", "");

            return userName + '@' + domain;
        }

        /// <summary>
        /// Validate if an email address is not in use on another contact
        /// </summary>
        /// <param name="emailAddress">Email address</param>
        /// <param name="conactID">Contact ID</param>
        /// <returns>Response</returns>
        public string ValidateEmailAddressNotInUseOnOtherContact(string emailAddress, string conactID)
        {
            CubLogger.Error("In EmailLogic:ValidateEmailAddressNotInUseOnOtherContact: ");

            try
            {
                // Only validate non-empty email addresses
                if (!String.IsNullOrEmpty(emailAddress.Trim()))
                {
                    string gmailAddress = null;

                    //If Gmail -- 'clean up' prior to validation
                    if (emailAddress.Contains("gmail.com"))
                    {
                        gmailAddress = CleanUpGmailAddress(emailAddress);
                    }

                    Contact contactIdWithMatchingEmail = GetEmailsByAddress(emailAddress, gmailAddress, conactID);


                    //                    if (contactIdWithMatchingEmail != null && contactIdWithMatchingEmail.ContactId.Value != new Guid(contextID)) // exclude if contact updates email with same email
                    if (contactIdWithMatchingEmail != null)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("ID:" + contactIdWithMatchingEmail.ContactId.ToString() + ":"); // return the contactid

                        if (string.IsNullOrEmpty(gmailAddress))
                        {
                            sb.AppendLine(string.Format("The email address {0} is already in use.", emailAddress));
                        }
                        else
                        {
                            string[] arrEmailAddy = emailAddress.Split('@');
                            string userName = arrEmailAddy.ElementAtOrDefault(0);

                            if (userName.Contains('.') || userName.Contains('+'))
                                sb.AppendLine(string.Format("The email address {0} equates to {1} which is already in use.", emailAddress, gmailAddress));
                            else
                                sb.AppendLine(string.Format("The email address {0} is already in use.", emailAddress));
                        }

                        return sb.ToString();
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                CubLogger.Error("", ex);
                return ex.Message;
            }
        }

        /// <summary>
        /// Get Email by Address
        /// </summary>
        /// <param name="emailAddress">Email Address</param>
        /// <param name="gmailAddress">Gmail address</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns>Contact</returns>
        public Contact GetEmailsByAddress(string emailAddress, string gmailAddress, string contactID)
        {
            if (contactID == "")
                contactID = "00000000-0000-0000-0000-000000000000"; // can't pass blank guid to linq

            Contact contact = (from con in CubOrgSvc.ContactSet
                               where con.EMailAddress1 == emailAddress || con.EMailAddress2 == emailAddress || con.EMailAddress3 == emailAddress
                               where con.ContactId.Value != new Guid(contactID) // exclude contact if updating
                               select new Contact
                               {
                                   ContactId = con.ContactId.Value
                               }).FirstOrDefault();

            if (contact != null)
                return contact;

            if (!String.IsNullOrEmpty(gmailAddress))
            {
                Contact contact2 = (from con in CubOrgSvc.ContactSet
                                    where con.EMailAddress1 == gmailAddress || con.EMailAddress2 == gmailAddress || con.EMailAddress3 == gmailAddress
                                    select new Contact
                                    {
                                        ContactId = con.ContactId.Value
                                    }).FirstOrDefault();

                if (contact2 != null)
                    return contact;

            }

            return null;
        }
    }
}
