/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.Xrm.Sdk.Messages;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Microsoft.Crm.Sdk.Messages;
using System.Net.Http;

namespace CUB.MSD.Logic
{
    public class SubsystemRequestJsonModel
    {
        public string PhoneNumber { get; set; }
        public string Regarding { get; set; }
        public bool IsVerified { get; set; }
        public List<string> Alerts { get; set; }
    }

    public class GetCasesRelatedToSubsystemIdResponseModel
    {
        public IEnumerable<WSMSDEntity> RelatedCases { get; set; }
        public string Error { get; set; }
    }

    public class SubsystemLogic : RestApiBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">Organization Service</param>
        public SubsystemLogic(IOrganizationService service) : base(service)
        {
        }

        /// <summary>
        /// Retrieves a list of active users, excluding the user making this call
        /// </summary>
        /// <param name="userId">The Guid of the user making this call/param>
        /// <returns>RetrieveActiveUsersResponseModel with an Error (string) if session could not be found or multiple attributes when data is returned
        /// </returns>
        public virtual GetCasesRelatedToSubsystemIdResponseModel GetCasesRelatedToSubsystemId(string subsystemId)
        {
            try
            {
                var relatedCasesFetch = 
                    $@"<fetch>
	                    <entity name='incident'>
                            <all-attributes/>
                            <filter type='and'>
                                <condition attribute='statecode' operator='eq' value='0' />
                                <condition attribute='cub_subsystemid' operator='eq' value='{subsystemId}' />
                            </filter>
                        </entity>
                    </fetch>";

                var relatedCasesResponse = this._organizationService.RetrieveMultiple(new FetchExpression(relatedCasesFetch));
                var relatedCases = relatedCasesResponse.Entities.ToList();
                var relatedCasesToReturn = new List<WSMSDEntity>();
                foreach(var relatedCase in relatedCases)
                {
                    var changedCase = new WSMSDEntity();
                    changedCase.Id = relatedCase.Id;
                    changedCase.LogicalName = relatedCase.LogicalName;                    
                    foreach(var attribute in relatedCase.Attributes)
                    {
                        changedCase.Attributes.Add(attribute.Key, attribute.Value);
                        if (relatedCase.FormattedValues.Contains(attribute.Key))
                        {
                            changedCase.Attributes[attribute.Key] = relatedCase.FormattedValues[attribute.Key];
                        }
                    }
                    relatedCasesToReturn.Add(changedCase);
                }
                return new GetCasesRelatedToSubsystemIdResponseModel()
                {
                    RelatedCases = relatedCasesToReturn
                };
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 2.11.3 subsystem/<subsystem-id>/traveltoken/canbelinked POST
        ///  This API is used to verify if the token exists in the subsystem and check if its eligible to be linked or not. 
        ///  If the token is not in the sub-system, based on the configuration if it is okay to automatically register in the subsystem, 
        ///  it will complete the token registration.
        ///  The API also returns information for the client to verify if the customer is indeed owner of the token.
        /// </summary>
        /// <param name="subsystemId">(Required) Unique identifier for the subsystem where the travel token is registered.</param>
        /// <param name="travelToken">(Required) Identifier of the travel token.</param>
        /// <param name="oneAccountId">(Optional) Unique identifier of the OneAccount if the customer has a OneAccount by this point.</param>
        /// <param name="foreignCustomerId">(Optional) Unique identifier for the foreign customer.</param>
        /// <param name="foreignTokenSubsystem">(Optional) Unique identifier for the foreign token subsystem if the token is owned by a different subsystem than the account-based subsystem.</param>
        /// <param name="autoCreateFlag">(Required) To indicate if the token should be auto created in the subsystem in case it is not yet created. For certain token types the system will not allow auto-create. In this case the autoCreateFlag will be ignored.</param>
        /// <returns>client to verify if the customer is indeed owner of the token.</returns>

        public MvcGenericResponse TravelTokenCanBeLinked(string tokenType, string subsystemId, string encryptedToken, string nonce,
                                                         int? oneAccountId, string foreignCustomerId, string foreignTokenSubsystem, bool autoCreateFlag)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISSubsystemTravelTokenCanBeLinkedPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, subsystemId);

                var fullEncryptedToken = new
                {
                    nonce = nonce,
                    payments = new
                    {
                        creditCard = new
                        {
                            jweEnryptedCardData = encryptedToken
                        }
                    }
                };
                CubLogger.InfoFormat("fullEncryptedToken: {0}", JsonConvert.SerializeObject(fullEncryptedToken));

                // Converting to Base64
                string fullEncryptedTokenAsString = JsonConvert.SerializeObject(fullEncryptedToken);
                byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(fullEncryptedTokenAsString);
                string fullEncryptedTokenAsBase64 = Convert.ToBase64String(toEncodeAsBytes);

                var requestBody = new
                {
                    travelToken = new
                    {
                        tokenType = "Bankcard",
                        encryptedToken = fullEncryptedTokenAsBase64,
                        keyName = GlobalsCache.AngularGetJweKeyName(_organizationService)
                    },
                    autoCreateFlag = autoCreateFlag
                };

                CubLogger.InfoFormat("requestBody: {0}", JsonConvert.SerializeObject(requestBody));

                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(requestBody));
                RestResponse resp = HttpClientPost(url, httpContent);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                wsresp.Header.errorMessage = ex.Message;
            }
            return wsresp;
        }

        public MvcGenericResponse BlockSubsystemTransitAccount(string subsystemId, string accountNumber,
                                                   string reasonCode, string notes)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISSubssytemBlockAccountPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, subsystemId, accountNumber);
                var request = new
                {
                    reasonCode = reasonCode,
                    notes = notes
                };
                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(request));
                RestResponse resp = HttpClientPost(url, httpContent);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// 2.12.6 subsystem/<subsystem-id>/subsystemaccount/<account-ref>/unblock POST
        ///  This API is used to unblock the subsystem account.
        /// </summary>
        /// <param name="subsystemId">(Required) Unique identifier for the subsystem where the travel token is registered.</param>
        /// <param name="accountNumber">(Required) Account number of the travel token associated within the given subsystem.</param>
        /// <param name="reasonCode">(Required)  Reason code for the blocking.</param>    
        /// <returns>No data is returned by this method.Common headers and http response codes are returned.</returns>
        public MvcGenericResponse UnblockSubsystemTransitAccount(string subsystemId, string accountNumber,
                                                         string reasonCode, string notes)
        {
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                string baseURL = GlobalsCache.NISApiBaseURL(_organizationService);
                string actionURL = GlobalsCache.NISSubssytemUnblockAccountPath(_organizationService).ToString();
                string url = baseURL + actionURL;
                url = string.Format(url, subsystemId,accountNumber);
                var request = new
                {
                    reasonCode = reasonCode,
                    notes = notes
                };

                StringContent httpContent = new StringContent(JsonConvert.SerializeObject(request));
                RestResponse resp = HttpClientPost(url, httpContent);
                if (resp.hdr.result == RestConstants.SUCCESSFUL)
                {
                    if (!string.IsNullOrEmpty(resp.responseBody))
                        wsresp.Body = resp.responseBody;
                    wsresp.Header = resp.hdr;
                }
                else
                {
                    string msg = $"{resp.hdr.fieldName} - {resp.hdr.errorKey} - {resp.hdr.errorMessage}";
                    CubLogger.Error(msg);
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }
            return wsresp;
        }

    }
}