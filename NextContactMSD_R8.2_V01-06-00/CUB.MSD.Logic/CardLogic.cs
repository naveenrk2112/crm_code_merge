﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model.NIS;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// Card Logic
    /// </summary>
    public class CardLogic : LogicBase
    {
        IOrganizationService _service;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public CardLogic(IOrganizationService service) : base(service)
        {
            _service = service;
        }


        /// <summary>
        /// Search Token
        /// </summary>
        /// <param name="subSystem">subSystem</param>
        /// <param name="tokenType">tokenType</param>
        /// <param name="pan">pan</param>
        /// <param name="expMonth">expMonth</param>
        /// <param name="expYear">expYear</param>
        /// <returns>Token JSON response</returns>
        public virtual string SearchToken(string subSystem, string tokenType, string pan, string expMonth, string expYear,
                                          string encryptedToken, string nonce)
        {
            NISApiLogic nisApi = new NISApiLogic(_service);
            if (tokenType != RestConstants.TokenTypes.bankCard || string.IsNullOrWhiteSpace(encryptedToken))
            {
                //trim all white spaces in card number
                pan = System.Text.RegularExpressions.Regex.Replace(pan, @"\s+", "");

                //trim all non-digits in card number
                pan = System.Text.RegularExpressions.Regex.Replace(pan, @"\D+", "");

                //If not all search parameters supplied, return null
                if (string.IsNullOrEmpty(subSystem) || string.IsNullOrEmpty(tokenType) || string.IsNullOrEmpty(pan)
                        || string.IsNullOrEmpty(expMonth) || string.IsNullOrEmpty(expYear))
                    return null;

                //Make a month two digit by appending zero at the beginning for Months from Jan to Sep
                if (expMonth.Length < 2) expMonth = "0" + expMonth;

                //if year is provided in 4 digits, make it two digit
                if (expYear.Length == 4) expYear = expYear.Substring(0, 2);

               
                string response = nisApi.SearchToken(subSystem, tokenType, pan, string.Format("{0}{1}", expMonth, expYear));

                return response;

            }
            else
            {
                CubLogger.InfoFormat("SearchCardEncrypted: subSystem= {0}, encryptedToken= {1}, nonce={2}", 
                                     subSystem, encryptedToken, nonce);
                string response = nisApi.SearchBankCardEntrypted(subSystem, encryptedToken, nonce);
                return response;
            }
        }
    }
}
