/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUB.MSD.Web.Models;
using CUB.MSD.Model.MSDApi;
using System.Text.RegularExpressions;
using Microsoft.Xrm.Sdk.Messages;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Microsoft.Crm.Sdk.Messages;
using System.ServiceModel;
using CUB.MSD.Model.NIS;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Numerics;

namespace CUB.MSD.Logic
{
    public class TransferSessionResponseModel
    {
        public string Error { get; set; }
    }

    public class RetrieveActiveUsersResponseModel
    {
        public string Error { get; set; }

        public Dictionary<string, string> Users { get; set; }

        public RetrieveActiveUsersResponseModel()
        {
            Users = new Dictionary<string, string>();
        }
    }

    public class SessionLogic : LogicBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">Organization Service</param>
        public SessionLogic(IOrganizationService service) : base(service)
        {

        }

        /// <summary>
        /// Find and return the most-recently created active session filtered by a single field.
        /// </summary>
        /// <param name="filterAttribute">The logical name of the Session attribute on which to filter</param>
        /// <param name="filterValue">The value by which to filter Sessions</param>
        /// <returns>A session record or null if none found.</returns>
        private cub_CSRSession FindActiveSessionByAttributeValue(string filterAttribute, object filterValue)
        {
            var fetchXml = $@"
				<fetch mapping='logical' count='1'>
					<entity name='cub_csrsession'>
						<attribute name='cub_csrsessionid' />
                        <attribute name='cub_name' />
						<attribute name='createdon' />
						<attribute name='cub_transitaccount' />
						<attribute name='cub_isverified' />
						<attribute name='cub_isregistered' />
						<attribute name='cub_isprimarycontact' />
						<attribute name='cub_customerid' />
						<attribute name='cub_contactid' />                        
                        <attribute name='createdby' />
                        <attribute name='cub_sessionchannel' />
                        <attribute name='cub_enddate' />
                        <attribute name='cub_notes' />
                        <attribute name='cub_transcripturl' />
                        <attribute name='modifiedby' />
                        <attribute name='modifiedon' />
                        <attribute name='statuscode'/>
                        <order attribute='createdon' descending='true' />
						<filter type='and'>
							<condition attribute='statecode' operator='eq' value='{(int)cub_CSRSessionState.Active}' />
                            <condition attribute='{filterAttribute}' operator='eq' value='{filterValue}' />
						</filter>
						<link-entity name='cub_transitaccount' from='cub_transitaccountid' to='cub_transitaccount' visible='false' link-type='outer' alias='transitaccount'>
							<attribute name='cub_name' />
							<attribute name='cub_subsystemid' />
						</link-entity>
						<link-entity name='account' from='accountid' to='cub_customerid' visible='false' link-type='outer' alias='customer'>
							<attribute name='name' />
						</link-entity>
						<link-entity name='contact' from='contactid' to='cub_contactid' visible='false' link-type='outer' alias='contact'>
							<attribute name='lastname' />
							<attribute name='firstname' />
						</link-entity>
                        <link-entity name='systemuser' from='systemuserid' to='modifiedby' link-type='outer' alias='modifeduser'>
			                <attribute name='fullname'/>
		               </link-entity>
                       <link-entity name='systemuser' from='systemuserid' to='createdby' link-type='outer' alias='createduser'>
			                <attribute name='fullname'/>
		               </link-entity>
					</entity>
				</fetch>";
            return _organizationService.RetrieveMultiple(new FetchExpression(fetchXml))
                .Entities.FirstOrDefault()?.ToEntity<cub_CSRSession>();
        }

        /// <summary>
        /// Get child sessions linked to parent session
        /// </summary>
        /// <param name="parentSessionId">The parent session Id</param>
        /// <returns>A list of linked session records or null if none found.</returns>
        private List<cub_csrlinkedsession> FindLinkedSessions(Guid parentSessionId)
        {
            var fetchXml = $@"
				<fetch mapping='logical'>
					<entity name='cub_csrlinkedsession'>
						<attribute name='cub_csrlinkedsessionid' />
                        <attribute name='cub_name' />
						<attribute name='createdon' />
						<attribute name='createdby' />
						<attribute name='modifiedon' />
						<attribute name='modifiedby' />
                        <attribute name='cub_childsession'/>
                        <order attribute='createdon' descending='true' />
						<filter type='and'>
							<condition attribute='cub_parentsession' operator='eq' value='{parentSessionId}' />
						</filter>
						<link-entity name='cub_csrsession' from='cub_csrsessionid' to='cub_childsession' visible='false' link-type='inner' alias='childsession'>
							<attribute name='cub_csrsessionid' />
                            <attribute name='createdon'/>
                            <attribute name='cub_enddate'/>
                            <attribute name='cub_name'/>	
                            <attribute name='cub_sessionchannel' />	
                            <attribute name='statuscode'/>	
                            <attribute name='modifiedby'/>
                            <attribute name='createdby'/>	
                            <attribute name='modifiedon' />	                            
						</link-entity>
					</entity>
				</fetch>";
            var res = _organizationService.RetrieveMultiple(new FetchExpression(fetchXml));
            if (res != null)
                return res.Entities.Select(c => c.ToEntity<cub_csrlinkedsession>()).ToList();
            else
                return null;
        }

        /// <summary>
        /// Find session detail by session id
        /// </summary>
        /// <param name="sessionId">The session Id</param>
        /// <returns>A session record or null if none found.</returns>
        private cub_CSRSession FindSessionById(Guid sessionId)
        {
            
            var fetchXml = $@"
				<fetch mapping='logical' count='1'>
					<entity name='cub_csrsession'>
						<attribute name='cub_csrsessionid' />
                        <attribute name='cub_name' />
						<attribute name='createdon' />
						<attribute name='cub_transitaccount' />
						<attribute name='cub_isverified' />
						<attribute name='cub_isregistered' />
						<attribute name='cub_isprimarycontact' />
						<attribute name='cub_customerid' />
						<attribute name='cub_contactid' />
                        <attribute name='createdby' />
                        <attribute name='cub_sessionchannel' />
                        <attribute name='cub_enddate' />
                        <attribute name='cub_notes' />
                        <attribute name='cub_transcripturl' />
                        <attribute name='modifiedby' />
                        <attribute name='modifiedon' />
                        <attribute name='statuscode'/>
                        <order attribute='createdon' descending='true' />
						<filter type='and'>
							<condition attribute='cub_csrsessionid' operator='eq' value='{sessionId}' />
						</filter>
						<link-entity name='cub_transitaccount' from='cub_transitaccountid' to='cub_transitaccount' visible='false' link-type='outer' alias='transitaccount'>
							<attribute name='cub_name' />
							<attribute name='cub_subsystemid' />
						</link-entity>
						<link-entity name='account' from='accountid' to='cub_customerid' visible='false' link-type='outer' alias='customer'>
							<attribute name='name' />
						</link-entity>
						<link-entity name='contact' from='contactid' to='cub_contactid' visible='false' link-type='outer' alias='contact'>
							<attribute name='lastname' />
							<attribute name='firstname' />
						</link-entity>
                       <link-entity name='systemuser' from='systemuserid' to='modifiedby' link-type='outer' alias='modifeduser'>
			                <attribute name='fullname'/>
		               </link-entity>
                       <link-entity name='systemuser' from='systemuserid' to='createdby' link-type='outer' alias='createduser'>
			                <attribute name='fullname'/>
		               </link-entity>
					</entity>
				</fetch>";
            return _organizationService.RetrieveMultiple(new FetchExpression(fetchXml))
                .Entities.FirstOrDefault()?.ToEntity<cub_CSRSession>();           

        }


        /// <summary>
        /// Find custom CRM activities linked to session
        /// </summary>
        /// <param name="sessionId">The session id</param>
        /// <returns>List of MSD custom activities or null if none found.</returns>
        private List<cub_CustomActivity>FindActivitiesLinkedToSession(Guid sessionId)
        {
            var fetchXml = $@"
				<fetch>
	                <entity name='cub_customactivity'>
		                <attribute name='createdon' />
                        <attribute name='cub_groupid'/>
                        <attribute name='cub_channel'/>
                        <attribute name='cub_activitytype'/>
                        <attribute name='cub_activitycategory'/>
                        <attribute name='cub_activitysubject'/>
                        <attribute name='createdby'/>
                        <attribute name='cub_session'/>
`                        <attribute name='statuscode'/>`                       
                        <order attribute='createdon' descending='true' />
						        <filter type='and'>
							        <condition attribute='cub_session' operator='eq' value='{sessionId}' />
						        </filter>
                        <link-entity name='systemuser' from='systemuserid' to='createdby' link-type='outer' alias='createduser'>
                            <attribute name='fullname'/>
                        </link-entity>
        
	                </entity>
                </fetch>";

            var res =  _organizationService.RetrieveMultiple(new FetchExpression(fetchXml));
            if (res != null)
                return res.Entities.Select(c => c.ToEntity<cub_CustomActivity>()).ToList();
            else
                return null;
            
        }

        /// <summary>
        /// Find CSR session by session number
        /// </summary>
        /// <param name="sessionNumber">The session id</param>
        /// <returns>A session record or null if none found.</returns>
        private cub_CSRSession FindSessionByNumber(string sessionNumber)
        {
            var fetchXml = $@"
				<fetch>
	               <entity name='cub_csrsession'>
						<attribute name='cub_csrsessionid' />
                        <attribute name='cub_name' />
						<attribute name='createdon' />
						<attribute name='cub_transitaccount' />
						<attribute name='cub_isverified' />
						<attribute name='cub_isregistered' />
						<attribute name='cub_isprimarycontact' />
						<attribute name='cub_customerid' />
						<attribute name='cub_contactid' />
                        <attribute name='createdby' />
                        <attribute name='cub_sessionchannel' />
                        <attribute name='cub_enddate' />
                        <attribute name='cub_notes' />
                        <attribute name='cub_transcripturl' />
                        <attribute name='modifiedby' />
                        <attribute name='modifiedon' />
                        <attribute name='statuscode'/>
`                        <attribute name='statuscode'/>`                     
						        <filter type='and'>
							        <condition attribute='cub_name' operator='eq' value='{sessionNumber}' />
						        </filter>
                        <link-entity name='systemuser' from='systemuserid' to='modifiedby' link-type='outer' alias='modifeduser'>
			                <attribute name='fullname'/>
		               </link-entity>
                       <link-entity name='systemuser' from='systemuserid' to='createdby' link-type='outer' alias='createduser'>
			                <attribute name='fullname'/>
		               </link-entity>
	                </entity>
                </fetch>";

            return _organizationService.RetrieveMultiple(new FetchExpression(fetchXml))
                        .Entities.FirstOrDefault()?.ToEntity<cub_CSRSession>();
            

        }

        /// <summary>
        /// Find CRM cases linked to session
        /// </summary>
        /// <param name="sessionId">The session id</param>
        /// <returns>A list of cases or null if none found.</returns>
        private List<Incident> FindCasesLinkedToSession(Guid sessionId)
        {
            var fetchXml = $@"
				<fetch>
	                <entity name='incident'>
		               <attribute name='createdon' />
                        <attribute name='ticketnumber'/>
                        <attribute name='prioritycode'/>
                        <attribute name='caseorigincode'/>
                        <attribute name='prioritycode'/>
                        <attribute name='statuscode'/>
                        <attribute name='customerid'/>
                        <attribute name='ownerid'/>
                        <attribute name='title'/>
                        <attribute name='isescalated'/>
                        <attribute name='cub_sessionid'/>
                        <order attribute='createdon' descending='true' />
						        <filter type='and'>
							       <condition attribute='cub_sessionid' operator='eq' value='{sessionId}' />
						        </filter>
                        <link-entity name='systemuser' from='systemuserid' to='createdby' link-type='outer' alias='createduser'>
                            <attribute name='fullname'/>
                        </link-entity>        
	                </entity>
                </fetch>";
            // <condition attribute='cub_sessionid' operator='null' />
            var res = _organizationService.RetrieveMultiple(new FetchExpression(fetchXml));
            if (res != null)
               return res.Entities.Select(c => c.ToEntity<Incident>()).ToList();
            else
                return null;
        }

        /// <summary>
        /// Find and return an active session related to the given user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>WSSessionResponse</returns>
        public WSSessionResponse RetrieveSession(Guid userId)
        {
            try
            {
                var session = FindActiveSessionByAttributeValue(cub_CSRSession.owneridAttribute, userId);
                var response = MapSessionToResponse(session);
                return response;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Find and return session by session number.
        /// </summary>
        /// <param name="sessionId">Session Id</param>
        /// <returns>WSSessionResponse</returns>
        public WSSessionResponse GetSessionDetail(Guid sessionId)
        {
            const string methodName = "GetSessionDetail";
            try
            {
                var session = FindSessionById(sessionId);
                var response = MapSessionToResponse(session);
                response.activities =JsonConvert.SerializeObject(MapSessionActvitiesToResponse(FindActivitiesLinkedToSession(sessionId)));
                response.cases = JsonConvert.SerializeObject(MapSessionCasesToResponse(FindCasesLinkedToSession(sessionId)));
                return response;
            }
            catch(Exception ex)
            {
                CubLogger.Error(methodName, ex);
                return null;
            }
        }

        /// <summary>
        /// Find and return child sessions linked to parent.
        /// </summary>
        /// <param name="parentSessionId">Parent Session Id</param>
        /// <returns>WSSessionResponse</returns>
        public MvcGenericResponse GetLinkedSessions(Guid parentSessionId)
        {
            const string methodName = "GetLinkedSessions";
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                var sessions = FindLinkedSessions(parentSessionId);

                if (sessions != null && sessions.Count > 0)
                {
                    wsresp.Body = JsonConvert.SerializeObject(MapLinkedSessionToResponse(sessions));
                    wsresp.Header.result = RestConstants.SUCCESSFUL;
                }
                             
            }
            catch (Exception ex)
            {
                CubLogger.Error(methodName, ex);
                wsresp.Header.result = RestConstants.FAILED;
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// Retrieves a list of active users, excluding the user making this call
        /// </summary>
        /// <param name="userId">The Guid of the user making this call/param>
        /// <returns>RetrieveActiveUsersResponseModel with an Error (string) if session could not be found or multiple attributes when data is returned
        /// </returns>
        public virtual RetrieveActiveUsersResponseModel RetrieveActiveUsers(Guid userId)
        {
            const string methodName = "RetrieveActiveUsers";
            try
            {
                if (userId == default(Guid))
                {
                    return new RetrieveActiveUsersResponseModel()
                    {
                        Error = "Missing or incorrect parameters provided"
                    };
                }

                var activeUsersFetch = String.Format(@"
                                        <fetch>
                                          <entity name='systemuser'>
                                            <attribute name='fullname' />
                                            <attribute name='systemuserid' />
                                            <order attribute='fullname' descending='false' />
                                            <filter type='and'>
                                              <condition attribute='isdisabled' operator='eq' value='0' />
                                              <condition attribute='systemuserid' operator='ne' value='{0}' />
                                            </filter>
                                          </entity>
                                        </fetch>", userId);

                EntityCollection activeUsersResponse = this._organizationService.RetrieveMultiple(new FetchExpression(activeUsersFetch));

                return new RetrieveActiveUsersResponseModel()
                {
                    Users = activeUsersResponse.Entities.ToDictionary(x => x.Id.ToString(), y => y.ToEntity<SystemUser>().FullName)
                };
            }
            catch (Exception ex)
            {
                CubLogger.Error(methodName, ex);
                throw;
            }
        }

        /// <summary>
        /// Creates a session for the current user.
        /// </summary>
        /// <param name="userId">The Guid of the user record to create the session for</param>
        /// <param name="recordId">The Guid of the record to create the session for</param>
        /// <param name="entityName">The type of the record to create the session for. Only valid input is "contact" and "account"</param>
        /// <returns>WSSessionResponse</returns>
        public WSSessionResponse CreateSession(Guid userId, Guid? recordId, string entityName, string channelName)
        {
            if (!string.IsNullOrEmpty(entityName) && recordId == null)
            {
                throw new ArgumentException($"No record ID provided for entity {entityName}");
            }

            switch (entityName)
            {
                case Account.EntityLogicalName:
                    return CreateSessionFromCustomer(recordId.Value,channelName);
                case Contact.EntityLogicalName:
                    return CreateSessionFromContact(recordId.Value,channelName);
                case cub_TransitAccount.EntityLogicalName:
                    return CreateSessionFromTransitAccount(recordId.Value,channelName);
            }

            // Create session without filling in any fields
            var response = new WSSessionResponse
            {
                startDateTime = DateTime.UtcNow
            };

            try
            {
                var session = new cub_CSRSession();
                response.sessionId = _organizationService.Create(session);
                response.sessionNumber = (from s in CubOrgSvc.cub_CSRSessionSet
                                          where s.cub_CSRSessionId.Equals(response.sessionId)
                                          select s.cub_name).FirstOrDefault();
                return response;
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                CubLogger.Error("Error creating new cub_CsrSession record", ex);
                throw new MSDException(ex);
            }
        }

        private WSSessionResponse CreateSessionFromCustomer(Guid customerId, string channelName)
        {
            WSSessionResponse response = null;
           
            try
            {
                response = (from account in CubOrgSvc.AccountSet
                            join contact in CubOrgSvc.ContactSet
                                on account.Id equals contact.ParentCustomerId.Id
                            join contactType in CubOrgSvc.cub_ContactTypeSet
                                on contact.cub_ContactTypeId.Id equals contactType.Id
                            where account.AccountId.Equals(customerId)
                            where contactType.cub_Name.Equals("Primary")
                            select new WSSessionResponse
                            {
                                customerId = account.AccountId,
                                customerFirstName = account.Name.Trim().Split(' ').First().Trim(),
                                customerLastName = account.Name.Trim().Split(' ').Last().Trim(),
                                contactId = contact.ContactId,
                                contactFirstName = contact.FirstName,
                                contactLastName = contact.LastName,
                                isRegistered = true,
                                isPrimaryContact = true,
                                channelName = channelName
                            }).FirstOrDefault();
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                CubLogger.Error("Error retrieving data by customer ID", ex);
                throw new MSDException(ex);
            }

            if (response == null)
            {
                var message = "Unable to find matching customer in MSD";
                CubLogger.Error(message);
                throw new InvalidOperationException(message);
            }

            var contactLogic = new ContactLogic(_organizationService);
            if (response.contactId.HasValue)
            {
                response.isVerified = contactLogic.IsRecentlyVerified(response.contactId.Value);
            }
           

            response.startDateTime = DateTime.UtcNow;

            cub_channel activityChannelEnum;
            if (!Enum.TryParse(channelName, out activityChannelEnum))
                activityChannelEnum = cub_channel.CRM;

        
            var session = new cub_CSRSession
            {
                cub_CustomerId = new EntityReference(Account.EntityLogicalName, customerId),
                cub_ContactId = response.contactId.HasValue
                    ? new EntityReference(Contact.EntityLogicalName, response.contactId.Value)
                    : null,
                cub_IsRegistered = response.isRegistered,
                cub_IsPrimaryContact = response.isPrimaryContact,
                cub_IsVerified = response.isVerified,
                cub_SessionChannel = new OptionSetValue(Convert.ToInt32(activityChannelEnum))
            };
            try
            {
                response.sessionId = _organizationService.Create(session);
                response.sessionNumber = (from s in CubOrgSvc.cub_CSRSessionSet
                                          where s.cub_CSRSessionId.Equals(response.sessionId)
                                          select s.cub_name).FirstOrDefault();
                return response;
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                CubLogger.Error("Error creating new cub_CsrSession record from customer", ex);
                throw new MSDException(ex);
            }
        }

        private WSSessionResponse CreateSessionFromContact(Guid contactId, string channelName)
        {
            WSSessionResponse response;
            try
            {
                response = (from contact in CubOrgSvc.ContactSet
                            join account in CubOrgSvc.AccountSet
                               on contact.ParentCustomerId.Id equals account.Id
                            join contactType in CubOrgSvc.cub_ContactTypeSet
                               on contact.cub_ContactTypeId.Id equals contactType.Id
                            where contact.ContactId.Equals(contactId)
                            select new WSSessionResponse
                            {
                                customerId = account.AccountId,
                                customerFirstName = account.Name.Trim().Split(' ').First().Trim(),
                                customerLastName = account.Name.Trim().Split(' ').Last().Trim(),
                                contactId = contact.ContactId,
                                contactFirstName = contact.FirstName,
                                contactLastName = contact.LastName,
                                isRegistered = true,
                                isPrimaryContact = contactType.cub_Name.Equals("Primary"),
                                channelName= channelName

                            }).FirstOrDefault();
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                CubLogger.Error("Error retrieving data by contact ID", ex);
                throw new MSDException(ex);
            }

            if (response == null)
            {
                var message = "Unable to find matching contact in MSD";
                CubLogger.Error(message);
                throw new InvalidOperationException(message);
            }

            var contactLogic = new ContactLogic(_organizationService);
            response.isVerified = contactLogic.IsRecentlyVerified(contactId);

            response.startDateTime = DateTime.UtcNow;

            cub_channel activityChannelEnum;
            if (!Enum.TryParse(channelName, out activityChannelEnum))
                activityChannelEnum = cub_channel.CRM;

            var session = new cub_CSRSession
            {
                cub_CustomerId = response.customerId.HasValue
                    ? new EntityReference(Account.EntityLogicalName, response.customerId.Value)
                    : null,
                cub_ContactId = new EntityReference(Contact.EntityLogicalName, contactId),
                cub_IsPrimaryContact = response.isPrimaryContact,
                cub_IsRegistered = response.isRegistered,
                cub_IsVerified = response.isVerified,
                cub_SessionChannel = new OptionSetValue(Convert.ToInt32(activityChannelEnum))
            };

            try
            {
                response.sessionId = _organizationService.Create(session);
                response.sessionNumber = (from s in CubOrgSvc.cub_CSRSessionSet
                                          where s.cub_CSRSessionId.Equals(response.sessionId)
                                          select s.cub_name).FirstOrDefault();
                return response;
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                CubLogger.Error("Error creating new cub_CsrSession record from contact", ex);
                throw new MSDException(ex);
            }
        }

        private WSSessionResponse CreateSessionFromTransitAccount(Guid transitAccountId,string channelName)
        {
            cub_TransitAccount transitAccount = null;
            var fetchXml = $@"
                <fetch count='1' >
                    <entity name='cub_transitaccount' >
                        <attribute name='cub_transitaccountid' />
                        <attribute name='cub_name' />
                        <attribute name='cub_subsystemid' />
                        <attribute name='cub_customer' />
                        <link-entity name='account' from='accountid' to='cub_customer' link-type='outer' alias='customer' >
                            <attribute name='name' />
                            <link-entity name='contact' from='accountid' to='accountid' link-type='outer' alias='contact' >
                                <attribute name='contactid' />
                                <attribute name='firstname' />
                                <attribute name='lastname' />
                                <link-entity name='cub_contacttype' from='cub_contacttypeid' to='cub_contacttypeid' link-type='outer' />
                            </link-entity>
                        </link-entity>
                        <filter type='and' >
                            <condition attribute='cub_transitaccountid' operator='eq' value='{transitAccountId}' />
                            <filter type='or' >
                                <condition attribute='cub_customer' operator='null' />
                                <condition entityname='cub_contacttype' attribute='cub_name' operator='eq' value='Primary' />
                            </filter>
                        </filter>
                    </entity>
                </fetch>";
            try
            {
                transitAccount = _organizationService.RetrieveMultiple(new FetchExpression(fetchXml))
                    .Entities.FirstOrDefault()?.ToEntity<cub_TransitAccount>();
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                CubLogger.Error("Error retrieving data by transit account ID", ex);
                throw new MSDException(ex);
            }

            if (transitAccount == null)
            {
                var message = "Unable to find matching transit account in MSD";
                CubLogger.Error(message);
                throw new InvalidOperationException(message);
            }

           
            var start = DateTime.UtcNow;

            cub_channel activityChannelEnum;
            if (!Enum.TryParse(channelName, out activityChannelEnum))
                activityChannelEnum = cub_channel.CRM;

            var session = new cub_CSRSession
            {
                cub_TransitAccount = transitAccount.ToEntityReference(),
                cub_IsRegistered = transitAccount.cub_Customer != null,
                cub_CustomerId = transitAccount.cub_Customer,
                cub_SessionChannel = new OptionSetValue(Convert.ToInt32(activityChannelEnum))
            };

            if (transitAccount.cub_Customer != null
                && transitAccount.Contains("contact.contactid"))
            {
                session.cub_IsPrimaryContact = true;
                var contactId = (Guid)transitAccount.GetAttributeValue<AliasedValue>("contact.contactid").Value;
                session.cub_ContactId = new EntityReference(Contact.EntityLogicalName, contactId);
                using (var logic = new ContactLogic(_organizationService))
                {
                    session.cub_IsVerified = logic.IsRecentlyVerified(contactId);
                }
            }

            try
            {
                session.Id = _organizationService.Create(session);
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                CubLogger.Error("Error creating session from transit account", ex);
                throw new MSDException(ex);
            }

            var response = new WSSessionResponse
            {
                sessionId = session.Id,
                startDateTime = start,
                transitAccountId = transitAccount.Id,
                subsystem = transitAccount.cub_SubsystemId,
                subsystemAccountReference = transitAccount.cub_Name,
                isRegistered = session.cub_IsRegistered,
                isPrimaryContact = session.cub_IsPrimaryContact,
                isVerified = session.cub_IsVerified,
                channelName= channelName,
                sessionNumber = (from s in CubOrgSvc.cub_CSRSessionSet
                                 where s.cub_CSRSessionId.Equals(session.Id)
                                 select s.cub_name).FirstOrDefault()
            };
            if (session.cub_IsRegistered.GetValueOrDefault())
            {
                response.customerId = transitAccount.cub_Customer?.Id;
                var customerName = transitAccount.GetAttributeValue<AliasedValue>("customer.name")?.Value as string;
                response.customerFirstName = customerName?.Trim().Split(' ').First().Trim();
                response.customerLastName = customerName?.Trim().Split(' ').Last().Trim();
                response.contactId = session.cub_ContactId?.Id;
                response.contactFirstName = transitAccount.GetAttributeValue<AliasedValue>("contact.firstname")?.Value as string;
                response.contactLastName = transitAccount.GetAttributeValue<AliasedValue>("contact.lastname")?.Value as string;
            }

            return response;
        }

        /// <summary>
        /// Deactivates an active session.
        /// </summary>
        /// <param name="sessionId">The guid of the session record to close</param>
        public void EndSession(Guid sessionId)
        {
            try
            {
                var updateSession = new cub_CSRSession()
                {
                    Id = sessionId,
                    statuscode = new OptionSetValue((int)cub_csrsession_statuscode.Inactive),
                    statecode = cub_CSRSessionState.Inactive,
                    cub_EndDate = DateTime.UtcNow
                };

                _organizationService.Update(updateSession);
            }
            catch (Exception ex)
            {
                CubLogger.Error("Error closing session", ex);
                throw;
            }
        }

        /// <summary>
        /// Update session Notes
        /// </summary>
        /// <param name="sessionId">The guid of the session record</param>
        /// <param name="notes">Notes</param>
        public void UpdateNotes(Guid sessionId, string notes)
        {
            try
            {
                var updateSession = new cub_CSRSession()
                {
                    Id = sessionId,
                    cub_Notes = notes
                };

                _organizationService.Update(updateSession);
            }
            catch (Exception ex)
            {
                CubLogger.Error("Error updating notes", ex);
                throw;
            }
        }

        /// <summary>
        /// Tranfers an active session to another user
        /// </summary>
        /// <param name="sessionId">The guid of the session record to close</param>
        /// <param name="userId">The Guid of the user record transfering the session</param>
        /// <param name="transferToId">The Guid of the user record that you want to transfer the record to</param>
        /// <returns>CloseSessionResponseModel with an Error (string) if the operation did not complete
        /// </returns>
        public virtual TransferSessionResponseModel TransferSession(Guid sessionId, Guid userId, Guid transferToId)
        {
            throw new NotImplementedException("Session transfer not yet implemented");
            //try
            //{
            //    if (sessionId == default(Guid) || userId == default(Guid) || transferToId == default(Guid))
            //    {
            //        return new TransferSessionResponseModel() { Error = "Missing or incorrect parameters provided" };
            //    }

            //    var existingSessionRecord = FindActiveSessionById(sessionId);

            //    if (existingSessionRecord == null)
            //    {
            //        return new TransferSessionResponseModel()
            //        {
            //            Error = "An active session record with that Id does not exist"
            //        };
            //    }

            //    var assignSession = new AssignRequest()
            //    {
            //        Assignee = new EntityReference(SystemUser.EntityLogicalName, transferToId),
            //        Target = existingSessionRecord.ToEntityReference()
            //    };

            //    this._organizationService.Execute(assignSession);

            //    return new TransferSessionResponseModel();
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}
        }

        /// <summary>
        /// Populate and return a list WSSessionResponse object for child session records.
        /// </summary>
        /// <param name="sessions">The list of session records</param>
        /// <returns>List<WSSessionResponse> </returns>
        private List<WSSessionResponse> MapLinkedSessionToResponse(List<cub_csrlinkedsession> sessions)
        {
            if (sessions == null || sessions.Count<1)
            {
                return null;
            }

            List<WSSessionResponse> linkedSessionResponse = new List<WSSessionResponse>();

            foreach (cub_csrlinkedsession s in sessions)
            {
                var response = new WSSessionResponse();                

                if (s.cub_ChildSession != null)
                {
                    response.linkSessionsId = s.cub_csrlinkedsessionId;
                    response.sessionId = s.cub_ChildSession.Id;
                    response.sessionNumber = s.GetAttributeValue<AliasedValue>("childsession.cub_name")?.Value as string;
                    if (s.Attributes.Contains("childsession.createdon") && s.FormattedValues.Contains("childsession.createdon"))
                        response.startDateTime = response.createdOnDateTime = Convert.ToDateTime(s.FormattedValues["childsession.createdon"]);
                    if(s.Attributes.Contains("childsession.cub_enddate") && s.FormattedValues.Contains("childsession.cub_enddate"))
                        response.endDateTime = Convert.ToDateTime(s.FormattedValues["childsession.cub_enddate"]);
                    if (s.Attributes.Contains("childsession.createdby"))
                    {
                       
                        response.createdById = (s.GetAttributeValue<AliasedValue>("childsession.createdby")?.Value as EntityReference).Id;
                        response.createdByUserFullName = (s.GetAttributeValue<AliasedValue>("childsession.createdby")?.Value as EntityReference).Name;
                    }
                    if (s.Attributes.Contains("childsession.modifiedby"))
                    {
                        response.modifiedById = (s.GetAttributeValue<AliasedValue>("childsession.modifiedby")?.Value as EntityReference).Id;
                        response.modifiedByUserFullName = (s.GetAttributeValue<AliasedValue>("childsession.modifiedby")?.Value as EntityReference).Name;
                    }
                    if (s.Attributes.Contains("childsession.modifiedon") && s.FormattedValues.Contains("childsession.modifiedon"))
                        response.modifiedOnDateTime = Convert.ToDateTime(s.FormattedValues["childsession.modifiedon"]);


                     if (s.Attributes.Contains("childsession.statuscode") && s.FormattedValues.Contains("childsession.statuscode"))
                        response.status= Convert.ToString(s.FormattedValues["childsession.statuscode"]);

                    if (s.Attributes.Contains("childsession.cub_sessionchannel") && s.FormattedValues.Contains("childsession.cub_sessionchannel"))
                        response.channelName = Convert.ToString(s.FormattedValues["childsession.cub_sessionchannel"]);
                    

                    linkedSessionResponse.Add(response);
                }
                
            }

            return linkedSessionResponse;
        }

        /// <summary>
        /// Populate and return a WSSessionResponse object from a Session record.
        /// </summary>
        /// <param name="session">The session record</param>
        /// <returns>WSSessionResponse</returns>
        private WSSessionResponse MapSessionToResponse(cub_CSRSession session)
        {
            if (session == null)
            {
                return null;
            }

            var response = new WSSessionResponse
            {
                sessionId = session.Id,
                sessionNumber = session.cub_name,
                startDateTime = session.CreatedOn,
                customerId = session.cub_CustomerId?.Id,
                contactId = session.cub_ContactId?.Id,
                transitAccountId = session.cub_TransitAccount?.Id,
                isVerified = session.cub_IsVerified,
                isRegistered = session.cub_IsRegistered,
                isPrimaryContact = session.cub_IsPrimaryContact,
                endDateTime = session.cub_EndDate,
                transcriptUrl = session.cub_TranscriptUrl,
                notes = session.cub_Notes,
                createdById = session.CreatedBy?.Id,
                modifiedById = session.ModifiedBy?.Id,
                createdOnDateTime = session.CreatedOn,
                modifiedOnDateTime = session.ModifiedOn
            };

            if(session.statuscode!=null)
            {
                //response.status = ((OptionSetValue)a.StatusCode).Value.ToString(),
                response.status = session.FormattedValues[cub_CSRSession.statuscodeAttribute].ToString();
            }

            if(session.cub_SessionChannel!=null)
            {
                response.channelName = session.FormattedValues[cub_CSRSession.cub_sessionchannelAttribute].ToString();
            }

            if (session.cub_CustomerId != null)
            {
                var fullName = session.GetAttributeValue<AliasedValue>("customer.name")?.Value as string;
                response.customerFirstName = fullName?.Trim().Split(' ').First().Trim();
                response.customerLastName = fullName?.Trim().Split(' ').Last().Trim();
            }
            if (session.cub_ContactId != null)
            {
                response.contactFirstName = session.GetAttributeValue<AliasedValue>("contact.firstname")?.Value as string;
                response.contactLastName = session.GetAttributeValue<AliasedValue>("contact.lastname")?.Value as string;
            }
            if (session.cub_TransitAccount != null)
            {
                response.subsystem = session.GetAttributeValue<AliasedValue>("transitaccount.cub_subsystemid")?.Value as string;
                response.subsystemAccountReference = session.GetAttributeValue<AliasedValue>("transitaccount.cub_name")?.Value as string;
            }
            if (session.CreatedBy?.Id != null)
            {
                response.createdByUserFullName = session.GetAttributeValue<AliasedValue>("createduser.fullname")?.Value as string;
            }
            if (session.ModifiedBy?.Id != null)
            {
                response.modifiedByUserFullName = session.GetAttributeValue<AliasedValue>("modifeduser.fullname")?.Value as string;
            }
            return response;
        }

        /// <summary>
        /// Populate and return a list of WSSessionActivitiesResponse object from a session activities.
        /// </summary>
        /// <param name="session">The session record</param>
        /// <returns>WSSessionResponse</returns>
        private List<WSSessionActivitiesResponse> MapSessionActvitiesToResponse(List<cub_CustomActivity> activiesSession)
        {
            if (activiesSession == null)
            {
                return null;
            }

            List<WSSessionActivitiesResponse> activityResponse = new List<WSSessionActivitiesResponse>();
            
           
            foreach(var a in activiesSession)
            {
                activityResponse.Add(new WSSessionActivitiesResponse
                {                     
                    createdOnDateTime = a.CreatedOn,
                    groupId = a.cub_GroupID,
                    activityId = a.ActivityId!= null?  a.ActivityId.ToString(): null,
                    //referenceNumber = GetReferenceNumber(a.ActivityId.Value),
                    //groupNumber = GetGroupNumber(a.cub_GroupID) ,
                    channelName = a.cub_Channel != null ? a.FormattedValues[cub_CustomActivity.cub_channelAttribute].ToString() : null,
                    subject = a.cub_ActivitySubject != null ? a.cub_ActivitySubject.Name : null,
                    activityType = a.cub_ActivityType != null ? a.cub_ActivityType.Name : null,
                    status = a.StatusCode != null ? a.FormattedValues[cub_CustomActivity.statuscodeAttribute].ToString() : null,
                    createdByName = a.CreatedBy != null ? a.CreatedBy.Name : null
                });                
            }
            
            return activityResponse;
        }

        private string GetGroupNumber(string groupNumber)
        {
            Guid groupId;
            string grpNumber = null;
            if (!string.IsNullOrEmpty(groupNumber))
            {
                if (Guid.TryParse(groupNumber, out groupId))
                    grpNumber = new BigInteger(groupId.ToByteArray()).ToString();
                else
                    grpNumber = groupNumber;
            }
            return grpNumber;
        }

        private string GetReferenceNumber(Guid activityId)
        {
            string refNumber = null;
            if (activityId!=null)
                refNumber = new BigInteger(activityId.ToByteArray()).ToString();
            
            return refNumber;
        }

        /// <summary>
        /// Populate and return a list of WSSessionActivitiesResponse object from a session cases.
        /// </summary>
        /// <param name="session">The session record</param>
        /// <returns>WSSessionResponse</returns>
        private List<WSSessionCasesResponse> MapSessionCasesToResponse(List<Incident> cases)
        {
            if (cases == null)
            {
                return null;
            }

            List<WSSessionCasesResponse> casesResponse = new List<WSSessionCasesResponse>();

            foreach (var c in cases)
            {
                casesResponse.Add(new WSSessionCasesResponse
                {
                    createdOnDateTime = c.CreatedOn,
                    caseNumber = c.TicketNumber,
                    priority = c.PriorityCode != null ? c.FormattedValues[Incident.prioritycodeAttribute].ToString() : null,
                    origin = c.CaseOriginCode != null ? c.FormattedValues[Incident.caseorigincodeAttribute].ToString() : null,
                    title = c.Title,
                    status = c.StatusCode != null ? c.FormattedValues[Incident.statuscodeAttribute].ToString() : null,
                    isEscalated = c.IsEscalated.HasValue ? c.IsEscalated.Value : false,
                    ownerId = c.OwnerId != null ? c.OwnerId.Id.ToString() : null,
                    ownerName = c.OwnerId != null ? c.OwnerId.Name : null,
                    customerId = c.CustomerId != null ? c.CustomerId.Id.ToString() : null,
                    customerName = c.CustomerId != null ? c.CustomerId.Name : null
                });

            }
            return casesResponse;
        }

        private bool ShouldUpdateSession(cub_CSRSession session, Guid userId, Guid? recordId, string entityName)
        {
            // check if transfer session to a different user
            if (entityName == cub_CSRSession.EntityLogicalName)
            {
                return !session.OwnerId.Id.Equals(userId);
            }
            else if (entityName == SystemUser.EntityLogicalName)
            {
                return recordId != null && !userId.Equals(recordId);
            }
            else if (entityName == Account.EntityLogicalName)
            {
                return (session.cub_CustomerId == null && recordId != null)
                    || (session.cub_CustomerId != null && recordId == null);
            }
            else if (entityName == Contact.EntityLogicalName)
            {
                return true;
            }
            else if (entityName == cub_TransitAccount.EntityLogicalName)
            {
                return true;
            }

            CubLogger.Warn($"No way to compare field {entityName} for current session. Default is to not update current session");
            return false;
        }

        ///TODO: some of this may be useful when expanding new Update method
        #region OLD UPDATE LOGIC
        /// <summary>
        /// Attempt to update an existing active CSR session based on the provided field.
        /// This method is also used to transfer active CSR sessions between CSR users.
        /// 
        /// When transferring a session to another user, the new CSR should be designated
        /// using recordId and entityName.
        /// 
        /// When transferring a session to oneself from another user, the 
        /// </summary>
        /// <param name="userId">An CSR user GUID</param>
        /// <param name="recordId"></param>
        /// <param name="entityName">The entity logical name to update</param>
        /// <returns></returns>
        //public bool UpdateSession(Guid userId, Guid recordId, string entityName)
        //{
        //    if (string.IsNullOrWhiteSpace(entityName))
        //    {
        //        throw new ArgumentNullException("entityName", "This field is required to determine whether to update the current session");
        //    }
        //    if (entityName == cub_CSRSession.EntityLogicalName && recordId == null)
        //    {
        //        throw new ArgumentNullException("recordId", "Cannot find session if no session ID provided");
        //    }

        //    cub_CSRSession session = null;
        //    session = entityName == cub_CSRSession.EntityLogicalName
        //        ? FindActiveSessionByAttributeValue(cub_CSRSession.cub_csrsessionidAttribute, recordId)
        //        : FindActiveSessionByAttributeValue(cub_CSRSession.owneridAttribute, userId); ;

        //    if (session == null || !ShouldUpdateSession(session, userId, recordId, entityName))
        //    {
        //        return false;
        //    }

        //    if (entityName == cub_CSRSession.EntityLogicalName)
        //    {
        //        session.OwnerId = new EntityReference(entityName, userId);
        //    }
        //    else if (entityName == SystemUser.EntityLogicalName)
        //    {
        //        session.OwnerId = new EntityReference(entityName, recordId);
        //    }
        //    else if (entityName == Account.EntityLogicalName)
        //    {
        //        session.cub_CustomerId = new EntityReference(entityName, recordId);
        //        using (var logic = new AccountLogic(_organizationService))
        //        {
        //            session.cub_ContactId = logic.GetPrimaryContactReference(recordId);
        //        }
        //        using (var logic = new ContactLogic(_organizationService))
        //        {
        //            session.cub_IsVerified = logic.IsRecentlyVerified(session.cub_ContactId.Id);
        //        }
        //        session.cub_TransitAccount = null;
        //        session.cub_IsRegistered = true;
        //        session.cub_IsPrimaryContact = true;
        //    }
        //    else if (entityName == Contact.EntityLogicalName)
        //    {
        //        session.cub_ContactId = new EntityReference(entityName, recordId);
        //        var columns = new ColumnSet(Contact.parentcustomeridAttribute);
        //        session.cub_CustomerId = _organizationService.Retrieve(entityName, recordId, columns)
        //            .ToEntity<Contact>().ParentCustomerId;
        //        session.cub_IsPrimaryContact = (from contact in CubOrgSvc.ContactSet
        //                                        join type in CubOrgSvc.cub_ContactTypeSet
        //                                          on contact.cub_ContactTypeId.Id equals type.Id
        //                                        where contact.ContactId.Equals(recordId)
        //                                        select contact.cub_ContactTypeId.Name.Equals("Primary")
        //                                        ).FirstOrDefault();
        //        session.cub_TransitAccount = null;
        //        session.cub_IsRegistered = true;
        //        using (var logic = new ContactLogic(_organizationService))
        //        {
        //            session.cub_IsVerified = logic.IsRecentlyVerified(recordId);
        //        }
        //    }
        //    else if (entityName == cub_TransitAccount.EntityLogicalName)
        //    {
        //        var columns = new ColumnSet(cub_TransitAccount.cub_customerAttribute, cub_TransitAccount.cub_contactAttribute);
        //        var transitAccount = _organizationService.Retrieve(entityName, recordId, columns)
        //            .ToEntity<cub_TransitAccount>();
        //        if (transitAccount.cub_Customer == null)
        //        {
        //            session.cub_TransitAccount = transitAccount.ToEntityReference();
        //        }
        //        session.cub_CustomerId = transitAccount.cub_Customer;
        //        session.cub_ContactId = transitAccount.cub_Contact;
        //        session.cub_IsRegistered = transitAccount.cub_Customer != null;
        //        if (transitAccount.cub_Contact != null)
        //        {
        //            using (var logic = new ContactLogic(_organizationService))
        //            {
        //                session.cub_IsVerified = logic.IsRecentlyVerified(transitAccount.cub_Contact.Id);
        //            }
        //            session.cub_IsPrimaryContact = (from contact in CubOrgSvc.ContactSet
        //                                            join type in CubOrgSvc.cub_ContactTypeSet
        //                                              on contact.cub_ContactTypeId.Id equals type.Id
        //                                            where contact.ContactId.Equals(transitAccount.cub_Contact.Id)
        //                                            select contact.cub_ContactTypeId.Name.Equals("Primary")
        //                                            ).FirstOrDefault();
        //        }
        //    }

        //    try
        //    {
        //        _organizationService.Update(session);
        //        return true;
        //    }
        //    catch (FaultException<OrganizationServiceFault> ex)
        //    {
        //        CubLogger.Error("Failed to update session record", ex);
        //        throw;
        //    }
        //}
        #endregion

        /// <summary>
        /// Update the given session fields and return the updated session
        /// </summary>
        /// <param name="session">An entity with only the fields set that need updating</param>
        /// <returns>WSSessionResponse</returns>
        public WSSessionResponse Update(cub_CSRSession session)
        {
            if(session == null)
            {
                throw new ArgumentNullException("session");
            }
            if(session.Id == null)
            {
                throw new ArgumentNullException("session.Id");
            }

            _organizationService.Update(session);

            var updatedSession = FindActiveSessionByAttributeValue(cub_CSRSession.cub_csrsessionidAttribute, session.Id);
            var response = MapSessionToResponse(updatedSession);
            return response;
        }

        /// <summary>
        /// Link child session to parent
        /// </summary>
        /// <param name="parentSessionId">Parent Session GUID</param>
        /// <param name="childSessionNumber">Child session number</param>
        public Guid LinkSession(Guid parentSessionId, string childSessionNumber)
        {
            if (parentSessionId == null)
            {
                throw new ArgumentNullException("parentSession.Id");
            }

            if (string.IsNullOrEmpty(childSessionNumber))
            {
                throw new ArgumentNullException("childSession.number");
            }

            var childSession = FindSessionByNumber(childSessionNumber);

            if(childSession==null || childSession.cub_CSRSessionId==null || !childSession.cub_CSRSessionId.HasValue)
            {
                throw new KeyNotFoundException("childSession.number");
            }

            var linkedSession = new cub_csrlinkedsession(){
                    cub_ParentSession  = new EntityReference(cub_CSRSession.EntityLogicalName, parentSessionId),
                    cub_ChildSession = new EntityReference(cub_CSRSession.EntityLogicalName, childSession.cub_CSRSessionId.Value)
                    
                  
                };

            return _organizationService.Create(linkedSession);
        }

        /// <summary>
        /// Link child session to parent
        /// </summary>
        /// <param name="parentSessionId">Parent Session GUID</param>
        /// <param name="childSessionId">Child session GUID</param>
        public Guid LinkSession(Guid? parentSessionId, Guid? childSessionId)
        {
            if (parentSessionId == null || !parentSessionId.HasValue)
            {
                throw new ArgumentNullException("parentSession.Id");
            }

            if (childSessionId==null || !childSessionId.HasValue)
            {
                throw new ArgumentNullException("childSession.Id");
            }


            var linkedSession = new cub_csrlinkedsession()
            {
                cub_ParentSession = new EntityReference(cub_CSRSession.EntityLogicalName, parentSessionId.Value),
                cub_ChildSession = new EntityReference(cub_CSRSession.EntityLogicalName, childSessionId.Value)


            };

            return _organizationService.Create(linkedSession);
        }

        /// <summary>
        /// Find session record by session number
        /// </summary>
        /// <param name="sessionNumber">User id making the request</param>
        /// <returns>MvcGenericResponse</returns>
        public MvcGenericResponse SearchSession(string sessionNumber)
        {
            const string methodName = "SearchSession";
            MvcGenericResponse wsresp = new MvcGenericResponse();
            try
            {
                var session = FindSessionByNumber(sessionNumber);

                if (session != null && session.cub_CSRSessionId.HasValue)
                {
                    wsresp.Body = JsonConvert.SerializeObject(new List<WSSessionResponse>() { MapSessionToResponse(session) });
                    wsresp.Header.result = RestConstants.SUCCESSFUL;
                }
                else
                {
                    wsresp.Header.result = RestConstants.FAILED;
                    wsresp.Header.errorMessage = "No record found";
                }

            }
            catch (Exception ex)
            {
                CubLogger.Error(methodName, ex);
                wsresp.Header.result = RestConstants.FAILED;
                wsresp.Header.errorMessage = ex.Message;
                throw;
            }
            return wsresp;
        }

        /// <summary>
        /// Remove linked session
        /// </summary>
        /// <param name="linkSessionId">Linked session Id</param>
        /// <returns>WSSessionResponse</returns>
        public void RemoveLinkedSession(Guid linkSessionId)
        {
           
            if (linkSessionId == null)
            {
                throw new ArgumentNullException("linkSession.Id");
            }           

            _organizationService.Delete(cub_csrlinkedsession.EntityLogicalName, linkSessionId);
        }
    }

    
    class WSSessionActivitiesResponse    {
       

        public DateTime? createdOnDateTime { get; set; }

        public string groupId { get; set; }
        public string groupNumber { get; set; }

        public string referenceNumber { get; set; }

        public string activityId { get; set; }

        public string channelName { get; set; }

        public string subject { get; set; }

        public string activityType { get; set; }

        public string status { get; set; }
        
        public string createdByName { get; set; }

    }

    class WSSessionCasesResponse
    {


        public DateTime? createdOnDateTime { get; set; }

        public string caseNumber { get; set; }

        public string priority { get; set; }

        public string origin { get; set; }

        public string status { get; set; }

        public string customerName { get; set; }

        public string customerId { get; set; }

        public string ownerName { get; set; }

        public string ownerId { get; set; }

        public string title { get; set; }

        public bool isEscalated { get; set; }
    }
}
