﻿/***********************************************************************************
* Copyright (c) 2018 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CUB.MSD.Logic
{
    public class AutoNumberLogic : LogicBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="orgService">MSD Organization Service instance</param>
        public AutoNumberLogic(IOrganizationService orgService) : base(orgService)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="orgService">MSD Organization Service instance</param>
        /// <param name="context">MSD Plugin Execution Context object</param>
        public AutoNumberLogic(IOrganizationService orgService, IPluginExecutionContext context) : base(orgService, context)
        {
        }

        public string GetNextAutoNumberById(Guid autoNumberId)
        {
            // Update a field on the AutoNumber to lock the record during a plugin transaction
            var oldAutoNumberEntity = new cub_AutoNumber
            {
                Id = autoNumberId,
                cub_Updating = DateTime.Now
            };

            _organizationService.Update(oldAutoNumberEntity);

            var columns = new ColumnSet(cub_AutoNumber.cub_numberAttribute, cub_AutoNumber.cub_maxvalueAttribute);

            var currentAutoNumberEntity = _organizationService.Retrieve(cub_AutoNumber.EntityLogicalName, 
                autoNumberId, columns)?.ToEntity<cub_AutoNumber>();

            var newNumber = string.Empty;
            if (currentAutoNumberEntity != null)
            {
                var maxLength = currentAutoNumberEntity.cub_MaxValue.GetValueOrDefault(-1);

                //pull current number from record and increment
                var currentNum = currentAutoNumberEntity.cub_Number.GetValueOrDefault();
                var nextNum = IncrementAutoNumber(currentNum, maxLength);

                if (nextNum > 0)
                {
                    newNumber = nextNum.ToString();
                    currentAutoNumberEntity.cub_Number = nextNum;
                    _organizationService.Update(currentAutoNumberEntity);
                }
                else
                {
                    throw new InvalidOperationException("Auto Number has reached its maximum value.");
                }
            }
            return newNumber;
        }

        public int IncrementAutoNumber(int num, int maxLength)
        {
            if (num < 0)
            {
                return num;
            }

            var nextNum = num + 1;

            if (maxLength > 0 && nextNum > maxLength)
            {
                throw new InvalidOperationException("Auto Number has reached its maximum value.");
            }

            return nextNum;
        }

        public Guid GetAutoNumberIdByEntity(string entityName)
        {
            var autoNumberId = (from autonumber in CubOrgSvc.cub_AutoNumberSet
                                where autonumber.cub_Entity == entityName
                                select autonumber.cub_AutoNumberId).FirstOrDefault();

            if (autoNumberId.HasValue)
            {
                return autoNumberId.Value;
            }
            else
            {
                throw new InvalidOperationException($"No Auto Number record could be found for entity {entityName}.");
            }
        }

        public cub_AutoNumber GetAutoNumberById(Guid id)
        {
            return (from autonumber in CubOrgSvc.cub_AutoNumberSet
                    where autonumber.cub_AutoNumberId.Equals(id)
                    select new cub_AutoNumber
                    {
                        Id = id,
                        cub_Entity = autonumber.cub_Entity,
                        cub_EntityField = autonumber.cub_EntityField,
                        cub_Prefix = autonumber.cub_Prefix,
                        cub_Suffix = autonumber.cub_Suffix
                    }).FirstOrDefault();
        }
    }
}
