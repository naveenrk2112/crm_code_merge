/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System.Linq;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// Option Set Translation Logic
    /// </summary>
    public class OptionSetTranslationLogic : LogicBase
    {
        private const string CACHE_NAME = "OPTIONSET_TRANSLATIONS";
        private OptionSetTranslationModel TranslationModel;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        public OptionSetTranslationLogic(IOrganizationService service) : base(service)
        {
            // Get from the cache
            TranslationModel = (OptionSetTranslationModel) GlobalsCache.GetFromCache(CACHE_NAME);
            // If not in the cache, load from MSD and add to the cache
            if (TranslationModel == null)
            {
                LoadOptionSetTranslations();
                GlobalsCache.AddToCache(CACHE_NAME, TranslationModel);
            }
        }

        /// <summary>
        /// Load all translation from MSD entities: OptionSetTranslation and OptionSetTranslationDetail
        /// </summary>
        private void LoadOptionSetTranslations()
        {
            var translations = from t in CubOrgSvc.cub_OptionSetTranslationSet
                               join d in CubOrgSvc.cub_OptionSetTranslationDetailSet
                                 on t.cub_OptionSetTranslationId equals d.cub_OptionSetTranslationId.Id
                               into joined
                               from translation in joined.DefaultIfEmpty()
                               orderby t.cub_EntityName, t.cub_OptionSetName
                               select new
                               {
                                   EntityName = t.cub_EntityName,
                                   OptionSetName = t.cub_OptionSetName,
                                   Text = translation.cub_Translation,
                                   Value = (!translation.cub_OptionSetValue.HasValue) ? null : (int?)translation.cub_OptionSetValue.Value
                               };
            TranslationModel = new OptionSetTranslationModel();
            OptionSetTranslation currentOptionSet = null;
            foreach (var t in translations)
            {
                if (currentOptionSet == null)
                {
                    currentOptionSet = TranslationModel.Add(t.EntityName, t.OptionSetName);
                }
                else if (!((currentOptionSet.EntityName == null || currentOptionSet.EntityName.Equals(t.EntityName)) &&
                          currentOptionSet.OptionSetName.Equals(t.OptionSetName)))
                {
                    currentOptionSet = TranslationModel.Add(t.EntityName, t.OptionSetName);
                }
                if (t.Text != null && t.Value.HasValue)
                {
                    currentOptionSet.Add(t.Text, t.Value.Value);
                }
            }
        }

        /// <summary>
        /// Get an OptionSet Value associated with a External String (Text)
        /// </summary>
        /// <param name="entityName">Entity Name</param>
        /// <param name="optionSetName">OptionSet Name</param>
        /// <param name="text">External String (Text)</param>
        /// <returns>OptionSet Value</returns>
        /// <exception cref="KeyNotFoundException">If the External String (Text) is not found</exception>
        public int GetValue(string entityName, string optionSetName, string text)
        {
            OptionSetTranslation trans = TranslationModel.Get(entityName, optionSetName);
            return trans.GetValue(text);
        }

        /// <summary>
        /// Get the External String (Text) associated with an OptionSet Value
        /// </summary>
        /// <param name="entityName">Entity Name</param>
        /// <param name="optionSetName">OptionSet Name</param>
        /// <param name="value">OptionSet Value</param>
        /// <returns>External String (Text)</returns>
        /// <exception cref="KeyNotFoundException">If the OptionSet Value is not found</exception>
        public string GetText(string entityName, string optionSetName, int value)
        {
            OptionSetTranslation trans = TranslationModel.Get(entityName, optionSetName);
            return trans.GetText(value);
        }
    }
}
