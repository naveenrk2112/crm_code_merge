/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using CUB.MSD.Model.MSDApi;
using System.Reflection;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// Organization Logic
    /// </summary>
    public class OrganizationLogic : LogicBase
    {
        IOrganizationService _service;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public OrganizationLogic(IOrganizationService service) : base(service)
        {
            _service = service;
        }

        /// <summary>
        /// Retrieve orgazination info for currently logged in user 
        /// </summary>
        /// <returns>CubResponse<WSOrganizationInfo></returns>
        public virtual CubResponse<WSOrganizationInfo> GetOrganziationInfo()
        {
            CubResponse<WSOrganizationInfo> orgInforesponse = new CubResponse<WSOrganizationInfo>();

            try
            {
                WhoAmIResponse me = (WhoAmIResponse)_service.Execute(new WhoAmIRequest());
                
                Organization org = _service.Retrieve(Organization.EntityLogicalName, me.OrganizationId,
                         new ColumnSet(new string[] { Organization.organizationidAttribute, Organization.nameAttribute,  })) as Organization;

                if(org!=null)
                {
                    orgInforesponse.ResponseObject = new WSOrganizationInfo(){ OrganizationId = org.OrganizationId, OrganizationName = org.Name  };
                    //add app version from global cache
                    orgInforesponse.ResponseObject.Version = GlobalsCache.AppInfoAppVersion(_service);
                }
                else
                {
                    orgInforesponse.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND, CUBConstants.Error.MSG_ORGANIZATION_NOT_FOUND));
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                orgInforesponse.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return orgInforesponse;
        }


    }

}
