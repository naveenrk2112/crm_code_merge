﻿using CUB.MSD.Model.NIS;
using System;
using System.ServiceModel;

namespace CUB.MSD.Logic
{
    public class TollingException : Exception
    {
        public WSWebResponseHeader Hdr { get; set; }

        public TollingException(WSWebResponseHeader hdr) : base(hdr != null ? hdr.SerializeObject() : "NIS header information unavailable")
        {
            Hdr = hdr;
        }

        public TollingException(string message) : base(message)
        {

        }

        public TollingException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public TollingException(WSWebResponseHeader hdr, Exception innerException) : base(innerException.Message, innerException)
        {
            Hdr = hdr;
        }
    }

    public class NISException : Exception
    {
        public WSWebResponseHeader Hdr { get; set; }

        public NISException(WSWebResponseHeader hdr) : base(hdr != null ? hdr.SerializeObject() : "NIS header information unavailable")
        {
            Hdr = hdr;
        }

        public NISException(string message) : base(message)
        {

        }

        public NISException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public NISException(WSWebResponseHeader hdr, Exception innerException) : base(innerException.Message, innerException)
        {
            Hdr = hdr;
        }
    }

    public class MSDException : Exception
    {
        public MSDException(string message) : base(message)
        {

        }

        public MSDException(string message, Exception innerException) : base(message, innerException)
        {
 
        }

        public MSDException(FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> orgServiceException) : base(orgServiceException.Message, orgServiceException)
        {

        }
    }
    public class GatewayException : Exception
    {
        public WSWebResponseHeader Hdr { get; set; }

        public GatewayException(WSWebResponseHeader hdr) : base(hdr != null ? hdr.SerializeObject() : "Gateway header information unavailable")
        {
            Hdr = hdr;
        }

        public GatewayException(string message) : base(message)
        {

        }

        public GatewayException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public GatewayException(WSWebResponseHeader hdr, Exception innerException) : base(innerException.Message, innerException)
        {
            Hdr = hdr;
        }
    }
}
