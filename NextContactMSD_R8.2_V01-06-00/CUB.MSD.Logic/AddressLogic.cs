﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk.Query;
using CUB.MSD.Model.MSDApi;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Reflection;

namespace CUB.MSD.Logic
{
    public class AddressLogic : LogicBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public AddressLogic(IOrganizationService service) : base(service)
        {
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <param name="executionContext">MSD plugin execution contect</param>
        public AddressLogic(IOrganizationService service, IPluginExecutionContext executionContext) : base(service, executionContext)
        {
        }

        #region Address Creation

        /// <summary>
        /// Creates an address
        /// </summary>
        /// <param name="address">Address data</param>
        /// <param name="customerId">Customer to link the address to</param>
        /// <returns>Void. However, the new address is set in the address input object</returns>
        public Guid? CreateAddress(WSAddress address, Guid customerId)
        {
            Guid existingAddressId;
            address.isExistingAddress = ExistingAddress(address, customerId,  out existingAddressId);
            if (address.isExistingAddress)
            {
                address.addressId = existingAddressId;
            }
            else
            {
                cub_Address newAddress = new cub_Address();
                newAddress.cub_CountryId = new EntityReference { LogicalName = cub_Country.EntityLogicalName, Id = address.countryId.Value };
                newAddress.cub_StateProvinceId = new EntityReference { LogicalName = cub_StateOrProvince.EntityLogicalName, Id = address.stateId.Value };
                newAddress.cub_PostalCodeId = new EntityReference { LogicalName = cub_ZipOrPostalCode.EntityLogicalName, Id = address.postalCodeId.Value };
                newAddress.cub_Name = $"{address.address1}{(string.IsNullOrEmpty(address.postalCode) ? "" : " in " + address.postalCode)}";
                newAddress.cub_Name = newAddress.cub_Name.Substring(0, Math.Min(newAddress.cub_Name.Length, 99)); //cub_name max size is 100 chars
                newAddress.cub_AccountAddressId = new EntityReference { LogicalName = Account.EntityLogicalName, Id =customerId };
                newAddress.cub_Street1 = string.IsNullOrWhiteSpace(address.address1) ? null : address.address1.Trim();
                newAddress.cub_Street2 = string.IsNullOrWhiteSpace(address.address2) ? null : address.address2.Trim();
                newAddress.cub_Street3 = string.IsNullOrWhiteSpace(address.address3) ? null : address.address3.Trim();
                newAddress.cub_City = string.IsNullOrWhiteSpace(address.city) ? null : address.city.Trim();
                address.addressId = _organizationService.Create(newAddress);
            }
            return address.addressId;
        }

        /// <summary>
        /// Create Address or return an existing one
        /// </summary>
        /// <param name="address">address fields</param>
        /// <param name="customerId">customer</param>
        /// <returns>isExistingAddress: true - if existing address, addressId: of the new or existing address</returns>
        public CubResponse<WSAddress> ValidateAndCreateAddress(WSAddress address, Guid customerId)
        {
            CubResponse<WSAddress> resp = new CubResponse<WSAddress>();

            try
            {
                resp = IsAddressValid(address);
                if (resp.Success)
                {
                    CreateAddress(address, customerId);
                    resp.ResponseObject = address;
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(null, ex);
                resp.Errors.Add(new WSMSDFault("CreateAddress", CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return resp;
        }

        /// <summary>
        /// Address PreUpdate Validate
        /// </summary>
        /// <param name="cubAddress">Cub Address</param>
        /// <returns>CubResponse<WSAddressUpdateResponse></returns>
        public CubResponse<WSAddressDependenciesResponse> AddressPreUpdateValidate(cub_Address cubAddress)
        {
            CubResponse<WSAddressDependenciesResponse> cubResponse = new CubResponse<WSAddressDependenciesResponse>();
            try
            {
                //Verify account id is there
                if (cubAddress.cub_AccountAddressId == null)
                    cubResponse.Errors.Add(new WSMSDFault(cub_Address.cub_accountaddressidAttribute, 
                        CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_GENERAL_RECORD_ID_NOT_A_VALID_RECORD_ID));


                //Check for duplicate
                if (cubResponse.Success)
                {
                    Guid currentAddressId = cubAddress.Id;

                    List<cub_Address> existingAddresses = GetAddressList(cubAddress);

                    //Check if the update to this address resulted in a duplicate address,
                    //other than the current address being edited
                    if (existingAddresses.Where(add => !add.Id.Equals(currentAddressId)).Count() > 0)
                        cubResponse.Errors.Add(new WSMSDFault(cub_Address.EntityLogicalName, CUBConstants.Error.ERR_GENERAL_VALUE_DUPLICATE, CUBConstants.Address.Error.ERR_EXISTING_ADDRESS));
                }

                //Check for dependencies 
                if (cubResponse.Success)
                {
                    //call NIS
                    NISApiLogic nisApi = new NISApiLogic(_organizationService);
                    cubResponse.ResponseObject = nisApi.GetAddressDependencies(cubAddress.cub_AccountAddressId.Id, cubAddress.Id);

                    if (!cubResponse.ResponseObject.hdr.result.Equals(Model.NIS.RestConstants.SUCCESSFUL))
                    {
                        // Getting no errors back in this case so setting it to a custom message that can be used to display on the client/MSD
                        cubResponse.ResponseObject.hdr.fieldName = cubResponse.ResponseObject.hdr.fieldName ?? CUBConstants.Address.ADDRESS_STRING;
                        cubResponse.ResponseObject.hdr.errorKey = cubResponse.ResponseObject.hdr.errorKey ?? CUBConstants.Error.ERR_GENERAL_DEPEND_EXCEPTION;
                        cubResponse.ResponseObject.hdr.errorMessage = cubResponse.ResponseObject.hdr.errorMessage ?? CUBConstants.Address.Error.ERR_NIS_ADDRESS_DEPENDS;
                    }

                }
            }
            catch (Exception ex)
            {
                cubResponse.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                throw;
            }

            return cubResponse;
        }

        /// <summary>
        /// Address Pre Create Validate
        /// </summary>
        /// <param name="cubAddress">Cub Address</param>
        /// <returns>CubResponse<WSAddress></returns>
        public CubResponse<WSAddress> AddressPreCreateValidate(cub_Address cubAddress)
        {
            CubResponse<WSAddress> response = new CubResponse<WSAddress>();
            try
            {
                response = IsAddressValid(cubAddress);
                if (response.Success)
                {
                    Guid existingAddressId;
                    if (ExistingAddress(cubAddress, out existingAddressId))
                        response.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, CUBConstants.Address.Error.ERR_EXISTING_ADDRESS));
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                throw;
            }



            return response;
        }

        /// <summary>
        /// Method used by address plug-in to hold all address post create tasks
        /// </summary>
        /// <param name="cubAddress"></param>
        public void DoAddressPostCreateTasks(cub_Address cubAddress)
        {
            // SS - not going to use the plugin code to create a new activity, since we don't need to have a contact linked on the new activity
            // commenting it out until there is a need for this code. Will create a wrokflow for this instead. 
            //CreateActivityOnAddressCreate(cubAddress);
        }

        private void CreateActivityOnAddressCreate(cub_Address cubAddress)
        {
            try
            {
                using (ActivityLogic activityLogic = new ActivityLogic(_organizationService))
                {
                    // Get Address account
                    var accountId = cubAddress.cub_AccountAddressId.Id;
                    // Get the contact (if any) using this address
                    var contacts = CubOrgSvc.ContactSet.Where(c => c.cub_AddressId.Id.Equals(cubAddress.Id)).OrderByDescending(o => o.CreatedOn).ToList();

                    CreateMSDActivityData activityData = new CreateMSDActivityData();

                    // Note: At the time this code is written, when a new address is created, it is not linked to a contact
                    // and there is no option to create an address from a contact. Therefore, the activity will have not address
                    if (contacts.Count > 0) activityData.ContactId = contacts[0].Id;

                    activityData.CustomerId = accountId;

                    activityData.CreatedById = cubAddress.OwnerId.Id;

                    //look for active session 
                    var csrSession = CubOrgSvc.cub_CSRSessionSet
                        .Where(s => s.cub_CustomerId.Id == activityData.CustomerId
                                && s.OwnerId.Id == cubAddress.OwnerId.Id
                                && s.statecode == cub_CSRSessionState.Active).ToList();
                    if (csrSession.Count == 1) activityData.SessionId = csrSession[0].Id;

                    //lookup Activity name from global to later lookup activty Id
                    string activityNameFromGlobals = CubOrgSvc.cub_GlobalDetailsSet
                        .Where(gd => gd.cub_AttributeValue == GlobalsCache.ActivityTypeAddAddress(_organizationService))
                        .Select(gd => gd.cub_AttributeValue).Single();

                    activityData.ActivityTypeId = activityLogic.GetActivityTypeId(activityNameFromGlobals);

                    activityData.Description = $"Address added:{cubAddress.cub_Name}";
                    CubResponse<WSCreateNewActivityResponse> cubResponse = activityLogic.CreateNewActivity(activityData);
                    if (cubResponse.Errors.Count > 0)
                    {
                        string err = string.Empty;
                        foreach (var item in cubResponse.Errors)
                        {
                            err += $"{Environment.NewLine}Key: {item.errorKey} Field: {item.fieldName} Message: {item.errorMessage}";
                        }
                        CubLogger.Error(err);
                    }
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
            }
            
        }

        /// <summary>
        /// Address Pre Delete Validation
        /// </summary>
        /// <param name="cubAddress">Cub Address</param>
        /// <returns></returns>
        public CubResponse<WSAddressDependenciesResponse> AddressPreDeleteValidation(cub_Address cubAddress)
        {
            CubResponse<WSAddressDependenciesResponse> cubResponse = new CubResponse<WSAddressDependenciesResponse>();
            try
            {
                //call NIS
                NISApiLogic nisApi = new NISApiLogic(_organizationService);
                cubResponse.ResponseObject = nisApi.GetAddressDependencies(cubAddress.cub_AccountAddressId.Id, cubAddress.Id);

                if (!cubResponse.ResponseObject.hdr.result.Equals(Model.NIS.RestConstants.SUCCESSFUL))
                {
                    // Getting no errors back in this case so setting it to a custom message that can be used to display on the client/MSD
                    cubResponse.ResponseObject.hdr.fieldName = cubResponse.ResponseObject.hdr.fieldName ?? CUBConstants.Address.ADDRESS_STRING;
                    cubResponse.ResponseObject.hdr.errorKey = cubResponse.ResponseObject.hdr.errorKey ?? CUBConstants.Error.ERR_GENERAL_DEPEND_EXCEPTION;
                    cubResponse.ResponseObject.hdr.errorMessage = cubResponse.ResponseObject.hdr.errorMessage ?? CUBConstants.Address.Error.ERR_NIS_ADDRESS_DEPENDS;
                }
            }
            catch (Exception ex)
            {
                cubResponse.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return cubResponse;
        }

        /// <summary>
        /// Validate address from the cub.msd.web 
        /// </summary>
        /// <param name="street1"></param>
        /// <param name="street2"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="zip"></param>
        /// <param name="country"></param>
        /// <returns>CubResponse WSAddress response</returns>
        public CubResponse<WSAddress> AddressPreCreateValidate(Guid accountId, string street1, string street2, string city, Guid stateId, Guid zipId, Guid countryId)
        {
            CubResponse<WSAddress> response = new CubResponse<WSAddress>();
            try
            {
                cub_Address cubAddress = new cub_Address();
                cubAddress.cub_Street1 = street1;
                cubAddress.cub_Street2 = street2;
                cubAddress.cub_City = city;
                cubAddress.cub_CountryId = new EntityReference { LogicalName = cub_Country.EntityLogicalName, Id = countryId };
                cubAddress.cub_StateProvinceId = new EntityReference { LogicalName = cub_StateOrProvince.EntityLogicalName, Id = stateId };
                cubAddress.cub_PostalCodeId = new EntityReference { LogicalName = cub_ZipOrPostalCode.EntityLogicalName, Id = zipId };
                cubAddress.cub_AccountAddressId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = accountId }; 

                response = AddressPreCreateValidate(cubAddress);
            }
            catch (Exception ex)
            {
                response.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                throw;
            }
            
            return response;
        }

        #endregion

        #region Get Related Data

        /// <summary>
        /// Get State Entity By State
        /// </summary>
        /// <param name="state">state</param>
        /// <param name="country">country</param>
        /// <returns>State</returns>
        public EntityReference GetStateEntityByState(string state, string country)
        {
            var rtn =
                (from ste in CubOrgSvc.cub_StateOrProvinceSet
                 join cnt in CubOrgSvc.cub_CountrySet on ste.cub_Country.Id equals cnt.cub_CountryId into joined
                 from j in joined.DefaultIfEmpty()
                 where ste.cub_Abbreviation == state && j.cub_Alpha2 == country
                 select ste.Id).FirstOrDefault();

            return new EntityReference { LogicalName = cub_StateOrProvince.EntityLogicalName, Id = rtn };
        }

        /// <summary>
        /// Get Country
        /// </summary>
        /// <param name="country">country</param>
        /// <returns>Country</returns>
        public EntityReference GetCountryEntity(string country)
        {
            var rtn =
                (from j in CubOrgSvc.cub_CountrySet
                 where j.cub_Alpha2 == country
                 select j.Id).FirstOrDefault();

            return new EntityReference { LogicalName = cub_Country.EntityLogicalName, Id = rtn };
        }

        /// <summary>
        /// Get Zipcode
        /// </summary>
        /// <param name="code">zipcode</param>
        /// <returns>Postal/zipcode</returns>
        public EntityReference GetZipEntity(string code)
        {
            var rtn =
                (from j in CubOrgSvc.cub_ZipOrPostalCodeSet
                 where j.cub_Code == code
                 select j.Id).FirstOrDefault();

            return new EntityReference { LogicalName = cub_ZipOrPostalCode.EntityLogicalName, Id = rtn };
        }

        /// <summary>
        /// Get Country
        /// </summary>
        /// <param name="countryId">Country id</param>
        /// <returns>Country</returns>
        private cub_Country GetCountryById(Guid countryId)
        {
            try
            {
                return
                    (from cnty in CubOrgSvc.cub_CountrySet
                     where cnty.cub_CountryId.Value == countryId
                     select cnty).FirstOrDefault();
            }
            catch (Exception ex)
            {
                CubLogger.Error("", ex);
                return null;
            }
        }

        /// <summary>
        /// Get Country B yAlfha 2 Or 3
        /// </summary>
        /// <param name="alpha2or3"></param>
        /// <param name="columns"></param>
        /// <returns>Country</returns>
        public virtual cub_Country GetCountryByAlfha2Or3(string alpha2or3, params string[] columns)
        {
            QueryExpression query = new QueryExpression(cub_Country.EntityLogicalName);
            query.ColumnSet.AddColumns(columns);
            query.Criteria = new FilterExpression(LogicalOperator.Or);
            query.Criteria.AddCondition(cub_Country.cub_alpha2Attribute, ConditionOperator.Equal, alpha2or3);
            query.Criteria.AddCondition(cub_Country.cub_alpha3Attribute, ConditionOperator.Equal, alpha2or3);
            EntityCollection result = _organizationService.RetrieveMultiple(query);
            if (result?.Entities?.Count == 0)
            {
                return null;
            }
            else
            {
                return (cub_Country)result[0];
            }
        }

        /// <summary>
        /// Get State by ID
        /// </summary>
        /// <param name="stateId">State ID</param>
        /// <returns>State or Province</returns>
        private cub_StateOrProvince GetStateById(Guid stateId)
        {
            return
                (from state in CubOrgSvc.cub_StateOrProvinceSet
                 where state.cub_StateOrProvinceId.Value == stateId
                 select state).FirstOrDefault();
        }

        /// <summary>
        /// Get Zip by ID
        /// </summary>
        /// <param name="zipId">zipcode</param>
        /// <returns>Zip or postal code</returns>
        private cub_ZipOrPostalCode GetZipById(Guid zipId)
        {
            return
                (from zip in CubOrgSvc.cub_ZipOrPostalCodeSet
                 where zip.cub_ZipOrPostalCodeId.Value == zipId
                 select zip).FirstOrDefault();
        }

        //public virtual IEnumerable<WSAddress> GetByAccountId(Guid accountId)
        //{
        //    IEnumerable<WSAddress> address = null;
        //    address = (from add in CubOrgSvc.cub_addressSet
        //               join cnt in CubOrgSvc.cub_countrySet on add.cub_countryid.Id equals cnt.Id
        //               where add.cub_AccountAddressId.Id == accountId
        //               select
        //                   new WSAddress()
        //                   {
        //                       address1 = add.cub_Street1,
        //                       address2 = add.cub_Street2,
        //                       city = add.cub_City,
        //                       state = add.cub_stateprovinceid.Name,
        //                       postalCode = add.cub_postalcodeid.Name,
        //                       country = cnt.cub_alpha2
        //                   }).ToList();
        //    return address;
        //}

        /// <summary>
        /// Get by Contact ID
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <returns>Conact</returns>
        public object GetByContactId(Guid contactId)
        {
            return (from address in CubOrgSvc.cub_AddressSet
                    join contact in CubOrgSvc.ContactSet
                    on address.cub_AddressId equals contact.cub_AddressId.Id
                    join state in CubOrgSvc.cub_StateOrProvinceSet
                    on address.cub_StateProvinceId.Id equals state.cub_StateOrProvinceId
                    join zip in CubOrgSvc.cub_ZipOrPostalCodeSet
                    on address.cub_PostalCodeId.Id equals zip.cub_ZipOrPostalCodeId
                    where contact.ContactId.Equals(contactId)
                        && address.statecode.Value.Equals((int)cub_AddressState.Active)
                    select new
                    {
                        addressId = address.cub_AddressId,
                        city = address.cub_City,
                        state = state.cub_Abbreviation,
                        street1 = address.cub_Street1,
                        street2 = address.cub_Street2,
                        street3 = address.cub_Street3,
                        zipCode = zip.cub_Code
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Get Update Dependencies Data By Id
        /// </summary>
        /// <param name="addressId">Address ID</param>
        /// <returns>WSNISUpdateDependencies</returns>
        private CUB.MSD.Model.NIS.WSNISUpdateDependencies GetUpdateDependenciesDataById(Guid addressId)
        {
            return (from add in CubOrgSvc.cub_AddressSet
                    join cnt in CubOrgSvc.cub_CountrySet on add.cub_CountryId.Id equals cnt.cub_CountryId into joined
                    from j in joined.DefaultIfEmpty()
                    where add.cub_AddressId.Equals(addressId)
                    select
                        new CUB.MSD.Model.NIS.WSNISUpdateDependencies()
                        {
                            customerId = add.cub_AccountAddressId.Id,
                            wsaddress = new CUB.MSD.Model.NIS.WSAddress()
                            {
                                address1 = add.cub_Street1,
                                address2 = add.cub_Street2,
                                city = add.cub_City,
                                state = add.cub_StateProvinceId.Name,
                                postalCode = add.cub_PostalCodeId.Name,
                                country = j.cub_Alpha2
                            }
                        }).SingleOrDefault();
        }

        #endregion Get Related Data

        /// <summary>
        /// Checks/returns an address exists
        /// </summary>
        /// <param name="addressId">Address id</param>
        /// <param name="existingAddress">Existing address</param>
        /// <returns>If the address exist</returns>
        private bool AddressExists(Guid addressId, out cub_Address existingAddress)
        {
            existingAddress = (cub_Address)_organizationService.Retrieve(cub_Address.EntityLogicalName, addressId, new Microsoft.Xrm.Sdk.Query.ColumnSet() { AllColumns = true });
            return existingAddress != null;

        }

        /// <summary>
        /// Get country by name
        /// </summary>
        /// <param name="country">country name</param>
        /// <returns>Country</returns>
        private cub_Country CountryExists(string country)
        {
            try
            {
                return
                    (from cnty in CubOrgSvc.cub_CountrySet
                     where cnty.cub_Alpha2 == country
                     select cnty).FirstOrDefault();
            }
            catch (Exception ex)
            {
                CubLogger.Error("", ex);
                return null;
            }
        }

        /// <summary>
        /// Get Country by ID
        /// </summary>
        /// <param name="countryId">Country ID</param>
        /// <returns>Country</returns>
        private cub_Country CountryExists(Guid? countryId)
        {
            if (countryId == null)
            {
                return null;
            }

            try
            {
                return
                    (from cnty in CubOrgSvc.cub_CountrySet
                     where cnty.Id == countryId.Value
                     select cnty).FirstOrDefault();
            }
            catch (Exception ex)
            {
                CubLogger.Error("", ex);
                return null;
            }
        }

        /// <summary>
        /// Check if a state exist for a country
        /// </summary>
        /// <param name="state">state abbreviation</param>
        /// <param name="country">country two character abbreviation</param>
        /// <param name="stateId">State ID</param>
        /// <returns>If exists or Not</returns>
        private bool StateExistsForCountry(string state, string country, out Guid stateId)
        {
            stateId =
                (from ste in CubOrgSvc.cub_StateOrProvinceSet
                 join cnt in CubOrgSvc.cub_CountrySet on ste.cub_Country.Id equals cnt.cub_CountryId into joined
                 from j in joined.DefaultIfEmpty()
                 where ste.cub_Abbreviation == state && j.cub_Alpha2 == country
                 select ste.Id).FirstOrDefault();

            return stateId == null ? false : true;
        }

        /// <summary>
        /// Check if  State exists for a country
        /// </summary>
        /// <param name="state">State abbreviation</param>
        /// <param name="country">country ID</param>
        /// <param name="stateId">State ID</param>
        /// <returns></returns>
        private bool StateExistsForCountry(string state, Guid country, out Guid stateId)
        {
            stateId =
                (from ste in CubOrgSvc.cub_StateOrProvinceSet
                 where ste.cub_Abbreviation == state && ste.cub_Country.Id == country
                 select ste.Id).FirstOrDefault();

            return stateId == null ? false : true;
        }

        /// <summary>
        /// Check if State exists for a country
        /// </summary>
        /// <param name="stateId">State ID</param>
        /// <param name="country">Country ID</param>
        /// <param name="returndStateId">State ID</param>
        /// <returns>Exist or Not</returns>
        private bool StateExistsForCountry(Guid? stateId, Guid country, out Guid returndStateId)
        {
            if (stateId == null)
            {
                returndStateId = Guid.Empty;
                return false;
            }

            returndStateId =
                (from ste in CubOrgSvc.cub_StateOrProvinceSet
                 where ste.Id == stateId.Value && ste.cub_Country.Id == country
                 select ste.Id).FirstOrDefault();

            return returndStateId == null ? false : true;
        }

        /// <summary>
        /// Check if a Zip/Postal Code exist for a countr
        /// </summary>
        /// <param name="zipCode">zipcode</param>
        /// <param name="countryId">Country ID</param>
        /// <param name="ziporpostalcode">Zip/Postal code</param>
        /// <returns>Exist or not</returns>
        private bool ZipExistsForCountry(string zipCode, Guid countryId, out cub_ZipOrPostalCode ziporpostalcode)
        {
            ziporpostalcode =
                (from zip in CubOrgSvc.cub_ZipOrPostalCodeSet
                 where zip.cub_Code == zipCode && zip.cub_Country.Id == countryId
                 select zip).FirstOrDefault();

            return ziporpostalcode == null ? false : true;
        }

        /// <summary>
        /// Check if Zip/Postal Code exist for a country
        /// </summary>
        /// <param name="countryId">Country ID</param>
        /// <param name="zipId">Zip/postal code ID</param>
        /// <returns>Exist or Not</returns>
        private bool ZipExistsForCountry(Guid countryId, Guid zipId)
        {
            return (from zip in CubOrgSvc.cub_ZipOrPostalCodeSet
                    where zip.Id == zipId && zip.cub_Country.Id == countryId
                    select zip).FirstOrDefault() != null;
        }

        /// <summary>
        /// Check if a state exists for a country
        /// </summary>
        /// <param name="countryId">Country ID</param>
        /// <param name="stateId">State ID</param>
        /// <returns>Exist or Not</returns>
        private bool StateExistsForCountry(Guid countryId, Guid stateId)
        {
            return (from state in CubOrgSvc.cub_StateOrProvinceSet
                    where state.Id == stateId && state.cub_Country.Id == countryId
                    select state).FirstOrDefault() != null;
        }

        /// <summary>
        /// Checks if address fields passed in as WSAddress contain valid data and assign the country, state and zip Ids to WSAddress paramater
        /// </summary>
        /// <param name="address">address fields contained in WSAddress</param>
        /// <returns> CubResponse<WSAddress>The missing country, state and zip values in input address will be set and are also returned as part of the response.</returns>
        public CubResponse<WSAddress> IsAddressValid(WSAddress address)
        {
            const string methodName = "IsAddressValid";
            CubResponse<WSAddress> response = new CubResponse<WSAddress>();
            Guid stateId;


            try
            {
                if (address == null)
                {
                    response.Errors.Add(new WSMSDFault(CUBConstants.Address.ADDRESS_STRING, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_VALUE_CANNOT_REMOVED));
                    return response;
                }

                #region Country
                if (string.IsNullOrEmpty(address.country) && address.countryId == null)
                {
                    response.Errors.Add(new WSMSDFault(CUBConstants.Country.COUNTRY_STRING, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_COUNTRY_REQUIRED));
                    return response;
                }

                // NOTE: the CountryExists method in the next If statement expects 2 digits country code as it compares against cub_alpha2 field.
                // If we need a way to make the lenght configurable but at the same time make sure the appropriate field is used inside CountryExists
                if (!UtilityLogic.IsStringLengthValid(address.country, 2))  // this test will return false if there is a value AND not the correct length
                {
                    response.Errors.Add(new WSMSDFault(CUBConstants.Country.COUNTRY_STRING, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                cub_Country cntry = CountryExists(address.country);     //lookup by string first
                if (cntry == null)
                {
                    cntry = CountryExists(address.countryId);           //lookup by id next
                    if (cntry == null)
                    {
                        response.Errors.Add(new WSMSDFault(CUBConstants.Country.COUNTRY_STRING, CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_COUNTRY));
                        return response;
                    }
                    else
                    {
                        address.country = cntry.cub_Alpha2;
                    }
                }
                else
                {
                    address.countryId = cntry.cub_CountryId;
                }

                response.ResponseObject.country = cntry.cub_Alpha2;
                response.ResponseObject.countryId = cntry.cub_CountryId;

                #endregion
                
                if (string.IsNullOrEmpty(address.address1))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_street1Attribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_ADDRESS1_REQUIRED));
                    return response;
                }

                // NOTE: if we make max city lenght of 50 configurable, we'll need to dynamically get the city field lenght as defined on the entity and make sure the 
                // max lenght doesn't exceed the field defined length
                if (!UtilityLogic.IsStringLengthValid(address.address1, 64))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_street1Attribute, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                response.ResponseObject.address1 = address.address1;

                if (string.IsNullOrEmpty(address.address2))
                {
                    //check if required per country
                    string Address2 = GetAttributeOptionSetText(cntry,cub_Country.cub_address2Attribute, typeof(cub_Countrycub_Address2)).ToString();
                    if (Address2 == cub_Countrycub_Address2.REQ.ToString())
                    {
                        response.Errors.Add(new WSMSDFault(cub_Address.cub_street2Attribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_ADDRESS2_REQUIRED));
                        return response;
                    }
                }

                // NOTE: if we make max city lenght of 50 configurable, we'll need to dynamically get the city field lenght as defined on the entity and make sure the 
                // max lenght doesn't exceed the field defined length
                if (!UtilityLogic.IsStringLengthValid(address.address2, 64))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_street2Attribute, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;

                }

                response.ResponseObject.address2 = address.address2;

                if (string.IsNullOrEmpty(address.city))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_cityAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_CITY_REQUIRED));
                    return response;
                }

                // NOTE: if we make max city lenght of 50 configurable, we'll need to dynamically get the city field lenght as defined on the entity and make sure the 
                // max lenght doesn't exceed the field defined length
                if (!UtilityLogic.IsStringLengthValid(address.city, 64))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_cityAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                response.ResponseObject.city = address.city;

                #region State
                if (string.IsNullOrEmpty(address.state) && address.stateId == null)
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_stateprovinceidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_STATE_REQUIRED));
                    return response;
                }

                // if state id is null but state string value is not
                if (address.stateId == null && !string.IsNullOrWhiteSpace(address.state))
                {
                    response.ResponseObject.state = address.state;
                    if (StateExistsForCountry(address.state, cntry.Id, out stateId))
                    {
                        response.ResponseObject.stateId = stateId;
                        address.stateId = stateId;
                    }
                    else
                    {
                        response.Errors.Add(new WSMSDFault(cub_Address.cub_stateprovinceidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_STATE));
                        return response;
                    }
                }
                // if state id is not null but state string value is
                else if (address.stateId != null && string.IsNullOrWhiteSpace(address.state))
                {
                    response.ResponseObject.stateId = address.stateId;
                    //check state id and country combination is correct
                    if (StateExistsForCountry(address.stateId, cntry.Id, out stateId))
                    {
                        cub_StateOrProvince stateEntity = GetStateById(stateId);
                        response.ResponseObject.state = stateEntity.cub_Abbreviation;
                        address.state = stateEntity.cub_Abbreviation;
                    }
                    else
                    {
                        response.Errors.Add(new WSMSDFault(cub_Address.cub_stateprovinceidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_STATE));
                        return response;
                    }
                }

                //if we get here we should have both the string value of the state and the state id

                // NOTE: the StateExistsForCountry method in the next If statement expects 2 digits state code as it compares against cub_abbreviation field.
                // TO DO: We'll need a way to make the lenght configurable but at the same time make sure the appropriate field is used inside StateExistsForCountry
                if (!UtilityLogic.IsStringLengthValid(address.state, 2) && address.stateId == null)
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_stateprovinceidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }
                #endregion
                
                #region Zip Code
                if (string.IsNullOrEmpty(address.postalCode) && address.postalCodeId == null)
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_POSTAL_CODE_REQUIRED));
                    return response;
                }

                cub_ZipOrPostalCode zipEntity;
                // if zip id is null but zip string value is not
                if (address.postalCodeId == null && !string.IsNullOrWhiteSpace(address.postalCode))
                {
                    response.ResponseObject.postalCode = address.postalCode;
                    if (ZipExistsForCountry(address.postalCode, cntry.Id, out zipEntity))
                    {
                        response.ResponseObject.postalCodeId = zipEntity.cub_ZipOrPostalCodeId;
                        address.postalCodeId = zipEntity.cub_ZipOrPostalCodeId;
                    }
                    else
                    {
                        response.Errors.Add(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_POSTAL_CODE_INVALID_FOR_COUNTRY));
                        return response;
                    }
                }
                // if zip id is not null but zip string value is
                else if (address.postalCodeId != null && string.IsNullOrWhiteSpace(address.postalCode))
                {
                    response.ResponseObject.postalCodeId = address.postalCodeId;
                    //check zip id and country combination is correct
                    if (ZipExistsForCountry(cntry.Id, address.postalCodeId.Value))
                    {
                        zipEntity = GetZipById(address.postalCodeId.Value);
                        response.ResponseObject.postalCode = zipEntity.cub_Code;
                        address.postalCode = zipEntity.cub_Code;
                    }
                    else
                    {
                        response.Errors.Add(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_POSTAL_CODE_INVALID_FOR_COUNTRY));
                        return response;
                    }
                }

                if (!string.IsNullOrEmpty(cntry.cub_ZipCodeValidationRegex))
                {
                    if (!Regex.IsMatch(address.postalCode.Trim(), cntry.cub_ZipCodeValidationRegex.Trim()))
                    {
                        response.Errors.Add(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_INVALID_POSTAL_CODE, CUBConstants.Error.MSG_POSTAL_CODE_INVALID_FORMAT_FOR_COUNTRY));
                        return response;
                    }
                }
                #endregion

            }
            catch(Exception ex)
            {
                CubLogger.Error(methodName, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                throw;
            }
            
            return response;
        }

        /// <summary>
        /// Check if a address is valid
        /// </summary>
        /// <param name="address">Address</param>
        /// <returns>CubResponse<WSAddress> cub_address is converted to WSAddress in the response</returns>
        private CubResponse<WSAddress> IsAddressValid(cub_Address address)
        {
            const string methodName = "IsAddressValid";
            CubResponse<WSAddress> response = new CubResponse<WSAddress>();
            response.ResponseObject = new WSAddress();

            try
            {
                if (address == null)
                {
                    response.Errors.Add(new WSMSDFault(CUBConstants.Address.ADDRESS_STRING, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Address.Error.ERR_CUB_ADDRESS_NULL));
                }

                if (address.cub_CountryId == null)
                {
                    response.Errors.Add(new WSMSDFault(CUBConstants.Country.COUNTRY_STRING, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_COUNTRY_REQUIRED));
                }

                cub_Country cntry = GetCountryById(address.cub_CountryId.Id);

                if (cntry == null)
                {
                    response.Errors.Add(new WSMSDFault(CUBConstants.Country.COUNTRY_STRING, CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_COUNTRY));
                }

                response.ResponseObject.countryId = address.cub_CountryId.Id;
                response.ResponseObject.country = cntry.cub_CountryCode;

                if (string.IsNullOrEmpty(address.cub_Street1))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_street1Attribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_ADDRESS1_REQUIRED));
                }
                
                if (!UtilityLogic.IsStringLengthValid(address.cub_Street1, 64))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_street1Attribute, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                }

                response.ResponseObject.address1 = address.cub_Street1;

                if (string.IsNullOrEmpty(address.cub_Street2))
                {
                    //check if required per country
                    string Address2 = GetAttributeOptionSetText(cntry, cub_Country.cub_address2Attribute, typeof(cub_Countrycub_Address2)).ToString();
                    if (Address2 == cub_Countrycub_Address2.REQ.ToString())
                    {
                        response.Errors.Add(new WSMSDFault(cub_Address.cub_street2Attribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_ADDRESS2_REQUIRED));
                    }
                }

                if (!UtilityLogic.IsStringLengthValid(address.cub_Street2, 64))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_street2Attribute, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));

                }

                response.ResponseObject.address2 = address.cub_Street2;

                if (string.IsNullOrEmpty(address.cub_City))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_cityAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_CITY_REQUIRED));
                }

                if (!UtilityLogic.IsStringLengthValid(address.cub_City, 64))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_cityAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                }

                response.ResponseObject.city = address.cub_City;

                if (address.cub_StateProvinceId == null)
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_stateprovinceidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_STATE_REQUIRED));
                }

                cub_StateOrProvince state = GetStateById(address.cub_StateProvinceId.Id);
                if (state == null)
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_stateprovinceidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_STATE_REQUIRED));
                }

                if (!StateExistsForCountry(address.cub_CountryId.Id, address.cub_StateProvinceId.Id))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_stateprovinceidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Address.Error.ERR_STATE_ZIP_INVALID_COMBO));
                }

                response.ResponseObject.stateId = address.cub_StateProvinceId.Id;
                response.ResponseObject.state = state.cub_Abbreviation;

                cub_ZipOrPostalCode zip = GetZipById(address.cub_PostalCodeId.Id);
                if (zip == null)
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_POSTAL_CODE_REQUIRED));
                }

                if (!ZipExistsForCountry(address.cub_CountryId.Id, address.cub_PostalCodeId.Id))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_POSTAL_CODE_INVALID_FOR_COUNTRY));
                }

                response.ResponseObject.postalCodeId = address.cub_PostalCodeId.Id;
                response.ResponseObject.postalCode = zip.cub_Code;

            }
            catch (Exception ex)
            {
                CubLogger.Error(methodName, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Validate address Id. Exit the function on first validation failure
        /// </summary>
        /// <param name="id">string of Address id</param>
        /// <returns>WSMSDResponse</returns>
        public CubResponse<WSAddress> IsAddressIdValid(string id)
        {
            CubResponse<WSAddress> response = new CubResponse<WSAddress>();

            //if (!UtilityLogic.IsStringLengthValid(id, 20))
            //{
            //    response.ErrorsMessages.Add(new WSMSDFault("Address & AddressId", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
            //    return response;
            //}

            Guid guid;
            if (!Guid.TryParse(id, out guid))
            {
                response.Errors.Add(new WSMSDFault("Address & AddressId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_ADDRESS_NOT_FOUND));
                return response;
            }
            else
            {
                cub_Address existingAddress = null;
                if (!AddressExists(guid, out existingAddress))
                {
                    response.Errors.Add(new WSMSDFault("Address & AddressId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_ADDRESS_NOT_FOUND));
                    return response;    
                }
                else
                {
                    response.ResponseObject.address1 = existingAddress.cub_Street1;
                    response.ResponseObject.address2 = existingAddress.cub_Street2;
                    response.ResponseObject.address3 = existingAddress.cub_Street3;
                    response.ResponseObject.addressId = existingAddress.cub_AddressId;
                    response.ResponseObject.city = existingAddress.cub_City;
                    response.ResponseObject.country = existingAddress.cub_CountryId.Name;
                    response.ResponseObject.countryId = existingAddress.cub_CountryId.Id;
                    response.ResponseObject.postalCode = existingAddress.cub_PostalCodeId.Name;
                    response.ResponseObject.postalCodeId = existingAddress.cub_PostalCodeId.Id;
                    response.ResponseObject.state = existingAddress.cub_StateProvinceId.Name.ToString();
                    response.ResponseObject.stateId = existingAddress.cub_StateProvinceId.Id;
                }
            }

            return response;
        }

        //public bool IsZipValid(string zip, string country, out Guid zipId)
        //{
        //    bool rtn = false;

        //    if (string.IsNullOrEmpty(zip))
        //        throw new FaultException<WSMSDFault>(new WSMSDFault("Zip", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_POSTAL_CODE_REQUIRED));
        //    else
        //    {

        //        rtn = ZipExistsForCountry(zip, country, out zipId);
        //        //regex check format per country
        //        //if (!matches regex)
        //        //zipValidateErrors = AddErrorTolist(zipValidateErrors, CUBConstants.Error.ERR_GENERAL_INVALID_POSTAL_CODE, CUBConstants.Error.MSG_INVALID_FORMAT_DICTATED);


        //    }

        //    return rtn;
        //}

        //private bool IsZipValid(string zip, cub_Country country, out Guid zipId)
        //{
        //    bool rtn = false;

        //    if (string.IsNullOrEmpty(zip))
        //        throw new FaultException<WSMSDFault>(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_POSTAL_CODE_REQUIRED));

        //    cub_ZipOrPostalCode ziporpostalcode = null;
        //    if (!ZipExistsForCountry(zip, country.Id, out ziporpostalcode))
        //        throw new FaultException<WSMSDFault>(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_INVALID_POSTAL_CODE, CUBConstants.Error.MSG_POSTAL_CODE_INVALID_FOR_COUNTRY));

        //    zipId = ziporpostalcode.Id;

        //    if (!string.IsNullOrEmpty(country.cub_ZipCodeValidationRegex))
        //    {
        //        if (!Regex.IsMatch(zip.Trim(), country.cub_ZipCodeValidationRegex.Trim()))
        //            throw new FaultException<WSMSDFault>(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_INVALID_POSTAL_CODE, CUBConstants.Error.MSG_POSTAL_CODE_INVALID_FORMAT_FOR_COUNTRY));
        //    }

        //    rtn = true;

        //    return rtn;
        //}

        /// <summary>
        /// Checks if a Zipcode is valid
        /// </summary>
        /// <param name="address">Address</param>
        /// <param name="country">Country</param>
        /// <returns>CubResponse<WSAddress></returns>
        private CubResponse<WSAddress> IsZipValid(WSAddress address, cub_Country country)
        {
            CubResponse<WSAddress> response =new  CubResponse<WSAddress>();



            if (string.IsNullOrEmpty(address.postalCode))
            {
                response.Errors.Add(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_POSTAL_CODE_REQUIRED));
                return response;
            }

            cub_ZipOrPostalCode ziporpostalcode = null;
            if (address.countryId.HasValue && !ZipExistsForCountry(address.postalCode, address.countryId.Value, out ziporpostalcode))
            {
                response.Errors.Add(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_INVALID_POSTAL_CODE, CUBConstants.Error.MSG_POSTAL_CODE_INVALID_FOR_COUNTRY));
                return response;
            }

            address.postalCodeId = ziporpostalcode.Id;

            if (!string.IsNullOrEmpty(country.cub_ZipCodeValidationRegex))
            {
                if (!Regex.IsMatch(address.postalCode.Trim(), country.cub_ZipCodeValidationRegex.Trim()))
                {
                    response.Errors.Add(new WSMSDFault(cub_Address.cub_postalcodeidAttribute, CUBConstants.Error.ERR_GENERAL_INVALID_POSTAL_CODE, CUBConstants.Error.MSG_POSTAL_CODE_INVALID_FORMAT_FOR_COUNTRY));
                    return response;
                }
            }


            response.ResponseObject = address;
            return response;
        }


        /// <summary>
        /// Looks up an existing address for a customer using the address 1, 2 and city from WSAddress and country, zip and state Ids from ContactUpdateReturnInfo
        /// </summary>
        /// <param name="address"></param>
        /// <param name="contactUptInfo"></param>
        /// <returns></returns>
        private bool ExistingAddress(WSAddress address, Guid customerId, out Guid addressId)
        {
            bool returnValue = false;
            addressId = Guid.Empty;

            address.address1 = string.IsNullOrWhiteSpace(address.address1) ? null : address.address1.Trim();
            address.address2 = string.IsNullOrWhiteSpace(address.address2) ? null : address.address2.Trim();
            address.address3 = string.IsNullOrWhiteSpace(address.address3) ? null : address.address3.Trim();
            address.city = string.IsNullOrEmpty(address.city) ? null : address.city.Trim();

            try
            {
                List<cub_Address> existingAddress = (from add in CubOrgSvc.cub_AddressSet
                                                     where (add.cub_Street1.Equals(address.address1)
                                                          && add.cub_Street2.Equals(address.address2)
                                                          && add.cub_Street3.Equals(address.address3)
                                                          && add.cub_City.Equals(address.city)
                                                          && add.cub_StateProvinceId.Id.Equals(address.stateId.Value)
                                                          && add.cub_CountryId.Id.Equals(address.countryId.Value)
                                                          && add.cub_PostalCodeId.Id.Equals(address.postalCodeId.Value)
                                                          && add.cub_AccountAddressId.Equals(customerId)
                                                          && add.statecode.Value == cub_AddressState.Active
                                                          )
                                                     select add).ToList();

                if (existingAddress.Count > 0)
                    addressId = existingAddress[0].cub_AddressId.Value;

                returnValue = existingAddress.Count > 0;

            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
            }

            return returnValue;

        }


        /// <summary>
        /// Check if a address exists
        /// </summary>
        /// <param name="address">Address</param>
        /// <param name="addressId">Address ID</param>
        /// <returns>Exist or Not</returns>
        private bool ExistingAddress(cub_Address address, out Guid addressId)
        {
            bool returnValue = false;
            addressId = Guid.Empty;
            
            try
            {
                List<cub_Address> existingAddress = GetAddressList(address);

                if (existingAddress.Count > 0)
                    addressId = existingAddress[0].cub_AddressId.Value;

                returnValue = existingAddress.Count > 0;
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
            }

            return returnValue;

        }

        /// <summary>
        /// Get Address List
        /// </summary>
        /// <param name="address">Address</param>
        /// <returns>Address List</returns>
        private List<cub_Address> GetAddressList(cub_Address address)
        {
            address.cub_Street1 = string.IsNullOrWhiteSpace(address.cub_Street1) ? null : address.cub_Street1.Trim();
            address.cub_Street2 = string.IsNullOrWhiteSpace(address.cub_Street2) ? null : address.cub_Street2.Trim();
            address.cub_Street3 = string.IsNullOrWhiteSpace(address.cub_Street3) ? null : address.cub_Street3.Trim();
            address.cub_City = string.IsNullOrWhiteSpace(address.cub_City) ? null : address.cub_City.Trim();

            return (from add in CubOrgSvc.cub_AddressSet
                                                 where (add.cub_Street1.Equals(address.cub_Street1)
                                                      && add.cub_Street2.Equals(address.cub_Street2)
                                                      && add.cub_Street3.Equals(address.cub_Street3)
                                                      && add.cub_City.Equals(address.cub_City)
                                                      && add.cub_StateProvinceId.Id.Equals(address.cub_StateProvinceId.Id)
                                                      && add.cub_CountryId.Id.Equals(address.cub_CountryId.Id)
                                                      && add.cub_PostalCodeId.Id.Equals(address.cub_PostalCodeId.Id)
                                                      && add.cub_AccountAddressId.Equals(address.cub_AccountAddressId.Id)
                                                      && add.statecode.Value == cub_AddressState.Active
                                                      )
                                                 select add).ToList();
        }

        /// <summary>
        /// Update Address - called by rest service (Incoming)
        /// </summary>
        /// <param name="address"></param>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public CubResponse<WSAddress> UpdateAddress(WSAddress address, Guid customerId, Guid addressId)
        {
            CubResponse<WSAddress> returnValue = new CubResponse<WSAddress>();

            try
            {
                CubLogger.Debug("Method Entered");

                #region account id validation
                CubLogger.Debug("Validating customerId");
                ValidateCustomer(customerId);
                #endregion account validation

                #region address id validation
                CubLogger.Debug("Validating addressId");
                cub_Address existingAddress = null;
                if (!AddressExists(addressId, out existingAddress))
                {
                    CubLogger.Error(string.Format("address {0} not found", addressId.ToString()));
                    throw new FaultException<WSMSDFault>(new WSMSDFault(CUBConstants.Address.ADDRESS_STRING, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_ADDRESS_NOT_FOUND));
                }
                #endregion address id validation

                CubLogger.Debug("Validating address fields");

                returnValue = IsAddressValid(address);
                if (returnValue.Success)
                {
                    CubLogger.Debug("address fields are valid");

                    existingAddress.cub_Street1 = address.address1?.Trim();
                    existingAddress.cub_Street2 = address.address2?.Trim();
                    existingAddress.cub_Street3 = address.address3?.Trim();
                    existingAddress.cub_City = address.city?.Trim();
                    existingAddress.cub_CountryId.Id = address.countryId.Value;
                    existingAddress.cub_PostalCodeId.Id = address.postalCodeId.Value;
                    existingAddress.cub_StateProvinceId.Id = address.stateId.Value;

                    existingAddress.EntityState = EntityState.Changed;

                    CubLogger.Debug("updating address in CRM");
                    _organizationService.Update(existingAddress);

                    // we dont need to call this here, it will be called once address is update as 
                    // part of address post update message in plugin
                    //CubLogger.("calling UpdateDependencies", LogLevel.Debug);
                    //UpdateDependencies(existingAddress.Id);
                }
                else
                {
                    CubLogger.Debug($"Error returned by IsAddressValid errorKey:{returnValue.Errors[0].errorKey}, fieldName:{returnValue.Errors[0].fieldName}, errorMessage:{returnValue.Errors[0].errorMessage}");
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(null, ex);
                returnValue.Errors.Add(new WSMSDFault(string.Empty, string.Empty, ex.Message));
            }

            CubLogger.Debug("Method Exit");
            return returnValue;
        }

        /// <summary>
        /// Update Address - called by MSD, makes call out to NIS (Outgoing)
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <param name="requestBody"></param>
        /// <returns></returns>
        public CubResponse<WSAddressUpdateResponse> UpdateAddress(string customerId, string addressId, WSAddressUpdateRequest requestBody)
        {
            CubResponse<WSAddressUpdateResponse> updateAddressResp = new CubResponse<WSAddressUpdateResponse>();

            try
            {
                //validate address
                CubResponse<WSAddress> addressValidation = IsAddressValid(requestBody.address);

                //note: the missing string values for country, state and zip in requestBody.address will be set
                // as long as the ids are provided
                // they are returned as part of the response.

                if (addressValidation.Success)
                {
                    using (var nisApi = new NISApiLogic(_organizationService))
                    {
                        updateAddressResp.ResponseObject.hdr = nisApi.UpdateAddress(customerId, addressId, requestBody);

                    }
                }
                else
                {
                    updateAddressResp.Errors = addressValidation.Errors;
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(null, ex);
                //updateAddressResp.Errors.Add(new WSMSDFault(string.Empty, string.Empty, ex.Message));
                throw;
            }

            return updateAddressResp;
        }

        /// <summary>
        /// Validates customer Id is a valid account. If the account is not found an exception is thrown. 
        /// </summary>
        /// <param name="customerId"></param>
        private void ValidateCustomer(Guid customerId)
        {
            AccountLogic al = new AccountLogic(_organizationService);
            Account acc = al.GetById(customerId);
            if (acc == null || !acc.AccountId.HasValue || acc.AccountId.Equals(Guid.Empty))
            {
                CubLogger.Error(string.Format("customer {0} not found", customerId.ToString()));
                acc = null;
                throw new Exception(CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND);
                //throw new FaultException<WSMSDFault>(new WSMSDFault(Account.accountidAttribute, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND));
            }
        }

        /// <summary>
        /// Updates NIS with address changes. Called from post address update plugin
        /// </summary>
        /// <param name="addressId"></param>
        public void UpdateDependencies(Guid addressId)
        {
            CUB.MSD.Model.NIS.WSNISUpdateDependencies addy = GetUpdateDependenciesDataById(addressId);
            if (addy == null) return;

            Guid customerId = addy.customerId;

            CUB.MSD.Model.NIS.WSAddress wsAddr = new CUB.MSD.Model.NIS.WSAddress();
            wsAddr = addy.wsaddress;

            NISApiLogic nisApi = new NISApiLogic(_organizationService);
            nisApi.UpdateDependencies(customerId, addressId, wsAddr);
            nisApi = null;
        }
        
        /// <summary>
        /// Makes a call to NIS to make sure there are no dependencies. If there are none then delete the address
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public CubResponse<WSAddressDeleteResponse> DeleteAddress(Guid customerId, Guid addressId)
        {
            WSAddressDeleteResponse deleteResponse = new WSAddressDeleteResponse();
            CubResponse<WSAddressDeleteResponse> returnValue = new CubResponse<WSAddressDeleteResponse>();

            try
            {

                #region account id validation
                CubLogger.Debug("Validating customerId");
                ValidateCustomer(customerId);
                #endregion account validation                

                #region address id validation
                CubLogger.Error("Validating addressId");
                cub_Address existingAddress = null;
                if (!AddressExists(addressId, out existingAddress))
                {
                    CubLogger.Error(string.Format("addredd {0} not found", addressId.ToString()));
                    throw new FaultException<WSMSDFault>(new WSMSDFault(CUBConstants.Address.ADDRESS_STRING, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_ADDRESS_NOT_FOUND));
                }
                #endregion address id validation

                HardOrSoftDeleteAddress(existingAddress);
            }
            catch (Exception ex)
            {
                CubLogger.Error(null, ex);
                returnValue.Errors.Add(new WSMSDFault(string.Empty, string.Empty, ex.Message));
            }

            return returnValue;
        }

        /// <summary>
        /// Deletes or mark an address inactive
        /// </summary>
        /// <param name="existingAddress"></param>
        private void HardOrSoftDeleteAddress(cub_Address existingAddress)
        {
            //Get org config for soft delete or not
            bool softDelete = GlobalsCache.AppInfoSoftDeleteAddress(_organizationService);

            //Mark inactive or delete contact
            if (softDelete)
            {
                SetState2(existingAddress.Id, cub_Address.EntityLogicalName, (int)cub_AddressState.Inactive, (int)cub_address_statuscode.Deleted);
            }
            else
            {
                _organizationService.Delete(cub_Address.EntityLogicalName, existingAddress.Id);
            }
        }
    }
}
