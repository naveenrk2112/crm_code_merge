﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Linq;
using System;

namespace CUB.MSD.Logic
{
    public class ActivityLogic : LogicBase
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="orgService">MSD Organization Service instance</param>
        public ActivityLogic(IOrganizationService orgService) : base(orgService)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="orgService">MSD Organization Service instance</param>
        /// <param name="context">MSD Plugin Execution Context object</param>
        public ActivityLogic(IOrganizationService orgService, IPluginExecutionContext context) : base(orgService, context)
        {
        }

        #endregion

        /// <summary>
        /// Retrieve all MSD activities related to a transit account, including 
        /// any related to the linked customer, if applicable.
        /// </summary>
        /// <param name="transitAccountId">Transit account ID</param>
        /// <returns>A list of MSD activities, ordered by most recently modified</returns>
        public WSGetActivitiesResponse GetByTransitAccount(string transitAccountId)
        {
            var response = new WSGetActivitiesResponse();

            if (string.IsNullOrEmpty(transitAccountId))
            {
                response.activities = new List<WSMSDActivity>();
                return response;
            }
            
            var customerRef = (from c in CubOrgSvc.AccountSet
                               join t in CubOrgSvc.cub_TransitAccountSet
                                    on c.AccountId equals t.cub_Customer.Id
                               where t.cub_Name == transitAccountId
                               select new EntityReference(Account.EntityLogicalName, c.Id))
                               .FirstOrDefault();

            var activityFetch = $@"
                <fetch>
                    <entity name='activitypointer'>
                        <attribute name='subject' />
                        <attribute name='description' />
                        <attribute name='statecode' />
                        <attribute name='statuscode' />
                        <attribute name='activitytypecode' />
                        <attribute name='modifiedon' />
                        <attribute name='modifiedby' />
                        <order attribute='modifiedon' descending='true' />
                        {(customerRef != null
                            ? @"<link-entity name='account' from='accountid' to='regardingobjectid' link-type='outer'>
                                    <attribute name='accountid' />
                             </link-entity>"
                            : string.Empty)}
                        <link-entity name='cub_transitaccount' from='cub_transitaccountid' to='regardingobjectid' link-type='outer'>
                            <attribute name='cub_name' />
                        </link-entity>
                        <filter type='or'>
                            {(customerRef != null
                                ? $"<condition entityname='account' attribute='accountid' operator='eq' value='{customerRef.Id}' />"
                                : string.Empty)}
                            <condition entityname='cub_transitaccount' attribute='cub_name' operator='eq' value='{transitAccountId}' />
                        </filter>
                    </entity>
                </fetch>";
            var activities = _organizationService.RetrieveMultiple(new FetchExpression(activityFetch))
                .Entities.Select(e =>
                {
                    var a = e.ToEntity<ActivityPointer>();
                    return new WSMSDActivity
                    {
                        id = a.Id,
                        subject = a.Subject,
                        description = a.Description,
                        state = a.StateCode.ToString(),
                        status = ((OptionSetValue)a.StatusCode).Value.ToString(),
                        modifiedOn = a.ModifiedOn,
                        modifiedBy = a.ModifiedBy.Name,
                        type = a.ActivityTypeCode
                    };
                }).ToList();
            response.activities = activities;
            return response;
        }

        public void SetGroupId(cub_CustomActivity cubCustomActivity)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(cubCustomActivity.cub_GroupID))
                {
                    cubCustomActivity.cub_GroupID = cubCustomActivity.cub_Session?.Id.ToString().ToUpper() ?? 
                        cubCustomActivity.Id.ToString().Replace("}", string.Empty).Replace("{", string.Empty).ToUpper();
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(null, ex);
            }
        }

        /// <summary>
        /// Create a record of type 'New Activity' in the entity cub_CustomActivity
        /// </summary>
        /// <returns></returns>
        public CubResponse<WSCreateNewActivityResponse> CreateNewActivity(CreateMSDActivityData activityData)
        {
            string methodName = "CreateNewActivity";
            CubResponse<WSCreateNewActivityResponse> resp = new CubResponse<WSCreateNewActivityResponse>();
            cub_ActivityType activityTypeRecord = new cub_ActivityType();
            ActivityAutoCreateConfigData activityAutoCreateConfigList = new ActivityAutoCreateConfigData();
            ActivityAutoCreateConfig autoCreateConfig = new ActivityAutoCreateConfig();

            try
            {
                #region Required fields check
                if (activityData.ActivityTypeId.Equals(Guid.Empty))
                {
                    resp.Errors.Add(new WSMSDFault(cub_CustomActivity.cub_activitytypeAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                }
                else
                {
                    //get the activity type record
                    activityTypeRecord = CubOrgSvc.cub_ActivityTypeSet
                            .Where(t => t.Id == activityData.ActivityTypeId)
                            .Single()
                            .ToEntity<cub_ActivityType>();

                    #region Get Activity Auto Create Rules/Configuration

                    if (!string.IsNullOrWhiteSpace(activityTypeRecord.cub_ActivityAutoCreateConfig))
                    {
                        activityAutoCreateConfigList = activityTypeRecord.cub_ActivityAutoCreateConfig.DeserializeObject<ActivityAutoCreateConfigData>();
                        //get the configuration for this particular type of activity
                        autoCreateConfig = activityAutoCreateConfigList.ActivityAutoCreateConfig
                            .Where(c => c.ActivityType == cub_CustomActivity.EntityLogicalName).FirstOrDefault();
                    }

                    #endregion

                    #region Set Subject
                    if (!activityData.ActivitySubjectId.Equals(Guid.Empty))
                    {
                        //if subject id was provided, make sure it is correct, otherwise replace it with the subject id from the type record
                        if (!activityData.ActivitySubjectId.Equals(activityTypeRecord.cub_ActivitySubject.Id))
                        {
                            activityData.ActivitySubjectId = activityTypeRecord.cub_ActivitySubject.Id;
                        }
                    }
                    else
                    {
                        //get subject using type id. 
                        activityData.ActivitySubjectId = activityTypeRecord.cub_ActivitySubject.Id;
                    }
                    #endregion

                }
                #endregion

                // If creation is enabled for this activity type, then continue
                if (autoCreateConfig.Enabled)
                {
                    #region If no errors, create activity
                    if (resp.Errors.Count <= 0)
                    {
                        cub_CustomActivity ca = new cub_CustomActivity();
                        ca.cub_ActivityType = new EntityReference(cub_ActivityType.EntityLogicalName, activityData.ActivityTypeId);
                        ca.cub_ActivitySubject = new EntityReference(cub_ActivitySubject.EntityLogicalName, activityData.ActivitySubjectId);
                        if (!activityData.SessionId.Equals(Guid.Empty))
                            ca.cub_Session = new EntityReference(cub_CSRSession.EntityLogicalName, activityData.SessionId);
                        if (!activityData.SubsystemId.Equals(Guid.Empty))
                        {
                            ca.cub_TransitAccountID = new EntityReference(cub_TransitAccount.EntityLogicalName, activityData.SubsystemId);
                        }

                        // if contact id was provided but no account id, lookup and set account id
                        if (activityData.CustomerId.Equals(Guid.Empty) && !activityData.ContactId.Equals(Guid.Empty))
                        {
                            activityData.CustomerId = CubOrgSvc.ContactSet
                                .Where(c => c.Id == activityData.ContactId)
                                .Select(c => c.ParentCustomerId.Id).SingleOrDefault();
                        }

                        if (!activityData.CustomerId.Equals(Guid.Empty))
                            ca.cub_Customer = new EntityReference(Account.EntityLogicalName, activityData.CustomerId);
                        if (!activityData.ContactId.Equals(Guid.Empty))
                        {
                            ca.cub_Contact = new EntityReference(Contact.EntityLogicalName, activityData.ContactId);
                            ca.RegardingObjectId = ca.cub_Contact;
                        }
                        if (!activityData.CaseId.Equals(Guid.Empty))
                            ca.cub_Case = new EntityReference(Incident.EntityLogicalName, activityData.CaseId);
                        if (!activityData.TaskId.Equals(Guid.Empty))
                            ca.cub_Task = new EntityReference(Task.EntityLogicalName, activityData.TaskId);
                        if (!activityData.CreatedById.Equals(Guid.Empty))
                            ca.OwnerId = new EntityReference(SystemUser.EntityLogicalName, activityData.CreatedById);

                        cub_activitycategory activityCategoryEnum;
                        if (!Enum.TryParse(activityData.Category, out activityCategoryEnum))
                            activityCategoryEnum = cub_activitycategory.Activity;

                        cub_channel activityChannelEnum;
                        if (!Enum.TryParse(activityData.Channel, out activityChannelEnum))
                            activityChannelEnum = cub_channel.CRM;

                        ca.cub_ActivityCategory = new OptionSetValue(Convert.ToInt32(activityCategoryEnum));
                        ca.cub_Channel = new OptionSetValue(Convert.ToInt32(activityChannelEnum));

                        // Set OOTB regarding object depending on what Id is available
                        if (!activityData.ContactId.Equals(Guid.Empty))
                            ca.RegardingObjectId = ca.cub_Contact;
                        else if (!activityData.CustomerId.Equals(Guid.Empty))
                            ca.RegardingObjectId = ca.cub_Customer;

                        // Set Description/Notes
                        ca.Description = activityData.Description;


                        // Since the required field check is dynamics, need to keep this check after the entity cub_CustomActivity is 
                        // instansiated and the data is set
                        if (autoCreateConfig?.RequiredFields?.Count > 0)
                        {
                            foreach (string field in autoCreateConfig.RequiredFields)
                            {
                                if (ca.GetAttributeValue<dynamic>(field) == null)
                                {
                                    resp.Errors.Add(new WSMSDFault(field, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                                }
                            }
                        }

                        //Set State
                        cub_CustomActivityState statecodeEnum;
                        if (!Enum.TryParse(autoCreateConfig?.StateCode, out statecodeEnum))
                            statecodeEnum = cub_CustomActivityState.Completed;

                        //Set Status
                        cub_customactivity_statuscode statuscodeEnum;
                        if (!Enum.TryParse(autoCreateConfig?.StatusCode, out statuscodeEnum))
                            statuscodeEnum = cub_customactivity_statuscode.Closed;


                        if (resp.Errors.Count <= 0)
                        {
                            resp.ResponseObject.Id = _organizationService.Create(ca);

                            // Set Status as neeed by the configuration
                            SetState2(resp.ResponseObject.Id, cub_CustomActivity.EntityLogicalName, (int)statecodeEnum, (int)statuscodeEnum);

                        }

                    }
                    #endregion
                }
                else
                {
                    CubLogger.Info($"Creating activity of type {activityTypeRecord.cub_type} is not enabled in the configuration.");
                }

            }
            catch (Exception ex)
            {
                resp.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }
            

            return resp;
        }

        public Guid GetActivityTypeId(string activityName)
        {
            return CubOrgSvc.cub_ActivityTypeSet.
            Where(at => at.cub_type == activityName)
            .Select(atId => atId.Id).Single();
        }
    }

    public class ActivityPointerLogic : LogicBase
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="orgService">MSD Organization Service instance</param>
        public ActivityPointerLogic(IOrganizationService orgService) : base(orgService)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="orgService">MSD Organization Service instance</param>
        /// <param name="context">MSD Plugin Execution Context object</param>
        public ActivityPointerLogic(IOrganizationService orgService, IPluginExecutionContext context) : base(orgService, context)
        {
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qe"></param>
        /// <param name="contactActivitiesRollUpToCustomerTriggerCondition"></param>
        /// <param name="regardingCondition"></param>
        public static void CreateCustomerRollUpActivitiesQueryExpression(QueryExpression qe, Guid regardingId)
        {
            CubLogger.Debug("Rollup activities to account");

            CubLogger.Debug("Adding distinct to avoid duplicates");
            qe.Distinct = true;

            #region Activity Party
            var leParty = AddLinkToQuery(qe, ActivityParty.EntityLogicalName, ActivityParty.activityidAttribute, ActivityParty.activityidAttribute, "activityparty", JoinOperator.LeftOuter);
            var leAccount = AddLinkToLinkedEntity(leParty, Account.EntityLogicalName, Account.accountidAttribute, ActivityParty.partyidAttribute, "accountparty", JoinOperator.LeftOuter);
            var leContact = AddLinkToLinkedEntity(leParty, Contact.EntityLogicalName, Contact.contactidAttribute, ActivityParty.partyidAttribute, "contactparty", JoinOperator.LeftOuter);
            var leContactAccount = AddLinkToLinkedEntity(leContact, Account.EntityLogicalName, Account.accountidAttribute, Contact.parentcustomeridAttribute, "contactaccount", JoinOperator.LeftOuter);
            #endregion

            #region Transit Account
            var leTA = AddLinkToQuery(qe, cub_TransitAccount.EntityLogicalName, cub_TransitAccount.cub_transitaccountidAttribute, ActivityPointer.regardingobjectidAttribute, "transitaccount", JoinOperator.LeftOuter);
            var leTAAccount = AddLinkToLinkedEntity(leTA, Account.EntityLogicalName, Account.accountidAttribute, cub_TransitAccount.cub_customerAttribute, "transitaccountaccount", JoinOperator.LeftOuter);
            #endregion

            CubLogger.Debug("Define filter for any related activity");
            var feOrConditions = new FilterExpression(LogicalOperator.Or);
            qe.Criteria.AddFilter(feOrConditions);
            feOrConditions.AddCondition(leAccount.EntityAlias, leContactAccount.LinkToAttributeName, ConditionOperator.UnderOrEqual, regardingId);
            feOrConditions.AddCondition(leContactAccount.EntityAlias, leContactAccount.LinkToAttributeName, ConditionOperator.UnderOrEqual, regardingId);
            feOrConditions.AddCondition(leTAAccount.EntityAlias, leTAAccount.LinkToAttributeName, ConditionOperator.UnderOrEqual, regardingId);
        }

        private static LinkEntity AddLinkToLinkedEntity(LinkEntity LinkedEntity, 
            string linkToEntityName, string linkToAttributeName, string linkFromAttributeName, 
            string EntityAlias, JoinOperator joinOperator)
        {
            CubLogger.Debug($"Adding link-entity for regarding {LinkedEntity.EntityAlias} {linkToEntityName}");
            var leTAAccount = LinkedEntity.AddLink(linkToEntityName, linkFromAttributeName, linkToAttributeName, joinOperator);
            leTAAccount.EntityAlias = EntityAlias;
            return leTAAccount;
        }

        private static LinkEntity AddLinkToQuery(QueryExpression qe, string linkToEntityName, string linkToAttributeName, string linkFromAttributeName, 
            string EntityAlias, JoinOperator joinOperator)
        {
            CubLogger.Debug($"Adding query link-entity for regarding {linkToEntityName}");
            var leTA = qe.AddLink(linkToEntityName, linkFromAttributeName, linkToAttributeName, joinOperator);
            leTA.EntityAlias = EntityAlias;
            return leTA;
        }
    }
}
