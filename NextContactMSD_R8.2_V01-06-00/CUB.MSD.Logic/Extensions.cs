/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// HTTPClient extension to offer new HTTP verbs
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Path method
        /// </summary>
        /// <param name="client">HTTPClient class referense</param>
        /// <param name="url">URL</param>
        /// <param name="iContent">Request Content</param>
        /// <returns></returns>
        public static HttpResponseMessage Patch(this HttpClient client, string url, HttpContent iContent)
        {
            Uri requestUri = new Uri(url);
            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, requestUri)
            {
                Content = iContent
            };
            return client.SendAsync(request).Result;
        }

        /// <summary>
        /// Serialize an object to Json string 
        /// </summary>
        /// <param name="source"></param>
        /// <returns>JSON string</returns>
        public static string SerializeObject(this object source)
        {
            string returnValue = string.Empty;

            try
            {
                returnValue = JsonConvert.SerializeObject(source);
            }
            catch (Exception ex)
            {
                CubLogger.Error(null, ex);
                //returning object's default ToString
                returnValue = source.ToString();
            }

            return returnValue;
        }

        /// <summary>
        /// Deserialize JSON string to a .NET type
        /// </summary>
        /// <typeparam name="T">type to convert to</typeparam>
        /// <param name="source">source JSON string</param>
        /// <returns>T</returns>
        public static T DeserializeObject<T>(this string source)
        {
            T returnValue = default(T);

            try
            {
                returnValue = JsonConvert.DeserializeObject<T>(source);
            }
            catch (Exception ex)
            {
                CubLogger.Error(null, ex);

            }

            return returnValue;
        }


    }
}
