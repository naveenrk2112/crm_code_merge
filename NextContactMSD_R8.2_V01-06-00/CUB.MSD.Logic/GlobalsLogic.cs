﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using System;
using Microsoft.Xrm.Sdk.Query;
using System.Threading.Tasks;

namespace CUB.MSD.Logic

{
    /// <summary>
    /// Globals Logic
    /// </summary>
    public class GlobalsLogic : LogicBase
    {
        const string GRID_CONFIGURATIONS_KEY = "custom_grid_configurations";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public GlobalsLogic(IOrganizationService service) : base(service)
        {
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <param name="executionContext">MSD plugin execution contect</param>
        public cub_Globals GetAllByAttribute(string attribute)
        {
                return (from g in CubOrgSvc.cub_GlobalsSet where g.cub_Name.Contains(attribute) select g).FirstOrDefault();
        }

        /// <summary>
        /// Retrieve all active global records
        /// </summary>
        /// <returns>List of globals</returns>
        public List<cub_Globals> GetAllGlobals()
        {
            // TODO: Add optional filtering
            return (from g in CubOrgSvc.cub_GlobalsSet
                    where g.statecode.Value.Equals((int)cub_GlobalsState.Active)
                    select new cub_Globals
                    {
                        cub_AttributeValue = g.cub_AttributeValue,
                        cub_Enabled = g.cub_Enabled,
                        cub_GlobalsId = g.cub_GlobalsId,
                        cub_Description = g.cub_Description,
                        cub_Name = g.cub_Name
                    }).ToList();
        }

        /// <summary>
        /// Get All Details By Global
        /// </summary>
        /// <param name="name">Global Name</param>
        /// <returns>Global Details</returns>
        public IEnumerable<cub_GlobalDetails> GetAllDetailsByGlobal(string name)
        {
            return (from gd in CubOrgSvc.cub_GlobalDetailsSet
                    join g in CubOrgSvc.cub_GlobalsSet on gd.cub_GlobalId.Id equals g.cub_GlobalsId
                    where g.cub_Name == name && gd.cub_Enabled == true
                    select new cub_GlobalDetails
                    {
                        cub_GlobalDetailsId = gd.cub_GlobalDetailsId,
                        cub_Name = gd.cub_Name,
                        cub_AttributeValue = gd.cub_AttributeValue
                    }).ToList();
        }

        /// <summary>
        /// Get Cached Details By Global
        /// </summary>
        /// <param name="key">Global Key</param>
        /// <returns>Global Details</returns>
        public object GetCachedDetailsByGlobal(string key)
        {
            key = key.Trim().ToLower();
            var detailsList = GlobalsCache.GetFromCache(key);
            if (detailsList != null)
            {
                return detailsList;
            }
                var results = (from gd in CubOrgSvc.cub_GlobalDetailsSet
                               join g in CubOrgSvc.cub_GlobalsSet on gd.cub_GlobalId.Id equals g.cub_GlobalsId
                               where g.cub_Name == key
                                  && gd.statecode.Value.Equals((int)cub_GlobalDetailsState.Active)
                                  && gd.cub_Enabled.GetValueOrDefault()
                               select new cub_GlobalDetails
                               {
                                   cub_Name = gd.cub_Name,
                                   cub_AttributeValue = gd.cub_AttributeValue
                               })
                 .ToList()
                 .Select<cub_GlobalDetails, object>(g =>
                 {
                     double doubleVal;
                     if (double.TryParse(g.cub_AttributeValue, out doubleVal))
                     {
                         return new KeyValuePair<string, double>(g.cub_Name, doubleVal);
                     }
                     else
                     {
                         return new KeyValuePair<string, string>(g.cub_Name, g.cub_AttributeValue);
                     }
                 });
                if (results.Any())
                {
                    GlobalsCache.AddToCache(key, results);
                }
                return results;
        }

        /// <summary>
        /// Retrieve all active global detail records
        /// </summary>
        /// <returns>List of global details</returns>
        public List<cub_GlobalDetails> GetAllGlobalDetails()
        {
                return (from g in CubOrgSvc.cub_GlobalDetailsSet
                        where g.statecode.Value.Equals((int)cub_GlobalDetailsState.Active)
                            && g.cub_Enabled.GetValueOrDefault()
                        select g).ToList();
        }

        /// <summary>
        /// Retrieve all active option set translation records
        /// </summary>
        /// <returns>List of option set translations</returns>
        public List<cub_OptionSetTranslation> GetAllOptionSetTranslations()
        {
            return (from o in CubOrgSvc.cub_OptionSetTranslationSet
                    where o.statecode.Value.Equals((int)cub_OptionSetTranslationState.Active)
                    select o).ToList();
        }

        /// <summary>
        /// Retrieve all active option set translation detail records
        /// </summary>
        /// <returns>List of option set translation details</returns>
        public List<cub_OptionSetTranslationDetail> GetAllOptionSetTranslationDetails()
        {
            return (from o in CubOrgSvc.cub_OptionSetTranslationDetailSet
                    where o.statecode.Value.Equals((int)cub_OptionSetTranslationDetailState.Active)
                    select o).ToList();
        }

        #region Copy functions

        /// <summary>
        /// Instantiate a new global record from an existing one
        /// </summary>
        /// <param name="global">The original global</param>
        /// <returns>The global copy</returns>
        public cub_Globals CopyGlobal(cub_Globals global)
        {
            return new cub_Globals
            {
                cub_Name = global.cub_Name,
                cub_Type = global.cub_Type,
                cub_Description = global.cub_Description,
                cub_Enabled = global.cub_Enabled
            //    cub_TransactionOrigin = global.cub_TransactionOrigin
            };
        }

        /// <summary>
        /// Instantiate a new global detail record from an existing one
        /// </summary>
        /// <param name="detail">The original global detail</param>
        /// <returns>The global detail copy</returns>
        public cub_GlobalDetails CopyGlobalDetail(cub_GlobalDetails detail)
        {
            // TODO: be more specific when getting parent?
            return new cub_GlobalDetails
            {
                cub_Name = detail.cub_Name,
                cub_AttributeValue = detail.cub_AttributeValue,
                cub_Description = detail.cub_Description,
                cub_Enabled = detail.cub_Enabled,
                cub_TransactionOrigin = detail.cub_TransactionOrigin,
                cub_GlobalId = (from g in CubOrgSvc.cub_GlobalsSet
                                where g.statecode.Value.Equals((int)cub_GlobalDetailsState.Active)
                                    && g.cub_Name.Equals(detail.cub_GlobalId.Name)
                                select g).FirstOrDefault().ToEntityReference()
            };
        }

        /// <summary>
        /// Instantiate a new option set translation record from an existing one
        /// </summary>
        /// <param name="ost">The original option set translation</param>
        /// <returns>The option set translation copy</returns>
        public cub_OptionSetTranslation CopyOptionSetTranslation(cub_OptionSetTranslation ost)
        {
            return new cub_OptionSetTranslation
            {
                cub_EntityName = ost.cub_EntityName,
                cub_OptionSetName = ost.cub_OptionSetName
            };
        }

        /// <summary>
        /// Instantiate a new option set translation detail record from an existing one
        /// </summary>
        /// <param name="ostd">The original option set translation detail</param>
        /// <returns>The option set translation detail copy</returns>
        public cub_OptionSetTranslationDetail CopyOptionSetTranslationDetail(cub_OptionSetTranslationDetail ostd)
        {
            return new cub_OptionSetTranslationDetail
            {
                cub_OptionSetValue = ostd.cub_OptionSetValue,
                cub_Translation = ostd.cub_Translation,
                cub_OptionSetTranslationId = (from o in CubOrgSvc.cub_OptionSetTranslationSet
                                              where o.statecode.Value.Equals((int)cub_OptionSetTranslationState.Active)
                                                && o.cub_OptionSetName.Equals(ostd.cub_OptionSetTranslationId.Name)
                                              select o).FirstOrDefault().ToEntityReference()
            };
        }

        #endregion

        #region Record Comparisons

        /// <summary>
        /// Compare two global records from different organizations to determine
        /// if target needs to be updated. Assumes they already match on name.
        /// </summary>
        /// <param name="source">Source record</param>
        /// <param name="target">Target record</param>
        /// <returns>True if one has been changed since last sync, false otherwise.</returns>
        /// <exception cref="ArgumentException">Thrown if name does not match.</exception>
        public bool NeedsUpdate(cub_Globals source, cub_Globals target)
        {
            if (!source.cub_Name.Equals(target.cub_Name))
            {
                throw new ArgumentException($"Globals {source.cub_Name} and {target.cub_Name} should not be compared");
            }
            if (!source.cub_Enabled.Equals(target.cub_Enabled))
            {
                return true;
            }
            if (source.cub_Description != null
                && !source.cub_Description.Equals(target.cub_Description))
            {
                return true;
            }
            if (source.cub_Type != null
                    && !source.cub_Type.Equals(target.cub_Type))
            {
                return true;
            }
            if (source.cub_TransactionOrigin != null
                    && !source.cub_TransactionOrigin.Equals(target.cub_TransactionOrigin))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Compare two global detail records from different organizations to 
        /// determine if target needs to be updated. Assumes they already match
        /// on name and parent global.
        /// </summary>
        /// <param name="source">Source record</param>
        /// <param name="target">Target record</param>
        /// <returns>True if one has been changed since last sync, false otherwise.</returns>
        /// <exception cref="ArgumentException">Thrown if name or parent global do not match.</exception>
        public bool NeedsUpdate(cub_GlobalDetails source, cub_GlobalDetails target)
        {
            if (!source.cub_Name.Equals(target.cub_Name)
                || !source.cub_GlobalId.Name.Equals(target.cub_GlobalId.Name))
            {
                throw new ArgumentException($"Global details {source.cub_Name} and {target.cub_Name} should not be compared");
            }
            if (!source.cub_Enabled.Equals(target.cub_Enabled))
            {
                return true;
            }
            if (source.cub_AttributeValue != null
                && !source.cub_AttributeValue.Equals(target.cub_AttributeValue))
            {
                return true;
            }
            if (source.cub_Description != null
                && !source.cub_Description.Equals(target.cub_Description))
            {
                return true;
            }
            if (source.cub_TransactionOrigin != null
                && !source.cub_TransactionOrigin.Equals(target.cub_TransactionOrigin))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Compare two option set translation records from different organizations to determine
        /// if target needs to be updated. Assumes they already match on name.
        /// </summary>
        /// <param name="source">Source record</param>
        /// <param name="target">Target record</param>
        /// <returns>True if one has been changed since last sync, false otherwise.</returns>
        /// <exception cref="ArgumentException">Thrown if name does not match.</exception>
        public bool NeedsUpdate(cub_OptionSetTranslation source, cub_OptionSetTranslation target)
        {
            if (!source.cub_OptionSetName.Equals(target.cub_OptionSetName))
            {
                throw new ArgumentException($"Option set translations {source.cub_OptionSetName} and {target.cub_OptionSetName} should not be compared");
            }
            if (source.cub_EntityName != null
                && !source.cub_EntityName.Equals(target.cub_EntityName))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Compare two option set translation detail records from different organizations to determine
        /// if target needs to be updated. Assumes they already match on name.
        /// </summary>
        /// <param name="source">Source record</param>
        /// <param name="target">Target record</param>
        /// <returns>True if one has been changed since last sync, false otherwise.</returns>
        /// <exception cref="ArgumentException">Thrown if name does not match.</exception>
        public bool NeedsUpdate(cub_OptionSetTranslationDetail source, cub_OptionSetTranslationDetail target)
        {
            if (!source.cub_OptionSetValue.Equals(target.cub_OptionSetValue)
                || !source.cub_OptionSetTranslationId.Name.Equals(target.cub_OptionSetTranslationId.Name))
            {
                throw new ArgumentException($"Option set translation details {source.cub_OptionSetValue} and {target.cub_OptionSetValue} should not be compared");
            }
            if (source.cub_Translation != null
                && !source.cub_Translation.Equals(target.cub_Translation))
            {
                return true;
            }
            return false;
        }

        #endregion

        /// <summary>
        /// Get Attribute Value
        /// </summary>
        /// <param name="name">Global Key Name</param>
        /// <param name="attribute">Global Attribute Name</param>
        /// <returns></returns>
        public string GetAttributeValue(string name, string attribute)
        {

            return (from g in CubOrgSvc.cub_GlobalsSet
                    join d in CubOrgSvc.cub_GlobalDetailsSet
                    on g.cub_GlobalsId.Value equals d.cub_GlobalId.Id
                    where g.cub_Name.Equals(name) &&
                          d.cub_Name.Equals(attribute) &&
                          g.cub_Enabled.Value.Equals(true) &&
                          d.cub_Enabled.Value.Equals(true)
                    select d.cub_AttributeValue).FirstOrDefault();
        }

        /// <summary>
        /// Add Error to List
        /// </summary>
        /// <param name="list">List</param>
        /// <param name="key">Key</param>
        /// <param name="message">Message</param>
        /// <returns>Error list</returns>
        public List<WSError> AddErrorTolist(List<WSError> list, string key, string message)
        {
            WSError vp_error = new WSError();
            vp_error.key = key;
            vp_error.message = message;
            list.Add(vp_error);
            return list;
        }

        /// <summary>
        /// Find look up table record by entity and field to be queried
        /// </summary>
        /// <param name="entity">Entity to be queried</param>
        /// <param name="fieldName">Entity field to be queried</param>
        /// <param name="fieldValue">Field Value</param>
        /// <returns>Guild of lookp>/returns>
        public Guid? GetLookupRecord(string entityname, string fieldName, object fieldValue)
        {
            Guid? lookupId;
            try
            {
                QueryExpression queryExp = new QueryExpression();
                queryExp.EntityName = entityname;
                queryExp.Criteria.AddCondition(fieldName, ConditionOperator.Equal, fieldValue);

                var returnRes = _organizationService.RetrieveMultiple(queryExp);

                if (returnRes != null && returnRes.Entities.Count > 0)
                    lookupId = returnRes.Entities[0].Id;
                else
                    lookupId = null;
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                lookupId = null;
            }
            return lookupId;

        }

        /// <summary>
        /// Retrieves all customizable grid configuration records from MSD and 
        /// caches the result before returning it.
        /// </summary>
        /// <returns>A list of customizable grid configuration data</returns>
        public object GetAllCustomGridConfigurations()
        {
            var cachedGridConfigs = GlobalsCache.GetFromCache(GRID_CONFIGURATIONS_KEY);
            if (cachedGridConfigs != null)
            {
                return cachedGridConfigs;
            }

            var gridConfigs = (from cgc in CubOrgSvc.cub_CustomizableGridConfigurationSet
                               where cgc.statecode == cub_CustomizableGridConfigurationState.Active
                               select new
                               {
                                   id = cgc.cub_Name,
                                   primaryKey = cgc.cub_PrimaryKey,
                                   displayPaginator = cgc.cub_DisplayPaginator,
                                   rowsToDisplay = cgc.cub_RowsToDisplay,
                                   maxRecords = cgc.cub_MaxRecordsToReturn,
                                   defaultSort = cgc.cub_DefaultSort,
                                   columns = cgc.cub_ColumnInformation.DeserializeObject<object[]>(),
                                   actions = (from action in CubOrgSvc.cub_CustomizableGridActionSet
                                              where action.statecode == cub_CustomizableGridActionState.Active
                                              where action.cub_CustomizableGrid.Id == cgc.cub_CustomizableGridConfigurationId
                                              orderby action.cub_Order
                                              select new
                                              {
                                                  label = action.cub_Label,
                                                  action = action.cub_Action,
                                                  condition = action.cub_Condition
                                              }).ToList()
                               }).ToList();

            GlobalsCache.AddToCache(GRID_CONFIGURATIONS_KEY, gridConfigs);
            return gridConfigs;
        }
    }
}
