﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using CUB.MSD.Model.NIS;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// Rest API Service
    /// </summary>
    public abstract class RestApiBase : LogicBase, IDisposable
    {
        RestResponse returnResponse;
        private RequestHeader Header { set; get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public RestApiBase(IOrganizationService service) : base(service)
        {
            PrepareHeader(service);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <param name="executionContext">MSD plugin execution contect</param>
        public RestApiBase(IOrganizationService service, IPluginExecutionContext executionContext) : base(service, executionContext)
        {
            PrepareHeader(service);
        }

        /// <summary>
        /// Prepare Header
        /// </summary>
        /// <param name="service">MSD Organization service</param>
        private void PrepareHeader(IOrganizationService service)
        {
            UserLogic userLogic = new UserLogic(this._organizationService);
            UserInfo userInfo = null;
            string sessionID = null;
            string fullUserName = System.Web.HttpContext.Current?.User?.Identity?.Name;
            if (!string.IsNullOrWhiteSpace(fullUserName))
            {
                userInfo = userLogic.GetCrmUserIdFirstLastName(fullUserName);
                SessionLogic sessionLogic = new SessionLogic(this._organizationService);
                var sessionInfo = sessionLogic.RetrieveSession(new Guid(userInfo.UserId));
                if (sessionInfo != null &&
                    sessionInfo.sessionId.HasValue)
                {
                    sessionID = sessionInfo.sessionId.Value.ToString();
                }
            }
            string trackingType = GlobalsCache.NISGet_x_cub_hdr_TrackingType(this._organizationService);
            string location = GlobalsCache.NISGet_x_cub_audit_Location(this._organizationService);
            string appId = GlobalsCache.NISGet_x_cub_hdr_AppId(this._organizationService);
            string channel = GlobalsCache.NISGet_x_cub_audit_Channel(this._organizationService);
            Header = new RequestHeader(GlobalsCache.NISApiUsername(service),
                                       GlobalsCache.NISApiPassword(service),
                                       GlobalsCache.NISApiDevice(service),
                                       null,
                                       appId,
                                       userInfo == null? null : userInfo.FirstName,
                                       userInfo == null? null : userInfo.LastName,
                                       sessionID,
                                       trackingType,
                                       location,
                                       channel);
        }

        /// <summary>
        /// This method POST data using the HttpClient Class introduced in .NET 4.5
        /// </summary>
        /// <param name="url"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public RestResponse HttpClientPost(string url, HttpContent content, List<KeyValuePair<string, string>> headerParams = null, string xCubAuditID = null)
        {
            returnResponse = new RestResponse();
            HttpResponseMessage respMsg = null;
            string nisUrl = String.Empty;
            string tollingUrl = String.Empty;

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    nisUrl = GlobalsCache.NISApiBaseURL(_organizationService);
                    tollingUrl = GlobalsCache.TollingApiBaseURL(_organizationService);

                    SetHeaders(headerParams, xCubAuditID, client);

                    DateTime requestTime = DateTime.UtcNow;

                    CubLogger.Info("POST Request",
                                   string.Format("URL: {0}\nHEADERS: {1}\nCONTENT: {2}",
                                                 url,
                                                 JsonConvert.SerializeObject(client.DefaultRequestHeaders),
                                                 JsonConvert.SerializeObject(content)),
                                   requestTime, null, null);

                    respMsg = client.PostAsync(url, content).Result;        //not async

                    CubLogger.Info("POST Request",
                                   string.Format("URL: {0}\nHEADERS: {1}\nCONTENT: {2}", 
                                                 url,
                                                 JsonConvert.SerializeObject(client.DefaultRequestHeaders),
                                                 JsonConvert.SerializeObject(content)),
                                   requestTime,
                                   JsonConvert.SerializeObject(respMsg),
                                   DateTime.UtcNow);

                    respMsg.EnsureSuccessStatusCode();

                    if (respMsg.Headers.Count() <= 0) //if the service is available, x-cub-hdr always exists.
                        throw new ApplicationException(RestConstants.SERVICE_UNAVAILABLE_MSG);

                    //Set Api response header
                    returnResponse.hdr = JsonConvert.DeserializeObject<WSWebResponseHeader>(respMsg.Headers.GetValues(RestConstants.CUB_HEADER).First());

                    //Set Api response content 
                    if (respMsg.Content != null) returnResponse.responseBody = respMsg.Content.ReadAsStringAsync().Result;      //not async

                    if (returnResponse.hdr.result != RestConstants.SUCCESSFUL)
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                }
                catch (Exception ex)
                {
                    HandleException(returnResponse, respMsg, ex);
                    if (url.Contains(nisUrl))
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                    else if (url.Contains(tollingUrl))
                    {
                        throw new TollingException(returnResponse.hdr);
                    }
                    else
                    {
                        throw new GatewayException(returnResponse.hdr, ex);
                    }
                }
            }

            return returnResponse;
        }

        private void SetHeaders(List<KeyValuePair<string, string>> headerParams, string xCubAuditID, HttpClient client)
        {
            var headers = Header.GetHeaders(xCubAuditID);
            foreach (string item in headers)
            {
                client.DefaultRequestHeaders.Add(item, headers.GetValues(item));
            }
            // Adding Additional headers
            if (headerParams != null)
            {
                foreach (KeyValuePair<string, string> item in headerParams)
                {
                    client.DefaultRequestHeaders.Add(item.Key, item.Value);
                }
            }
        }

        /// <summary>
        /// This method POST data using the HttpClient Class introduced in .NET 4.5
        /// </summary>
        /// <param name="url"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public async Task<RestResponse> HttpClientPostAsync(string url, HttpContent content, List<KeyValuePair<string, string>> headerParams = null, string xCubAuditID = null)
        {
            returnResponse = new RestResponse();
            HttpResponseMessage respMsg = null;
            string nisUrl = String.Empty;
            string tollingUrl = String.Empty;

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    nisUrl = GlobalsCache.NISApiBaseURL(_organizationService);
                    tollingUrl = GlobalsCache.TollingApiBaseURL(_organizationService);

                    if (Header == null) Header = 
                            new RequestHeader(GlobalsCache.NISApiUsername(_organizationService),
                                              GlobalsCache.NISApiPassword(_organizationService),
                                              GlobalsCache.NISApiDevice(_organizationService));

                    SetHeaders(headerParams, xCubAuditID, client);

                    DateTime requestTime = DateTime.UtcNow;

                    CubLogger.Info("POST Request",
                                   string.Format("URL: {0}\nHEADERS: {1}\nCONTENT: {2}",
                                                 url,
                                                 JsonConvert.SerializeObject(client.DefaultRequestHeaders),
                                                 JsonConvert.SerializeObject(content)),
                                   requestTime, null, null);

                    respMsg = await client.PostAsync(url, content);

                    CubLogger.Info("POST Request",
                                   string.Format("URL: {0}\nHEADERS: {1}\nCONTENT: {2}",
                                                 url,
                                                 JsonConvert.SerializeObject(client.DefaultRequestHeaders),
                                                 JsonConvert.SerializeObject(content)),
                                   requestTime,
                                   JsonConvert.SerializeObject(respMsg),
                                   DateTime.UtcNow);

                    respMsg.EnsureSuccessStatusCode();

                    if (respMsg.Headers.Count() <= 0) //if the service is available, x-cub-hdr always exists.
                        throw new ApplicationException(RestConstants.SERVICE_UNAVAILABLE_MSG);

                    //Set Api response header
                    returnResponse.hdr = JsonConvert.DeserializeObject<WSWebResponseHeader>(respMsg.Headers.GetValues(RestConstants.CUB_HEADER).First());

                    //Set Api response content 
                    if (respMsg.Content != null) returnResponse.responseBody = await respMsg.Content.ReadAsStringAsync();

                    if (returnResponse.hdr.result != RestConstants.SUCCESSFUL)
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                }
                catch (Exception ex)
                {
                    HandleException(returnResponse, respMsg, ex);
                    if (url.Contains(nisUrl))
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                    else if (url.Contains(tollingUrl))
                    {
                        throw new TollingException(returnResponse.hdr);
                    }
                    else
                    {
                        throw new GatewayException(returnResponse.hdr, ex);
                    }
                }
            }

            return returnResponse;
        }


        /// <summary>
        /// Http Client Get Request
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="headerParams">Header parameters</param>
        /// <returns>Response</returns>
        public RestResponse HttpClientGet(string url, List<KeyValuePair<string, string>> headerParams = null, string xCubAuditID = null)
        {
            returnResponse = new RestResponse();
            HttpResponseMessage respMsg = null;
            string nisUrl = String.Empty;
            string tollingUrl = String.Empty;

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    nisUrl = GlobalsCache.NISApiBaseURL(_organizationService);
                    tollingUrl = GlobalsCache.TollingApiBaseURL(_organizationService);

                    SetHeaders(headerParams, xCubAuditID, client);

                    DateTime requestTime = DateTime.UtcNow;

                    CubLogger.Info("GET Request",
                                   string.Format("URL: {0}\nHEADERS: {1}", url, JsonConvert.SerializeObject(client.DefaultRequestHeaders)),
                                   requestTime, null, null);

                    respMsg = client.GetAsync(url).Result;        //not async

                    CubLogger.Info("GET Request",
                                   string.Format("URL: {0}\nHEADERS: {1}", url, JsonConvert.SerializeObject(client.DefaultRequestHeaders)),
                                   requestTime,
                                   JsonConvert.SerializeObject(respMsg),
                                   DateTime.UtcNow);

                    respMsg.EnsureSuccessStatusCode();

                    // TODO: unreachable exception
                    if (respMsg.Headers.Count() <= 0) //if the service is available, x-cub-hdr always exists.
                        throw new ApplicationException(RestConstants.SERVICE_UNAVAILABLE_MSG);

                    //Set Api response header
                    // temp comment out for tolling
                    returnResponse.hdr = JsonConvert.DeserializeObject<WSWebResponseHeader>(respMsg.Headers.GetValues(RestConstants.CUB_HEADER).First());

                    //Set Api response content 
                    if (respMsg.Content != null) returnResponse.responseBody = respMsg.Content.ReadAsStringAsync().Result;      //not async

                    if (returnResponse.hdr.result != RestConstants.SUCCESSFUL)
                    {
                        throw new Exception();
                    }
                }
                catch (Exception ex)
                {
                    HandleException(returnResponse, respMsg, ex);
                    if (url.Contains(nisUrl))
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                    else if (url.Contains(tollingUrl))
                    {
                        throw new TollingException(returnResponse.hdr);
                    }
                    else
                    {
                        throw new GatewayException(returnResponse.hdr, ex);
                    }
                }
            }

            return returnResponse;
        }

        /// <summary>
        /// Http Client Delete
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="headerParams">Header parameters</param>
        /// <returns>Delet Response</returns>
        public RestResponse HttpClientDelete(string url, List<KeyValuePair<string, string>> headerParams = null, string xCubAuditID = null)
        {
            returnResponse = new RestResponse();
            HttpResponseMessage respMsg = null;
            string nisUrl = String.Empty;
            string tollingUrl = String.Empty;

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    nisUrl = GlobalsCache.NISApiBaseURL(_organizationService);
                    tollingUrl = GlobalsCache.TollingApiBaseURL(_organizationService);

                    SetHeaders(headerParams, xCubAuditID, client);

                    DateTime requestTime = DateTime.UtcNow;

                    CubLogger.Info("DELETE Request",
                                   string.Format("URL: {0}\nHEADERS: {1}\n",
                                                 url,
                                                 JsonConvert.SerializeObject(client.DefaultRequestHeaders)),
                                   requestTime, null, null);

                    respMsg = client.DeleteAsync(url).Result;        //not async

                    CubLogger.Info("DELETE Request",
                                   string.Format("URL: {0}\nHEADERS: {1}\nCONTENT: {2}",
                                                 url,
                                                 JsonConvert.SerializeObject(client.DefaultRequestHeaders),
                                                 JsonConvert.SerializeObject(respMsg.Content)),
                                   requestTime,
                                   JsonConvert.SerializeObject(respMsg),
                                   DateTime.UtcNow);

                    respMsg.EnsureSuccessStatusCode();

                    if (respMsg.Headers.Count() <= 0) //if the service is available, x-cub-hdr always exists.
                        throw new ApplicationException(RestConstants.SERVICE_UNAVAILABLE_MSG);

                    //Set Api response header
                    returnResponse.hdr = JsonConvert.DeserializeObject<WSWebResponseHeader>(respMsg.Headers.GetValues(RestConstants.CUB_HEADER).First());

                    //Set Api response content 
                    if (respMsg.Content != null) returnResponse.responseBody = respMsg.Content.ReadAsStringAsync().Result;      //not async

                }
                catch (Exception ex)
                {
                    HandleException(returnResponse, respMsg, ex);
                    if (url.Contains(nisUrl))
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                    else if (url.Contains(tollingUrl))
                    {
                        throw new TollingException(returnResponse.hdr);
                    }
                    else
                    {
                        throw new GatewayException(returnResponse.hdr, ex);
                    }
                }
            }

            return returnResponse;
        }

        /// <summary>
        /// Http Client Patch
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="content">Content</param>
        /// <param name="headerParams">Header parameters</param>
        /// <returns></returns>
        public RestResponse HttpClientPatch(string url, HttpContent content, List<KeyValuePair<string, string>> headerParams = null, string xCubAuditID = null)
        {
            returnResponse = new RestResponse();
            HttpResponseMessage respMsg = null;
            string nisUrl = String.Empty;
            string tollingUrl = String.Empty;

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    nisUrl = GlobalsCache.NISApiBaseURL(_organizationService);
                    tollingUrl = GlobalsCache.TollingApiBaseURL(_organizationService);

                    SetHeaders(headerParams, xCubAuditID, client);

                    DateTime requestTime = DateTime.UtcNow;

                    CubLogger.Info("PATCH Request",
                                    string.Format("URL: {0}\nHEADERS: {1}\nCONTENT: {2}",
                                                  url,
                                                  JsonConvert.SerializeObject(client.DefaultRequestHeaders),
                                                  JsonConvert.SerializeObject(content)),
                                    requestTime, null, null);

                    respMsg = client.Patch(url, content);

                    CubLogger.Info("PATCH Request",
                                   string.Format("URL: {0}\nHEADERS: {1}\nCONTENT: {2}",
                                                 url,
                                                 JsonConvert.SerializeObject(client.DefaultRequestHeaders),
                                                 JsonConvert.SerializeObject(content)),
                                   requestTime,
                                   JsonConvert.SerializeObject(respMsg),
                                   DateTime.UtcNow);

                    respMsg.EnsureSuccessStatusCode();
                    if (respMsg.Headers.Count() <= 0) //if the service is available, x-cub-hdr always exists.
                        throw new ApplicationException(RestConstants.SERVICE_UNAVAILABLE_MSG);
                    returnResponse.hdr = JsonConvert.DeserializeObject<WSWebResponseHeader>(respMsg.Headers.GetValues(RestConstants.CUB_HEADER).First());
                    if (respMsg.Content != null) returnResponse.responseBody = respMsg.Content.ReadAsStringAsync().Result;      //not async

                    if (returnResponse.hdr.result != RestConstants.SUCCESSFUL)
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                }
                catch (Exception ex)
                {
                    HandleException(returnResponse, respMsg, ex);
                    if (url.Contains(nisUrl))
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                    else if (url.Contains(tollingUrl))
                    {
                        throw new TollingException(returnResponse.hdr);
                    }
                    else
                    {
                        throw new GatewayException(returnResponse.hdr, ex);
                    }
                }
            }
            return returnResponse;
        }

        /// <summary>
        /// This method PUT data using the HttpClient Class introduced in .NET 4.5
        /// </summary>
        /// <param name="url"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public RestResponse HttpClientPut(string url, HttpContent content, List<KeyValuePair<string, string>> headerParams = null, string xCubAuditID = null)
        {
            returnResponse = new RestResponse();
            HttpResponseMessage respMsg = null;
            string nisUrl = String.Empty;
            string tollingUrl = String.Empty;

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    nisUrl = GlobalsCache.NISApiBaseURL(_organizationService);
                    tollingUrl = GlobalsCache.TollingApiBaseURL(_organizationService);

                    SetHeaders(headerParams, xCubAuditID, client);

                    DateTime requestTime = DateTime.UtcNow;

                    CubLogger.Info("PUT Request",
                                   string.Format("URL: {0}\nHEADERS: {1}\nCONTENT: {2}",
                                                 url,
                                                 JsonConvert.SerializeObject(client.DefaultRequestHeaders),
                                                 JsonConvert.SerializeObject(content)),
                                   requestTime, null, null);

                    respMsg = client.PutAsync(url, content).Result;        //not async

                    CubLogger.Info("PUT Request",
                                   string.Format("URL: {0}\nHEADERS: {1}\nCONTENT: {2}",
                                                 url,
                                                 JsonConvert.SerializeObject(client.DefaultRequestHeaders),
                                                 JsonConvert.SerializeObject(content)),
                                   requestTime,
                                   JsonConvert.SerializeObject(respMsg),
                                   DateTime.UtcNow);

                    respMsg.EnsureSuccessStatusCode();

                    if (respMsg.Headers.Count() <= 0) //if the service is available, x-cub-hdr always exists.
                        throw new ApplicationException(RestConstants.SERVICE_UNAVAILABLE_MSG);

                    //Set Api response header
                    returnResponse.hdr = JsonConvert.DeserializeObject<WSWebResponseHeader>(respMsg.Headers.GetValues(RestConstants.CUB_HEADER).First());

                    //Set Api response content 
                    if (respMsg.Content != null) returnResponse.responseBody = respMsg.Content.ReadAsStringAsync().Result;      //not async

                    if (returnResponse.hdr.result != RestConstants.SUCCESSFUL)
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                }
                catch (Exception ex)
                {
                    HandleException(returnResponse, respMsg, ex);
                    if (url.Contains(nisUrl))
                    {
                        throw new NISException(returnResponse.hdr);
                    }
                    else if (url.Contains(tollingUrl))
                    {
                        throw new TollingException(returnResponse.hdr);
                    }
                    else
                    {
                        throw new GatewayException(returnResponse.hdr, ex);
                    }
                }
            }

            return returnResponse;
        }


        /// <summary>
        /// Called from the catch block to handle exception. If the API response message is null, use the EX value otherwise send the header
        /// information as that will have the error message
        /// </summary>
        /// <param name="returnResponse"></param>
        /// <param name="respMsg"></param>
        /// <param name="ex"></param>
        private void HandleException(RestResponse returnResponse, HttpResponseMessage respMsg, Exception ex)
        {
            if (respMsg != null && respMsg.Headers != null && respMsg.Headers.Contains(RestConstants.CUB_HEADER))
            {
                returnResponse.hdr = JsonConvert.DeserializeObject<WSWebResponseHeader>(respMsg.Headers.GetValues(RestConstants.CUB_HEADER).FirstOrDefault());
            }
            else
            {
                returnResponse.hdr.result = RestConstants.FAILED;
                returnResponse.hdr.result = ex.Message;
            }
            CubLogger.Error(string.Empty, ex);
        }

        #region unsed code

        /// <summary>
        /// This method POST data using the HttpClient Class introduced in .NET 1.1
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        //public RestResponse WebRequestPost(string url)
        //{
        //    returnResponse = new RestResponse();

        //    var uri = new Uri(url);

        //    var request = WebRequest.Create(uri);
        //    request.Method = WebRequestMethods.Http.Post;
        //    //request.ContentType = "application/json";
        //    request.ContentType = RestConstants.MEDIA_TYPE_JSON_UTF8;

        //    request.Headers = Header.GetHeaders();

        //    try
        //    {
        //        using (var response = request.GetResponse())
        //        {
        //            using (var reader = new StreamReader(response.GetResponseStream()))
        //            {
        //                returnResponse.responseBody = reader.ReadToEnd();
        //            }
        //        }
        //    }
        //    catch (WebException wex)
        //    {
        //        returnResponse.hdr = HandleWebException(wex);
        //    }

        //    return returnResponse;

        //}

        /// <summary>
        /// This method POST data using the HttpClient Class introduced in .NET 1.1
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        //public RestResponse WebClientPost(string url)
        //{
        //    returnResponse = new RestResponse();

        //    using (WebClient client = new WebClient())
        //    {
        //        client.Headers = Header.GetHeaders();
        //        //client.Headers.Add("Content-Type", "application/json");

        //        try
        //        {
        //            returnResponse.responseBody = client.UploadString(url, string.Empty);
        //        }
        //        catch (WebException wex)
        //        {
        //            returnResponse.hdr = HandleWebException(wex);
        //        }

        //    }

        //    return returnResponse;

        //}



        //private WSWebResponseHeader HandleWebException(WebException wex)
        //{
        //    WSWebResponseHeader errorResponseHdr = new WSWebResponseHeader();

        //    if (wex.Response != null)
        //    {
        //        using (var errorResponse = (HttpWebResponse)wex.Response)
        //        {
        //            if (errorResponse.Headers.Count <= 0) //if the service is available, x-cub-hdr always exists.
        //                throw new ApplicationException(RestConstants.SERVICE_UNAVAILABLE_MSG);

        //            errorResponseHdr = JsonConvert.DeserializeObject<WSWebResponseHeader>(errorResponse.Headers.Get(RestConstants.CUB_HEADER));

        //        }
        //    }
        //    return errorResponseHdr;
        //}

        #endregion unsed code

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    returnResponse = null;
                    Header = null;
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~RestApiBase() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }

    /// <summary>
    /// Request Header
    /// </summary>
    public class RequestHeader
    {
        private string username { get; set; }
        private string password { get; set; }
        public string uid { get; set; }
        public string device { get; set; }
        private string appId { get; set; }
        public string authorization { get { return generateAuthorizationHeader(this.username, this.password); } }
        private string firstName { get; set; }
        private string lastName { get;  set; }
        private string fullUserName { get; set; }
        private string msdSessionId { get; set; }
        private string trackingType { get; set; }
        private string location { get; set; }
        private string channel { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="username">User Name</param>
        /// <param name="password">Password</param>
        /// <param name="device">Device</param>
        /// <param name="uid">UID</param>
        /// <param name="fistName">First Name</param>
        /// <param name="uid"Last Name</param>
        public RequestHeader(string username, string password, string device, string uid = "",
                             string appId = "", string firstName = "", string lastName = "", 
                             string sessionID = "", string trackingType = "", string location = "", string channel = "")
        {
            if (string.IsNullOrEmpty(uid)) uid = Guid.NewGuid().ToString();
            this.username = username;
            this.password = password;
            this.uid = uid;
            this.device = device;
            this.appId = appId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.msdSessionId = sessionID;
            this.trackingType = trackingType;
            this.location = location;
            this.channel = channel;
            this.fullUserName = System.Web.HttpContext.Current?.User?.Identity?.Name;
        }

        /// <summary>
        /// Get Headers
        /// </summary>
        /// <returns>Headers collection</returns>
        public WebHeaderCollection GetHeaders(string xCubAuditID = null)
        {
            WebHeaderCollection headers = new WebHeaderCollection();
            //headers.Add("device", this.device);
            headers.Add(RestConstants.AUTHORIZATION_HEADER, this.authorization);
            headers.Add(RestConstants.UID_HEADER, this.uid);
            headers.Add(RestConstants.DEVICE_HEADER, this.device);
            /*
             * Adding the new headers
             */
            Add_X_Cub_Hdr(headers, xCubAuditID);
            Add_X_Cub_Audit(headers);
            Add_Cub_txnid(headers);
            return headers;
        }

        /// <summary>
        /// x-cub-hdr: (Required)  HTTP header with information that uniquely identifies the request and the originating device.
        /// </summary>
        /// <param name="headers"></param>
        private void Add_Cub_txnid(WebHeaderCollection headers)
        {
            headers.Add("x-cub-txnid", this.msdSessionId);
        }

        /// <summary>
        /// x-cub-audit: (Required) Used to track location and user who initiated the request.
        /// </summary>
        /// <param name="headers"></param>
        private void Add_X_Cub_Audit(WebHeaderCollection headers)
        {
            X_Cub_Audit audit = new X_Cub_Audit();
            audit.sourceIp = getLocalIP();
            audit.userId = this.fullUserName;
            audit.firstName = this.firstName;
            audit.lastName = this.lastName;
            audit.channel = this.channel;
            audit.location = this.location;
            headers.Add("x-cub-audit", JsonConvert.SerializeObject(audit));
        }

        private string getLocalIP()
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress addr in localIPs)
            {
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    return addr.ToString();
                }
            }
            return null;
        }

        /// <summary>
        /// x-cub-txnid: (Optional) This field is intended to be used to correlate all subsequent API requests with the same transaction Id.Although not required, this is helpful to troubleshoot and track all API requests that result from this initial request.
        /// This value needs to be globally unique so a UUID is recommended for this field.
        /// </summary>
        /// <param name="headers"></param>
        private void Add_X_Cub_Hdr(WebHeaderCollection headers, string xCubAuditID = null)
        {
            X_Cub_Hdr hdr = new X_Cub_Hdr();
            hdr.uid = (xCubAuditID != null) ? xCubAuditID : Guid.NewGuid().ToString();
            hdr.appId = this.appId;
            hdr.device = this.device;
            hdr.tracking = new TrackingInfo[1];
            hdr.tracking[0] = new TrackingInfo();
            hdr.tracking[0].trackingId = this.msdSessionId;
            hdr.tracking[0].trackingType = this.trackingType;
            headers.Add("x-cub-hdr", JsonConvert.SerializeObject(hdr));
        }


        /// <summary>
        /// Generate Authorization Header
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        private static string generateAuthorizationHeader(string user, string password)
        {
            user = !string.IsNullOrEmpty(user) ? user : string.Empty;
            password = !string.IsNullOrEmpty(password) ? password : string.Empty;

            string credentials = Base64Encode($"{user}:{password}");
            StringBuilder s = new StringBuilder(128).Append("Basic").Append(" ").Append(credentials);
            return s.ToString();
        }

        /// <summary>
        /// Base 64 Encode
        /// </summary>
        /// <param name="plainText">Plain text</param>
        /// <returns>Base 64 encoded</returns>
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

    }

    /// <summary>
    /// HTTP header with information that uniquely identifies the request and the originating device.
    /// </summary>
    public class X_Cub_Hdr
    {
        /// <summary>
        /// (Required)  Identifies the request. The client assigns this value, and it must be a unique value for each request.
        /// The server will echo this value back in the response. 40 character alpha-numeric max.
        /// </summary>
        public string uid { get; set; }

        /// <summary>
        /// (Required)  Identifies the device submitting the request. 
        /// 40 character alpha-numeric max.
        /// </summary>
        public string device { get; set; }

        /// <summary>
        /// (Optional)  Identifies the mobile application submitting the request.
        /// 40 character alpha-numeric max.
        /// </summary>
        public string appId { get; set; }

        /// <summary>
        /// (Optional) The tracking info associated with the CSR’s communication with the patron.  This can represent either a CRM Case or CRM Session.
        /// This will only be present when utilizing a customer service interface where a customer interaction may occur.
        /// </summary>
        public TrackingInfo[] tracking { get; set; }
    }

    /// <summary>
    /// The tracking info associated with the CSR’s communication with the patron.
    /// This can represent either a CRM Case or CRM Session.  This will only be present when utilizing a customer service interface where a customer interaction may occur.
    /// </summary>
    public class TrackingInfo
    {
        /// <summary>
        /// (Required)  The tracking id is a reference to data contained within another system that will be associated with to any activities that are logged
        /// </summary>
        public string trackingId { get; set; }

        /// <summary>
        /// (Required)  Identifies the type of tracking id that is being sent to the server.
        /// </summary>
        public string trackingType { get; set; }
    }

    /// <summary>
    /// Used to track location and user who initiated the request. 
    /// </summary>
    public class X_Cub_Audit
    {
        /// <summary>
        /// (Required) The source IP of the request that caused the activity to be caused.
        /// This could be the remote ip address of the CSR, Patron, or the internal system.
        /// </summary>
        public string sourceIp { get; set; }

        /// <summary>
        /// (Conditionally-Required) The unique identifier of the CSR who caused the activity.
        /// Can be the contact id or the authenticated user to the back office system.
        /// It is a required field when the tracking info in the x-cub-hdr is not empty.
        /// </summary>
        public string userId { get; set; }

        /// <summary>
        /// (Conditionally-Required)  The first name of either the Patron or the CSR who caused the activity.
        /// It is a required field  when the tracking info in the x-cub-hdr is not empty.
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// (Conditionally-Required) The last name of either the Patron or the CSR who caused the activity.
        /// It is a required field when the tracking info in the x-cub-hdr is not empty.
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// (Required) The channel where the request originated.
        /// Refer to section 2.6.1 Channel for a list of channels.
        /// </summary>
        public string channel { get; set; }

        /// <summary>
        /// (Optional) The location where the activity originated.  This will contain the initial contents of the device field in the x-cub-hdr before it gets modified by any other server processing.
        /// Once this field is set it should not be modified.
        /// </summary>
        public string location { get; set; }
    }
}
