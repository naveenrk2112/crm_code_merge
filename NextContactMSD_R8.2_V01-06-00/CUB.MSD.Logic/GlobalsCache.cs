/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;

namespace CUB.MSD.Logic
{
    public static class GlobalsCache
    {
        const double DEFAULT_CACHE_EXPIRATION_MINUTES = 1;

        private static double? _CacheExpirationMinutes = null;

        private static MemoryCache memoryCache = MemoryCache.Default;

        private static GlobalsLogic _GlobalsLogic = null;

        public static bool Clear()
        {
            try
            {
                memoryCache.Trim(100);
                return true;
            }
            catch(Exception e)
            {
                CubLogger.Error(e.Message, e);
                return false;
            }
        }

        public static bool UpdateCacheExpirationMinutes(string offset)
        {
            double value;
            var success = double.TryParse(offset, out value);
            if (success)
            {
                _CacheExpirationMinutes = value;
            }
            return success;
        }

        public static double GetCacheExpirationMinutes(IOrganizationService service)
        {
            if (_CacheExpirationMinutes.HasValue)
            {
                return _CacheExpirationMinutes.Value;
            }

            if (_GlobalsLogic == null)
            {
                _GlobalsLogic = new GlobalsLogic(service);
            }

            var value = _GlobalsLogic.GetAttributeValue(CUBConstants.Globals.CUB_MSD_WEB, CUBConstants.Globals.WEB_APP_CACHE_DURATION_MINUTES);
            double minutes;
            if (!string.IsNullOrEmpty(value) && double.TryParse(value, out minutes))
            {
                _CacheExpirationMinutes = minutes;
            }
            else
            {
                CubLogger.Warn(CUBConstants.Exceptions.UNABLE_TO_GET_GLOBALS_VALUE, $"{CUBConstants.Globals.CUB_MSD_WEB}, {CUBConstants.Globals.WEB_APP_CACHE_DURATION_MINUTES}");
            }
            return _CacheExpirationMinutes ?? DEFAULT_CACHE_EXPIRATION_MINUTES;
        }

        // Customer Registration Globals
        public static string CustomerRegistrationFirstNameMaxLength(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CustomerRegistration, CUBConstants.Globals.CustomerRegistration_FirstName_MaxLength).ToString();
        }

        public static string CustomerRegistrationLastNameMaxLength(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CustomerRegistration, CUBConstants.Globals.CustomerRegistration_LastName_MaxLength).ToString();
        }
        
        public static bool CustomerRegistrationIsAddressRequired(IOrganizationService service)
        {
            bool RET_VAL = false;
            string val = GetKeyFromCache(service, CUBConstants.Globals.CustomerRegistration, CUBConstants.Globals.CustomerRegistration_IsAddressRequired).ToString();
            val = val.ToUpper();

            if (string.Compare(val, "TRUE") == 0 || string.Compare(val, "YES") == 0 || string.Compare(val, "1") == 0) RET_VAL = true;

            return RET_VAL;
        }

        public static string CustomerRegistrationAddressLine1MaxLength(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CustomerRegistration, CUBConstants.Globals.CustomerRegistration_AddressLine1_MaxLength).ToString();
        }

        public static string CustomerRegistrationAddressLine2MaxLength(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CustomerRegistration, CUBConstants.Globals.CustomerRegistration_AddressLine2_MaxLength).ToString();
        }

        public static string CustomerRegistrationPinMaxLength(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Pin.PIN_VALIDATION, CUBConstants.Pin.PIN_MAX_LENGTH).ToString();
        }

        public static string CustomerRegistrationCityMaxLength(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CustomerRegistration, CUBConstants.Globals.CustomerRegistration_City_MaxLength).ToString();
        }

        public static string CustomerRegistrationSecurityAnswerMaxLength(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CustomerRegistration, CUBConstants.Globals.CustomerRegistration_SecurityAnswer_MaxLength).ToString();
        }

        public static string CustomerRegistrationSecurityQuestionsMax(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CustomerRegistration, CUBConstants.Globals.CustomerRegistration_SecurityQuestions_Max).ToString();
        }

        public static string CustomerRegistrationSecurityQuestionsMinimum(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CustomerRegistration, CUBConstants.Globals.CustomerRegistration_SecurityQuestions_Minimum).ToString();
        }

        // Tolling Globals
        public static string TollingApiBaseURL(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.Tolling_API, CUBConstants.Globals.Tolling_API_BASE_URL).ToString();
        }


        public static string NISApiBaseURL(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_BASE_URL).ToString();
        }

        public static string NISGetCustomerNotificationPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CUSTOMER_NOTIFICATION).ToString();
        }

        public static string NISGetCustomerNotificationDetailPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CUSTOMER_NOTIFICATION_DETAIL).ToString();
        }

        public static string NISCustomerNotificationResendPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CUSTOMER_NOTIFICATION_RESEND).ToString();
        }

        public static string NISCustomerFundingSourcePath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CUSTOMER_FUNDINGSOURCE_POST).ToString();
        }

        public static string NISGetOneAccountSummaryPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_ONEACCOUNT_SUMMARY).ToString();
        }

        public static string NISCompleteRegistrationPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_COMP_REG_PATH).ToString();
        }

        public static string NISDeleteAddressPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_DELETE_ADDRESS_PATH).ToString();
        }

        public static string NISUpdateAddressPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_UPDATE_ADDRESS_PATH).ToString();
        }

        public static string NISAddressDependenciesPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_ADDRESS_DEPENDENCIES).ToString();
        }

        public static string NISSendForgottenUsernamePath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_SEND_FORGOTTEN_USERNAME).ToString();
        }

        public static string NISSubsystemTravelTokenCanBeLinkedPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_SUBSYSTEM_TRAVEL_TOKEN_CAN_BE_LINKED_PATH).ToString();
        }

        public static string NISSubssytemBlockAccountPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_SUBSYSTEM_BLOCK_ACCOUNT_PATH).ToString();
        }
        public static string NISSubssytemUnblockAccountPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_SUBSYSTEM_UNBLOCK_ACCOUNT_PATH).ToString();
        }

        public static string NISSubssytemTokenActionPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_SUBSYSTEM_TOKEN_ACTION_PATH).ToString();
        }
        public static string NISApiVerificationTokenReportPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_VERIFICATION_TOKEN_REPORT).ToString();
        }

        public static string AngularGetJweKeyName(IOrganizationService service) 
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CUB_MSD_WEB_ANGULAR, CUBConstants.Globals.ANGULAR_JWE_KEY_NAME).ToString();
        }

        public static string NISSearchTokenPath(IOrganizationService service)
        {
            if (memoryCache.Get(CUBConstants.MemoryCache.NIS_API_SEARCH_TOKEN_PATH) == null)
            {
                if (_GlobalsLogic == null)
                    _GlobalsLogic = new GlobalsLogic(service);
                string attribValue = _GlobalsLogic.GetAttributeValue(CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_SEARCH_TOKEN_PATH);
                if (string.IsNullOrEmpty(attribValue)) throw new NullReferenceException(CUBConstants.Exceptions.UNABLE_TO_GET_GLOBALS_VALUE);
                var offsetInMinutes = GetCacheExpirationMinutes(service);
                memoryCache.Add(CUBConstants.MemoryCache.NIS_API_SEARCH_TOKEN_PATH, attribValue, new DateTimeOffset(DateTime.Now.AddMinutes(offsetInMinutes)));
            }
            return memoryCache.Get(CUBConstants.MemoryCache.NIS_API_SEARCH_TOKEN_PATH).ToString();
        }

        public static string NISApiUsername(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_USERNAME).ToString();
        }

        public static string NISApiPassword(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_PASSWORD).ToString();
        }

        public static string NISApiDevice(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_DEVICE).ToString();
        }

        public static string NISGetAccountStatusPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_STATUS).ToString();
        }

        public static string NISGetAccountBalanceHistoryPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_BALANCE_HISTORY).ToString();
        }
        public static string NISGetAccountTravelHistoryPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_TRAVEL_HISTORY).ToString();
        }

        public static string NISGetAccountTravelHistoryTapsPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_TRAVEL_HISTORY_TAPS).ToString();
        }

        public static string NISGetAccountoOrderHistoryPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_ORDER_HISTORY).ToString();
        }

        public static string NISUpdateDependenciesPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_UPDATE_DEPENDANCIES).ToString();
        }

        public static string NISReportChangePath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_REPORT_CHANGE).ToString();
        }

        internal static string AppInfoAppVersion(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.AppInfo, CUBConstants.Globals.AppInfo_AppVersion).ToString();
        }

        internal static string AppInfoClientName(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.AppInfo, CUBConstants.Globals.AppInfo_ClientName).ToString();
        }

        internal static bool AppInfoSoftDeleteAddress(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Globals.AppInfo, CUBConstants.Globals.APPINFO_SOFT_DELETE_ADDRESS).ToString();
            return (value.ToLower() == "Yes".ToLower() || value == "1");
        }

        internal static int AppInfoMaxFailedLoginAttempts(IOrganizationService service)
        {
            //note: we want error thrown if there is not global defined or we couldn't parse it
            string value = GetKeyFromCache(service, CUBConstants.Globals.AppInfo, CUBConstants.Globals.APPINFO_MAX_FAILED_LOGIN_ATTEMPTS).ToString();
            return int.Parse(value);
        }

        public static bool RollupLinkedEntityActivitiesToAccount(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Globals.AppInfo, CUBConstants.Globals.RollupLinkedEntityActivitiesToAccount).ToString();
            bool returnValue = false;
            bool.TryParse(value, out returnValue);
            return returnValue;
        }

        public static int PasswordMinLength(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_MIN_LENGTH).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static int PinMinLength(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Pin.PIN_VALIDATION, CUBConstants.Pin.PIN_MIN_LENGTH).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static int PinMaxLength(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Pin.PIN_VALIDATION, CUBConstants.Pin.PIN_MAX_LENGTH).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static bool PinNumericOnly(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Pin.PIN_VALIDATION, CUBConstants.Pin.PIN_NUMERIC_ONLY).ToString();
            if (string.IsNullOrEmpty(value) || value != "1")
                return false;
            else
                return true;
        }

        public static string EmailMatchRegEX(IOrganizationService service)
        {
           return GetKeyFromCache(service, CUBConstants.Email.EMAIL_FORMAT_VALIDATION, CUBConstants.Email.EMAIL_MATCH_REGEX).ToString();
           
        }

        public static int PasswordMaxLength(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_MAX_LENGTH).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static int PasswordUpperAlpha(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_UPPER_ALPHA).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static int PasswordLowerAlpha(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_LOWER_ALPHA).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static int NumberOfPasswordsToArchive(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.NUMBER_OF_PASSWORDS_ARCHIVE).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
         
        }

        public static int PasswordNonAlphaNumeric(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_NON_ALPHANUMERIC).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static int PasswordNumeric(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_NUMERIC).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static bool PasswordAllowUsername(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_ALLOW_USERNAME).ToString();
            if (string.IsNullOrEmpty(value))
                return false;
            else
                return Convert.ToBoolean(value);
        }

        public static bool PasswordAllowConsecutiveChars(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_ALLOW_CONSECUTIVE_CHARS).ToString();
            if (string.IsNullOrEmpty(value))
                return true;
            else
                return Convert.ToBoolean(value);
        }

        public static bool PasswordAllowContiguousChars(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_ALLOW_CONTIGUOUS_CHARS).ToString();
            if (string.IsNullOrEmpty(value))
                return true;
            else
                return Convert.ToBoolean(value);
        }

        public static bool PasswordAllowDictionaryWords(IOrganizationService service)
        {
            try
            {
                string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.PASSWORD_ALLOW_DICTIONARY_WORDS).ToString();
                if (string.IsNullOrEmpty(value))
                    return true;
                else
                    return Convert.ToBoolean(value);
            }
            catch (Exception e)
            {
                if (string.Compare(e.Message, CUBConstants.Exceptions.UNABLE_TO_GET_GLOBALS_VALUE) == 0)
                {
                    return true;
                }

                throw e;
            }
        }

        public static double TempPasswordDurationDays(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Password.PASSWORD_VALIDATION, CUBConstants.Password.TEMP_PASSWORD_DURATION_DAYS).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToDouble(value);
        }

        public static int UsernameMinLength(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Globals.USERNAME_VALIDATION, CUBConstants.Globals.USERNAME_MIN_LENGTH).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static int UsernameMaxLength(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Globals.USERNAME_VALIDATION, CUBConstants.Globals.USERNAME_MAX_LENGTH).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static bool UsernameRequireEmail(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Globals.USERNAME_VALIDATION, CUBConstants.Globals.USERNAME_REQUIRE_EMAIL).ToString();
            if (string.IsNullOrEmpty(value) || value != "1")
                return false;
            else
                return true;
        }

        public static int MobileTokenMaxLength(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Globals.MOBILE_TOKEN_VALIDATION, CUBConstants.Globals.MOBILE_TOKEN_MAX_LENGTH).ToString();
            if (string.IsNullOrEmpty(value))
                return 0;
            else
                return Convert.ToInt32(value);
        }

        public static string ActivityTypeAddPhoneNumber(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.ACTIVITY_AUTO_CREATE_RULES, CUBConstants.Globals.ACTIVITY_ADD_PHONE_NUMBER_ACTIVITY_TYPE).ToString();

        }

        public static string ActivityTypeAddAddress(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.ACTIVITY_AUTO_CREATE_RULES, CUBConstants.Globals.ACTIVITY_ADD_ADDRESS_ACTIVITY_TYPE).ToString();

        }

        public static string ActivityTypeContactVerifyContactInfo(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.ACTIVITY_AUTO_CREATE_RULES, CUBConstants.Globals.ACTIVITY_CONTACT_VERIFY_CONTACT_INFO).ToString();

        }

        public static string ActivityTypeContactVerifySecurityQuestion(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.ACTIVITY_AUTO_CREATE_RULES, CUBConstants.Globals.ACTIVITY_CONTACT_VERIFY_SECURITY_QUESTION_INFO).ToString();

        }
        public static string ActivityTypeContactVerificationUnverified(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.ACTIVITY_AUTO_CREATE_RULES, CUBConstants.Globals.ACTIVITY_CONTACT_VERIFY_UNVERIFIED).ToString();

        }

        public static string NISGetNotificationsPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_NOTIFICATIONS_PATH).ToString();
        }

        public static string NISGetNotificationDetailsPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_NOTIFICATIONS_DETAILS_PATH).ToString();
        }

        public static string NISGetOrderNotificationsPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_ORDER_NOTIFICATIONS_PATH).ToString();
        }

        public static string NISGetResendNotificationsPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_RESEND_NOTIFICATION_PATH).ToString();
        }

        public static string NISGetOrderPaymentReceiptPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_ORDER_PAYMENT_RECEIPT_PATH).ToString();
        }

        public static bool AllowMultipleContactsPerCustomer(IOrganizationService service)
        {
            string value = GetKeyFromCache(service, CUBConstants.Globals.AppInfo, CUBConstants.Globals.AllowMultipleContactsPerCustomer).ToString();
            bool returnValue = false;
            bool.TryParse(value, out returnValue);
            return returnValue;
        }

        /// <summary>
        /// Get a GlobalKey from the cache.
        /// In the case the key is not in the cache it will be recovered from MSD and added to the cache
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <param name="keyName">Global Key Name</param>
        /// <param name="attributeName">Global Attribute Name</param>
        /// <returns>Global Value</returns>
        public static object GetKeyFromCache(IOrganizationService service, string keyName, string attributeName)
        {
            string cacheKeyName = string.Format("{0}.{1}", keyName, attributeName);
            if (memoryCache.Get(cacheKeyName) == null)
            {
                if (_GlobalsLogic == null)
                    _GlobalsLogic = new GlobalsLogic(service);
                string attribValue = _GlobalsLogic.GetAttributeValue(keyName, attributeName);
                if (attribValue == null)
                {
                    throw new NullReferenceException(CUBConstants.Exceptions.UNABLE_TO_GET_GLOBALS_VALUE);
                }
                var offsetInMinutes = GetCacheExpirationMinutes(service);
                memoryCache.Add(cacheKeyName, attribValue, new DateTimeOffset(DateTime.Now.AddMinutes(offsetInMinutes)));
            }
            return memoryCache.Get(cacheKeyName);
        }

        /// <summary>
        /// Get a key value from the Cache
        /// </summary>
        /// <param name="keyName">Key Name</param>
        /// <returns>Cache key value or null if not present into the cache</returns>
        public static object GetFromCache(string keyName)
        {
            return memoryCache.Get(keyName);
        }

        /// <summary>
        /// Add a key/value to the Cache
        /// </summary>
        /// <param name="keyName">Key Name</param>
        /// <param name="value">Value</param>
        public static void AddToCache(string keyName, object value)
        {
            var offsetInMinutes = _CacheExpirationMinutes ?? DEFAULT_CACHE_EXPIRATION_MINUTES;
            memoryCache.Add(keyName, value, new DateTimeOffset(DateTime.Now.AddMinutes(offsetInMinutes)));
        }

        /// <summary>
        /// Get Account Types from Cache. If it is not already in cache it is retrieved from MSD and added to the cache.
        /// Entity: Cub_AccountType
        /// </summary>
        /// <param name="organizationService">Organization Service</param>
        /// <returns>Account type</returns>
        public static List<KeyValuePair<Guid, string>> GetAccountTypesForUI(IOrganizationService organizationService)
        {
            string cacheName = CUBConstants.GlobalCustomerRegisterConstants.ACCOUNT_TYPES_FOR_UI;
            List<KeyValuePair<Guid, string>> types = (List<KeyValuePair<Guid, string>>)GlobalsCache.GetFromCache(cacheName);
            if (types == null)
            {
                using (var osc = new CUBOrganizationServiceContext(organizationService))
                {
                    types = (from t
                             in osc.cub_AccountTypeSet
                             orderby t.cub_AccountName
                             select new KeyValuePair<Guid, string>(t.cub_AccountTypeId.Value, t.cub_AccountName)).ToList();
                    GlobalsCache.AddToCache(cacheName, types);
                }
            }
            return types;
        }

        /// <summary>
        /// Get Contact Types from Cache. If it is not already in cache it is retrieved from MSD and added to the cache
        /// Entity: Cub_ContactType
        /// </summary>
        /// <param name="organizationService">Organization Service</param>
        /// <returns>Contact Type</returns>
        public static List<KeyValuePair<Guid, string>> GetContactTypesForUI(IOrganizationService organizationService)
        {
            string cacheName = CUBConstants.GlobalCustomerRegisterConstants.CONTACT_TYPES_FOR_UI;
            List<KeyValuePair<Guid, string>> types = (List<KeyValuePair<Guid, string>>)GlobalsCache.GetFromCache(cacheName);
            if (types == null)
            {
                using (var osc = new CUBOrganizationServiceContext(organizationService))
                {
                    types = (from t
                                 in osc.cub_ContactTypeSet
                             orderby t.cub_Name
                             select new KeyValuePair<Guid, string>(t.cub_ContactTypeId.Value, t.cub_Name)
                                ).ToList();
                    GlobalsCache.AddToCache(cacheName, types);
                }
            }
            return types;
        }

        /// <summary>
        /// Get Countries from Cache. If it is not already in cache the account types are retrieved from MSD and added to the cache
        /// Entity: Cub_Country
        /// </summary>
        /// <param name="organizationService">Organization Service</param>
        /// <returns>Countries</returns>
        public static List<KeyValuePair<Guid, string>> GetCountryListForUI(IOrganizationService organizationService)
        {
            string cacheName = CUBConstants.GlobalCustomerRegisterConstants.COUNTRIES_FOR_UI;
            List<KeyValuePair<Guid, string>> countries = (List<KeyValuePair<Guid, string>>)GlobalsCache.GetFromCache(cacheName);
            if (countries == null)
            {
                using (var osc = new CUBOrganizationServiceContext(organizationService))
                {
                    countries = (from c
                                 in osc.cub_CountrySet
                                 orderby c.cub_CountryName
                                 select new KeyValuePair<Guid, string>(c.cub_CountryId.Value, c.cub_CountryName)
                                ).ToList();
                    GlobalsCache.AddToCache(cacheName, countries);
                }
            }
            return countries;
        }

        /// <summary>
        /// Get States from Cache. If it is not already in cache the account types are retrieved from MSD and added to the cache
        /// Entity: cub_states
        /// </summary>
        /// <param name="organizationService">Organization Service</param>
        /// <param name="countryAlpha2">(Optional) Country alpha 2</param>
        /// <returns>States</returns>
        public static List<KeyValuePair<Guid, string>> GetStatesListForUI(IOrganizationService organizationService, 
                                                                          string countryAlpha2 = null)
        {
            string cacheName = CUBConstants.GlobalCustomerRegisterConstants.STATES_FOR_UI;
            if (countryAlpha2 != null)
            {
                cacheName = cacheName + "." + countryAlpha2;
            }
            List<KeyValuePair<Guid, string>> states = (List<KeyValuePair<Guid, string>>)GlobalsCache.GetFromCache(cacheName);
            if (states == null)
            {
                using (var osc = new CUBOrganizationServiceContext(organizationService))
                {
                    if (countryAlpha2 == null)
                    {
                        states = (from s in osc.cub_StateOrProvinceSet
                                  orderby s.cub_StateName
                                  select new KeyValuePair<Guid, string>(s.cub_StateOrProvinceId.Value, String.Format("{0} - {1}", s.cub_Abbreviation, s.cub_StateName))
                                  ).ToList();
                    }
                    else
                    {
                        states = (from s in osc.cub_StateOrProvinceSet
                                  join c in osc.cub_CountrySet on s.cub_Country.Id equals c.cub_CountryId
                                  orderby s.cub_StateName
                                  where c.cub_Alpha2.Equals(countryAlpha2)
                                  select new KeyValuePair<Guid, string>(s.cub_StateOrProvinceId.Value, String.Format("{0} - {1}", s.cub_Abbreviation, s.cub_StateName))
                                  ).ToList();
                    }
                    GlobalsCache.AddToCache(cacheName, states);
                }
            }
            return states;
        }

        /// <summary>
        /// Get Security Questions from Cache. If it is not already in cache the account types are retrieved from MSD and added to the cache
        /// Entity: cub_securityquestion
        /// </summary>
        /// <param name="organizationService">Organization Service</param>
        /// <returns>Security Questions</returns>
        public static List<KeyValuePair<Guid, string>> GetSecurityQuestionsForUI(IOrganizationService organizationService)
        {
            string cacheName = CUBConstants.GlobalCustomerRegisterConstants.SECURITY_QUESTION_FOR_UI;
            List<KeyValuePair<Guid, string>> questions = (List<KeyValuePair<Guid, string>>)GlobalsCache.GetFromCache(cacheName);
            if (questions == null)
            {
                using (var osc = new CUBOrganizationServiceContext(organizationService))
                {
                    questions = (from s
                                 in osc.cub_SecurityQuestionSet
                                 orderby s.cub_SecurityQuestionId
                                 select new KeyValuePair<Guid, string>(s.cub_SecurityQuestionId.Value, s.cub_SecurityQuestionDesc)
                                ).ToList();
                    GlobalsCache.AddToCache(cacheName, questions);
                }
            }
            return questions;
        }

        /// <summary>
        /// Get the Link Subsystem to Customer Account MVC action path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>MVC action path</returns>
        public static string NISLinkSubssystemToCustomerAccountPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_LINK_SUBSYSTEM_TO_CUSTOMER_ACCOUNT).ToString();
        }

        /// <summary>
        /// Get the Delink Subsystem from Customer Account MVC action path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>MVC action path</returns>
        public static string NISCanBeDelinkedSubssystemFromCustomerAccountPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CANBEDELINKED_SUBSYSTEM_FROM_CUSTOMER_ACCOUNT).ToString();
        }

        /// <summary>
        /// Get the Delink Subsystem from Customer Account MVC action path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>MVC action path</returns>
        public static string NISDelinkSubssystemFromCustomerAccountPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_DELINK_SUBSYSTEM_TO_CUSTOMER_ACCOUNT).ToString();
        }

        /// <summary>
        /// Get the Order DebtCollect MVC action path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>MVC action path</returns>
        public static string NISOrderDebtCollectPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_ORDER_DEBTCOLLECT).ToString();
        }

        /// <summary>
        /// Get Order Adjustment MVC action path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>MVC action path</returns>
        public static string NISOrderAdjustmentPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_ORDER_ADJUSTMENT).ToString();
        }

        /// <summary>
        /// Get Order Travel Correction Path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>MVC action path</returns>
        public static string NISOrderTravelCorrectionPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_ORDER_TRAVEL_CORRECTION).ToString();
        }


        /// <summary>
        /// Get the Customer Notification Preferences MVC action path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>MVC action path</returns>
        public static string NISGetCustomerNotificationPreferencesPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CUSTOMER_NOTIFICATION_PREFERENCES).ToString();
        }

        /// <summary>
        /// Get the Config Notification Channel MVC action path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>MVC action path</returns>
        public static string NISGetConfigNotificationChannelPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CONFIG_NOTIFICATION_CHANNEL).ToString();
        }

        /// <summary>
        /// Get the path for the contact notification of lockout
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public static string NISApiNotificationLockoutReportPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_REPORT_ACCOUNT_LOCKED).ToString();
        }

        /// <summary>
        /// Get the path for the report contact password change
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public static string NISPasswordReportChangePath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_PASSWORD_REPORT_CHANGE).ToString();
        }

        /// <summary>
        /// Get the Customer Notification Preferences Patch Path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>MVC action path</returns>
        public static string NISCustomerNotificationPreferencesPatchPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CUSTOMER_NOTIFICATION_PREFERENCES_PATCH).ToString();
        }
        
        public static string SubsystemTokenTypeMappingsPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_SUBSYSTEM_TOKEN_TYPE_MAPPING).ToString();
        }

        /// <summary>
        /// Get Customer Activity path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns></returns>
        public static string NISGetCustomerActivityPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CUSTOMER_ACTIVITY_PATH).ToString();
        }

        /// <summary>
        /// Get Transit Account Subsystem Activity path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns></returns>
        public static string NISTASubsystemActivityPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_TASUBSYSTEM_ACTIVITY_PATH).ToString();
        }

        /// <summary>
        /// Get Customer Activity Details path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns></returns>
        public static string NISGetCustomerActivityDetailPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CUSTOMER_ACTIVITY_DETAIL_PATH).ToString();
        }

        /// <summary>
        /// Get Transit Account Subsystem Activity Details path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns></returns>
        public static string NISTASubsystemActivityDetailPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_TASUBSYSTEM_ACTIVITY_DETAIL_PATH).ToString();
        }

        /// <summary>
        /// Get Transit Account Bank Card Charge History Path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>Transit Account Bank Card Charge History Path</returns>
        public static string NISGetTransitAccountBankCardChargeHistoryPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_BANK_CARD_CHARGE_HISTORY_PATH).ToString();
        }

        /// <summary>
        /// Get Transit Account Agregated Details Path
        /// </summary>
        /// <param name="service">MSD Organization Service</param>
        /// <returns>Transit Account Aggegated Details Path</returns>
        public static string NISGetTransitBankChargeAggregationDetailsPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_BANK_CHARGE_AGGREGATION_DETAILS_PATH).ToString();
        }

        /// <summary>
        /// Get x_cub_hdr Tracking Type from Globals
        /// </summary>
        /// <param name="service">MSd service</param>
        /// <returns>x_cub_hdr Tracking Type from Globals</returns>
        public static string NISGet_x_cub_hdr_TrackingType(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_X_CUB_HDR_TRACKING_TYPE).ToString();
        }

        /// <summary>
        /// Get x_cub_hdr App Id from Globals
        /// </summary>
        /// <param name="service">MSd service</param>
        /// <returns>x_cub_hdr App from Globals</returns>
        public static string NISGet_x_cub_hdr_AppId(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_X_CUB_HDR_APP_ID).ToString();
        }

        /// <summary>
        /// Get x_cub_audit Location from Globals
        /// </summary>
        /// <param name="service">MSd service</param>
        /// <returns>x_cub_audit Location from Globals</returns>
        public static string NISGet_x_cub_audit_Location(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_X_CUB_AUDIT_LOCATION).ToString();
        }

        /// <summary>
        /// Get x_cub_audit channel from Globals
        /// </summary>
        /// <param name="service">MSd service</param>
        /// <returns>x_cub_audit channel from Globals</returns>
        public static string NISGet_x_cub_audit_Channel(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_X_CUB_AUDIT_CHANNEL).ToString();
        }

        /// <summary>
        /// Get subsystem travel modes URL path from Globals
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <returns>subsystem travel modes URL path from Globals</returns>
        public static string NISSubsystemTravelModesPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_SUBSYSTEM_TRAVEL_MODES_PATH).ToString();
        }

        /// <summary>
        /// Get subsystem travel operators URL path from Globals
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <returns>subsystem travel operators URL path from Globals</returns>
        public static string NISSubsystemTravelOperatorsPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_SUBSYSTEM_TRAVEL_OPERATORS_PATH).ToString();
        }

        /// <summary>
        /// Get subsystem tap types URL path from Globals
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <returns>subsystem tap types URL path from Globals</returns>
        public static string NISSubsystemTapTypesPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_SUBSYSTEM_TAP_TYPES_PATH).ToString();
        }

        /// <summary>
        /// Get subsystem tap statuses URL path from Globals
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <returns>subsystem tap statuses URL path from Globals</returns>
        public static string NISSubsystemTapStatusesPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_SUBSYSTEM_TAP_STATUSES_PATH).ToString();
        }

        public static string NISSubsystemLocationsPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_SUBSYSTEM_LOCATIONS_PATH).ToString();
        }

        /// <summary>
        /// Get Order Config Data Types path from Globals
        /// </summary>
        /// <param name="service">MSD service</param>
        /// <returns>Get Order Data Types path from Globals</returns>
        public static string NISGetOrderConfigDataTypesPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_ORDER_CONFIG_DATA_TYPES_PATH).ToString();
        }

        /// <summary>
        /// Get Customer Service Reason Codes
        /// </summary>
        /// <param name="service">MSD service</param>
        /// <returns>Customer Service Reason Codes</returns>
        public static string NISGetCustomerServiceReasonCodesPath(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_CUSTOMER_SERVICE_REASON_CODES_PATH).ToString();
        }

        public static string NISGetPaymentRepositoryNoncePath(IOrganizationService _organizationService)
        {
            //GetPaymentRepositoryNoncePath
            return GetKeyFromCache(_organizationService, CUBConstants.Globals.NIS_API, CUBConstants.Globals.NIS_API_PAYMENT_REPOSITORY_GET_NONCE_PATH).ToString();
        }



        /// <summary>
        /// Get recent verification activity time offset from Globals
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <returns>recent verification activity time offset from Globals</returns>
        public static string RecentVerificationsOffsetInMinutes(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CUB_MSD_WEB_ANGULAR, CUBConstants.Globals.RECENT_VERIFICATIONS_OFFSET_IN_MINUTES).ToString();
        }

        /// <summary>
        /// Get VERIFIED subject from Globals
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <returns>VERIFIED subject from Globals</returns>
        public static string VerificationSubjectVerified(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CUB_MSD_WEB_ANGULAR, CUBConstants.Globals.VERIFICATION_SUBJECT_VERIFIED).ToString();
        }

        /// <summary>
        /// Get NOT VERIFIED subject from Globals
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <returns>NOT VERIFIED subject from Globals</returns>
        public static string VerificationSubjectNotVerified(IOrganizationService service)
        {
            return GetKeyFromCache(service, CUBConstants.Globals.CUB_MSD_WEB_ANGULAR, CUBConstants.Globals.VERIFICATION_SUBJECT_NOT_VERIFIED).ToString();
        }
    }
}
