/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk.Query;
using CUB.MSD.Model.MSDApi;
using System.ServiceModel;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CUB.MSD.Logic
{
    /// <summary>
    /// Contact Logic
    /// </summary>
    public class ContactLogic : LogicBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public ContactLogic(IOrganizationService service) : base(service)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <param name="executionContext">MSD plugin execution contect</param>
        public ContactLogic(IOrganizationService service, IPluginExecutionContext executionContext) : base(service, executionContext)
        {
        }

        /// <summary>
        /// Get Country by ID
        /// </summary>
        /// <param name="countryId">Country ID</param>
        /// <returns>Country</returns>
        public virtual cub_Country GetCountryById(Guid countryId)
        {
            try
            {
                return
                    (from cnty in CubOrgSvc.cub_CountrySet
                     where cnty.cub_CountryId.Value == countryId
                     select cnty).FirstOrDefault();
            }
            catch (Exception ex)
            {
                CubLogger.Error("", ex);
                return null;
            }
        }

        /// <summary>
        /// Get State by ID
        /// </summary>
        /// <param name="stateId">State ID</param>
        /// <returns>State</returns>
        public virtual cub_StateOrProvince GetStateById(Guid stateId)
        {
            return
                (from state in CubOrgSvc.cub_StateOrProvinceSet
                 where state.cub_StateOrProvinceId.Value == stateId
                 select state).FirstOrDefault();
        }

        /// <summary>
        /// Get ZipCode by ID
        /// </summary>
        /// <param name="zipId">Zipcode</param>
        /// <returns>Zip/Postal code</returns>
        public virtual cub_ZipOrPostalCode GetZipById(Guid zipId)
        {
            return
                (from zip in CubOrgSvc.cub_ZipOrPostalCodeSet
                 where zip.cub_ZipOrPostalCodeId.Value == zipId
                 select zip).FirstOrDefault();
        }

        /// <summary>
        /// Get Contacts by Account ID
        /// </summary>
        /// <param name="accountId">Account ID</param>
        /// <returns>Conacts</returns>
        public virtual IEnumerable<Contact> GetByAccountId(Guid accountId)
        {
            IEnumerable<Contact> contacts = null;
            contacts = (from con in CubOrgSvc.ContactSet
                        where con.AccountId.Id == accountId
                        select
                            new Contact()
                            {
                                ParentCustomerId = con.AccountId,
                                FirstName = con.FirstName,
                                LastName = con.LastName,
                                MiddleName = con.MiddleName,
                                EMailAddress1 = con.EMailAddress1,
                                Telephone1 = con.Telephone1,
                                Telephone2 = con.Telephone2,
                                Telephone3 = con.Telephone3,
                                cub_Phone1Type = con.cub_Phone1Type,
                                cub_Phone2Type = con.cub_Phone2Type,
                                cub_Phone3Type = con.cub_Phone3Type,
                                ContactId = con.ContactId
                            }).ToList();
            return contacts;
        }

        public bool MultipleContactsAllowedOnCustomer(Contact createContact, out string err)
        {
            bool returnValue = false;

            err = string.Empty;

            //Get existing contacts linked to the customer of the new contact
            var existingContact = CubOrgSvc.ContactSet.Where(c => c.AccountId.Id.Equals(createContact.ParentCustomerId.Id)).ToList();
            if (existingContact.Count > 0)
            {
                // Get app setting 
                if (!GlobalsCache.AllowMultipleContactsPerCustomer(_organizationService))
                {
                    err = CUBConstants.Contact.Error.MULTIPLE_CONTACTS_NOT_ALLOWED;
                    return returnValue;
                }

            }

            returnValue = true;
            return returnValue;
        }

        /// <summary>
        /// Get random Account Number
        /// </summary>
        /// <returns><Acc/returns>
        public virtual string GetAccountNumber()
        {
            Random rndgen = new Random();
            return rndgen.ToString();
        }

        /// <summary>
        /// Perform any pre-update tasks before contact update operation
        /// </summary>
        /// <param name="preUpdateContact">Contact entity containing pre update values. 
        /// Note: 
        ///     1. Make sure to update crmregister file with any changes to the fields    
        ///     2. preUpdateAdress only contains the fields as specified in the Attributes property of the Image tag in the crmregister file
        /// </param>
        /// <param name="updateContact">Contact entity containing update values not yet commited
        /// Note:
        ///     1. updateAddress only contains the value of the changed fields. The platform only send the changed values
        /// </param>
        public void DoPreUpdateTasks(Contact preUpdateContact, Contact updateContact)
        {
            #region Create activity if a phone is added
            string oldValue = preUpdateContact.GetAttributeValue<string>(Contact.telephone1Attribute);
            string newValue = updateContact.GetAttributeValue<string>(Contact.telephone1Attribute);
            List<string> phoneChanges = new List<string>();
            if (string.IsNullOrWhiteSpace(oldValue) && !string.IsNullOrWhiteSpace(newValue))
            {
                phoneChanges.Add($"{Contact.telephone1Attribute}: {newValue}");
            }
            oldValue = preUpdateContact.GetAttributeValue<string>(Contact.telephone2Attribute);
            newValue = updateContact.GetAttributeValue<string>(Contact.telephone2Attribute);
            if (string.IsNullOrWhiteSpace(oldValue) && !string.IsNullOrWhiteSpace(newValue))
            {
                phoneChanges.Add($"{Contact.telephone2Attribute}: {newValue}");
            }
            oldValue = preUpdateContact.GetAttributeValue<string>(Contact.telephone3Attribute);
            newValue = updateContact.GetAttributeValue<string>(Contact.telephone3Attribute);
            if (string.IsNullOrWhiteSpace(oldValue) && !string.IsNullOrWhiteSpace(newValue))
            {
                phoneChanges.Add($"{Contact.telephone3Attribute}: {newValue}");
            }
            //create activity of type 'Add Phone Number'
            if (phoneChanges.Count > 0)
                CreateActivityOnPhoneAdd(updateContact, phoneChanges);
            #endregion Create activity if a phone is added
        }

        private void CreateActivityOnPhoneAdd(Contact updateContact, List<string> phoneChanges)
        {
            using (ActivityLogic activityLogic = new ActivityLogic(_organizationService))
            {
                CreateMSDActivityData activityData = new CreateMSDActivityData();

                activityData.ContactId = updateContact.Id;
                Guid ownerId = CubOrgSvc.ContactSet
                    .Where(c => c.ContactId == updateContact.Id)
                    .Select(u => u.OwnerId.Id).Single();
                activityData.CreatedById = ownerId;
                //look for active session 
                var csrSession = CubOrgSvc.cub_CSRSessionSet
                    .Where(s => s.cub_ContactId.Id == updateContact.Id
                            && s.OwnerId.Id == ownerId
                            && s.statecode == cub_CSRSessionState.Active).ToList();
                if (csrSession.Count == 1)
                    activityData.SessionId = csrSession[0].Id;
                //lookup Activity name from global to later lookup activty Id
                string activityNameFromGlobals = CubOrgSvc.cub_GlobalDetailsSet
                    .Where(gd => gd.cub_AttributeValue == GlobalsCache.ActivityTypeAddPhoneNumber(_organizationService))
                    .Select(gd => gd.cub_AttributeValue).Single();

                activityData.ActivityTypeId = activityLogic.GetActivityTypeId(activityNameFromGlobals);

                activityData.Description = $"Phone(s) added:{Environment.NewLine} {string.Join(Environment.NewLine, phoneChanges)}";
                CubResponse<WSCreateNewActivityResponse> cubResponse = activityLogic.CreateNewActivity(activityData);
                if (cubResponse.Errors.Count > 0)
                {
                    string err = string.Empty;
                    foreach (var item in cubResponse.Errors)
                    {
                        err += $"{Environment.NewLine}Key: {item.errorKey} Field: {item.fieldName} Message: {item.errorMessage}";
                    }
                    CubLogger.Error(err);
                }
            }
        }

        /// <summary>
        /// Get birth date
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <returns>Birth date</returns>
        public DateTime? GetBirthDate(Guid contactId)
        {
            return (from c in CubOrgSvc.ContactSet
                    where c.Id.Equals(contactId)
                        && c.StateCode.Value.Equals((int)ContactState.Active)
                    select c.BirthDate).FirstOrDefault();
        }

        /// <summary>
        /// Get User Name an PIN
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <returns>Credentials</returns>
        public cub_Credential GetUsernameAndPin(Guid contactId)
        {
            return (from c in CubOrgSvc.cub_CredentialSet
                    where c.cub_ContactId.Id.Equals(contactId)
                        && c.statecode.Value.Equals((int)cub_CredentialState.Active)
                    select new cub_Credential
                    {
                        cub_Username = c.cub_Username,
                        cub_Pin = c.cub_Pin
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Create Verification Activity
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <returns>Activity ID</returns>
        public Guid CreateVerificationActivity(Guid contactId, string subject, string body)
        {
            var contactRef = (from c in CubOrgSvc.ContactSet
                              where c.ContactId == contactId
                              select new Contact
                              {
                                  ContactId = contactId
                              }).FirstOrDefault().ToEntityReference();
            var va = new cub_Verification
            {
                Subject = "Customer Verification",
                RegardingObjectId = contactRef
            };
            var id = _organizationService.Create(va);
            va.Id = id;
            var note = new Annotation
            {
                Subject = subject,
                NoteText = body,
                ObjectId = va.ToEntityReference()
            };
            _organizationService.Create(note);
            va.StateCode = cub_VerificationState.Completed;
            va.StatusCode = new OptionSetValue((int)cub_verification_statuscode.Completed);
            _organizationService.Update(va);
            return id;
        }

        /// <summary>
        /// Search By First Name, Last Name, Phone and Email
        /// </summary>
        /// <param name="firstname">First Name</param>
        /// <param name="lastname">Last Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">Phone</param>
        /// <returns>Conact data as JSON</returns>
        public virtual string SearchByFirstLastPhoneEmail(string firstname, string lastname, string email, string phone)
        {
            //If no search parameters supplied, return null
            if (string.IsNullOrEmpty(firstname) && string.IsNullOrEmpty(lastname) && string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone))
                return null;
            //trim all white spaces
            if (!string.IsNullOrEmpty(firstname)) firstname = firstname.Trim();
            if (!string.IsNullOrEmpty(lastname)) lastname = lastname.Trim();
            if (!string.IsNullOrEmpty(email)) email = email.Trim();
            if (!string.IsNullOrEmpty(phone))
            {
                var digitsOnly = new Regex(@"[^\d]");
                phone = digitsOnly.Replace(phone, "");
                phone = phone.Trim();
            }
            var queryExpression = new QueryExpression(Contact.EntityLogicalName);
            queryExpression.NoLock = true;
            queryExpression.Distinct = true;
            queryExpression.ColumnSet = new ColumnSet(Contact.contactidAttribute,
                                                      Contact.cub_contacttypeidAttribute,
                                                      Contact.fullnameAttribute,
                                                      Contact.firstnameAttribute,
                                                      Contact.lastnameAttribute,
                                                      Contact.emailaddress1Attribute,
                                                      Contact.telephone1Attribute,
                                                      Contact.telephone2Attribute,
                                                      Contact.telephone3Attribute,
                                                      Contact.cub_phone1typeAttribute,
                                                      Contact.cub_phone2typeAttribute,
                                                      Contact.cub_phone3typeAttribute);
            //Adding Customer (Account) to the resultset
            queryExpression.LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, Account.EntityLogicalName, Contact.parentcustomeridAttribute,
                                                            Account.accountidAttribute, JoinOperator.LeftOuter));
            queryExpression.LinkEntities[0].Columns.AddColumns(Account.accountidAttribute,
                                                               Account.nameAttribute,
                                                               Account.cub_accounttypeidAttribute,
                                                               Account.cub_oneaccountidAttribute);
            queryExpression.LinkEntities[0].EntityAlias = "customer";
            queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Account.nameAttribute, OrderType.Ascending));
            //Add account(customer) type to the resultset
            queryExpression.LinkEntities[0].LinkEntities.Add(new LinkEntity(Account.EntityLogicalName, cub_AccountType.EntityLogicalName,
                                                                            Account.cub_accounttypeidAttribute, cub_AccountType.cub_accounttypeidAttribute,
                                                                            JoinOperator.LeftOuter));
            queryExpression.LinkEntities[0].LinkEntities[0].Columns.AddColumns(cub_AccountType.cub_accountnameAttribute);
            queryExpression.LinkEntities[0].LinkEntities[0].EntityAlias = "accountType";
            //Adding Address city to the resultset
            queryExpression.LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_Address.EntityLogicalName, Contact.cub_addressidAttribute,
                                                            cub_Address.cub_addressidAttribute, JoinOperator.LeftOuter));
            queryExpression.LinkEntities[1].Columns.AddColumns(cub_Address.cub_cityAttribute);
            queryExpression.LinkEntities[1].EntityAlias = "address";
            //Add contact type to the resultset
            queryExpression.LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_ContactType.EntityLogicalName, Contact.cub_contacttypeidAttribute, cub_ContactType.cub_contacttypeidAttribute, JoinOperator.LeftOuter));
            queryExpression.LinkEntities[2].Columns.AddColumns(cub_ContactType.cub_nameAttribute);
            queryExpression.LinkEntities[2].EntityAlias = "contactType";
            //Adding sorting  - account name, contact full name
            queryExpression.Orders.Add(new OrderExpression(Contact.fullnameAttribute, OrderType.Ascending));
            queryExpression.Orders.Add(new OrderExpression(Contact.emailaddress1Attribute, OrderType.Ascending));
            FilterExpression filter = new FilterExpression(LogicalOperator.Or);
            FilterExpression filterContactName = new FilterExpression(LogicalOperator.And);
            if (!string.IsNullOrEmpty(firstname))
            {
                if (firstname.Substring(firstname.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                    filterContactName.AddCondition(Contact.firstnameAttribute, ConditionOperator.BeginsWith, firstname.Remove(firstname.Length - 1));
                else
                    filterContactName.AddCondition(Contact.firstnameAttribute, ConditionOperator.Equal, firstname);
            }
            if (!string.IsNullOrEmpty(lastname))
            {
                if (lastname.Substring(lastname.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                    filterContactName.AddCondition(Contact.lastnameAttribute, ConditionOperator.BeginsWith, lastname.Remove(lastname.Length - 1));
                else
                    filterContactName.AddCondition(Contact.lastnameAttribute, ConditionOperator.Equal, lastname);
            }
            if (!string.IsNullOrEmpty(email))
            {
                if (email.Substring(email.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                    filter.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.BeginsWith, email.Remove(email.Length - 1));
                else
                    filter.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.Equal, email);
            }
            if (!string.IsNullOrEmpty(phone))
            {
                filter.AddCondition(Contact.telephone1Attribute, ConditionOperator.Equal, phone.Trim());
                filter.AddCondition(Contact.telephone2Attribute, ConditionOperator.Equal, phone.Trim());
                filter.AddCondition(Contact.telephone3Attribute, ConditionOperator.Equal, phone.Trim());
            }
            if (filterContactName.Conditions.Count > 0)
            {
                filter.AddFilter(filterContactName);
            }
            queryExpression.Criteria = filter;
            EntityCollection res = this._organizationService.RetrieveMultiple(queryExpression);
            var query = from r in res.Entities
                        select new
                        {
                            ContactId = GetAttributeValue(r, Contact.contactidAttribute),
                            ContactTypeID = GetAttributeValue(r, Contact.cub_contacttypeidAttribute),
                            FullName = GetAttributeValue(r, Contact.fullnameAttribute),
                            FirstName = GetAttributeValue(r, Contact.firstnameAttribute),
                            LastName = GetAttributeValue(r, Contact.lastnameAttribute),
                            Email = GetAttributeValue(r, Contact.emailaddress1Attribute) ?? string.Empty,
                            Phone1 = GetAttributeValue(r, Contact.telephone1Attribute) ?? string.Empty,
                            Phone2 = GetAttributeValue(r, Contact.telephone2Attribute) ?? string.Empty,
                            Phone3 = GetAttributeValue(r, Contact.telephone3Attribute) ?? string.Empty,
                            Phone1Type = GetAttributeOptionSetText(r, Contact.cub_phone1typeAttribute, typeof(cub_phonetype)),
                            Phone2Type = GetAttributeOptionSetText(r, Contact.cub_phone2typeAttribute, typeof(cub_phonetype)),
                            Phone3Type = GetAttributeOptionSetText(r, Contact.cub_phone3typeAttribute, typeof(cub_phonetype)),
                            AccountId = GetAttributeValue(r, $"customer.{Account.accountidAttribute}"),
                            AccountName = GetAttributeValue(r, $"customer.{Account.nameAttribute}"),
                            AccountType = GetAttributeValue(r, $"customer.{Account.cub_accounttypeidAttribute}"),
                            City = GetAttributeValue(r, $"address.{cub_Address.cub_cityAttribute}"),
                            OneAccountId = GetAttributeValue(r, $"customer.{Account.cub_oneaccountidAttribute}")
                        };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }

        /// <summary>
        /// Search for Username based on email and/or phone plus possible other data
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="pin"></param>
        /// <param name="postalcode"></param>
        /// <returns></returns>
        private List<WSForgotUsernameResponse> SearchForForgotUserName(string firstname, string lastname, string email, string phone, string pin, string postalcode)
        {
            const string methodName = "SearchForForgotUserName";
            JoinOperator addJoin = new JoinOperator();
            JoinOperator credsJoin = new JoinOperator();
            CubLogger.Debug("Method Entered");
            List<WSForgotUsernameResponse> custContact = new List<WSForgotUsernameResponse>();

            try
            {
                //If no search parameters supplied, return null
                if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone))
                    return null;
                //trim all white spaces
                if (!string.IsNullOrEmpty(firstname)) firstname = firstname.Trim();
                if (!string.IsNullOrEmpty(lastname)) lastname = lastname.Trim();
                if (!string.IsNullOrEmpty(email)) email = email.Trim();
                if (!string.IsNullOrEmpty(phone)) phone = phone.Trim();
                if (!string.IsNullOrEmpty(pin)) pin = pin.Trim();
                if (!string.IsNullOrEmpty(postalcode)) postalcode = postalcode.Trim();

                if (string.IsNullOrEmpty(postalcode))
                    addJoin = JoinOperator.LeftOuter;
                else
                    addJoin = JoinOperator.Inner;

                if (string.IsNullOrEmpty(pin))
                    credsJoin = JoinOperator.LeftOuter;
                else
                    credsJoin = JoinOperator.Inner;

                var queryExpression = new QueryExpression(Contact.EntityLogicalName);
                queryExpression.NoLock = true;
                queryExpression.Distinct = true;
                queryExpression.ColumnSet = new ColumnSet(Contact.contactidAttribute,
                                                          Contact.parentcustomeridAttribute,
                                                          Contact.cub_contacttypeidAttribute,
                                                          Contact.fullnameAttribute,
                                                          Contact.firstnameAttribute,
                                                          Contact.lastnameAttribute,
                                                          Contact.emailaddress1Attribute,
                                                          Contact.telephone1Attribute,
                                                          Contact.telephone2Attribute,
                                                          Contact.telephone3Attribute,
                                                          Contact.cub_phone1typeAttribute,
                                                          Contact.cub_phone2typeAttribute,
                                                          Contact.cub_phone3typeAttribute);
                //Adding Customer (Account) to the resultset
                queryExpression.LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, Account.EntityLogicalName, Contact.parentcustomeridAttribute,
                                                                Account.accountidAttribute, JoinOperator.LeftOuter));
                queryExpression.LinkEntities[0].Columns.AddColumns(Account.accountidAttribute,
                                                                   Account.nameAttribute,
                                                                   Account.cub_accounttypeidAttribute);
                queryExpression.LinkEntities[0].EntityAlias = "customer";
                queryExpression.LinkEntities[0].Orders.Add(new OrderExpression(Account.nameAttribute, OrderType.Ascending));
                //Add account(customer) type to the resultset
                queryExpression.LinkEntities[0].LinkEntities.Add(new LinkEntity(Account.EntityLogicalName, cub_AccountType.EntityLogicalName,
                                                                                Account.cub_accounttypeidAttribute, cub_AccountType.cub_accounttypeidAttribute,
                                                                                JoinOperator.LeftOuter));
                queryExpression.LinkEntities[0].LinkEntities[0].Columns.AddColumns(cub_AccountType.cub_accountnameAttribute);
                queryExpression.LinkEntities[0].LinkEntities[0].EntityAlias = "accountType";

                #region Link Address to Contact
                //Adding Address city to the resultset
                queryExpression.LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_Address.EntityLogicalName, Contact.cub_addressidAttribute,
                                                                cub_Address.cub_addressidAttribute, addJoin));
                queryExpression.LinkEntities[1].Columns.AddColumns(cub_Address.cub_cityAttribute, cub_Address.cub_postalcodeidAttribute);

                queryExpression.LinkEntities[1].EntityAlias = "address";
                #endregion Link Address to Contact

                #region Link Postal Code to Address
                //Add postal code to the resultset
                queryExpression.LinkEntities[1].LinkEntities.Add(new LinkEntity(cub_Address.EntityLogicalName, cub_ZipOrPostalCode.EntityLogicalName,
                    cub_Address.cub_postalcodeidAttribute, cub_ZipOrPostalCode.cub_ziporpostalcodeidAttribute, addJoin));
                queryExpression.LinkEntities[1].LinkEntities[0].Columns.AddColumns(cub_ZipOrPostalCode.cub_codeAttribute);

                FilterExpression postalCodeFilter = new FilterExpression(LogicalOperator.And);

                if (!string.IsNullOrEmpty(postalcode))
                {
                    postalCodeFilter.AddCondition(cub_ZipOrPostalCode.cub_codeAttribute, ConditionOperator.Equal, postalcode);
                }

                if (postalCodeFilter.Conditions.Count > 0) queryExpression.LinkEntities[1].LinkEntities[0].LinkCriteria = postalCodeFilter;

                queryExpression.LinkEntities[1].LinkEntities[0].EntityAlias = "postalCode";
                #endregion Link Postal Code to Address

                #region Link Credentials to Contact
                queryExpression.LinkEntities.Add(new LinkEntity(Contact.EntityLogicalName, cub_Credential.EntityLogicalName, Contact.contactidAttribute, cub_Credential.cub_contactidAttribute, credsJoin));
                queryExpression.LinkEntities[2].Columns.AddColumns(cub_Credential.cub_usernameAttribute, cub_Credential.cub_pinAttribute, cub_Credential.statuscodeAttribute, cub_Credential.cub_credentialidAttribute);

                FilterExpression pinFilter = new FilterExpression(LogicalOperator.And);

                if (!string.IsNullOrEmpty(pin))
                {
                    pinFilter.AddCondition(cub_Credential.cub_pinAttribute, ConditionOperator.Equal, pin);
                }

                if (pinFilter.Conditions.Count > 0) queryExpression.LinkEntities[2].LinkCriteria = pinFilter;
                queryExpression.LinkEntities[2].EntityAlias = "contactCreds";
                #endregion Link Credentials to Contact 

                //Adding sorting  - account name, contact full name
                queryExpression.Orders.Add(new OrderExpression(Contact.fullnameAttribute, OrderType.Ascending));
                queryExpression.Orders.Add(new OrderExpression(Contact.emailaddress1Attribute, OrderType.Ascending));
                FilterExpression filter = new FilterExpression(LogicalOperator.And);
                FilterExpression filterMain = new FilterExpression(LogicalOperator.And);
                if (!string.IsNullOrEmpty(firstname))
                {
                    if (firstname.Substring(firstname.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                        filterMain.AddCondition(Contact.firstnameAttribute, ConditionOperator.BeginsWith, firstname.Remove(firstname.Length - 1));
                    else
                        filterMain.AddCondition(Contact.firstnameAttribute, ConditionOperator.Equal, firstname);
                }
                if (!string.IsNullOrEmpty(lastname))
                {
                    if (lastname.Substring(lastname.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                        filterMain.AddCondition(Contact.lastnameAttribute, ConditionOperator.BeginsWith, lastname.Remove(lastname.Length - 1));
                    else
                        filterMain.AddCondition(Contact.lastnameAttribute, ConditionOperator.Equal, lastname);
                }
                if (!string.IsNullOrEmpty(email))
                {
                    if (email.Substring(email.Length - 1) == CUBConstants.Common.SearchWildCharacter)
                        filterMain.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.BeginsWith, email.Remove(email.Length - 1));
                    else
                        filterMain.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.Equal, email);
                }

                FilterExpression filterPhone = new FilterExpression(LogicalOperator.Or);
                if (!string.IsNullOrEmpty(phone))
                {
                    filterPhone.AddCondition(Contact.telephone1Attribute, ConditionOperator.Equal, phone.Trim());
                    filterPhone.AddCondition(Contact.telephone2Attribute, ConditionOperator.Equal, phone.Trim());
                    filterPhone.AddCondition(Contact.telephone3Attribute, ConditionOperator.Equal, phone.Trim());
                }

                if (filterMain.Conditions.Count > 0)
                    filter.AddFilter(filterMain);
                if (filterPhone.Conditions.Count > 0)
                    filter.AddFilter(filterPhone);
                queryExpression.Criteria = filter;
                EntityCollection res = this._organizationService.RetrieveMultiple(queryExpression);
                custContact = (from r in res.Entities
                               select new WSForgotUsernameResponse
                               {
                                   contactId = GetAttributeValue(r, Contact.contactidAttribute).ToString(),
                                   customerId = ReturnStringValue(GetAttributeValue(r, Contact.parentcustomeridAttribute), "ID"),
                                   credentialId = ReturnStringValue(GetAttributeValue(r, "contactCreds." + cub_Credential.cub_credentialidAttribute), "ID"),
                                   postalCode = ReturnStringValue(GetAttributeValue(r, "postalCode." + cub_ZipOrPostalCode.cub_codeAttribute)),
                                   firstName = ReturnStringValue(GetAttributeValue(r, Contact.firstnameAttribute)),
                                   lastName = ReturnStringValue(GetAttributeValue(r, Contact.lastnameAttribute)),
                                   email = ReturnStringValue(GetAttributeValue(r, Contact.emailaddress1Attribute)),
                                   phone = ReturnStringValue(GetAttributeValue(r, Contact.telephone1Attribute)),
                                   pin = ReturnStringValue(GetAttributeValue(r, "contactCreds." + cub_Credential.cub_pinAttribute)),
                                   username = ReturnStringValue(GetAttributeValue(r, "contactCreds." + cub_Credential.cub_usernameAttribute)),
                                   credStatus = GetAttributeValue(r, "contactCreds." + cub_Credential.statuscodeAttribute) == null ? cub_credential_statuscode.Inactive : ConvertOptionSetValueToCredentialStatusCode(GetAttributeValue(r, "contactCreds." + cub_Credential.statuscodeAttribute))
                               }).ToList();

            }
            catch (Exception ex)
            {
                CubLogger.Error("Exception in " + methodName, ex);
                throw ex;
            }

            return custContact;
        }

        /// <summary>
        /// Convert OptionSet Value To Credential StatusCode
        /// </summary>
        /// <param name="v">OptionSet Value</param>
        /// <returns>Credentials Status Code</returns>
        private cub_credential_statuscode ConvertOptionSetValueToCredentialStatusCode(object v)
        {
            OptionSetValue osv = (OptionSetValue)v;
            cub_credential_statuscode ret_code = (cub_credential_statuscode)osv.Value;
            return ret_code;
        }

        /// <summary>
        /// Forgot Username Lookup, send email if found and if multiple records found then return items that may help define a unique record
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="pin"></param>
        /// <param name="postalcode"></param>
        /// <returns></returns>
        public CubResponse<WSForgotUsernameResponse> ForgotUsername(string firstname, string lastname, string email, string phone, string pin, string postalcode)
        {
            string methodName = "ForgotUsername";
            CubResponse<WSForgotUsernameResponse> response = new CubResponse<WSForgotUsernameResponse>();

            try
            {
                List<WSForgotUsernameResponse> list = SearchForForgotUserName(firstname, lastname, email, phone, pin, postalcode);

                if (list == null || list.Count == 0)
                {
                    response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT));
                }
                else
                {
                    if (list.Count == 1)
                    {
                        Guid customerGuid;
                        Guid contactGuid;

                        if (list[0].credStatus == cub_credential_statuscode.Inactive)
                            response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_INACTIVE, CUBConstants.Error.MSG_LOGIN_INACTIVE));
                        if (list[0].credStatus == cub_credential_statuscode.Locked)
                            response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_LOCKED, CUBConstants.Error.MSG_LOGIN_LOCKED));
                        if (list[0].credStatus == cub_credential_statuscode.PendingActivation)
                            response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_PENDING_ACTIVATION, CUBConstants.Error.MSG_LOGIN_PENDING_ACTIVATION));

                        if (!Guid.TryParse(list[0].customerId, out customerGuid))
                            response.Errors.Add(new WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Customer not found"));
                        if (!Guid.TryParse(list[0].contactId, out contactGuid))
                            response.Errors.Add(new WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, "Contact not found"));

                        if (response.Success)
                        {
                            //send CustomerServiceApi Report Forgotten Username (POST) with username
                            ReportUsername(customerGuid, contactGuid, list[0].username);
                        }
                    }
                    else
                    {
                        // add items to fieldname where populated
                        string fieldname = null;

                        FindPopulatedFields(list, firstname, lastname, email, phone, pin, postalcode, out fieldname);
                        response.Errors.Add(new WSMSDFault(fieldname, CUBConstants.Error.ERR_ACCNT_NOT_UNIQUE, CUBConstants.Error.MSG_UNABLE_FIND_UNIQUE_ACCOUNT));
                    }
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error("Exception in " + methodName, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Find populated fields when searching for username
        /// </summary>
        /// <param name="list"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="pin"></param>
        /// <param name="postalcode"></param>
        /// <param name="fieldname"></param>
        private void FindPopulatedFields(List<WSForgotUsernameResponse> list, string firstname, string lastname, string email, string phone, string pin, string postalcode, out string fieldname)
        {
            bool firstnameInList = false;
            bool lastnameInList = false;
            bool emailInList = false;
            bool phoneInList = false;
            bool pinInList = false;
            bool postalcodeInList = false;

            fieldname = string.Empty;

            foreach (WSForgotUsernameResponse r in list)
            {
                if (!string.IsNullOrEmpty(r.firstName))
                {
                    firstnameInList = true;
                }
                if (!string.IsNullOrEmpty(r.lastName))
                {
                    lastnameInList = true;
                }
                if (!string.IsNullOrEmpty(r.email))
                {
                    emailInList = true;
                }
                if (!string.IsNullOrEmpty(r.phone))
                {
                    phoneInList = true;
                }
                if (!string.IsNullOrEmpty(r.pin))
                {
                    pinInList = true;
                }
                if (!string.IsNullOrEmpty(r.postalCode))
                {
                    postalcodeInList = true;
                }
            }

            if (string.IsNullOrEmpty(email) && emailInList) fieldname = "Email";
            if (string.IsNullOrEmpty(phone) && phoneInList)
            {
                if (string.IsNullOrEmpty(fieldname))
                {
                    fieldname = "Phone";
                }
                else
                {
                    fieldname += ", Phone";
                }
            }
            if (string.IsNullOrEmpty(firstname) && firstnameInList)
            {
                if (string.IsNullOrEmpty(fieldname))
                {
                    fieldname = "FirstName";
                }
                else
                {
                    fieldname += ", FirstName";
                }
            }
            if (string.IsNullOrEmpty(lastname) && lastnameInList)
            {
                if (string.IsNullOrEmpty(fieldname))
                {
                    fieldname = "LastName";
                }
                else
                {
                    fieldname += ", LastName";
                }
            }
            if (string.IsNullOrEmpty(pin) && pinInList)
            {
                if (string.IsNullOrEmpty(fieldname))
                {
                    fieldname = "PIN";
                }
                else
                {
                    fieldname += ", PIN";
                }
            }
            if (string.IsNullOrEmpty(postalcode) && postalcodeInList)
            {
                if (string.IsNullOrEmpty(fieldname))
                {
                    fieldname = "PostalCode";
                }
                else
                {
                    fieldname += ", PostalCode";
                }
            }
        }

        /// <summary>
        /// Seach for a contact using the First Name, Last Name, Email and Phone
        /// The search rules are:
        /// First Name AND Last Name
        /// OR Email
        /// OR Phone (Phone will be compared with the three MSD phone fields: telephone1, telephone2 and telephone3)
        /// * All search is full match (Equals) not partial or wilcards
        /// * The name search is only executed with both first name and last name is provided
        /// </summary>
        /// <param name="firstName">First Name</param>
        /// <param name="lastName">Last Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">Phone</param>
        /// <returns>JSON with the list of found records or empty if not found.
        /// The returned field list is: ContactId, FirstName, LastName, EMailAddress1, Telephone1, Telephone2, Telephone3
        /// </returns>
        public virtual string SeacrhContact(string firstName, string lastName, string email, string phone)
        {
            if (string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName) &&
                string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone))
                return "{}";
            if (!string.IsNullOrEmpty(firstName)) firstName = firstName.Trim();
            if (!string.IsNullOrEmpty(lastName)) lastName = lastName.Trim();
            if (!string.IsNullOrEmpty(email)) email = email.Trim();
            if (!string.IsNullOrEmpty(phone)) phone = phone.Trim();
            var queryExpression = new QueryExpression(Contact.EntityLogicalName);
            queryExpression.NoLock = true;
            queryExpression.Distinct = true;
            queryExpression.ColumnSet = new ColumnSet(Contact.contactidAttribute,
                                                      Contact.fullnameAttribute,
                                                      Contact.firstnameAttribute,
                                                      Contact.lastnameAttribute,
                                                      Contact.cub_contacttypeidAttribute,
                                                      Contact.emailaddress1Attribute,
                                                      Contact.telephone1Attribute,
                                                      Contact.telephone2Attribute,
                                                      Contact.telephone3Attribute,
                                                      Contact.cub_phone1typeAttribute,
                                                      Contact.cub_phone2typeAttribute,
                                                      Contact.cub_phone3typeAttribute);
            queryExpression.Orders.Add(new OrderExpression(Contact.fullnameAttribute, OrderType.Ascending));
            FilterExpression filter = new FilterExpression(LogicalOperator.Or);
            //Name filter
            FilterExpression nameFilter = new FilterExpression(LogicalOperator.And);
            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            {
                nameFilter.AddCondition(Contact.firstnameAttribute, ConditionOperator.Equal, firstName);
                nameFilter.AddCondition(Contact.lastnameAttribute, ConditionOperator.Equal, lastName);
            }
            // Email filter
            FilterExpression emailFilter = new FilterExpression();
            if (!string.IsNullOrEmpty(email))
                emailFilter.AddCondition(Contact.emailaddress1Attribute, ConditionOperator.Equal, email);
            // Phone filter
            FilterExpression phoneFilter = new FilterExpression(LogicalOperator.Or);
            if (!string.IsNullOrEmpty(phone))
            {
                phoneFilter.AddCondition(Contact.telephone1Attribute, ConditionOperator.Equal, phone.Trim());
                phoneFilter.AddCondition(Contact.telephone2Attribute, ConditionOperator.Equal, phone.Trim());
                phoneFilter.AddCondition(Contact.telephone3Attribute, ConditionOperator.Equal, phone.Trim());
            }
            // Adding the filters
            if (nameFilter.Conditions.Count > 0)
                filter.AddFilter(nameFilter);
            if (emailFilter.Conditions.Count > 0)
                filter.AddFilter(emailFilter);
            if (phoneFilter.Conditions.Count > 0)
                filter.AddFilter(phoneFilter);
            // No condition. This is protection when we have only First Name or Last Name and no other condition
            // We should always search for First Name and Last Name
            if (filter.Filters.Count == 0)
            {
                return "{}";
            }
            queryExpression.Criteria = filter;
            EntityCollection res = this._organizationService.RetrieveMultiple(queryExpression);
            List<Contact> contacts = res.Entities.Select(c => c.ToEntity<Contact>()).ToList();
            var reducedContact = from c in contacts
                                 select new
                                 {
                                     ContactId = c.ContactId.ToString(),
                                     ContactTypeID = c.cub_ContactTypeId,
                                     FullName = c.FullName,
                                     FirstName = c.FirstName,
                                     LastName = c.LastName,
                                     Email = c.EMailAddress1,
                                     Phone1 = c.Telephone1,
                                     Phone2 = c.Telephone2,
                                     Phone3 = c.Telephone3,
                                     Phone1Type = c.cub_Phone1Type,
                                     Phone2Type = c.cub_Phone2Type,
                                     Phone3Type = c.cub_Phone3Type,
                                 };
            return Newtonsoft.Json.JsonConvert.SerializeObject(reducedContact);
        }

        /// <summary>
        /// Update Gmail Email
        /// </summary>
        /// <param name="contact">Contact</param>
        public void UpdateGmailEmail(Entity contact)
        {
            // todo: if more than 1 email address is added to the contact form, add loop below
            //if (contact.Contains(Contact.emailaddress1Attribute) && contact.Attributes[Contact.emailaddress1Attribute] != null)
            //    emailAddress = contact.Attributes[Contact.emailaddress1Attribute].ToString();
            //else if (contact.Contains(Contact.emailaddress2Attribute) && contact.Attributes[Contact.emailaddress2Attribute] != null)
            //    emailAddress = contact.Attributes[Contact.emailaddress2Attribute].ToString();
            //else if (contact.Contains(Contact.emailaddress3Attribute) && contact.Attributes[Contact.emailaddress3Attribute] != null)
            //    emailAddress = contact.Attributes[Contact.emailaddress3Attribute].ToString();

            if (contact.Contains(Contact.emailaddress1Attribute) && contact.Attributes[Contact.emailaddress1Attribute] != null)
            {
                string emailAddress = contact.Attributes[Contact.emailaddress1Attribute].ToString();
                EmailLogic emailLogic = new EmailLogic(_organizationService, _executionContext);

                // update gmail address in contact if gmail - this could be a separate plugin on pre-operation
                string gmailAddress = null;

                //If Gmail -- 'clean up' prior to validation
                if (emailAddress != "" && emailAddress.Contains(CUBConstants.Common.gmaildotcom))
                {
                    gmailAddress = emailLogic.CleanUpGmailAddress(emailAddress);

                    if (!contact.Attributes.Contains(Contact.cub_emailaddress1gmailAttribute))
                    {
                        contact.Attributes.Add(Contact.cub_emailaddress1gmailAttribute, gmailAddress);
                    }
                    else
                    {
                        contact.Attributes[Contact.cub_emailaddress1gmailAttribute] = gmailAddress;
                    }
                }
            }
        }

        /// <summary>
        /// Update Telephone
        /// </summary>
        /// <param name="contact">Contact</param>
        /// <param name="telephoneField">Telephone field</param>
        public void UpdateTelephone(Entity contact, string telephoneField)
        {
            if (contact.Contains(telephoneField) && contact.Attributes[telephoneField] != null)
            {
                string phone = contact.Attributes[telephoneField].ToString();
                var ret = UnFormatPhoneNumber(phone);
                contact.Attributes[telephoneField] = ret;
                CubLogger.DebugFormat("Contact phone updated: Contact ID = {0}; New Phone = {1}", contact.Attributes[Contact.contactidAttribute], contact.Attributes[telephoneField]);
            }
        }

        /// <summary>
        /// Unformat phone number
        /// </summary>
        /// <param name="phoneNumber">phone number</param>
        /// <returns>phone number unformatted</returns>
        public string UnFormatPhoneNumber(string phoneNumber)
        {
            // Remove any special characters
            var sTmp = new string(phoneNumber.Where(char.IsDigit).ToArray());
            return sTmp;
        }

        /// <summary>
        /// Validates Username, verification Token and Password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="verificationToken"></param>
        /// <param name="new_password"></param>
        /// <param name="tokenType"></param>
        /// <returns></returns>
        public CubResponse<WSForgotUsernameResponse> ValidateToken(string username, string verificationToken, string new_password, cub_verificationtokentype tokenType)
        {
            CubLogger.Debug("Method Entered");
            CubResponse<WSForgotUsernameResponse> forgotUsernameResponse = new CubResponse<WSForgotUsernameResponse>();

            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    forgotUsernameResponse.Errors.Add(new WSMSDFault(cub_Credential.cub_usernameAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                }
                else
                {
                    CubLogger.Debug($"Validating username: {username}");

                    forgotUsernameResponse = GetCustomerCreds(username);

                    CubLogger.Debug(Newtonsoft.Json.JsonConvert.SerializeObject(forgotUsernameResponse));

                    if (forgotUsernameResponse.Success)
                    {
                        if (string.IsNullOrEmpty(verificationToken))
                            forgotUsernameResponse.Errors.Add(new WSMSDFault(cub_VerificationToken.cub_verificationtokenidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                        else if (string.IsNullOrEmpty(forgotUsernameResponse.ResponseObject.credentialId))
                            forgotUsernameResponse.Errors.Add(new WSMSDFault(cub_Credential.cub_credentialidAttribute, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.VerificationToken.Error.ERR_VALIDATE_VT_CRED_ID_MISSING));
                        else
                        {
                            CubLogger.Debug($"Check if no. of login attemps is already reached for cred id = {forgotUsernameResponse.ResponseObject.credentialId}");

                            //check to see if the number of failed login attempts has already been reached
                            if ((tokenType != cub_verificationtokentype.UnlockAccount) && (IsFailedLoginAttemptsLimitReached(forgotUsernameResponse.ResponseObject.credentialId, false)))
                            {
                                forgotUsernameResponse.ResponseObject.credStatus = cub_credential_statuscode.Locked;

                                forgotUsernameResponse.Errors.Add(new WSMSDFault(cub_Credential.cub_credentialidAttribute,
                                    CUBConstants.Error.ERR_ACCNT_LOCKED, CUBConstants.Error.MSG_LOGIN_LOCKED));
                            }

                            if ((tokenType != cub_verificationtokentype.Password) && (forgotUsernameResponse.ResponseObject.credStatus == cub_credential_statuscode.PendingActivation))
                            {
                                forgotUsernameResponse.Errors.Add(new WSMSDFault(cub_Credential.cub_credentialidAttribute,
                                    CUBConstants.Error.ERR_ACCNT_PENDING_ACTIVATION, CUBConstants.Error.MSG_LOGIN_PENDING_ACTIVATION));
                            }

                            if (forgotUsernameResponse.Success)
                            {
                                CubLogger.Debug($"Validating verificationToken: {verificationToken} for credential Id: {forgotUsernameResponse.ResponseObject.credentialId}");

                                CubResponse<WSForgotUsernameResponse> validateVerTokenResp = ValidateVerificationToken(forgotUsernameResponse.ResponseObject.credentialId, verificationToken, tokenType);

                                if (validateVerTokenResp.Success)
                                {
                                    forgotUsernameResponse.ResponseObject.verificationtokenId = validateVerTokenResp.ResponseObject.verificationtokenId;

                                    if (!string.IsNullOrEmpty(new_password))
                                    {
                                        CubLogger.Debug("Validating new password");

                                        //Validate/Change Password
                                        // note: the function expects unencrypted existing password
                                        CubResponse<WSPasswordPrevalidateResults> changePassResp =
                                            ChangePassword(Guid.Parse(forgotUsernameResponse.ResponseObject.customerId),
                                                    Guid.Parse(forgotUsernameResponse.ResponseObject.contactId),
                                                    UtilityLogic.DecryptTripleDES(forgotUsernameResponse.ResponseObject.existingPassword), new_password);

                                        if (!changePassResp.Success)
                                        {
                                            forgotUsernameResponse.Errors = changePassResp.Errors;
                                            CubLogger.Error(changePassResp.Errors?[0].errorMessage);
                                        }
                                        else
                                        {
                                            List<WSError> redeemAndResetErrors = RedeemTokenAndResetFailedAttempts(Guid.Parse(forgotUsernameResponse.ResponseObject.verificationtokenId),
                                                Guid.Parse(forgotUsernameResponse.ResponseObject.credentialId));

                                            //NOTE: not sending redeemAndResetErrors (if any)
                                            //back at this time since everything else went through unless the token type is unlock account.
                                            if (tokenType == cub_verificationtokentype.UnlockAccount)
                                            {
                                                if (redeemAndResetErrors.Count > 0)
                                                {
                                                    //since we are dealing with an unlock token and we are not able to reset the failed login attempts, 
                                                    //meaning the account is still locked, then pass back those errors
                                                    foreach (WSError err in redeemAndResetErrors)
                                                    {
                                                        WSMSDFault fault = new WSMSDFault("MSD", err.key, err.message);
                                                        forgotUsernameResponse.Errors.Add(fault);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        List<WSError> redeemAndResetErrors = RedeemTokenAndResetFailedAttempts(Guid.Parse(forgotUsernameResponse.ResponseObject.verificationtokenId),
                                            Guid.Parse(forgotUsernameResponse.ResponseObject.credentialId));

                                        if (tokenType == cub_verificationtokentype.UnlockAccount)
                                        {
                                            if (redeemAndResetErrors.Count > 0)
                                            {
                                                //since we are dealing with an unlock token and we are not able to reset the failed login attempts, 
                                                //meaning the account is still locked, then pass back those errors
                                                foreach (WSError err in redeemAndResetErrors)
                                                {
                                                    WSMSDFault fault = new WSMSDFault("MSD", err.key, err.message);
                                                    forgotUsernameResponse.Errors.Add(fault);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    forgotUsernameResponse.Errors = validateVerTokenResp.Errors;

                                    if (string.Compare(validateVerTokenResp.Errors[0].errorKey, CUBConstants.Error.ERR_ACCNT_LOCKED) == 0)
                                    {
                                        Guid customer;
                                        Guid contact;
                                        Guid.TryParse(forgotUsernameResponse.ResponseObject.customerId, out customer);
                                        Guid.TryParse(forgotUsernameResponse.ResponseObject.contactId, out contact);

                                        RequestSendingCustomerLockoutNotification(customer, contact);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                forgotUsernameResponse.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            CubLogger.Debug("Method Exit");
            return forgotUsernameResponse;
        }

        /// <summary>
        /// Validate Mobile Token
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="verificationToken">Verification Token</param>
        /// <returns>CubResponse<WSMobileToken></returns>
        public CubResponse<WSMobileToken> ValidateMobileToken(Guid customerId, Guid contactId, string verificationToken)
        {
            CubLogger.Debug("Method Entered");
            CubResponse<WSMobileToken> mobileTokenResponse = new CubResponse<WSMobileToken>();

            try
            {
                if (customerId == Guid.Empty)
                {
                    mobileTokenResponse.Errors.Add(new WSMSDFault("customerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return mobileTokenResponse;
                }

                if (contactId == Guid.Empty)
                {
                    mobileTokenResponse.Errors.Add(new WSMSDFault("contactId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return mobileTokenResponse;
                }

                if (string.IsNullOrEmpty(verificationToken))
                {
                    mobileTokenResponse.Errors.Add(new WSMSDFault("verificationToken", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return mobileTokenResponse;
                }

                // Get Customer Creds
                CubResponse<WSCustomerCredential> credResponse = GetCustomerCreds(customerId, contactId);

                if (!credResponse.Success)
                {
                    mobileTokenResponse.Errors.AddRange(credResponse.Errors);
                    return mobileTokenResponse;
                }



                CubLogger.Info($"Validating verificationToken: {verificationToken} for credential Id: {credResponse.ResponseObject.credentialId}");

                CubResponse<WSForgotUsernameResponse> validateVerTokenResp = ValidateVerificationToken(credResponse.ResponseObject.credentialId, verificationToken, cub_verificationtokentype.Mobile);

                if (validateVerTokenResp.Success)
                {
                    // return the mobile token from the verificaiton token table
                    mobileTokenResponse.ResponseObject.mobileToken = validateVerTokenResp.ResponseObject.mobileDeviceToken;

                    List<WSError> redeemAndResetErrors = RedeemTokenAndResetFailedAttempts(Guid.Parse(validateVerTokenResp.ResponseObject.verificationtokenId),
                                                                                            Guid.Parse(credResponse.ResponseObject.credentialId));

                    if (redeemAndResetErrors.Count > 0)
                    {
                        //since we are dealing with an unlock token and we are not able to reset the failed login attempts, 
                        //meaning the account is still locked, then pass back those errors
                        foreach (WSError err in redeemAndResetErrors)
                        {
                            mobileTokenResponse.Errors.Add(new WSMSDFault("MSD", err.key, err.message));
                        }
                    }
                }
                else
                {
                    mobileTokenResponse.Errors.AddRange(validateVerTokenResp.Errors);
                    return mobileTokenResponse;
                }



            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                mobileTokenResponse.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            CubLogger.Debug("Method Exit");
            return mobileTokenResponse;
        }

        /// <summary>
        /// Redeem Token And Reset Failed Attempts
        /// </summary>
        /// <param name="verificationtokenId">Verification token ID</param>
        /// <param name="credentialId">Credential ID</param>
        /// <returns>List<WSError></returns>
        private List<WSError> RedeemTokenAndResetFailedAttempts(Guid verificationtokenId, Guid credentialId)
        {
            // mark token redeemed cub_RedemptionIndicator.Value = true
            List<WSError> errMarkVeriTokenRedeemed = MarkVerificationTokenRedeemed(verificationtokenId);

            // reset failed login attempts to zero
            List<WSError> resetFailedLoginAttempts = ResetFailedLoginAttempts(credentialId);

            //at this point only return errors from resetting the failed login attempts
            //resetFailedLoginAttempts.AddRange(errMarkVeriTokenRedeemed);

            return resetFailedLoginAttempts;
        }

        /// <summary>
        /// External call to reset the credentials to unlock an account
        /// </summary>
        /// <param name="credentialId"></param>
        /// <returns></returns>
        public CubResponse<WSEmptyClass> CallResetFailedLoginAttempts(string credentialId, string calling_from)
        {
            CubResponse<WSEmptyClass> response = new CubResponse<WSEmptyClass>();

            try
            {
                List<WSError> list = new List<WSError>();

                Guid credentialGuid;

                if (!Guid.TryParse(credentialId, out credentialGuid))
                    list.Add(new WSError { key = CUBConstants.Error.ERR_GENERAL_EXCEPTION, message = CUBConstants.Error.MSG_GENERAL_RECORD_ID_NOT_A_VALID_RECORD_ID });

                if (list.Count == 0) list.AddRange(ResetFailedLoginAttempts(credentialGuid, calling_from));

                foreach (WSError e in list)
                {
                    response.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, e.key, e.message));
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Reset Failed Login Attempts
        /// </summary>
        /// <param name="credentialId">Credentials OD</param>
        /// <param name="from">From</param>
        /// <returns>List<WSError></returns>
        private List<WSError> ResetFailedLoginAttempts(Guid credentialId, string from = null)
        {
            List<WSError> resp = new List<WSError>();

            try
            {
                cub_Credential creds = new cub_Credential();
                creds.cub_CredentialId = credentialId;
                creds.cub_FailedLoginAttempts = 0;
                creds.cub_LockoutDtm = null;
                switch (from)
                {
                    case CUBConstants.GlobalCallingFromConstants.CALLING_FROM_IVR:
                        creds.cub_LastIvrLoginDtm = DateTime.UtcNow;
                        break;
                    case CUBConstants.GlobalCallingFromConstants.CALLING_FROM_MOBILE:
                        creds.cub_LastMobileLoginDtm = DateTime.UtcNow;
                        break;
                    case CUBConstants.GlobalCallingFromConstants.CALLING_FROM_WEB:
                        creds.cub_LastWebLoginDtm = DateTime.UtcNow;
                        break;
                }
                SetState2(credentialId, cub_Credential.EntityLogicalName, (int)cub_CredentialState.Active, (int)cub_credential_statuscode.Active);
                _organizationService.Update(creds);
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                resp.Add(new WSError() { key = CUBConstants.Error.ERR_GENERAL_EXCEPTION, message = ex.Message });
            }

            return resp;
        }

        /// <summary>
        /// MarkVerificationTokenRedeemed
        /// </summary>
        /// <param name="verificationtokenId">Verification ID</param>
        /// <returns>List<WSError></returns>
        private List<WSError> MarkVerificationTokenRedeemed(Guid verificationtokenId)
        {
            List<WSError> resp = new List<WSError>();
            try
            {
                cub_VerificationToken vt = new cub_VerificationToken();
                vt.Id = verificationtokenId;
                vt.cub_RedemptionIndicator = true;
                _organizationService.Update(vt);
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                resp.Add(new WSError() { key = CUBConstants.Error.ERR_GENERAL_EXCEPTION, message = ex.Message });
            }

            return resp;
        }

        /// <summary>
        /// Validate Verification Token
        /// </summary>
        /// <param name="credentialId">Credential ID</param>
        /// <param name="verificationToken">Verification Token</param>
        /// <param name="tokenType">Token Type</param>
        /// <returns>CubResponse<WSForgotUsernameResponse></returns>
        private CubResponse<WSForgotUsernameResponse> ValidateVerificationToken(
            string credentialId, string verificationToken, cub_verificationtokentype tokenType)
        {
            CubResponse<WSForgotUsernameResponse> resp = new CubResponse<WSForgotUsernameResponse>();

            try
            {
                #region validate input
                //these checks are done in case we make this method public and call from other places
                if (string.IsNullOrEmpty(credentialId))
                    resp.Errors.Add(new WSMSDFault("credentialId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.VerificationToken.Error.ERR_VALIDATE_VT_CRED_ID_MISSING));
                if (string.IsNullOrEmpty(verificationToken))
                    resp.Errors.Add(new WSMSDFault("verificationToken", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.VerificationToken.Error.ERR_VALIDATE_VT_CRED_ID_MISSING));
                #endregion

                var result = (from vt in CubOrgSvc.cub_VerificationTokenSet
                              where vt.cub_CredentialsId.Id == Guid.Parse(credentialId)
                                && vt.cub_Name.Equals(UtilityLogic.EncryptTripleDES(verificationToken))
                                 && (cub_verificationtokentype)vt.cub_TokenType == tokenType
                              select vt).ToList();

                if (result.Count > 0)
                {
                    resp.ResponseObject.verificationtokenId = result[0].cub_VerificationTokenId.ToString();
                    resp.ResponseObject.mobileDeviceToken = UtilityLogic.DecryptTripleDES(result[0].cub_MobileDeviceToken);
                    if (result[0].cub_TokenExpirationDate.HasValue && result[0].cub_TokenExpirationDate.Value.CompareTo(DateTime.Now) < 0)
                        resp.Errors.Add(new WSMSDFault("cub_TokenExpirationDate", CUBConstants.VerificationToken.Error.ERR_SECURITY_VT_EXPIRED, CUBConstants.VerificationToken.Error.ERR_TOKEN_EXPIRED_MSG));
                    else if (result[0].cub_RedemptionIndicator.HasValue && result[0].cub_RedemptionIndicator.Value)
                        resp.Errors.Add(new WSMSDFault("cub_RedemptionIndicator", CUBConstants.VerificationToken.Error.ERR_TOKEN_INVALID, CUBConstants.VerificationToken.Error.ERR_TOKEN_INVALID_MSG));
                }
                else
                {
                    //Increment the count
                    if (IsFailedLoginAttemptsLimitReached(credentialId, true))
                    {
                        //since we reached the login limit and locked the account, clear previous errors and send this
                        resp.Errors.Clear();
                        resp.Errors.Add(new WSMSDFault(credentialId,
                            CUBConstants.Error.ERR_ACCNT_LOCKED, CUBConstants.Error.MSG_LOGIN_LOCKED));
                    }
                    else
                    {
                        resp.Errors.Add(new WSMSDFault(cub_VerificationToken.cub_verificationtokenidAttribute,
                            CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND,
                            $"cub_VerificationToken not found for credential id = {credentialId} and tokenType = {tokenType}"));
                    }
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                resp.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return resp;
        }

        /// <summary>
        /// External call to find out if the number of failed login attempts has been reached OR to increment the attempts and then check, locking if necessary
        /// </summary>
        /// <param name="credentialId"></param>
        /// <param name="incrementTheFailedLoginAttempts"></param>
        /// <returns></returns>
        public CubResponse<WSEmptyClass> CallIsFailedLoginAttemptsLimitReached(string credentialId, bool incrementTheFailedLoginAttempts = true)
        {
            CubResponse<WSEmptyClass> response = new CubResponse<WSEmptyClass>();

            try
            {
                if (IsFailedLoginAttemptsLimitReached(credentialId, incrementTheFailedLoginAttempts))
                {
                    response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_LOCKED, CUBConstants.Error.MSG_LOGIN_LOCKED));
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Checks if login attempts have reached the limit and lock the record if it has
        /// </summary>
        /// <param name="credentialId">A valid credential Id</param>
        /// <param name="incrementFailedLoginAttempts">If true the cub_FailedLoginAttempts will be increment AND another check will be done to make sure we didn't exceed the limit </param>
        /// <returns>True if limit has reach otherwise false</returns>
        private bool IsFailedLoginAttemptsLimitReached(string credentialId, bool? incrementFailedLoginAttempts)
        {
            bool limitReached = true;

            var cred = (from c in CubOrgSvc.cub_CredentialSet
                        where c.Id == Guid.Parse(credentialId)
                        select c).Single();

            if (!cred.cub_FailedLoginAttempts.HasValue ||
                (cred.cub_FailedLoginAttempts.HasValue && cred.cub_FailedLoginAttempts.Value < GlobalsCache.AppInfoMaxFailedLoginAttempts(_organizationService)))
            {
                limitReached = false;
                #region Increment failed login count if requested
                if (incrementFailedLoginAttempts.HasValue && incrementFailedLoginAttempts.Value)
                {
                    cub_Credential updateCred = new cub_Credential();
                    updateCred.cub_CredentialId = Guid.Parse(credentialId);
                    updateCred.cub_FailedLoginAttempts = ++cred.cub_FailedLoginAttempts ?? 1;
                    _organizationService.Update(updateCred);
                    //NOTE: recursive call, this time to check if the limis is reached after incrementing the value
                    // if it has, the status should be changed
                    limitReached = IsFailedLoginAttemptsLimitReached(credentialId, null);
                }
                #endregion

            }
            else // lock account
            {
                if (UtilityLogic.ConvertOptionSetObjectToEnum<cub_credential_statuscode>(cred.statuscode) != cub_credential_statuscode.Locked)
                {
                    LockCredentialRecord(credentialId);
                }
            }

            return limitReached;
        }

        /// <summary>
        /// Lock Credential Record 
        /// </summary>
        /// <param name="credentialId">Credential ID</param>
        private void LockCredentialRecord(string credentialId)
        {
            cub_Credential cred = new cub_Credential();
            cred.cub_CredentialId = Guid.Parse(credentialId);
            SetState2(Guid.Parse(credentialId), cub_Credential.EntityLogicalName, (int)cub_CredentialState.Inactive, (int)cub_credential_statuscode.Locked);
            cred.cub_LockoutDtm = DateTime.UtcNow;
            _organizationService.Update(cred);
        }

        /// <summary>
        /// GetCustomerCreds - query credentials by username and return contact and customer ids
        /// </summary>
        /// <param name="username"></param>
        public CubResponse<WSForgotUsernameResponse> GetCustomerCreds(string username)
        {
            CubResponse<WSForgotUsernameResponse> resp = new CubResponse<WSForgotUsernameResponse>();

            try
            {
                var result = (from cont in CubOrgSvc.ContactSet
                              join creds in CubOrgSvc.cub_CredentialSet
                                 on cont.Id equals creds.cub_ContactId.Id
                              join acc in CubOrgSvc.AccountSet
                                 on cont.ParentCustomerId.Id equals acc.Id
                              where creds.cub_Username.Equals(username)
                              select new WSForgotUsernameResponse
                              {
                                  customerId = cont.ParentCustomerId.Id.ToString(),
                                  contactId = cont.Id.ToString(),
                                  credentialId = creds.cub_CredentialId.ToString(),
                                  credStatus = creds.statuscode != null ? UtilityLogic.ConvertOptionSetObjectToEnum<cub_credential_statuscode>(creds.statuscode) : cub_credential_statuscode.Inactive,
                                  existingPassword = creds.cub_Password,
                                  lockoutDtm = creds.cub_LockoutDtm,
                                  forceChangePwd = creds.cub_ForceChangePassword != null ? (bool)creds.cub_ForceChangePassword : false,
                                  accountStatus = acc.StatusCode != null ? UtilityLogic.ConvertOptionSetObjectToEnum<account_statuscode>(acc.StatusCode) : account_statuscode.Close,
                              }).ToList();

                ////NOTE: Assuming username is unique system wide
                if (result.Count > 1) resp.Errors.Add(new WSMSDFault(string.Empty, CUBConstants.Credential.Error.ERR_KEY_MULTIPLE_USERNAMES,
                            CUBConstants.Credential.Error.ERR_MULTIPLE_USERNAMES_FOUND));

                if (result.Count < 1) resp.Errors.Add(new WSMSDFault(string.Empty, CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND, $"{username} not found"));

                if (result.Count == 1)
                {
                    resp.ResponseObject = result[0];

                    /*
                     * Checking if the account is Closed or Pending close
                     */
                    if (resp.ResponseObject.accountStatus.Equals(account_statuscode.Close) ||
                        resp.ResponseObject.accountStatus.Equals(account_statuscode.PendingClose))
                    {
                        resp.Errors.Add(new WSMSDFault("ContactId", 
                                                       CUBConstants.Error.ERR_ACCNT_CLOSED_OR_PENDING_CLOSURE, 
                                                       CUBConstants.Error.MSG_ACCNT_CLOSED_OR_PENDING));
                    }

                    if (result[0].credStatus.Equals(cub_credential_statuscode.Inactive))
                    {
                        resp.Errors.Add(new WSMSDFault(string.Empty, CUBConstants.Error.ERR_ACCNT_INACTIVE,
                                                    CUBConstants.Error.MSG_LOGIN_INACTIVE));
                    }
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                resp.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return resp;
        }

        /// <summary>
        /// GetCustomerCreds - query credentials by customer id and contact Id and return credentials
        /// </summary>
        /// <param name="username"></param>
        public CubResponse<WSCustomerCredential> GetCustomerCreds(Guid customrId, Guid contactId)
        {
            CubResponse<WSCustomerCredential> resp = new CubResponse<WSCustomerCredential>();

            try
            {

                var custCreds = (from cont in CubOrgSvc.ContactSet
                                 join creds in CubOrgSvc.cub_CredentialSet
                                    on cont.Id equals creds.cub_ContactId.Id
                                 where cont.ParentCustomerId.Equals(customrId) && creds.cub_ContactId.Equals(contactId)
                                 select new WSCustomerCredential
                                 {
                                     customerId = cont.ParentCustomerId.Id.ToString(),
                                     contactId = cont.Id.ToString(),
                                     credentialId = creds.cub_CredentialId.ToString(),
                                     credStatus = creds.statuscode != null ? UtilityLogic.ConvertOptionSetObjectToEnum<cub_credential_statuscode>(creds.statuscode) : cub_credential_statuscode.Inactive,
                                     lockoutDtm = creds.cub_LockoutDtm
                                 }
                         ).ToList();



                ////NOTE: Assuming username is unique system wide
                if (custCreds.Count > 1)
                {
                    resp.Errors.Add(new WSMSDFault("Credentials", CUBConstants.Credential.Error.ERR_CONTACT_MULTIPLE_CREDENTIALS,
                            CUBConstants.Credential.Error.ERR_MULTIPLE_USERNAMES_FOUND));
                    return resp;
                }
                if (custCreds.Count < 1)
                {
                    resp.Errors.Add(new WSMSDFault("Credentials", CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND, CUBConstants.Credential.Error.ERR_CONTACT_CREDENTIALS_NOT_FOUND));
                    return resp;
                }
                if (custCreds.Count == 1)
                {
                    resp.ResponseObject = custCreds[0];
                    if (custCreds[0].credStatus.Equals(cub_credential_statuscode.Locked) || custCreds[0].lockoutDtm.HasValue)
                    {
                        resp.Errors.Add(new WSMSDFault(string.Empty, CUBConstants.Error.ERR_ACCNT_LOCKED,
                                                    CUBConstants.Error.MSG_LOGIN_LOCKED));
                        return resp;
                    }

                    if (custCreds[0].credStatus.Equals(cub_credential_statuscode.PendingActivation) || custCreds[0].credStatus.Equals(cub_credential_statuscode.Inactive))
                    {
                        resp.Errors.Add(new WSMSDFault(string.Empty, CUBConstants.Error.ERR_ACCNT_INACTIVE,
                                                    CUBConstants.Error.MSG_LOGIN_NOT_ACTIVE));
                        return resp;
                    }
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                resp.Errors.Add(new WSMSDFault("MSD : " + MethodBase.GetCurrentMethod()?.Name, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return resp;
        }

        public bool IsRecentlyVerified(Guid contactId)
        {
            var globalsLogic = new GlobalsLogic(_organizationService);
            var offset = (string)GlobalsCache.GetFromCache("RecentVerificationsOffsetInMinutes");
            if (offset == null)
            {
                offset = globalsLogic.GetAttributeValue("CUB.MSD.Web.Angular", "RecentVerificationsOffsetInMinutes");
                GlobalsCache.AddToCache("RecentVerificationsOffsetInMinutes", offset);
            }
            var subjectVerified = (string)GlobalsCache.GetFromCache("SubjectVerified");
            if (subjectVerified == null)
            {
                subjectVerified = globalsLogic.GetAttributeValue("CUB.MSD.Web.Angular", "SubjectVerified");
                GlobalsCache.AddToCache("SubjectVerified", subjectVerified);
            }

            var cutOffTime = DateTime.Now.ToUniversalTime().AddMinutes(-double.Parse(offset));

            var result = (from verification in CubOrgSvc.cub_VerificationSet
                          join note in CubOrgSvc.AnnotationSet
                            on verification.ActivityId equals note.ObjectId.Id
                          where verification.RegardingObjectId.Id.Equals(contactId)
                          where verification.ModifiedOn > cutOffTime
                          orderby verification.ModifiedOn descending
                          select note.Subject).FirstOrDefault();
            return result != null && result.Equals(subjectVerified, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Get a contact First and Last names and Email by contact ID
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <returns>Contact First/Last Names and Email as a JSON object</returns>
        public string GetContactNameEmailById(Guid contactId)
        {
            var contact = (from c in CubOrgSvc.ContactSet
                           where c.ContactId.Equals(contactId)
                           select new
                           {
                               FirstName = c.FirstName,
                               LastName = c.LastName,
                               Email = c.EMailAddress1
                           }).FirstOrDefault();
            return JsonConvert.SerializeObject(contact);
        }

        #region NIS
        /// <summary>
        /// Report USer Name
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="username">User Name</param>
        public void ReportUsername(Guid customerId, Guid contactId, string username)
        {
            try
            {
                CubLogger.Debug(string.Format("ReportUsername customer[{0}] - contact[{1}]", customerId, contactId));
                System.Threading.Tasks.Task.Run(() =>
                {
                    NISApiLogic nisApiLogic = new NISApiLogic(_organizationService, _executionContext);
                    nisApiLogic.SendUsernameReminderNotification(customerId, contactId, username);
                });
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
            }
        }

        /// <summary>
        /// Report USer Name
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="username">User Name</param>
        public void RequestSendingOfVerificationToken(Guid customerId, Guid contactId, string verificationToken, DateTime tokenExpiry, cub_verificationtokentype tokenType)
        {
            try
            {
                CubLogger.Debug(string.Format("RequestSendingOfVerificationToken customer[{0}] - contact[{1}]", customerId, contactId));
                System.Threading.Tasks.Task.Run(() =>
                {
                    NISApiLogic nisApiLogic = new NISApiLogic(_organizationService, _executionContext);
                    nisApiLogic.VerificationTokenReport(customerId, contactId, verificationToken, tokenExpiry, tokenType);
                });
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
            }
        }

        /// <summary>
        /// Report USer Name
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="username">User Name</param>
        public void RequestSendingCustomerLockoutNotification(Guid customerId, Guid contactId)
        {
            try
            {
                CubLogger.Debug(string.Format("RequestSendingCustomerLockoutNotification customer[{0}] - contact[{1}]", customerId, contactId));
                System.Threading.Tasks.Task.Run(() =>
                {
                    NISApiLogic nisApiLogic = new NISApiLogic(_organizationService, _executionContext);
                    nisApiLogic.CustomerLockoutNotificationReport(customerId, contactId);
                });
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
            }
        }

        /// <summary>
        /// Report Chnage
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        public void ReportChange(Guid contactId)
        {
            try
            {
                CubLogger.DebugFormat("contactId Id = {0}", contactId);
                CubLogger.Debug("Before calling GetRptChangeDataById");
                Contact cnt = GetRptChangeDataById(contactId);
                CubLogger.Debug("After calling GetRptChangeDataById");
                NISApiLogic nisApi = new NISApiLogic(_organizationService, _executionContext);
                nisApi.ReportChange(cnt);
                nisApi = null;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
            }
        }

        /// <summary>
        /// Get Report Change Date by ID
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <returns>Contact</returns>
        public virtual Contact GetRptChangeDataById(Guid contactId)
        {
            return (from con in CubOrgSvc.ContactSet
                    where con.Id.Equals(contactId)
                    select
                        new Contact()
                        {
                            ParentCustomerId = new EntityReference(Account.EntityLogicalName, con.ParentCustomerId.Id),
                            FirstName = con.FirstName,
                            LastName = con.LastName,
                            MiddleName = con.MiddleName,
                            //@ToDo suffix is string for me... doc wants int
                            EMailAddress1 = con.EMailAddress1,
                            Telephone1 = GetSMSFormattedPhone(con.Telephone1, con.cub_Phone1Type) ?? GetSMSFormattedPhone(con.Telephone2, con.cub_Phone2Type) ?? GetSMSFormattedPhone(con.Telephone3, con.cub_Phone3Type),
                            ContactId = con.ContactId
                        }).SingleOrDefault();
        }

        /// <summary>
        /// Get SMS Formatted Phone
        /// </summary>
        /// <param name="phoneNumber">Phone number</param>
        /// <param name="phoneNumType">Phone Type</param>
        /// <returns></returns>
        public string GetSMSFormattedPhone(string phoneNumber, object phoneNumType)
        {
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                if (phoneNumType != null && ((cub_phonetype)((OptionSetValue)phoneNumType).Value).ToString("G") == "Mobile")
                {
                    return String.Format("{0:###-###-####}", Convert.ToInt64(UnFormatPhoneNumber(phoneNumber)));
                }
            }
            return null;
        }

        /// <summary>
        /// Validate Customer/Contact credentials (PIN)
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="pin">PIN</param>
        public void CredentialsValidate(string customerId, string contactId, string pin)
        {

            var account = (from a
                           in CubOrgSvc.AccountSet
                           where a.AccountId.Value.Equals(new Guid(customerId))
                           select new
                           {
                               AccountStatus = a.StatusCode
                           }).ToList();
            if (account == null || account.Count == 0)
            {
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("customer-id",
                                                                                              CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND,
                                                                                              CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND));
            }
            else
            {
                if (account[0].AccountStatus.Equals(account_statuscode.Close) ||
                    account[0].AccountStatus.Equals(account_statuscode.PendingClose))
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("customer-id",
                                                                                                  CUBConstants.Error.ERR_ACCNT_CLOSED_OR_PENDING_CLOSURE,
                                                                                                  CUBConstants.Error.MSG_ACCNT_CLOSED_OR_PENDING));
                }
            }


            if (UtilityLogic.IsStringTooShort(pin, GlobalsCache.PinMinLength(_organizationService)))
            {
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Pin",
                                                                                              CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL,
                                                                                              CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET));
            }
            if (!UtilityLogic.IsStringLengthValid(pin, GlobalsCache.PinMaxLength(_organizationService)))
            {
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("Pin",
                                                                                              CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG,
                                                                                              CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
            }

            // customer-id	errors.general.record.not.found. The customer not found.
            // customer-id	errors.general.value.required. Field is blank.
            // customer-id	errors.general.value.toolong. Maximum length exceeded.
            // contact-id	errors.general.record.not.found. The contact not found.
            // pin. errors.general.value.unexpected. Invalid status action.
            // errors.account.is.closed. The account is closed
            // errors.account.is.locked. Login is locked.
            // errors.account.pending.activation. Login is pending activation.
            // errors.invalid.user.or.password. The pin is invalid.
            var credentials = (from c
                               in CubOrgSvc.cub_CredentialSet
                               where c.cub_ContactId.Id.Equals(new Guid(contactId))
                               select new
                               {
                                   Status = c.statecode,
                                   State = c.statuscode,
                                   Pin = c.cub_Pin
                               }).ToList();

            if (credentials == null || credentials.Count == 0)
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("contactId", 
                                                                                              CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND,
                                                                                              CUBConstants.Error.ERR_CONTACT_NOT_FOUND));
            bool found = false;
            bool foundButInactive = false;
            // A contact may have multiple credentials. So checking each credential
            foreach (var cred in credentials)
            {
                if (cred.Pin != null &&  cred.Pin.Equals(pin))
                {
                    // If the pin is found checking if it is active
                    if (cred.Status == cub_CredentialState.Active)
                        found = true;
                    else
                    {
                        foundButInactive = true;
                        if (cred.State != null && ((cub_credential_statuscode)cred.State) == cub_credential_statuscode.Locked)
                        {
                            throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("pin",
                                                                                                          CUBConstants.Error.ERR_ACCNT_LOCKED,
                                                                                                          CUBConstants.Error.MSG_LOGIN_LOCKED));
                        } else if (cred.State != null && ((cub_credential_statuscode)cred.State) == cub_credential_statuscode.PendingActivation)
                        {
                            throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("pin",
                                                                                                          CUBConstants.Error.ERR_ACCNT_PENDING_ACTIVATION,
                                                                                                          CUBConstants.Error.MSG_LOGIN_LOCKED));
                        }
                    }
                }
            }
            if (!found)
            {
                // Checking first if it was found but inactive
                if (foundButInactive)
                {
                    throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("pin",
                                                                                                  CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED,
                                                                                                  CUBConstants.Error.ERR_GENERAL_INVALID_STATUS));
                }
                // Not found
                throw new FaultException<Model.MSDApi.WSMSDFault>(new Model.MSDApi.WSMSDFault("pin",
                                                                                              CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD,
                                                                                              CUBConstants.Error.ERR_INVALID_PIN));
            }
        }

        #endregion NIS

        #region Password
        /// <summary>
        /// Archived Password
        /// </summary>
        public class ArchivedPwds
        {
            public Guid? Id { get; set; }
            public Guid ContactId { get; set; }
            public string Password { get; set; }
            public DateTime ArchivedDate { get; set; }
        }

        /// <summary>
        /// Password Credentials
        /// </summary>
        public class PwdCredentials
        {
            public Guid? Id { get; set; }
            public string Username { get; set; }
            public string Old_Password { get; set; }
            public DateTime? Lock_Out_DateTime { get; set; }
            public string Status { get; set; }
        }

        /// <summary>
        /// WSCredentials
        /// </summary>
        public class WSCredentials
        {
            public Guid? Id { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string Pin { get; set; }
            public DateTime? Lock_Out_DateTime { get; set; }
            public string Status { get; set; }
        }

        /// <summary>
        /// Contact Update Return Info
        /// </summary>
        public class ContactUpdateReturnInfo
        {
            public string PhoneRegex { get; set; }
            public Guid StateId { get; set; }
            public Guid ZipId { get; set; }
            public Guid CountryId { get; set; }
            public Guid CustomerId { get; set; }
            public Guid ContactId { get; set; }
        }

        /// <summary>
        /// return the client username rules currently in place
        /// </summary>
        /// <returns></returns>
        public WSUsernameRules GetClientUsernameRules()
        {
            try
            {
                WSUsernameRules unrules = new WSUsernameRules();

                unrules.maxLength = GlobalsCache.UsernameMaxLength(_organizationService);
                unrules.minLength = GlobalsCache.UsernameMinLength(_organizationService);
                unrules.requiresEmail = GlobalsCache.UsernameRequireEmail(_organizationService);

                return unrules;
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Validate Username
        /// </summary>
        /// <param name="userName"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        private List<WSError> IsUsernameValid(string username)
        {
            List<WSError> usernameErrors = new List<WSError>();

            try
            {
                GlobalsLogic glb = new GlobalsLogic(_organizationService);

                if (!string.IsNullOrEmpty(username))
                {
                    if (UtilityLogic.IsStringTooShort(username, GlobalsCache.UsernameMinLength(_organizationService)))
                        usernameErrors = glb.AddErrorTolist(usernameErrors, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL, CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET);

                    if (!UtilityLogic.IsStringLengthValid(username, GlobalsCache.UsernameMaxLength(_organizationService)))
                        usernameErrors = glb.AddErrorTolist(usernameErrors, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED);

                    if (GlobalsCache.UsernameRequireEmail(_organizationService))
                    {
                        char a = username.ElementAt(0);
                        string b = username.Substring(0, 4);
                        b = b.ToUpper();

                        if (a == '.' || a == ',' || a == '@' || b == "WWW.")
                            usernameErrors = glb.AddErrorTolist(usernameErrors, CUBConstants.Error.ERR_GENERAL_CANNOT_START_WITH_SPEC_CHAR, CUBConstants.Error.MSG_CANNOT_START_WITH_SPEC_CHAR);

                        if (!UtilityLogic.isValidEmailFormat(username))
                            usernameErrors = glb.AddErrorTolist(usernameErrors, CUBConstants.Error.ERR_GENERAL_MUST_BE_STD_EMAIL_FRMT, CUBConstants.Error.MSG_MUST_BE_STD_EMAIL_FRMT);
                    }
                }
                else
                {
                    usernameErrors = glb.AddErrorTolist(usernameErrors, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK);
                }
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Validate Username", "err_general_validate_username_failed", e.Message));
            }

            return usernameErrors;
        }

        /// <summary>
        /// Validate Username
        /// </summary>
        /// <param name="WSCustomerContact"></param>
        /// <returns></returns>
        private CubResponse<WSCustomerContact> ValidateUserName(WSCustomerContact contact)
        {
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            const string methodName = "ValidateUserName";
            try
            {
                if (string.IsNullOrEmpty(contact.username))
                {
                    response.Errors.Add(new WSMSDFault("User Name", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }

                if (UtilityLogic.IsStringTooShort(contact.username, GlobalsCache.UsernameMinLength(_organizationService)))
                {
                    response.Errors.Add(new WSMSDFault("User Name", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL, CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET));
                    return response;
                }

                if (!UtilityLogic.IsStringLengthValid(contact.username, GlobalsCache.UsernameMaxLength(_organizationService)))
                {
                    response.Errors.Add(new WSMSDFault("User Name", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                }


                if (GlobalsCache.UsernameRequireEmail(_organizationService))
                {
                    char a = contact.username.ElementAt(0);
                    string b = contact.username.Substring(0, 4);
                    b = b.ToUpper();

                    if (a == '.' || a == ',' || a == '@' || b == "WWW.")
                    {
                        response.Errors.Add(new WSMSDFault("User Name", CUBConstants.Error.ERR_GENERAL_CANNOT_START_WITH_SPEC_CHAR, CUBConstants.Error.MSG_CANNOT_START_WITH_SPEC_CHAR));
                        return response;
                    }


                    if (!UtilityLogic.isValidEmailFormat(contact.username))
                    {
                        response.Errors.Add(new WSMSDFault("User Name", CUBConstants.Error.ERR_GENERAL_MUST_BE_STD_EMAIL_FRMT, CUBConstants.Error.MSG_MUST_BE_STD_EMAIL_FRMT));
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        public WSPasswordRules GetClientPasswordRules()
        {
            try
            {
                WSPasswordRules pwdrules = new WSPasswordRules();

                pwdrules.minDigit = GlobalsCache.PasswordNumeric(_organizationService);
                int lower = GlobalsCache.PasswordUpperAlpha(_organizationService);
                int upper = GlobalsCache.PasswordLowerAlpha(_organizationService);
                pwdrules.minLetter = lower + upper;
                pwdrules.specialChars = "any character not in the alphabet nor a digit";
                pwdrules.minSpecialChars = GlobalsCache.PasswordNonAlphaNumeric(_organizationService);
                pwdrules.useDictionary = !GlobalsCache.PasswordAllowDictionaryWords(_organizationService);

                if (lower > 0 && upper > 0)
                {
                    pwdrules.forceMixedCase = true;
                }
                else
                {
                    pwdrules.forceMixedCase = false;
                }

                if (!GlobalsCache.PasswordAllowConsecutiveChars(_organizationService) && !GlobalsCache.PasswordAllowContiguousChars(_organizationService))
                {
                    pwdrules.checkRepeats = true;
                }
                else
                {
                    pwdrules.checkRepeats = false;
                }

                pwdrules.minLength = GlobalsCache.PasswordMinLength(_organizationService);
                pwdrules.maxLength = GlobalsCache.PasswordMaxLength(_organizationService);

                return pwdrules;
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Validate contact password
        /// </summary>
        /// <param name="contact">WSCustomerContact</param>
        /// <returns>WSCustomerContact</returns>
        private CubResponse<WSCustomerContact> ValidatePassword(WSCustomerContact contact)
        {
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            const string methodName = "ValidatePassword";
            try
            {

                if (string.IsNullOrEmpty(contact.password))
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }

                if (UtilityLogic.IsStringTooShort(contact.password, GlobalsCache.PasswordMinLength(_organizationService)))
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL, CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET));
                    return response;
                }
                if (!UtilityLogic.IsStringLengthValid(contact.password, GlobalsCache.PasswordMaxLength(_organizationService)))
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;

                }

                if (!GlobalsCache.PasswordAllowUsername(_organizationService) && contact.password.Contains(contact.username))
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_CONTAIN_USERNAME, CUBConstants.Error.MSG_PASSWORD_CAN_NOT_CONTAIN_USERNAME));
                    return response;
                }

                if (!GlobalsCache.PasswordAllowConsecutiveChars(_organizationService) && UtilityLogic.HasConsecutiveChars(contact.password))
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_SAME_CONSECUTIVE_CHARS, CUBConstants.Error.MSG_SAME_CONSECUTIVE_CHARS));
                    return response;
                }


                if (!GlobalsCache.PasswordAllowContiguousChars(_organizationService) && UtilityLogic.HasContiguousChars(contact.password))
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_CONTIGUOUS_CHARS, CUBConstants.Error.MSG_CONTIGUOUS_CHARS));
                    return response;
                }

                if (UtilityLogic.NumericCount(contact.password) < GlobalsCache.PasswordNumeric(_organizationService))
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_CONTAIN_DIGIT, CUBConstants.Error.MSG_CONTAINS_DIGITS));
                    return response;
                }

                int pwdLwrAlpha = GlobalsCache.PasswordLowerAlpha(_organizationService);
                int pwdUprAlpha = GlobalsCache.PasswordUpperAlpha(_organizationService);

                if (pwdLwrAlpha > 0)
                {
                    if (pwdLwrAlpha > 0 && (UtilityLogic.LowerCaseCount(contact.password) < pwdLwrAlpha || UtilityLogic.UpperCaseCount(contact.password) < pwdLwrAlpha))
                    {
                        response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_MIXED_CASE_CHARS, CUBConstants.Error.MSG_MIXED_CASE_CHARS));
                        return response;
                    }
                    else
                    {
                        if (UtilityLogic.LowerCaseCount(contact.password) < pwdLwrAlpha)
                        {
                            response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_MIXED_CASE_CHARS, CUBConstants.Error.MSG_MIXED_CASE_CHARS));
                            return response;
                        }
                    }
                }
                else
                {
                    if (pwdUprAlpha > 0)
                    {
                        if (UtilityLogic.UpperCaseCount(contact.password) < pwdUprAlpha)
                        {
                            response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_MIXED_CASE_CHARS, CUBConstants.Error.MSG_MIXED_CASE_CHARS));
                            return response;
                        }
                    }
                }

                if (UtilityLogic.UpperCaseCount(contact.password) + UtilityLogic.LowerCaseCount(contact.password) < pwdUprAlpha + pwdLwrAlpha)
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_CONTAINS_AT_LEAST_CHARS, CUBConstants.Error.MSG_CONTAINS_AT_LEAST_CHARS));
                    return response;
                }
                if (UtilityLogic.NonAlphaCount(contact.password) < GlobalsCache.PasswordNonAlphaNumeric(_organizationService))
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_CONTAINS_A_SPECIAL_CHAR, CUBConstants.Error.MSG_CONTAINS_A_SPECIAL_CHAR));
                    return response;
                }

                if (!GlobalsCache.PasswordAllowDictionaryWords(_organizationService) && PasswordContainsDictionaryWord(contact.password))
                {
                    response.Errors.Add(new WSMSDFault("Password", CUBConstants.Error.ERR_CONTAINS_DICTIOANARY_WORD, CUBConstants.Error.MSG_CONTAINS_DICTIOANARY_WORD));
                    return response;
                }

                response.ResponseObject = contact;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }


        /// <summary>
        /// Validate contact PIN
        /// </summary>
        /// <param name="contact">WSCustomerContact</param>
        /// <returns>WSCustomerContact</returns>
        private CubResponse<WSCustomerContact> ValidatePin(WSCustomerContact contact)
        {
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            const string methodName = "ValidatePin";
            try
            {
                //if pin is empty, there is nothing to validate
                if (string.IsNullOrEmpty(contact.pin))
                {
                    response.ResponseObject = contact;
                    return response;
                }

                if (UtilityLogic.IsStringTooShort(contact.pin, GlobalsCache.PinMinLength(_organizationService)))
                {
                    response.Errors.Add(new WSMSDFault("Pin", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL, CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET));
                    return response;
                }
                if (!UtilityLogic.IsStringLengthValid(contact.pin, GlobalsCache.PinMaxLength(_organizationService)))
                {
                    response.Errors.Add(new WSMSDFault("Pin", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;

                }

                if (contact.pin.Contains(contact.username))
                {
                    response.Errors.Add(new WSMSDFault("Pin", CUBConstants.Error.ERR_PIN_CONTAIN_USERNAME, CUBConstants.Error.MSG_PIN_CAN_NOT_CONTAIN_USERNAME));
                    return response;
                }

                if (UtilityLogic.HasConsecutiveChars(contact.pin))
                {
                    response.Errors.Add(new WSMSDFault("Pin", CUBConstants.Error.ERR_PIN_SAME_CONSECUTIVE_CHARS, CUBConstants.Error.MSG_PIN_SAME_CONSECUTIVE_CHARS));
                    return response;
                }


                if (UtilityLogic.HasContiguousChars(contact.pin))
                {
                    response.Errors.Add(new WSMSDFault("Pin", CUBConstants.Error.ERR_PIN_CONTIGUOUS_CHARS, CUBConstants.Error.MSG_PIN_CONTIGUOUS_CHARS));
                    return response;
                }

                if (GlobalsCache.PinNumericOnly(_organizationService) && !UtilityLogic.IsAllNumeric(contact.pin))
                {
                    response.Errors.Add(new WSMSDFault("Pin", CUBConstants.Error.ERR_GENERAL_NUMERIC_ONLY, CUBConstants.Error.MSG_GENERAL_NONNUMERIC_CHARCTERS));
                    return response;
                }

                response.ResponseObject = contact;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Validate Username and Password
        /// </summary>
        /// <param name="password"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public CubResponse<WSCredentailsPrevalidateResults> ValidateUsernameAndPassword(string username, string password)
        {
            CubResponse<WSCredentailsPrevalidateResults> response = new CubResponse<WSCredentailsPrevalidateResults>();

            response.ResponseObject = new WSCredentailsPrevalidateResults();

            response.ResponseObject.usernameErrors = IsUsernameValid(username);

            if (response.ResponseObject.isUsernameValid) response.ResponseObject.isUsernameAvailable = IsUserNameAvailable(username);
            else response.ResponseObject.isUsernameAvailable = true;

            response.ResponseObject.passwordErrors = IsPasswordValid(password, username);

            return response;
        }

        /// <summary>
        /// Validate Username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public CubResponse<WSUsernamePrevalidateResults> ValidateUsername(string username)
        {
            CubResponse<WSUsernamePrevalidateResults> response = new CubResponse<WSUsernamePrevalidateResults>();

            response.ResponseObject = new WSUsernamePrevalidateResults();

            response.ResponseObject.usernameErrors = IsUsernameValid(username);

            if (response.ResponseObject.isUsernameValid) response.ResponseObject.isUsernameAvailable = IsUserNameAvailable(username);
            else response.ResponseObject.isUsernameAvailable = false;

            return response;
        }

        /// <summary>
        /// Validate Password
        /// </summary>
        /// <param name="password"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public CubResponse<WSPasswordPrevalidateResults> ValidatePassword(string password, string username)
        {
            CubResponse<WSPasswordPrevalidateResults> response = new CubResponse<WSPasswordPrevalidateResults>();

            response.ResponseObject = new WSPasswordPrevalidateResults();

            response.ResponseObject.passwordErrors = IsPasswordValid(password, username);

            return response;
        }

        /// <summary>
        /// Validate Secuirty Question/Asnwer
        /// </summary>
        /// <param name="contact">Contact</param>
        /// <returns>CubResponse<WSCustomerContact></returns>
        private CubResponse<WSCustomerContact> ValidateSecuirtyQA(WSCustomerContact contact)
        {

            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();


            if (contact.securityQAs != null)
            {
                foreach (WSSecurityQA sqa in contact.securityQAs)
                {
                    if (string.IsNullOrEmpty(sqa.securityAnswer))
                    {
                        response.Errors.Add(new WSMSDFault("Security QA", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_SECURITY_ANSWER_REQUIRED));
                        return response;
                    }


                    if (!UtilityLogic.IsStringLengthValid(sqa.securityAnswer, 64))
                    {
                        response.Errors.Add(new WSMSDFault("Security QA", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_SECURITY_ANSWER_MAX_LENGTH_EXCEED));
                        return response;
                    }


                    Guid? secQId = GetSecurityQuestionId(sqa.securityQuestion);
                    if (!secQId.HasValue)
                    {
                        response.Errors.Add(new WSMSDFault("Security QA", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_SECURITY_INVALID_QUESTION));
                        return response;
                    }
                    else
                    {
                        sqa.securityQuestionId = secQId.Value;
                    }



                }
            }

            response.ResponseObject = contact;
            return response;

        }


        /// <summary>
        /// Validate Password
        /// </summary>
        /// <param name="password"></param>
        /// <param name="userName"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        private List<WSError> IsPasswordValid(string password, string username)
        {
            string currentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<WSError> passwordErrors = new List<WSError>();

            try
            {
                if (UtilityLogic.IsStringTooShort(password, GlobalsCache.PasswordMinLength(_organizationService)))
                    passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL, message = CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET });

                if (!UtilityLogic.IsStringLengthValid(password, GlobalsCache.PasswordMaxLength(_organizationService)))
                    passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, message = CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED });

                if (passwordErrors.Count() == 0)
                {
                    if (!GlobalsCache.PasswordAllowUsername(_organizationService) && password.Contains(username))
                        passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_CONTAIN_USERNAME, message = CUBConstants.Error.MSG_PASSWORD_CAN_NOT_CONTAIN_USERNAME });

                    if (!GlobalsCache.PasswordAllowConsecutiveChars(_organizationService) && UtilityLogic.HasConsecutiveChars(password))
                        passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_SAME_CONSECUTIVE_CHARS, message = CUBConstants.Error.MSG_SAME_CONSECUTIVE_CHARS });

                    if (!GlobalsCache.PasswordAllowContiguousChars(_organizationService) && UtilityLogic.HasContiguousChars(password))
                        passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_CONTIGUOUS_CHARS, message = CUBConstants.Error.MSG_CONTIGUOUS_CHARS });

                    if (UtilityLogic.NumericCount(password) < GlobalsCache.PasswordNumeric(_organizationService))
                        passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_CONTAIN_DIGIT, message = CUBConstants.Error.MSG_CONTAINS_DIGITS });

                    int pwdLwrAlpha = GlobalsCache.PasswordLowerAlpha(_organizationService);
                    int pwdUprAlpha = GlobalsCache.PasswordUpperAlpha(_organizationService);

                    if (pwdLwrAlpha > 0)
                    {
                        if (pwdLwrAlpha > 0 && (UtilityLogic.LowerCaseCount(password) < pwdLwrAlpha || UtilityLogic.UpperCaseCount(password) < pwdLwrAlpha))
                        {
                            passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_MIXED_CASE_CHARS, message = CUBConstants.Error.MSG_MIXED_CASE_CHARS });
                        }
                        else
                        {
                            if (UtilityLogic.LowerCaseCount(password) < pwdLwrAlpha)
                                passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_MIXED_CASE_CHARS, message = CUBConstants.Error.MSG_MIXED_CASE_CHARS });
                        }
                    }
                    else
                    {
                        if (pwdUprAlpha > 0)
                        {
                            if (UtilityLogic.UpperCaseCount(password) < pwdUprAlpha)
                                passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_MIXED_CASE_CHARS, message = CUBConstants.Error.MSG_MIXED_CASE_CHARS });
                        }
                    }

                    if (UtilityLogic.UpperCaseCount(password) + UtilityLogic.LowerCaseCount(password) < pwdUprAlpha + pwdLwrAlpha)
                        passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_CONTAINS_AT_LEAST_CHARS, message = CUBConstants.Error.MSG_CONTAINS_AT_LEAST_CHARS });

                    if (UtilityLogic.NonAlphaCount(password) < GlobalsCache.PasswordNonAlphaNumeric(_organizationService))
                        passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_CONTAINS_A_SPECIAL_CHAR, message = CUBConstants.Error.MSG_CONTAINS_A_SPECIAL_CHAR });

                    if (!GlobalsCache.PasswordAllowDictionaryWords(_organizationService) && PasswordContainsDictionaryWord(password))
                        passwordErrors.Add(new WSError() { key = CUBConstants.Error.ERR_CONTAINS_DICTIOANARY_WORD, message = CUBConstants.Error.MSG_CONTAINS_DICTIOANARY_WORD });
                }
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault(currentMethod, CUBConstants.Error.ERR_GENERAL_VALIDATE_PASSWORD_FAILED, e.Message));
            }

            return passwordErrors;
        }

        /// <summary>
        /// Change Password
        /// </summary>
        /// <param name="customerId">customer Id</param>
        /// <param name="contactId">contact Id</param>
        /// <param name="origPwd">Original password in clear text (not encypted)</param>
        /// <param name="newPwd">New password in clear text (not encrypted)</param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public CubResponse<WSPasswordPrevalidateResults> ChangePassword(Guid customerId, Guid contactId, string origPwd, string newPwd)
        {
            string currentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            CubResponse<WSPasswordPrevalidateResults> response = new CubResponse<WSPasswordPrevalidateResults>();
            response.ResponseObject = new WSPasswordPrevalidateResults();
            response.ResponseObject.passwordErrors = new List<WSError>();

            try
            {
                PwdCredentials cred = IsCurrentPwd(customerId, contactId, origPwd);

                if (cred != null && cred.Id.HasValue)
                {
                    response.ResponseObject.passwordErrors = IsPasswordValid(newPwd, cred.Username);

                    if (response.ResponseObject.passwordErrors.Count() == 0)
                    {
                        ArchivedPwds[] usedPwds = GetPreviouslyUsedPwds(customerId, contactId);
                        if (usedPwds.Count() > 0 && PasswordPreviouslyUsed(customerId, contactId, newPwd, usedPwds))
                            response.Errors.Add(new WSMSDFault("password", CUBConstants.Error.ERR_PWD_PREVIOUSLY_USED, CUBConstants.Error.MSG_PWD_PREVIOUSLY_USED));

                        if (response.Success && response.ResponseObject.passwordErrors.Count() == 0)
                        {
                            UpdatePasswordInDB(customerId, contactId, origPwd, newPwd, cred.Id.Value, usedPwds);
                        }
                    }
                }
                else
                {
                    response.Errors.Add(new WSMSDFault("contact", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CONTACT_NOT_FOUND));
                }
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault(currentMethod, CUBConstants.Error.ERR_GENERAL_CHANGE_PASSWORD_FAILED, e.Message));
            }

            return response;
        }

        /// <summary>
        /// Check for Password Previously Used
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID</param>
        /// <param name="newPwd">new password</param>
        /// <param name="usedPwds">used password</param>
        /// <returns></returns>
        private bool PasswordPreviouslyUsed(Guid customerId, Guid contactId, string newPwd, ArchivedPwds[] usedPwds)
        {
            string encryptedPwd = UtilityLogic.EncryptTripleDES(newPwd);
            ArchivedPwds foundPwd = Array.Find(usedPwds, p => p.Password == encryptedPwd);

            return (foundPwd == null) ? false : true;
        }

        /// <summary>
        /// Generate Token
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="tokenType"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public CubResponse<WSGenerateTokenResponse> GenerateToken(Guid customerId, Guid contactId, cub_verificationtokentype tokenType, string deviceToken = null)
        {
            CubResponse<WSGenerateTokenResponse> response = new CubResponse<WSGenerateTokenResponse>();
            response.ResponseObject = new WSGenerateTokenResponse();

            try
            {
                GlobalsLogic glb = new GlobalsLogic(_organizationService);

                // get credentials...
                PwdCredentials cred = FindContactCred(customerId, contactId);
                if (cred != null && cred.Id.HasValue)
                {
                    //validate
                    if (cred.Lock_Out_DateTime != null && !tokenType.Equals(cub_verificationtokentype.UnlockAccount))
                    {
                        response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_LOCKED, CUBConstants.Error.MSG_LOGIN_LOCKED));
                    }

                    switch (tokenType)
                    {
                        case cub_verificationtokentype.Mobile:
                            if (string.IsNullOrEmpty(deviceToken))
                                response.Errors.Add(new WSMSDFault("VerificationToken.mobileToken", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                            if (!UtilityLogic.IsStringLengthValid(deviceToken, GlobalsCache.MobileTokenMaxLength(_organizationService)))
                                response.Errors.Add(new WSMSDFault("VerificationToken.mobileToken", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                            if (string.Compare(cred.Status, CUBConstants.Credential.Locked, true) == 0)
                                response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_LOCKED, CUBConstants.Error.MSG_LOGIN_LOCKED));
                            if (string.Compare(cred.Status, CUBConstants.Credential.Inactive, true) == 0)
                                response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_INACTIVE, CUBConstants.Error.MSG_LOGIN_INACTIVE));
                            if (string.Compare(cred.Status, CUBConstants.Credential.Pending, true) == 0)
                                response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_PENDING_ACTIVATION, CUBConstants.Error.MSG_LOGIN_PENDING_ACTIVATION));
                            break;
                        case cub_verificationtokentype.Password:
                            if (string.Compare(cred.Status, CUBConstants.Credential.Locked, true) == 0)
                                response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_LOCKED, CUBConstants.Error.MSG_LOGIN_LOCKED));
                            if (string.Compare(cred.Status, CUBConstants.Credential.Inactive, true) == 0)
                                response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_INACTIVE, CUBConstants.Error.MSG_LOGIN_INACTIVE));
                            break;
                        case cub_verificationtokentype.UnlockAccount:
                            if (string.Compare(cred.Status, CUBConstants.Credential.Locked, true) != 0)
                                response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_ACCNT_NOT_LOCKED, CUBConstants.Error.MSG_LOGIN_NOT_LOCKED));
                            break;
                        case cub_verificationtokentype.NewCustomerPassword:
                            break;
                        default:
                            response.Errors.Add(new WSMSDFault("MSD", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_GENERAL_TOKEN_TYPE_UNKNOWN));
                            break;
                    }

                    if (response.Success)
                    {
                        cub_VerificationToken vtkn = new cub_VerificationToken();
                        double numDays = GlobalsCache.TempPasswordDurationDays(_organizationService);
                        string token = string.Empty;

                        switch (tokenType)
                        {
                            case cub_verificationtokentype.Mobile:
                                vtkn.cub_TokenExpirationDate = DateTime.Now.AddDays(numDays);
                                token = GenerateVerificationToken(tokenType);
                                vtkn.cub_Name = UtilityLogic.EncryptTripleDES(token);
                                var mobilecrdnt = new EntityReference { LogicalName = cub_Credential.EntityLogicalName, Id = cred.Id.Value };
                                vtkn.cub_CredentialsId = mobilecrdnt;
                                vtkn.cub_TokenType = new OptionSetValue(Convert.ToInt32(cub_verificationtokentype.Mobile));
                                vtkn.cub_MobileDeviceToken = UtilityLogic.EncryptTripleDES(deviceToken);
                                _organizationService.Create(vtkn);

                                response.ResponseObject.token = token;
                                response.ResponseObject.tokenExpiryTime = vtkn.cub_TokenExpirationDate;
                                break;
                            case cub_verificationtokentype.NewCustomerPassword:
                            case cub_verificationtokentype.Password:
                                vtkn.cub_TokenExpirationDate = DateTime.Now.AddDays(numDays);
                                token = GenerateVerificationToken(tokenType);
                                vtkn.cub_Name = UtilityLogic.EncryptTripleDES(token);
                                var pwdcrdnt = new EntityReference { LogicalName = cub_Credential.EntityLogicalName, Id = cred.Id.Value };
                                vtkn.cub_CredentialsId = pwdcrdnt;
                                vtkn.cub_TokenType = new OptionSetValue(Convert.ToInt32(cub_verificationtokentype.Password));
                                _organizationService.Create(vtkn);

                                response.ResponseObject.token = token;
                                response.ResponseObject.tokenExpiryTime = vtkn.cub_TokenExpirationDate;
                                break;
                            case cub_verificationtokentype.UnlockAccount:
                                vtkn.cub_TokenExpirationDate = DateTime.Now.AddDays(numDays);
                                token = GenerateVerificationToken(tokenType);
                                vtkn.cub_Name = UtilityLogic.EncryptTripleDES(token);
                                var unlockAccountcrdnt = new EntityReference { LogicalName = cub_Credential.EntityLogicalName, Id = cred.Id.Value };
                                vtkn.cub_CredentialsId = unlockAccountcrdnt;
                                vtkn.cub_TokenType = new OptionSetValue(Convert.ToInt32(cub_verificationtokentype.UnlockAccount));
                                _organizationService.Create(vtkn);

                                response.ResponseObject.token = token;
                                response.ResponseObject.tokenExpiryTime = vtkn.cub_TokenExpirationDate;
                                break;
                            default:
                                response.Errors.Add(new WSMSDFault("MSD", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_GENERAL_TOKEN_TYPE_UNKNOWN));
                                break;
                        }
                    }
                }
                else
                {
                    response.Errors.Add(new WSMSDFault("Account", CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_CONTACT_NOT_FOUND));
                }
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Generate Token", "err_general_generate_token_failed", e.Message));
            }

            return response;
        }

        /// <summary>
        /// Update Password
        /// includes archiving the past x passwords
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="origPwd"></param>
        /// <param name="newPwd"></param>
        /// <param name="credId"></param>
        /// <param name="CubOrgSvc"></param>
        /// <param name="usedPwds"></param>
        /// <param name="updateToken"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public void UpdatePasswordInDB(Guid customerId, Guid contactId, string origPwd, string newPwd, Guid credId, ArchivedPwds[] usedPwds)
        {
            try
            {
                // Update credentials...
                cub_Credential crd = new cub_Credential();
                crd.cub_CredentialId = credId;

                crd.cub_Password = newPwd;

                _organizationService.Update(crd);

                System.Threading.Tasks.Task.Run(() =>
                {
                    NISApiLogic nisLogic = new NISApiLogic(_organizationService, _executionContext);
                    nisLogic.PasswordReportChange(customerId, contactId);
                });

                ArchivePwd(customerId, contactId, newPwd, usedPwds);
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Update Password", "err_general_udate_password_failed", e.Message));
            }
        }

        /// <summary>
        /// Archive Password. If number of passwords in archive exceeds the number we need, delete the oldest password first before adding new one to archive
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="pwd"></param>
        /// <param name="usedPwds"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public void ArchivePwd(Guid customerId, Guid contactId, string pwd, ArchivedPwds[] usedPwds)
        {
            try
            {

                //If there are passwords already in archive, check the setting how many passwords we need to keep in archive.
                //If setting value is greater than or equal to n umber of passwords we currently have, delete the oldest password form archive

                if (usedPwds != null)
                {
                    //Checking setting of how many passwords we need to keep in archive
                    int numOfCapturedPwds = GlobalsCache.NumberOfPasswordsToArchive(_organizationService);

                    if (usedPwds.Length >= numOfCapturedPwds)
                    {
                        ////Determine number of passwords that exceed the threshold plus 1 for current password that will be adding later
                        int numToRemove = (numOfCapturedPwds - usedPwds.Length) + 1;
                        //loop password array backwards to remove oldest passwords first. Passwords in array are sorted by date entered ascending
                        for (int i = numToRemove; i > 0; i--)
                        {
                            _organizationService.Delete(cub_ArchivedPassword.EntityLogicalName, usedPwds[i].Id.Value);
                        }
                    }


                }

                AddArchivePwd(contactId, pwd);

            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Archive Password", "err_general_update_password_failed", e.Message));
            }
        }

        /// <summary>
        /// Create Archive Password
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="pwd"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public void AddArchivePwd(Guid contactId, string pwd)
        {
            try
            {
                cub_ArchivedPassword password = new cub_ArchivedPassword();
                password.cub_ArchivedPasswordId = Guid.NewGuid();
                password.cub_Password = UtilityLogic.EncryptTripleDES(pwd);
                password.cub_ArchivedDate = DateTime.UtcNow;
                password.cub_ContactId = new EntityReference(Contact.EntityLogicalName, contactId);
                _organizationService.Create(password);
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Create Archive Password", "err_general_create_archive_password_failed", e.Message));
            }
        }

        /// <summary>
        /// Returns list of security questions and answers for contact 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns>List<WSSecurityQA></returns>
        public List<WSSecurityQA> GetSecurityAnswers(Guid contactId)
        {
            List<WSSecurityQA> listOfAnswers = new List<WSSecurityQA>();
            try
            {

                IQueryable<WSSecurityQA> returnValue;
                returnValue = (from sqa in CubOrgSvc.cub_SecurityAnswerSet
                               join sq in CubOrgSvc.cub_SecurityQuestionSet on sqa.cub_SecurityQuestionId.Id equals sq.cub_SecurityQuestionId into joined
                               from j in joined.DefaultIfEmpty()
                               where sqa.cub_ContactId.Id.Equals(contactId)
                               orderby sqa.cub_displayorder
                               select new WSSecurityQA()
                               {
                                   securityAnswer = sqa.cub_SecurityAnswerDesc,
                                   securityQuestion = j.cub_SecurityQuestionDesc,
                                   securityQuestionId = j.cub_SecurityQuestionId,
                                   securityAnswerId = sqa.cub_SecurityAnswerId,
                                   displayOrder = sqa.cub_displayorder.HasValue ? sqa.cub_displayorder.Value : 1
                               });

                if (returnValue != null)
                    listOfAnswers = returnValue.ToList();
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }

            return listOfAnswers;
        }

        /// <summary>
        /// Is Current Password
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="origPwd"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public PwdCredentials IsCurrentPwd(Guid customerId, Guid contactId, string origPwd)
        {

            return MatchedPwd(customerId, contactId, origPwd);
        }

        /// <summary>
        /// Match Credentials by Password
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="contactId">Contact Id</param>
        /// <param name="origPw">User current password in clear text (not encrypted)</param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        private PwdCredentials MatchedPwd(Guid customerId, Guid contactId, string origPwd)
        {
            PwdCredentials returnValue = null;

            try
            {
                returnValue = (from cred in CubOrgSvc.cub_CredentialSet
                               join cnt in CubOrgSvc.ContactSet on cred.cub_ContactId.Id equals cnt.ContactId into joined
                               from j in joined.DefaultIfEmpty()
                               where cred.cub_ContactId.Id.Equals(contactId)
                                   && cred.cub_Password.Equals(UtilityLogic.EncryptTripleDES(origPwd))
                                   && j.ParentCustomerId.Equals(customerId)
                               select new PwdCredentials()
                               { Id = cred.Id, Username = cred.cub_Username }
                    ).SingleOrDefault();
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Find Contact Credentals
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="CubOrgSvc"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        private PwdCredentials FindContactCred(Guid customerId, Guid contactId)
        {
            return MatchedContactCred(customerId, contactId);
        }

        /// <summary>
        /// Find Contact Credentals by Contact and Customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        private PwdCredentials MatchedContactCred(Guid customerId, Guid contactId)
        {
            PwdCredentials returnValue = null;
            try
            {
                returnValue = (from cred in CubOrgSvc.cub_CredentialSet
                               join cnt in CubOrgSvc.ContactSet on cred.cub_ContactId.Id equals cnt.ContactId into joined
                               from j in joined.DefaultIfEmpty()
                               where cred.cub_ContactId.Id.Equals(contactId)
                                   && j.ParentCustomerId.Equals(customerId)
                               select new PwdCredentials()
                               {
                                   Id = cred.Id,
                                   Username = cred.cub_Username,
                                   Old_Password = cred.cub_Password,
                                   Lock_Out_DateTime = cred.cub_LockoutDtm,
                                   Status = UtilityLogic.ConvertOptionSetObjectToStringName<cub_credential_statuscode>(cred.statuscode)
                               }
                    ).SingleOrDefault();
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Matched Cerdential By Contact", "err_general_find_matched_credential_by_contact_failed", e.Message));
            }
            return returnValue;
        }


        /// <summary>
        /// Getting list of security questions from MSD
        /// </summary>
        /// <returns>List<cub_securityquestion></returns>
        private List<cub_SecurityQuestion> GetListOfSecurityQuestions()
        {
            List<cub_SecurityQuestion> seqQuestions = new List<cub_SecurityQuestion>();
            var q = (from sq in CubOrgSvc.cub_SecurityQuestionSet
                     select sq
                     );
            if (q != null && q.Count() > 0)
                seqQuestions = q.ToList();
            return seqQuestions;
        }

        /// <summary>
        /// Get Security Question Id
        /// </summary>
        /// <param name="secQuestion">Security question</param>
        /// <returns>Security Question ID</returns>
        private Guid? GetSecurityQuestionId(string secQuestion)
        {
            Guid? secQuestionId;
            secQuestionId = (from sq in CubOrgSvc.cub_SecurityQuestionSet
                             where sq.cub_SecurityQuestionDesc == secQuestion
                             select sq.cub_SecurityQuestionId
                     ).FirstOrDefault();


            return secQuestionId;
        }

        /// <summary>
        /// Get list of previously used pwd
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        private ArchivedPwds[] GetPreviouslyUsedPwds(Guid customerId, Guid contactId)
        {
            string currentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<ArchivedPwds> listofPass = new List<ArchivedPwds>();

            try
            {
                IQueryable<cub_ArchivedPassword> archivedPasses;
                archivedPasses = (from pwds in CubOrgSvc.cub_ArchivedPasswordSet
                                  join cnt in CubOrgSvc.ContactSet on pwds.cub_ContactId.Id equals cnt.ContactId into joined
                                  from j in joined.DefaultIfEmpty()
                                  where pwds.cub_ContactId.Id.Equals(contactId)
                                      && j.ParentCustomerId.Equals(customerId)
                                  orderby pwds.cub_ArchivedDate ascending
                                  select pwds);

                if (archivedPasses != null && archivedPasses.ToList().Count() > 0)
                {
                    foreach (cub_ArchivedPassword pass in archivedPasses)
                    {
                        listofPass.Add(new ArchivedPwds() { Id = pass.Id, ArchivedDate = pass.cub_ArchivedDate.Value, Password = pass.cub_Password });
                    }
                }

            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault(currentMethod, CUBConstants.Error.ERR_GENERAL_FIND_USED_PASSWORDS_FAILED, e.Message));
            }

            return listofPass.ToArray();
        }

        /// <summary>
        /// Password Contains Dictionary Word
        /// </summary>
        /// <param name="pwd">password</param>
        /// <returns>Contain dictionary word or not</returns>
        public bool PasswordContainsDictionaryWord(string pwd)
        {
            return false; //bypass
        }

        /// <summary>
        /// Generate Token
        /// </summary>
        /// <param name="tokenType"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public string GenerateVerificationToken(cub_verificationtokentype tokenType)
        {
            string returnValue = string.Empty;

            try
            {
                int length = 8;
                if (length <= 0) length = 10;
                if (length < 4) length = 4;
                bool UPPER = false;
                bool LOWER = false;
                bool SPECIAL = false;
                bool NUMBER = false;
                bool USE_LETTER = true;
                bool THREE_OF_FOUR = false;
                string allowedUpperChars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
                string allowedLowerChars = "abcdefghijkmnpqrstuvwxyz";
                string allowedLetterChars = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
                string allowedOtherChars = "2!=3@4?5$6%7-^8_9*+";
                string allowedNumberChars = "23456789";
                string allowedSpecialChars = "!@$%^*?-_+=";
                Random rd = new Random();

                switch (tokenType)
                {
                    case cub_verificationtokentype.NewCustomerPassword:
                    case cub_verificationtokentype.Password:
                        length = GlobalsCache.PasswordMinLength(_organizationService);
                        if (GlobalsCache.PasswordMinLength(_organizationService) > length) length = GlobalsCache.PasswordMinLength(_organizationService);
                        if (GlobalsCache.PasswordUpperAlpha(_organizationService) <= 0 && GlobalsCache.PasswordLowerAlpha(_organizationService) <= 0 && GlobalsCache.PasswordNonAlphaNumeric(_organizationService) <= 0 && GlobalsCache.PasswordNumeric(_organizationService) <= 0)
                        {
                            UPPER = true;
                            LOWER = true;
                            SPECIAL = true;
                            NUMBER = true;
                        }
                        else
                        {
                            if (GlobalsCache.PasswordUpperAlpha(_organizationService) > 0) UPPER = true;
                            if (GlobalsCache.PasswordLowerAlpha(_organizationService) > 0) LOWER = true;
                            if (GlobalsCache.PasswordNonAlphaNumeric(_organizationService) > 0) SPECIAL = true;
                            if (GlobalsCache.PasswordNumeric(_organizationService) > 0) NUMBER = true;
                        }
                        break;
                    default:
                        UPPER = true;
                        LOWER = true;
                        SPECIAL = false;
                        NUMBER = true;
                        break;

                }

                if (UPPER && LOWER && SPECIAL && NUMBER) THREE_OF_FOUR = true;

                length = rd.Next(length, length + 4);
                char[] chars = new char[length];

                for (int i = 0; i < length; i++)
                {
                    if (UPPER || LOWER || SPECIAL || NUMBER)
                    {
                        int count = 0;

                        if (UPPER) count++;
                        if (LOWER) count++;
                        if (SPECIAL) count++;
                        if (NUMBER) count++;
                        bool DONE = false;

                        int choice = rd.Next(1, count);

                        switch (choice)
                        {
                            case 1:
                                if (!DONE && UPPER)
                                {
                                    chars[i] = allowedUpperChars[rd.Next(0, allowedUpperChars.Length)];
                                    UPPER = false;
                                    DONE = true;
                                }
                                if (!DONE && LOWER)
                                {
                                    chars[i] = allowedLowerChars[rd.Next(0, allowedLowerChars.Length)];
                                    LOWER = false;
                                    DONE = true;
                                }
                                if (!DONE && SPECIAL)
                                {
                                    chars[i] = allowedSpecialChars[rd.Next(0, allowedSpecialChars.Length)];
                                    SPECIAL = false;
                                    DONE = true;
                                }
                                if (!DONE && NUMBER)
                                {
                                    chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                                    NUMBER = false;
                                    DONE = true;
                                }
                                break;

                            case 2:
                                if (!DONE && LOWER)
                                {
                                    chars[i] = allowedLowerChars[rd.Next(0, allowedLowerChars.Length)];
                                    LOWER = false;
                                    DONE = true;
                                }
                                if (!DONE && SPECIAL)
                                {
                                    chars[i] = allowedSpecialChars[rd.Next(0, allowedSpecialChars.Length)];
                                    SPECIAL = false;
                                    DONE = true;
                                }
                                if (!DONE && NUMBER)
                                {
                                    chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                                    NUMBER = false;
                                    DONE = true;
                                }
                                if (!DONE && UPPER)
                                {
                                    chars[i] = allowedUpperChars[rd.Next(0, allowedUpperChars.Length)];
                                    UPPER = false;
                                    DONE = true;
                                }
                                break;

                            case 3:
                                if (!DONE && SPECIAL)
                                {
                                    chars[i] = allowedSpecialChars[rd.Next(0, allowedSpecialChars.Length)];
                                    SPECIAL = false;
                                    DONE = true;
                                }
                                if (!DONE && NUMBER)
                                {
                                    chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                                    NUMBER = false;
                                    DONE = true;
                                }
                                if (!DONE && UPPER)
                                {
                                    chars[i] = allowedUpperChars[rd.Next(0, allowedUpperChars.Length)];
                                    UPPER = false;
                                    DONE = true;
                                }
                                if (!DONE && LOWER)
                                {
                                    chars[i] = allowedLowerChars[rd.Next(0, allowedLowerChars.Length)];
                                    LOWER = false;
                                    DONE = true;
                                }
                                break;

                            default:
                                if (!DONE && NUMBER)
                                {
                                    chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                                    NUMBER = false;
                                    DONE = true;
                                }
                                if (!DONE && UPPER)
                                {
                                    chars[i] = allowedUpperChars[rd.Next(0, allowedUpperChars.Length)];
                                    UPPER = false;
                                    DONE = true;
                                }
                                if (!DONE && LOWER)
                                {
                                    chars[i] = allowedLowerChars[rd.Next(0, allowedLowerChars.Length)];
                                    LOWER = false;
                                    DONE = true;
                                }
                                if (!DONE && SPECIAL)
                                {
                                    chars[i] = allowedSpecialChars[rd.Next(0, allowedSpecialChars.Length)];
                                    SPECIAL = false;
                                    DONE = true;
                                }
                                break;
                        }

                        if (THREE_OF_FOUR && count == 2)
                        {
                            UPPER = false;
                            LOWER = false;
                            SPECIAL = false;
                            NUMBER = false;
                        }
                    }
                    else
                    {
                        if (USE_LETTER)
                        {
                            chars[i] = allowedLetterChars[rd.Next(0, allowedLetterChars.Length)];
                            USE_LETTER = false;
                        }
                        else
                        {
                            chars[i] = allowedOtherChars[rd.Next(0, allowedOtherChars.Length)];
                            USE_LETTER = true;
                        }
                    }
                }
                returnValue = new string(chars);
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Generate Token", "err_general_generate_token_failed", e.Message));
            }

            return returnValue;
        }
        #endregion Password

        #region Contact Validation
        /// <summary>
        /// Validate Contact For Insert\Update. Exit the function on first validation failure
        /// </summary>
        /// <param name="contact"></param>
        /// <returns>WSMSDResponse</returns>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        /// 
        public CubResponse<WSCustomerContact> ValidateContactForUpdate(WSCustomerContact contact)
        {
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            const string methodName = "ValidateContactForUpdate";
            try
            {
                DateTime contactBirthDate;

                if (!contact.contactId.HasValue)
                    response.Errors.Add(new WSMSDFault("ContactId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_CONTACT_NOT_FOUND));
                if (contact.customerId.HasValue)
                    response.Errors.Add(new WSMSDFault("CustomerId", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_CUSTOMER_ID_NULL));

                if (!string.IsNullOrEmpty(contact.dateOfBirth))
                {
                    if (!DateTime.TryParse(contact.dateOfBirth, out contactBirthDate))
                    {
                        response.Errors.Add(new WSMSDFault("Date Of Birth", CUBConstants.Error.ERR_INVALID_DATE_OF_BIRTH, CUBConstants.Error.MSG_INVALID_DATE_OF_BIRTH));
                        return response;
                    }
                }

                if (contact.contactTypeSet)
                {
                    CubResponse<WSCustomerContact> custConResponse = IsContactTypeValid(contact);
                    if (!custConResponse.Success)
                    {
                        response.Errors = custConResponse.Errors;
                        return response;
                    }
                }

                if (contact.nameSet)
                {
                    CubResponse<WSName> nameValidResponse = IsNameValid(contact.name);
                    if (!nameValidResponse.Success)
                    {
                        response.Errors = nameValidResponse.Errors;
                        return response;
                    }
                }

                if (contact.emailSet)
                {
                    EmailLogic emailLogic = new EmailLogic(_organizationService);
                    response = emailLogic.IsEmailValid(contact);
                    if (!response.Success)
                        return response;
                }

                if (contact.securityQASet)
                {
                    response = ValidateSecuirtyQA(contact);


                    if (!response.Success)
                        return response;
                }


                if (contact.addressId != null && contact.address != null)
                {
                    response.Errors.Add(new WSMSDFault("Address & AddressId", CUBConstants.Error.ERR_ADDRESS_AND_ADDID_NOT_ALLOWED, CUBConstants.Error.MSG_REQUEST_CONTAINS_BOTH_ADDRESS_AND_ID));
                    return response;
                }
                else
                {
                    AddressLogic addyLogic = new AddressLogic(_organizationService);
                    if (contact.addressIdSet && contact.addressId != null)
                    {
                        CubResponse<WSAddress> addrResponse = addyLogic.IsAddressIdValid(contact.addressId);
                        if (!addrResponse.Success)
                        {
                            response.Errors.AddRange(addrResponse.Errors);
                            return response;
                        }

                    }
                    else
                    {
                        if (contact.addressSet && contact.address != null)
                        {

                            CubResponse<WSAddress> addressResponse = addyLogic.IsAddressValid(contact.address);
                            if (!addressResponse.Success)
                            {
                                response.Errors.AddRange(addressResponse.Errors);
                                return response;
                            }
                        }
                    }
                }

                if (contact.phoneSet)
                {
                    response = IsPhoneValid(contact);
                    if (!response.Success)
                        return response;
                }

                if (contact.personalIdentifierInfoSet)
                {
                    response = IsPersonalIdentifierInfoValid(contact);
                    if (!response.Success)
                        return response;
                }

                if (contact.userNameSet)
                {
                    response = ValidateUserName(contact);
                    if (!response.Success)
                        return response;
                }

                if (contact.passwordSet)
                {
                    response = ValidatePassword(contact);
                    if (!response.Success)
                        return response;
                }

                if (contact.pinSet)
                {
                    response = ValidatePin(contact);
                    if (!response.Success)
                        return response;
                }

                response.ResponseObject = contact;


            }
            catch (Exception ex)
            {
                CubLogger.Error(ex.Message, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Validate Contact For Insert\Update. Exit the function on first validation failure
        /// </summary>
        /// <param name="contact"></param>
        /// <returns>WSMSDResponse</returns>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        /// 
        public CubResponse<WSCustomerContact> ValidateContactForInsert(WSCustomerContact contact)
        {
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            const string methodName = "ValidateContactForUpdate";
            try
            {
                DateTime contactBirthDate;
                //contact type is required
                if (string.IsNullOrEmpty(contact.contactType))
                {
                    response.Errors.Add(new WSMSDFault("Contact Type", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }

                if (!string.IsNullOrEmpty(contact.dateOfBirth))
                {
                    if (!DateTime.TryParse(contact.dateOfBirth, out contactBirthDate))
                    {
                        response.Errors.Add(new WSMSDFault("Date Of Birth", CUBConstants.Error.ERR_INVALID_DATE_OF_BIRTH, CUBConstants.Error.MSG_INVALID_DATE_OF_BIRTH));
                        return response;
                    }
                }

                CubResponse<WSCustomerContact> custConResponse = IsContactTypeValid(contact);
                if (!custConResponse.Success)
                {
                    response.Errors = custConResponse.Errors;
                    return response;
                }

                CubResponse<WSName> nameValidResponse = IsNameValid(contact.name);
                if (!nameValidResponse.Success)
                {
                    response.Errors = nameValidResponse.Errors;
                    return response;
                }

                EmailLogic emailLogic = new EmailLogic(_organizationService);
                response = emailLogic.IsEmailValid(contact);
                if (!response.Success)
                    return response;

                response = ValidateSecuirtyQA(contact);

                if (!response.Success)
                    return response;

                if (contact.addressId != null && contact.address != null)
                {
                    response.Errors.Add(new WSMSDFault("Address & AddressId", CUBConstants.Error.ERR_ADDRESS_AND_ADDID_NOT_ALLOWED, CUBConstants.Error.MSG_REQUEST_CONTAINS_BOTH_ADDRESS_AND_ID));
                    return response;
                }
                else
                {
                    AddressLogic addyLogic = new AddressLogic(_organizationService);
                    if (contact.addressId != null)
                    {
                        CubResponse<WSAddress> addrResponse = addyLogic.IsAddressIdValid(contact.addressId);
                        if (!addrResponse.Success)
                        {
                            response.Errors.AddRange(addrResponse.Errors);
                            return response;
                        }
                    }
                    else
                    {
                        if (contact.address != null)
                        {

                            CubResponse<WSAddress> addressResponse = addyLogic.IsAddressValid(contact.address);
                            if (!addressResponse.Success)
                            {
                                response.Errors.AddRange(addressResponse.Errors);
                                return response;
                            }
                        }
                    }
                }


                response = IsPhoneValid(contact);
                if (!response.Success)
                    return response;


                response = IsPersonalIdentifierInfoValid(contact);
                if (!response.Success)
                    return response;

                response = ValidateUserName(contact);
                if (!response.Success)
                    return response;

                response = ValidatePassword(contact);
                if (!response.Success)
                    return response;

                response = ValidatePin(contact);
                if (!response.Success)
                    return response;

                response.ResponseObject = contact;


            }
            catch (Exception ex)
            {
                CubLogger.Error(ex.Message, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Update Contact 
        /// </summary>
        /// <param name="contact">WSCustomerContact</param>
        /// <param name="callerName">Checking a caller�s name. If call was made from CreateAccount, then contact validation was already been done so we can skip it.</param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public CubResponse<WSCustomerContact> UpdateContact(WSCustomerContact contact, [CallerMemberName] string callerName = "")
        {
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            OptionSetTranslationLogic logic = new OptionSetTranslationLogic(_organizationService);
            const string methodName = "UpdateContact";
            try
            {
                DateTime contactBirthDate;

                //if method wasn't called from CreateAccount, then validation is needed

                if (string.Compare(callerName, "UpdateAccount", true) != 0)
                {
                    response = ValidateContactForUpdate(contact);
                    if (!response.Success)
                        return response;
                }
                Contact cnt = new Contact();
                cnt.ContactId = contact.contactId;
                if (contact.customerId.HasValue)
                {
                    cnt.ParentCustomerId = new EntityReference(Account.EntityLogicalName, contact.customerId.Value);
                }

                #region ContactInfo
                if (contact.contactTypeId.HasValue)
                {
                    cnt.cub_ContactTypeId = new EntityReference(cub_ContactType.EntityLogicalName, contact.contactTypeId.Value);
                }
                else
                {
                    if (contact.contactType != null)
                        cnt.cub_ContactTypeId = new EntityReference(cub_ContactType.EntityLogicalName, cub_ContactType.cub_nameAttribute, contact.contactType);
                }

                if (!string.IsNullOrEmpty(contact.dateOfBirth))
                {
                    if (DateTime.TryParse(contact.dateOfBirth, out contactBirthDate))
                    {
                        if (DateTime.MinValue != contactBirthDate)
                            cnt.BirthDate = DateTime.SpecifyKind(contactBirthDate, DateTimeKind.Utc);
                    }

                }

                if (!string.IsNullOrEmpty(contact.email)) cnt.EMailAddress1 = contact.email;


                if (contact.securityQAs != null)
                {
                    CubResponse<WSSecurityQA> addressResponse = UpdateSecurityQuestionAnswer(contact.securityQAs, contact.contactId.Value);
                    if (!addressResponse.Success)
                    {
                        response.Errors.AddRange(addressResponse.Errors);
                        return response;
                    }
                }

                if (contact.personalIdentifierInfo != null)
                {
                    cnt.cub_PersonalIdentifier = contact.personalIdentifierInfo.personalIdentifier;
                    cnt.cub_PersonalIdentifierType = new OptionSetValue(logic.GetValue(null, "Contactcub_PersonalIdentifierType", contact.personalIdentifierInfo.personalIdentifierType));
                }
                #endregion ContactInfo

                #region WSName
                if (contact.name != null)
                {
                    if (!string.IsNullOrEmpty(contact.name.title))
                        cnt.cub_NameTitle = new OptionSetValue(logic.GetValue(null, "cub_nametitles", contact.name.title));
                    cnt.FirstName = contact.name.firstName;
                    cnt.MiddleName = contact.name.middleInitial;
                    cnt.LastName = contact.name.lastName;
                    if (!string.IsNullOrEmpty(contact.name.nameSuffix))
                        cnt.cub_Suffix = new OptionSetValue(logic.GetValue(null, "cub_contactsuffix", contact.name.nameSuffix));
                }
                #endregion WSName


                #region WSAddress & AddressId
                if (contact.address != null)
                {
                    Guid addGuid;
                    if (contact.addressId != null && Guid.TryParse(contact.addressId, out addGuid))
                    {
                        var addy = new EntityReference { LogicalName = cub_Address.EntityLogicalName, Id = addGuid };
                        cnt.cub_AddressId = addy;
                    }
                    else
                    {
                        if (contact.address.addressId.HasValue)
                        {
                            cnt.cub_AddressId = new EntityReference(cub_Address.EntityLogicalName, contact.address.addressId.Value);
                        }
                        else if (contact.address != null)
                        {
                            AddressLogic add = new AddressLogic(_organizationService);
                            CubResponse<WSAddress> addCreatResp = add.ValidateAndCreateAddress(contact.address, contact.customerId.Value);
                            if (addCreatResp.Success)
                            {
                                cnt.cub_AddressId = new EntityReference { LogicalName = cub_Address.EntityLogicalName, Id = ((WSAddress)addCreatResp.ResponseObject).addressId.Value }; ;
                            }


                        }
                    }

                }
                #endregion WSAddress & AddressId

                #region Credentials
                //Update Credentials.... Are these really allowed to be changed
                bool updCred = false;
                WSCredentials updateCred = new WSCredentials();
                if (contact.username != null)
                {
                    updateCred.Username = contact.username;
                    updCred = true;
                }

                if (contact.password != null)
                {
                    updateCred.Password = contact.password;
                    updCred = true;
                }

                if (contact.pin != null)
                {
                    updateCred.Pin = contact.pin;
                    updCred = true;
                }

                if (updCred) UpdateCredentials(contact.customerId.Value, contact.contactId.Value, updateCred);
                #endregion Credentials

                //set all phone numbers in Contact object before updating to database
                SetContactPhones(contact, cnt);

                _organizationService.Update(cnt);

            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Create new Contact record
        /// </summary>
        /// <param name="contact">WSCustomerContact</param>
        /// <param name="callerName">Checking a caller�s name. If call was made from CreateAccount, then contact validation was already been done so we can skip it. </param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public CubResponse<WSCustomerContact> CreateContact(WSCustomerContact contact, [CallerMemberName] string callerName = "")
        {
            const string methodName = "CreateContact";
            CubLogger.Debug("Method Entered");
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            OptionSetTranslationLogic logic = new OptionSetTranslationLogic(_organizationService);

            Guid? newcontactId;

            try
            {
                DateTime contactBirthDate;
                if (string.Compare(callerName, "CreateAccount", true) != 0)
                {
                    response = ValidateContactForInsert(contact);
                    if (!response.Success)
                        return response;
                }

                Contact cnt = new Contact();

                #region ContactInfo
                if (contact.contactTypeId.HasValue)
                    cnt.cub_ContactTypeId = new EntityReference(cub_ContactType.EntityLogicalName, contact.contactTypeId.Value);
                else
                {
                    if (contact.contactType != null)
                        cnt.cub_ContactTypeId = new EntityReference(cub_ContactType.EntityLogicalName, cub_ContactType.cub_nameAttribute, contact.contactType);
                }

                if (!string.IsNullOrEmpty(contact.dateOfBirth))
                {
                    if (DateTime.TryParse(contact.dateOfBirth, out contactBirthDate))
                    {
                        if (DateTime.MinValue != contactBirthDate)
                            cnt.BirthDate = DateTime.SpecifyKind(contactBirthDate, DateTimeKind.Utc);
                    }
                }

                if (!string.IsNullOrEmpty(contact.email))
                    cnt.EMailAddress1 = contact.email;
                #endregion ContactInfo

                #region WSName
                if (contact.name != null)
                {
                    if (!string.IsNullOrEmpty(contact.name.title)) cnt.cub_NameTitle = new OptionSetValue(logic.GetValue(null, "cub_nametitles", contact.name.title));
                    if (!string.IsNullOrEmpty(contact.name.firstName)) cnt.FirstName = contact.name.firstName;
                    if (!string.IsNullOrEmpty(contact.name.middleInitial)) cnt.MiddleName = contact.name.middleInitial;
                    if (!string.IsNullOrEmpty(contact.name.lastName)) cnt.LastName = contact.name.lastName;
                    if (!string.IsNullOrEmpty(contact.name.nameSuffix)) cnt.cub_Suffix = new OptionSetValue(logic.GetValue(null, "cub_contactsuffix", contact.name.nameSuffix));
                }
                #endregion WSName

                //set all phone numbers in Contact object before adding to database
                SetContactPhones(contact, cnt);

                //assign contact to customer
                if (contact.customerId.HasValue)
                {
                    cnt.ParentCustomerId = new EntityReference(Account.EntityLogicalName, contact.customerId.Value);
                }

                #region WSAddress & AddressId
                if (contact.address != null && contact.address.addressId.HasValue)
                {
                    cnt.cub_AddressId = new EntityReference(cub_Address.EntityLogicalName, contact.address.addressId.Value);
                }
                else if (contact.address != null)
                {
                    AddressLogic add = new AddressLogic(_organizationService);
                    CubResponse<WSAddress> addCreatResp = add.ValidateAndCreateAddress(contact.address, contact.customerId.Value);
                    if (addCreatResp.Success)
                    {
                        cnt.cub_AddressId = new EntityReference(cub_Address.EntityLogicalName, addCreatResp.ResponseObject.addressId.Value);
                    }
                }
                #endregion WSAddress & AddressId

                #region Personal Identifier
                if (contact.personalIdentifierInfo != null)
                {
                    cnt.cub_PersonalIdentifier = contact.personalIdentifierInfo.personalIdentifier;

                    if (contact.personalIdentifierInfo.personalIdentifierTypeId.HasValue)
                        cnt.cub_PersonalIdentifierType = new OptionSetValue(contact.personalIdentifierInfo.personalIdentifierTypeId.Value);
                }
                #endregion Personal Identifier

                newcontactId = _organizationService.Create(cnt);
                contact.contactId = newcontactId;

                #region Security QAs
                if (contact.securityQAs != null)
                {
                    CubResponse<WSSecurityQA> secResponse = UpdateSecurityQuestionAnswer(contact.securityQAs, newcontactId.Value);
                    if (!secResponse.Success)
                    {
                        response.Errors.AddRange(secResponse.Errors);
                        return response;
                    }
                }
                #endregion Security QAs

                #region Credentials
                //Create credentials
                bool updCred = false;
                WSCredentials updateCred = new WSCredentials();

                if (contact.username != null)
                {
                    updateCred.Username = contact.username;
                    updCred = true;
                }

                if (contact.password != null)
                {
                    updateCred.Password = contact.password;
                    updCred = true;
                }

                if (contact.pin != null)
                {
                    updateCred.Pin = contact.pin;
                    updCred = true;
                }

                if (updCred) CreateCredentials(newcontactId.Value, updateCred);
                #endregion Credentials

                response.ResponseObject = contact;
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            CubLogger.Debug("Method Exit");
            return response;
        }

        /// <summary>
        /// Update Credentials
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="contactId"></param>
        /// <param name="updCred"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public bool UpdateCredentials(Guid customerId, Guid contactId, WSCredentials updCred)
        {
            bool returnValue = false;
            try
            {
                PwdCredentials cred = FindContactCred(customerId, contactId);
                if (cred != null && cred.Id.HasValue)
                {
                    cub_Credential crd = new cub_Credential();
                    crd.Id = cred.Id.Value;
                    if (updCred.Username != null) crd.cub_Username = updCred.Username;
                    if (updCred.Password != null) crd.cub_Password = updCred.Password;
                    if (updCred.Pin != null) crd.cub_Pin = updCred.Pin;

                    _organizationService.Update(crd);
                    returnValue = true;
                }
                else
                {
                    Guid credId = CreateCredentials(contactId, updCred);
                    updCred.Id = credId;
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Credential Update", "err_general_update_credentail_failed", e.Message));
            }
            return returnValue;
        }

        /// <summary>
        /// Update Security Question/Answer
        /// </summary>
        /// <param name="contactSQA">Contact Security Question/Answers</param>
        /// <param name="contactId">Contact ID</param>
        /// <returns>CubResponse<WSSecurityQA></returns>
        public CubResponse<WSSecurityQA> UpdateSecurityQuestionAnswer(WSSecurityQA[] contactSQA, Guid contactId)
        {
            CubResponse<WSSecurityQA> response = new CubResponse<WSSecurityQA>();
            const string methodName = "UpdateSecurityQuestionAnswer";
            try
            {
                if (contactSQA != null && contactSQA.Count() > 0)
                {
                    List<WSSecurityQA> currentSQA = GetSecurityAnswers(contactId);

                    foreach (WSSecurityQA sqa in contactSQA)
                    {
                        //Check if contact already has question in the system
                        var a = (from qa in currentSQA where qa.securityQuestionId == sqa.securityQuestionId select qa).FirstOrDefault();

                        // contact doesn�t have an answer to the question in the system, so enter a new record
                        if (a == null || !a.securityAnswerId.HasValue)
                        {
                            CubResponse<WSSecurityQA> secResponse = AddSecurityAnswer(sqa, contactId);
                            if (!secResponse.Success)
                            {
                                response.Errors.AddRange(secResponse.Errors);
                                return response;

                            }
                        }
                        //Contact already has an answer to the question in the system. Check if answer is different, and if so update the record with new answer
                        else
                        {
                            sqa.securityAnswerId = a.securityAnswerId;
                            CubResponse<WSSecurityQA> secResponse = UpdateSecurityAnswer(sqa);
                            if (!secResponse.Success)
                            {
                                response.Errors.AddRange(secResponse.Errors);
                                return response;

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }
            return response;
        }

        /// <summary>
        /// Create Credentials
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="updCred"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public Guid CreateCredentials(Guid contactId, WSCredentials updCred)
        {
            Guid returnValue = Guid.Empty;

            try
            {
                cub_Credential cred = new cub_Credential();
                cred.cub_ContactId = new EntityReference(Contact.EntityLogicalName, contactId);
                if (!string.IsNullOrEmpty(updCred.Password)) cred.cub_Password = updCred.Password;
                if (!string.IsNullOrEmpty(updCred.Username)) cred.cub_Username = updCred.Username;
                if (!string.IsNullOrEmpty(updCred.Pin)) cred.cub_Pin = updCred.Pin;

                returnValue = _organizationService.Create(cred);
            }
            catch (Exception ex)
            {
                LogAndReThrowException(ex);
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Credential Create", "err_general_create_credential_failed", e.Message));
            }
            return returnValue;
        }

        /// <summary>
        /// Validate Name. Exit on first validation failure
        /// name is the class WSName
        /// </summary>
        /// <param name="name">WSName</param>
        /// <returns>WSMSDResponse</returns>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public CubResponse<WSName> IsNameValid(WSName name)
        {
            const string methodName = "IsNameValid";
            CubResponse<WSName> response = new CubResponse<WSName>();
            OptionSetTranslationLogic logic = new OptionSetTranslationLogic(_organizationService);

            try
            {
                if (name == null)
                {
                    response.Errors.Add(new WSMSDFault("Name", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }


                if (!UtilityLogic.IsStringLengthValid(name.title, 20))
                {
                    response.Errors.Add(new WSMSDFault("Title", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                if (!string.IsNullOrEmpty(name.title))
                {
                    try
                    {
                        logic.GetValue(null, "cub_nametitles", name.title);
                    }
                    catch (KeyNotFoundException e)
                    {
                        response.Errors.Add(new WSMSDFault("Title", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_NAME_TITLE));
                        return response;
                    }
                }

                if (string.IsNullOrEmpty(name.firstName))
                {
                    response.Errors.Add(new WSMSDFault("First name", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }

                if (!UtilityLogic.IsStringLengthValid(name.firstName, 60))
                {
                    response.Errors.Add(new WSMSDFault("First name", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                if (!UtilityLogic.IsStringLengthValid(name.middleInitial, 1))
                {
                    response.Errors.Add(new WSMSDFault("Middle initial", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                if (string.IsNullOrEmpty(name.lastName))
                {
                    response.Errors.Add(new WSMSDFault("Last name", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }

                if (!UtilityLogic.IsStringLengthValid(name.lastName, 60))
                {
                    response.Errors.Add(new WSMSDFault("Last name", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }


                if (!UtilityLogic.IsStringLengthValid(name.nameSuffix, 20))
                {
                    response.Errors.Add(new WSMSDFault("Name suffix", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                if (!string.IsNullOrEmpty(name.nameSuffix))
                {
                    try
                    {
                        logic.GetValue(null, "cub_contactsuffix", name.nameSuffix);
                    }
                    catch (KeyNotFoundException e)
                    {
                        response.Errors.Add(new WSMSDFault("Name suffix", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_NAME_SUFFIX));
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {

                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Validate Contact Type. Exit function on first validation failure
        /// </summary>
        /// <param name="contact">WSCustomerContact</param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public CubResponse<WSCustomerContact> IsContactTypeValid(WSCustomerContact contact)
        {
            const string methodName = "IsContactTypeValid";
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            try
            {
                if (string.IsNullOrEmpty(contact.contactType))
                {
                    response.Errors.Add(new WSMSDFault("Contact Type", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }

                GlobalsLogic globalLogic = new GlobalsLogic(_organizationService);

                if (!UtilityLogic.IsStringLengthValid(contact.contactType, 20))
                {
                    response.Errors.Add(new WSMSDFault("Contact Type", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                //Check if contact type is valid value, if so add contact type id to WSCustomerContact
                Guid? contactTypeId = globalLogic.GetLookupRecord(cub_ContactType.EntityLogicalName, cub_ContactType.cub_nameAttribute, contact.contactType);
                if (!contactTypeId.HasValue)
                {
                    response.Errors.Add(new WSMSDFault("Contact Type", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_CONTACT_TYPE));
                    return response;
                }
                else
                    contact.contactTypeId = contactTypeId.Value;

            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                //throw new FaultException<WSMSDFault>(new WSMSDFault("MSD Contact Type Validate", "err_general_validate_contact_type_failed", e.Message));
            }

            return response;
        }

        /// <summary>
        /// Validate Contact Type
        /// </summary>
        /// <param name="contactType"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public CubResponse<WSCustomerContact> IsPhoneValid(WSCustomerContact contact)
        {
            const string methodName = "IsPhoneValid";
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            try
            {
                CubResponse<WSPhone> phoneResponse;
                if (contact.phone == null || contact.phone.Length == 0)
                {
                    response.Errors.Add(new WSMSDFault("Phone", CUBConstants.Error.ERR_GENERAL_EMPTY_LIST_NOT_ALLOWED, CUBConstants.Error.MSG_AT_LEAST_ONE_VALUE_REQUIRED));
                    return response;
                }


                foreach (WSPhone phn in contact.phone)
                {
                    phoneResponse = IsPhoneValid(phn);
                    if (!phoneResponse.Success)
                    {
                        response.Errors.AddRange(phoneResponse.Errors);
                        return response;
                    }

                }

                response.ResponseObject = contact;

            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Validate Contact Type
        /// </summary>
        /// <param name="contactType"></param>
        /// <history>
        /// Revision#   Date    Author  Description
        /// </history>
        public CubResponse<WSCustomerContact> IsPersonalIdentifierInfoValid(WSCustomerContact contact)
        {
            const string methodName = "IsPersonalIdentifierInfoValid";
            CubResponse<WSCustomerContact> response = new CubResponse<WSCustomerContact>();
            try
            {
                //If personalIdentifierInfo is not provided, exit the function
                if (contact.personalIdentifierInfo == null)
                {
                    response.ResponseObject = contact;
                    return response;

                }

                if (string.IsNullOrEmpty(contact.personalIdentifierInfo.personalIdentifier))
                {
                    response.Errors.Add(new WSMSDFault("Personal Identifier", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_PERSONAL_IDENTIFIER_VALUE_REQUIRED));
                    return response;
                }

                if (!UtilityLogic.IsStringLengthValid(contact.personalIdentifierInfo.personalIdentifier, 20))
                {
                    response.Errors.Add(new WSMSDFault("Personal Identifier", CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED));
                    return response;
                }

                if (string.IsNullOrEmpty(contact.personalIdentifierInfo.personalIdentifierType))
                {
                    response.Errors.Add(new WSMSDFault("Personal Identifier Type", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_PERSONAL_IDENTIFIER_TYPE_REQUIRED));
                    return response;
                }

                OptionSetTranslationLogic logic = new OptionSetTranslationLogic(_organizationService);

                try
                {
                    int? personalIdnetifierType = logic.GetValue("", "Contactcub_PersonalIdentifierType", contact.personalIdentifierInfo.personalIdentifierType);
                    if (personalIdnetifierType.HasValue && personalIdnetifierType.Value > 0)
                        contact.personalIdentifierInfo.personalIdentifierTypeId = personalIdnetifierType.Value;
                }
                catch (KeyNotFoundException e)
                {
                    response.Errors.Add(new WSMSDFault("Personal Identifier Type", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_PERSONAL_IDENTIFIER_TYPE));
                    return response;
                }

                response.ResponseObject = contact;

            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Check if a phone number is valid
        /// </summary>
        /// <param name="phone">Phone number</param>
        /// <returns>Phone valid or not</returns>
        public CubResponse<WSPhone> IsPhoneValid(WSPhone phone)
        {
            const string methodName = "IsPhoneValid";
            CubResponse<WSPhone> response = new CubResponse<WSPhone>();
            try
            {
                if (phone == null)
                {
                    response.Errors.Add(new WSMSDFault("Phone", CUBConstants.Error.ERR_GENERAL_EMPTY_LIST_NOT_ALLOWED, CUBConstants.Error.MSG_AT_LEAST_ONE_VALUE_REQUIRED));
                    return response;
                }



                if (string.IsNullOrEmpty(phone.number))
                {
                    response.Errors.Add(new WSMSDFault("Phone number", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }
                //else
                //if regex not null && regex check number format per country
                //if (!matches regex)
                //addressValidateErrors = glb.AddErrorTolist(addressValidateErrors, CUBConstants.Error.ERR_GENERAL_INVALID_PHONE_NUMBER, CUBConstants.Error.MSG_INVALID_PHONE_NUMBER);

                if (string.IsNullOrEmpty(phone.type))
                {
                    response.Errors.Add(new WSMSDFault("Phone type", CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, CUBConstants.Error.MSG_FIELD_IS_BLANK));
                    return response;
                }

                OptionSetTranslationLogic logic = new OptionSetTranslationLogic(_organizationService);

                try
                {
                    logic.GetValue("", "cub_phonetype", phone.type);
                }
                catch (KeyNotFoundException e)
                {
                    response.Errors.Add(new WSMSDFault("Phone type", CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, CUBConstants.Error.MSG_INVALID_PHONE_TYPE));
                    return response;
                }


                response.ResponseObject = phone;

            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                throw;
            }

            return response;
        }

        /// <summary>
        /// Check if a user name is available
        /// </summary>
        /// <param name="userName">User name</param>
        /// <returns>Boolean indicating if the user name is available</returns>
        private bool IsUserNameAvailable(string userName)
        {
            var result = (from c in CubOrgSvc.cub_CredentialSet
                          where c.cub_Username.Equals(userName)
                          select c.cub_CredentialId).FirstOrDefault();
            return (result == null);
        }

        #endregion Contact Validation

        /// <summary>
        /// Check the most recent verification activity for a contact created by a user.
        /// Returns an object containing values hasRecentActivity and isVerified.
        /// isVerified should only be used if hasRecentActivity is true.
        /// </summary>
        /// <param name="userId">User GUID</param>
        /// <param name="contactId">contact GUID</param>
        /// <returns>a duple of bools, hasRecentActivity which is true if an activity was created within the time cutoff, and isVerified which is true if that recent activity was VERIFIED</returns>
        public object GetCurrentVerificationStatus(Guid userId, Guid contactId)
        {
            var offset = GlobalsCache.RecentVerificationsOffsetInMinutes(_organizationService);
            var cutOffTime = DateTime.Now.ToUniversalTime().AddMinutes(-double.Parse(offset));
            var subjectVerified = GlobalsCache.VerificationSubjectVerified(_organizationService);

            var subject = (from v in CubOrgSvc.cub_VerificationSet
                           join n in CubOrgSvc.AnnotationSet on v.ActivityId equals n.ObjectId.Id
                           where v.ModifiedBy.Id == userId
                           where v.ModifiedOn > cutOffTime
                           where v.RegardingObjectId.Id == contactId
                           orderby v.ModifiedOn descending
                           select n.Subject).FirstOrDefault();

            var result = new
            {
                hasRecentActivity = !string.IsNullOrEmpty(subject),
                isVerified = subject == subjectVerified
            };
            return result;
        }

        public bool MaxSecurityQAsReached(Guid contactId)
        {
            var maxSecurityQuestions = int.Parse(GlobalsCache.CustomerRegistrationSecurityQuestionsMax(_organizationService));
            var existingAnswers = (from answer in CubOrgSvc.cub_SecurityAnswerSet
                                        where answer.cub_ContactId.Id == contactId
                                         && answer.statecode.Equals((int)cub_SecurityAnswerState.Active)
                                        select answer.cub_SecurityAnswerId).ToList();
            return existingAnswers.Count >= maxSecurityQuestions;
        }

        #region private methods
        /// <summary>
        /// Adding new security answer record for contact to the system
        /// </summary>
        /// <param name="securityAnswer">WSSecurityQA</param>
        /// <param name="contactId">contact Id</param>
        /// <returns>WSSecurityQA with new secuorty Id that was created</returns>
        private CubResponse<WSSecurityQA> AddSecurityAnswer(WSSecurityQA securityAnswer, Guid contactId)
        {
            CubResponse<WSSecurityQA> response = new CubResponse<WSSecurityQA>();
            const string methodName = "AddSecurityAnswer";
            try
            {
                cub_SecurityAnswer seqAnswer = new cub_SecurityAnswer();
                seqAnswer.cub_SecurityAnswerId = Guid.NewGuid();
                seqAnswer.cub_ContactId = new EntityReference(Contact.EntityLogicalName, contactId);
                seqAnswer.cub_SecurityQuestionId = new EntityReference(cub_SecurityAnswer.EntityLogicalName, securityAnswer.securityQuestionId.Value);
                seqAnswer.cub_SecurityAnswerDesc = securityAnswer.securityAnswer;

                Guid newSQAId = _organizationService.Create(seqAnswer);

                if (newSQAId != null)
                    securityAnswer.securityAnswerId = newSQAId;

                response.ResponseObject = securityAnswer;
            }


            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Updating security answer record for contact to the system
        /// </summary>
        /// <param name="securityAnswer">WSSecurityQA</param>
        /// <returns>WSSecurityQA with new secuorty Id that was created</returns>
        private CubResponse<WSSecurityQA> UpdateSecurityAnswer(WSSecurityQA securityAnswer)
        {
            CubResponse<WSSecurityQA> response = new CubResponse<WSSecurityQA>();
            const string methodName = "UpdateSecurityAnswer";
            try
            {

                cub_SecurityAnswer seqAnswer = (cub_SecurityAnswer)_organizationService.Retrieve(cub_SecurityAnswer.EntityLogicalName, securityAnswer.securityAnswerId.Value, new ColumnSet(new string[] { cub_SecurityAnswer.cub_securityquestionidAttribute, cub_SecurityAnswer.cub_securityanswerdescAttribute }));

                if (securityAnswer != null)
                {
                    seqAnswer.cub_SecurityQuestionId = new EntityReference(cub_SecurityAnswer.EntityLogicalName, securityAnswer.securityQuestionId.Value);
                    seqAnswer.cub_SecurityAnswerDesc = securityAnswer.securityAnswer;

                    _organizationService.Update(seqAnswer);

                }
                else
                {
                    response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, CUBConstants.Error.MSG_SECURITY_ANSWER_NOT_FOUND));

                }
            }
            catch (Exception ex)
            {
                CubLogger.Error(string.Empty, ex);
                response.Errors.Add(new WSMSDFault(methodName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
            }

            return response;
        }

        /// <summary>
        /// Set Contact Phones
        /// </summary>
        /// <param name="customerContact">Customer contact</param>
        /// <param name="cnt">Conact</param>
        private void SetContactPhones(WSCustomerContact customerContact, Contact cnt)
        {
            OptionSetTranslationLogic logic = new OptionSetTranslationLogic(_organizationService);
            int counter = 1;
            foreach (WSPhone ph in customerContact.phone)
            {
                if (counter == 1)
                {
                    cnt.Telephone1 = ph.number;
                    cnt.cub_Phone1Type = new OptionSetValue(logic.GetValue(null, "cub_phonetype", ph.type));
                }
                else if (counter == 2)
                {
                    cnt.Telephone2 = ph.number;
                    cnt.cub_Phone2Type = new OptionSetValue(logic.GetValue(null, "cub_phonetype", ph.type));
                }
                else if (counter == 3)
                {
                    cnt.Telephone3 = ph.number;
                    cnt.cub_Phone3Type = new OptionSetValue(logic.GetValue(null, "cub_phonetype", ph.type));
                }

                counter++;
            }

        }
        #endregion private methods
    }
}

