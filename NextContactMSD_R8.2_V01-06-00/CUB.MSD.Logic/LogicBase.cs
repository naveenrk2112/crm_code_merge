﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
//using System.Data.EntityClient;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
//using Microsoft.Xrm.Sdk.Client;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace CUB.MSD.Logic
{

    public class LogicBase : IDisposable
    {
        #region Member Variables
        protected IOrganizationService _organizationService;
        protected IPluginExecutionContext _executionContext;
        protected CUBOrganizationServiceContext CubOrgSvc;
        //private Credentials _credential;

        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        public LogicBase(IOrganizationService service)
        {
            _organizationService = service;
            CubOrgSvc = new CUBOrganizationServiceContext(_organizationService);
            CubLogger.Init(service);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">MSD organization service</param>
        /// <param name="executionContext">MSD plugin execution contect</param>
        public LogicBase(IOrganizationService service, IPluginExecutionContext executionContext) : this(service)
        {
            _executionContext = executionContext;
        }

        #endregion

        #region Properties
        /// <summary>
        ///   Decorate the log file name with who calls the logic, e.g., plugin, workflow, web
        /// </summary>
        public string LogSource { get; set; }

        //private Credentials Credential
        //{
        //    get
        //    {
        //        //suppose we have numerous sproc calls in the same class instance
        //        //lazy evauation here would prevent from having to fetch the credentials more than once.
        //        if (_credential == null)
        //        {
        //            _credential = GetCredentialsFromWebResource();
        //        }
        //        return _credential;
        //    }
        //    set { _credential = value; }
        //}

        #endregion

        /// <summary>
        /// Create Entity
        /// </summary>
        /// <param name="entity">Entity record</param>
        /// <param name="entityId">New Entity record ID</param>
        /// <returns>SaveChangesResultCollection</returns>
        protected SaveChangesResultCollection Create(Entity entity, out Guid entityId)
        {
            if (null == entity)
            {
                entityId = Guid.Empty;
                return null;
            }

            using (var osc = new CUBOrganizationServiceContext(_organizationService))
            {
                osc.AddObject(entity);
                SaveChangesResultCollection result = osc.SaveChanges();
                entityId = entity.Id;
                return result;
            }
        }

        /// <summary>
        /// Update Entity
        /// </summary>
        /// <param name="entity">Entity record</param>
        /// <returns>SaveChangesResultCollection</returns>
        protected SaveChangesResultCollection Update(Entity entity)
        {
            if (null == entity)
            {
                return null;
            }

            using (var osc = new CUBOrganizationServiceContext(_organizationService))
            {
                if (!osc.IsAttached(entity))
                {
                    osc.Attach(entity);
                }
                osc.UpdateObject(entity);
                SaveChangesResultCollection result = osc.SaveChanges();
                osc.Detach(entity);
                return result;
            }
        }

        /// <summary>
        /// Delete entity
        /// </summary>
        /// <param name="entity">Entity record</param>
        /// <returns>SaveChangesResultCollection</returns>
        protected SaveChangesResultCollection Delete(Entity entity)
        {
            if (null == entity)
            {
                return null;
            }

            using (var osc = new CUBOrganizationServiceContext(_organizationService))
            {
                if (!osc.IsAttached(entity))
                {
                    osc.Attach(entity);
                }
                osc.DeleteObject(entity);
                SaveChangesResultCollection result = osc.SaveChanges();
                osc.Detach(entity);
                return result;
            }
        }

        /// <summary>
        /// Set entity State
        /// </summary>
        /// <param name="entityId">Entity ID</param>
        /// <param name="entityLogicalName">Entity Logical Name</param>
        /// <param name="newStateValue">New State value</param>
        /// <param name="newStateName">New State NAme</param>
        /// <param name="statusCode">Status Code</param>
        protected void SetState(Guid entityId, string entityLogicalName, int newStateValue, string newStateName,
            int statusCode)
        {
            var orgServiceProxy = _organizationService as IOrganizationService;
            if (null == orgServiceProxy)
            {
                throw new Exception(
                    string.Format(
                        "LogicBase.SetState({0}): _organizationService is not of type IOrganizationService.",
                        entityLogicalName));
            }

            var entityReference = new EntityReference {LogicalName = entityLogicalName, Id = entityId};

            // validate the state transition. This only works on supported entities http://msdn.microsoft.com/en-us/library/microsoft.crm.sdk.messages.isvalidstatetransitionrequest.aspx
            if (entityLogicalName.ToLower() == "incident" || entityLogicalName.ToLower() == "opportunity")
            {
                var checkStateRequest = new IsValidStateTransitionRequest
                {
                    Entity = entityReference,
                    NewState = newStateName,
                    NewStatus = statusCode
                };
                var checkStateResponse = (IsValidStateTransitionResponse) orgServiceProxy.Execute(checkStateRequest);

                if (!checkStateResponse.IsValid)
                {
                    throw new Exception("LogicBase.SetState: Invalid state transition.");
                }
            }

            var stateRequest = new SetStateRequest
            {
                State = new OptionSetValue(newStateValue),
                Status = new OptionSetValue(statusCode),
                EntityMoniker = entityReference
            };

            orgServiceProxy.Execute(stateRequest);
        }

        /// <summary>
        /// Set State 2
        /// </summary>
        /// <param name="entityId">Entity ID</param>
        /// <param name="entityLogicalName">Entity Logical Name</param>
        /// <param name="newStateValue">State Value</param>
        /// <param name="statusCode">Status Code</param>
        public void SetState2(Guid entityId, string entityLogicalName, int newStateValue, int statusCode)
        {
            var entityReference = new EntityReference {LogicalName = entityLogicalName, Id = entityId};

            var stateRequest = new SetStateRequest
            {
                State = new OptionSetValue(newStateValue),
                Status = new OptionSetValue(statusCode),
                EntityMoniker = entityReference
            };


            _organizationService.Execute(stateRequest);
        }

        /// <summary>
        /// Get Sql Connection String
        /// </summary>
        /// <param name="orgname">Organization Name</param>
        /// <returns></returns>
        public string GetSqlConnectionString(string orgname)
        {
            string adminName;
            string adminPassword;
            string dbInstance;

            Credentials credentials = GetCredentialsFromWebResource();

            if (string.IsNullOrEmpty(credentials.crmAdminName) || string.IsNullOrEmpty(credentials.crmAdminPassword) ||
                string.IsNullOrEmpty(credentials.dbInstance))
            {
                //If we get here it's because the credentials web resource wasn't there.
                //revert back to the settingings file, which should have production values.
                adminName = GetAdminNameFromSettings();
                adminPassword = GetAdminPasswordFromSettings();
                dbInstance = GetDBInstance(orgname);
            }
            else
            {
                //credential has been populated from the web resource
                adminName = credentials.crmAdminName;
                adminPassword = credentials.crmAdminPassword;
                dbInstance = credentials.dbInstance;
            }

            var builder = new SqlConnectionStringBuilder();
            builder.DataSource = dbInstance;
            builder.InitialCatalog = orgname + "_mscrm";
            builder.IntegratedSecurity = true;
            builder.UserID = adminName;
            builder.Password = adminPassword;

            return builder.ToString();
        }

        // logging using log4net
        //public void LogEvent(string orgName, string message, LogLevel logLevel, Exception exception = null)
        //{
        //    try
        //    {
        //        // if the org isn't set up to log at this level, get out
        //        if (!Logger.DoLog(orgName, logLevel))
        //            return;

        //        MethodBase method = null;
        //        try
        //        {
        //            var stacktrace = new StackTrace();
        //            method = stacktrace.GetFrame(1).GetMethod();
        //        }
        //        catch (Exception)
        //        {
        //            method = null;
        //        }

        //        try
        //        {
        //            Logger.Log(logLevel, message, orgName, exception, method, LogSource);
        //        }
        //        catch (Exception e)
        //        {
        //            Trace.WriteLine(e.ToString());
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        /// <summary>
        /// Retieve an CRM entity record based on the record ID
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="id">Entity record id</param>
        /// <param name="columns">List of columns</param>
        /// <returns>Entity record or null if nout found</returns>
        public T Retrieve<T>(Guid id, params string[] columns) where T : Entity
        {
            if (columns?.Length == 0)
                throw new Exception("The columns paramter is required");
            string entityName = (string) typeof(T).GetField("EntityLogicalName").GetValue(null);
            return (T)_organizationService.Retrieve(entityName, id, new ColumnSet(columns));
        }

        #region settings helpers

        //this could probably be cached on an org basis since the credentials web resource will rarely be changed.
        /// <summary>
        /// Get Credentials From Web Resource
        /// </summary>
        /// <returns>Credentials</returns>
        public Credentials GetCredentialsFromWebResource()
        {
            var credentials = new Credentials();
            WebResource webResourceCredential = GetWebResourceCredential();
            if (webResourceCredential == null)
            {
                CubLogger.Debug("Credentials web resource wasn't found, reverting back to the settings file ");

                return credentials;
                //nothing has been fetched so we're returning an empty credential which will be checked by the caller.
            }

            byte[] binary = {};
            try
            {
                binary = Convert.FromBase64String(webResourceCredential.Content);
            }
            catch (Exception ex)
            {
                CubLogger.Error("Credentials web resource content conversion failed. ", ex);
                return credentials;
            }

            string resourceContent;

            try
            {
                resourceContent = Encoding.UTF8.GetString(binary);
            }
            catch (Exception ex)
            {
                CubLogger.Error("Credentials web resource content encoding failed. ", ex);
                return credentials;
            }

            XDocument xDoc = XDocument.Parse(resourceContent);

            XElement crmAdminPassword = xDoc.XPathSelectElement("//crmAdminPassword");
            if (crmAdminPassword == null)
            {
                CubLogger.Debug("crmAdminPassword missing from credentials web resource. ");
            }
            else
            {
                credentials.crmAdminPassword = StringCipher.Decrypt(crmAdminPassword.Value, "weckl");
            }

            XElement crmAdminName = xDoc.XPathSelectElement("//crmAdminName");
            if (crmAdminName == null)
            {
                CubLogger.Debug("crmAdminName missing from credentials web resource. ");
            }
            else
            {
                credentials.crmAdminName = crmAdminName.Value;
            }

            XElement dbInstance = xDoc.XPathSelectElement("//dbInstance");
            if (dbInstance == null)
            {
                CubLogger.Debug("dbInstance missing from credentials web resource. ");
            }
            else
            {
                credentials.dbInstance = dbInstance.Value;
            }

            XElement reportServerUrl = xDoc.XPathSelectElement("//reportServerUrl");  //seems to be used primarly for the the email overloads
            if (dbInstance == null)
            {
                CubLogger.Debug("reportServerUrl missing from credentials web resource. ");
            }
            else
            {
                credentials.reportServerUrl = reportServerUrl.Value;
            }

            return credentials;
        }

        /// <summary>
        /// Get Admin Name From Settings
        /// </summary>
        /// <returns>Admin Name</returns>
        public string GetAdminNameFromSettings()
        {
            // todo   string adminName = Settings.Default.crmAdminName;

            //   return adminName;
            return "";
        }

        /// <summary>
        /// Get Web Resource Credential
        /// </summary>
        /// <returns>WebResource</returns>
        private WebResource GetWebResourceCredential()
        {
            //using (var osc = new Microsoft.Xrm.Sdk.Client.OrganizationServiceContext(_organizationService))
            //{
            //    WebResource webResourceCredential = (from wr in osc.WebResourceSet
            //                                         where wr.WebResourceType.Value == (int)WebResourceWebResourceType.Data_XML
            //                                         where wr.Name == "<WEB RESOURCE NAME"
            //                                         select wr).FirstOrDefault();
            //    return webResourceCredential;
            //}
            // temp
            return new WebResource();
        }

        /// <summary>
        /// Get Admin Password From Settings
        /// </summary>
        /// <returns>Admin Password</returns>
        public string GetAdminPasswordFromSettings()
        {
            //string adminPassword = Settings.Default.crmAdminPassword;
            //return adminPassword;
            return "";
        }

        /// <summary>
        /// GetDBInstance
        /// </summary>
        /// <param name="orgName">MSD ORganization Name</param>
        /// <returns>DB instance</returns>
        public string GetDBInstance(string orgName)
        {
            //string dbInstance = Settings.Default.dbInstance;
            //return dbInstance;
            return "";
        }

        //helper class used to contain the results of a call to the web resource credentials
        /// <summary>
        /// Credentials
        /// </summary>
        public class Credentials
        {
            public string crmAdminName;
            public string crmAdminPassword;
            public string dbInstance;
            public string reportServerUrl;
        }

        #region encryption

        // added these classed to encrypt the admin password that is stored in the web resource.
        // usages:
        //
        // var encrypt = LogicBase.StringCipher.Encrypt("somepassword", "<PWORD>");  
        // var decrypt = LogicBase.StringCipher.Decrypt(encrypt, "<PWORD>");
        public static class StringCipher
        {
            // This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
            // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
            // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.

            // This constant is used to determine the keysize of the encryption algorithm.
            private const int keysize = 256;
            private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");

            public static string Encrypt(string plainText, string passPhrase)
            {
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                using (var password = new PasswordDeriveBytes(passPhrase, null))
                {
                    byte[] keyBytes = password.GetBytes(keysize/8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.Mode = CipherMode.CBC;
                        using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                        {
                            using (var memoryStream = new MemoryStream())
                            {
                                using (
                                    var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
                                    )
                                {
                                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                    cryptoStream.FlushFinalBlock();
                                    byte[] cipherTextBytes = memoryStream.ToArray();
                                    return Convert.ToBase64String(cipherTextBytes);
                                }
                            }
                        }
                    }
                }
            }

            /// <summary>
            /// Decrypt
            /// </summary>
            /// <param name="cipherText">Cipher Text</param>
            /// <param name="passPhrase">Pass Phrase</param>
            /// <returns>Decrypted</returns>
            public static string Decrypt(string cipherText, string passPhrase)
            {
                byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
                using (var password = new PasswordDeriveBytes(passPhrase, null))
                {
                    byte[] keyBytes = password.GetBytes(keysize/8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.Mode = CipherMode.CBC;
                        using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                        {
                            using (var memoryStream = new MemoryStream(cipherTextBytes))
                            {
                                using (
                                    var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                {
                                    var plainTextBytes = new byte[cipherTextBytes.Length];
                                    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                    return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Log and Re Throw Exception
        /// </summary>
        /// <param name="ex">Exception</param>
        internal void LogAndReThrowException(Exception ex)
        {
            CubLogger.Error(string.Empty, ex);
            throw ex;
        }

        /// <summary>
        /// Dispoase
        /// </summary>
        public void Dispose()
        {
            CubOrgSvc.Dispose();

        }

        #endregion

        /// <summary>
        /// Get Attribute Option Set Text
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <param name="attributeName">Attribute Name</param>
        /// <param name="optionSetType">OptionSet Type</param>
        /// <param name="translate">Translate</param>
        /// <returns>Option Set Text</returns>
        public string GetAttributeOptionSetText(Entity entity, string attributeName, Type optionSetType, bool translate = false)
        {
            if (!entity.Contains(attributeName))
            {
                return string.Empty;
            }
            int value;

            if (entity[attributeName] is AliasedValue)
                value = ((OptionSetValue)((AliasedValue)entity[attributeName]).Value).Value;
            else
                value = ((OptionSetValue)entity[attributeName]).Value;

            if (translate)
            { 
                UtilityLogic utilLogic = new UtilityLogic();
                List<KeyValuePair<int, string>> optionSet = utilLogic.GetOptionSetMetaData(optionSetType);
                OptionSetTranslationLogic logic = new OptionSetTranslationLogic(_organizationService);
                return logic.GetText(null, optionSetType.Name, value);
            }
            else
                return Enum.GetName(optionSetType, value);
        }

        /// <summary>
        /// Get Attribute Value
        /// </summary>
        /// <param name="targetEntity">Target entity</param>
        /// <param name="attributeName">Attribute Name</param>
        /// <returns>Attribute value</returns>
        public object GetAttributeValue(Entity targetEntity, string attributeName)
        {
            if (string.IsNullOrEmpty(attributeName) ||
                !targetEntity.Contains(attributeName))
            {
                return null;
            }
            if (targetEntity[attributeName] is AliasedValue)
            {
                return (targetEntity[attributeName] as AliasedValue).Value;
            }
            else
            {
                return targetEntity[attributeName];
            }
        }

        /// <summary>
        /// Return String value
        /// </summary>
        /// <param name="objData">Object</param>
        /// <param name="formatType">Format Type</param>
        /// <returns></returns>
        public string ReturnStringValue(object objData, string formatType = null)
        {
            if (objData == null)
            {
                return null;
            }
            else
            {
                Type t = objData.GetType();

                if (t.Equals(typeof(string)))
                {
                    if (!string.IsNullOrEmpty(formatType) && formatType.ToUpper().Equals("BDAY"))
                    {
                        DateTime dob = DateTime.SpecifyKind((DateTime)objData, DateTimeKind.Utc);
                        return dob.ToString("o");
                    }
                    else
                    {
                        return objData.ToString();
                    }
                }
                else if (t.Equals(typeof(EntityReference)))
                {
                    EntityReference entRef = (EntityReference)objData;

                    if (!string.IsNullOrEmpty(formatType) && formatType.ToUpper().Equals("ID"))
                    {
                        return entRef.Id.ToString().ToUpper();
                    }

                    return entRef.Name;
                }
                else if (t.Equals(typeof(DateTime)))
                {
                    DateTime dob = DateTime.SpecifyKind((DateTime)objData, DateTimeKind.Utc);
                    return dob.ToString("o");
                }
                else if (t.Equals(typeof(Guid)))
                {
                    return objData.ToString().ToUpper();
                }
                else
                {
                    return objData.ToString();
                }
            }
        }


        #endregion

        /// <summary>
        /// Retrieve an entity's type code number. This is important when using
        /// type codes because custom entity type codes can vary by organization.
        /// </summary>
        /// <param name="logicalName">Entity logical name</param>
        /// <returns>Entity type code</returns>
        public int GetEntityTypeCode(string logicalName)
        {
            if (string.IsNullOrEmpty(logicalName))
            {
                throw new ArgumentNullException("logicalName");
            }
            var request = new RetrieveEntityRequest
            {
                LogicalName = logicalName,
                EntityFilters = EntityFilters.Entity
            };
            var response = (RetrieveEntityResponse)_organizationService.Execute(request);
            if ((response?.EntityMetadata?.ObjectTypeCode).HasValue)
            {
                return response.EntityMetadata.ObjectTypeCode.Value;
            }
            else
            {
                throw new NullReferenceException($"Unable to get type code for entity {logicalName}");
            }
        }
    }
}
