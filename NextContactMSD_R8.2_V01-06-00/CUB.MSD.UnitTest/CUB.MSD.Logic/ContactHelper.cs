/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using CUB.MSD.Model;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    public class ContactHelper
    {
       
        public static Entity GetContact(Guid AccountId)
        {
           return  GetContact(Guid.NewGuid(), AccountId);
        }

        public static Entity GetContact(Guid contactId, Guid AccountId)
        {
            Entity data = (new Contact
            {
                ParentCustomerId = new EntityReference(Account.EntityLogicalName, AccountId),
                ContactId = contactId,
                FirstName = "Tony",
                MiddleName = "S",
                LastName = "Riak",
                EMailAddress1 = "tonyriak@cubic.com",

                Telephone1 = "(508)415-8569",
                cub_Phone1Type = new OptionSetValue(971880001),
                Telephone2 = "(798)512-8965",
                cub_Phone2Type = new OptionSetValue(971880002),
                Telephone3 = "(785)986-5236",
                cub_Phone3Type = new OptionSetValue(971880000)
            });
            return data;
        }

        public static Entity GetCountry(Guid countryId)
        {
            Entity data = (new cub_Country
            {
                cub_CountryName = "US",
                cub_CountryId= countryId
            }
                );

            return data;
        }

        public static List<Entity> GetSecurityQuestions()
        {
            List<Entity> data = new List<Entity>();
                data.Add(new cub_SecurityQuestion
                {
                    cub_SecurityQuestionId = Guid.NewGuid(),
                    cub_SecurityQuestionDesc = "What was your mother's maiden name?"
                }

                   );

            data.Add(new cub_SecurityQuestion
            {
                cub_SecurityQuestionId = Guid.NewGuid(),
                cub_SecurityQuestionDesc = "What was the make of your first car?"
            });



                data.Add(new cub_SecurityQuestion
                {
                    cub_SecurityQuestionId = Guid.NewGuid(),
                    cub_SecurityQuestionDesc = "What was your mother's maiden name?"
                }

                   );

            return data;
        }

      
    }
}
