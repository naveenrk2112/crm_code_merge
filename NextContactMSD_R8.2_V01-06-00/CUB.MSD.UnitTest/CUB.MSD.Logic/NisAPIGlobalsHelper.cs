/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using CUB.MSD.Model;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    public class NisAPIGlobalsHelper
    {
        public static void GetNisApiGlobals(List<Entity> data)
        {
            EntityReference nisApiGlobalID = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());
            EntityReference tollingApiGlobalID = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());
            EntityReference cubMsdWebAngularGlobalID = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());
            data.Add(new cub_Globals
            {
                cub_GlobalsId = nisApiGlobalID.Id,
                cub_Name = "NISApi",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "BaseUrl",
                cub_AttributeValue = "http://usdcep-bapp01.cts.cubic.cub:8201/nis",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "Device",
                cub_AttributeValue = "MSD",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "Username",
                cub_AttributeValue = "username",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "Password",
                cub_AttributeValue = "password",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TransitAccount",
                cub_AttributeValue = "/nwapi/v2/transitaccount/{0}/subsystem/{1}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CompleteRegistrationPath",
                cub_AttributeValue = "/nwapi/v2/customer/{0}/completeregistration",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "SearchTokenPath",
                cub_AttributeValue = "/csapi/v1/subsystem/{0}/traveltoken/search",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = CUBConstants.Globals.NIS_API_REPORT_CHANGE,
                cub_AttributeValue = "/nwapi/v2/customer/{0}/contact/{1}/reportchange",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = CUBConstants.Globals.NIS_API_UPDATE_DEPENDANCIES,
                cub_AttributeValue = "/nwapi/v2/customer/{0}/address/{1}/updatedependencies",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_BALANCE_HISTORY,
                cub_AttributeValue = "/nwapi/v2/transitaccount/{0}/subsystem/{1}/balancehistory?startDateTime={2}T00:00:00.000Z&endDateTime={3}T23:59:59.999Z",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_TRAVEL_HISTORY,
                cub_AttributeValue = "/nwapi/v2/transitaccount/{0}/subsystem/{1}/travelhistory?startDateTime={2}T00:00:00.000Z&endDateTime={3}T23:59:59.999Z",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "LinkSubssystemToCustomerAccount",
                cub_AttributeValue = "/nwapi/v2/oneaccount/{0}/subsystem/{1}/subsystemaccount/{2}/link",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "NotificationPath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/notification",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "NotificationDetailPath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/notification/{1}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "NotificationResendPath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/contact/{1}/resend",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "ConfigNotificationChannelPath",
                cub_AttributeValue = "/csapi/v1/config/notificationchannel",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "OneAccountSummaryPath",
                cub_AttributeValue = "/csapi/v1/oneaccount/customer/{0}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CustomerNotificationPreferecesPath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/notificationpreferences?contactId={1}&reqType={2}&channel={3}&type={4}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordReportChangePath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/contact/{1}/password/reportchange",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CustomerNotificationPreferencesPatchPath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/notificationpreferences",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "OneAccountBalanceHistoryPath",
                cub_AttributeValue = "/csapi/v1/oneaccount/{0}/balancehistory",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "OneAccountBalanceHistoryDetailPath",
                cub_AttributeValue = "/csapi/v1/oneaccount/{0}/journalentry/{1}/balancehistorydetail",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "OneAccountTravelHistoryPath",
                cub_AttributeValue = "/csapi/v1/oneaccount/{0}/travelhistory",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "OneAccountTravelHistoryDetailPath",
                cub_AttributeValue = "/csapi/v1/oneaccount/{0}/transaction/{1}/travelhistorydetail",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TransitAccountTravelHistoryDetailPath",
                cub_AttributeValue = "csapi/v1/transitaccount/{0}/subsystem/{1}/transaction/{2}/travelhistorydetail",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "FundingSourcePostPath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/fundingsource",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "OrderDebtCollectPath",
                cub_AttributeValue = "/csapi/v1/order/debtcollect",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CustomerOrderPath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/order/{1}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "SubsystemTravelTokenCanBeLinkedPath",
                cub_AttributeValue = "/csapi/v1/subsystem/{0}/traveltoken/canbelinked",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TransitAccountBankCardChargeHistoryPath",
                cub_AttributeValue = "/csapi/v1/transitaccount/{0}/subsystem/{1}/bankcardchargehistory",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CustomerActivityPath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/activity",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TransitAccountBankChargeAggregationDetailsPath",
                cub_AttributeValue = "/csapi/v1/transitaccount/{0}/subsystem/{1}/bankcardcharge/{2}/aggregation",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "x-cub-hdr.Tracking.TrackingType",
                cub_AttributeValue = "CRM Session",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "x-cub-hdr.appId",
                cub_AttributeValue = "MSD",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "x-cub-audit.location",
                cub_AttributeValue = "MSD WEB",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "x-cub-audit.channel",
                cub_AttributeValue = "MSD",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CustomerOrderHistoryPath",
                cub_AttributeValue = "/csapi/v1/customer/{0}/order/history",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TransitAccountOrderHistoryPath",
                cub_AttributeValue = "/csapi/v1/transitaccount/{0}/subsystem/{1}/order/history",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CustomerServiceReasonCodesPath",
                cub_AttributeValue = "/csapi/v1/customerservice/reasoncodes",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "OrderTravelCorrectionPath",
                cub_AttributeValue = "/csapi/v1/order/travelcorrection",
                cub_Enabled = true
            });

            /*
             * Tooling
             */
            data.Add(new cub_Globals
            {
                cub_GlobalsId = tollingApiGlobalID.Id,
                cub_Name = "TollingAPI",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = tollingApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TollingGetAccountSummaryPath",
                cub_AttributeValue = "/api/tam/getAccountSummary?customerId={0}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = tollingApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TollingGetTransactionHistoryPath",
                cub_AttributeValue = "/api/tam/getTrxnHistory?customerId={0}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = tollingApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TollingGetTranspondersPath",
                cub_AttributeValue = "/api/tam/getTrxnHistory?customerId={0}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = tollingApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TollingBaseUrl",
                cub_AttributeValue = "https://app.avatardemo.com.au/CUBIC-API-0.1-SNAPSHOT",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = tollingApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TollingGetVehiclesPath",
                cub_AttributeValue = "/api/tam/getVehicles?customerId={0}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "DelinkSubsystemFromCustomerAccount",
                cub_AttributeValue = "/csapi/v1/oneaccount/{0}/subsystem/{1}/subsystemaccount/{2}/delink",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TransitAccountStatusHistoryPath",
                cub_AttributeValue = "/csapi/v1/transitaccount/{0}/subsystem/{1}/statushistory",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TransitAccountBalanceHistoryPath",
                cub_AttributeValue = "/csapi/v1/transitaccount/{0}/subsystem/{1}/balancehistory",
                cub_Enabled = true
            });
            data.Add(new cub_Globals
            {
                cub_GlobalsId = cubMsdWebAngularGlobalID.Id,
                cub_Name = "CUB.MSD.Web.Angular",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = cubMsdWebAngularGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "RecentVerificationsOffsetInMinutes",
                cub_AttributeValue = "5",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = cubMsdWebAngularGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "SubjectVerified",
                cub_AttributeValue = "Verified",
                cub_Enabled = true
            });
        }
    }
}
