﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Collections.Generic;
using CUB.MSD.Logic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using System.Linq;
using CUB.MSD.Web.Models;
using Microsoft.QualityTools.Testing.Fakes;
using System.Net.Http.Fakes;
using System.Reflection;
using System.Net.Http;
using System.Net;
using CUB.MSD.Model.NIS;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class SubsystemLogicTest
    {
        [TestMethod]
        public void TravelTokenCanBeLinkedTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            SubsystemLogic logic = new SubsystemLogic(fakedService);

            string message = "{\"Header\": {\"result\": \"Successful\",\"uid\": \"d8a14e71-2cc2-498b-a1d0-fca680cb2f77\",\"fieldName\": \"\",\"errorKey\": \"\",\"errorMessage\": \"\"},\"Body\": \"{\"subsystemAccountReference\":\"330000000019\",\"tokenVerificationInfo\":{\"type\":\"ABP\",\"transactions\":[{\"transactionDateTime\":\"2017-12-01T21:41:05.000Z\",\"transactionType\":\"Trip\",\"transactionDescription\":\"1, 1\",\"transactionAmount\":\"760\"},{\"transactionDateTime\":\"2017-11-30T18:23:47.000Z\",\"transactionType\":\"Value Load\",\"transactionDescription\":\"Load value for Unrestricted\",\"product\":\"Stored Value\",\"transactionAmount\":\"5000\"},{\"transactionDateTime\":\"2017-11-17T17:49:57.000Z\",\"transactionType\":\"Trip\",\"transactionDescription\":\"1, 1\",\"transactionAmount\":\"760\"},{\"transactionDateTime\":\"2017-11-15T16:59:20.000Z\",\"transactionType\":\"Trip\",\"transactionDescription\":\"1, 1\",\"transactionAmount\":\"0\"},{\"transactionDateTime\":\"2017-11-15T16:59:17.000Z\",\"transactionType\":\"Trip\",\"transactionDescription\":\"1, 1\",\"transactionAmount\":\"760\"},{\"transactionDateTime\":\"2017-11-02T22:34:22.000Z\",\"transactionType\":\"Adjustment\",\"transactionDescription\":\"Adjustment of Stored Value for Unrestricted\",\"product\":\"Stored Value\",\"transactionAmount\":\"-1000\"},{\"transactionDateTime\":\"2017-11-02T22:29:22.000Z\",\"transactionType\":\"Adjustment\",\"transactionDescription\":\"Adjustment of Stored Value for Unrestricted\",\"product\":\"Stored Value\",\"transactionAmount\":\"-1000\"},{\"transactionDateTime\":\"2017-11-02T22:28:52.000Z\",\"transactionType\":\"Adjustment\",\"transactionDescription\":\"Adjustment of Stored Value for Unrestricted\",\"product\":\"Stored Value\",\"transactionAmount\":\"-1000\"},{\"transactionDateTime\":\"2017-10-25T16:10:54.000Z\",\"transactionType\":\"Trip\",\"transactionDescription\":\"1, 1\",\"transactionAmount\":\"760\"},{\"transactionDateTime\":\"2017-10-24T23:38:47.000Z\",\"transactionType\":\"Trip\",\"transactionDescription\":\"66, 66\",\"transactionAmount\":\"0\"}]}}\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                //WSBankcardTravelToken token = new WSBankcardTravelToken
                //{
                //   tokenType = "Bankcard",
                //   PAN = "4692012299990010",
                //   cardExpiryMMYY = "1018"
                //};
                //var response = logic.TravelTokenCanBeLinked("ABP", token, null, null, null, true);
                //Assert.AreEqual(response.Header.result, "Successful");
                //Assert.IsNotNull(response.Body);
            }
        }
    }
}
