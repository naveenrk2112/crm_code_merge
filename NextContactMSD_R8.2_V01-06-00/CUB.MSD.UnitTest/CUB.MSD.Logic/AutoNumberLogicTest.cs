﻿using CUB.MSD.Logic;
using CUB.MSD.Model;
using FakeXrmEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class AutoNumberLogicTest
    {
        [TestMethod]
        public void GetNextAutoNumberByIdTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            var startingNumber = 0;
            var autoNumber = new cub_AutoNumber
            {
                Id = Guid.NewGuid(),
                cub_Number = startingNumber
            };
            var data = new List<Entity>();
            data.Add(autoNumber);
            fakedContext.Initialize(data);
            var logic = new AutoNumberLogic(fakedService);

            var next = logic.GetNextAutoNumberById(autoNumber.Id);
            Assert.AreEqual((1 + startingNumber).ToString(), next);
            var updated = fakedService.Retrieve(cub_AutoNumber.EntityLogicalName, autoNumber.Id, new ColumnSet(cub_AutoNumber.cub_updatingAttribute)).ToEntity<cub_AutoNumber>();
            Assert.IsNotNull(updated.cub_Updating);
            var updatedDT = updated.cub_Updating;

            next = logic.GetNextAutoNumberById(autoNumber.Id);
            Assert.AreEqual((2 + startingNumber).ToString(), next);
            updated = fakedService.Retrieve(cub_AutoNumber.EntityLogicalName, autoNumber.Id, new ColumnSet(cub_AutoNumber.cub_updatingAttribute)).ToEntity<cub_AutoNumber>();
            Assert.IsTrue(updated.cub_Updating > updatedDT);
        }

        [TestMethod]
        public void IncrementAutoNumberTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            var logic = new AutoNumberLogic(fakedService);

            var num = 0;
            var value = logic.IncrementAutoNumber(num, -1);
            Assert.AreEqual(num + 1, value);

            try
            {
                value = logic.IncrementAutoNumber(1, 1);
                throw new InternalTestFailureException("IncrementAutoNumber should have failed");
            }
            catch(Exception e)
            {
                Equals(e, new InvalidOperationException("Auto Number has reached its maximum value."));
            }
        }

        [TestMethod]
        public void GetAutoNumberIdByEntityTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            var entityName = "cub_myentity";
            var autoNumber = new cub_AutoNumber
            {
                Id = Guid.NewGuid(),
                cub_Entity = entityName
            };
            var data = new List<Entity>();
            data.Add(autoNumber);
            fakedContext.Initialize(data);

            var logic = new AutoNumberLogic(fakedService);
            var result = logic.GetAutoNumberIdByEntity(entityName);
            Assert.AreEqual(result, autoNumber.Id);
        }

        [TestMethod]
        public void GetAutoNumberByIdTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            var entityName = "cub_myentity";
            var autoNumber = new cub_AutoNumber
            {
                Id = Guid.NewGuid(),
                cub_Entity = entityName
            };
            var data = new List<Entity>();
            data.Add(autoNumber);
            fakedContext.Initialize(data);

            var logic = new AutoNumberLogic(fakedService);
            var result = logic.GetAutoNumberById(autoNumber.Id);
            Assert.AreEqual(autoNumber.Id, result.Id);
        }
    }
}
