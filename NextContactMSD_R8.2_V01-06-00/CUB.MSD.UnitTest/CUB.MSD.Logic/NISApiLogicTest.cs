/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CUB.MSD.Logic;
using System.Net.Http;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.QualityTools.Testing.Fakes;
using System.Net.Http.Fakes;
using CUB.MSD.Logic.Fakes;
using System.Net;
using static CUB.MSD.Model.CUBConstants;
using System;
using CUB.MSD.Model;
using CUB.MSD.Model.NIS;
using CUB.MSD.Model.MSDApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class NISApiLogicTest
    {
        [TestMethod]
        public void TransitAccountGetStatusHistory()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = "{\"message\":\"FAKE MESSAGE\"}";

            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetTransitAccountStatusHistory("330000013491", "ABP","", 0, 6);
                Assert.AreEqual(response.Header.result, "Successful");
            }
        }

        [TestMethod]
        public void TransitAccountGetBalanceHistory()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = "{\"message\":\"FAKE MESSAGE\"}";

            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetTransitAccountBalanceHistory("330000013491", "ABP", "","");
                Assert.AreEqual(response, message);
            }
        }

        [TestMethod]
        public void DelinkSubssystemFromCustomerAccountTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.DeLinkSubssystemFromCustomerAccount("160", "ABP", "1");
                Assert.AreEqual(response.hdr.result, "Successful");
            }
        }
        [TestMethod]
        public void LinkSubssystemToCustomerAccountTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.LinkSubssystemToCustomerAccount("160", "MSD", "1", "Account Nick Name");
                Assert.AreEqual(response.hdr.result, "Successful");
            }
        }

        [TestMethod]
        public void CustomerNotificationTest() // in progress
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                var response = logic.GetCustomerNotification("B08800FD-2BF2-E611-80E2-005056814569", "b5262c34-ce29-e711-80e6-005056814569", "", "", "", "", "", "","","",0,0);

                Assert.AreEqual(response.hdr.result, "Successful");
            }
        }

        [TestMethod]
        public void CustomerNotificationDetailTest() // in progress
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                var response = logic.GetCustomerNotificationDetail("2B9634F0-3F29-E711-80EA-005056813E0D", "8685");

                Assert.IsTrue(response.hdr.errorMessage == "");
            }
        }

        [TestMethod]
        public void CustomerNotificationResendTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.CustomerNotificationResend("2B9634F0-3F29-E711-80EA-005056813E0D", "b5262c34-ce29-e711-80e6-005056814569", "CNG-8231", "NewCustomer", true, "EMAIL", "b5262c34-ce29-e711-80e6-005056814569");

                Assert.IsTrue(response.Success);
            }
        }

        [TestMethod]
        public void GetConfigNotificationChannelTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                var response = logic.GetConfigNotificationChannel(true);

                Assert.IsTrue(response.hdr.errorMessage == "");
            }
        }

        // Funding Source tests
        [TestMethod]
        public void CustomerFundingSourceGetTest() 
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                                
                var response = logic.CustomerFundingSourceGet("2B9634F0-3F29-E711-80EA-005056813E0D", false);
                Assert.IsTrue(response == "");
            }
        }

        [TestMethod]
        public void CustomerFundingSourceGetByIdTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                var response = logic.CustomerFundingSourceGetById("2B9634F0-3F29-E711-80EA-005056813E0D", "1");
                Assert.IsTrue(response.hdr.errorMessage == "");
            }
        }

        [TestMethod]
        public void CustomerFundingSourcePostCCRefTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                Random rndgen = new Random();
                string pgCardId = rndgen.Next().ToString();
                var response = logic.CustomerFundingSourcePostCCRef("2B9634F0-3F29-E711-80EA-005056813E0D", pgCardId, "1111", "0120", "Bill Smith", "Visa", "1", false);
                Assert.IsTrue(response.ResponseObject.hdr.errorMessage == "");
            }
        }

        [TestMethod]
        public void CustomerFundingSourcePostCCClearTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                var response = logic.CustomerFundingSourcePostCCClear("2B9634F0-3F29-E711-80EA-005056813E0D", "Visa", "5555555555557777", "0120", "Bill Smith", "12345", "First Street", "US", "1", false);
                Assert.IsTrue(response.ResponseObject.hdr.errorMessage == "");
            }
        }

        [TestMethod]
        public void CustomerFundingSourceDeleteTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                var response = logic.CustomerFundingSourcePostDDClear("2B9634F0-3F29-E711-80EA-005056813E0D", "Bill Smith", "000123456789", "11000-000", "C", "Chase", "0", false);
                Assert.IsTrue(response.ResponseObject.hdr.errorMessage == "");
            }
        }

        [TestMethod]
        public void CustomerFundingSourcePatchCCTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                var response = logic.CustomerFundingSourcePatchCC("2B9634F0-3F29-E711-80EA-005056813E0D", "1", "507508", "0120", "Johnny Smith", "11 Elm", "US", "92008", "1", true);
                Assert.IsNull(response);
            }
        }

        [TestMethod]
        public void CustomerOrderDebtCollectTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                var response = logic.GetOrderDebtCollect("2B9634F0-3F29-E711-80EA-005056813E0D", "", "b5262c34-ce29-e711-80e6-005056814569", "ABP", "330000000217");
                //Assert.IsTrue(response.ResponseObject.hdr.errorMessage == "");
                Assert.AreEqual(response.Header.result, "Successful");
            }
        }

        [TestMethod]
        public void GetByAccountIdTest()
        {
            var fakedContext = new XrmFakedContext();

            Guid customerId = new Guid("2B9634F0-3F29-E711-80EA-005056813E0D");
            //cub_AccountType accType = new cub_AccountType();
            //accType.cub_AccountName = "Organization";
            //accType.cub_AccountTypeId = accTypeId;

            List<Entity> data = new List<Entity>();
            //data.Add(accType);


            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(data);

            AccountLogic logic = new AccountLogic(fakedService);
            IEnumerable<WSContact> ret = logic.GetByAccountId(customerId);
            // need assert - returns empty
         
        }

        [TestMethod]
        public void GetCustomerNotificationPreferencesAccountTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetCustomerNotificationPreferences("B08800FD-2BF2-E611-80E2-005056814569", null, null, null, null);
                Assert.AreEqual(response.hdr.result, "Successful");
            }
        }

        [TestMethod]
        public void GetTransitAccountStatus()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetTransitAccountStatusByID("123456789", "MSD");
                Assert.AreEqual(response.hdr.result, "Successful");
            }
        }

        [TestMethod]
        public void GetOneAccountSummaryTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                var response = logic.GetOneAccountSummary("B08800FD-2BF2-E611-80E2-005056814569", true);

                // Assert.IsTrue(response.hdr.errorMessage == "");
            }
        }

        [TestMethod]
        public void CustomerNotificationPreferencesPatch()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            CustomerNotificationPreferencesRequest request = new Model.NIS.CustomerNotificationPreferencesRequest();
            request.customerId = "B08800FD-2BF2-E611-80E2-005056814569";
            request.contacts = new List<WSCustomerContactNotificationPreferenceUpdate>();
            var contact = new WSCustomerContactNotificationPreferenceUpdate();
            contact.contactId = new Guid("2089e69e-2ef6-e611-80e2-005056814569").ToString();
            contact.preferences = new List<WSNotificationTypePreferenceUpdate>();
            WSNotificationTypePreferenceUpdate preferenceType = new WSNotificationTypePreferenceUpdate();
            preferenceType.notificationType = "SubsystemAccountCharge";
            preferenceType.optIn = true;
            preferenceType.channels = new List<WSNotificationChannelPreference>();
            preferenceType.channels.Add(new WSNotificationChannelPreference
            {
                channel = "EMAIL",
                enabled = true
            });
            contact.preferences.Add(preferenceType);
            request.contacts.Add(contact);

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimExtensions.PatchHttpClientStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task.Result;
                };

                var response = logic.CustomerNotificationPreferencesPatch(request);
                Assert.IsTrue(response.Success);
            }
        }

        [TestMethod]
        public void OneAccountBalanceHistoryTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountBalanceHistoryResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetOneAccountBalanceHistory("2B9634F0-3F29-E711-80EA-005056813E0D", "2017-04-01", "2017-04-15",
                                                                 "LoadValue", "1001", "All", "date", 0, 15);
                Assert.AreEqual(response.hdr.result, "Successful");
                Assert.AreEqual(response.lineItems.Count, 2);
                Assert.AreEqual(response.lineItems[0].purseId, 1);
            }
        }

        private string CreateFakeOneAccountBalanceHistoryResponse()
        {
            OneAccountBalanceHistoryResponse response = new OneAccountBalanceHistoryResponse();
            response.hdr = new WSWebResponseHeader { };
            response.lineItems = new List<WSBalanceHistoryLineItem>();
            response.lineItems.Add(new WSBalanceHistoryLineItem {
                availableBalance = 15,
                endingBalance = 35,
                entryDateTime = "2017/06/01",
                entryStatus = "OK",
                entryStatusDescription = "Description",
                entryType = "EntryType",
                entryTypeDescription = "Entry Type Description",
                journalEntryAmount = 45,
                journalEntryId = 1,
                purseId = 1,
                purseNickname = "Purse nick name"
            });
            response.lineItems.Add(new WSBalanceHistoryLineItem
            {
                availableBalance = 10,
                endingBalance = 40,
                entryDateTime = "2017/06/21",
                entryStatus = "OK",
                entryStatusDescription = "Description",
                entryType = "EntryType",
                entryTypeDescription = "Entry Type Description",
                journalEntryAmount = 45,
                journalEntryId = 1,
                purseId = 1,
                purseNickname = "Purse nick name"
            });

            return JsonConvert.SerializeObject(response);
        }

        [TestMethod]
        public void OneAccountBalanceHistoryDetailTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountBalanceHistoryDetailResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetOneAccountBalanceHistoryDetail("2B9634F0-3F29-E711-80EA-005056813E0D", "1001");
                Assert.AreEqual(response.hdr.result, "Successful");
                Assert.AreEqual(response.purseId, "1");
            }
        }

        private string CreateFakeOneAccountBalanceHistoryDetailResponse()
        {
            OneAccountBalanceHistoryDetailResponse response = new OneAccountBalanceHistoryDetailResponse
            {
                allocatedAmount = 10,
                availableBalance = 60,
                endingBalance = 50,
                entryDateTime = "2017,06,15",
                entryStatus = "Status",
                entryStatusDescription = "Status Description",
                entryType = "Type",
                entryTypeDescription = "Type Description",
                purseId = "1",
                journalEntryAmount = 15,
                purseNickname = "Purse nick name",
                unallocatedAmount = 13,
                financialTransaction = new WSFinancialTransaction
                {
                    authorizationReferenceNumber = "11101",
                    externalReferenceNumber = "1234",
                    financialTransactionId = 12,
                    financialTransactionType = "Credit Card",
                    transactionAmount = 25,
                    transactionDateTime = "2017/06/10",
                    transactionDescription = "Description"
                }
            };
            return JsonConvert.SerializeObject(response);
        }

        [TestMethod]
        public void OneAccountTravelHistoryTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountTravelHistory();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                OneAccountTravelHistoryResponse response = logic.GetOneAccountTravelHistory("2B9634F0-3F29-E711-80EA-005056813E0D", "2017-04-01", "2017-04-15", null,
                                                                                            "1002", "1001", "All", "date", 0, 15);
                Assert.AreEqual(response.hdr.result, "Successful");
                Assert.AreEqual(response.totalCount, response.lineItems.Count);
                Assert.AreEqual(response.lineItems[0].authorityId, 12);
            }
        }

        private string CreateFakeOneAccountTravelHistory()
        {
            OneAccountTravelHistoryResponse response = new OneAccountTravelHistoryResponse();
            response.totalCount = 1;
            response.lineItems = new List<WSTripHistoryLineItem>();
            response.lineItems.Add(new WSTripHistoryLineItem
            {
                authorityId = 12,
                authorityName = "Authority Name",
                isCorrectable = true,
                endDateTime = "2017/06/30",
                endLocationDescription = "End Location Description",
                startDateTime = "2017/06/01",
                startLocationDescription = "Start Location Description",
                subsystem = "ABP",
                subsystemAccountNickname = "Subsystem Account Nick Name",
                subsystemAccountReference = "111222",
                totalFare = 45,
                tripId = "1",
                travelMode = "train",
                travelModeDescription = "Train",
                unpaidFare = 15,
                token = new WSMobileTravelTokenDisplay
                {
                    lastUseDTM = new DateTime(2017, 6, 27),
                    tokenNickname = "My Token",
                    tokenType = "Mobile",
                    serializedMobileToken = "8923hfyu23er"
                },
                productDescription = "Produc Description"
            });

            return JsonConvert.SerializeObject(response);
        }

        [TestMethod]
        public void OneAccountTravelHistoryDetailTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountTravelHistoryDetailResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetOneAccountTravelHistoryDetail("2B9634F0-3F29-E711-80EA-005056813E0D", "1001");
                Assert.AreEqual(response.hdr.result, "Successful");
                Assert.AreEqual(response.tripInfo.authorityId, 12);
            }
        }

        private string CreateFakeOneAccountTravelHistoryDetailResponse()
        {
            OneAccountTravelHistoryDetailResponse response = new OneAccountTravelHistoryDetailResponse
            {
                tripInfo = new WSTripHistoryLineItem
                {
                    authorityId = 12,
                    authorityName = "Authority Name",
                    isCorrectable = true,
                    endDateTime = "2017/06/30",
                    endLocationDescription = "End Location Description",
                    startDateTime = "2017/06/01",
                    startLocationDescription = "Start Location Description",
                    subsystem = "ABP",
                    subsystemAccountNickname = "Subsystem Account Nick Name",
                    subsystemAccountReference = "111222",
                    totalFare = 45,
                    tripId = "1",
                    travelMode = "train",
                    travelModeDescription = "Train",
                    unpaidFare = 15,
                    token = new WSMobileTravelTokenDisplay
                    {
                        lastUseDTM = new DateTime(2017, 6, 27),
                        tokenNickname = "My Token",
                        tokenType = "Mobile",
                        serializedMobileToken = "8953423S43"
                    },
                },
                tripPayments = new List<WSTravelPayment>(),
            };
            response.tripPayments.Add(new WSTravelPayment
            {
                id = 2,
                paidFare = 17,
                payingSystemDescription = "Desc",
                paymentDateTime = "2017/06/10",
                productDescription = "Prod. Description",
                purseDescription = "Purse Desc",
                paymentSource = new List<WSTravelPaymentSource>()
            });
            response.tripPayments[0].paymentSource.Add(new WSTravelPaymentSource {
                amount = 10,
                authReferenceNumber = "123445",
                dateTime = "2017/06/01",
                fundingSourceInfo = "Funding source info",
                id = 121,
                paymentReferenceNumber = "12199",
                paymentSourceType = "Payment Source Type",
                paymentType = "Payment Type",
                paymentTypeDescription = "Payment Description"
            });
            return JsonConvert.SerializeObject(response);
        }

        [TestMethod]
        public void TransitAccountTravelHistoryDetailTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeTransitAccountTravelHistoryDetailResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetTransitAccountTravelHistoryDetail("1001", "ABP", "1001");
                Assert.AreEqual(response.Header.result, "Successful");
         //       Assert.AreEqual(response.travelMode, "train");
            }
        }

        private string CreateFakeTransitAccountTravelHistoryDetailResponse()
        {
            // todo fix this
            TransitAccountTravelHistoryDetailResponse response = new TransitAccountTravelHistoryDetailResponse
            {
                tripInfo = new WSTransitTripHistoryLineItem
                {
                    isCorrectable = true,
                    endDateTime = "2017/06/30",
                    endLocationDescription = "End Location Description",
                    startDateTime = "2017/06/01",
                    startLocationDescription = "Start Location Description",
                    totalFare = 45,
                    tripId = "1",
                    travelMode = "train",
                    travelModeDescription = "Train",
                    unpaidFare = 15,
                //    token = new WSTravelTokenDisplay
                //    {
                        
                //        tokenNickname = "My Token",
                //        tokenType = "Mobile",
                //        serializedMobileToken = "0q8yt3e"
                //    },

                },
                travelPresenceEvents = new List<WSTransitTravelPresence>(),
                tripPayments = new List<WSTravelPayment>(),
            };
            response.tripPayments.Add(new WSTravelPayment
            {
                id = 2,
                paidFare = 17,
                payingSystemDescription = "Desc",
                paymentDateTime = "2017/06/10",
                productDescription = "Prod. Description",
                purseDescription = "Purse Desc",
                paymentSource = new List<WSTravelPaymentSource>()
            });
            response.tripPayments[0].paymentSource.Add(new WSTravelPaymentSource
            {
                amount = 10,
                authReferenceNumber = "123445",
                dateTime = "2017/06/01",
                fundingSourceInfo = "Funding source info",
                id = 121,
                paymentReferenceNumber = "12199",
                paymentSourceType = "Payment Source Type",
                paymentType = "Payment Type",
                paymentTypeDescription = "Payment Description"
            });
            return JsonConvert.SerializeObject(response);
        }

        [TestMethod]
        public void CustomerOrderHistoryTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountOrderHistoryResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetCustomerOrderHistory("2B9634F0-3F29-E711-80EA-005056813E0D", "2017-04-01", "2017-04-15",
                                                             "LoadValue","Completed","Payment Received",null,false, "order date",null, 20);
                Assert.IsNotNull(response.Body);
            }
        }

        private string CreateFakeOneAccountOrderHistoryResponse()
        {
            CustomerOrderHistoryResponse response = new CustomerOrderHistoryResponse();
            response.hdr = new WSWebResponseHeader { };
            response.Body = "Some body";

            return JsonConvert.SerializeObject(response);
        }
        [TestMethod]
        public void OneAccountOrderTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountOrderResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetCustomerOrder("2B9634F0-3F29-E711-80EA-005056813E0D", "2B9634F0-3F29-E711-80EA-005056813E0D", "closeAccountRef");
                Assert.AreEqual(response.hdr.result, "Successful");
                Assert.AreEqual(response.order.lineItems.Count, 2);
            }
        }

        private string CreateFakeOneAccountOrderResponse()
        {
            WSGetCustomerOrderResponse response = new WSGetCustomerOrderResponse();
            response.hdr = new WSWebResponseHeader { };
            response.order = new WSOrder {
                orderId = 1,
                customerId = "2B9634F0-3F29-E711-80EA-005056813E0D",
                orderStatus = "OrderStatus",
                orderStatusDescription = "Order Status",
                insertedDtm = DateTime.Now,
                lastStatusDtm = DateTime.Now,
                updatedDtm = DateTime.Now,
                orderTotalAmount = 45,
                itemsSubtotalAmount = 25,
                orderType = "OrderType",
                orderTypeDescription = "Order Type",
                salesTaxAmount = 15,
                unregisteredEmail = "someone@email.com",
                lineItems = new List<WSOrderLineItem>()
            };
            response.order.lineItems.Add(new WSOrderLineItem
            {
                fulfillDtm = DateTime.Now,
                fulfillErrorMessage = "fulfillErrorMessage",
                fulfillErrorReason = "fulfillErrorReason",
                fulfillErrorStackTrace = "fulfillErrorStackTrace",
                fulfillResponseRefId = "fulfillResponseRefId",
                itemTotalAmount = 10,
                lineItemId = 1,
                lineItemStatus = "Status",
                lineItemStatusDescription = "Status Desc",
                lineItemType = "Type",
                lineItemTypeDescription = "Type Desc",
                salesTaxAmount = 5,
                updatedDtm = DateTime.Now,
            });
            response.order.lineItems.Add(new WSOrderLineItem
            {
                fulfillDtm = DateTime.Now,
                fulfillErrorMessage = "fulfillErrorMessage",
                fulfillErrorReason = "fulfillErrorReason",
                fulfillErrorStackTrace = "fulfillErrorStackTrace",
                fulfillResponseRefId = "fulfillResponseRefId",
                itemTotalAmount = 10,
                lineItemId = 2,
                lineItemStatus = "Status",
                lineItemStatusDescription = "Status Desc",
                lineItemType = "Type",
                lineItemTypeDescription = "Type Desc",
                salesTaxAmount = 5,
                updatedDtm = DateTime.Now,
            });

            return JsonConvert.SerializeObject(response);
        }

        [TestMethod]
        public void TransitAccountGetBankCardChargeHistoryTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountOrderResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetTransitAccountCardChargeHistory("330000008624", "ABP", null, null, null, null, null, null, null, null);
                Assert.AreEqual(response.Header.result, "Successful");
            }
        }

        [TestMethod]
        public void CustomerGetActivityTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountOrderResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetCustomerActivities("330000008624", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                Assert.AreEqual(response.Header.result, "Successful");
            }
        }

        [TestMethod]
        public void TransitAccountGetBankChargeAggregationTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountOrderResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.getBankChargeAggregationDetails("330000008624", "ABP", "1");
                Assert.AreEqual(response.Header.result, "Successful");
            }
        }

        [TestMethod]
        public void TransitAccountOrderHistoryTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountOrderHistoryResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetTransitAccountOrderHistory("330000008624", "ABP", null, null, "type", "status", "paymentStatus", null, null, null, null, null);
                Assert.IsNotNull(response.Body);
            }
        }


        public void TransitAccountOrderDetailTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountOrderHistoryResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetTransitAccountOrderDetail("330000008624", "ABP", "1234", false);
                Assert.IsNotNull(response.Body);
            }
        }

        public void ResendOrderNotificationTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountOrderHistoryResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.ResendNotification("OrderReceipt", Guid.NewGuid().ToString(), 1234, null,null,null);
                Assert.AreEqual(RestConstants.SUCCESSFUL,response.Header.result);
            }
        }

        [TestMethod]
        public void CustomerServiceGetReasonCodesTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);
            string message = CreateFakeOneAccountOrderHistoryResponse();
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (str, ctx) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var response = logic.GetCustomerServiceReasonCodes(1, true);
                Assert.IsNotNull(response.Body);
            }
        }

        [TestMethod]
        public void PostVoidTripTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            WSTripCorrection request = new WSTripCorrection();
            request.clientRefId = "device.01";
            request.customerId = Guid.NewGuid().ToString();
            request.correctionLineItems = new List<WSTravelCorrectionLineItem>();
            request.correctionLineItems.Add(new WSTravelCorrectionLineItem());
            request.correctionLineItems[0].correction = new WSTravelCorrection();
            request.correctionLineItems[0].correction.subsystem = "ABP";
            request.correctionLineItems[0].correction.subsystemAccountReference = "440000018174";
            request.correctionLineItems[0].correction.notes = "Notes";
            request.correctionLineItems[0].correction.reasonCode = "11";
            request.correctionLineItems[0].correction.subsystemAccountReference = "TransitAccountTripVoid";
            request.correctionLineItems[0].correction.transactionId = "1416";

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };

                
                var response = logic.PostTravelCorrection(request, Guid.NewGuid());

                Assert.AreEqual(response.Header.result, "Successful");
            }
        }
    }
}