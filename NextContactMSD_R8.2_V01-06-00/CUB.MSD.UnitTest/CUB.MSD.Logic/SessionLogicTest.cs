﻿using CUB.MSD.Logic;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using FakeXrmEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class SessionLogicTest
    {
        [TestMethod]
        public void FindActiveSessionByAttributeValueTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            var owner1 = Guid.NewGuid();
            var owner2 = Guid.NewGuid();
            var session1 = new cub_CSRSession
            {
                Id = Guid.NewGuid(),
                OwnerId = new EntityReference(SystemUser.EntityLogicalName, owner1)
            };
            var session2 = new cub_CSRSession
            {
                Id = Guid.NewGuid(),
                OwnerId = new EntityReference(SystemUser.EntityLogicalName, owner2)
            };
            var data = new List<Entity>();
            data.Add(session1);
            data.Add(session2);
            //NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            var logic = new SessionLogic(fakedService);

            var result = logic.RetrieveSession(owner1);
            Assert.AreEqual(session1.Id, result.sessionId);

            result = logic.RetrieveSession(owner2);
            Assert.AreEqual(session2.Id, result.sessionId);
        }

        [TestMethod]
        public void CreateSessionNoContextTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            var logic = new SessionLogic(fakedService);

            var result = logic.CreateSession(Guid.NewGuid(), null, null, "CRM");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.sessionId);

            var session = fakedService.Retrieve(cub_CSRSession.EntityLogicalName, result.sessionId.Value, new ColumnSet(cub_CSRSession.cub_csrsessionidAttribute));
            Assert.IsNotNull(session);
        }

        [TestMethod]
        public void CreateSessionFromCustomerContextTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            var customer = new Account
            {
                Id = Guid.NewGuid(),
                Name = "FIRST LAST"
            };
            var contactType = new cub_ContactType
            {
                Id = Guid.NewGuid(),
                cub_Name = "Primary"
            };
            var contact = new Contact
            {
                Id = Guid.NewGuid(),
                cub_ContactTypeId = contactType.ToEntityReference(),
                ParentCustomerId = customer.ToEntityReference()
            };
            var data = new List<Entity>();
            data.Add(customer);
            data.Add(contactType);
            data.Add(contact);
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);

            var logic = new SessionLogic(fakedService);
            var result = logic.CreateSession(Guid.NewGuid(), customer.Id, customer.LogicalName, "CRM");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.sessionId);
            Assert.AreEqual(customer.Id, result.customerId);

            var session = fakedService.Retrieve(cub_CSRSession.EntityLogicalName, result.sessionId.Value, new ColumnSet(cub_CSRSession.cub_customeridAttribute)).ToEntity<cub_CSRSession>();
            Assert.IsNotNull(session);
            Assert.IsNotNull(session.cub_CustomerId);
            Assert.AreEqual(customer.Id, session.cub_CustomerId.Id);
        }

        [TestMethod]
        public void CreateSessionFromContactContextTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            var customer = new Account
            {
                Id = Guid.NewGuid(),
                Name = "FIRST LAST"
            };
            var contactType = new cub_ContactType
            {
                Id = Guid.NewGuid(),
                cub_Name = "Primary"
            };
            var contact = new Contact
            {
                Id = Guid.NewGuid(),
                cub_ContactTypeId = contactType.ToEntityReference(),
                ParentCustomerId = customer.ToEntityReference()
            };
            var data = new List<Entity>();
            data.Add(customer);
            data.Add(contactType);
            data.Add(contact);
            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);

            var logic = new SessionLogic(fakedService);
            var result = logic.CreateSession(Guid.NewGuid(), contact.Id, contact.LogicalName, "CRM");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.sessionId);
            Assert.AreEqual(contact.Id, result.contactId);

            var session = fakedService.Retrieve(cub_CSRSession.EntityLogicalName, result.sessionId.Value, new ColumnSet(cub_CSRSession.cub_contactidAttribute)).ToEntity<cub_CSRSession>();
            Assert.IsNotNull(session);
            Assert.IsNotNull(session.cub_ContactId);
            Assert.AreEqual(contact.Id, session.cub_ContactId.Id);
        }

        [TestMethod]
        public void CreateSessionFromTransitAccountContextTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            var transitAccount = new cub_TransitAccount
            {
                Id = Guid.NewGuid()
            };
            var data = new List<Entity>();
            data.Add(transitAccount);
            fakedContext.Initialize(data);

            var logic = new SessionLogic(fakedService);
            var result = logic.CreateSession(Guid.NewGuid(), transitAccount.Id, transitAccount.LogicalName, "CRM");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.sessionId);
            Assert.AreEqual(transitAccount.Id, result.transitAccountId);

            var session = fakedService.Retrieve(cub_CSRSession.EntityLogicalName, result.sessionId.Value, new ColumnSet(cub_CSRSession.cub_transitaccountAttribute)).ToEntity<cub_CSRSession>();
            Assert.IsNotNull(session);
            Assert.IsNotNull(session.cub_TransitAccount);
            Assert.AreEqual(transitAccount.Id, session.cub_TransitAccount.Id);
        }

        [TestMethod]
        public void EndSessionTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            var session = new cub_CSRSession
            {
                Id = Guid.NewGuid()
            };
            var data = new List<Entity>();
            data.Add(session);
            fakedContext.Initialize(data);

            var activeSession = fakedService.Retrieve(cub_CSRSession.EntityLogicalName, session.Id, new ColumnSet(cub_CSRSession.statecodeAttribute)).ToEntity<cub_CSRSession>();
            Assert.IsNotNull(activeSession);
            Assert.AreEqual(cub_CSRSessionState.Active, activeSession.statecode.GetValueOrDefault());

            var logic = new SessionLogic(fakedService);
            logic.EndSession(session.Id);
            var inactiveSession = fakedService.Retrieve(cub_CSRSession.EntityLogicalName, session.Id, new ColumnSet(cub_CSRSession.statecodeAttribute)).ToEntity<cub_CSRSession>();
            Assert.IsNotNull(inactiveSession);
            Assert.AreEqual(cub_CSRSessionState.Inactive, inactiveSession.statecode.GetValueOrDefault());
        }

        [TestMethod]
        public void UpdateNotesTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            var session = new cub_CSRSession
            {
                Id = Guid.NewGuid()
            };
            var data = new List<Entity>();
            data.Add(session);
            fakedContext.Initialize(data);

            var activeSession = fakedService.Retrieve(cub_CSRSession.EntityLogicalName, session.Id, new ColumnSet(cub_CSRSession.cub_notesAttribute)).ToEntity<cub_CSRSession>();
            Assert.IsNotNull(activeSession);

            var logic = new SessionLogic(fakedService);
            logic.UpdateNotes(session.Id, "test notes");
            activeSession = fakedService.Retrieve(cub_CSRSession.EntityLogicalName, session.Id, new ColumnSet(cub_CSRSession.cub_notesAttribute)).ToEntity<cub_CSRSession>();
            Assert.IsNotNull(activeSession);
            Assert.AreEqual("test notes", activeSession.cub_Notes);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ServiceModel.CommunicationException), AllowDerivedTypes = true)]
        public void RemoveLinkedSessionTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            var sessionLink = new cub_csrlinkedsession
            {
                Id = Guid.NewGuid()
            };

            var data = new List<Entity>();
            data.Add(sessionLink);
            fakedContext.Initialize(data);

            var linkedSession = fakedService.Retrieve(cub_csrlinkedsession.EntityLogicalName, sessionLink.Id, new ColumnSet(cub_csrlinkedsession.cub_csrlinkedsessionidAttribute)).ToEntity<cub_csrlinkedsession>();
            Assert.IsNotNull(linkedSession);

            var logic = new SessionLogic(fakedService);
            logic.RemoveLinkedSession(sessionLink.Id);
            var deletedLink = fakedService.Retrieve(cub_csrlinkedsession.EntityLogicalName, sessionLink.Id, new ColumnSet(cub_csrlinkedsession.cub_csrlinkedsessionidAttribute));
            Assert.IsNull(deletedLink);
        }

        [TestMethod]
        public void SearchSessionTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            string existSessionNUmber = "0001";
            string nonExistSessionNumber = "1001";
            var session = new cub_CSRSession
            {
                Id = Guid.NewGuid(),
                cub_name = existSessionNUmber
            };

            var data = new List<Entity>();
            data.Add(session);

            //NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            var logic = new SessionLogic(fakedService);

            var result = logic.SearchSession(nonExistSessionNumber);
            Assert.IsNotNull(result);
            Assert.AreEqual(RestConstants.FAILED, result.Header.result);
            Assert.AreEqual("No record found", result.Header.errorMessage);

            result = logic.SearchSession(existSessionNUmber);
            Assert.IsNotNull(result);
            Assert.AreEqual(RestConstants.SUCCESSFUL, result.Header.result);

            var sesResp = JsonConvert.DeserializeObject(result.Body, typeof(List<WSSessionResponse>));
            Assert.AreEqual(sesResp.GetType(), typeof(List<WSSessionResponse>));
            Assert.AreEqual(1, (sesResp as List<WSSessionResponse>).Count);
            Assert.AreEqual(existSessionNUmber, (sesResp as List<WSSessionResponse>)[0].sessionNumber);
        }

        [TestMethod]
        public void LinkSessionTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            Guid parentSessionId = Guid.NewGuid();
            Guid childSessionId = Guid.NewGuid();
            string childSessionNumber = "0001";


            var session = new cub_CSRSession
            {
                Id = Guid.NewGuid(),
                cub_name = childSessionNumber
            };


            var data = new List<Entity>();
            data.Add(session);
            fakedContext.Initialize(data);

            var logic = new SessionLogic(fakedService);
            var newLinkSessionId = logic.LinkSession(parentSessionId, childSessionId);

            var linkedSession = fakedService.Retrieve(cub_csrlinkedsession.EntityLogicalName, newLinkSessionId, new ColumnSet(cub_csrlinkedsession.cub_csrlinkedsessionidAttribute)).ToEntity<cub_csrlinkedsession>();
            Assert.IsNotNull(linkedSession);
            Assert.IsTrue(linkedSession.cub_csrlinkedsessionId.HasValue);
            Assert.AreEqual(newLinkSessionId, linkedSession.cub_csrlinkedsessionId.Value);

            newLinkSessionId = logic.LinkSession(parentSessionId, childSessionNumber);

            linkedSession = fakedService.Retrieve(cub_csrlinkedsession.EntityLogicalName, newLinkSessionId, new ColumnSet(cub_csrlinkedsession.cub_csrlinkedsessionidAttribute)).ToEntity<cub_csrlinkedsession>();
            Assert.IsNotNull(linkedSession);
            Assert.IsTrue(linkedSession.cub_csrlinkedsessionId.HasValue);
            Assert.AreEqual(newLinkSessionId, linkedSession.cub_csrlinkedsessionId.Value);
        }


        [TestMethod]
        public void GetLinkedSessionsTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            Guid parentSessionId = Guid.NewGuid();
            Guid childSessionId = Guid.NewGuid();
            Guid childSession2Id = Guid.NewGuid();


            var sessionParent = new cub_CSRSession
            {
                Id = parentSessionId
            };

            var sessionChild = new cub_CSRSession
            {
                Id = childSessionId
            };


            var sessionChild2 = new cub_CSRSession
            {
                Id = childSession2Id
            };


            var sessionLink = new cub_csrlinkedsession
            {
                Id = Guid.NewGuid(),
                cub_ParentSession = new EntityReference(cub_CSRSession.EntityLogicalName, parentSessionId),
                cub_ChildSession = new EntityReference(cub_CSRSession.EntityLogicalName, childSessionId)
            };

            var sessionLink2 = new cub_csrlinkedsession
            {
                Id = Guid.NewGuid(),
                cub_ParentSession = new EntityReference(cub_CSRSession.EntityLogicalName, parentSessionId),
                cub_ChildSession = new EntityReference(cub_CSRSession.EntityLogicalName, childSession2Id)
            };

            var data = new List<Entity>();
            data.Add(sessionParent);
            data.Add(sessionChild);
            data.Add(sessionChild2);
            data.Add(sessionLink);
            data.Add(sessionLink2);
            fakedContext.Initialize(data);

            var logic = new SessionLogic(fakedService);
            var result = logic.GetLinkedSessions(parentSessionId);

            Assert.IsNotNull(result);
            Assert.AreEqual(RestConstants.SUCCESSFUL, result.Header.result);

            var sesResp = JsonConvert.DeserializeObject(result.Body, typeof(List<WSSessionResponse>));
            Assert.AreEqual(sesResp.GetType(), typeof(List<WSSessionResponse>));
            Assert.AreEqual(2, (sesResp as List<WSSessionResponse>).Count);
        }


        [TestMethod]
        public void GetSessionDetailTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            Guid sessionId = Guid.NewGuid();
            Guid caseId = Guid.NewGuid();
            Guid activityId = Guid.NewGuid();
            string sessionNumber = "0001";

            var session = new cub_CSRSession
            {
                Id = sessionId,
                cub_name = sessionNumber
            };


            var cases = new Incident
            {
                Id = caseId,
                cub_SessionId = new EntityReference(cub_CSRSession.EntityLogicalName, sessionId)

            };

            var activities = new cub_CustomActivity
            {
                Id = activityId,
                cub_Session = new EntityReference(cub_CSRSession.EntityLogicalName, sessionId)
            };

            var data = new List<Entity>();
            data.Add(session);
            data.Add(cases);
            data.Add(activities);
            fakedContext.Initialize(data);

            var logic = new SessionLogic(fakedService);
            var result = logic.GetSessionDetail(sessionId);

            Assert.IsNotNull(result);
            Assert.AreEqual(sessionId, result.sessionId);
            Assert.IsNotNull(result.cases);
            Assert.IsNotNull(result.activities);

            var casesResponse = JsonConvert.DeserializeObject(result.cases);
            var activitiesResponse = JsonConvert.DeserializeObject(result.cases);
            Assert.IsNotNull(casesResponse);
            Assert.IsNotNull(activitiesResponse);
        }
    }
}
