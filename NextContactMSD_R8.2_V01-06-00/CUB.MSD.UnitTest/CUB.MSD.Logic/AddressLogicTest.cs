/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Collections.Generic;
using CUB.MSD.Logic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using Newtonsoft.Json.Linq;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class AddressLogicTest
    {
        [TestMethod]
        public void IsAddressIdValidTest()
        {
            var fakedContext = new XrmFakedContext();

            cub_Address myAddress = new cub_Address();
            Guid addressId = Guid.NewGuid();
            myAddress.cub_AddressId = addressId;         

            List<Entity> data = new List<Entity>();
            data.Add(myAddress);

           
            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(data);
            

            AddressLogic logic = new AddressLogic(fakedService);
            CubResponse<WSAddress> response;

            
            response = logic.IsAddressIdValid("Incorrect guid");

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Address & AddressId", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_ADDRESS_NOT_FOUND, response.Errors[0].errorMessage);

            response = logic.IsAddressIdValid(addressId.ToString());

            Assert.AreEqual(true, response.Success);
            Assert.AreEqual(0, response.Errors.Count);
        }

        [TestMethod]
        public void GetByContactIdTest()
        {
            var fakedContext = new XrmFakedContext();

            var contactId = Guid.NewGuid();
            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Abbreviation = "WA"
            };
            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "98109"
            };
            var address = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_City = "Seattle",
                cub_PostalCodeId = zip.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_Street1 = "400 Broad St",
                statecode = cub_AddressState.Active
            };
            var contact = new Contact
            {
                ContactId = contactId,
                cub_AddressId = address.ToEntityReference()
            };

            var entities = new List<Entity>();
            entities.Add(state);
            entities.Add(zip);
            entities.Add(address);
            entities.Add(contact);

            fakedContext.Initialize(entities);
            var fakedService = fakedContext.GetOrganizationService();

            var logic = new AddressLogic(fakedService);
            var response = logic.GetByContactId(contactId);
            var jObj = JObject.FromObject(response);
            Assert.AreEqual("400 Broad St", jObj["street1"]);
            Assert.AreEqual("Seattle", jObj["city"]);
            Assert.AreEqual("WA", jObj["state"]);
            Assert.AreEqual("98109", jObj["zipCode"]);
        }
    }
}
