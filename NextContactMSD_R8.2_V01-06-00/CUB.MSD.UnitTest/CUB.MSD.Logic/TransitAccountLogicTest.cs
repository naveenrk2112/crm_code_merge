﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using FakeXrmEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using CUB.MSD.Logic;
using System.Linq;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class TransitAccountLogicTest
    {
        [TestMethod]
        public void UpsertTransitAccountCreateTest()
        {
            const string ID = "330000000001";
            const string SUBSYSTEM = "AAA";
            var fakedContext = new XrmFakedContext();
            var customer = new Account { Id = Guid.NewGuid() };
            var entities = new List<Entity> { customer };
            fakedContext.Initialize(entities);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new TransitAccountLogic(fakedService);
            logic.UpsertTransitAccount(ID, SUBSYSTEM, customer.ToEntityReference());

            var result = (from t in fakedContext.CreateQuery<cub_TransitAccount>()
                          select t).FirstOrDefault();
            Assert.IsNotNull(result);
            Assert.AreEqual(ID, result.cub_Name);
            Assert.AreEqual(customer.ToEntityReference(), result.cub_Customer);
        }

        [TestMethod]
        public void UpsertTransitAccountUpdateTest()
        {
            const string ID = "330000000002";
            const string SUBSYSTEM = "BBB";
            var fakedContext = new XrmFakedContext();
            var customer = new Account { Id = Guid.NewGuid() };
            var transitAccount = new cub_TransitAccount { Id = Guid.NewGuid(), cub_Name = ID };
            var entities = new List<Entity> { customer, transitAccount };
            fakedContext.Initialize(entities);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new TransitAccountLogic(fakedService);
            logic.UpsertTransitAccount(ID, SUBSYSTEM, customer.ToEntityReference());

            var result = (from t in fakedContext.CreateQuery<cub_TransitAccount>()
                          select t).FirstOrDefault();
            Assert.IsNotNull(result);
            Assert.AreEqual(ID, result.cub_Name);
            Assert.AreEqual(customer.ToEntityReference(), result.cub_Customer);
        }

        [TestMethod]
        public void UpsertTransitAccountNoCustomer()
        {
            const string ID = "330000000003";
            const string SUBSYSTEM = "CCC";
            var fakedContext = new XrmFakedContext();
            var customer = new Account { Id = Guid.NewGuid() };
            var transitAccount = new cub_TransitAccount
            {
                Id = Guid.NewGuid(),
                cub_Name = ID,
                cub_Customer = customer.ToEntityReference()
            };
            var entities = new List<Entity> { customer, transitAccount };
            fakedContext.Initialize(entities);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new TransitAccountLogic(fakedService);
            logic.UpsertTransitAccount(ID, SUBSYSTEM);

            var result = (from t in fakedContext.CreateQuery<cub_TransitAccount>()
                          select t).FirstOrDefault();
            Assert.IsNotNull(result);
            Assert.AreEqual(ID, result.cub_Name);
            Assert.IsNull(result.cub_Customer);
        }

        [TestMethod]
        public void LinkToCustomerTest()
        {
            const string ID = "330000000004";
            const string SUBSYSTEM = "DDD";
            const string OAMID = "100";
            var fakedContext = new XrmFakedContext();
            var customer = new Account { Id = Guid.NewGuid(), cub_OneAccountId = OAMID };
            var entities = new List<Entity> { customer };
            fakedContext.Initialize(entities);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new TransitAccountLogic(fakedService);
            logic.AssociateWithCustomer(ID, SUBSYSTEM, OAMID);

            var result = (from t in fakedContext.CreateQuery<cub_TransitAccount>()
                          select t).FirstOrDefault();
            Assert.IsNotNull(result);
            Assert.AreEqual(ID, result.cub_Name);
            Assert.AreEqual(customer.ToEntityReference(), result.cub_Customer);
        }

        [TestMethod]
        public void LinkToCustomerNoExistingCustomerTest()
        {
            const string ID = "330000000005";
            const string SUBSYSTEM = "EEE";
            const string OAMID = "101";
            var fakedContext = new XrmFakedContext();

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new TransitAccountLogic(fakedService);
            logic.AssociateWithCustomer(ID, SUBSYSTEM, OAMID);

            var result = (from t in fakedContext.CreateQuery<cub_TransitAccount>()
                          select t).FirstOrDefault();
            Assert.IsNotNull(result);
            Assert.AreEqual(ID, result.cub_Name);
            Assert.IsNull(result.cub_Customer);
        }
    }
}
