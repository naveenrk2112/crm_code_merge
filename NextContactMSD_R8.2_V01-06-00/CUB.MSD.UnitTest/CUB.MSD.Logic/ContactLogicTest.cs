/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Collections.Generic;
using CUB.MSD.Logic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using System.Linq;
using CUB.MSD.Web.Models;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class ContactLogicGeneralFunctionsTest
    {
        [TestMethod]
        public void IsContactTypeValidTest()
        {
            var fakedContext = new XrmFakedContext();

            Guid contactTypeId = Guid.NewGuid();
            cub_ContactType accType = new cub_ContactType();
            accType.cub_Name = "Primary";
            accType.cub_ContactTypeId = contactTypeId;

            List<Entity> data = new List<Entity>();
            data.Add(accType);


            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(data);

            ContactLogic logic = new ContactLogic(fakedService);
            CubResponse<WSCustomerContact> resposne;
            WSCustomerContact customerContact = new WSCustomerContact();
            customerContact.contactType = "Test";
            resposne = logic.IsContactTypeValid(customerContact);

            Assert.AreEqual(false, resposne.Success);

            customerContact.contactType = "Primary";
            resposne = logic.IsContactTypeValid(customerContact);

            Assert.AreEqual(true, resposne.Success);
            Assert.AreEqual(contactTypeId, customerContact.contactTypeId);
        }

        [TestMethod]
        public void IsNameValid()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            ContactLogic logic = new ContactLogic(fakedService);

            CubResponse<WSName> response;
            response = logic.IsNameValid(null);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Name", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_FIELD_IS_BLANK, response.Errors[0].errorMessage);

            WSName myName = new WSName();

            myName.title = "I have a very big title that exceeds twenty character";
            response = logic.IsNameValid(myName);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Title", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED, response.Errors[0].errorMessage);

            myName.title = "Dr.";

            response = logic.IsNameValid(myName);
            Assert.AreEqual("First name", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_FIELD_IS_BLANK, response.Errors[0].errorMessage);


            myName.firstName = "I have very big first name that exceeds sixty characters. Wow, what a first name!.";

            response = logic.IsNameValid(myName);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("First name", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED, response.Errors[0].errorMessage);

            myName.firstName = "John";
            myName.middleInitial = "I have very big middle initial that exceeds one character.";

            response = logic.IsNameValid(myName);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Middle initial", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED, response.Errors[0].errorMessage);

            myName.middleInitial = "A";

            response = logic.IsNameValid(myName);
            Assert.AreEqual("Last name", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_FIELD_IS_BLANK, response.Errors[0].errorMessage);

            myName.lastName = "I have very big Last name that exceeds sixty characters. Wow, what a Last name!.";

            response = logic.IsNameValid(myName);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Last name", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED, response.Errors[0].errorMessage);

            myName.lastName = "Johnson";

            myName.nameSuffix = "I have very big name suffix that exceeds twenty characters.";

            response = logic.IsNameValid(myName);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Name suffix", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_MAXIMUM_LENGTH_EXCEEDED, response.Errors[0].errorMessage);

            myName.nameSuffix = "Sr";

            response = logic.IsNameValid(myName);

            Assert.AreEqual(true, response.Success);
            Assert.AreEqual(0, response.Errors.Count);
        }

        [TestMethod]
        public void GetSecurityAnswersTest()
        {
            var fakedContext = new XrmFakedContext();

            Guid contactId = Guid.NewGuid();
            Guid securityQId = Guid.NewGuid();
            Guid securityAId = Guid.NewGuid();

            cub_SecurityQuestion securityQ = new cub_SecurityQuestion();
            securityQ.cub_SecurityQuestionId = securityQId;
            securityQ.cub_SecurityQuestionDesc = "Some smart question";

            cub_SecurityAnswer securityA = new cub_SecurityAnswer();
            securityA.cub_SecurityAnswerId = securityAId;
            securityA.cub_SecurityQuestionId = new EntityReference(cub_SecurityQuestion.EntityLogicalName, securityQId);
            securityA.cub_ContactId = new EntityReference(Contact.EntityLogicalName, contactId);
            securityA.cub_SecurityAnswerDesc = "Even better answer";


            List<Entity> data = new List<Entity>();
            data.Add(securityQ);
            data.Add(securityA);


            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(data);

            ContactLogic logic = new ContactLogic(fakedService);
            List<WSSecurityQA> resultList;
            resultList = logic.GetSecurityAnswers(Guid.NewGuid());

            Assert.AreEqual(0, resultList.Count);

            resultList = logic.GetSecurityAnswers(contactId);

            Assert.AreEqual(1, resultList.Count);
        }

        [TestMethod]
        public void ValidateContactForUpdateTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            ContactLogic logic = new ContactLogic(fakedService);
            WSCustomerContact customerContact = new WSCustomerContact();
        

            CubResponse<WSCustomerContact> response;

            
            response = logic.ValidateContactForUpdate(customerContact);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Contact Type", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_FIELD_IS_BLANK, response.Errors[0].errorMessage);

            Guid contactTypeId = Guid.NewGuid();
            cub_ContactType accType = new cub_ContactType();
            accType.cub_Name = "Primary";
            accType.cub_ContactTypeId = contactTypeId;

            List<Entity> data = new List<Entity>();
            data.Add(accType);
            data.AddRange(ContactHelper.GetSecurityQuestions());
            fakedContext.Initialize(data);

            customerContact.contactType = "Primary";
            customerContact.name.firstName = "John";
            customerContact.name.middleInitial = "A";
            customerContact.name.lastName = "Johnson";

            customerContact.securityQAs = new WSSecurityQA[] { new WSSecurityQA() { securityQuestion = "What was your mother's maiden name?", securityAnswer = "" } };


            response = logic.ValidateContactForUpdate(customerContact);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Security QA", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_SECURITY_ANSWER_REQUIRED, response.Errors[0].errorMessage);

            customerContact.securityQAs = new WSSecurityQA[] { new WSSecurityQA() { securityQuestion = "What was your mother's maiden name?", securityAnswer = "I have very long answer to my securty question. Waaaaaaaaaaaaaaaaaaaaaaaaaaay too long" } };


            response = logic.ValidateContactForUpdate(customerContact);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Security QA", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_TOO_LONG, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_SECURITY_ANSWER_MAX_LENGTH_EXCEED, response.Errors[0].errorMessage);

            customerContact.securityQAs = new WSSecurityQA[] { new WSSecurityQA() { securityQuestion = "What was your father's maiden name?", securityAnswer = "Bill" } };


            response = logic.ValidateContactForUpdate(customerContact);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Security QA", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_SECURITY_INVALID_QUESTION, response.Errors[0].errorMessage);

            customerContact.securityQAs = new WSSecurityQA[] { new WSSecurityQA() { securityQuestion = "What was your mother's maiden name?", securityAnswer = "Lisa" } };



            customerContact.addressId = Guid.NewGuid().ToString();
            customerContact.address.address1 = "1 Main Street";

            response = logic.ValidateContactForUpdate(customerContact);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual("Address & AddressId", response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_ADDRESS_AND_ADDID_NOT_ALLOWED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_REQUEST_CONTAINS_BOTH_ADDRESS_AND_ID, response.Errors[0].errorMessage);
        }

        [TestMethod]
        public void IsPhoneValidTest()
        {

        }

        [TestMethod]
        public void GetBirthDateTest()
        {
            var fakedContext = new XrmFakedContext();

            var contactId1 = Guid.NewGuid();
            var contactId2 = Guid.NewGuid();
            var contact1 = new Contact
            {
                ContactId = contactId1
            };
            var contact2 = new Contact
            {
                ContactId = contactId2,
                BirthDate = new DateTime(1990, 8, 24)
            };

            var contacts = new List<Entity>();
            contacts.Add(contact1);
            contacts.Add(contact2);
            fakedContext.Initialize(contacts);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new ContactLogic(fakedService);

            var response = logic.GetBirthDate(contactId1);
            Assert.AreEqual(false, response.HasValue);

            response = logic.GetBirthDate(contactId2);
            Assert.AreEqual(true, response.HasValue);
            Assert.AreEqual("8/24/1990", response?.ToShortDateString());
        }

        [TestMethod]
        public void GetUsernameAndPinTest()
        {
            var fakedContext = new XrmFakedContext();

            var contactId1 = Guid.NewGuid();
            var contactId2 = Guid.NewGuid();
            var contact1 = new Contact
            {
                ContactId = contactId1
            };
            var contact2 = new Contact
            {
                ContactId = contactId2,
            };
            var cred1 = new cub_Credential
            {
                cub_CredentialId = Guid.NewGuid(),
                cub_ContactId = contact1.ToEntityReference(),
                cub_Username = "user@example.com"
            };
            var cred2 = new cub_Credential
            {
                cub_CredentialId = Guid.NewGuid(),
                cub_ContactId = contact2.ToEntityReference(),
                cub_Username = "alice@acme.com",
                cub_Pin = "5678"
            };

            var entities = new List<Entity>();
            entities.Add(contact1);
            entities.Add(contact2);
            entities.Add(cred1);
            entities.Add(cred2);
            fakedContext.Initialize(entities);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new ContactLogic(fakedService);

            var response = logic.GetUsernameAndPin(contactId1);
            Assert.AreEqual("user@example.com", response.cub_Username);
            Assert.AreEqual(null, response.cub_Pin);

            response = logic.GetUsernameAndPin(contactId2);
            Assert.AreEqual("alice@acme.com", response.cub_Username);
            Assert.AreEqual("5678", response.cub_Pin);
        }

        [TestMethod]
        public void CreateVerificationActivityTest()
        {
            var fakedContext = new XrmFakedContext();

            var contactId = Guid.NewGuid();
            var contact = new Contact
            {
                ContactId = contactId
            };

            var entities = new List<Entity>();
            entities.Add(contact);
            fakedContext.Initialize(entities);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new ContactLogic(fakedService);
            var activityId = logic.CreateVerificationActivity(contact.ContactId.Value, "Verified", "Address, Email, Mobile");
            Assert.IsNotNull(activityId);

            using (var osc = new CUBOrganizationServiceContext(fakedService))
            {
                var note = (from n in osc.AnnotationSet
                                select n).FirstOrDefault();
                Assert.IsNotNull(note);
                Assert.AreEqual("Verified", note.Subject);
                Assert.AreEqual("Address, Email, Mobile", note.NoteText);
                Assert.AreEqual(activityId, note.ObjectId.Id);
            }
        }
    }

    [TestClass]
    public class ContactLogicValidateTokenTests
    {
        #region Validate Token Related
        [TestMethod]
        public void NullUsernameTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, null);
            fakedContext.Initialize(testData);
            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>();

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken(null, string.Empty, string.Empty, cub_verificationtokentype.Password);

            Assert.IsNotNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED)));
        }

        [TestMethod]
        public void InvalidUsernameTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, null);
            fakedContext.Initialize(testData);
            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>();

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("invaliduser", string.Empty, string.Empty, cub_verificationtokentype.Password);

            Assert.IsNotNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND)));
        }

        [TestMethod]
        public void ValidUsernameInactiveCredentialTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Inactive, null);
            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Inactive
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", string.Empty, string.Empty, cub_verificationtokentype.Password);

            Assert.IsNotNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_ACCNT_INACTIVE)));
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
        }

        [TestMethod]
        public void ValidUsernameActiveCredentialTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, null);
            fakedContext.Initialize(testData);
            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Active
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", string.Empty, string.Empty, cub_verificationtokentype.Password);

            Assert.IsNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND)));
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
        }

        [TestMethod]
        public void ValidUsernameLockedCredentialTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Locked, null);
            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Locked
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", string.Empty, string.Empty, cub_verificationtokentype.Password);

            Assert.IsNotNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_ACCNT_LOCKED)));
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
        }

        [TestMethod]
        public void ValidUsernameNullStatusCodeCredentialTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", null, null);
            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Inactive
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", string.Empty, string.Empty, cub_verificationtokentype.Password);

            Assert.IsNotNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_ACCNT_INACTIVE)));
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
        }

        [TestMethod]
        public void ValidUserNameActiveCredNoVerificationToken()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, null);
            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Active
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", string.Empty, string.Empty, cub_verificationtokentype.Password);

            Assert.IsNotNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED)
                && e.fieldName.Equals(cub_VerificationToken.cub_verificationtokenidAttribute)));
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
        }

        [TestMethod]
        public void ValidUsernameActiveCredVeriTokenLoginLimitReached()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, 5);
            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Locked
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", "token", string.Empty, cub_verificationtokentype.Password);

            Assert.IsNotNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_ACCNT_LOCKED)
                && e.fieldName.Equals(cub_Credential.cub_credentialidAttribute)));
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
        }

        [TestMethod]
        public void ValidUsernameActiveCredVeriTokenLoginLimitNotReached()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, 3, "token");
            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Active,
                    verificationtokenId = (from t in testData where t.LogicalName.Equals("cub_verificationtoken") select t.Id.ToString()).FirstOrDefault()
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", "token", string.Empty, cub_verificationtokentype.Password);

            Assert.AreEqual(resp.Errors.Count, 0);
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
            Assert.AreEqual(expectedResponse.ResponseObject.verificationtokenId, resp.ResponseObject.verificationtokenId);
        }

        [TestMethod]
        public void ValidUserActiveCredVeriTokenNewPasswordAlreadyUsed()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, 0, "token", cub_verificationtokentype.Password, true);
            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Active,
                    verificationtokenId = (from t in testData where t.LogicalName.Equals("cub_verificationtoken") select t.Id.ToString()).FirstOrDefault()
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", "token", "NewWord", cub_verificationtokentype.Password);

            Assert.IsNotNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_PWD_PREVIOUSLY_USED)));
        }

        [TestMethod]
        public void ValidUserActiveCredVeriTokenNewPassword()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, 3, "token");
            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Active,
                    verificationtokenId = (from t in testData where t.LogicalName.Equals("cub_verificationtoken") select t.Id.ToString()).FirstOrDefault()
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", "token", "NewWord", cub_verificationtokentype.Password);

            Assert.AreEqual(resp.Errors.Count, 0);
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
            Assert.AreEqual(expectedResponse.ResponseObject.verificationtokenId, resp.ResponseObject.verificationtokenId);
        }

        [TestMethod]
        public void InvalidTokenTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, 0, "nekot", cub_verificationtokentype.UnlockAccount);

            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Active,
                    verificationtokenId = (from t in testData where t.LogicalName.Equals("cub_verificationtoken") select t.Id.ToString()).FirstOrDefault()
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", "token", null, cub_verificationtokentype.UnlockAccount);

            Assert.IsNotNull(resp.Errors.Find(e => e.errorKey.Equals(CUBConstants.Error.ERR_GENERAL_VALUE_NOT_FOUND)));
        }

        [TestMethod]
        public void UnlockAccountTokenWithoutPassword()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, 0, "token", cub_verificationtokentype.UnlockAccount);

            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Active,
                    verificationtokenId = (from t in testData where t.LogicalName.Equals("cub_verificationtoken") select t.Id.ToString()).FirstOrDefault()
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", "token", null, cub_verificationtokentype.UnlockAccount);

            Assert.AreEqual(resp.Errors.Count, 0);
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
            Assert.AreEqual(expectedResponse.ResponseObject.verificationtokenId, resp.ResponseObject.verificationtokenId);
        }

        [TestMethod]
        public void UnlockAccountTokenWithPassword()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> testData = new List<Entity>();
            ContactLogic logic = new ContactLogic(fakedService);

            PrepTestData(testData, "username", cub_credential_statuscode.Active, 0, "token", cub_verificationtokentype.UnlockAccount);

            fakedContext.Initialize(testData);

            CubResponse<WSForgotUsernameResponse> expectedResponse = new CubResponse<WSForgotUsernameResponse>()
            {
                ResponseObject = new WSForgotUsernameResponse
                {
                    customerId = (from t in testData where t.LogicalName.Equals("account") select t.Id.ToString()).FirstOrDefault(),
                    contactId = (from t in testData where t.LogicalName.Equals("contact") select t.Id.ToString()).FirstOrDefault(),
                    credentialId = (from t in testData where t.LogicalName.Equals("cub_credential") select t.Id.ToString()).FirstOrDefault(),
                    credStatus = cub_credential_statuscode.Active,
                    verificationtokenId = (from t in testData where t.LogicalName.Equals("cub_verificationtoken") select t.Id.ToString()).FirstOrDefault()
                }
            };

            CubResponse<WSForgotUsernameResponse> resp = logic.ValidateToken("username", "token", "NewWord", cub_verificationtokentype.UnlockAccount);

            Assert.AreEqual(resp.Errors.Count, 0);
            Assert.AreEqual(expectedResponse.ResponseObject.customerId, resp.ResponseObject.customerId);
            Assert.AreEqual(expectedResponse.ResponseObject.contactId, resp.ResponseObject.contactId);
            Assert.AreEqual(expectedResponse.ResponseObject.credentialId, resp.ResponseObject.credentialId);
            Assert.AreEqual(expectedResponse.ResponseObject.credStatus, resp.ResponseObject.credStatus);
            Assert.AreEqual(expectedResponse.ResponseObject.verificationtokenId, resp.ResponseObject.verificationtokenId);
        }

        private void PrepTestData(List<Entity> testData, string username, cub_credential_statuscode? statusCode = null,
            int? failedLoginAttempts = null, string verificationToken = "",
            cub_verificationtokentype tokenType = cub_verificationtokentype.Password, bool add_pwd_already_used = false)
        {
            Account acc = new Account() { AccountId = Guid.NewGuid(), Name = "AccountName" };

            Contact newContact = new Contact()
            {
                ContactId = Guid.NewGuid(),
                FirstName = "Firstname",
                LastName = "Lastname",
                ParentCustomerId = new EntityReference(Account.EntityLogicalName, acc.Id)
            };

            cub_Credential newCred = new cub_Credential()
            {
                cub_CredentialId = Guid.NewGuid(),
                cub_Username = username,
                cub_ContactId = new EntityReference(Contact.EntityLogicalName, newContact.Id),
                statecode = cub_CredentialState.Active,
                cub_FailedLoginAttempts = failedLoginAttempts,
                cub_Password = UtilityLogic.EncryptTripleDES("NewWord")
            };

            if (statusCode != null) newCred.statuscode = new OptionSetValue((int)statusCode);

            cub_VerificationToken veriToken = new cub_VerificationToken()
            {
                cub_VerificationTokenId = Guid.NewGuid(),
                cub_CredentialsId = new EntityReference(cub_Credential.EntityLogicalName, newCred.Id),
                cub_Name = UtilityLogic.EncryptTripleDES(verificationToken),
                cub_TokenType = tokenType
            };

            cub_ArchivedPassword archPasswords = new cub_ArchivedPassword()
            {
                cub_ArchivedPasswordId = Guid.NewGuid(),
                cub_ContactId = new EntityReference(Contact.EntityLogicalName, newContact.Id),
                cub_ArchivedDate = DateTime.Now.AddDays(-5)
            };

            if (add_pwd_already_used) archPasswords.cub_Password = newCred.cub_Password;

            GlobalsHelper.GetGlobalsData(testData);

            testData.Add(acc);
            testData.Add(newContact);
            testData.Add(newCred);
            testData.Add(veriToken);
            testData.Add(archPasswords);
        }

        #endregion Validate Token Related
    }

    [TestClass]
    public class ContactLogicSearchForForgotUsernameTest
    {
        [TestMethod]
        public void FindUsernameSuccessTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            cub_AccountType accountType = new cub_AccountType();
            accountType.cub_AccountTypeId = Guid.NewGuid();
            accountType.cub_AccountName = "Personal";

            Account account = new Account();
            account.cub_AccountTypeId = accountType.ToEntityReference();
            account.AccountId = Guid.NewGuid();
            account.StatusCode = account_statuscode.Active;

            var country = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "USA",
                cub_CountryCode = "99"
            };

            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = country.ToEntityReference(),
                cub_StateName = "Tennessee",
                cub_Abbreviation = "TN"
            };

            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            Contact contact = new Contact();
            contact.ContactId = credentials.cub_ContactId.Id;
            contact.ParentCustomerId = account.ToEntityReference();
            contact.EMailAddress1 = "TheDude@Cubic.com";
            contact.FirstName = "The";
            contact.LastName = "Dude";
            contact.Telephone1 = "9314546408";
            contact.cub_AddressId = address.ToEntityReference();

            Contact contact2 = new Contact();
            contact2.ContactId = Guid.NewGuid();
            contact2.ParentCustomerId = account.ToEntityReference();
            contact2.EMailAddress1 = contact.EMailAddress1;
            contact2.FirstName = contact.FirstName;
            contact2.LastName = contact.LastName;
            contact2.Telephone1 = "9314546409";
            contact2.cub_AddressId = address.ToEntityReference();

            data.Add(credentials);
            data.Add(accountType);
            data.Add(account);
            data.Add(country);
            data.Add(state);
            data.Add(zip);
            data.Add(address);
            data.Add(contact);
            data.Add(contact2);

            fakedContext.Initialize(data);

            ContactLogic logic = new ContactLogic(fakedService);

            string email = contact.EMailAddress1;
            string firstName = contact.FirstName;
            string lastName = contact.LastName;
            string phone = contact.Telephone1;
            string pin = credentials.cub_Pin;
            string postalCode = zip.cub_Code;

            CubResponse<WSForgotUsernameResponse> response = logic.ForgotUsername(firstName, lastName, email, phone, pin, postalCode);

            Assert.AreEqual(true, response.Success);
        }

        [TestMethod]
        public void AccountIsInactiveTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Inactive);

            cub_AccountType accountType = new cub_AccountType();
            accountType.cub_AccountTypeId = Guid.NewGuid();
            accountType.cub_AccountName = "Personal";

            Account account = new Account();
            account.cub_AccountTypeId = accountType.ToEntityReference();
            account.AccountId = Guid.NewGuid();
            account.StatusCode = account_statuscode.Active;

            var country = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "USA",
                cub_CountryCode = "99"
            };

            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = country.ToEntityReference(),
                cub_StateName = "Tennessee",
                cub_Abbreviation = "TN"
            };

            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            Contact contact = new Contact();
            contact.ContactId = credentials.cub_ContactId.Id;
            contact.ParentCustomerId = account.ToEntityReference();
            contact.EMailAddress1 = "TheDude@Cubic.com";
            contact.FirstName = "The";
            contact.LastName = "Dude";
            contact.Telephone1 = "9314546408";
            contact.cub_AddressId = address.ToEntityReference();

            Contact contact2 = new Contact();
            contact2.ContactId = credentials.cub_ContactId.Id;
            contact2.ParentCustomerId = account.ToEntityReference();
            contact2.EMailAddress1 = contact.EMailAddress1;
            contact2.FirstName = contact.FirstName;
            contact2.LastName = contact.LastName;
            contact2.Telephone1 = contact.Telephone1;
            contact2.cub_AddressId = address.ToEntityReference();

            data.Add(credentials);
            data.Add(accountType);
            data.Add(account);
            data.Add(country);
            data.Add(state);
            data.Add(zip);
            data.Add(address);
            data.Add(contact);
            data.Add(contact2);

            fakedContext.Initialize(data);

            ContactLogic logic = new ContactLogic(fakedService);

            string email = contact2.EMailAddress1;
            string firstName = contact2.FirstName;
            string lastName = contact2.LastName;
            string phone = "";
            string pin = credentials.cub_Pin;
            string postalCode = "37388";

            CubResponse<WSForgotUsernameResponse> response = logic.ForgotUsername(firstName, lastName, email, phone, pin, postalCode);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(CUBConstants.Error.ERR_ACCNT_INACTIVE, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_LOGIN_INACTIVE, response.Errors[0].errorMessage);
        }

        [TestMethod]
        public void AccountIsLockedTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Locked);

            cub_AccountType accountType = new cub_AccountType();
            accountType.cub_AccountTypeId = Guid.NewGuid();
            accountType.cub_AccountName = "Personal";

            Account account = new Account();
            account.cub_AccountTypeId = accountType.ToEntityReference();
            account.AccountId = Guid.NewGuid();
            account.StatusCode = account_statuscode.Active;

            var country = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "USA",
                cub_CountryCode = "99"
            };

            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = country.ToEntityReference(),
                cub_StateName = "Tennessee",
                cub_Abbreviation = "TN"
            };

            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            Contact contact = new Contact();
            contact.ContactId = credentials.cub_ContactId.Id;
            contact.ParentCustomerId = account.ToEntityReference();
            contact.EMailAddress1 = "TheDude@Cubic.com";
            contact.FirstName = "The";
            contact.LastName = "Dude";
            contact.Telephone1 = "9314546408";
            contact.cub_AddressId = address.ToEntityReference();

            Contact contact2 = new Contact();
            contact2.ContactId = credentials.cub_ContactId.Id;
            contact2.ParentCustomerId = account.ToEntityReference();
            contact2.EMailAddress1 = contact.EMailAddress1;
            contact2.FirstName = contact.FirstName;
            contact2.LastName = contact.LastName;
            contact2.Telephone1 = contact.Telephone1;
            contact2.cub_AddressId = address.ToEntityReference();

            data.Add(credentials);
            data.Add(accountType);
            data.Add(account);
            data.Add(country);
            data.Add(state);
            data.Add(zip);
            data.Add(address);
            data.Add(contact);
            data.Add(contact2);

            fakedContext.Initialize(data);

            ContactLogic logic = new ContactLogic(fakedService);

            string email = contact2.EMailAddress1;
            string firstName = contact2.FirstName;
            string lastName = contact2.LastName;
            string phone = "";
            string pin = credentials.cub_Pin;
            string postalCode = "37388";

            CubResponse<WSForgotUsernameResponse> response = logic.ForgotUsername(firstName, lastName, email, phone, pin, postalCode);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(CUBConstants.Error.ERR_ACCNT_LOCKED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_LOGIN_LOCKED, response.Errors[0].errorMessage);
        }

        [TestMethod]
        public void AccountIsPendingActivationTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.PendingActivation);

            cub_AccountType accountType = new cub_AccountType();
            accountType.cub_AccountTypeId = Guid.NewGuid();
            accountType.cub_AccountName = "Personal";

            Account account = new Account();
            account.cub_AccountTypeId = accountType.ToEntityReference();
            account.AccountId = Guid.NewGuid();
            account.StatusCode = account_statuscode.Active;

            var country = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "USA",
                cub_CountryCode = "99"
            };

            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = country.ToEntityReference(),
                cub_StateName = "Tennessee",
                cub_Abbreviation = "TN"
            };

            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            Contact contact = new Contact();
            contact.ContactId = credentials.cub_ContactId.Id;
            contact.ParentCustomerId = account.ToEntityReference();
            contact.EMailAddress1 = "TheDude@Cubic.com";
            contact.FirstName = "The";
            contact.LastName = "Dude";
            contact.Telephone1 = "9314546408";
            contact.cub_AddressId = address.ToEntityReference();

            Contact contact2 = new Contact();
            contact2.ContactId = credentials.cub_ContactId.Id;
            contact2.ParentCustomerId = account.ToEntityReference();
            contact2.EMailAddress1 = contact.EMailAddress1;
            contact2.FirstName = contact.FirstName;
            contact2.LastName = contact.LastName;
            contact2.Telephone1 = contact.Telephone1;
            contact2.cub_AddressId = address.ToEntityReference();

            data.Add(credentials);
            data.Add(accountType);
            data.Add(account);
            data.Add(country);
            data.Add(state);
            data.Add(zip);
            data.Add(address);
            data.Add(contact);
            data.Add(contact2);

            fakedContext.Initialize(data);

            ContactLogic logic = new ContactLogic(fakedService);

            string email = contact2.EMailAddress1;
            string firstName = contact2.FirstName;
            string lastName = contact2.LastName;
            string phone = "";
            string pin = credentials.cub_Pin;
            string postalCode = "37388";

            CubResponse<WSForgotUsernameResponse> response = logic.ForgotUsername(firstName, lastName, email, phone, pin, postalCode);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(CUBConstants.Error.ERR_ACCNT_PENDING_ACTIVATION, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_LOGIN_PENDING_ACTIVATION, response.Errors[0].errorMessage);
        }

        [TestMethod]
        public void MultipleContactsFoundTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.PendingActivation);

            cub_AccountType accountType = new cub_AccountType();
            accountType.cub_AccountTypeId = Guid.NewGuid();
            accountType.cub_AccountName = "Personal";

            Account account = new Account();
            account.cub_AccountTypeId = accountType.ToEntityReference();
            account.AccountId = Guid.NewGuid();
            account.StatusCode = account_statuscode.Active;

            var country = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "USA",
                cub_CountryCode = "99"
            };

            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = country.ToEntityReference(),
                cub_StateName = "Tennessee",
                cub_Abbreviation = "TN"
            };

            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            Contact contact = new Contact();
            contact.ContactId = credentials.cub_ContactId.Id;
            contact.ParentCustomerId = account.ToEntityReference();
            contact.EMailAddress1 = "TheDude@Cubic.com";
            contact.FirstName = "The";
            contact.LastName = "Dude";
            contact.Telephone1 = "9314546408";
            contact.cub_AddressId = address.ToEntityReference();

            Contact contact2 = new Contact();
            contact2.ContactId = Guid.NewGuid();
            contact2.ParentCustomerId = account.ToEntityReference();
            contact2.EMailAddress1 = contact.EMailAddress1;
            contact2.FirstName = contact.FirstName;
            contact2.LastName = contact.LastName;
            contact2.Telephone1 = "9314546409";
            contact2.cub_AddressId = address.ToEntityReference();

            data.Add(credentials);
            data.Add(accountType);
            data.Add(account);
            data.Add(country);
            data.Add(state);
            data.Add(zip);
            data.Add(address);
            data.Add(contact);
            data.Add(contact2);

            fakedContext.Initialize(data);

            ContactLogic logic = new ContactLogic(fakedService);

            string email = contact2.EMailAddress1;
            string firstName = contact2.FirstName;
            string lastName = contact2.LastName;
            string phone = "";
            string pin = "";
            string postalCode = "";

            CubResponse<WSForgotUsernameResponse> response = logic.ForgotUsername(firstName, lastName, email, phone, pin, postalCode);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(CUBConstants.Error.ERR_ACCNT_NOT_UNIQUE, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_UNABLE_FIND_UNIQUE_ACCOUNT, response.Errors[0].errorMessage);
        }

        [TestMethod]
        public void FindUsernameNoContactFoundTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            cub_AccountType accountType = new cub_AccountType();
            accountType.cub_AccountTypeId = Guid.NewGuid();
            accountType.cub_AccountName = "Personal";

            Account account = new Account();
            account.cub_AccountTypeId = accountType.ToEntityReference();
            account.AccountId = Guid.NewGuid();
            account.StatusCode = account_statuscode.Active;

            var country = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "USA",
                cub_CountryCode = "99"
            };

            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = country.ToEntityReference(),
                cub_StateName = "Tennessee",
                cub_Abbreviation = "TN"
            };

            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            Contact contact = new Contact();
            contact.ContactId = credentials.cub_ContactId.Id;
            contact.ParentCustomerId = account.ToEntityReference();
            contact.EMailAddress1 = "thedude@cubic.com";
            contact.FirstName = "The";
            contact.LastName = "Dude";
            contact.Telephone1 = "9314546408";
            contact.cub_AddressId = address.ToEntityReference();

            Contact contact2 = new Contact();
            contact2.ContactId = Guid.NewGuid();
            contact2.ParentCustomerId = account.ToEntityReference();
            contact2.EMailAddress1 = contact.EMailAddress1;
            contact2.FirstName = contact.FirstName;
            contact2.LastName = contact.LastName;
            contact2.Telephone1 = "9314546409";
            contact2.cub_AddressId = address.ToEntityReference();

            data.Add(credentials);
            data.Add(accountType);
            data.Add(account);
            data.Add(country);
            data.Add(state);
            data.Add(zip);
            data.Add(address);
            data.Add(contact);
            data.Add(contact2);

            fakedContext.Initialize(data);

            ContactLogic logic = new ContactLogic(fakedService);

            string email = "NotTheDude@Cubic.com";
            string firstName = "NotThe";
            string lastName = "Dude";
            string phone = "";
            string pin = "5678";
            string postalCode = "60601";

            CubResponse<WSForgotUsernameResponse> response = logic.ForgotUsername(firstName, lastName, email, phone, pin, postalCode);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT, response.Errors[0].errorMessage);
        }

        [TestMethod]
        public void FindPopulatedFieldsTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.PendingActivation);

            cub_Credential credentials2 = new cub_Credential();
            credentials2.cub_CredentialId = Guid.NewGuid();
            credentials2.cub_ContactId = new EntityReference();
            credentials2.cub_ContactId.Id = Guid.NewGuid();
            credentials2.cub_Username = "theDude";
            credentials2.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials2.cub_LockoutDtm = null;
            credentials2.cub_Pin = "1234";
            credentials2.cub_ForceChangePassword = false;
            credentials2.statuscode = new OptionSetValue((int)cub_credential_statuscode.PendingActivation);

            cub_Credential credentials3 = new cub_Credential();
            credentials3.cub_CredentialId = Guid.NewGuid();
            credentials3.cub_ContactId = new EntityReference();
            credentials3.cub_ContactId.Id = Guid.NewGuid();
            credentials3.cub_Username = "theDude";
            credentials3.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials3.cub_LockoutDtm = null;
            credentials3.cub_Pin = "1234";
            credentials3.cub_ForceChangePassword = false;
            credentials3.statuscode = new OptionSetValue((int)cub_credential_statuscode.PendingActivation);

            cub_AccountType accountType = new cub_AccountType();
            accountType.cub_AccountTypeId = Guid.NewGuid();
            accountType.cub_AccountName = "Personal";

            Account account = new Account();
            account.cub_AccountTypeId = accountType.ToEntityReference();
            account.AccountId = Guid.NewGuid();
            account.StatusCode = account_statuscode.Active;

            var country = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "USA",
                cub_CountryCode = "99"
            };

            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = country.ToEntityReference(),
                cub_StateName = "Tennessee",
                cub_Abbreviation = "TN"
            };

            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            var zip2 = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address2 = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            var zip3 = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address3 = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            Contact contact = new Contact();
            contact.ContactId = credentials.cub_ContactId.Id;
            contact.ParentCustomerId = account.ToEntityReference();
            contact.EMailAddress1 = "TheDude@Cubic.com";
            contact.FirstName = "The";
            contact.LastName = "Dude";
            contact.Telephone1 = "9314546408";
            contact.cub_AddressId = address.ToEntityReference();

            Contact contact2 = new Contact();
            contact2.ContactId = credentials2.cub_ContactId.Id;
            contact2.ParentCustomerId = account.ToEntityReference();
            contact2.EMailAddress1 = contact.EMailAddress1;
            contact2.FirstName = contact.FirstName;
            contact2.LastName = contact.LastName;
            contact2.Telephone1 = contact.Telephone1;
            contact2.cub_AddressId = address2.ToEntityReference();

            Contact contact3 = new Contact();
            contact3.ContactId = credentials3.cub_ContactId.Id;
            contact3.ParentCustomerId = account.ToEntityReference();
            contact3.EMailAddress1 = contact.EMailAddress1;
            contact3.FirstName = contact.FirstName;
            contact3.LastName = contact.LastName;
            contact3.Telephone1 = contact.Telephone1;
            contact3.cub_AddressId = address3.ToEntityReference();

            data.Add(credentials);
            data.Add(credentials2);
            data.Add(credentials3);
            data.Add(accountType);
            data.Add(account);
            data.Add(country);
            data.Add(state);
            data.Add(zip);
            data.Add(zip2);
            data.Add(zip3);
            data.Add(address);
            data.Add(address2);
            data.Add(address3);
            data.Add(contact);
            data.Add(contact2);
            data.Add(contact3);

            fakedContext.Initialize(data);

            ContactLogic logic = new ContactLogic(fakedService);

            string email = contact2.EMailAddress1;
            string firstName = "";
            string lastName = "";
            string phone = "";
            string pin = "";
            string postalCode = "";

            CubResponse<WSForgotUsernameResponse> response = logic.ForgotUsername(firstName, lastName, email, phone, pin, postalCode);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(CUBConstants.Error.ERR_ACCNT_NOT_UNIQUE, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_UNABLE_FIND_UNIQUE_ACCOUNT, response.Errors[0].errorMessage);
        }
    }

    [TestClass]
    public class ContactLogicCustomerValidationTest
    {
        [TestMethod]
        public void HappyPathTest()
        {
            var fakedContext = new XrmFakedContext();
            cub_AccountType accountType;
            cub_ContactType contactType;
            cub_Country country;
            cub_StateOrProvince state;
            cub_ZipOrPostalCode zip;
            cub_SecurityQuestion question;
            List<Entity> data = new List<Entity>();
            PrepareData(data, out accountType, out contactType, out country, out state, out zip, out question);
            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(data);
            CustomerRegistrationLogic logic = new CustomerRegistrationLogic(fakedService);
            CustomerRegistrationModel model = new CustomerRegistrationModel
            {
                AccountType = accountType.cub_AccountTypeId.Value,
                ContactType = contactType.cub_ContactTypeId.Value,
                FirstName = "Jose",
                LastName = "Santos",
                MiddleNameInitial = "M",
                BirthDate = new DateTime(1972, 3, 25),
                Email = "airamez@gmail.com",
                Phone1Type = (int)cub_phonetype.Mobile,
                Phone1 = "+1 604-3188645",
                Address1 = "11006 Buckerfield Dr",
                City = "San Diego",
                State = state.cub_StateOrProvinceId.Value,
                Country = country.cub_CountryId.Value,
                ZipPostalCodeId = zip.cub_ZipOrPostalCodeId.Value,
                SecurityQAs = new Dictionary<Guid, string>()
                {
                    {
                        question.cub_SecurityQuestionId.Value, "This is my anser"
                    }
                },

                PIN = "12345",
                UserName = "airamez"
            };
            var response = logic.ValidateFieldsNewCustomerRegistration(model);
            Assert.AreEqual(0, response.Count);
        }

        [TestMethod]
        public void AllErrorsTest()
        {
            var fakedContext = new XrmFakedContext();
            cub_AccountType accountType;
            cub_ContactType contactType;
            cub_Country country;
            cub_StateOrProvince state;
            cub_ZipOrPostalCode zip;
            cub_SecurityQuestion question;
            List<Entity> data = new List<Entity>();
            PrepareData(data, out accountType, out contactType, out country, out state, out zip, out question);
            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(data);
            CustomerRegistrationLogic logic = new CustomerRegistrationLogic(fakedService);
            CustomerRegistrationModel model = new CustomerRegistrationModel
            {
                AccountType = accountType.cub_AccountTypeId.Value,
                ContactType = contactType.cub_ContactTypeId.Value,
                FirstName = "Jose".PadRight(100, 'X'),
                LastName = "",
                MiddleNameInitial = "M".PadRight(2, 'X'),
                BirthDate = new DateTime(1972, 3, 25),
                Email = "aaa",
                Phone1Type = (int)cub_phonetype.Mobile,
                Phone1 = "3188645",
                Address1 = "11006 Buckerfield Dr".PadRight(500, 'X'),
                City = "San Diego".PadRight(500, 'X'),
                State = state.cub_StateOrProvinceId.Value,
                Country = country.cub_CountryId.Value,
                ZipPostalCodeId = zip.cub_ZipOrPostalCodeId.Value,
                SecurityQAs = new Dictionary<Guid, string>() {
                    {
                        question.cub_SecurityQuestionId.Value, "This is my anser".PadRight(500, 'X')
                    }
                },
                PIN = "12345".PadRight(50, 'X'),
                UserName = "airamez".PadRight(500, 'X')
            };
            var response = logic.ValidateFieldsNewCustomerRegistration(model);
            Assert.AreEqual(7, response.Count);
        }

        private static void PrepareData(List<Entity> data, out cub_AccountType accountType, out cub_ContactType contactType, out cub_Country country, out cub_StateOrProvince state, out cub_ZipOrPostalCode zip, out cub_SecurityQuestion question)
        {
            accountType = new cub_AccountType { cub_AccountTypeId = Guid.NewGuid(), cub_AccountName = "Account Type" };
            data.Add(accountType);
            contactType = new cub_ContactType { cub_ContactTypeId = Guid.NewGuid(), cub_Name = "Contact Type" };
            data.Add(contactType);
            country = new cub_Country { cub_CountryId = Guid.NewGuid(), cub_CountryName = "US" };
            data.Add(country);
            state = new cub_StateOrProvince
            {
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, country.cub_CountryId.Value),
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_StateName = "California",
                cub_Abbreviation = "CA"
            };
            data.Add(state);
            zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, country.cub_CountryId.Value),
                cub_State = new EntityReference(cub_StateOrProvince.EntityLogicalName, state.cub_StateOrProvinceId.Value),
                cub_Code = "9999"
            };
            data.Add(zip);
            question = new cub_SecurityQuestion
            {
                cub_SecurityQuestionId = Guid.NewGuid(),
                cub_SecurityQuestionDesc = "Security Question"
            };
            data.Add(question);
            cub_Globals globals = new cub_Globals
            {
                cub_GlobalsId = Guid.NewGuid(),
                cub_Enabled = true,
                cub_Name = "UsernameValidation"
            };
            data.Add(globals);
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Enabled = true,
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, globals.cub_GlobalsId.Value),
                cub_Name = "UsernameMinLength",
                cub_AttributeValue = "5"
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Enabled = true,
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, globals.cub_GlobalsId.Value),
                cub_Name = "UsernameMaxLength",
                cub_AttributeValue = "10"
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Enabled = true,
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, globals.cub_GlobalsId.Value),
                cub_Name = "UsernameRequireEmail",
                cub_AttributeValue = "0"
            });
            globals = new cub_Globals
            {
                cub_GlobalsId = Guid.NewGuid(),
                cub_Enabled = true,
                cub_Name = "ValidateEmailFormat"
            };
            data.Add(globals);
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Enabled = true,
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, globals.cub_GlobalsId.Value),
                cub_Name = "EmailMatchRegex",
                cub_AttributeValue = "//^.{6,}$//"
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Enabled = true,
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, globals.cub_GlobalsId.Value),
                cub_Name = "UseEmailForUsername",
                cub_AttributeValue = "0"
            });
        }
    }
}

