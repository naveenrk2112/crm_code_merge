/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System.Collections.Generic;
using CUB.MSD.Logic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System;
using CUB.MSD.Plugins.Test;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class AccountLogicTest
    {
      
        [TestMethod]
        public void CreateAccountTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            AccountLogic logic = new AccountLogic(fakedService);
            WSCustomer customer = new WSCustomer();
            CubResponse<WSCustomer> response;

            List<Entity> data = new List<Entity>();
            data.AddRange(AccountHelper.GetCustomerTypes());

            Guid countryId = Guid.NewGuid();
            data.Add(ContactHelper.GetCountry(countryId));

            Guid contactTypeId = Guid.NewGuid();
            cub_ContactType accType = new cub_ContactType();
            accType.cub_Name = "Primary";
            accType.cub_ContactTypeId = contactTypeId;

            data.Add(accType);
            data.AddRange(ContactHelper.GetSecurityQuestions());
            fakedContext.Initialize(data);


            response = logic.RegisterCustomer(customer);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual(Account.cub_accounttypeidAttribute, response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_FIELD_IS_BLANK, response.Errors[0].errorMessage);

            customer.customerType = "TEST";

            response = logic.RegisterCustomer(customer);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual(Account.cub_accounttypeidAttribute, response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_INVALID_CUSTOMER_TYPE, response.Errors[0].errorMessage);

            customer.customerType = "Individual";

            response = logic.RegisterCustomer(customer);

            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(1, response.Errors.Count);
            Assert.AreEqual(Contact.EntityLogicalName, response.Errors[0].fieldName);
            Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, response.Errors[0].errorKey);
            Assert.AreEqual(CUBConstants.Error.MSG_FIELD_IS_BLANK, response.Errors[0].errorMessage);


            customer.contact= new WSCustomerContact()
            {
                contactType = "Primary",
                email = "billymitchell@gmail.com",
                dateOfBirth = "1879-12-29T05:00:00Z",
                username = "TheGeneral",
                password = "Sup3rS3cr3tP@s$w0rd",
                pin = "1917",

                name = new WSName()
                {

                    firstName = "Billy",
                    lastName = "Mitchell",
                    middleInitial = "L",
                    title = "Mr"
                },

                address = new WSAddress()
                {
                    address1 = "33 Long Street",
                    address2 = "Apt 34c",
                    city = "New York City",
                    state = "NY",
                    postalCode = "10006",
                    country = "US"
                },

                phone = new WSPhone[] { new WSPhone() { type = "M", number = "2128907678" }, new WSPhone() { type = "H", number = "2127554382" }, new WSPhone() { type = "W", number = "2123542990" } },

                personalIdentifierInfo = new WSPersonalIdentifier()
                {
                    personalIdentifier = "1",
                    personalIdentifierType = "DriversLicense"
                },

                securityQAs = new WSSecurityQA[] { new WSSecurityQA() { securityQuestion = "What was your mother's maiden name?", securityAnswer = "Harriet" }, new WSSecurityQA() { securityQuestion = "What was the make of your first car?", securityAnswer = "SPAD" } }
            };

            response = logic.RegisterCustomer(customer);

        }

        [TestMethod]
        public void SearchCustomerTest()
        {
            var fakedContext = new XrmFakedContext(); ;
            var fakedService = fakedContext.GetOrganizationService();

            fakedContext.Initialize(AccountHelper.GetCustomerData());
            
            AccountLogic logic = new AccountLogic(fakedService);
            CubResponse<Model.NIS.WSCustomerSearchResponse> resposne;

            //Return all Customers where customerType = "Indiviual";
            resposne = logic.SearchCustomer("Individual", null, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, 0);
            Assert.AreEqual(true, resposne.Success);
            Assert.AreEqual(typeof(Model.NIS.WSCustomerSearchResponse), resposne.ResponseObject.GetType());
            Assert.AreEqual(1, resposne.ResponseObject.totalCount);

            //Return all Customers where customerType = "Indiviual" And Contact First Name = "Tony";
            resposne = logic.SearchCustomer("Individual", "Cubic", null, null, null, null, null, null, null, null, null, null, null, null, null, 0, 0);
            Assert.AreEqual(true, resposne.Success);
            Assert.AreEqual(typeof(Model.NIS.WSCustomerSearchResponse), resposne.ResponseObject.GetType());
            Assert.AreEqual(1, resposne.ResponseObject.totalCount);

            //Return all Customers where customerType = "Indiviual" And Contact Last Name = "Riak";
            resposne = logic.SearchCustomer("Individual", null, "Riak", null, null, null, null, null, null, null, null, null, null, null, null, 0, 0);
            Assert.AreEqual(true, resposne.Success);
            Assert.AreEqual(typeof(Model.NIS.WSCustomerSearchResponse), resposne.ResponseObject.GetType());
            Assert.AreEqual(0, resposne.ResponseObject.totalCount);
        }


        [TestMethod]
        public void SearchCustomerByIdTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();

            fakedContext.Initialize(AccountHelper.GetCustomerData());

            AccountLogic logic = new AccountLogic(fakedService);
            CubResponse<Model.NIS.WSCustomerSearchResponse> resposne;

            List<string> customerIds = new List<string>();
            customerIds.Add("5ee2976d-6ab6-e611-80e0-005056813f0c");

            //Return 1 customer;
            resposne = logic.SearchCustomerById(customerIds, null, 0, 0);
            Assert.AreEqual(true, resposne.Success);
            Assert.AreEqual(typeof(Model.NIS.WSCustomerSearchResponse), resposne.ResponseObject.GetType());
            Assert.AreEqual(1, resposne.ResponseObject.totalCount);

            ////Return all Customers where customerType = "Indiviual" And Contact First Name = "Tony";
            //resposne = logic.SearchCustomer("Individual", "Cubic", null, null, null, null, null, null, null, 0, 0);
            //Assert.AreEqual(true, resposne.Success);
            //Assert.AreEqual(typeof(Model.NIS.WSCustomerSearchResponse), resposne.ResponseObject.GetType());
            //Assert.AreEqual(1, resposne.ResponseObject.totalCount);

            ////Return all Customers where customerType = "Indiviual" And Contact Last Name = "Riak";
            //resposne = logic.SearchCustomer("Individual", null, "Riak", null, null, null, null, null, null, 0, 0);
            //Assert.AreEqual(false, resposne.Success);
            //Assert.AreEqual(typeof(Model.NIS.WSCustomerSearchResponse), resposne.ResponseObject.GetType());
            //Assert.AreEqual(CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT, resposne.Errors[0].errorMessage);
        }
    }
}
