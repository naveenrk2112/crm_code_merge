/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CUB.MSD.Logic;
using CUB.MSD.Model;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UtilityLogicTest
    {
        public UtilityLogicTest()
        {
        }

        [TestMethod]
        public void GetOptionSetDataTest()
        {
            UtilityLogic util = new UtilityLogic();
            List<KeyValuePair<int, string>> optionSetData = util.GetOptionSetMetaData(typeof(cub_phonetype));
            Assert.AreEqual(optionSetData.Count, 8);
            Assert.AreEqual(optionSetData[0].Value, "Home");
            Assert.AreEqual(optionSetData[0].Key, 971880000);
            Assert.AreEqual(optionSetData[2].Value, "Mobile");
            Assert.AreEqual(optionSetData[2].Key, 971880001);
            Assert.AreEqual(optionSetData[3].Value, "Work");
            Assert.AreEqual(optionSetData[3].Key, 971880003);
        }
    }
}
