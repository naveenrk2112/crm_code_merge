﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using FakeXrmEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using CUB.MSD.Logic;
using System.Linq;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class ActivityLogicTest
    {
        [TestMethod]
        public void GetByTransitAccountNoArgumentTest()
        {
            var fakedContext = new XrmFakedContext();
            var fakedService = fakedContext.GetOrganizationService();
            var logic = new ActivityLogic(fakedService);
            var result = logic.GetByTransitAccount(null);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.activities);
            Assert.IsFalse(result.activities.Any());
        }

        // Fails because of the following condition in fetch XML, which works in production:
        // <condition entityname='cub_transitaccount' attribute='cub_name' operator='eq' value='{transitAccountId}' />
        [TestMethod]
        public void GetByTransitAccountNoCustomerTest()
        {
            const string ID = "001";
            var fakedContext = new XrmFakedContext();

            var transitAccount = new cub_TransitAccount
            {
                Id = Guid.NewGuid(),
                cub_Name = ID
            };
            var activity = new PhoneCall
            {
                Id = Guid.NewGuid(),
                Subject = "Activity",
                RegardingObjectId = transitAccount.ToEntityReference()
            };

            var entities = new List<Entity>();
            entities.Add(transitAccount);
            entities.Add(activity);
            fakedContext.Initialize(entities);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new ActivityLogic(fakedService);
            var results = logic.GetByTransitAccount(ID);
            Assert.AreEqual(true, results.activities.Any());
            Assert.AreEqual(1, results.activities.Count());
        }

        // Fails because of the following condition in fetch XML, which works in production:
        // <condition entityname='cub_transitaccount' attribute='cub_name' operator='eq' value='{transitAccountId}' />
        [TestMethod]
        public void GetByTransitAccountWithCustomerTest()
        {
            const string ID = "002";
            var fakedContext = new XrmFakedContext();

            var customer = new Account
            {
                Id = Guid.NewGuid()
            };
            var activity1 = new Appointment
            {
                Id = Guid.NewGuid(),
                Subject = "Activity 1",
                RegardingObjectId = customer.ToEntityReference()
            };

            var transitAccount = new cub_TransitAccount
            {
                Id = Guid.NewGuid(),
                cub_Name = ID,
                cub_Customer = customer.ToEntityReference()
            };
            var activity2 = new Email
            {
                Id = Guid.NewGuid(),
                Subject = "Activity 2",
                RegardingObjectId = transitAccount.ToEntityReference()
            };

            var entities = new List<Entity>();
            entities.Add(customer);
            entities.Add(transitAccount);
            entities.Add(activity1);
            entities.Add(activity2);
            fakedContext.Initialize(entities);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new ActivityLogic(fakedService);
            var results = logic.GetByTransitAccount(ID);
            Assert.AreEqual(true, results.activities.Any());
            Assert.AreEqual(2, results.activities.Count());
        }
    }
}
