/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using FakeXrmEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using CUB.MSD.Logic;
using System.Linq;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class UserLogicTest
    {
        [TestMethod]
        public void GetRecentlyVerifiedContactIdsTest()
        {
            var fakedContext = new XrmFakedContext();

            var globalId = Guid.NewGuid();
            var global = new cub_Globals
            {
                cub_GlobalsId = globalId,
                cub_Name = "CUB.MSD.Web.Angular",
                cub_Enabled = true
            };
            var globalDetail1 = new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_GlobalId = global.ToEntityReference(),
                cub_Name = "RecentVerificationsOffsetInMinutes",
                cub_AttributeValue = "30",
                cub_Enabled = true
            };
            var globalDetail2 = new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_GlobalId = global.ToEntityReference(),
                cub_Name = "SubjectVerified",
                cub_AttributeValue = "Verified",
                cub_Enabled = true
            };
            var contact = new Contact
            {
                ContactId = Guid.NewGuid()
            };
            var oldActivity = new cub_Verification
            {
                ActivityId = Guid.NewGuid(),
                Subject = "Customer Verification",
                RegardingObjectId = contact.ToEntityReference()
            };
            var oldActivityEntity = oldActivity.ToEntity<Entity>();
            var oldTime = DateTime.Now.AddDays(-5);
            oldActivityEntity["createdon"] = oldTime;
            oldActivityEntity["modifiedon"] = oldTime;
            var oldNote = new Annotation
            {
                AnnotationId = Guid.NewGuid(),
                ObjectId = oldActivity.ToEntityReference(),
                Subject = "Not Verified",
            };
            var newActivity = new cub_Verification
            {
                ActivityId = Guid.NewGuid(),
                Subject = "Customer Verification",
                RegardingObjectId = contact.ToEntityReference()
            };
            var newNote = new Annotation
            {
                AnnotationId = Guid.NewGuid(),
                ObjectId = newActivity.ToEntityReference(),
                Subject = globalDetail2.cub_AttributeValue
            };

            var entities = new List<Entity>();
            entities.Add(global);
            entities.Add(globalDetail1);
            entities.Add(globalDetail2);
            entities.Add(contact);
            entities.Add(oldActivityEntity);
            entities.Add(oldNote);
            entities.Add(newActivity);
            entities.Add(newNote);
            fakedContext.Initialize(entities);

            var fakedService = fakedContext.GetOrganizationService();
            var logic = new UserLogic(fakedService);
            var results = logic.GetRecentlyVerifiedContactIds(Guid.NewGuid());
            Assert.AreEqual(true, results.Any());
            Assert.AreEqual(1, results.Count());
            Assert.AreEqual(contact.ContactId.ToString(), results.FirstOrDefault());
        }
    }
}
