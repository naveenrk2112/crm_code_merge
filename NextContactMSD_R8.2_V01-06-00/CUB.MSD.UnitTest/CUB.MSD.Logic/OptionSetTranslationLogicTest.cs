/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CUB.MSD.Logic;
using CUB.MSD.Model;
using FakeXrmEasy;
using System.Reflection;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    [TestClass]
    public class OptionSetTranslationLogicTest
    {
        [TestMethod]
        public void TestGlobalOptionSet()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(GetFakeData());
            OptionSetTranslationLogic logic = new OptionSetTranslationLogic(fakedService);
            // Testing GetValue
            Assert.AreEqual((int)cub_phonetype.Home, logic.GetValue(null, "cub_phonetype", "H"));
            Assert.AreEqual((int)cub_phonetype.Fax, logic.GetValue(null, "cub_phonetype", "F"));
            Assert.AreEqual((int)cub_phonetype.Mobile, logic.GetValue(null, "cub_phonetype", "M"));
            Assert.AreEqual((int)cub_phonetype.Work, logic.GetValue(null, "cub_phonetype", "W"));
            Assert.AreEqual((int)cub_phonetype.DayTime, logic.GetValue(null, "cub_phonetype", "D"));
            Assert.AreEqual((int)cub_phonetype.Evening, logic.GetValue(null, "cub_phonetype", "E"));
            Assert.AreEqual((int)cub_phonetype.Pager, logic.GetValue(null, "cub_phonetype", "P"));
            Assert.AreEqual((int)cub_phonetype.Other, logic.GetValue(null, "cub_phonetype", "O"));
            // Testing the caching
            logic = new OptionSetTranslationLogic(fakedService);
            // Testing GetText
            Assert.AreEqual("H", logic.GetText(null, "cub_phonetype", (int)cub_phonetype.Home));
            Assert.AreEqual("F", logic.GetText(null, "cub_phonetype", (int)cub_phonetype.Fax));
            Assert.AreEqual("M", logic.GetText(null, "cub_phonetype", (int)cub_phonetype.Mobile));
            Assert.AreEqual("W", logic.GetText(null, "cub_phonetype", (int)cub_phonetype.Work));
            Assert.AreEqual("D", logic.GetText(null, "cub_phonetype", (int)cub_phonetype.DayTime));
            Assert.AreEqual("E", logic.GetText(null, "cub_phonetype", (int)cub_phonetype.Evening));
            Assert.AreEqual("P", logic.GetText(null, "cub_phonetype", (int)cub_phonetype.Pager));
            Assert.AreEqual("O", logic.GetText(null, "cub_phonetype", (int)cub_phonetype.Other));
        }

        [TestMethod]
        public void TestOptionSet()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            cub_OptionSetTranslation translation = new cub_OptionSetTranslation
            {
                cub_OptionSetTranslationId = Guid.NewGuid(),
                cub_EntityName = "entityName",
                cub_OptionSetName = "optionSetName"
            };
            data.Add(translation);
            EntityReference translationReference = new EntityReference(cub_OptionSetTranslation.EntityLogicalName,
                                                                       translation.cub_OptionSetTranslationId.Value);
            cub_OptionSetTranslationDetail detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "X",
                cub_OptionSetValue = 99999
            };
            data.Add(detail);
            fakedContext.Initialize(data);
            OptionSetTranslationLogic logic = new OptionSetTranslationLogic(fakedService);
            // Testing GetValue
            Assert.AreEqual(99999, logic.GetValue("entityName", "optionSetName", "X"));
            // Testing the caching
            logic = new OptionSetTranslationLogic(fakedService);
            // Testing GetText
            Assert.AreEqual("X", logic.GetText("entityName", "optionSetName", 99999));
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void TestMissingTranslation()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            OptionSetTranslationLogic logic = new OptionSetTranslationLogic(fakedService);
            logic.GetText("entityName", "cub_phonetype", (int)cub_phonetype.Home);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void TestMissingValue()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            cub_OptionSetTranslation translation = new cub_OptionSetTranslation
            {
                cub_OptionSetTranslationId = Guid.NewGuid(),
                cub_EntityName = "entityName",
                cub_OptionSetName = "cub_phonetype"
            };
            data.Add(translation);
            fakedContext.Initialize(data);
            OptionSetTranslationLogic logic = new OptionSetTranslationLogic(fakedService);
            logic.GetText("entityName", "cub_phonetype", (int)cub_phonetype.Home);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void TestMissingText()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            cub_OptionSetTranslation translation = new cub_OptionSetTranslation
            {
                cub_OptionSetTranslationId = Guid.NewGuid(),
                cub_EntityName = "entityName",
                cub_OptionSetName = "cub_phonetype"
            };
            data.Add(translation);
            fakedContext.Initialize(data);
            OptionSetTranslationLogic logic = new OptionSetTranslationLogic(fakedService);
            logic.GetValue("entityName", "cub_phonetype", "H");
        }

        private static List<Entity> GetFakeData()
        {
            List<Entity> data = new List<Entity>();
            cub_OptionSetTranslation translation = new cub_OptionSetTranslation
            {
                cub_OptionSetTranslationId = Guid.NewGuid(),
                cub_EntityName = null,
                cub_OptionSetName = "cub_phonetype"
            };
            data.Add(translation);
            EntityReference translationReference = new EntityReference(cub_OptionSetTranslation.EntityLogicalName,
                                                                       translation.cub_OptionSetTranslationId.Value);
            cub_OptionSetTranslationDetail detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "H",
                cub_OptionSetValue = 971880000
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "F",
                cub_OptionSetValue = 971880002
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "M",
                cub_OptionSetValue = 971880001
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "W",
                cub_OptionSetValue = 971880003
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "D",
                cub_OptionSetValue = 971880004
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "E",
                cub_OptionSetValue = 971880005
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "P",
                cub_OptionSetValue = 971880007
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "O",
                cub_OptionSetValue = 971880006
            };
            data.Add(detail);
            return data;
        }
    }
}
