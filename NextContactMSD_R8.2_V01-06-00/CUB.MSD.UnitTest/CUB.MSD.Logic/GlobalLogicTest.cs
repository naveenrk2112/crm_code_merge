/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Collections.Generic;
using CUB.MSD.Logic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;

namespace CUB.MSD.Plugins.Test
{
    [TestClass]
    public class GlobalLogicUnitTest
    {
        [TestMethod]
        public void GetLookupRecord()
        {
            var fakedContext = new XrmFakedContext();

            Guid accTypeId = Guid.NewGuid();
            cub_AccountType accType = new cub_AccountType();
            accType.cub_AccountName = "Organization";
            accType.cub_AccountTypeId = accTypeId;

            List<Entity> data = new List<Entity>();
            data.Add(accType);
          

            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(data);

            GlobalsLogic logic = new GlobalsLogic(fakedService);
            Assert.AreEqual(accTypeId,logic.GetLookupRecord(cub_AccountType.EntityLogicalName, cub_AccountType.cub_accountnameAttribute, "Organization"));
            Assert.IsNull(logic.GetLookupRecord(cub_AccountType.EntityLogicalName, cub_AccountType.cub_accountnameAttribute, "test"));
        }
    }
}
