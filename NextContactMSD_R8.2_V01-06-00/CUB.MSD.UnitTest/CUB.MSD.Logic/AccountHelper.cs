/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    public static class  AccountHelper
    {
        public static List<Entity> GetCustomerTypes()
        {
            List<Entity> data = new List<Entity>();

            data.Add(new cub_AccountType
            {
                cub_AccountName = "Individual",
                cub_AccountTypeId = Guid.NewGuid()
            });
            return data;
        }

        public static List<Entity> GetCustomerData()
        {
            List<Entity> data = new List<Entity>();
            
            #region Add Country Data
            cub_Country us = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "United States of America",
                cub_Alpha2 = "US",
                cub_Address2 = new OptionSetValue((int)cub_Countrycub_Address2.REQ),
                cub_Address3 = new OptionSetValue((int)cub_Countrycub_Address3.OPT),
                cub_City = new OptionSetValue((int)cub_Countrycub_City.REQ),
                cub_State = new OptionSetValue((int)cub_Countrycub_State.REQ),
                cub_PostalCode = new OptionSetValue((int)cub_Countrycub_PostalCode.REQ),
                cub_DialingCode = "1"
            };
            data.Add(us);
            cub_StateOrProvince stateTN = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "TN",
                cub_StateName = "Tennesee"
            };
            data.Add(stateTN);
            cub_StateOrProvince stateCA = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "CA",
                cub_StateName = "California"
            };
            data.Add(stateCA);
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "FL",
                cub_StateName = "Florida"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "WA",
                cub_StateName = "Washington"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "CO",
                cub_StateName = "Colorado"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "TX",
                cub_StateName = "Texas"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "OR",
                cub_StateName = "Oregon"
            });
            cub_Country canada = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "Canada",
                cub_Alpha2 = "CA",
                cub_Address2 = new OptionSetValue((int)cub_Countrycub_Address2.REQ),
                cub_Address3 = new OptionSetValue((int)cub_Countrycub_Address3.OPT),
                cub_City = new OptionSetValue((int)cub_Countrycub_City.REQ),
                cub_State = new OptionSetValue((int)cub_Countrycub_State.REQ),
                cub_PostalCode = new OptionSetValue((int)cub_Countrycub_PostalCode.REQ),
                cub_DialingCode = "1"
            };
            data.Add(canada);
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, canada.cub_CountryId.Value),
                cub_Abbreviation = "BC",
                cub_StateName = "British Columbia"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, canada.cub_CountryId.Value),
                cub_Abbreviation = "ON",
                cub_StateName = "Ontario"
            });
            #endregion Add Country Data

            Account acc = new Account();
            acc.AccountId = new Guid("5ee2976d-6ab6-e611-80e0-005056813f0c");
            //acc.CustomerTypeCode = (object)"Individual";
            acc.cub_AccountTypeId = new EntityReference();
            acc.cub_AccountTypeId.Id = Guid.NewGuid();
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            acc.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = acc.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Individual";

            cub_ContactType cct = new cub_ContactType();
            cct.Id = Guid.NewGuid();
            cct.cub_Name = "CubName";

            cub_Address add = new cub_Address();
            add.cub_AddressId = Guid.NewGuid();
            add.cub_AccountAddressId = new EntityReference();
            add.cub_AccountAddressId.Id = (Guid)acc.AccountId;
            add.cub_Name = "street";
            add.cub_Street1 = "1308 South Wsahington Street";
            add.cub_Street2 = "Building 2, Cubicle - The Dude";
            add.cub_City = "Tullahoma";
            add.cub_StateProvinceId = new EntityReference { LogicalName = cub_StateOrProvince.EntityLogicalName, Id = (Guid)stateTN.Id };
            add.cub_PostalCodeId = new EntityReference();
            add.cub_PostalCodeId.Name = "37388";
            add.cub_CountryId = new EntityReference { LogicalName = cub_Country.EntityLogicalName, Id = (Guid)us.cub_CountryId };

            Contact con = new Contact();
            con.ContactId = new Guid("A11942CA-D287-4AA9-A600-59B9B6CD128B"); //Guid.NewGuid();
            con.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = acc.AccountId.Value };
            con.cub_ContactTypeId = new EntityReference { LogicalName = cub_ContactType.EntityLogicalName, Id = cct.Id };
            con.Salutation = "Mr.";
            con.FirstName = "Cubic";
            con.LastName = "Tester";
            con.MiddleName = "W";
            con.Telephone1 = "9314541408";
            con.Telephone2 = "9314541409";
            con.Telephone3 = "9314541410";
            con.cub_Phone1Type = new OptionSetValue((int)cub_phonetype.Work);
            con.cub_Phone2Type = new OptionSetValue((int)cub_phonetype.Mobile);
            con.cub_Phone3Type = new OptionSetValue((int)cub_phonetype.Home);
            con.cub_AddressId = new EntityReference { LogicalName = cub_Address.EntityLogicalName, Id = (Guid)add.cub_AddressId };
            con.Suffix = "Jr.";
            con.EMailAddress1 = "MrTester@cubic.com";
            con.BirthDate = DateTime.Now;
            con.cub_PersonalIdentifier = "Drivers License";
            con.cub_PersonalIdentifierType = new OptionSetValue((int)Contactcub_PersonalIdentifierType.DriversLicense);

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_Username = "the_user_name";
            cc.cub_Pin = "1234";
            cc.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };

            cub_SecurityQuestion csq = new cub_SecurityQuestion();
            csq.cub_SecurityQuestionId = Guid.NewGuid();
            csq.cub_SecurityQuestionDesc = "What is the answer to the ultimate question?";

            cub_SecurityAnswer csa = new cub_SecurityAnswer();
            csa.cub_SecurityAnswerId = Guid.NewGuid();
            csa.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };
            csa.cub_securityquestion_securityanswer = csq;
            csa.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq.cub_SecurityQuestionId };
            csa.cub_SecurityAnswerDesc = "42";

            cub_OptionSetTranslation translation = new cub_OptionSetTranslation
            {
                cub_OptionSetTranslationId = Guid.NewGuid(),
                cub_EntityName = null,
                cub_OptionSetName = "cub_phonetype"
            };
            data.Add(translation);
            EntityReference translationReference = new EntityReference(cub_OptionSetTranslation.EntityLogicalName,
                                                                       translation.cub_OptionSetTranslationId.Value);
            cub_OptionSetTranslationDetail detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "H",
                cub_OptionSetValue = 971880000
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "F",
                cub_OptionSetValue = 971880002
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "M",
                cub_OptionSetValue = 971880001
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "W",
                cub_OptionSetValue = 971880003
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "D",
                cub_OptionSetValue = 971880004
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "E",
                cub_OptionSetValue = 971880005
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "P",
                cub_OptionSetValue = 971880007
            };
            data.Add(detail);
            detail = new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = translationReference,
                cub_Translation = "O",
                cub_OptionSetValue = 971880006
            };
            data.Add(detail);

            data.Add(acc);
            data.Add(cat);
            data.Add(cct);
            data.Add(add);
            data.Add(con);
            data.Add(cc);
            data.Add(csq);
            data.Add(csa);

            return data;
        }

        public static List<Entity> GetCustData()
        {
            #region Load Fake Data
            List<Entity> data = new List<Entity>();

            #region Customer Data
            cub_AccountType accType = (new cub_AccountType
            {
                cub_AccountTypeId = Guid.NewGuid(),
                cub_AccountName = "Individual"
            });

            Account acc = (new Account
            {
                AccountId = Guid.NewGuid(),
                cub_AccountTypeId = new EntityReference(cub_AccountType.EntityLogicalName, accType.cub_AccountTypeId.Value),
            });

            #endregion Customer Data

            #region Address Data
            cub_Country cntry = (new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_Alpha2 = "US"
            });


            cub_StateOrProvince state = (new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_StateName = "IL"
            });

            cub_ZipOrPostalCode zip = (new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "60098"
            });

            cub_StateOrProvince state1 = (new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_StateName = "MI"
            });

            cub_StateOrProvince state2 = (new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_StateName = "CA"
            });

            cub_Address addy = (new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_AccountAddressId = new EntityReference(Account.EntityLogicalName, acc.AccountId.Value),
                cub_Street1 = "337 Vine Street",
                cub_Street2 = null,
                cub_City = "Woodstock",

                cub_CountryId = new EntityReference(cub_Country.EntityLogicalName, cntry.cub_CountryId.Value),
            });

            cub_Address addy2 = (new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_AccountAddressId = new EntityReference(Account.EntityLogicalName, Guid.NewGuid()),
                cub_Street1 = "337 Vine Street",
                cub_Street2 = null,
                cub_City = "Woodstock",

                cub_CountryId = new EntityReference(cub_Country.EntityLogicalName, cntry.cub_CountryId.Value),
            });

            cub_Address addy3 = (new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_AccountAddressId = new EntityReference(Account.EntityLogicalName, Guid.NewGuid()),
                cub_Street1 = "337 Vine Street",
                cub_Street2 = null,
                cub_City = "Woodstock",

                cub_CountryId = new EntityReference(cub_Country.EntityLogicalName, cntry.cub_CountryId.Value),
            });

            addy.cub_StateProvinceId = new EntityReference(cub_StateOrProvince.EntityLogicalName, state.cub_StateOrProvinceId.Value);
            addy.cub_StateProvinceId.Name = state.cub_StateName;
            addy.cub_PostalCodeId = new EntityReference(cub_ZipOrPostalCode.EntityLogicalName, zip.cub_ZipOrPostalCodeId.Value);
            addy.cub_PostalCodeId.Name = zip.cub_Code;

            data.Add(addy);
            data.Add(addy2);
            data.Add(addy3);
            data.Add(cntry);
            data.Add(zip);
            data.Add(state);
            #endregion Address Data

            #region Contact Data
            data.Add(new Contact
            {
                ContactId = Guid.NewGuid(),
                FirstName = "John",
                LastName = "Smith",
                EMailAddress1 = "John.Smith@msn.com",
                Telephone1 = "(508)415-8569",
                Telephone2 = "(798)512-8965",
                Telephone3 = "(785)986-5236"
            });

            data.Add(new Contact
            {
                ContactId = Guid.NewGuid(),
                FirstName = "Johnathan",
                LastName = "Trump",
                EMailAddress1 = "Johnathan.Trump@hotmail.com",
                Telephone1 = "(508)256-8520",
                Telephone2 = "(798)123-7458"
            });

            data.Add(new Contact
            {
                ContactId = Guid.NewGuid(),
                FirstName = "John",
                LastName = "Dowe",
                EMailAddress1 = "JDowe@hotmail.com",
                Telephone1 = "(125)856-8523",
                Telephone2 = "(508)853-8520",
                Telephone3 = "(798)123-7458"
            });

            data.Add(new Contact
            {
                ContactId = Guid.NewGuid(),
                FirstName = "Tony",
                LastName = "Riak",
                EMailAddress1 = "TRiak@hotmail.com",
                Telephone1 = "(815)856-8523",
                Telephone2 = "(708)807-8520",
                Telephone3 = "(733)123-7458"
            });
            #endregion Contact Data
            #endregion Load Fake Data

            return data;
        }

        private static List<Entity> PrepareCountryData()
        {
            List<Entity> data = new List<Entity>();
            cub_Country us = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "United States of America",
                cub_Alpha2 = "US",
                cub_Address2 = new OptionSetValue((int)cub_Countrycub_Address2.REQ),
                cub_Address3 = new OptionSetValue((int)cub_Countrycub_Address3.OPT),
                cub_City = new OptionSetValue((int)cub_Countrycub_City.REQ),
                cub_State = new OptionSetValue((int)cub_Countrycub_State.REQ),
                cub_PostalCode = new OptionSetValue((int)cub_Countrycub_PostalCode.REQ),
                cub_DialingCode = "1"
            };
            data.Add(us);
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "CA",
                cub_StateName = "California"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "FL",
                cub_StateName = "Florida"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "WA",
                cub_StateName = "Washington"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "CO",
                cub_StateName = "Colorado"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "TX",
                cub_StateName = "Texas"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "OR",
                cub_StateName = "Oregon"
            });
            cub_Country ca = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "Canada",
                cub_Alpha2 = "CA",
                cub_Address2 = new OptionSetValue((int)cub_Countrycub_Address2.REQ),
                cub_Address3 = new OptionSetValue((int)cub_Countrycub_Address3.OPT),
                cub_City = new OptionSetValue((int)cub_Countrycub_City.REQ),
                cub_State = new OptionSetValue((int)cub_Countrycub_State.REQ),
                cub_PostalCode = new OptionSetValue((int)cub_Countrycub_PostalCode.REQ),
                cub_DialingCode = "1"
            };
            data.Add(ca);
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, ca.cub_CountryId.Value),
                cub_Abbreviation = "BC",
                cub_StateName = "British Columbia"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, ca.cub_CountryId.Value),
                cub_Abbreviation = "ON",
                cub_StateName = "Ontario"
            });
            return data;
        }
    }
}
