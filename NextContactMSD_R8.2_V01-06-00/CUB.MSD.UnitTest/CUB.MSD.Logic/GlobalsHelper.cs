/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUB.MSD.UnitTest.CUB.MSD.Logic
{
    public class GlobalsHelper
    {
        public static void GetGlobalsData(List<Entity> data)
        {
            EntityReference appInfoGlobalEntityRef = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());
            EntityReference PasswordValidationGlobalEntityRef = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());

            #region AppInfo
            data.Add(new cub_Globals()
            {
                Id = appInfoGlobalEntityRef.Id,
                cub_Enabled = true,
                cub_Name = "AppInfo"
            });

            data.Add(new cub_GlobalDetails()
            {
                Id = Guid.NewGuid(),
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, appInfoGlobalEntityRef.Id),
                cub_Name = "MaxFailedLoginAttempts",
                cub_AttributeValue = "5",
                cub_Enabled = true
            });
            #endregion

            #region PasswordValidation
            data.Add(new cub_Globals()
            {
                Id = PasswordValidationGlobalEntityRef.Id,
                cub_Enabled = true,
                cub_Name = "PasswordValidation"
            });
            data.Add(new cub_GlobalDetails()
            {
                Id = Guid.NewGuid(),
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, PasswordValidationGlobalEntityRef.Id),
                cub_Name = "PasswordMinLength",
                cub_AttributeValue = "4",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails()
            {
                Id = Guid.NewGuid(),
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, PasswordValidationGlobalEntityRef.Id),
                cub_Name = "PasswordMaxLength",
                cub_AttributeValue = "64",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails()
            {
                Id = Guid.NewGuid(),
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, PasswordValidationGlobalEntityRef.Id),
                cub_Name = "PasswordNumeric",
                cub_AttributeValue = "0",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails()
            {
                Id = Guid.NewGuid(),
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, PasswordValidationGlobalEntityRef.Id),
                cub_Name = "PasswordLowerAlpha",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails()
            {
                Id = Guid.NewGuid(),
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, PasswordValidationGlobalEntityRef.Id),
                cub_Name = "PasswordUpperAlpha",
                cub_AttributeValue = "0",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails()
            {
                Id = Guid.NewGuid(),
                cub_GlobalId = new EntityReference(cub_Globals.EntityLogicalName, PasswordValidationGlobalEntityRef.Id),
                cub_Name = "PasswordNonAlpha",
                cub_AttributeValue = "0",
                cub_Enabled = true
            });

            #endregion

            NisAPIGlobalsHelper.GetNisApiGlobals(data);


        }
    }
}
