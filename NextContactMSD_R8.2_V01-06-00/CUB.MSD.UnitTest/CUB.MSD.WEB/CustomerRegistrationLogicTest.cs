/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Web.Models;
using CUB.MSD.Model;
using CUB.MSD.Logic;
using System.Linq;

namespace CUB.MSD.UnitTest.CUB.MSD.WEB
{
    [TestClass]
    public class CustomerRegistrationLogicTest
    {
        [TestMethod]
        public void RegistrationTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            List<Entity> data = new List<Entity>();
            CustomerRegistrationModel model = PrepareModelAndData(data);
            fakedContext.Initialize(data);
            var fakedService = fakedContext.GetOrganizationService();
            CustomerRegistrationLogic logic = new CustomerRegistrationLogic(fakedService);
            logic.RegisterCustomer(model);

            /*
             * Account
             */
            var accounts = fakedContext.Data[Account.EntityLogicalName].Values.GetEnumerator();
            accounts.MoveNext();
            var account = (Account)accounts.Current;
            Assert.AreEqual(account.cub_AccountTypeId.Id, model.AccountType);

            /*
             * Address
             */
             var addresses = fakedContext.Data[cub_Address.EntityLogicalName].Values.GetEnumerator();
            addresses.MoveNext();
            var address = (cub_Address)addresses.Current;
            Assert.AreEqual(address.cub_Name, model.Address1 + " " + model.Address2);
            Assert.AreEqual(address.cub_Street1, model.Address1);
            Assert.AreEqual(address.cub_Street2, model.Address2);
            Assert.AreEqual(address.cub_City, model.City);
            Assert.AreEqual(address.cub_StateProvinceId.Id, model.State);
            Assert.AreEqual(address.cub_PostalCodeId.Id, model.ZipPostalCodeId);
            Assert.AreEqual(address.cub_CountryId.Id, model.Country);
            /*
             * Contact
             */
            var contacts =  fakedContext.Data[Contact.EntityLogicalName].Values.GetEnumerator();
            contacts.MoveNext();
            var contact = (Contact) contacts.Current;
            Assert.IsNotNull(contact.ParentCustomerId);
            Assert.IsNotNull(contact.cub_AddressId);
            Assert.AreEqual(contact.cub_ContactTypeId.Id, model.ContactType);
            Assert.AreEqual(contact.FirstName, model.FirstName);
            Assert.AreEqual(contact.LastName, model.LastName);
            Assert.AreEqual(contact.EMailAddress1, model.Email);
            Assert.AreEqual(((OptionSetValue)contact.cub_Phone1Type).Value, model.Phone1Type);
            Assert.AreEqual(contact.Telephone1, model.Phone1);
            Assert.AreEqual(((OptionSetValue)contact.cub_Phone2Type).Value, model.Phone2Type);
            Assert.AreEqual(contact.Telephone2, model.Phone2);
            Assert.AreEqual(((OptionSetValue)contact.cub_Phone3Type).Value, model.Phone3Type);
            Assert.AreEqual(contact.Telephone3, model.Phone3);

            /*
             * Security Question Answer
             */
            var sqas = fakedContext.Data[cub_SecurityAnswer.EntityLogicalName].Values.GetEnumerator();
            sqas.MoveNext();
            var sqa = (cub_SecurityAnswer) sqas.Current;
            Assert.AreEqual(sqa.cub_ContactId.Id, contact.ContactId);
            Assert.IsNotNull(sqa.cub_SecurityQuestionId);
            Assert.AreEqual(sqa.cub_SecurityAnswerDesc, model.SecurityQAs.Count > 0 ? model.SecurityQAs.First().Value : String.Empty);

            /*
             * Credentials
             */
            var creds = fakedContext.Data[cub_Credential.EntityLogicalName].Values.GetEnumerator();
            creds.MoveNext();
            var cred = (cub_Credential) creds.Current;
            Assert.AreEqual(cred.cub_Pin, model.PIN);
            Assert.AreEqual(cred.cub_Username, model.UserName);

        }

        private static CustomerRegistrationModel PrepareModelAndData(List<Entity> data)
        {
            CustomerRegistrationModel model = new CustomerRegistrationModel();
            model.AccountType = Guid.NewGuid();
            data.Add(new cub_AccountType { cub_AccountTypeId = model.AccountType, cub_AccountName = "Acount Type" });
            model.Address1 = "Adress 1";
            model.Address2 = "Address 2";
            model.City = "City";
            model.ContactType = Guid.NewGuid();
            data.Add(new cub_ContactType  { cub_ContactTypeId = model.ContactType,  cub_Name = "Contact Type" });
            model.Country = Guid.NewGuid();
            data.Add(new cub_Country { cub_CountryId = model.Country});
            model.Email = "Email";
            model.FirstName = "First Name";
            model.LastName = "Last Name";
            model.Phone1 = "Phone1";
            model.Phone1Type = (int)cub_phonetype.DayTime;
            model.Phone2 = "Phone2";
            model.Phone2Type = (int)cub_phonetype.Evening;
            model.Phone3 = "Phone3";
            model.Phone3Type = (int)cub_phonetype.Fax;
            model.PIN = "1234";
            model.SecurityQAs = new Dictionary<Guid, string>() {
                {
                    Guid.NewGuid(), "Answer"
                }
            };
            data.Add(new cub_SecurityQuestion { cub_SecurityQuestionId = model.SecurityQAs.First().Key });
            model.State = Guid.NewGuid();
            data.Add(new cub_StateOrProvince { cub_StateOrProvinceId = model.State });
            model.UserName = "UserName";
            model.Zip = "Zip";
            model.ZipPostalCodeId = Guid.NewGuid();
            data.Add(new cub_ZipOrPostalCode {
                cub_ZipOrPostalCodeId = model.ZipPostalCodeId,
                cub_City = model.City,
                cub_State = new EntityReference(cub_StateOrProvince.EntityLogicalName, model.State.Value),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, model.Country.Value),
                cub_Code = "99999",
                cub_CodeType = "Unique"
            });
            return model;
        }
    }
}
