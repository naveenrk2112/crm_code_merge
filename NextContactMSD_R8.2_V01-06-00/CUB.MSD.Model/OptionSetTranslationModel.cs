/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUB.MSD.Model
{
    /// <summary>
    /// This class represent the collection of all Option Set Translation definition
    /// A translation definition is formed by a combination of an Entity Name and OptionSet Name plus
    /// tha mapping of each external string to a MSD option set value
    /// </summary>
    public class OptionSetTranslationModel
    {
        private Dictionary<string, OptionSetTranslation> Translations;

        /// <summary>
        /// Constructor
        /// </summary>
        public OptionSetTranslationModel ()
        {
            Translations = new Dictionary<string, OptionSetTranslation>();
        }

        /// <summary>
        /// Add a new translation definition
        /// </summary>
        /// <param name="entityName">Entity Name. Null if it is a Global OptionSet</param>
        /// <param name="optionSetName">OptionSet Name</param>
        /// <returns>Reference to the new OptionSetTranslation</returns>
        public OptionSetTranslation Add(string entityName, string optionSetName)
        {
            OptionSetTranslation newEntry = new OptionSetTranslation(entityName, optionSetName);
            Translations.Add(newEntry.GetKey(), newEntry);
            return newEntry;
        }

        /// <summary>
        /// Get a OptionSet Translation definition
        /// </summary>
        /// <param name="entityName">Entity Name</param>
        /// <param name="optionSetName">OptionSet Name</param>
        /// <returns>OptionSet translation definition</returns>
        /// <exception cref="KeyNotFoundException">If the OptionSet Value is not found</exception>
        public OptionSetTranslation Get(string entityName, string optionSetName)
        {
            try
            {
                return Translations[OptionSetTranslation.GetKey(entityName, optionSetName)];
            } catch (KeyNotFoundException)
            {
                string msg = string.Format("Entity + OptionSet combination not found: {0}, {1}", entityName, optionSetName);
                throw new KeyNotFoundException(msg);
            }
        }
    }

    /// <summary>
    /// OptionSet translation definition
    /// </summary>
    public class OptionSetTranslation
    {
        /// <summary>
        /// Keyname Separator
        /// </summary>
        private const string KEY_MASK_SEPARATOR = "_._";

        public const string KEY_MASK = "{0}{1}{2}";
        /// <summary>
        /// Entity Name
        /// </summary>
        public string EntityName { set; get; }
        /// <summary>
        /// OptionSet Name
        /// </summary>
        public string OptionSetName { set; get; }

        /// <summary>
        /// Dictionary indexed by the Texts
        /// </summary>
        private Dictionary<string, OptionSetTranslationDetail> Texts;
        
        /// <summary>
        /// Dictionary indexed by the Values
        /// </summary>
        private Dictionary<int, OptionSetTranslationDetail> Values;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="entityName">Entity Name</param>
        /// <param name="optionSetName">OptionSet Name</param>
        public OptionSetTranslation(string entityName, string optionSetName)
        {
            EntityName = entityName;
            OptionSetName = optionSetName;
            Texts = new Dictionary<string, OptionSetTranslationDetail>();
            Values = new Dictionary<int, OptionSetTranslationDetail>();
        }

        /// <summary>
        /// Add a new translation mapping an external string to a OptionSet value
        /// </summary>
        /// <param name="text">External string</param>
        /// <param name="value">OptionSet value</param>
        public void Add(string text, int value)
        {
            OptionSetTranslationDetail newEntry = new OptionSetTranslationDetail(text, value);
            Texts.Add(newEntry.Text, newEntry);
            Values.Add(value, newEntry);
        }

        /// <summary>
        /// Get an OptionSet from a External String (Text)
        /// </summary>
        /// <param name="text">External String (Text)</param>
        /// <returns>OptionSet Value</returns>
        /// <exception cref="KeyNotFoundException">If the External String (Text) is not found</exception>
        public int GetValue(string text)
        {
            if (Texts.ContainsKey(text))
                return Texts[text].Value;
            else
                throw new KeyNotFoundException(string.Format("Text {0} not found", text));
        }

        /// <summary>
        /// Get the external string (Text) from an OptionSet value
        /// </summary>
        /// <param name="value">OptionSet value</param>
        /// <returns>External String (Text) associate to the OptionSet value</returns>
        /// <exception cref="KeyNotFoundException">If the OptionSet Value is not found</exception>
        public string GetText(int value)
        {
            if (Values.ContainsKey(value))
                return Values[value].Text;
            else
                throw new KeyNotFoundException(string.Format("Value {0} not found", value));
        }

        /// <summary>
        /// Get the key name format by the EntityName, separator and the OptionSetName
        /// </summary>
        /// <returns>Key Name</returns>
        public string GetKey()
        {
            return string.Format(KEY_MASK, EntityName, KEY_MASK_SEPARATOR, OptionSetName);
        }

        /// <summary>
        /// Get the key name format by the EntityName, separator and the OptionSetName
        /// </summary>
        /// <param name="entityName">Entity Name</param>
        /// <param name="optionSetName">OptionSet Name</param>
        /// <returns>Key Name</returns>
        public static string GetKey(string entityName, string optionSetName)
        {
            return string.Format(KEY_MASK, entityName, KEY_MASK_SEPARATOR, optionSetName);
        }
    }

    /// <summary>
    /// OptionSet Translation Detail
    /// Represent a mapping between a External String (Text) and a OptionSet Value
    /// </summary>
    public class OptionSetTranslationDetail
    {
        /// <summary>
        /// External String (Text)
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// OptionSet Value
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Constrtuctor
        /// </summary>
        /// <param name="text">External String (Text)</param>
        /// <param name="value">OptionSet Value</param>
        public OptionSetTranslationDetail(string text, int value)
        {
            Text = text;
            Value = value;
        }
    }
}
