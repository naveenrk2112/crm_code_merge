﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUB.MSD.Model.NIS;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;

namespace CUB.MSD.Model.MSDApi
{
    /// <summary>
    /// CubResponse
    /// </summary>
    /// <typeparam name="T">T</typeparam>
    public class CubResponse<T> where T : WSMSD, new()
    {
        /// <summary>
        /// Success
        /// </summary>
        public bool Success {
            get { return Errors.Count() == 0; }
        }

        /// <summary>
        /// Errors
        /// </summary>
        public List<WSMSDFault> Errors { get; set; } = new List<WSMSDFault>();

        /// <summary>
        /// ResponseObject
        /// </summary>
        public T ResponseObject { get; set; } = new T();

    }

    /// <summary>
    /// WSMSD 
    /// </summary>
    public abstract class WSMSD { }

    /// <summary>
    /// WSEmptyClass
    /// </summary>
    public class WSEmptyClass : WSMSD { }

    /// <summary>
    /// WSForgotUsernameResponse
    /// </summary>
    public class WSForgotUsernameResponse : WSMSD
    {
        /// <summary>
        /// contactId
        /// </summary>
        public string contactId { get; set; }

        /// <summary>
        /// customerId
        /// </summary>
        public string customerId { get; set; }

        /// <summary>
        /// credentialId
        /// </summary>
        public string credentialId { get; set; }

        /// <summary>
        /// postalCode
        /// </summary>
        public string postalCode { get; set; }

        /// <summary>
        /// firstName
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// lastName
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// phone
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// email
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// pin
        /// </summary>
        public string pin { get; set; }

        /// <summary>
        /// verificationtokenId
        /// </summary>
        public string verificationtokenId { get; set; }

        /// <summary>
        /// username
        /// </summary>
        public string username { get; set; }

        /// <summary>
        /// lockoutDtm
        /// </summary>
        public DateTime? lockoutDtm { get; set; }

        /// <summary>
        /// forceChangePwd
        /// </summary>
        public bool forceChangePwd { get; set; }

        /// <summary>
        /// cub_credential_statuscode
        /// </summary>
        public cub_credential_statuscode credStatus { get; set; }

        /// <summary>
        /// Encrypted existing password from the credential record
        /// </summary>
        public string existingPassword { get; set; }

        /// <summary>
        /// mobileDeviceToken
        /// </summary>
        public string mobileDeviceToken { get; set; }

        /// <summary>
        /// Account Status code
        /// </summary>
        public account_statuscode accountStatus { get; set; }
    }

    /// <summary>
    /// WSCustomerCredential
    /// </summary>
    public class WSCustomerCredential : WSMSD
    {
        /// <summary>
        /// contactId
        /// </summary>
        public string contactId { get; set; }

        /// <summary>
        /// customerId
        /// </summary>
        public string customerId { get; set; }

        /// <summary>
        /// credentialId
        /// </summary>
        public string credentialId { get; set; }

        /// <summary>
        /// postalCode
        /// </summary>
        public string postalCode { get; set; }

        /// <summary>
        /// firstName
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// lastName
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// phone
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// email
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// pin
        /// </summary>
        public string pin { get; set; }

        /// <summary>
        /// verificationtokenId
        /// </summary>
        public string verificationtokenId { get; set; }

        /// <summary>
        /// username
        /// </summary>
        public string username { get; set; }

        /// <summary>
        ///lockoutDtm
        /// </summary>
        public DateTime? lockoutDtm { get; set; }

        /// <summary>
        /// forceChangePwd
        /// </summary>
        public bool forceChangePwd { get; set; }

        /// <summary>
        /// credStatus
        /// </summary>
        public cub_credential_statuscode credStatus { get; set; }

        /// <summary>
        /// Encrypted existing password from the credential record
        /// </summary>
        public string existingPassword { get; set; }

    }

    /// <summary>
    /// WSOrganizationInfo
    /// </summary>
    public class WSOrganizationInfo : WSMSD
    {
        /// <summary>
        /// OrganizationId
        /// </summary>
        public Guid? OrganizationId { get; set; }

        /// <summary>
        /// OrganizationName
        /// </summary>
        public string OrganizationName { get; set; }


        /// <summary>
        /// Version
        /// </summary>
        public string Version { get; set; }
    }

    /// <summary>
    /// Customer Contact request for the call 2.5.8
    /// </summary>
    public class WSCustomerContactRequest
    {
        /// <summary>
        /// Contact
        /// </summary>
        public WSCustomerContact contact { set; get; }
    }

    /// <summary>
    /// WSCustomerContact
    /// </summary>
    public class WSCustomerContact : WSMSD
    {
        /// <summary>
        /// 
        /// </summary>
        private bool _passwordSet;
        private bool _contactTypeSet;
        private bool _nameSet;
        private bool _addressSet;
        private bool _addressIdSet;
        private bool _phoneSet;
        private bool _emailSet;
        private bool _dateOfBirthSet;
        private bool _userNameSet;
        private bool _personalIdentifierInfoSet;
        private bool _pinSet;
        private bool _securityQASet;

        /// <summary>
        /// 
        /// </summary>
        private string _password;
        private string _contactType;
        private WSName _name;
        private WSAddress _address;
        private string _addressId;
        private WSPhone[] _phone;
        private string _email;
        private string _dateOfBirth;
        private string _userName;
        private WSPersonalIdentifier _personalIdentifierInfo;
        private string _pin;
        private WSSecurityQA[] _securityQAs;

        /// <summary>
        /// 
        /// </summary>
        public string contactType {
            get { return this._contactType; }
            set
            {
                this._contactType = value;
                _contactTypeSet = true;
            }
        }

        public bool contactTypeSet
        {
            get { return _contactTypeSet; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid? contactTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public WSName name
        {
            get { return this._name; }
            set
            {

                this._name = value;
                _nameSet = true;
            }
        }

        public bool nameSet
        {
            get { return _nameSet; }

        }
        /// <summary>
        /// 
        /// </summary>
        public WSAddress address
        {
            get { return this._address; }
            set
            {

                this._address = value;
                _addressSet = true;
            }
        }

        public bool addressSet
        {
            get { return _addressSet; }

        }
        /// <summary>
        /// 
        /// </summary>
        public string addressId {
            get { return this._addressId; }
            set
            {

                this._addressId = value;
                _addressIdSet = true;
            }
        }

        public bool addressIdSet
        {
            get { return _addressIdSet; }

        }

        /// <summary>
        /// 
        /// </summary>
        public WSPhone[] phone {
            get { return this._phone; }
            set
            {

                this._phone = value;
                _phoneSet = true;
            }

        }

        public bool phoneSet
        {
            get { return _phoneSet; }

        }

        /// <summary>
        /// 
        /// </summary>
        public string email {
            get { return this._email; }
            set
            {

                this._email = value;
                _emailSet = true;
            }
        }

        public bool emailSet
        {
            get { return _emailSet; }

        }

        /// <summary>
        /// 
        /// </summary>
        public string dateOfBirth
        {
            get { return this._dateOfBirth; }
            set
            {

                this._dateOfBirth = value;
                _dateOfBirthSet = true;
            }

        }

        public bool dateofBirthSet
        {
            get { return _dateOfBirthSet; }

        }

        /// <summary>
        /// 
        /// </summary>
        public WSPersonalIdentifier personalIdentifierInfo {
            get { return this._personalIdentifierInfo; }
            set
            {

                this._personalIdentifierInfo = value;
                _personalIdentifierInfoSet = true;
            }
        }

        public bool personalIdentifierInfoSet
        {
            get { return _personalIdentifierInfoSet; }

        }

        /// <summary>
        /// 
        /// </summary>
        public string username {

            get { return this._userName; }
            set
            {

                this._userName = value;
                _userNameSet = true;
            }
        }

        public bool userNameSet
        {
            get { return _userNameSet; }

        }
        /// <summary>
        /// password
        /// </summary>
        public string password {
            get { return this._password; }
            set { this._password = value;
                _passwordSet = true;
            }
        }
        /// <summary>
        /// passwordSet
        /// </summary>
        public bool passwordSet
        {
            get { return _passwordSet; }
        }


        /// <summary>
        /// pin
        /// </summary>
        public string pin {
            get { return this._pin; }
            set
            {
                this._pin = value;
                _pinSet = true;
            }
        }

        public bool pinSet
        {
            get { return _pinSet; }
        }

        /// <summary>
        /// securityQAs
        /// </summary>
        public WSSecurityQA[] securityQAs {
            get { return this._securityQAs; }
            set
            {
                this._securityQAs = value;
                _securityQASet = true;
            }
        }

        public bool securityQASet
        {
            get { return _securityQASet; }
        }

        /// <summary>
        /// customerId
        /// </summary>
        public Guid? customerId { get; set; }

        /// <summary>
        /// contactId
        /// </summary>
        public Guid? contactId { get; set; }

    }

    /// <summary>
    /// WSCustomer
    /// </summary>
    public class WSCustomer : WSMSD
    {
        /// <summary>
        /// customerId
        /// </summary>
        public Guid? customerId { get; set; }

        /// <summary>
        /// customerType
        /// </summary>
        public string customerType { get; set; }

        /// <summary>
        /// customerTypeId
        /// </summary>
        public Guid? customerTypeId { get; set; }

        /// <summary>contact
        /// 
        /// </summary>
        public WSCustomerContact contact { get; set; }

    }


    /// <summary>
    /// WSName
    /// </summary>
    public class WSName : WSMSD
    {
        /// <summary>
        /// title
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// firstName
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// middleInitial
        /// </summary>
        public string middleInitial { get; set; }

        /// <summary>
        /// lastName
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// nameSuffix
        /// </summary>
        public string nameSuffix { get; set; }
    }

    /// <summary>
    /// WSAddress
    /// </summary>
    public class WSAddress : WSMSD
    {
        /// <summary>
        /// empty constructor
        /// </summary>
        public WSAddress() { } 

        /// <summary>
        /// constructor that moves data from a WSAddressRootObject to this object
        /// </summary>
        /// <param name="rootAddress"></param>
        public WSAddress(CUB.MSD.Model.NIS.WSUpdateAddress rootAddress)
        {
            if (rootAddress != null)
            {
                address1 = rootAddress.address.address1;
                address2 = rootAddress.address.address2;
                address3 = rootAddress.address.address3;
                city = rootAddress.address.city;
                country = rootAddress.address.country;
                postalCode = rootAddress.address.postalCode;
                state = rootAddress.address.state;
            }
        }

        /// <summary>
        /// address1
        /// </summary>
        public string address1 { get; set; }

        /// <summary>
        /// address2
        /// </summary>
        public string address2 { get; set; }

        /// <summary>
        /// address3
        /// </summary>
        public string address3 { get; set; }

        /// <summary>
        /// city
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// state
        /// </summary>
        public string state { get; set; }

        /// <summary>
        /// stateId
        /// </summary>
        public Guid? stateId { get; set; }

        /// <summary>
        /// country
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// countryId
        /// </summary>
        public Guid? countryId { get; set; }

        /// <summary>
        /// postalCode
        /// </summary>
        public string postalCode { get; set; }

        /// <summary>
        /// postalCodeId
        /// </summary>
        public Guid? postalCodeId { get; set; }

        /// <summary>
        /// addressId
        /// </summary>
        public Guid? addressId { get; set; }

        /// <summary>
        /// isExistingAddress
        /// </summary>
        public bool isExistingAddress { get; set; }
    }

    /// <summary>
    /// WSMobileToken
    /// </summary>
    public class WSMobileToken : WSMSD
    {
        /// <summary>
        /// mobileToken
        /// </summary>
        public string mobileToken { get; set; }
    }

    /// <summary>
    /// WSPhone
    /// </summary>
    public class WSPhone : WSMSD
    {
        /// <summary>
        /// number
        /// </summary>
        public string number { get; set; }

        /// <summary>
        /// type
        /// </summary>
        public string type { get; set; }
    }

    /// <summary>
    /// WSPersonalIdentifier
    /// </summary>
    public class WSPersonalIdentifier : WSMSD
    {
        /// <summary>
        /// personalIdentifier
        /// </summary>
        public string personalIdentifier { get; set; }

        /// <summary>
        /// personalIdentifierType
        /// </summary>
        public string personalIdentifierType { get; set; }

        /// <summary>
        /// personalIdentifierTypeId
        /// </summary>
        public int? personalIdentifierTypeId { get; set; }
    }

    /// <summary>
    /// WSSecurityQA
    /// </summary>
    public class WSSecurityQA : WSMSD
    {
        /// <summary>
        /// securityQuestion
        /// </summary>
        public string securityQuestion { get; set; }

        /// <summary>
        /// securityAnswer
        /// </summary>
        public string securityAnswer { get; set; }

        /// <summary>
        /// securityQuestionId
        /// </summary>
        public Guid? securityQuestionId { get; set; }

        /// <summary>
        /// securityAnswerId
        /// </summary>
        public Guid? securityAnswerId { get; set; }

        /// <summary>
        /// displayOrder
        /// </summary>
        public int displayOrder { get; set; }
    }

    /// <summary>
    /// WSError
    /// </summary>
    public class WSError
    {
        /// <summary>
        /// key
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// message
        /// </summary>
        public string message { get; set; }
    }

    /// <summary>
    /// WSMSDFault
    /// </summary>
    public class WSMSDFault
    {
        /// <summary>
        /// fieldName
        /// </summary>
        public string fieldName { get; set; }

        /// <summary>
        /// errorKey
        /// </summary>
        public string errorKey { get; set; }

        /// <summary>
        /// errorMessage
        /// </summary>
        public string errorMessage { get; set; }

        /// <summary>
        /// Constructor with defined parameters
        /// </summary>
        /// <param name="fieldName">fieldName</param>
        /// <param name="errorKey">errorKey</param>
        /// <param name="errorMessage">errorMessage</param>
        public WSMSDFault(string field_Name, string error_Key, string error_Message)
        {
            fieldName = field_Name;
            errorKey = error_Key;
            errorMessage = error_Message;
        }
    }

    /// <summary>
    /// WSGenerateTokenResponse
    /// </summary>
    public class WSGenerateTokenResponse : WSMSD
    {
        /// <summary>
        /// token
        /// </summary>
        public string token { get; set; }

        /// <summary>
        /// tokenExpiryTime
        /// </summary>
        public DateTime? tokenExpiryTime { get; set; }
    }

    /// <summary>
    /// WSAddressCreationResponse
    /// </summary>
    public class WSAddressCreationResponse : WSMSD
    {
        /// <summary>
        /// addressId
        /// </summary>
        public string addressId { get; set; }

        /// <summary>
        /// isExistingAddress
        /// </summary>
        public bool isExistingAddress { get; set; }
    }

    /// <summary>
    /// WSCredentailsPrevalidateResults
    /// </summary>
    public class WSCredentailsPrevalidateResults : WSMSD
    {
        /// <summary>
        /// isUsernameAvailable
        /// </summary>
        public bool isUsernameAvailable { get; set; }

        /// <summary>
        /// isUsernameValid
        /// </summary>
        public bool isUsernameValid { get { return usernameErrors.Count() == 0; } }

        /// <summary>
        /// usernameErrors
        /// </summary>
        public List<WSError> usernameErrors { get; set; }

        /// <summary>
        /// isPasswordValid
        /// </summary>
        public bool isPasswordValid { get { return passwordErrors.Count() == 0; } }

        /// <summary>
        /// passwordErrors
        /// </summary>
        public List<WSError> passwordErrors { get; set; }
    }

    /// <summary>
    /// WSUsernamePrevalidateResults
    /// </summary>
    public class WSUsernamePrevalidateResults : WSMSD
    {
        /// <summary>
        /// isUsernameAvailable
        /// </summary>
        public bool isUsernameAvailable { get; set; }

        /// <summary>
        /// isUsernameValid
        /// </summary>
        public bool isUsernameValid { get { return usernameErrors.Count() == 0; } }

        /// <summary>
        /// usernameErrors
        /// </summary>
        public List<WSError> usernameErrors { get; set; }
    }

    /// <summary>
    /// WSPasswordPrevalidateResults
    /// </summary>
    public class WSPasswordPrevalidateResults : WSMSD
    {
        /// <summary>
        /// isPasswordValid
        /// </summary>
        public bool isPasswordValid { get { return passwordErrors.Count() == 0; } }

        /// <summary>
        /// passwordErrors
        /// </summary>
        public List<WSError> passwordErrors { get; set; }
    }

    /// <summary>
    /// Response from MSD Address Delete combining NIS response 
    /// </summary>
    public class WSAddressDeleteResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// fundingSourceDependencies
        /// </summary>
        public List<WSFundingSourceExt> fundingSourceDependencies { get; set; } = new List<WSFundingSourceExt>();

        /// <summary>
        /// contactDependencies
        /// </summary>
        public List<WSCustomerContactInfo> contactDependencies { get; set; } = new List<NIS.WSCustomerContactInfo>();

    }

    /// <summary>
    /// Contains list of dependencies from NIS 
    /// </summary>
    public class WSAddressDependenciesResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// fundingSourceDependencies
        /// </summary>
        public List<WSFundingSourceExt> fundingSourceDependencies { get; set; } = new List<WSFundingSourceExt>();

        /// <summary>
        /// contactDependencies
        /// </summary>
        public List<WSCustomerContactInfo> contactDependencies { get; set; } = new List<NIS.WSCustomerContactInfo>();

    }


    /// <summary>
    /// 2.8.13 - As of Aug 8, 2017, NIS update address call returns nothing in the body. Only the usual header is return 
    /// </summary>
    public class WSAddressUpdateResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();
    }

    /// <summary>
    /// 2.8.13 - As of Aug 8, 2017, NIS update address call returns nothing in the body. Only the usual header is return 
    /// </summary>
    public class WSAddressUpdateRequest
    {
        /// <summary>
        /// New Address
        /// </summary>
        public WSAddress address { get; set; } = new WSAddress();

        /// <summary>
        /// contactDependencies
        /// </summary>
        public List<string> contactsUpdated { get; set; } = new List<string>();

        /// <summary>
        /// fundingSourceDependencies
        /// </summary>
        public List<string> fundingSourcesUpdated { get; set; } = new List<string>();

    }

    /// <summary>
    /// Used in address delete response and GET funding source response from NIS. 
    /// </summary>
    public class WSFundingSourceExt : WSFundingSource
    {
        /// <summary>
        /// fundingSourceId
        /// </summary>
        public int fundingSourceId { get; set; }

        /// <summary>
        /// status
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// statusDescription
        /// </summary>
        public string statusDescription { get; set; }

        /// <summary>
        /// (Required) Last status date
        /// </summary>
        public DateTime statusDateTime { get; set; }

        /// <summary>
        /// (Conditionally-Required) Number of soft declines on the funding 
        /// source which impacts greylisting of it eventually.
        /// </summary>
        public int? declineCount { get; set; }
    }

    /// <summary>
    /// Internal extension class for NIS API's WSFundingSourceExt that includes billingAddress string value
    /// </summary>
    public class WSMSDFundingSource : WSFundingSourceExt
    {
        public string billingAddress { get; set; }
    }

    /// <summary>
    /// Response from CRM API OA Summary combining NIS header/response 
    /// </summary>
    public class WSOneAccountSummaryResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// oneAccountId
        /// </summary>
        public int oneAccountId { get; set; }

        /// <summary>
        /// accountStatus
        /// </summary>
        public WSKeyValue accountStatus { get; set; } = new WSKeyValue();

        /// <summary>
        /// purse
        /// </summary>
        public List<WSPurseExt> purse { get; set; } = new List<WSPurseExt>();
        /// <summary>
        /// linkedAccounts
        /// </summary>
        public List<WSSubsystemAccountInfo> linkedAccounts { get; set; } = new List<WSSubsystemAccountInfo>();
    }

    /// <summary>
    /// Returned from CRM API Get Config Notification Channel
    /// </summary>
    public class WSNotificationChannelInfo : WSMSD
    {
        /// <summary>
        /// channel
        /// </summary>
        public string channel { get; set; }

        /// <summary>
        /// enabled
        /// </summary>
        public bool enabled { get; set; }
    }
    /// <summary>
    /// Response from CRM API Get Config Notification Channel combining NIS header/response 
    /// </summary>
    public class WSGetConfigNotificationChannelResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// channels
        /// </summary>
        public List<WSNotificationChannelInfo> channels { get; set; } = new List<WSNotificationChannelInfo>();
    }

    /// <summary>
    /// Response from CRM API Get Customer Notification combining NIS header/response 
    /// </summary>
    public class WSGetCustomerNotificationResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// notifications
        /// </summary>
        public List<WSCustomerNotificationInfo> notifications { get; set; } = new List<WSCustomerNotificationInfo>();

        /// <summary>
        /// totalCount
        /// </summary>
        public int totalCount { get; set; } = 0;
    }

    /// <summary>
    /// Response from CRM API Get Customer Notification combining NIS header/response 
    /// </summary>
    public class WSGetCustomerNotificationDetailResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// contact
        /// </summary>
        public WSCustomerContactInfo contact { get; set; } = new WSCustomerContactInfo();

        /// <summary>
        /// mobileDevice
        /// </summary>
        public WSMobileDevice mobileDevice { get; set; } = new WSMobileDevice();

        /// <summary>
        /// message
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// alternateMessage
        /// </summary>
        public string alternateMessage { get; set; }

        /// <summary>
        /// failReason
        /// </summary>
        public string failReason { get; set; }

        /// <summary>
        /// contactId
        /// </summary>
        public string contactId { get; set; }

        /// <summary>
        /// firstName
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// lastName
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// email
        /// </summary>
        public string email { get; set; }

        /// <summary>smsPhone
        /// 
        /// </summary>
        public string smsPhone { get; set; }

        /// <summary>
        /// mandatory
        /// </summary>
        public bool mandatory { get; set; }

        /// <summary>
        /// mobileAppId
        /// </summary>
        public string mobileAppId { get; set; }

        /// <summary>
        /// mobileAppNickname
        /// </summary>
        public string mobileAppNickname { get; set; }

        /// <summary>
        /// pnToken
        /// </summary>
        public string pnToken { get; set; }

        /// <summary>
        /// osType
        /// </summary>
        public string osType { get; set; }

        /// <summary>
        /// osVersion
        /// </summary>
        public string osVersion { get; set; }
    }

    /// <summary>
    /// WSContact
    /// </summary>
    public class WSContact : WSMSD
    {

        /// <summary>
        /// contactId
        /// </summary>
        public Guid contactId { get; set; }

        /// <summary>
        /// firstName
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// lastName
        /// </summary>
        public string lastName { get; set; }
    }

    /// <summary>
    /// Response from CRM API Get Customer Contacts 
    /// </summary>
    public class WSGetCustomerContactsResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// contacts
        /// </summary>
        public List<WSContact> contacts { get; set; } = new List<WSContact>();
    }

    /// <summary>
    /// Customer Order History
    /// </summary>
    public class WSGetCustomerOrderHistoryResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// Orders
        /// </summary>
        public List<WSOrderHistoryLineItem> orders { get; set; } = new List<WSOrderHistoryLineItem>();

        /// <summary>
        /// totalCount
        /// </summary>
        public int totalCount { get; set; } = 0;
    }

    public class WSOrderHistoryLineItem
    {
        /// <summary>
        /// (Required) Unique id for the order
        /// </summary>
        public int orderId { set; get; }

        /// <summary>
        /// (Required) The date and time when the order was created
        /// </summary>
        public DateTime orderDateTime { set; get; }

        /// <summary>
        /// (Optional) The date and time when the order was last changed
        /// </summary>
        public DateTime? orderLastUpdate { get; set; }

        /// <summary>
        /// (Required) The type of order.
        /// Valid values are:
        /// - Sale
        /// - Adjustment
        /// - AutoloadEnrollment
        /// - CloseAccount
        /// - Autoload
        /// </summary>
        public string orderType { get; set; }

        /// <summary>
        /// (Required) The description of the order type.
        /// </summary>
        public string orderTypeDescription { get; set; }

        /// <summary>
        /// (Required) Current status of the order.
        /// Valid values are:
        /// - OrderAccepted
        /// - PaymentFailed
        /// - FulfillmentFailed
        /// - Completed
        /// - CompletedWithErrors
        /// - PaymentAccpeted
        /// - PreAccpeted
        /// </summary>
        public string orderStatus { get; set; }

        /// <summary>
        /// (Required) The description of the order status.
        /// </summary>
        public string orderStatusDescription { get; set; }

        /// <summary>
        /// (Required) Total order amount, including item prices, taxes, and fees.
        /// </summary>
        public int orderTotalAmount { get; set; }

        /// <summary>
        /// string(20)
        /// (Optional) The current status of the payment.
        /// - Pending
        /// - Authorized
        /// - FailedAuthorization
        /// - Confirmed
        /// - FailedConfirmation
        /// - PendingConfirmation
        /// - ConfirmationExpired
        /// </summary>
        public string paymentStatus { get; set; }

        /// <summary>
        /// (Required) Reference number for the payment.
        /// </summary>
        public string paymentStatusDescription { get; set; }
    }

    /// <summary>
    /// Customer Order History Detail
    /// </summary>
    public class WSGetCustomerOrderHistoryDetailResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// Payment Details
        /// </summary>
        public List<WSPaymentDetailsLineItem> paymentDetails { get; set; } = new List<WSPaymentDetailsLineItem>();

        /// <summary>
        /// (Required) The total amount paid for the order in cents.
        /// </summary>
        public int totalAmount { get; set; }

        public int totalCount { get; set; } = 0;
    }

    public class WSPaymentDetailsLineItem
    {
        /// <summary>
        /// (Required) Unique id for the payment details
        /// </summary>
        public int paymentDetailsId { get; set; }

        /// <summary>
        /// (Required) The amount of the payment in cents
        /// </summary>
        public int paymentAmount { get; set; }

        /// <summary>
        /// (Optional) The payment type.
        /// Valid values are:
        /// - NoPayment
        /// - Cash
        /// - CreditCard
        /// - Check
        /// - StoredValue
        /// - DebitCard
        /// - BenefitValue
        /// - DirectDebit
        /// - CreditCardNotPresent
        /// </summary>
        public string paymentType { get; set; }

        /// <summary>
        /// (Required) Descritpion of the payment type
        /// </summary>
        public string paymentTypeDescription { get; set; }

        /// <summary>
        /// (Optional) The type of card used
        /// </summary>
        public string cardType { get; set; }

        /// <summary>
        /// (Optional) Masked PAN for the card used
        /// </summary>
        public string maskedPan { get; set; }

        /// <summary>
        /// (Optional) ID of the transit account
        /// </summary>
        public string transitAccountId { get; set; }

        /// <summary>
        /// (Optional) ID of the one account
        /// </summary>
        public string oneAccountId { get; set; }

        /// <summary>
        /// (Required) Reference number for payment
        /// </summary>
        public string retrievalReferenceNumber { get; set; }
    }

    /// <summary>
    /// Get Customer Order Response
    /// </summary>
    public class WSGetCustomerOrderResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// Order
        /// </summary>
        public WSOrder order { get; set; } = new WSOrder();
    }

    /// <summary>
    /// Customer Order
    /// </summary>
    public class WSOrder
    {
        /// <summary>
        /// (Required) The unique identifier for the order.
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// (Required) The type of order.
        /// Valid values are:
        /// - Sale
        /// - Adjustment
        /// - AutoloadEnrollment
        /// - CloseAccount
        /// - Autoload
        /// </summary>
        public string orderType { get; set; }

        /// <summary>
        /// (Required) The description of the order type.
        /// </summary>
        public string orderTypeDescription { get; set; }

        /// <summary>
        /// (Required) Current status of the order.
        /// Valid values are:
        /// - OrderAccepted
        /// - PaymentFailed
        /// - FulfillmentFailed
        /// - Completed
        /// - CompletedWithErrors
        /// - PaymentAccepted
        /// - PreAccepted
        /// - Waiting
        /// - Expired
        /// - Failed
        /// - PaymentRequested
        /// - RefundIssued
        /// - WaitingComplete
        /// - ReadyToClose
        /// - ManualReview
        /// - QueuedForAging
        /// - RefundPending
        /// </summary>
        public string orderStatus { get; set; }

        /// <summary>
        /// (Required) The description of the order status.
        /// </summary>
        public string orderStatusDescription { get; set; }

        /// <summary>
        /// (Required)  Unique identifier for the customer.
        /// </summary>
        public string customerId { get; set; }

        /// <summary>
        /// (Required) Sum of the total amounts from all line items. This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int itemsSubtotalAmount { get; set; }

        /// <summary>
        /// (Optional) Total Sales Tax amount. This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int salesTaxAmount { get; set; }

        /// <summary>
        /// (Required) Total order amount, including item prices, taxes, and fees.
        /// Sum of itemsSubtotalAmount + salesTaxAmount.
        ///This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int orderTotalAmount { get; set; }

        /// <summary>
        /// (Optional) Total fee amount.
        /// This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int feeTotalAmount { get; set; }

        /// <summary>
        /// (Optional) The sum of all the deposit line items. 
        /// This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int depositTotalAmount { get; set; }

        /// <summary>
        /// If the customer is not registered (i.e. customerId is not provided), the email address to send order notifications.
        // If customerId is specified, this field will be ignored.
        /// </summary>
        public string unregisteredEmail { get; set; }

        /// <summary>
        /// (Required) The date the order was created.
        /// </summary>
        public DateTime insertedDtm { get; set; }

        /// <summary>
        /// (Required) The date the order was last updated.
        /// </summary>
        public DateTime updatedDtm { get; set; }

        /// <summary>
        /// (Optional)  The date the status of the order last changed.
        /// </summary>
        public DateTime? lastStatusDtm { get; set; }

        public List<WSOrderLineItem> lineItems { get; set; }

    }

    /// <summary>
    /// Order Line Item
    /// </summary>
    public class WSOrderLineItem {
        /// <summary>
        /// (Required) The unique identifier for the line item.
        /// </summary>
        public int lineItemId { get; set; }

        /// <summary>
        /// (Required) The type of line item.  Valid values are:
        /// LoadProduct
        /// IssueMedia
        /// ThresholdAutoload
        /// OrderLevelFee
        /// Adjustment
        /// Ticket
        /// AutoloadEnrollment
        /// Autoload
        /// CloseAccount
        /// </summary>
        public string lineItemType { get; set; }

        /// <summary>
        /// (Required) The description of the line item type.
        /// </summary>
        public string lineItemTypeDescription { get; set; }

        /// <summary>
        /// (Required) Current status of the line item.
        /// Valid values are:
        /// - New
        /// - Fulfilled
        /// - Failed
        /// </summary>
        public string lineItemStatus { get; set; }

        /// <summary>
        /// (Required) The description of the line item status.
        /// </summary>
        public string lineItemStatusDescription { get; set; }


        /// <summary>
        /// (Optional) Reason code for the line item.
        /// </summary>
        public string reasonCode { get; set; }

        /// <summary>
        /// (Optional) Notes for the line item.
        /// </summary>
        public string notes { get; set; }

        /// <summary>
        /// (Optional) The unique identifier of the One Account that the line item is related to.
        /// </summary>
        public int oneAccountId { get; set; }

        /// <summary>
        /// (Optional) Unique identifier of the subsystem that the line item is related to.
        /// </summary>
        public string subsystem { get; set; }

        /// <summary>
        /// (Optional) Unique identifier for the account in the subsystem the line item is related to.
        /// </summary>
        public string subsystemAccountReference { get; set; }

        /// <summary>
        /// (Required) Number of units for this line item.
        /// </summary>
        public int quantity { get; set; }

        /// <summary>
        /// (Optional) Fee amount for the line item if applicable. This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int feeAmount { get; set; }

        /// <summary>
        /// (Optional) Deposit amount for the line item if applicable. This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int depositAmount { get; set; }

        /// <summary>
        /// (Required) Sum of the total amounts for the line item. This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int itemTotalAmount { get; set; }

        /// <summary>
        /// (Optional) Total Sales Tax amount. This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int salesTaxAmount { get; set; }

        /// <summary>
        /// (Required) The date the line item was last updated.
        /// </summary>
        public DateTime updatedDtm { get; set; }

        /// <summary>
        /// (Optional) External reference sent for the fulfillment request
        /// </summary>
        public string fulfillResponseRefId { get; set; }

        public DateTime? fulfillDtm { get; set; }

        /// <summary>
        /// (Optional)The reason for failure, typically this will be an error key
        /// </summary>
        public string fulfillErrorReason { get; set; }

        /// <summary>
        /// (Optional)A message describing the error.
        /// </summary>
        public string fulfillErrorMessage { get; set; }

        /// <summary>
        /// (Optional)The stacktrace, if any, associated with the fullfillment error.
        /// </summary>
        public string fulfillErrorStackTrace { get; set; }
    }


    /// <summary>
    /// Tolling Get Transponders Response
    /// </summary>
    public class WSTollingGetTranspondersResponse
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// Transponders
        /// </summary>
        public List<WSTollingTransponders> transponders { get; set; } = new List<WSTollingTransponders>();

        /// <summary>
        /// Count
        /// </summary>
        public int totalCount { get; set; }
    }
    
    /// <summary>
    /// Tolling Vehicles
    /// </summary>
    public class WSTollingTransponders
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// Tag Id
        /// </summary>
        public string tagId { get; set; }

        /// <summary>
        /// Serial #
        /// </summary>
        public string sno { get; set; }

        /// <summary>
        /// License Plate
        /// </summary>
        public string licPlate { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public string customerId { get; set; }


        /// <summary>
        /// Created Date
        /// </summary>
        public string createdDate { get; set; }

        /// <summary>
        /// UID Created
        /// </summary>
        public int uidCreated { get; set; }

        /// <summary>
        /// UID Updated
        /// </summary>
        public int uidUpdated { get; set; }

        /// <summary>
        /// Is Active
        /// </summary>
        public string isActive { get; set; }
    }

    /// <summary>
    /// Tolling Get Vehicles Response
    /// </summary>
    public class WSTollingGetVehiclesResponse
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// Vehicles
        /// </summary>
        public List<WSTollingVehicles> vehicles { get; set; } = new List<WSTollingVehicles>();

        /// <summary>
        /// Count
        /// </summary>
        public int totalCount { get; set; }
    }

    /// <summary>
    /// Tolling Vehicles
    /// </summary>
    public class WSTollingVehicles
    {
        /// <summary>
        /// Color
        /// </summary>
        public string color { get; set; }

        /// <summary>
        /// Year
        /// </summary>
        public int year { get; set; }

        /// <summary>
        /// UID Created
        /// </summary>
        public int uidCreated { get; set; }

        /// <summary>
        /// Origin
        /// </summary>
        public string origin { get; set; }

        /// <summary>
        /// Axles
        /// </summary>
        public string axles { get; set; }

        /// <summary>
        /// Plate #
        /// </summary>
        public string plateNum { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// Is Active
        /// </summary>
        public string isActive { get; set; }

        /// <summary>
        /// TS Update
        /// </summary>
        public string tsUpdated { get; set; }

        /// <summary>
        /// Customer ID
        /// </summary>
        public string customerId { get; set; }

        /// <summary>
        /// TS Created
        /// </summary>
        public string tsCreated { get; set; }

        /// <summary>
        /// Model
        /// </summary>
        public string model { get; set; }


        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// State
        /// </summary>
        public string state { get; set; }

        /// <summary>
        /// License Plate #
        /// </summary>
        public string licensePlateNo { get; set; }

        /// <summary>
        /// UID Updated
        /// </summary>
        public int uidUpdated { get; set; }

        /// <summary>
        /// Make
        /// </summary>
        public string make { get; set; }
    }

    /// <summary>
    /// Tolling Get Transaction History Response
    /// </summary>
    public class WSTollingGetTransactionHistoryResponse
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// Transaction History
        /// </summary>
        public List<WSTollingTransactionHistory> trxnHistory { get; set; } = new List<WSTollingTransactionHistory>();

        /// <summary>
        /// Count
        /// </summary>
        public int totalCount { get; set; }
    }

    public class WSTollingTransactionHistory
    {
        /// <summary>
        /// TollAmount
        /// </summary>
        public string tollAmount { get; set; }

        /// <summary>
        /// Tag Agency
        /// </summary>
        public string tagAgency { get; set; }

        /// <summary>
        /// TagS #
        /// </summary>
        public string tagSno { get; set; }

        /// <summary>
        /// License Plate Type
        /// </summary>
        public string lpType { get; set; }

        /// <summary>
        /// License Plate #
        /// </summary>
        public string lpNum { get; set; }

        /// <summary>
        /// Origin
        /// </summary>
        public string origin { get; set; }

        /// <summary>
        /// Updated Date
        /// </summary>
        public string updatedDate { get; set; }

        /// <summary>
        /// Image Present
        /// </summary>
        public string imagePresent { get; set; }

        /// <summary>
        /// Paid Status
        /// </summary>
        public string paidStatus { get; set; }

        /// <summary>
        /// License Plate
        /// </summary>
        public string licensePlate { get; set; }

        /// <summary>
        /// Paid Date
        /// </summary>
        public string paidDate { get; set; }

        /// <summary>
        /// Violation Type
        /// </summary>
        public string violationType { get; set; }

        /// <summary>
        /// Ticket #
        /// </summary>
        public string ticketNo { get; set; }

        /// <summary>
        /// Entry Lane
        /// </summary>
        public string entryLane { get; set; }

        /// <summary>
        /// Toll Paid
        /// </summary>
        public string tollPaid { get; set; }

        /// <summary>
        /// Toll Due
        /// </summary>
        public string tollDue { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Actual Axles
        /// </summary>
        public string actualAxles { get; set; }

        /// <summary>
        /// Exit Lane
        /// </summary>
        public string exitLane { get; set; }

        /// <summary>
        /// UID Lane
        /// </summary>
        public string uidUpdated { get; set; }

        /// <summary>
        /// Validation Status
        /// </summary>
        public string validationStatus { get; set; }

        /// <summary>
        /// Exit Date
        /// </summary>
        public string exitDate { get; set; }

        /// <summary>
        /// Entry Date
        /// </summary>
        public string entryDate { get; set; }

        /// <summary>
        /// Confidence
        /// </summary>
        public string confidence { get; set; }

        /// <summary>
        /// License Plate State
        /// </summary>
        public string lpState { get; set; }

        /// <summary>
        /// Toll Date
        /// </summary>
        public string tollDate { get; set; }

        /// <summary>
        /// Toll Fare
        /// </summary>
        public string tollFare { get; set; }
    }

    /// <summary>
    /// Tolling Get Account Summary Response
    /// </summary>
    public class WSTollingGetAccountSummaryResponse
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// Account Summary
        /// </summary>
        public WsTollingAccountSummary accountSummary { get; set; } = new WsTollingAccountSummary();
    }

    public class WsTollingAccountSummary
    {
        /// <summary>
        /// Post Apa Id Balance
        /// </summary>
        public string postapaidBalance { get; set; }

        /// <summary>
        /// Last Invoice Payments
        /// </summary>
        public string lastInvPayments { get; set; }

        /// <summary>
        /// Last Trip Post
        /// </summary>
        public string lastTripPost { get; set; }

        /// <summary>
        /// Current Balance
        /// </summary>
        public string currentBalance { get; set; }

        /// <summary>
        /// Last Invoice Balance
        /// </summary>
        public string lastInvBalance { get; set; }

        /// <summary>
        /// Prepaid Balance
        /// </summary>
        public string prepaidBalance { get; set; }

        /// <summary>
        /// Last Invoice Tolls
        /// </summary>
        public string lastInvTolls { get; set; }

        /// <summary>
        /// Last Statement Date
        /// </summary>
        public string lastStatementDate { get; set; }

        /// <summary>
        /// Payment Plan Balance
        /// </summary>
        public string paymentPlanBalance { get; set; }

        /// <summary>
        /// Last Invoice Adjustments
        /// </summary>
        public string lastInvAdjustments { get; set; }

        /// <summary>
        /// Transponder Deposit
        /// </summary>
        public string transponderDeposit { get; set; }

        /// <summary>
        /// Tag Count
        /// </summary>
        public int tagCount { get; set; }

        /// <summary>
        /// Plate Count
        /// </summary>
        public int plateCount { get; set; }

        /// <summary>
        /// Collection Balance
        /// </summary>
        public string collectionBalance { get; set; }

        /// <summary>
        /// Last Payment Post
        /// </summary>
        public string lastPaymentPost { get; set; }

        /// <summary>
        /// Violation Balance
        /// </summary>
        public string violationBalance { get; set; }
    }

    /// <summary>
    /// WSBusiness
    /// </summary>
    public class WSBusiness
    {
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
    }

    /// <summary>
    /// Transit Account Get Activities response object
    /// http://localhost:12665/TransitAccount/GetActivities?transitAccountId=330000000258
    /// </summary>
    public class WSGetActivitiesResponse
    {
        /// <summary>
        /// List of MSD Activity records
        /// </summary>
        public List<WSMSDActivity> activities { get; set; }
    }

    /// <summary>
    /// Generic Activity object for MSD Activity data
    /// </summary>
    public class WSMSDActivity
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid? id { get; set; }

        /// <summary>
        /// Subject
        /// </summary>
        public string subject { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// StateCode
        /// </summary>
		public string state { get; set; }

        /// <summary>
        /// StatusCode
        /// </summary>
		public string status { get; set; }

        /// <summary>
        /// ModifiedOn
        /// </summary>
		public DateTime? modifiedOn { get; set; }

        /// <summary>
        /// ModifiedBy
        /// </summary>
		public string modifiedBy { get; set; }

        /// <summary>
        /// ActivityTypeCode
        /// </summary>
		public string type { get; set; }
    }

    /// <summary>
    /// Generic Entity object for MSD Entity data
    /// </summary>
    public class WSMSDEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// Logical name of Entity
        /// </summary>
        public string LogicalName { get; set; }

        /// <summary>
        /// Dictionary of Attributes
        /// </summary>
        public Dictionary<string, Object> Attributes;    
        
        public WSMSDEntity()
        {
            Attributes = new Dictionary<string, object>();
        }    
    }

    /// <summary>
    /// Line items to adjust product in the one account or subsystem account.
    /// </summary>
    public class WSAdjustProductLineItem
    {
        /// <summary>
        /// (Required)  The product to be adjusted.
        /// </summary>
        public WSAdjustProduct product { get; set; }
    }

    /// <summary>
    /// The WSAdjustProduct is the base class for value and pass adjustments.
    /// </summary>
    public class WSAdjustProduct
    {
        /// <summary>
        /// (Required)  Signed amount to represent the value of the adjustment.
        /// This number is the number of the base unit of currency, such as cents. 
        /// E.g.: -150 indicates to deduct 1.50.
        /// For “value” products, the value to be adjusted onto the farecard or into the account.
        /// For “pass” products, the value of the pass.
        /// </summary>
        public int adjustmentValue { get; set; }

        /// <summary>
        /// (Required) Determines the type of adjust product line item. 
        // Valid values:
        /// - AdjustOneAccountValue
        /// - AdjustSubsystemAccountValue
        /// (More will be added as we support other types of loads)
        /// </summary>
        public string productLineItemType { get; set; }


        /// <summary>
        /// (Required) The unique identifier of the One Account that the value is to be loaded to.
        /// </summary>
        public string oneAccountId { get; set; }

        /// <summary>
        /// (Required) The unique reference to the purse that the value is to be loaded to.
        /// </summary>
        public string purseId { get; set; }

        /// <summary>
        /// (Required) Unique identifier of the subsystem.
        /// </summary>
        public string subsystem  { get; set; }

        /// <summary>
        /// (Required) Unique identifier for the customer’s account in the subsystem.
        /// </summary>
        public string subsystemAccountReference { get; set; }

        /// <summary>
        /// (Required)  Type of purse it is. 
        /// </summary>
        public string purseType { get; set; }

        /// <summary>
        /// (Required)  Type of the purse restriction.
        /// </summary>
        public string purseRestriction { get; set; }

    }

    public class WSTripCorrection
    {
        /// <summary>
        /// (Required)  The client’s unique identifier for sending this notification.
        ///  This field will be used for detecting duplicate requests and therefore must be globally unique.This value should be specified as:
        /// "<device-id>:<unique-id>".
        /// </summary>
        public string clientRefId { get; set; }

        /// <summary>
        /// (Conditionally-Required)  Unique identifier for the customer. Required for adjustments on One Account and all registered customers.
        /// </summary>
        public string customerId { get; set; }

        /// <summary>
        /// (Optional)  If the customer is not registered (i.e. customerId is not provided), the email address to send order notifications.
        /// If customerId is specified, this field will be ignored.
        /// </summary>
        public string unregisteredEmail { get; set; }

        /// <summary>
        /// (Required) Line items to correct trips in the subsystem account.
        /// </summary>
        public List<WSTravelCorrectionLineItem> correctionLineItems { get; set; }

        /// <summary>
        /// (Optional)  Specifies if the order is approved to be processed. Default is true.
        /// </summary>
        public bool isApproved { get; set; }
    }

    /// <summary>
    /// Line items to correct trips in the subsystem account.
    /// </summary>
    public class WSTravelCorrectionLineItem
    {
        /// <summary>
        /// (Required)  The product to be adjusted.
        /// </summary>
        public WSTravelCorrection correction { get; set; }
    }

    /// <summary>
    /// The WSTravelCorrection is the base class for tap correction and voiding trips.
    /// </summary>
    public class WSTravelCorrection
    {
        /// <summary>
        /// (Required) Unique identifier of the subsystem.
        /// </summary>
        public string subsystem { get; set; }

        /// <summary>
        /// (Required) Unique identifier for the customer’s account in the subsystem.
        /// </summary>
        public string subsystemAccountReference { get; set; }

        /// <summary>
        /// (Required)  Reason code for the correction.
        /// </summary>
        public string reasonCode { get; set; }

        /// <summary>
        /// (Optional)  Notes for the correction.
        /// </summary>
        public string notes { get; set; }

        /// <summary>
        /// (Required) Determines the type of travel correction line item. 
        /// Valid values:
        /// - TransitAccountTripCorrection
        /// - TransitAccountTripVoid
        /// - TransitAccountTapVoid
        /// </summary>
        public string correctionLineItemType { get; set; }

        /// <summary>
        /// (Required)  Unique id in the subsystem for the travel history transaction being voided (trip Id). Required if a travelPresenceId is not provided.
        /// </summary>
        public string transactionId { get; set; }


        /// <summary>
        /// (Optional) Uniquely identifies the travel presence event to be corrected within this travel transaction.
        /// If not provided, it indicates that the correction is for adding a new presence event.
        /// </summary>
        public string travelPresenceId { get; set; }


        /// <summary>
        /// (Required) Fare collection system assigned identifier or code for the operator of the travel service.
        /// </summary>
        [JsonProperty(PropertyName = "operator")]
        public string Operator { get; set; }

        /// <summary>
        /// (Required)  The unique reference to the mode of travel.
        /// </summary>
        public string travelMode { get; set; }

        /// <summary>
        /// (Optional)  Stop Id or Code for the corrected or new travel presence. 
        /// </summary>
        public string stopPoint { get; set; }


    }


    /// <summary>
    /// Response object for many calls to SessionController
    /// </summary>
    public class WSSessionResponse
    {
        /// <summary>
        /// The session ID
        /// </summary>
        public Guid? sessionId { get; set; }

        /// <summary>
        /// The auto-incrementing session number
        /// </summary>
        public string sessionNumber { get; set; }

        /// <summary>
        /// The session start timestamp
        /// </summary>
        public DateTime? startDateTime { get; set; }

        /// <summary>
        /// The registered customer ID
        /// </summary>
        public Guid? customerId { get; set; }

        /// <summary>
        /// The registered customer's first name
        /// </summary>
        public string customerFirstName { get; set; }

        /// <summary>
        /// The registered customer's last name
        /// </summary>
        public string customerLastName { get; set; }

        /// <summary>
        /// The contact ID
        /// </summary>
        public Guid? contactId { get; set; }

        /// <summary>
        /// The contact's first name
        /// </summary>
        public string contactFirstName { get; set; }

        /// <summary>
        /// The contat's last name
        /// </summary>
        public string contactLastName { get; set; }

        /// <summary>
        /// The unlinked MSD cub_transitaccount ID
        /// </summary>
        public Guid? transitAccountId { get; set; }

        /// <summary>
        /// The unlinked transit account's subsystem
        /// </summary>
        public string subsystem { get; set; }

        /// <summary>
        /// The unlinked transit account's reference ID
        /// </summary>
        public string subsystemAccountReference { get; set; }

        /// <summary>
        /// Does this session reference a registered customer?
        /// </summary>
        public bool? isRegistered { get; set; }

        /// <summary>
        /// Does this session reference a verified contact?
        /// </summary>
        public bool? isVerified { get; set; }

        /// <summary>
        /// Does this session reference a registered customer's primary contact?
        /// </summary>
        public bool? isPrimaryContact { get; set; }

        /// <summary>
        /// The session end timestamp
        /// </summary>
        public DateTime? endDateTime { get; set; }

        /// <summary>
        /// The session channel Name
        /// </summary>
        public string channelName { get; set; }

        /// <summary>
        /// The session transcript URL
        /// </summary>
        public string transcriptUrl { get; set; }

        /// <summary>
        /// The session notes
        /// </summary>
        public string notes { get; set; }

        /// <summary>
        /// The Created By user ID
        /// </summary>
        public Guid? createdById { get; set; }

        /// <summary>
        /// The modified by user ID
        /// </summary>
        public Guid? modifiedById { get; set; }

        // <summary>
        /// The created by user full name
        /// </summary>
        public string createdByUserFullName { get; set; }

        // <summary>
        /// The modified by user full name
        /// </summary>
        public string modifiedByUserFullName { get; set; }

        // <summary>
        /// The created date and time
        /// </summary>
        public DateTime? createdOnDateTime { get; set; }

        // <summary>
        /// The modified date and time
        /// </summary>
        public DateTime? modifiedOnDateTime { get; set; }

        // <summary>
        /// The duration time
        /// </summary>
        public TimeSpan? durationTime { get { return createdOnDateTime.HasValue && endDateTime.HasValue ? endDateTime.Value - createdOnDateTime.Value  : (TimeSpan?)null  ; }   }

        // <summary>
        /// The session status
        /// </summary>
        public string status { get; set; }

        // <summary>
        /// The list of custom activities linked to session
        /// </summary>
        public string activities { get; set; }

        // <summary>
        /// The list of cases activities linked to session
        /// </summary>
        public string cases { get; set; }

        /// <summary>
        /// The id of link between two sessions
        /// </summary>
        public Guid? linkSessionsId { get; set; }

    }
   

        /// <summary>
        /// Response object for some TransitAccountLogic methods
        /// </summary>
        public class WSCubTransitAccount
    {
        /// <summary>
        /// MSD record ID
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// TransitAccountId aka SubsystemAccountReference
        /// </summary>
        public string TransitAccountId { get; set; }

        /// <summary>
        /// SubsystemId
        /// </summary>
        public string SubsystemId { get; set; }

        /// <summary>
        /// MSD Customer record reference
        /// </summary>
        public EntityReference Customer { get; set; }

        /// <summary>
        /// MSDContact record reference
        /// </summary>
        public EntityReference Contact { get; set; }
    }

    /// <summary>
    /// Rules for passwords
    /// </summary>
    public class WSPasswordRules
    {
        /// <summary>
        /// Minimum number of digits required in password
        /// </summary>
        public int minDigit { get; set; }

        /// <summary>
        /// Minimum number of letters required in password
        /// </summary>
        public int minLetter { get; set; }

        /// <summary>
        /// Valid special characters allowed in password
        /// </summary>
        public string specialChars { get; set; }

        /// <summary>
        /// Minimum number of special characters required in password
        /// </summary>
        public int minSpecialChars { get; set; }

        /// <summary>
        /// Boolean flag if password needs to have mixed characters
        /// </summary>
        public bool forceMixedCase { get; set; }

        /// <summary>
        /// Boolean flag if password rejects valid dictionary words.
        /// </summary>
        public bool useDictionary { get; set; }

        /// <summary>
        /// Minimum length of password
        /// </summary>
        public int minLength { get; set; }

        /// <summary>
        /// Maximum length of password
        /// </summary>
        public int maxLength { get; set; }

        /// <summary>
        /// Boolean flag if password is to be checked for consecutive characters and character sets
        /// </summary>
        public bool checkRepeats { get; set; }
    }

    /// <summary>
    /// Rules for username
    /// </summary>
    public class WSUsernameRules
    {
        /// <summary>
        /// Maximum length of username
        /// </summary>
        public int maxLength { get; set; }

        /// <summary>
        /// Minimum length of username
        /// </summary>
        public int minLength { get; set; }

        /// <summary>
        /// If username should be in valid email format
        /// </summary>
        public bool requiresEmail { get; set; }
    }


    #region Auto create activity related

    /// <summary>
    /// WSCreateNewActivityResponse - returns new activity id
    /// </summary>
    public class WSCreateNewActivityResponse : WSMSD
    {
        public Guid Id { get; set; }
    }

    #region Classes used in deserialization of the config data stored in cub_ActivityAutoCreateConfig

    public class ActivityAutoCreateConfig
    {
        public string ActivityType { get; set; }
        public List<string> RequiredFields { get; set; }
        public string StateCode { get; set; } = "Completed";
        public string StatusCode { get; set; } = "Closed";
        public bool Enabled { get; set; } = true;
    }

    public class ActivityAutoCreateConfigData
    {
        public List<ActivityAutoCreateConfig> ActivityAutoCreateConfig { get; set; }
    }

    #endregion

    /// <summary>
    /// This class is used to pass data needed to create MSD Activity
    /// </summary>
    public class CreateMSDActivityData
    {
        #region Public Properties
        public Guid ActivityTypeId { get; set; } = Guid.Empty;
        public Guid ActivitySubjectId { get; set; } = Guid.Empty;
        public Guid SessionId { get; set; } = Guid.Empty;
        public Guid SubsystemId { get; set; } = Guid.Empty;
        public Guid CustomerId { get; set; } = Guid.Empty;
        public Guid ContactId { get; set; } = Guid.Empty;
        public Guid CaseId { get; set; } = Guid.Empty;
        public Guid TaskId { get; set; } = Guid.Empty;
        public string Category { get; set; } = "Activity";
        public string Channel { get; set; } = "CRM";
        public Guid CreatedById { get; set; } = Guid.Empty;
        public string Description { get; set; } = string.Empty;
        #endregion
    }

    #endregion
}