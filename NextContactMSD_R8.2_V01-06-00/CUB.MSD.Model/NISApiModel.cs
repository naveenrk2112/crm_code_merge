/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model.MSDApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUB.MSD.Model.NIS
{
    /// <summary>
    /// NISApiModel
    /// </summary>
    public class NISApiModel
    {
    }

    /// <summary>
    /// WSCompleteCustomerRegistrationRequest
    /// </summary>
    public class WSCompleteCustomerRegistrationRequest
    {
        /// <summary>
        /// contactId
        /// </summary>
        public string contactId;
        //public WSName contactName;
        //public string contactSmsPhone;
        //public string contactEmail;
        //public string contactUsername;
        //public string contactTempPassword;
        //public DateTime contactTempPasswordExpirationDate;
    }

    /// <summary>
    /// WSCompleteCustomerRegistrationResponse
    /// </summary>
    public class WSCompleteCustomerRegistrationResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// oneAccountId
        /// </summary>
        public long oneAccountId;
    }

    /// <summary>
    /// WSTravelTokenSearchRequest
    /// </summary>
    public abstract class WSTravelTokenSearchRequest
    {
        /// <summary>
        /// tokenType
        /// </summary>
        public string tokenType { get; set; }
    }

    /// <summary>
    /// WSBankcardTravelTokenSearchRequest
    /// </summary>
    public class WSBankcardTravelTokenSearchRequest : WSTravelTokenSearchRequest
    {
        /// <summary>
        /// pan
        /// </summary>
        public string pan { get; set; }

        /// <summary>
        /// cardExpiryMMYY
        /// </summary>
        public string cardExpiryMMYY { get; set; }
    }

    /// <summary>
    /// WSTravelTokenSearchResponse
    /// </summary>
    public class WSTravelTokenSearchResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// totalCount
        /// </summary>
        public int totalCount { get; set; }

        /// <summary>
        /// tokenSeacrhResult
        /// </summary>
        public WSTravelTokenSearchResult tokenSeacrhResult { get; set; }
    }

    /// <summary>
    /// WSTravelTokenSearchResult
    /// </summary>
    public class WSTravelTokenSearchResult
    {
        /// <summary>
        /// subsystemAccountData
        /// </summary>
        public WSSubsystemAccountSearchResult subsystemAccountData { get; set; }

        /// <summary>
        /// oneAccountId
        /// </summary>
        public long oneAccountId { get; set; }

        /// <summary>
        /// nickname
        /// </summary>
        public string nickname { get; set; }

        /// <summary>
        /// customerData
        /// </summary>
        public WSCustomerSearchResult customerData { get; set; }
    }

    /// <summary>
    /// WSCustomerSearchResult
    /// </summary>
    public class WSCustomerSearchResult
    {
        /// <summary>
        /// customerId
        /// </summary>
        public string customerId { get; set; }

        /// <summary>
        /// customerType
        /// </summary>
        public string customerType { get; set; }

        /// <summary>
        /// (Required) The current account status identifier as the key and the account status description as the value.
        /// </summary>
        public WSKeyValue customerStatus { get; set; }

        /// <summary>
        /// contacts
        /// </summary>
        public WSCustomerContactInfo[] contacts { get; set; }
    }

    /// <summary>
    /// WSCustomerNotificationResendRequest
    /// </summary>
    public class WSCustomerNotificationResendRequest
    {
        /// <summary>
        /// notificationId
        /// </summary>
        public string notificationId { get; set; }

        /// <summary>
        /// notificationType
        /// </summary>
        public string notificationType { get; set; }

        /// <summary>
        /// allowReformat
        /// </summary>
        public bool allowReformat { get; set; }

        /// <summary>
        /// channel
        /// </summary>
        public string channel { get; set; }

        /// <summary>
        /// clientRefId
        /// </summary>
        public string clientRefId { get; set; }
    }

    /// <summary>
    /// WSCustomerContactInfo
    /// </summary>
    public class WSCustomerContactInfo
    {
        /// <summary>
        /// contactId
        /// </summary>
        public string contactId { get; set; }

        /// <summary>
        /// contactType
        /// </summary>
        public string contactType { get; set; }

        /// <summary>
        /// email
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// dateOfBirth
        /// </summary>
        public string dateOfBirth { get; set; }

        /// <summary>
        /// username
        /// </summary>
        public string username { get; set; }

        /// <summary>
        /// pin
        /// </summary>
        public string pin { get; set; }

        /// <summary>
        /// name
        /// </summary>
        public WSName name { get; set; }

        /// <summary>
        /// address
        /// </summary>
        public WSAddressExt address { get; set; }

        /// <summary>
        /// phone
        /// </summary>
        public WSPhoneExt[] phone { get; set; }

        /// <summary>
        /// personalIdentifierInfo
        /// </summary>
        public WSPersonalIdentifier personalIdentifierInfo { get; set; }

        /// <summary>
        /// securityQAs
        /// </summary>
        public WSSecurityQA[] securityQAs { get; set; }

        /// <summary>
        /// contactStatus
        /// </summary>
        public string contactStatus { get; set; }

        /// <summary>
        /// credentialStatus
        /// </summary>
        public string credentialStatus { get; set; }
    }

    /// <summary>
    /// WSMobileDevice
    /// </summary>
    public class WSMobileDevice
    {
        /// <summary>
        /// mobileAppId
        /// </summary>
        public string mobileAppId { get; set; }

        /// <summary>
        /// mobileAppNickname
        /// </summary>
        public string mobileAppNickname { get; set; }

        /// <summary>
        /// pnToken
        /// </summary>
        public string pnToken { get; set; }

        /// <summary>
        /// osType
        /// </summary>
        public string osType { get; set; }

        /// <summary>
        /// osVersion
        /// </summary>
        public string osVersion { get; set; }
    }

    /// <summary>
    /// WSName
    /// </summary>
    public class WSName
    {
        /// <summary>
        /// title
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// firstName
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// middleInitial
        /// </summary>
        public string middleInitial { get; set; }

        /// <summary>
        /// lastName
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// nameSuffix
        /// </summary>
        public string nameSuffix { get; set; }
    }

    /// <summary>
    /// WSAddressExt Response
    /// </summary>
    public class WSAddressExtResponse
    {
        public WSAddressExt address { get; set; }
    }

    /// <summary>
    /// WSAddressExt
    /// </summary>
    public class WSAddressExt : WSAddress
    {
        /// <summary>
        /// addressId
        /// </summary>
        public string addressId { get; set; }
    }

    /// <summary>
    /// WSAddress
    /// </summary>
    public class WSAddress
    {
        /// <summary>
        /// address1
        /// </summary>
        public string address1 { get; set; }

        /// <summary>
        /// address2
        /// </summary>
        public string address2 { get; set; }

        /// <summary>
        /// address3
        /// </summary>
        public string address3 { get; set; }

        /// <summary>
        /// city
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// state
        /// </summary>
        public string state { get; set; }

        /// <summary>
        /// country
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// postalCode
        /// </summary>
        public string postalCode { get; set; }

    }

    /// <summary>
    /// class to reference an address object containing update information
    /// </summary>
    public class WSUpdateAddress
    {
        public WSAddress address { get; set; }
    }

    /// <summary>
    /// Abstract class to reference a specific travel token object. 
    /// Each specific token defines the fields to uniquely identify th specific travel token.
    /// </summary>
    public abstract class WSTravelToken
    {
        /// <summary>
        /// (Required)  Specific identifier of the travel token type.
        /// Types: Mobile, Bankcard, Smartcard, LicensePlate, Transponder
        /// </summary>
        public string tokenType { get; set; }
    }

    /// <summary>
    /// WSMobileTravelToken is a subclass of WSTravelToken. This object represents the mobile travel token details
    /// Expected to set the tokenType as �Mobile�.
    /// </summary>
    public class WSMobileTravelToken : WSTravelToken
    {
        /// <summary>
        /// (Required) The derived unique token identifying the mobile device for use.
        /// </summary>
        public string mobileToken { get; set; }
    }

    /// <summary>
    /// WSBankcardTravelToken is a subclass of WSTravelToken. This object represents the bankcard travel token details. 
    /// Expected to set the tokenType as �Bankcard�.    
    /// </summary>
    public class WSBankcardTravelToken : WSTravelToken
    {
        public string TokenType { set; get; }
        public string SubSystem { set; get; }

        public string encryptedToken { set; get; }

        /// <summary>
        /// (Required)  The name of the key used to encrypt the token. Valid values are project specific.
        /// e.g. �MobileKeyNameV1�
        /// </summary>
        public string keyName { set; get; }

        /// <summary>
        /// (Required)  The system generated security token.
        /// </summary>
        public string nonce { get; set; }

    }

    /// <summary>
    /// WSSmartcardTravelToken is a subclass of WSTravelToken. This object represents the smartcard travel token details.
    /// Expected to set the tokenType as �Smartcard�.
    /// </summary>
    public class WSSmartcardTravelToken : WSTravelToken
    {
        /// <summary>
        /// (Required) The serial number of the card.
        /// </summary>
        public string serialNumber { get; set; }

        /// <summary>
        /// (Optional) The card expiry date.
        /// </summary>
        public string cardExpiryDate { get; set; }
    }

    /// <summary>
    /// Class to reference a specific travel token object as a user-friendly displayable search result. 
    /// Each specific token defines the fields to uniquely identify the specific travel token.
    /// </summary>
    [JsonConverter(typeof(TravelTokenDisplayConverter))]
    public abstract class WSTravelTokenDisplay
    {
        /// <summary>
        /// (Required) Specific identifier of the travel token type. See section 2.6.10 for allowed values.
        /// </summary>
        public string tokenType { get; set; }

        /// <summary>
        /// (Optional) Nick name of the travel token.
        /// </summary>
        public string tokenNickname { get; set; }

        /// <summary>
        /// (Required) When the token was last utilized.
        /// </summary>
        public DateTime lastUseDTM { get; set; }
    }

    /// <summary>
    /// WSBankcardTravelTokenDisplay is a subclass of WSTravelTokenDisplay. 
    /// This object represents displayable fields for bankcard travel tokens. 
    /// Expected to set the tokenType as �Bankcard�.
    /// </summary>
    public class WSBankcardTravelTokenDisplay : WSTravelTokenDisplay
    {
        /// <summary>
        /// string (20) (Required) The masked PAN
        /// </summary>
        public string maskedPAN { get; set; }

        /// <summary>
        /// string (4) (Required) The card expiry date in MMYY format. 
        /// For MM, 01 = January
        /// </summary>
        public string cardExpiryMMYY { get; set; }
    }

    /// <summary>
    /// WSSmartcardTravelTokenDisplay is a subclass of WSTravelTokenDisplay. 
    /// This object represents displayable fields for smartcard travel tokens. 
    /// Expected to set the tokenType as �Smartcard�.
    /// </summary>
    public class WSSmartcardTravelTokenDisplay : WSTravelTokenDisplay
    {
        /// <summary>
        /// string (20) (Required) The complete serial number of the card
        /// </summary>
        public string serialNumber { get; set; }

        /// <summary>
        /// string date (Required) The card expiry
        /// </summary>
        public string cardExpiryDate { get; set; }
    }

    /// <summary>
    /// WSMobileTravelTokenDisplay is a subclass of WSTravelTokenDisplay. 
    /// This object represents displayable fields for mobile travel tokens. 
    /// Expected to set the tokenType as �Mobile�.
    /// </summary>
    public class WSMobileTravelTokenDisplay : WSTravelTokenDisplay
    {
        /// <summary>
        /// string (20) (Required) Serialized mobile token for display
        /// </summary>
        public string serializedMobileToken { get; set; }
    }

    /// <summary>
    /// WSLicensePlateTravelTokenDisplay is a subclass of WSTravelTokenDisplay. 
    /// This object represents displayable fields for license plate travel tokens. 
    /// Expected to set the tokenType as �LicensePlate�.
    /// </summary>
    public class WSLicensePlateTravelTokenDisplay : WSTravelTokenDisplay
    {
        /// <summary>
        /// string (20) (Required) License plate number for display
        /// </summary>
        public string LicensePlate { get; set; }
    }

    /// <summary>
    /// WSTransponderTravelTokenDisplay is a subclass of WSTravelTokenDisplay. 
    /// This object represents displayable fields for transponder travel tokens. 
    /// Expected to set the tokenType as �Transponder�.
    /// </summary>
    public class WSTransponderTravelTokenDisplay : WSTravelTokenDisplay
    {
        /// <summary>
        /// string (20) (Required) Serialized transponder token for display
        /// </summary>
        public string serializedTransponderToken { get; set; }
    }

    /// <summary>
    /// WSPhone
    /// </summary>
    public class WSPhone
    {
        /// <summary>
        /// Country
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// number
        /// </summary>
        public string number { get; set; }

        /// <summary>
        /// type
        /// </summary>
        public string type { get; set; }
    }
    
    /// <summary>
    /// WSPhoneExt
    /// </summary>
    public class WSPhoneExt : WSPhone
    {
        public string displayNumber { get; set; }
    }

    /// <summary>
    /// WSPersonalIdentifier
    /// </summary>
    public class WSPersonalIdentifier
    {
        /// <summary>
        /// personalIdentifier
        /// </summary>
        public string personalIdentifier { get; set; }

        /// <summary>
        /// personalIdentifierType
        /// </summary>
        public string personalIdentifierType { get; set; }
    }

    /// <summary>
    /// WSSecurityQA
    /// </summary>
    public class WSSecurityQA
    {
        /// <summary>
        /// securityQuestion
        /// </summary>
        public string securityQuestion { get; set; }

        /// <summary>
        /// securityAnswer
        /// </summary>
        public string securityAnswer { get; set; }
    }

    /// <summary>
    /// Response for get funding source
    /// 2.8.3 customer/<customerid>/fundingsource/<fundingsourceid>
    /// </summary>
    public class WSGetFundingSourceResponse : WSMSD
    {
        /// <summary>
        /// header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required)  Funding source details
        /// </summary>
        public WSMSDFundingSource fundingSource { get; set; }
    }

    /// <summary>
    /// Response for get funding source
    /// 2.8.4 customer/<customerid>/fundingsource
    /// </summary>
    public class WSGetFundingSourcesResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required)  Funding source details.
        /// </summary>
        public List<WSMSDFundingSource> fundingSources { get; set; }
        //public string fundingSources { get; set; }
    }

    /// <summary>
    /// Request for post funding source - 2.8.2
    /// </summary>
    public class WSFundingSourceRequest
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// Funding source
        /// </summary>
        public WSFundingSource fundingSource { get; set; }
    }

    /// <summary>
    /// Defines a single funding source which is either credit card or bank account details (not both). 
    /// Contains a new billing address or a reference to an existing address but not both
    /// </summary>
    public class WSFundingSource
    {
        /// <summary>
        /// (Conditionally Required) The credit card information associated with this funding source.
        /// Either credit card or direct debit but not both can be set per funding
        /// </summary>
        public WSCreditCard creditCard { get; set; }

        /// <summary>
        /// (Conditionally Required) The debit information associated with this funding source.
        /// Either credit card or direct debit but not both can be set per funding source
        /// </summary>
        public WSDirectDebit directDebit { get; set; }

        /// <summary>
        /// (Optional) True if this is to be marked as primary funding source for the account. 
        /// False will be ignored
        /// </summary>
        public bool setAsPrimary { get; set; }

        /// <summary>
        /// string(40) (Required) A unique reference to an address of the customer
        /// </summary>
        public string billingAddressId { get; set; } 
    }

    [JsonConverter(typeof(CreditCardConverter))]
    public abstract class WSCreditCard
    {
        /// <summary>
        /// string(40) (Required)  The unique card ID, assigned by the tokenization system
        /// </summary>
        public string pgCardId { get; set; }

        /// <summary>
        /// string(4) (Required)  The card expiry date in MMYY format.
        /// For MM, 01 = January
        /// </summary>
        public string cardExpiryMMYY { get; set; }

        /// <summary>
        /// string(60) (Required)  Full name of credit card owner as it appears on credit card.
        /// </summary>
        public string nameOnCard { get; set; }

        /// <summary>
        /// string(30) (Required)  The type of credit card (Such as Visa, Mastercard, etc)
        /// </summary>
        public string creditCardType { get; set; }
    }

    public class WSCreditCardReference : WSCreditCard
    {
        /// <summary>
        /// string(20) (Required)  The masked PAN.  
        /// The masking format is project specific, 
        /// but is commonly just the last 4 digits of the PAN
        /// </summary>
        public string maskedPan { get; set; }
    }

    /// <summary>
    /// Clear Credit card data
    /// </summary>
    public class WSCreditCardClear : WSCreditCard
    {
        /// <summary>
        /// string(20) The clear PAN
        /// </summary>
        public string PAN { get; set; }

        /// <summary>
        /// (Required)  Address verification details for the associated billing address
        /// </summary>
        public WSAvsData avsData { get; set; }
    }

    public class WSAvsData
    {
        /// <summary>
        /// string(50) (Required)  First line of the address
        /// </summary>
        public string address1 { get; set; }

        /// <summary>
        /// string(2) (Required)  See section 2.6.1 for allowable values
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// string(10) (Required)  Valid values are country specific. 
        /// For example: for U.S. addresses, the postal code must be exactly 5 digits
        /// </summary>
        public string postalCode { get; set; }
    }

    /// <summary>
    /// Abstract class to reference either WSDirectDebitClear or WSDirectDebitReference
    /// </summary>
    [JsonConverter(typeof(DirectDebitConverter))]
    public abstract class WSDirectDebit
    {
        /// <summary>
        /// string(100) (Required)  Full name of the account holder
        /// </summary>
        public string nameOnAccount { get; set; }

        /// <summary>
        /// string(1) (Required)  The type of the account. 
        /// C � Checking
        /// S � Savings
        /// </summary>
        public string accountType { get; set; }

        /// <summary>
        /// string(50) (Required)  The name of the financial institution
        /// </summary>
        public string financialInstitutionName { get; set; }
    }

    public class WSDirectDebitClear : WSDirectDebit
    {
        /// <summary>
        /// string(17) (Required)  The bank account number of the account holder
        /// </summary>
        public string bankAccountNumber { get; set; }

        /// <summary>
        /// string(10) (Required)  Bank routing number
        /// </summary>
        public string bankRoutingNumber { get; set; }
    }

    public class WSDirectDebitReference : WSDirectDebit
    {
        /// <summary>
        /// made up data since the document has left it out
        /// </summary>
        public string directDebitReferenceId { get; set; }
    } 

    /// <summary>
    /// status of an existing transit account associated to a subsystem
    /// </summary>
    public class WSTransitAccountSubsystemStatusResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// string(40) (Required)  The current status of the account. 
        /// Possible values are:
        ///�	Active
        ///�	Suspended
        ///�	Closed
        /// </summary>
        public string accountStatus { get; set; }

        /// <summary>
        /// string(40) (Required)  The textual description of the accountStatus field
        /// </summary>
        public string accountStatusDescription { get; set; }

        /// <summary>
        /// DateTime string in zulu format (Optional) When the account status was last changed
        /// </summary>
        public string accountStatusChangeDateTime { get; set; }

        /// <summary>
        /// string(20) (Required)  The class of rider permitted to use this card. 
        /// E.g. Adult, Senior, Child, etc.
        /// Rider classes are system specific
        /// </summary>
        public string riderClass { get; set; }

        /// <summary>
        /// string(405) (Required)  A textual description of the riderClass field        
        /// </summary>
        public string riderClassDescription { get; set; }

        /// <summary>
        /// DateTime string in zulu format (Optional) When the account was created
        /// </summary>
        public string accountCreatedDateTime { get; set; }

        /// <summary>
        /// (Required)  Tokens associated with the account
        /// </summary>
        public List<WSTransitAccountToken> tokens { get; set; }

        /// <summary>
        /// (Optional)  The current list of passes in the account
        /// </summary>
        public List<WSTransitAccountPass> passes { get; set; }

        /// <summary>
        /// (Required) List of purses for this account
        /// </summary>
        public List<WSSubsystemPurse> purses { get; set; }

        /// <summary>
        /// (Optional) Last usage of the account
        /// </summary>
        public WSLastUsageDetails accountLastUsageDetails { get; set; }

        /// <summary>
        /// (Optional) Unique identifier for the foreign customer.
        /// </summary>
        public string foreignCustomerid { get; set; }

        /// <summary>
        /// (Optional) Information on tap correction info like counts.
        /// </summary>
        public WSTapCorrectionInfo tapCorrectionInfo { get; set; }

        /// <summary>
        /// (Optional) Details of any outstanding debt on the account and any action available to recover the debt.
        /// </summary>
        public WSDebtCollectionInfo debtCollectionInfo { get; set; }
    }

    /// <summary>
    /// Description of the last captured usage
    /// </summary>
    public class WSLastUsageDetails
    {
        /// <summary>
        /// DateTime string in zulu format (Required)  When the tap occurred (as reported by the device).
        /// </summary>
        public string transactionDateTime { get; set; }

        /// <summary>
        /// string(40) (Required)  Device where the last tap was seen 
        /// </summary>
        public string device { get; set; }

        /// <summary>
        /// string(40) (Required)  The tap status reported by the device
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// string(40) (Required)  The description of the tap status reported by the device
        /// </summary>
        public string statusDescription { get; set; }

        /// <summary>
        /// string(40) (Required)  The description of the location
        /// </summary>
        public string location { get; set; }
    }

    public class WSTapCorrectionInfo
    {
        /// <summary>
        /// (Required) Total of incomplete journeys against the account
        /// </summary>
        public int totalIncompleteCount { get; set; }

        /// <summary>
        /// (Required) Total of incomplete journeys that may be corrected within the late data window
        /// </summary>
        public int provisionalIncompleteCount { get; set; }

        /// <summary>
        /// (Required) Number of manual fills within the period
        /// </summary>
        public int manualFillCount { get; set; }

        /// <summary>
        /// (Required) Number of auto fills within the period
        /// </summary>
        public int autoFillCount { get; set; }
    }

    /// <summary>
    /// Defines the Transit Account Tokens
    /// </summary>
    public class WSTransitAccountToken
    {

        public string tokenId { get; set; }

        /// <summary>
        /// string(40) (Required)  The type of token. 
        /// Examples are:
        ///�	Smartcard
        ///�	StandardBankcard
        ///�	AgencyIssuedBankcard
        ///�	ClosedLoopBankcard
        /// </summary>
        public string subsystemTokenType { get; set; }

        /// <summary>
        /// string(40) (Required)  The textual description of subsystemTokenType field
        /// </summary>
        public string subsystemTokenTypeDescription { get; set; }

        /// <summary>
        /// (Required) Token credentials that can be returned.
        /// Examples: 
        /// For bankcard masked PAN with expiry date; 
        /// For smart card serial number with expiry date; 
        /// For license plates the license plate number, state, plate type, etc..
        /// </summary>
        public WSTravelTokenDisplay tokenInfo { get; set; }

        /// <summary>
        /// string(40) (Optional) Tokens current status.
        ///�	Active
        ///�	Suspended
        ///�	Closed
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// string(40) Text describing the status
        /// </summary>
        public string statusDescription { get; set; }

        /// <summary>
        /// DateTime string in zulu format (Optional) When the token status was last changed
        /// </summary>
        public string statusChangeDateTime { get; set; }

        /// <summary>
        /// (Optional) Last usage of the token
        /// </summary>
        public WSLastUsageDetails tokenLastUsageDetails { get; set; }
    }

    /// <summary>
    /// Class containing the subsystem information
    /// </summary>
    public class WSSubsystemInfo
    {
        /// <summary>
        /// string(20) (Required) Unique identifier of the subsystem
        /// </summary>
        public string subsystem { get; set; }

        /// <summary>
        /// string(30) (Required) Name of the subsystem
        /// </summary>
        public string subsystemDescription { get; set; }

        /// <summary>
        /// (Required) Unique identifier of the authority owning the subsystem
        /// </summary>
        public int authorityId { get; set; }

        /// <summary>
        /// string(30) (Required) Name of the authority
        /// </summary>
        public string authorityDescription { get; set; }

        /// <summary>
        /// (Required) List of travel modes supported by the subsystem
        /// </summary>
        public List<WSKeyValue> travelModes { get; set; }
    }

    /// <summary>
    /// Subsytem token details
    /// </summary>
    public class WSSubsystemAccountToken
    {
        /// <summary>
        /// string(40) (Required)  The type of token. Examples are:
        /// �	Smartcard
        /// �	StandardBankcard
        /// �	AgencyIssuedBankcard
        /// �	ClosedLoopBankcard
        /// </summary>
        public string subsystemTokenType { get; set; }

        /// <summary>
        /// string(40) (Required)  The textual description of subsystemTokenType field
        /// </summary>
        public string subsystemTokenTypeDescription { get; set; }

        /// <summary>
        /// (Required) Token credentials that can be returned.
        /// Examples: 
        /// For bankcard -  masked PAN with expiry date; 
        /// For smartcard - serial number with expiry date; 
        /// For license plates - the license plate number, state, plate type, etc..
        /// </summary>
        public WSTravelTokenDisplay tokenInfo { get; set; }

        /// <summary>
        /// string(40) (Optional) Tokens current status.
        ///�	Active
        ///�	Suspended
        ///�	Closed
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// string(40) (Required)  The textual description of status field
        /// </summary>
        public string statusDescription { get; set; }

        /// <summary>
        /// (Optional) When the token status was last changed
        /// </summary>
        public DateTime statusChangeDateTime { get; set; }
    }

    /// <summary>
    /// Purse detail information
    /// </summary>
    public class WSSubsystemPurse
    {
        /// <summary>
        /// string(30) (Optional)  The nickname for this purse. 
        /// If a nickname is not specified, a default will be assigned
        /// </summary>
        public string nickname { get; set; }

        /// <summary>
        /// (Required)  Current balance of the purse
        /// </summary>
        public int balance { get; set; }

        /// <summary>
        /// string(40) (Required)  Type of purse it is
        /// </summary>
        public string purseType { get; set; }

        /// <summary>
        /// string(40) (Required)  Description of the purse type
        /// </summary>
        public string purseTypeDescription { get; set; }

        /// <summary>
        /// string(40) (Required)  Type of the purse restriction
        /// </summary>
        public string purseRestriction { get; set; }

        /// <summary>
        /// string(40) (Required)  Description of the purse restriction
        /// </summary>
        public string purseRestrictionDescription { get; set; }
    }

    /// <summary>
    /// WSSubsystemAccountDetailedInfo
    /// </summary>
    public class WSSubsystemAccountDetailedInfo
    {
        /// <summary>
        /// accountStatus
        /// </summary>
        public string accountStatus { get; set; }

        /// <summary>
        /// accountStatusDescription
        /// </summary>
        public string accountStatusDescription { get; set; }

        /// <summary>
        /// accountStatusCause
        /// </summary>
        public string accountStatusCause { get; set; }

        /// <summary>
        /// accountType
        /// </summary>
        public string accountType { get; set; }

        /// <summary>
        /// accountTypeDescription
        /// </summary>
        public string accountTypeDescription { get; set; }

        /// <summary>
        /// tokens
        /// </summary>
        public List<WSSubsystemAccountToken> tokens { get; set; }

        /// <summary>
        /// purses
        /// </summary>
        public List<WSSubsystemPurse> purses { get; set; }

        /// <summary>
        /// accountAlerts
        /// </summary>
        public List<string> accountAlerts { get; set; }

        /// <summary>
        /// lastChangedStatusDTM
        /// </summary>
        public DateTime lastChangedStatusDTM { get; set; }

        /// <summary>
        /// debtCollectionInfo
        /// </summary>
        public WSDebtCollectionInfo debtCollectionInfo { get; set; }
    }

    /// <summary>
    /// WSSubsystemAccountSearchResult
    /// </summary>
    public class WSSubsystemAccountSearchResult
    {
        /// <summary>
        /// subsystemAccountReference
        /// </summary>
        public string subsystemAccountReference { get; set; }

        /// <summary>
        /// subsystemTokenTypeDescription
        /// </summary>
        public string subsystemTokenTypeDescription { get; set; }

        /// <summary>
        /// tokenStatusDescription
        /// </summary>
        ///
        public string tokenStatusDescription { get; set; }

        /// <summary>
        /// subsystemAccountTypeDescription
        /// </summary>
        public string subsystemAccountTypeDescription { get; set; }

        /// <summary>
        /// subsystemAccountStatusDescription
        /// </summary>
        public string subsystemAccountStatusDescription { get; set; }

        /// <summary>
        /// tokenInfo
        /// </summary>
        public WSTravelTokenDisplay tokenInfo { get; set; }
    }

    /// <summary>
    /// WSSubsystemAccountInfo
    /// </summary>
    public class WSSubsystemAccountInfo
    {
        /// <summary>
        /// nickname
        /// </summary>
        public string nickname { get; set; }

        /// <summary>
        /// subsystemAccountReference
        /// </summary>
        public string subsystemAccountReference { get; set; }

        /// <summary>
        /// subsystemInfo
        /// </summary>
        public WSSubsystemInfo subsystemInfo { get; set; }

        /// <summary>
        /// subsystemAccountDetailedInfo
        /// </summary>
        public WSSubsystemAccountDetailedInfo subsystemAccountDetailedInfo { get; set; }

    }
    /// <summary>
    /// WSSmartcardTravelTokenSearchRequest
    /// </summary>
    public class WSSmartcardTravelTokenSearchRequest : WSTravelTokenSearchRequest
    {
        /// <summary>
        /// serialNumber
        /// </summary>
        public string serialNumber { get; set; }

        /// <summary>
        /// cardExpiryMMYY
        /// </summary>
        public string cardExpiryMMYY { get; set; }
    }

    /// <summary>
    /// WSWebExceptionResponseHeader
    /// </summary>
    public class WSWebExceptionResponseHeader
    {
        /// <summary>
        /// result
        /// </summary>
        public string result { get; set; }

        /// <summary>
        /// uid
        /// </summary>
        public string uid { get; set; }

        /// <summary>
        /// fieldName
        /// </summary>
        public string fieldName { get; set; }

        /// <summary>errorKey
        /// 
        /// </summary>
        public string errorKey { get; set; }

        /// <summary>
        /// errorMessage
        /// </summary>
        public string errorMessage { get; set; }

    }
    /// <summary>
    /// WSWebResponseHeader
    /// </summary>
    public class WSWebResponseHeader
    {
        /// <summary>
        /// result
        /// </summary>
        public string result { get; set; } = string.Empty;

        /// <summary>
        /// uid
        /// </summary>
        public string uid { get; set; } = string.Empty;

        /// <summary>
        /// fieldName
        /// </summary>
        public string fieldName { get; set; } = string.Empty;

        /// <summary>
        /// errorKey
        /// </summary>
        public string errorKey { get; set; } = string.Empty;

        /// <summary>
        /// errorMessage
        /// </summary>
        public string errorMessage { get; set; } = string.Empty;
    }

    /// <summary>
    /// RestResponse
    /// </summary>
    public class RestResponse
    {
        /// <summary>
        /// responseBody
        /// </summary>
        public string responseBody = string.Empty;

        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr = new WSWebResponseHeader();

        /// <summary>
        /// RestResponse
        /// </summary>
        public RestResponse()
        {
            hdr.result = RestConstants.SUCCESSFUL;
        }

    }


    /// <summary>
    /// WSNISUpdateDependencies
    /// </summary>
    public class WSNISUpdateDependencies
    {
        /// <summary>
        /// customerId
        /// </summary>
        public Guid customerId { get; set; }

        /// <summary>
        /// wsaddress
        /// </summary>
        public WSAddress wsaddress { get; set; }
    }

    /// <summary>
    /// WSReportChangeRequest
    /// </summary>
    public class WSReportChangeRequest
    {
        /// <summary>
        /// contactName
        /// </summary>
        public WSName contactName;

        /// <summary>
        /// contactSmsPhone
        /// </summary>
        public string contactSmsPhone;

        /// <summary>
        /// contactEmail
        /// </summary>
        public string contactEmail;
    }

    /// <summary>
    /// 
    /// </summary>
    public static class RestConstants
    {
        #region Constants
        /// <summary>
        /// MEDIA_TYPE_JSON_UTF8
        /// </summary>
        public const string MEDIA_TYPE_JSON_UTF8 = "application/json; charset=UTF-8";

        /// <summary>
        /// AUTHORIZATION_HEADER
        /// </summary>
        public const string AUTHORIZATION_HEADER = "Authorization";

        /// <summary>
        /// AUTHENTICATION_SCHEME_BASIC
        /// </summary>
        public const string AUTHENTICATION_SCHEME_BASIC = "Basic";

        /// <summary>
        /// UID_HEADER
        /// </summary>
        public const string UID_HEADER = "x-cub-uid";

        /// <summary>
        /// DEVICE_HEADER
        /// </summary>
        public const string DEVICE_HEADER = "x-cub-device";
        /// <summary>
        /// APP_ID_HEADER
        /// </summary>
        public const string APP_ID_HEADER = "x-cub-appid";

        /// <summary>
        /// CUB_HEADER
        /// </summary>
        public const string CUB_HEADER = "x-cub-hdr";

        /// <summary>
        /// NEXT_WAVE_API
        /// </summary>
        public const string NEXT_WAVE_API = "NextWaveApi";

        /// <summary>
        /// NEXT_WAVE_API_URL_ROOT
        /// </summary>
        public const string NEXT_WAVE_API_URL_ROOT = "nwapi";

        /// <summary>
        /// CMS_API
        /// </summary>
        public const string CMS_API = "CMSApi";

        /// <summary>
        /// CMS_API_URL_ROOT
        /// </summary>
        public const string CMS_API_URL_ROOT = "cmsapi";

        //response codes
        /// <summary>
        /// SUCCESSFUL
        /// </summary>
        public const string SUCCESSFUL = "Successful";

        /// <summary>
        /// DATA_VALIDATION_ERROR
        /// </summary>
        public const string DATA_VALIDATION_ERROR = "DataValidationError";

        /// <summary>
        /// UNEXPECTED_SYSTEM_ERROR
        /// </summary>
        public const string UNEXPECTED_SYSTEM_ERROR = "UnexpectedSystemError";

        /// <summary>
        /// SERVICE_UNAVAILABLE
        /// </summary>
        public const string SERVICE_UNAVAILABLE = "ServiceUnavailable";

        /// <summary>
        /// AUTHENTICATION_FAILED
        /// </summary>
        public const string AUTHENTICATION_FAILED = "AuthenticationFailed";

        /// <summary>
        /// UNSUCCESSFUL_STATUS_CODE
        /// </summary>
        public const string UNSUCCESSFUL_STATUS_CODE = "Unsuccessful Status Code Returned";

        /// <summary>
        /// FAILED
        /// </summary>
        public const string FAILED = "Failed";

        //Exceptions string

        /// <summary>
        /// SERVICE_UNAVAILABLE_MSG
        /// </summary>
        public const string SERVICE_UNAVAILABLE_MSG = "Service Unavailable. Response returned no headers";

        /// <summary>
        /// TokenTypes
        /// </summary>
        public struct TokenTypes
        {
            /// <summary>
            /// mobile
            /// </summary>
            public const string mobile =  "Mobile";

            /// <summary>
            /// bankCard
            /// </summary>
            public const string bankCard = "Bankcard";

            /// <summary>
            /// smartCard
            /// </summary>
            public const string smartCard = "Smartcard";

            /// <summary>
            /// licensePlate
            /// </summary>
            public const string licensePlate = "LicensePlate";

            /// <summary>
            /// transponder
            /// </summary>
            public const string transponder = "Transponder";

        }
        #endregion Constants
    }


    /// address
    /// <summary>
    /// 
    /// </summary>
    public class WSUpdateDependenciesRequest
    {
        /// <summary>
        /// customerId
        /// </summary>
        public string customerId { get; set; }

        /// <summary>
        /// addressId
        /// </summary>
        public string addressId { get; set; }

        /// <summary>
        /// address
        /// </summary>
        public WSAddress address { get; set; }

        /// <summary>
        /// contactsUpdated
        /// </summary>
        public List<string> contactsUpdated { get; set; } = new List<string>();
    }

    /// <summary>
    /// Class used in NIS Customer Service API call for sending the username to the patron
    /// </summary>
    ///
    public class WSSendUsernameReminder
    {
        /// <summary>
        /// 
        /// </summary>
        public string username { get; set; }
    }

    /// <summary>
    /// Class used in NIS Customer Service call for sending the verification token to the patron
    /// </summary>
    public class WSReportVerificationToken
    {
        /// <summary>
        /// verification token to be sent to the patron
        /// </summary>
        public string verificationToken { get; set; }

        /// <summary>
        /// verification token type (password, mobile, unlockaccount, etc.)
        /// </summary>
        public string verificationType { get; set; }

        /// <summary>
        /// expiration date and time for the token 
        /// </summary>
        public DateTime tokenExpiryDateTime { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class WSCustomerSearchResponse : Model.MSDApi.WSMSD
    {
        /// <summary>
        /// customers
        /// </summary>
        public List<WSCustomerSearchResult> customers { get; set; }

        /// <summary>
        /// totalCount
        /// </summary>
        public int totalCount { get; set; }
    }

    /// <summary>
    /// WS Customer Contract Create Response
    /// </summary>
    public class WSCustomerContactCreateReponse : WSMSD
    {
        /// <summary>
        /// (Required)  Unique identifier of the contact.
        /// </summary>
        public string contactId { set; get; }
    }

    /// <summary>
    /// LinkSubssystemToCustomerAccountRequest
    /// </summary>
    public class WSLinkSubssystemToCustomerAccountRequest
    {
        /// <summary>
        /// subsystemAccountNickname
        /// </summary>
        public string subsystemAccountNickname { get; set; }
    }

    /// <summary>
    /// DelinkSubssystemFromCustomerAccountRequest
    /// </summary>
    public class WSDelinkSubssystemFromCustomerAccountRequest
    {
    }
    /// <summary>
    /// WSCustomerNotificationInfo
    /// </summary>
    public class WSCustomerNotificationInfo
    {
        /// <summary>
        /// notificationId
        /// </summary>
        public int notificationId { get; set; }

        /// <summary>
        /// contactName
        /// </summary>
        public string contactName { get; set; }

        /// <summary>
        /// createDateTime
        /// </summary>
        public DateTime createDateTime { get; set; }

        /// <summary>
        /// notificationReference
        /// </summary>
        public string notificationReference { get; set; }

        /// <summary>
        /// type
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// channel
        /// </summary>
        public string channel { get; set; }

        /// <summary>
        /// recipient
        /// </summary>
        public string recipient { get; set; }

        /// <summary>
        /// subject
        /// </summary>
        public string subject { get; set; }

        /// <summary>
        /// status
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// sendDateTime
        /// </summary>
        public DateTime sendDateTime { get; set; }

        /// <summary>
        /// failDateTime
        /// </summary>
        public DateTime failDateTime { get; set; }

        /// <summary>
        /// allowResend
        /// </summary>
        public bool allowResend { get; set; }

        /// <summary>
        /// globalTxnId
        /// </summary>
        public string globalTxnId { get; set; }

    }

    /// <summary>
    /// LinkSubssystemToCustomerAccountResponse
    /// </summary>
    public class WSLinkSubssystemToCustomerAccountResponse : MSDApi.WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();
    }

    /// <summary>
    /// WSCanBeDelinkedSubssystemFromCustomerAccountResponse
    /// </summary>
    public class WSCanBeDelinkedSubssystemFromCustomerAccountResponse : MSDApi.WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();
        public List<WSError> delinkErrors { get; set; }
    }


    /// <summary>
    /// DelinkSubssystemFromCustomerAccountResponse
    /// </summary>
    public class WSDelinkSubssystemFromCustomerAccountResponse : MSDApi.WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();
    }

    /// <summary>
    /// WSCustomerNotificationResendResponse
    /// </summary>
    public class WSCustomerNotificationResendResponse : MSDApi.WSMSD
    {

        /// <summary>
        /// result
        /// </summary>
        public string result { get; set; }

        /// <summary>
        /// uid
        /// </summary>
        public string uid { get; set; }
    }

    /// <summary>
    /// WSCustomerNotificationPreferencesPatchResponse
    /// </summary>
    public class WSCustomerNotificationPreferencesPatchResponse : MSDApi.WSMSD
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();
    }

    /// <summary>
    /// Customer Notification Preferences PATCH
    /// </summary>
    public class CustomerNotificationPreferencesRequest
    {
        /// <summary>
        /// (Required)  Identifies the customer registering
        /// </summary>
        public string customerId { get; set; }
        
        /// <summary>
        /// (Required)  List of customer contacts to update.
        /// </summary>
        public List<WSCustomerContactNotificationPreferenceUpdate> contacts { get; set; }
    }

    /// <summary>
    /// Response for patching a funding source
    /// 2.8.2 customer/<customerid>/fundingsource
    /// </summary>
    public class WSCustomerFundingSourcePostResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required)  Funding source identifier
        /// </summary>
        public string fundingSourceId  { get; set; }
    }

    /// <summary>
    /// WSCustomerContactNotificationPreferenceUpdate
    /// </summary>
    public class WSCustomerContactNotificationPreferenceUpdate
    {
        /// <summary>
        /// (Required)  The contact to be updated.
        /// </summary>
        public string contactId { get; set; }

        /// <summary>
        /// (Optional)  The contact�s first name to be used for the update. 
        /// The value cannot be removed.
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        ///(Optional)  The contact�s last name to be used for the update.
        ///The value cannot be removed.
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// (Optional)  The contact�s email address to be used for the update. 
        /// The value cannot be removed.
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// (Optional)  The contact�s phone number for receiving text messages.
        /// </summary>
        public string smsPhone { get; set; }

        /// <summary>
        /// (Optional)  True means the contact receives mandatory notifications.  
        /// False or null means the contact does not receive mandatory notifications.
        /// </summary>
        public string mandatory { get; set; }

        /// <summary>
        /// preferences
        /// </summary>
        public List<WSNotificationTypePreferenceUpdate> preferences { get; set; }
    }

    /// <summary>
    /// WSNotificationTypePreferenceUpdate
    /// </summary>
    public class WSNotificationTypePreferenceUpdate
    {
        /// <summary>
        /// notificationType
        /// </summary>
        public string notificationType { get; set; }

        /// <summary>
        /// optIn
        /// </summary>
        public bool optIn { get; set; }

        /// <summary>
        /// channels
        /// </summary>
        public List<WSNotificationChannelPreference> channels { get; set; }
    }

    /// <summary>
    /// WSNotificationChannelPreference
    /// </summary>
    public class WSNotificationChannelPreference
    {
        /// <summary>
        /// channel
        /// </summary>
        public string channel { get; set; }

        /// <summary>
        /// enabled
        /// </summary>
        public bool enabled { get; set; }
    }

    /// <summary>
    /// WSKeyValue
    /// </summary>
    public class WSKeyValue : MSDApi.WSMSD
    {
        /// <summary>
        /// key
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// value
        /// </summary>
        public string value { get; set; }
    }

    /// <summary>
    /// WSPurseExt
    /// </summary>
    public class WSPurseExt : MSDApi.WSMSD
    {
        /// <summary>
        /// purseId
        /// </summary>
        public int purseId { get; set; }

        /// <summary>
        /// nickName
        /// </summary>
        public string nickName { get; set; }

        /// <summary>
        /// balance
        /// </summary>
        public int balance { get; set; }

        /// <summary>
        /// purseStatus
        /// </summary>
        public WSKeyValue purseStatus { get; set; }
    }

    /// <summary>
    /// OnAccount Balance History Response object
    /// </summary>
    public class OneAccountBalanceHistoryResponse : WSMSD
    {
        /// <summary>
        /// header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required)  The total number of results, this is used to calculate the total number of pages
        /// </summary>
        public int totalCount { get; set; } = 0;

        /// <summary>
        /// (Required)  The list of balance history line items
        /// </summary>
        public List<WSBalanceHistoryLineItem> lineItems { get; set; } = new List<WSBalanceHistoryLineItem>();
    }

    /// <summary>
    /// Balance History Line Items
    /// </summary>
    public class WSBalanceHistoryLineItem
    {
        /// <summary>
        /// (Required) Unique id for the journal entry
        /// </summary>
        public int journalEntryId { get; set; }

        /// <summary>
        /// (Required) The date and time when the journal entry occurred
        /// </summary>
        public string entryDateTime { get; set; }

        /// <summary>
        /// (Required) The unique identifier of the purse the line item occurred in
        /// </summary>
        public int purseId { get; set; }

        /// <summary>
        /// string(30) (Required) The nickname of the purse
        /// </summary>
        public string purseNickname { get; set; }

        /// <summary>
        /// string(20) (Required) The unique identifier of the entry type
        /// </summary>
        public string entryType { get; set; }

        /// <summary>
        /// string(30)(Required) The description of entry type
        /// </summary>
        public string entryTypeDescription { get; set; }

        /// <summary>
        /// string(20) (Optional) The unique identifier of the entry sub type 
        /// (for charge it is the charge type, for loads it is the load type)
        /// </summary>
        public string entrySubType { get; set; }

        /// <summary>
        /// string(30) (Optional) The description of entry sub type
        /// </summary>
        public string entrySubTypeDescription { get; set; }

        /// <summary>
        /// string(20) (Optional) The unique identifier of the entry status
        /// </summary>
        public string entryStatus { get; set; }

        /// <summary>
        /// string(30) (Optional) The description of entry status
        /// </summary>
        public string entryStatusDescription { get; set; }

        /// <summary>
        /// (Required) The journal entry amount in cents
        /// </summary>
        public int journalEntryAmount { get; set; }

        /// <summary>
        /// (Required) The available balance of the purse in cents, after this transaction.  
        /// The available balance is the actual purse balance minus any pending authorizations
        /// </summary>
        public int availableBalance { get; set; }

        /// <summary>
        /// (Required) The ending balance of the purse in cents, after this transaction.  
        /// This is the balance based on confirmed charges
        /// </summary>
        public int endingBalance { get; set; }
    }

    /// <summary>
    /// TransitAccountTravelHistoryResponse
    /// </summary>
    public class TransitAccountTravelHistoryResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required)  The total number of results, this is used to calculate the total number of pages
        /// </summary>
        public int totalCount { get; set; } = 0;

        /// <summary>
        /// (Required)  The list of trip history line items
        /// </summary>
        public List<WSTransitTripHistoryLineItem> lineItems { get; set; } = new List<WSTransitTripHistoryLineItem>();
    }


    ///
    /// Generic MVC response
    ///
    public class MvcGenericResponse : WSMSD
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader Header { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// Body from the NIS JSON response
        /// </summary>
        public string Body { get; set; }
    }

    /// <summary>
    /// WSTransitTripHistoryLineItem
    /// </summary>
    public class WSTransitTripHistoryLineItem
    {
        /// <summary>
        /// string(20) (Required)  Unique id for the trip history transaction
        /// </summary>
        public string tripId { get; set; }

        public string journeyId { get; set; }

        public string @operator { get; set; }

        public string operatorDescription { get; set; }

        /// <summary>
        /// (Conditionally-Required)  The date and time when the travel started. 
        /// Either a startDateTime or endDateTime is always returned
        /// </summary>
        public string startDateTime { get; set; }

        /// <summary>
        /// (Conditionally-Required)  The date when the travel ended. 
        /// Either a startDateTime or endDateTime is always returned
        /// </summary>
        public string endDateTime { get; set; }

        /// <summary>
        /// string(30) (Conditionally-Required)  The travel starting location. 
        /// Either a startLocation or endLocation is always returned
        /// </summary>
        public string startLocationDescription { get; set; }

        /// <summary>
        /// string(30) (Conditionally-Required)  The travel ending location. 
        /// Either a startLocation or endLocation is always returned
        /// </summary>
        public string endLocationDescription { get; set; }

        /// <summary>
        /// string(20) (Required)  The unique reference to the mode of travel
        /// </summary>
        public string travelMode { get; set; }

        /// <summary>
        /// string(100) (Required)  Description of travel mode
        /// </summary>
        public string travelModeDescription { get; set; }

        /// <summary>
        /// (Required)  Token used for the travel
        /// </summary>
        public WSTravelTokenDisplay token { get; set; }

        /// <summary>
        /// string(100) (Optional)  Description of the product used to pay for the services
        /// </summary>
        public string productDescription { get; set; }

        /// <summary>
        /// (Required)  The total amount of the fare charged to the customer in cents
        /// </summary>
        public int totalFare { get; set; }

        /// <summary>
        /// (Required)  Amount of the fare that has not yet been paid for in cents
        /// </summary>
        public int unpaidFare { get; set; }

        /// <summary>
        /// (Optional)  The unique identifier of the trip type
        /// </summary>
        public string tripType { get; set; }

        /// <summary>
        /// (Optional) Description of trip type
        /// </summary>
        public string tripTypeDescription { get; set; }

        /// <summary>
        /// (Optional) The unique id of the trip status
        /// </summary>
        public string tripStatus { get; set; }

        /// <summary>
        /// (Optional) Description of the trip status
        /// </summary>
        public string tripStatusDescription { get; set; }

        /// <summary>
        /// (Required) The date time when the trip status changed 
        /// </summary>
        public string tripStatusDateTime { get; set; }

        /// <summary>
        /// (Required) List of indicators on the trip based on the travel presence events within the trip (incomplete, auto-filled, manual-filled, complete) 
        /// </summary>
        public List<string> travelPresenceIndicator { get; set; } = new List<string>();

        /// <summary>
        /// (Required)  Flag to indicate if this travel transaction can be corrected
        /// </summary>
        public bool isCorrectable { get; set; }

        public string riderClass { get; set; }

        public string riderClassDescription { get; set; }

        public string timeCategory { get; set; }

        public string timeCategoryDescription { get; set; }

        public string concession { get; set; }

        public string concessionDescription { get; set; }

        /// Leaving these in case they decide to add them back in per Anne's design
        /// <summary>
        /// (Conditionally Required) The date time when the pass was activated.  Only specified for time based passes for card and account based systems
        /// </summary>
        //public string activationDateTime { get; set; }

        /// <summary>
        /// (Conditionally required) Number of days remaining on the pass. 
        /// </summary>
        //public int daysRemaining { get; set; }

        /// <summary>
        /// (Conditionally required) Number of rides remaining on the pass. 
        /// </summary>
        //public int ridesRemaining { get; set; }

        /// <summary>
        /// (Conditionally required) Remaining stored value in cents 
        /// </summary>
        //public int valueRemaining { get; set; }

    }

    /// <summary>
    /// Travel History Account Travel History Taps Response
    /// </summary>
    public class TransitAccountTravelHistoryTapResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// The body from the NIS response
        /// </summary>
        public string Body { get; set; }
    }

    /// <summary>
    /// OneAccount Travel History Response
    /// </summary>
    public class OneAccountTravelHistoryResponse : WSMSD
    {
        /// <summary>
        /// header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required)  The total number of results, this is used to calculate the total number of pages
        /// </summary>
        public int totalCount { get; set; } = 0;

        /// <summary>
        /// (Required)  The list of travel history line items
        /// </summary>
        public List<WSTripHistoryLineItem> lineItems { get; set; } = new List<WSTripHistoryLineItem>();
    }

    /// <summary>
    /// Order DebtCollect Request
    /// </summary>
    public class WSOrderDebtCollectRequest
    {
        /// <summary>
        /// (Conditionally-Required)  Unique identifier for the customer. Required for adjustments on One Account and all registered customers.
        /// </summary>
        public string customerId { get; set; }

        /// <summary>
        /// (Optional)  If the customer is not registered (i.e. customerId is not provided), the email address to send order notifications.
        /// If customerId is specified, this field will be ignored.
        /// </summary>
        public string unregisteredEmail { get; set; }

        /// <summary>
        /// (Required)  The client�s unique identifier for sending this notification.
        /// </summary>
        public string clientRefId { get; set; }

        /// <summary>
        /// (Required) Line items for the debt recovery requests.
        /// </summary>
        public List<WSDebtCollectLineItemBankcard> collectionLineItems { get; set; } = new List<WSDebtCollectLineItemBankcard>(); 

    }

    /// <summary>
    /// WSDebtCollectLineItem - Line item for the debt recovery requests
    /// </summary>
    public class WSDebtCollectLineItemBankcard
    {
        public WSSubsystemDebtCollectionBankcard collection { get; set; }

    }

    public class WSDebtCollectionInfo
    {
        /// <summary>
        /// (Required) Specifies the possible action for recovering outstanding debt on this account.
        /// </summary>
        public string action { get; set; }

        /// <summary>
        /// (Required) The amount due.
        /// </summary>
        public int amountDue { get; set; }

        /// <summary>
        /// (Conditionally-Required) When action is Available or AvailableWait, indicates the bankcard that will be charged to collect the outstanding debt.
        /// </summary>
        public WSDebtCollectionBankcard bankcardCharged { get; set; }

        /// <summary>
        /// (Conditionally-Required) When action is AvailableWait, indicates how much time must elapse before the next attempt to collect the outstanding debt can be made.
        /// </summary>
        public int attemptInSeconds { get; set; }

        /// <summary>
        /// (Conditionally-Required) When action is AvailableWait, indicates how much attempts are available for the day to collect the outstanding debt.
        /// </summary>
        public int remainingAttemptsForDay { get; set; }
    }

    /// <summary>
    /// WSDebtCollectionBankcard
    /// </summary>
    public class WSDebtCollectionBankcard
    {
        /// <summary>
        /// (Required) The masked PAN. The masking format is project specific, but is commonly just the last 4 digits of the PAN.
        /// </summary>
        public string maskedPan { get; set; }

        /// <summary>
        /// (Required) The card expiry date in MMYY format. For MM, 01 = January.
        /// </summary>
        public string cardExpiryMMYY { get; set; }
    }

    /// <summary>
    /// WSDebtCollectLineItem - Line item for the debt recovery requests
    /// </summary>
    public class WSDebtCollectLineItem
    {
        public WSDebtCollection collection { get; set; }

    }

    /// <summary>
    /// WSDebtCollection - The WSDebtCollection is the base class for debt collection requests.
    /// </summary>
    public class WSDebtCollection
    {
        /// <summary>
        /// (Required) Determines the type of debt collection line item. 
        /// </summary>
        public string collectionLineItemType { get; set; }
    }

    /// <summary>
    /// WSSubsystemDebtCollectionBankcard is a subclass of WSDebtCollection. 
    /// This class is used to collect debt from a bankcard token in a subsystem account. Expected to set the collectionLineItemType as �SubsystemDebtCollectionBankcard�.
    /// </summary>
    public class WSSubsystemDebtCollectionBankcard : WSDebtCollection
    {
        /// <summary>
        /// (Required) Unique identifier of the subsystem.
        /// </summary>
        public string subsystem { get; set; }

        /// <summary>
        /// (Required) Unique identifier for the customer�s account in the subsystem
        /// </summary>
        public string subsystemAccountReference { get; set; }
    }

    /// <summary>
    /// Order DebtCollect Response
    /// </summary>
    public class WSOrderDebtCollectResponse : WSMSD
    {
        /// <summary>
        /// header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required)  The unique order identifier from Order Management System (OMS).
        /// </summary>
        public string orderId { get; set; }

        /// <summary>
        /// (Required)  The order result.	OrderAccepted 	FulfillmentFailed	Completed	CompletedWithErrors
        /// </summary>
        public string responseCode { get; set; }

        /// <summary>
        /// (Optional)  A unique identifier for the order payment
        /// </summary>
        public string paymentRefNbr { get; set; }

        /// <summary>
        /// (Required)  List of payment results including payment information submitted in the authorize payment request
        /// </summary>
        public List<WSPaymentResult> paymentResults { get; set; } = new List<WSPaymentResult>();

        /// <summary>
        /// (Optional)  A list of errors encountered during order processing
        /// </summary>
        public List<WSOrderError> errors { get; set; } = new List<WSOrderError>();
    }

    /// <summary>
    /// WSOrderError - Holds error information encountered when processing a patron order
    /// </summary>
    public class WSOrderError
    {
        /// <summary>
        /// (Optional) A key describing the error.
        /// </summary>
        public string errorKey { get; set; }

        /// <summary>
        /// (Optional) A string describing the error.
        /// </summary>
        public string errorMessage { get; set; }

    }

    /// <summary>
    /// WSPaymentResult - This object represents the results and payment information of a payment authorization requested by the system for a customer order.
    /// </summary>
    public class WSPaymentResult
    {
        /// <summary>
        /// (Required) Only populated for payments authorized or attempted to authorize.
        /// </summary>
        public string responseCode { get; set; }

        /// <summary>
        /// (Conditionally-Required)  A unique identifier for this authorization.  Only populated for payments authorized or attempted to authorize.
        /// </summary>
        public string authRefNbr { get; set; }

        /// <summary>
        /// (Conditionally-Required)  The 6 character authorization code returned by the bank. Only populated for credit card payments.
        /// </summary>
        public string bankAuthCode { get; set; }

        /// <summary>
        /// (Conditionally-Required)  The date/time the payment was authorized. This value will not be returned in the event that a prior failure means this authorization was not attempted.
        /// </summary>
        public string authDateTime { get; set; }

        /// <summary>
        /// (Required)  The credit/debit or transit account payment associated with the authorize payment result.
        /// </summary>
        public WSPayment payment { get; set; }
    }

    /// <summary>
    /// WSPayment - This object represents the payments being done by the system for an order.
    /// </summary>
    public class WSPayment
    {
        /// <summary>
        /// (Required) indicates the type of payment. Refer to Section 2.6.8 for allowed values
        /// </summary>
        public string paymentType { get; set; }

        /// <summary>
        /// (Required) The payment amount. This number is the number of the base unit of currency, such as cents.
        /// </summary>
        public int paymentAmount { get; set; }

    }

    /// <summary>
    /// WSCreditDebitPayment - This object extends WSPayment and represents a credit/debit card payment.
    /// </summary>
    public class WSCreditDebitPayment : WSPayment
    {
        /// <summary>
        /// (Required) The token representing the credit card to be authorized
        /// </summary>
        public string pgCardId { get; set; }

        /// <summary>
        /// (Optional) The type of credit card. See section 2.6.2 for valid values
        /// </summary>
        public string creditCardType { get; set; }

        /// <summary>
        /// (Optional) The masked PAN.
        /// </summary>
        public string maskedPan { get; set; }

        /// <summary>
        /// (Optional) Customer Verification Value.
        /// </summary>
        public string cvv { get; set; }

        /// <summary>
        /// (Required) Point of service entry mode. Indicates the actual card data entry mode and the terminal capability
        /// </summary>
        public int pointOfEntryMode { get; set; }

        /// <summary>
        /// (Required) The merchant who processed the credit or debit card payment.
        /// </summary>
        public string merchantId { get; set; }

        /// <summary>
        /// (Required) Must be one of:  ECOMMERCE RECURRING MOTO(Mail Order/Telephone Order) IVR
        /// </summary>
        public string transactionType { get; set; }

    }

    /// <summary>
    /// WSAccountValuePayment - This object extends WSPayment when paymentType is StoredValue and represents a payment using value from an account (OneAccount or Subsystem).
    /// </summary>
    public class WSAccountValuePayment : WSPayment
    {
        /// <summary>
        /// Type of the account:	OneAccount	SubsystemAccount
        /// </summary>
        public string accountType { get; set; }


    }

    /// <summary>
    /// WSOneAccountValuePayment - This object extends WSAccountValuePayment and represents a OneAccount purse value payment.
    /// </summary>
    public class WSOneAccountValuePayment : WSAccountValuePayment
    {
        /// <summary>
        /// (Required) Unique identifier for the customer�s One Account
        /// </summary>
        public int oneAccountId { get; set; }

        /// <summary>
        /// (Required)  Unique identifier for the customer�s One Account purse being used for the payment.
        /// </summary>
        public int purseId { get; set; }

        /// <summary>
        /// (Required)  The nickname for this purse. This is required so that it can be displayed on receipts.
        /// </summary>
        public string nickname { get; set; }

    }

    /// <summary>
    /// WSSubsystemValuePayment - This object extends WSAccountValuePayment and represents a Subsystem purse value payment.
    /// </summary>
    public class WSSubsystemValuePayment : WSAccountValuePayment
    {
        /// <summary>
        /// (Required) Unique identifier for the subsystem where the travel token is registered.
        /// </summary>
        public string subsystem { get; set; }

        /// <summary>
        /// (Required) Unique identifier for the customer�s account in the subsystem
        /// </summary>
        public string subsystemAccountReference { get; set; }

        /// <summary>
        /// (Required)  Type of purse it is. 
        /// </summary>
        public string purseType { get; set; }

        /// <summary>
        /// (Required)  Description of the purse type.
        /// </summary>
        public string purseTypeDescription { get; set; }

        /// <summary>
        /// (Required)  Type of the purse restriction.
        /// </summary>
        public string purseRestriction { get; set; }

        /// <summary>
        /// (Required)  Description of the purse restriction
        /// </summary>
        public string purseRestrictionDescription { get; set; }

        /// <summary>
        /// (Optional)  The nickname for this purse.
        /// </summary>
        public string nickname { get; set; }

    }

    /// <summary>
    /// Travel History Line Item
    /// </summary>
    public class WSTripHistoryLineItem
    {
        /// <summary>
        /// (Required)  Unique id for the travel history transaction
        /// </summary>
        public string tripId { get; set; }

        /// <summary>
        /// (Conditionally-Required)  The date and time when the travel started. 
        /// Either a startDateTime or endDateTime is always returned
        /// </summary>
        public string startDateTime { get; set; }

        /// <summary>
        /// (Conditionally-Required)  The date when the travel ended. 
        /// Either a startDateTime or endDateTime is always returned
        /// </summary>
        public string endDateTime { get; set; }

        /// <summary>
        /// (Conditionally-Required)  The travel starting location. 
        /// Either a startLocation or endLocation is always returned
        /// </summary>
        public string startLocationDescription { get; set; }

        /// <summary>
        /// (Conditionally-Required)  The travel ending location. 
        /// Either a startLocation or endLocation is always returned
        /// </summary>
        public string endLocationDescription { get; set; }

        /// <summary>
        /// (Required)  The unique identifier of the travel authority
        /// </summary>
        public int authorityId { get; set; }

        /// <summary>
        /// (Required)  The name of the travel authority
        /// </summary>
        public string authorityName { get; set; }

        /// <summary>
        /// (Required)  The unique reference to the mode of travel
        /// </summary>
        public string travelMode { get; set; }

        /// <summary>
        /// (Required)  Description of travel mode
        /// </summary>
        public string travelModeDescription { get; set; }

        /// <summary>
        /// (Required) Unique identifier of the subsystem where travel occured
        /// </summary>
        public string subsystem { get; set; }

        /// <summary>
        /// (Required) Unique identifier for the account in the subsystem on which travel occured
        /// </summary>
        public string subsystemAccountReference { get; set; }

        /// <summary>
        /// (Required) Nickname of the subsystem account as in One Account
        /// </summary>
        public string subsystemAccountNickname { get; set; }

        /// <summary>
        /// (Required)  Token used for the travel
        /// </summary>
        public WSTravelTokenDisplay token { get; set; }

        /// <summary>
        /// (Optional)  Description of the product used to pay for the services
        /// </summary>
        public string productDescription { get; set; }

        /// <summary>
        /// (Required)  The total amount of the fare charged to the customer in cents
        /// </summary>
        public int totalFare { get; set; }

        /// <summary>
        /// (Required)  Amount of the fare that has not yet been paid for in cents
        /// </summary>
        public int unpaidFare { get; set; }

        /// <summary>
        /// (Optional) The unique id of the trip type
        /// </summary>
        public string tripType { get; set; }

        /// <summary>
        /// (Optional) The description of the trip type
        /// </summary>
        public string tripTypeDescription { get; set; }

        /// <summary>
        /// (Optional) The unique id of the status
        /// </summary>
        public string tripStatus { get; set; }

        /// <summary>
        /// (Optional) The description of the trip status
        /// </summary>
        public string tripStatusDescription { get; set; }

        /// <summary>
        /// (Optional) The date time when the trip status changed
        /// </summary>
        public string tripStatusDateTime { get; set; }

        /// <summary>
        /// (Optional) List of indicators on the trip based on travel presence events
        /// </summary>
        public List<string> travelPresenceIndicator { get; set; } = new List<string>();

        /// <summary>
        /// (Required)  Flag to indicate to indicate if the travel transaction can be corrected
        /// </summary>
        public bool isCorrectable { get; set; }

    }

    /// <summary>
    /// Return response to get the balance history details of an account based on a journal entry ID
    /// 2.10.3 oneaccount/<oneaccount-id>/journalentry/<journalentry-id>/balancehistorydetail GET
    /// </summary>
    public class OneAccountBalanceHistoryDetailResponse : WSMSD
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required)  The date and time when the journal entry occurred
        /// </summary>
        public string entryDateTime { get; set; }

        /// <summary>
        /// (Required) The unique identifier of the purse the line item occurred in.
        /// </summary>
        public string purseId { get; set; }

        /// <summary>
        /// (Required)  The nickname of the purse for which the journal entry was applied
        /// </summary>
        public string purseNickname { get; set; }

        /// <summary>
        /// (Required) The unique identifier of the entry type
        /// </summary>
        public string entryType { get; set; }

        /// <summary>
        /// (Required) The description of entry type
        /// </summary>
        public string entryTypeDescription { get; set; }

        /// <summary>
        /// (Optional) The unique identifier of the entry status
        /// </summary>
        public string entryStatus { get; set; }

        /// <summary>
        /// (Optional) The description of entry status
        /// </summary>
        public string entryStatusDescription { get; set; }

        /// <summary>
        /// (Required) The journal entry amount in cents
        /// </summary>
        public int journalEntryAmount { get; set; } = 0;

        /// <summary>
        /// Required) The available balance of the purse in cents, after this transaction.  
        /// The available balance is the actual purse balance minus any pending authorizations
        /// </summary>
        public int availableBalance { get; set; } = 0;

        /// <summary>
        /// (Required) The ending balance of the purse in cents, after this transaction. 
        /// This is the balance based on confirmed charges
        /// </summary>
        public int endingBalance { get; set; } = 0;

        /// <summary>
        /// (Required)  The financial transaction that originated to the journal entry in question
        /// </summary>
        public WSFinancialTransaction financialTransaction { get; set; }

        /// <summary>
        /// (Required)  The total allocated amount in cents for this journal entry
        /// </summary>
        public int allocatedAmount { get; set; } = 0;

        /// <summary>
        /// (Required)  The total unallocated amount in cents for this journal entry
        /// </summary>
        public int unallocatedAmount { get; set; } = 0;

        /// <summary>
        /// (Required)  The list of journal allocations (credit or debit allocations) for this journal entry
        /// </summary>
        public List<WSJournalAllocation> journalAllocations { get; set; }
    }


    /// <summary>
    /// A financial transaction is the result of the original external system request and may be posted 
    /// to the purses as multiple journal entries
    /// </summary>
    public class WSFinancialTransaction
    {
        /// <summary>
        /// (Required)  Identifier for the load transaction
        /// </summary>
        public int financialTransactionId { get; set; }

        /// <summary>
        /// (Required)  Type of financial transaction:
        ///     PurseLoad
        ///     Charge
        ///     PurseAdjustment
        /// </summary>
        public string financialTransactionType { get; set; }

        /// <summary>
        /// (Required) A unique reference number for the authorization of funds
        /// </summary>
        public string authorizationReferenceNumber { get; set; }

        /// <summary>
        /// (Required)  An identifier for the external action that initiated the financial transaction. 
        /// For example, the OMS order line item Id
        /// </summary>
        public string externalReferenceNumber { get; set; }

        /// <summary>
        /// (Required)  The timestamp for the original transaction that resulted in this financial transaction. 
        /// For example, timestamp for a use or timestamp for the load fulfillment
        /// </summary>
        public string transactionDateTime { get; set; }

        /// <summary>
        /// (Optional)  A description of the transaction. In case of Use or a travel transaction, 
        /// it should include information relevant to the travel mode and location of the transaction 
        /// (entry, exit, tolling point, etc)
        /// </summary>
        public string transactionDescription { get; set; }

        /// <summary>
        /// (Required)  The amount in cents requested for this transaction by the external system
        /// </summary>
        public int transactionAmount { get; set; }
    }

    /// <summary>
    /// This object represents the journal entry allocation (debit or credit allocation) in relation to other journal entries in the same purse
    /// </summary>
    public class WSJournalAllocation
    {
        /// <summary>
        /// (Required)  The unique reference to the credit or debit journal allocated to a given journal entry
        /// </summary>
        public int journalEntryId { get; set; }

        /// <summary>
        /// (Required)  The allocated journal entry allocated amount to the journal entry in question
        /// </summary>
        public int allocatedAmount { get; set; }

        /// <summary>
        /// (Required)  The allocated journal entry total amount
        /// </summary>
        public int entryAmount { get; set; }

        /// <summary>
        /// (Required)  The allocated journal entry type. E.g. Load or Charge
        /// </summary>
        public string entryType { get; set; }

        /// <summary>
        /// (Required)  The description of allocated journal entry type. E.g. Load or Charge
        /// </summary>
        public string entryTypeDescription { get; set; }

        /// <summary>
        /// (Required)  The date/time on which the allocated journal entry was posted to the account purse
        /// </summary>
        public string entryDateTime { get; set; }

        /// <summary>
        /// (Required)  The financial transaction that originated the allocated journal entry 
        /// </summary>
        public WSFinancialTransaction financialTransaction { get; set; }
    }

    /// <summary>
    /// Return response to get the travel history details of an account based on a journal entry ID
    /// 2.14.3 oneaccount/<oneaccount-id>/journalentry/<journalentry-id>/balancehistorydetail GET - 10/5/17 - major changes made to this api!!!
    /// </summary>
    public class TransitAccountTravelHistoryDetailResponse : WSMSD
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required) The information of the trip.
        /// </summary>
        public WSTransitTripHistoryLineItem tripInfo { get; set; }

        /// <summary>
        /// (Required) The capture of the traveler�s presence events for the given trip.
        /// </summary>
        public List<WSTransitTravelPresence> travelPresenceEvents { get; set; }

        /// <summary>
        /// (Optional)  List of payments for the given travel transaction.
        /// </summary>
        public List<WSTravelPayment> tripPayments { get; set; }

    }

    /// <summary>
    /// One Account Travel History Detail
    /// </summary>
    public class OneAccountTravelHistoryDetailResponse : WSMSD
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// (Required) The information of the trip.
        /// </summary>
        public WSTripHistoryLineItem tripInfo { get; set; }

        /// <summary>
        /// (Required) The capture of the traveler�s presence throughout the given travel.
        /// </summary>
        public List<WSTravelPresence> travelPresenceEvents { get; set; }

        /// <summary>
        /// (Optional)  List of payments for the given travel transaction.
        /// </summary>
        public List<WSTravelPayment> tripPayments { get; set; }
    }

    /// <summary>
    /// WSTransitTravelPresence extends WSTravelPresence 
    /// </summary>
    public class WSTransitTravelPresence : WSTravelPresence
    {
       
        /// <summary>
        /// string(30) (Required)  Unique identifier of the device that detected the travel presence.
        /// </summary>
        public string deviceId { get; set; }

        /// <summary>
        /// string(100) (Required)  A description of the transaction.
        /// It should include information relevant to the travel mode and location 
        /// of the transaction(entry, exit, tolling point, etc)
        /// </summary>
        public string transactionDescription { get; set; }

        /// <summary>
        /// string(20) (Required)  The status of the transaction.
        /// </summary>
        public string transactionStatus { get; set; }

        /// <summary>
        /// string(100) (Required)  A description of the transaction status.
        /// </summary>
        public string transactionStatusDescription { get; set; }

        /// <summary>
        /// string(40) (Optional)  Stop Id or Code where the use transaction was 
        /// captured. The stopPoint here is the starting location (entry tap).
        /// </summary>
        public string stopPoint { get; set; }

        /// <summary>
        /// string(20) (Optional)  Route Id or Code where the use transaction was captured.
        /// </summary>
        public string routeNumber { get; set; }

        /// <summary>
        /// string(20) (Optional)  Zone Id or Code where the use transaction was captured.
        /// </summary>
        public string zone { get; set; }

        /// <summary>
        /// string(40) (Optional)  Inbound or Outbound Direction.
        /// </summary>
        public string direction { get; set; }

        /// <summary>
        /// string(20) (Optional)  Sector Id or Code where the use transaction was captured.
        /// </summary>
        public string sector { get; set; }
        /// <summary>
        /// string(20) (Optional)  Service type to be used for pricing.
        /// </summary>
        public string serviceType { get; set; }

        /// <summary>
        /// WSGPSCoordinate (Optional)  Global positioning coordinates where the use was detected.
        /// </summary>
        public WSGPSCoordinate gpsCoordinate { get; set; }
     
    }

    /// <summary>
    /// the gps class with longitude and latitude
    /// </summary>
    public class WSGPSCoordinate
    {
        public WSGPSCoordinate (double longitude, double latitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }

        /// <summary>
        /// the universal latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// The universal longitude
        /// </summary>
        public double Longitude { get; set; }
    }

    /// <summary>
    /// WSTransitTravelAccountPass 
    /// </summary>
    public class WSTransitAccountPass
    {
        /// <summary>
        /// (Required) The unique identifier of the pass.
        /// </summary>
        public string passSKU { get; set; }

        /// <summary>
        /// (Required) A textual description of the passSKU field.
        /// </summary>
        public string passDescription { get; set; }

        /// <summary>
        /// (Optional) Time the pass was created.
        /// </summary>
        public string createdDateTime { get; set; }

        /// <summary>
        /// (Optional) The date and time that starts the validity period of the pass.
        /// </summary>
        public string startDateTime { get; set; }

        /// <summary>
        /// (Optional) The date and time that ends the validity period of the pass.
        /// </summary>
        public string endDateTime { get; set; }

        /// <summary>
        /// (Optional) Duration of the pass.
        /// </summary>
        public int? remainingDuration { get; set; }

        /// <summary>
        /// (Optional) Unit type for the duration field.
        /// Uses
        /// Days
        /// Hours
        /// Minutes
        /// </summary>
        public string durationType { get; set; }

        /// <summary>
        /// (Required) The date and time when the pass expires even if not used.
        /// </summary>
        public string expirationDateTime { get; set; }

        /// <summary>
        /// (Optional) The date and time the pass was deactivated.
        /// </summary>
        public string deactivatedDateTime { get; set; }

        /// <summary>
        /// (Optional) Reason the pass was deactivated.
        /// </summary>
        public string deactivatedReason { get; set; }

        /// <summary>
        /// (Required) The unique identifier for the pass instance. 
        /// </summary>
        public string passSerialNbr { get; set; }
        
        /// <summary>
        /// (Required) The fare product type.
        /// </summary>
        public string fareProductTypeId { get; set; }
        
        /// <summary>
        /// (Required) Indicates whether the product may be used with Autoload.
        /// </summary>
        public bool supportsAutoload { get; set; }
    }

    /// <summary>
    /// object for where the traveler's presence was noted - 10/5/17 - modified for changes to API
    /// </summary>
    public class WSTravelPresence
    {
        /// <summary>
        /// boolean (Required)  type of travel presence
        /// </summary>
        public string objectType { get; set; }

        /// <summary>
        /// string(20) (Required)  Unique identifier for the travel presence event
        /// </summary>
        public string travelPresenceId { get; set; }  // 10/5/17 - changed from id

        /// <summary>
        /// boolean (Required)  The date when the traveler's presence was captured
        /// </summary>
        public string transactionDateTime { get; set; }

        /// <summary>
        /// string(20) (Required)  Type of travel presence. This value should be used to determine 
        /// sub-classes when they c?
        /// Valid values are:
        /// TransitTravel
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// (Required)  The description of the travel presence type
        /// </summary>
        public string typeDescription { get; set; }

        /// <summary>
        /// (Required)  The location where the traveler's presence was captured
        /// </summary>
        public string locationDescription { get; set; }

        /// <summary>
        /// (Required)  Unique identifier of the operator
        /// </summary>
        public string @operator { get; set; }

        /// <summary>
        /// (Required)  Description of the operator
        /// </summary>
        public string operatorDescription { get; set; }

        /// <summary>
        /// (Required)  Token used to travel
        /// </summary>
        public WSTravelTokenDisplay token { get; set; }

        /// <summary>
        /// (Required)  The unique identifier of the travel presence event status
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// (Required)  Description of travel presence event status
        /// </summary>
        public string statusDescription { get; set; }

        /// <summary>
        /// (Required)  The unique identifier of the cause of the travel presence event
        /// </summary>
        public string generatedCause { get; set; }

        /// <summary>
        /// (Required)  Description of the cause of the travel presence event
        /// </summary>
        public string generatedCauseDescription { get; set; }

        /// <summary>
        /// (Required)  Indicator of the travel presence event
        /// </summary>
        public string travelPresenceIndicator { get; set; }

        /// <summary>
        /// boolean (Required)  Flag to indicate if this travel presence record can be corrected
        /// </summary>
        public bool isCorrectable { get; set; }
    }

    /// <summary>
    /// This object represents a payment made for traveling
    /// </summary>
    public class WSTravelPayment
    {
        /// <summary>
        /// (Required) Unique identifier for the payment transaction.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// (Required) Timestamp when the paid fare portion was paid by the source system. 
        /// </summary>
        public string paymentDateTime { get; set; }

        /// <summary>
        /// (Required) Amount in cents for the fare portion paid by this travel payment. 
        /// </summary>
        public int paidFare { get; set; }

        /// <summary>
        /// (Optional) Description of the product used to pay for the services if any.
        /// </summary>
        public string productDescription { get; set; }

        /// <summary>
        /// (Required) Either the authority associated with the product used or One Account.
        /// Examples: NYCT, Ventra, One Account.
        /// </summary>
        public string payingSystemDescription { get; set; }

        public string purseDescription { get; set; }

        public List<WSTravelPaymentSource> paymentSource { get; set; }
    }

    /// <summary>
    /// This object represents a payment source used to make a payment for travel.
    /// </summary>
    public class WSTravelPaymentSource
    {
        /// <summary>
        /// (Required) Unique identifier for the payment source transaction.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// (Required) Timestamp when load was posted to the account. 
        /// </summary>
        public string dateTime { get; set; }

        /// <summary>
        /// (Required) Amount in cents for the load in question. 
        /// </summary>
        public int amount { get; set; }

        /// <summary>
        /// (Optional) If available, the load type: One-Time, Recurring, Adjustment, etc.
        /// </summary>
        public string paymentSourceType { get; set; }

        /// <summary>
        /// (Required) The payment type. See valid values in section 2.6.8.
        /// </summary>
        public string paymentType { get; set; }

        /// <summary>
        /// (Required) The payment type description. See valid values in section 2.6.8
        /// </summary>
        public string paymentTypeDescription { get; set; }

        /// <summary>
        /// (Optional) The unique identifier for the payment that funded the purse credit.
        /// </summary>
        public string paymentReferenceNumber { get; set; }

        /// <summary>
        /// (Optional) The unique identifier for this authorization.
        /// </summary>
        public string authReferenceNumber { get; set; }

        /// <summary>
        /// (Optional)  The display string for the funding source details.
        /// Eg., If paid by Credit/Debit card, then the details will be credit card type and masked PAN(VISA    4111******1234).
        /// If paid by ACH, then the details will be bank name, masked bank a/c number(Bank of America******2344) Etc.
        /// </summary>
        public string fundingSourceInfo { get; set; }
    }


    /// <summary>
    /// Used by 2.8.16 customer/<customer-id>/notificationpreferences GET
    /// Response contains header and contacts
    /// </summary>
    public class CustomerNotificationPreferencesResponse : WSMSD
    {
        /// <summary>
        /// Header
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();
        /// <summary>
        /// List<WSCustomerContactNotificationPreferenceInfo>
        /// (Required)  List of contacts for the given customer and their respective preferences.
        /// </summary>
        public List<WSCustomerContactNotificationPreferenceInfo> contacts = new List<WSCustomerContactNotificationPreferenceInfo>();
    }

    /// <summary>
    /// Customer Account Order History Response
    /// </summary>
    public class CustomerOrderHistoryResponse : WSMSD
    {
        /// <summary>
        /// hdr
        /// </summary>
        public WSWebResponseHeader hdr { get; set; } = new WSWebResponseHeader();

        /// <summary>
        /// The body from the NIS response
        /// </summary>
        public string Body { get; set; }
    }

    /// <summary>
    /// WSCustomerContactNotificationPreferenceInfo represents the customer�s contact information in CNG along with their notification preferences. 
    /// </summary>
    public class WSCustomerContactNotificationPreferenceInfo
    {
        /// <summary>
        /// string(40)
        /// (Required)  The contact registering for the mandatory notifications.
        /// </summary>
        public string contactId { get; set; } = string.Empty;
        /// <summary>
        /// string(30)
        /// (Required)  The contact�s first name
        /// </summary>
        public string firstName { get; set; } = string.Empty;
        /// <summary>
        /// string(30)
        /// (Required)  The contact�s last name
        /// </summary>
        public string lastName { get; set; } = string.Empty;
        /// <summary>
        /// string(100)
        /// (Required)  The contact�s email address
        /// </summary>
        public string email { get; set; } = string.Empty;
        /// <summary>
        /// string(20)
        /// (Required)  The contact�s phone number for receiving text messages.If no SMS Phone is saved, the field is still returned but with no value
        /// </summary>
        public string smsPhone { get; set; } = string.Empty;
        /// <summary>
        /// boolean
        /// (Required)  True means the contact receives mandatory notifications.False means the contact does not receive mandatory notifications.
        /// </summary>
        public bool mandatory { get; set; } = false;
        /// <summary>
        /// List<WSNotificationTypePreferenceInfo>
        /// (Required)  The contact�s notification preferences.
        /// </summary>
        public List<WSNotificationTypePreferenceInfo> preferences { get; set; } = new List<WSNotificationTypePreferenceInfo>();
    }

    /// <summary>
    /// WSNotificationTypePreferenceInfo represents a customer contact notification type preference.
    /// </summary>
    public class WSNotificationTypePreferenceInfo
    {
        /// <summary>
        /// string(40)
        /// (Required)  The notification type.
        /// </summary>
        public string notificationType { get; set; }
        /// <summary>
        /// string(60)
        /// (Required)  The notification type description.
        /// </summary>
        public string notificationDescription { get; set; }
        /// <summary>
        /// WSNotificationTypeCategoryInfo
        /// (Optional)  The category for this notification type.
        /// </summary>
        public WSNotificationTypeCategoryInfo category { get; set; } = new WSNotificationTypeCategoryInfo();
        /// <summary>
        /// WSNotificationTypeSubCategoryInfo
        /// (Optional)  The sub-category for this notification type.
        /// </summary>
        public WSNotificationTypeSubCategoryInfo subCategory { get; set; } = new WSNotificationTypeSubCategoryInfo();
        /// <summary>
        /// boolean
        /// (Required)  True means the contact receives notifications for the specified notification type.False means the contact does not receive notifications for the type.
        /// </summary>
        public bool optIn { get; set; }
        /// <summary>
        /// List<WSNotificationChannelPreferenceInfo>
        /// (Required)  The methods the contact receives the notifications on.
        /// </summary>
        public List<WSNotificationChannelPreferenceInfo> channels { get; set; } = new List<WSNotificationChannelPreferenceInfo>();
    }

    /// <summary>
    /// WSNotificationChannelPreferenceInfo represents the channels preferences for a notification type for a customer contact
    /// </summary>
    public class WSNotificationChannelPreferenceInfo
    {
        /// <summary>
        /// string(20)
        /// (Required)  The notification channel.
        /// </summary>
        public string channel { get; set; }
        /// <summary>
        /// boolean
        /// (Required)  True means the contact receives the notification(s) on this channel.False means the contact does not receive the notification(s) on this channel.
        /// </summary>
        public bool enabled { get; set; }
        /// <summary>
        /// string(20)
        /// (Required)  Indicates the system requirement type for the notification type.Valid values are:
        /// �	Mandatory: the customer cannot opt out from receiving the notification via this channel.
        /// �	NotMandatory:  notification types that can be opted in or opted out (includes Default and Optional).
        /// </summary>
        public string reqType { get; set; }
    }

    /// <summary>
    /// The second level of notification type category used to group notification types within a category
    /// </summary>
    public class WSNotificationTypeSubCategoryInfo
    {
        /// <summary>
        /// string(40)
        /// (Required) The unique notification type sub-category reference
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// string(100)
        /// (Required)  The notification type sub-category description.
        /// </summary>
        public string description { get; set; }
    }

    /// <summary>
    /// The first level of notification type category used to group notification types
    /// </summary>
    public class WSNotificationTypeCategoryInfo
    {
        /// <summary>
        /// string(40)
        /// (Required) The unique notification type category reference
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// string(100)
        /// (Required)  The notification type category description.
        /// </summary>
        public string description { get; set; }
    }

    /// <summary>
    /// 2.8.11 customer/<customer-id>/address/<address-id>/dependencies GET
    /// </summary>
    public class WSAddressDependencies
    {
        /// <summary>
        /// contactDependencies
        /// </summary>
        public List<WSCustomerContactInfo> contacts { get; set; } = new List<WSCustomerContactInfo>();

        /// <summary>
        /// fundingSourceDependencies
        /// </summary>
        public List<WSFundingSourceExt> fundingSources { get; set; } = new List<WSFundingSourceExt>();
    }

    /// <summary>
    /// 2.5.10 customer/<customer-id>/contact/<contact-id>/status/<statusaction> PUT
    /// </summary>
    public class WSCustomerContactStatusReponse : WSMSD
    {

    }

    /// <summary>
    /// payment/repository/nonce
    /// </summary>
    public class NonceResponseBody
    {
        /// <summary>
        /// (Required)  The system generated security token.
        /// </summary>
        public string nonce { get; set; }
    }
}

