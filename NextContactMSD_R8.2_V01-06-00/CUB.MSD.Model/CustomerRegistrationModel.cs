/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CUB.MSD.Web.Models
{
    /// <summary>
    /// CustomerRegistrationModel
    /// </summary>
    public class CustomerRegistrationModel
    {
        /// <summary>
        /// AccountType
        /// </summary>
        public Guid AccountType { set; get; }


        /// <summary>ContactType
        /// 
        /// </summary>
        public Guid ContactType { get; set; }

        /// <summary>
        /// FirstName
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// LastName
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// MiddleNameInitial
        /// </summary>
        public string MiddleNameInitial { get; set; }

        /// <summary>
        /// BirthDate
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone1Type
        /// </summary>
        public int Phone1Type { get; set; }

        /// <summary>
        /// Phone1
        /// </summary>
        public string Phone1 { get; set; }
        
        /// <summary>
        /// 
        /// 
        /// </summary>
        public int Phone2Type { get; set; }

        /// <summary>
        /// Phone2
        /// </summary>
        public string Phone2 { get; set; }

        /// <summary>
        /// Phone3Type
        /// </summary>
        public int Phone3Type { get; set; }

        /// <summary>
        /// Phone3
        /// </summary>
        public string Phone3 { get; set; }

        /// <summary>
        /// Address1
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Address2
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid? State { get; set; }

        /// <summary>
        /// ZipPostalCodeId
        /// </summary>
        public Guid? ZipPostalCodeId { get; set; }

        /// <summary>
        /// Zip
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public Guid? Country { get; set; }

        /// <summary>
        /// SecurityQAs
        /// </summary>
        public Dictionary<Guid, string> SecurityQAs { get; set; }

        /// <summary>
        /// PIN
        /// </summary>
        public string PIN { get; set; }

        /// <summary>
        /// UserName
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// CustomerRegistrationModel
        /// </summary>
        public CustomerRegistrationModel()
        {
            SecurityQAs = new Dictionary<Guid, string>();
        }
    }
}
