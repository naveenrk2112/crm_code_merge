﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CUB.MSD.Model.NIS
{
    public class TravelTokenDisplayConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(WSTravelTokenDisplay));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JContainer lJContainer = default(JContainer);

            if (reader.TokenType == JsonToken.StartObject)
            {
                lJContainer = JObject.Load(reader);

                if (lJContainer["tokenType"].Value<string>() == "Mobile")
                    objectType = typeof(WSMobileTravelTokenDisplay);

                if (lJContainer["tokenType"].Value<string>() == "Bankcard")
                    objectType = typeof(WSBankcardTravelTokenDisplay);

                if (lJContainer["tokenType"].Value<string>() == "Smartcard")
                    objectType = typeof(WSSmartcardTravelTokenDisplay);

                if (lJContainer["tokenType"].Value<string>() == "LicensePlate")
                    objectType = typeof(WSLicensePlateTravelTokenDisplay);

                if (lJContainer["tokenType"].Value<string>() == "Transponder")
                    objectType = typeof(WSTransponderTravelTokenDisplay);

                if (objectType == typeof(WSTravelTokenDisplay)) throw new NotImplementedException();

                existingValue = Convert.ChangeType(existingValue, objectType);
                existingValue = Activator.CreateInstance(objectType);

                serializer.Populate(lJContainer.CreateReader(), existingValue);
            }

            return existingValue;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }


    public class CreditCardConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(WSCreditCard));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JContainer lJContainer = default(JContainer);

            if (reader.TokenType == JsonToken.StartObject)
            {
                lJContainer = JObject.Load(reader);

                if (lJContainer["maskedPan"] != null)
                    objectType = typeof(WSCreditCardReference);

                if (lJContainer["PAN"] != null)
                    objectType = typeof(WSCreditCardClear);

                if (objectType == typeof(WSCreditCard)) throw new NotImplementedException();

                existingValue = Convert.ChangeType(existingValue, objectType);
                existingValue = Activator.CreateInstance(objectType);

                serializer.Populate(lJContainer.CreateReader(), existingValue);
            }

            return existingValue;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }


    public class DirectDebitConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(WSDirectDebit));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JContainer lJContainer = default(JContainer);

            if (reader.TokenType == JsonToken.StartObject)
            {
                lJContainer = JObject.Load(reader);

                if (lJContainer["bankRoutingNumber"] != null)
                    objectType = typeof(WSDirectDebitClear);

                if (lJContainer["directDebitReferenceId"] != null)
                    objectType = typeof(WSDirectDebitReference);

                if (objectType == typeof(WSDirectDebit)) throw new NotImplementedException();

                existingValue = Convert.ChangeType(existingValue, objectType);
                existingValue = Activator.CreateInstance(objectType);

                serializer.Populate(lJContainer.CreateReader(), existingValue);
            }

            return existingValue;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
