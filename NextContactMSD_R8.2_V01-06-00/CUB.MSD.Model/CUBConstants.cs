/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUB.MSD.Model
{
    public static class CUBConstants
    {
        #region Exceptions
        /// <summary>
        /// Exceptions
        /// </summary>
        public static class Exceptions
        {
            /// <summary>
            /// LOCAL_CONTEXT_NOT_PASSED
            /// </summary>
            public const string LOCAL_CONTEXT_NOT_PASSED = "Plugin Error: LocalContext not passed.";
            /// <summary>
            /// UNABLE_TO_GET_GLOBALS_VALUE
            /// </summary>
            public const string UNABLE_TO_GET_GLOBALS_VALUE = "Unable to get CUB globals value";
            /// <summary>
            /// entitynotfound
            /// </summary>
            public static string entitynotfound = "Error: {0} entity not found";
            /// <summary>
            /// GET_TARGET_ENTITY_FAILED
            /// </summary>
            public static string GET_TARGET_ENTITY_FAILED = "Error: {0} Inputpararmeters Image not passed.";
            /// <summary>
            /// ERROR_PLUGIN_REQUIRED_PARAM_MISSING
            /// </summary>
            public const string ERROR_PLUGIN_REQUIRED_PARAM_MISSING = "Plugin Error: Required paramater missing value.";
            /// <summary>
            /// UNABLE_TO_CONVERT_ENTITY_AS_SPECIFIED_TYPE
            /// </summary>
            public const string UNABLE_TO_CONVERT_ENTITY_AS_SPECIFIED_TYPE = "Unable to convert Entity as specified type";
        }

        #endregion Exceptions

        #region Account
        /// <summary>
        /// Account
        /// </summary>
        public static class Account
        {
            /// <summary>
            /// ENTITY_NAME
            /// </summary>
            public static string ENTITY_NAME = "account";

            /// <summary>
            /// ACCOUNT_PLUGIN
            /// </summary>
            public const string ACCOUNT_PLUGIN = "AccountPlugin";
        }
        #endregion Account

        #region Contact
        /// <summary>
        /// Contact
        /// </summary>
        public static class Contact
        {
            /// <summary>
            /// CONTACT_PLUGIN
            /// </summary>
            public const string CONTACT_PLUGIN = "ContactPlugin";

            public static class Error
            {
                public const string MULTIPLE_CONTACTS_NOT_ALLOWED = "Multiple contacts per customer are not supported";
            }
        }
        #endregion Contact

        #region Address
        /// <summary>
        /// Address
        /// </summary>
        public static class Address
        {
            /// <summary>
            /// ADDRESS_STRING
            /// </summary>
            public const string ADDRESS_STRING = "Address";

            /// <summary>
            /// ADDRESS_STATUS_CODE_DELETED
            /// </summary>
            public const string ADDRESS_STATUS_CODE_DELETED = "Deleted";

            /// <summary>
            /// PLUG_IN_ACTION_PRE_DELETE_VALIDATION
            /// </summary>
            public const string PLUG_IN_ACTION_PRE_DELETE_VALIDATION = "CUB_AddressPreDeleteValidation";

            /// <summary>
            /// Error
            /// </summary>
            public static class Error
            {
                /// <summary>
                /// ERR_CUB_ADDRESS_NULL
                /// </summary>
                public const string ERR_CUB_ADDRESS_NULL = "cub_address is null";


                /// <summary>
                /// ERR_STATE_ZIP_INVALID_COMBO
                /// </summary>
                public const string ERR_STATE_ZIP_INVALID_COMBO = "State/Province code is invalid for the specified Postal/Zip code";

                /// <summary>
                /// ERR_EXISTING_ADDRESS
                /// </summary>
                public const string ERR_EXISTING_ADDRESS = "This address already exists";

                /// <summary>
                /// ERR_DELETE_ADDRESS
                /// </summary>
                public const string ERR_DELETE_ADDRESS = "Unable to delete address as it is linked to one or more contact and/or funding sources";

                /// <summary>
                /// ERR_PLUGIN_STEP_IMAGE_NOT_FOUND
                /// </summary>
                public const string ERR_PLUGIN_STEP_IMAGE_NOT_FOUND = "Plugin step image not found. Check plugin registration";

                /// <summary>
                /// ERR_NIS_ADDRESS_DEPENDS
                /// </summary>
                public const string ERR_NIS_ADDRESS_DEPENDS = "NIS call to get address dependencies returned unsuccessful response in the response header";
            }
        }
        #endregion Address

        #region Country
        /// <summary>
        /// Country
        /// </summary>
        public static class Country
        {
            /// <summary>
            /// COUNTRY_STRING
            /// </summary>
            public const string COUNTRY_STRING = "Country";
        }
        #endregion Country

        #region Credential
        /// <summary>
        /// Credential entity related
        /// </summary>
        public static class Credential
        {
            /// <summary>
            /// Active
            /// </summary>
            public const string Active = "Active";

            /// <summary>
            /// Inactive
            /// </summary>
            public const string Inactive = "Inactive";

            /// <summary>
            /// Locked
            /// </summary>
            public const string Locked = "Locked";

            /// <summary>
            /// Pending
            /// </summary>
            public const string Pending = "PendingActivation";

            /// <summary>
            /// Errors specific to Credetial entity
            /// </summary>
            public static class Error
            {
                /// <summary>
                /// ERR_KEY_MULTIPLE_USERNAMES
                /// </summary>
                public const string ERR_KEY_MULTIPLE_USERNAMES = "errors.username.multiple.records.found";

                /// <summary>
                /// ERR_CONTACT_MULTIPLE_CREDENTIALS
                /// </summary>
                public const string ERR_CONTACT_MULTIPLE_CREDENTIALS = "errors.contact.multiple.records.found";

                /// <summary>
                /// ERR_MULTIPLE_USERNAMES_FOUND
                /// </summary>
                public const string ERR_MULTIPLE_USERNAMES_FOUND = "More than one credential records found";

                /// <summary>
                /// ERR_CONTACT_CREDENTIALS_NOT_FOUND
                /// </summary>
                public const string ERR_CONTACT_CREDENTIALS_NOT_FOUND = "Contact credentials not found";


                /// <summary>
                /// ERR_CREDENTIAL_INACTIVE
                /// </summary>
                public const string ERR_CREDENTIAL_INACTIVE = "Credential is inactive";

                /// <summary>
                /// ERR_CREDENTIAL_LOCKED
                /// </summary>
                public const string ERR_CREDENTIAL_LOCKED = "Credential is locked";

                /// <summary>
                /// ERR_CREDENTIAL_PENDING_ACTIVATION
                /// </summary>
                public const string ERR_CREDENTIAL_PENDING_ACTIVATION = "Credential is pending activation";
            }
        }
        #endregion Credential

        #region Verification Token related
        /// <summary>
        /// VerificationToken
        /// </summary>
        public static class VerificationToken
        {
            /// <summary>
            /// Error
            /// </summary>
            public static class Error
            {
                /// <summary>
                /// ERR_VALIDATE_VT_CRED_ID_MISSING
                /// </summary>
                public const string ERR_VALIDATE_VT_CRED_ID_MISSING = "Unable to validate verification token, missing credential Id";

                /// <summary>
                /// ERR_SECURITY_VT_EXPIRED
                /// </summary>
                public const string ERR_SECURITY_VT_EXPIRED = "errors.security.verificationtoken.expired";

                /// <summary>
                /// ERR_TOKEN_EXPIRED_MSG
                /// </summary>
                public const string ERR_TOKEN_EXPIRED_MSG = "The verification token has expired and a new one needs to be requested to reset the password";

                /// <summary>
                /// ERR_TOKEN_INVALID
                /// </summary>
                public const string ERR_TOKEN_INVALID = "errors.security.invalid";

                /// <summary>
                /// ERR_TOKEN_INVALID_MSG
                /// </summary>
                public const string ERR_TOKEN_INVALID_MSG = "The verification token is invalid.  This could mean that it was not found or was already redeemed";
            }
        }
        #endregion

        #region Globals
        public static class Globals
        {

            /// <summary>
            /// CustomerRegistration
            /// </summary>
            public const string CustomerRegistration = "CustomerRegistration";

            /// <summary>
            /// CustomerRegistration_FirstName_MaxLength
            /// </summary>
            public const string CustomerRegistration_FirstName_MaxLength = "FirstName_MaxLength";

            /// <summary>
            /// CustomerRegistration_Lastname_MaxLength
            /// </summary>
            public const string CustomerRegistration_LastName_MaxLength = "LastName_MaxLength";

            /// <summary>
            /// CustomerRegistration_IsAddressRequired
            /// </summary>
            public const string CustomerRegistration_IsAddressRequired = "IsAddressRequired";

            /// <summary>
            /// CustomerRegistration_AddressLine1_MaxLength
            /// </summary>
            public const string CustomerRegistration_AddressLine1_MaxLength = "AddressLine1_MaxLength";

            /// <summary>
            /// CustomerRegistration_AddressLine2_MaxLength
            /// </summary>
            public const string CustomerRegistration_AddressLine2_MaxLength = "AddressLine2_MaxLength";

            /// <summary>
            /// CustomerRegistration_City_MaxLength
            /// </summary>
            public const string CustomerRegistration_City_MaxLength = "City_MaxLength";

            /// <summary>
            /// CustomerRegistration_City_MaxLength
            /// </summary>
            public const string CustomerRegistration_SecurityAnswer_MaxLength = "SecurityAnswer_MaxLength";

            /// <summary>
            /// CustomerRegistration_SecurityQuestions_Max
            /// </summary>
            public const string CustomerRegistration_SecurityQuestions_Max = "SecurityQuestions_Max";
            
            /// <summary>
            /// CustomerRegistration_SecurityQuestions_Minimum
            /// </summary>
            public const string CustomerRegistration_SecurityQuestions_Minimum = "SecurityQuestions_Minimum";

            /// <summary>
            /// Tolling_API
            /// </summary>
            public const string Tolling_API = "TollingApi";

            /// <summary>
            /// NIS_API_BASE_URL
            /// </summary>
            public const string Tolling_API_BASE_URL = "TollingBaseUrl";

            /// <summary>
            /// NIS_API
            /// </summary>
            public const string NIS_API = "NISApi";

            /// <summary>
            /// NIS_API_BASE_URL
            /// </summary>
            public const string NIS_API_BASE_URL = "BaseUrl";

            /// <summary>
            /// NIS_API_COMP_REG_PATH
            /// </summary>
            public const string NIS_API_COMP_REG_PATH = "CompleteRegistrationPath";

            /// <summary>
            /// NIS_API_VERIFICATION_TOKEN_REPORT
            /// </summary>
            public const string NIS_API_VERIFICATION_TOKEN_REPORT = "VerificationTokenReportPath";

            /// <summary>
            /// 
            /// </summary>
            public const string ANGULAR_JWE_KEY_NAME = "JWE.Encrypt.KeyName";

            /// <summary>
            /// NIS_API_USERNAME
            /// </summary>
            public const string NIS_API_USERNAME = "Username";

            /// <summary>
            /// NIS_API_PASSWORD
            /// </summary>
            public const string NIS_API_PASSWORD = "Password";

            /// <summary>
            /// NIS_API_DEVICE
            /// </summary>
            public const string NIS_API_DEVICE = "Device";

            /// <summary>
            /// NIS_API_TRANSIT_ACCOUNT_STATUS
            /// </summary>
            public const string NIS_API_TRANSIT_ACCOUNT_STATUS = "TransitAccount";

            /// <summary>
            /// NIS_API_SEARCH_TOKEN_PATH
            /// </summary>
            public const string NIS_API_SEARCH_TOKEN_PATH = "SearchTokenPath";

            /// <summary>
            /// NIS_API_UPDATE_DEPENDANCIES
            /// </summary>
            public const string NIS_API_UPDATE_DEPENDANCIES = "UpdateDependenciesPath";

            /// <summary>
            /// NIS_API_REPORT_CHANGE
            /// </summary>
            public const string NIS_API_REPORT_CHANGE = "ReportChangePath";

            /// <summary>
            /// NIS_API_SEND_FORGOTTEN_USERNAME
            /// </summary>
            public const string NIS_API_SEND_FORGOTTEN_USERNAME = "ReportForgottenUsernamePath";

            /// <summary>
            /// NIS_API_LINK_SUBSYSTEM_TO_CUSTOMER_ACCOUNT
            /// </summary>
            public const string NIS_API_LINK_SUBSYSTEM_TO_CUSTOMER_ACCOUNT = "LinkSubssystemToCustomerAccount";

            /// <summary>
            /// NIS_API_CANBEDELINKED_SUBSYSTEM_FROM_CUSTOMER_ACCOUNT
            /// </summary>
            public const string NIS_API_CANBEDELINKED_SUBSYSTEM_FROM_CUSTOMER_ACCOUNT = "CanBeDelinkedSubsystemFromCustomerAccount";

            /// <summary>
            /// NIS_API_DELINK_SUBSYSTEM_TO_CUSTOMER_ACCOUNT
            /// </summary>
            public const string NIS_API_DELINK_SUBSYSTEM_TO_CUSTOMER_ACCOUNT = "DelinkSubsystemFromCustomerAccount";

            /// <summary>
            /// NIS_API_CUSTOMER_NOTIFICATION_PREFERENCES
            /// </summary>
            public const string NIS_API_CUSTOMER_NOTIFICATION_PREFERENCES = "CustomerNotificationPreferecesPath";

            /// <summary>
            /// NIS_API_CONFIG_NOTIFICATION_CHANNEL
            /// </summary>
            public const string NIS_API_CONFIG_NOTIFICATION_CHANNEL = "ConfigNotificationChannelPath";

            /// <summary>
            /// NIS_API_REPORT_ACCOUNT_LOCKED
            /// </summary>
            public const string NIS_API_REPORT_ACCOUNT_LOCKED = "ReportAccountLockedPath";

            /// <summary>
            /// NIS_SUBSYSTEM_TRAVEL_TOKEN_CAN_BE_LINKED_PATH
            /// </summary>
            public const string NIS_SUBSYSTEM_TRAVEL_TOKEN_CAN_BE_LINKED_PATH = "SubsystemTravelTokenCanBeLinkedPath";

            /// <summary>
            /// NIS_SUBSYSTEM_BLOCK_ACCOUNT_PATH
            /// </summary>
            public const string NIS_SUBSYSTEM_BLOCK_ACCOUNT_PATH = "SubsystemBlockAccountPath";

            /// <summary>
            /// NIS_SUBSYSTEM_BLOCK_ACCOUNT_PATH
            /// </summary>
            public const string NIS_SUBSYSTEM_UNBLOCK_ACCOUNT_PATH = "SubsystemUnblockAccountPath";

            /// <summary>
            /// NIS_SUBSYSTEM_BLOCK_ACCOUNT_PATH
            /// </summary>
            public const string NIS_SUBSYSTEM_TOKEN_ACTION_PATH = "TokenActionPath";

            /// <summary>
            /// NIS_SEND_CNG_PATH
            /// </summary>
            public const string NIS_SEND_CNG_PATH = "SendCNGPath";

            /// <summary>
            /// AppInfo
            /// </summary>
            public const string AppInfo = "AppInfo";

            /// <summary>
            /// UnregisteredCustomerID
            /// </summary>
            public const string UnregisteredCustomerID = "UnregisteredCustomerID";

            /// <summary>
            /// CustomerFeedbackTitleTemplate
            /// </summary>
            public const string CustomerFeedbackTitleTemplate = "CustomerFeedbackTitleTemplate";


            /// <summary>
            /// AppInfo_AppVersion
            /// </summary>
            public const string AppInfo_AppVersion = "AppVersion";

            /// <summary>
            /// AppInfo_ClientName
            /// </summary>
            public const string AppInfo_ClientName = "ClientName";

            /// <summary>
            /// RollupLinkedEntityActivitiesToAccount
            /// </summary>
            public const string RollupLinkedEntityActivitiesToAccount = "RollupLinkedEntityActivitiesToAccount";

            /// <summary>
            /// AllowMultipleContactsPerCustomer
            /// </summary>
            public const string AllowMultipleContactsPerCustomer = "AllowMultipleContactsPerCustomer";

            /// <summary>
            /// NIS_API_TRANSIT_ACCOUNT_BALANCE_HISTORY
            /// </summary>
            public const string NIS_API_TRANSIT_ACCOUNT_BALANCE_HISTORY = "TransitAccountBalanceHistoryPath";

            /// <summary>
            /// NIS_API_TRANSIT_ACCOUNT_TRAVEL_HISTORY
            /// </summary>
            public const string NIS_API_TRANSIT_ACCOUNT_TRAVEL_HISTORY = "TransitAccountTravelHistoryPath";

            /// <summary>
            /// NIS_API_TRANSIT_ACCOUNT_TRAVEL_HISTORY_TAPS
            /// </summary>
            public const string NIS_API_TRANSIT_ACCOUNT_TRAVEL_HISTORY_TAPS = "TransitAccountTravelHistoryTapsPath";

            /// <summary>
            /// NIS_API_TRANSIT_ACCOUNT_ORDER_HISTORY
            /// </summary>
            public const string NIS_API_TRANSIT_ACCOUNT_ORDER_HISTORY = "TransitAccountOrderHistoryPath";

            /// <summary>
            /// NIS_API_DELETE_ADDRESS_PATH
            /// </summary>
            public const string NIS_API_DELETE_ADDRESS_PATH = "DeleteAddressPath";

            /// <summary>
            /// NIS_API_UPDATE_ADDRESS_PATH
            /// </summary>
            public const string NIS_API_UPDATE_ADDRESS_PATH = "UpdateAddressPath";

            /// <summary>
            /// APPINFO_SOFT_DELETE_ADDRESS
            /// </summary>
            public const string APPINFO_SOFT_DELETE_ADDRESS = "SoftDeleteAddress";

            /// <summary>
            /// NIS_API_CUSTOMER_NOTIFICATION
            /// </summary>
            public const string NIS_API_CUSTOMER_NOTIFICATION = "NotificationPath";

            /// <summary>
            /// NIS_API_CUSTOMER_NOTIFICATION_DETAIL
            /// </summary>
            public const string NIS_API_CUSTOMER_NOTIFICATION_DETAIL = "NotificationDetailPath";

            /// <summary>
            /// NIS_API_CUSTOMER_NOTIFICATION_RESEND
            /// </summary>
            public const string NIS_API_CUSTOMER_NOTIFICATION_RESEND = "NotificationResendPath";

            /// <summary>
            /// NIS_API_CUSTOMER_FUNDINGSOURCE_POST
            /// </summary>
            public const string NIS_API_CUSTOMER_FUNDINGSOURCE_POST = "FundingSourcePostPath";

            /// <summary>
            /// NIS_API_ORDER_DEBTCOLLECT
            /// </summary>
            public const string NIS_API_ORDER_DEBTCOLLECT = "OrderDebtCollectPath";

            /// <summary>
            /// NIS_API_ORDER_ADJUSTMENT
            /// </summary>
            public const string NIS_API_ORDER_ADJUSTMENT = "OrderAdjustmentPath";
            
            /// <summary>
            /// NIS_API_ORDER_CORRECTION
            /// </summary>
            public const string NIS_API_ORDER_TRAVEL_CORRECTION = "OrderTravelCorrectionPath";

            /// <summary>
            /// NIS_API_ONEACCOUNT_SUMMARY
            /// </summary>
            public const string NIS_API_ONEACCOUNT_SUMMARY = "OneAccountSummaryPath";

            /// <summary>
            /// APPINFO_MAX_FAILED_LOGIN_ATTEMPTS
            /// </summary>
            public const string APPINFO_MAX_FAILED_LOGIN_ATTEMPTS = "MaxFailedLoginAttempts";

            /// <summary>
            /// NIS_API_PASSWORD_REPORT_CHANGE
            /// </summary>
            public const string NIS_API_PASSWORD_REPORT_CHANGE = "PasswordReportChangePath";

            /// <summary>
            /// NIS_API_CUSTOMER_NOTIFICATION_PREFERENCES_PATCH
            /// </summary>
            public const string NIS_API_CUSTOMER_NOTIFICATION_PREFERENCES_PATCH = "CustomerNotificationPreferencesPatchPath";

            /// <summary>
            /// NIS_API_SUBSYSTEM_TOKEN_TYPE_MAPPING
            /// </summary>
            public const string NIS_API_SUBSYSTEM_TOKEN_TYPE_MAPPING = "TokenTypeMappings";

            /// <summary>
            /// NIS_API_ADDRESS_DEPENDENCIES
            /// </summary>
            public const string NIS_API_ADDRESS_DEPENDENCIES = "AddressDependenciesPath";

            /// <summary>
            /// USERNAME_VALIDATION
            /// </summary>
            public const string USERNAME_VALIDATION = "UsernameValidation";

            /// <summary>
            /// USERNAME_MIN_LENGTH
            /// </summary>
            public const string USERNAME_MIN_LENGTH = "UsernameMinLength";

            /// <summary>
            /// USERNAME_MAX_LENGTH
            /// </summary>
            public const string USERNAME_MAX_LENGTH = "UsernameMaxLength";

            /// <summary>
            /// USERNAME_REQUIRE_EMAIL
            /// </summary>
            public const string USERNAME_REQUIRE_EMAIL = "UsernameRequireEmail";

            /// <summary>
            /// MOBILE_TOKEN_VALIDATION
            /// </summary>
            public const string MOBILE_TOKEN_VALIDATION = "MobileTokenValidation";

            /// <summary>
            /// MOBILE_TOKEN_MAX_LENGTH
            /// </summary>
            public const string MOBILE_TOKEN_MAX_LENGTH = "MobileTokenMaxLength";

            /// <summary>
            /// One Account Balance History Path
            /// </summary>
            public const string ONE_ACCOUNT_BALANCE_HISTORY_PATH = "OneAccountBalanceHistoryPath";

            /// <summary>
            /// One Account Balance History Detail Path
            /// </summary>
            public const string ONE_ACCOUNT_BALANCE_HISTORY_DETAIL_PATH = "OneAccountBalanceHistoryDetailPath";

            /// <summary>
            /// One Account Travel History Path
            /// </summary>
            public const string ONE_ACCOUNT_TRAVEL_HISTORY_PATH = "OneAccountTravelHistoryPath";

            /// <summary>
            /// One Account Travel History Detail Path
            /// </summary>
            public const string ONE_ACCOUNT_TRAVEL_HISTORY_DETAIL_PATH = "OneAccountTravelHistoryDetailPath";

            /// <summary>
            /// Transit Account Travel History Detail Path
            /// </summary>
            public const string TRANSIT_ACCOUNT_TRAVEL_HISTORY_DETAIL_PATH = "TransitAccountTravelHistoryDetailPath";

            /// <summary>
            /// Transit Account Status History Path
            /// </summary>
            public const string TRANSIT_ACCOUNT_STATUS_HISTORY_PATH = "TransitAccountStatusHistoryPath";

            /// <summary>
            /// Transit Account Status History Path
            /// </summary>
            public const string TOKEN_STATUS_HISTORY_PATH = "TransitAccountTokenStatusHistoryPath";

            /// <summary>
            /// Order History Path
            /// </summary>
            public const string ORDER_HISTORY_PATH = "OrderHistoryPath";

            /// <summary>
            /// Order History By Order Id Path
            /// </summary>
            public const string ORDER_HISTORY_BY_ORDER_ID_PATH = "OrderHistoryByOrderIdPath";

            /// <summary>
            /// Customer Order History Path
            /// </summary>
            public const string CUSTOMER_ORDER_HISTORY_PATH = "CustomerOrderHistoryPath";

            /// <summary>
            /// Customer Order History Detail Path
            /// </summary>
            public const string CUSTOMER_ORDER_HISTORY_DETAIL_PATH = "CustomerOrderHistoryDetailPath";

            /// <summary>
            /// Customer Order Path
            /// </summary>
            public const string CUSTOMER_ORDER_PATH = "CustomerOrderPath";

            /// <summary>
            /// Tolling Get Transponders Path
            /// </summary>
            public const string TOLLING_GET_TRANSPONDERS_PATH = "TollingGetTranspondersPath";

            /// <summary>
            /// Tolling Get Vehicles Path
            /// </summary>
            public const string TOLLING_GET_VEHICLES_PATH = "TollingGetVehiclesPath";

            /// <summary>
            /// Tolling Get Transaction History Path
            /// </summary>
            public const string TOLLING_GET_TRANSACTION_HISTORY_PATH = "TollingGetTransactionHistoryPath";

            /// <summary>
            /// Tolling Get Account Summary Path
            /// </summary>
            public const string TOLLING_GET_ACCOUNT_SUMMARY_PATH = "TollingGetAccountSummaryPath";

            /// <summary>
            /// Customer API Activity Path
            /// </summary>
            public const string NIS_API_CUSTOMER_ACTIVITY_PATH = "CustomerActivityPath";

            /// <summary>
            /// Customer API Transit Account Subsystem Activity Path
            /// </summary>
            public const string NIS_API_TASUBSYSTEM_ACTIVITY_PATH = "TASubsystemActivityPath";

            /// <summary>
            /// Customer API Activity Detail Path
            /// </summary>
            public const string NIS_API_CUSTOMER_ACTIVITY_DETAIL_PATH = "CustomerActivityDetailPath";

            /// <summary>
            /// Customer API Transit Account Subsystem Activity Detail Path
            /// </summary>
            public const string NIS_API_TASUBSYSTEM_ACTIVITY_DETAIL_PATH = "TASubsystemActivityDetailPath";

            /// <summary>
            /// NIS_API_TRANSIT_ACCOUNT_BANK_CARD_CHARGE_HISTORY_PATH
            /// </summary>
            public const string NIS_API_TRANSIT_ACCOUNT_BANK_CARD_CHARGE_HISTORY_PATH = "TransitAccountBankCardChargeHistoryPath";

            /// <summary>
            /// NIS_API_TRANSIT_ACCOUNT_AGGREATED_DETAILS_PATH
            /// </summary>
            public const string NIS_API_TRANSIT_ACCOUNT_BANK_CHARGE_AGGREGATION_DETAILS_PATH = "TransitAccountBankChargeAggregationDetailsPath";

            /// <summary>
            /// NIS_API_X_CUB_HDR_TRACKING_TYPE
            /// </summary>
            public const string NIS_API_X_CUB_HDR_TRACKING_TYPE = "x-cub-hdr.Tracking.TrackingType";

            /// <summary>
            /// NIS_API_X_CUB_HDR_APP_ID
            /// </summary>
            public const string NIS_API_X_CUB_HDR_APP_ID = "x-cub-hdr.appId";

            /// <summary>
            /// NIS_API_X_CUB_AUDIT_LOCATION
            /// </summary>
            public const string NIS_API_X_CUB_AUDIT_LOCATION = "x-cub-audit.location";

            /// <summary>
            /// NIS_API_X_CUB_AUDIT_CHANNEL
            /// </summary>
            public const string NIS_API_X_CUB_AUDIT_CHANNEL = "x-cub-audit.channel";

            /// <summary>
            /// NIS_SUBSYSTEM_TRAVEL_MODES_PATH
            /// </summary>
            public const string NIS_SUBSYSTEM_TRAVEL_MODES_PATH = "SubsystemTravelModesPath";

            /// <summary>
            /// NIS_SUBSYSTEM_TRAVEL_OPERATORS_PATH
            /// </summary>
            public const string NIS_SUBSYSTEM_TRAVEL_OPERATORS_PATH = "SubsystemTravelOperatorsPath";

            /// <summary>
            /// NIS_SUBSYSTEM_TAP_TYPES_PATH
            /// </summary>
            public const string NIS_SUBSYSTEM_TAP_TYPES_PATH = "SubsystemTapTypesPath";

            /// <summary>
            /// NIS_SUBSYSTEM_TAP_STATUSES_PATH
            /// </summary>
            public const string NIS_SUBSYSTEM_TAP_STATUSES_PATH = "SubsystemTapStatusesPath";

            /// <summary>
            /// NIS_SUBSYSTEM_LOCATIONS_PATH
            /// </summary>
            public const string NIS_SUBSYSTEM_LOCATIONS_PATH = "TransitAccountLocation";

            /// <summary>
            /// NIS_API_ORDER_CONFIG_DATA_TYPES_PATH
            /// </summary>
            public const string NIS_API_ORDER_CONFIG_DATA_TYPES_PATH = "OrderGetConfigDataTypesPath";

            /// <summary>
            /// NIS_API_CUSTOMER_SERVICE_REASON_CODES_PATH
            /// </summary>
            public const string NIS_API_CUSTOMER_SERVICE_REASON_CODES_PATH = "CustomerServiceReasonCodesPath";

            /// <summary>
            /// NIS_API_PAYMENT_REPOSITORY_GET_NONCE_PATH
            /// </summary>
            public const string NIS_API_PAYMENT_REPOSITORY_GET_NONCE_PATH = "PaymentRepositoryGetNoncePath";

            /// <summary>
            /// CUB_MSD_WEB_ANGULAR
            /// </summary>
            public const string CUB_MSD_WEB_ANGULAR = "CUB.MSD.Web.Angular";

            /// <summary>
            /// RECENT_VERIFICATIONS_OFFSET_IN_MINUTES
            /// </summary>
            public const string RECENT_VERIFICATIONS_OFFSET_IN_MINUTES = "RecentVerificationsOffsetInMinutes";

            /// <summary>
            /// VERIFICATION_SUBJECT_VERIFIED
            /// </summary>
            public const string VERIFICATION_SUBJECT_VERIFIED = "SubjectVerified";

            /// <summary>
            /// VERIFICATION_SUBJECT_NOT_VERIFIED
            /// </summary>
            public const string VERIFICATION_SUBJECT_NOT_VERIFIED = "SubjectNotVerified";

            /// <summary>
            /// CUB_MSD_WEB
            /// </summary>
            public const string CUB_MSD_WEB = "CUB.MSD.WEB";

            /// <summary>
            /// WEB_APP_URL
            /// </summary>
            public const string WEB_APP_URL = "WEB.APP.URL";

            /// <summary>
            /// WEB_APP_CACHE_DURATIN_MINUTES
            /// </summary>
            public const string WEB_APP_CACHE_DURATION_MINUTES = "WEB.APP.CACHE.DURATION.MINUTES";

            /// <summary>
            /// WEB_APP_URL_UPDATECACHEDURATION
            /// </summary>
            public const string WEB_APP_URL_UPDATECACHEDURATION = "WEB.APP.URL.UPDATECACHEDURATION";

            /// <summary>
            /// GLOBAL_ACTIVITY_AUTO_CREATE_RULES
            /// </summary>
            public const string ACTIVITY_AUTO_CREATE_RULES = "ActivityAutoCreateRules";

            /// <summary>
            /// ACTIVITY_ADD_PHONE_NUMBER_ACTIVITY_TYPE
            /// </summary>
            public const string ACTIVITY_ADD_PHONE_NUMBER_ACTIVITY_TYPE = "AddPhoneNumberActivityType";

            /// <summary>
            /// ACTIVITY_ADD_ADDRESS_ACTIVITY_TYPE
            /// </summary>
            public const string ACTIVITY_ADD_ADDRESS_ACTIVITY_TYPE = "AddAddressActivityType";

            /// <summary>
            /// ACTIVITY_ADD_PHONE_NUMBER_ACTIVITY_TYPE
            /// </summary>
            public const string ACTIVITY_CONTACT_VERIFY_CONTACT_INFO = "ContactVerificationContactInfo";

            /// <summary>
            /// ACTIVITY_ADD_PHONE_NUMBER_ACTIVITY_TYPE
            /// </summary>
            public const string ACTIVITY_CONTACT_VERIFY_SECURITY_QUESTION_INFO = "ContactVerificationSecurityQuestion";

            /// <summary>
            /// ACTIVITY_ADD_PHONE_NUMBER_ACTIVITY_TYPE
            /// </summary>
            public const string ACTIVITY_CONTACT_VERIFY_UNVERIFIED = "ContactVerificationUnverified";

            /// <summary>
            /// NIS_API_NOTIFICATIONS_PATH
            /// </summary>
            public const string NIS_API_NOTIFICATIONS_PATH = "NotificationsPath";

            /// <summary>
            /// NIS_API_NOTIFICATIONS_PATH
            /// </summary>
            public const string NIS_API_NOTIFICATIONS_DETAILS_PATH = "NotificationDetailsPath";


            /// <summary>
            /// NIS_API_NOTIFICATIONS_PATH
            /// </summary>
            public const string NIS_API_ORDER_NOTIFICATIONS_PATH = "OrderNotificationPath";

            /// <summary>
            /// NIS_API_RESEND_ORDER_NOTIFICATION_PATH
            /// </summary>
            public const string NIS_API_RESEND_NOTIFICATION_PATH = "ResendNotificationPath";

            /// <summary>
            /// NIS_API_ORDER_PAYMENT_RECEIPT_PATH
            /// </summary>
            public const string NIS_API_ORDER_PAYMENT_RECEIPT_PATH = "OrderPaymentReceiptPath";

        }
        #endregion Globals

        /// <summary>
        /// Error
        /// </summary>
        public static class Error
        {
            /// <summary>
            /// SUCCESS
            /// </summary>
            public const string SUCCESS = "success";

            /// <summary>
            /// FAILURE
            /// </summary>
            public const string FAILURE =  "failure";

            /// <summary>
            /// ERR_GENERAL_EXCEPTION
            /// </summary>
            public const string SUCCESS_CHANGE_PASSWORD = "SuccessMustChangePwd";

            /// <summary>
            /// ERR_GENERAL_EXCEPTION
            /// </summary>
            public const string ERR_GENERAL_EXCEPTION = "errors.general.exception";

            /// <summary>
            /// ERR_GENERAL_DEPEND_EXCEPTION
            /// </summary>
            public const string ERR_GENERAL_DEPEND_EXCEPTION = "errors.general.dependencies.exception";

            /// <summary>
            /// ERR_GENERAL_VALUE_TOO_SMALL
            /// </summary>
            public const string ERR_GENERAL_VALUE_TOO_SMALL = "errors.general.value.too.small";

            /// <summary>
            /// ERR_GENERAL_VALUE_TOO_LONG
            /// </summary>
            public const string ERR_GENERAL_VALUE_TOO_LONG = "errors.general.value.too.long";

            /// <summary>
            /// ERR_GENERAL_VALUE_REQUIRED
            /// </summary>
            public const string ERR_GENERAL_VALUE_REQUIRED = "errors.general.value.required";

            /// <summary>
            /// ERR_GENERAL_RECORD_NOT_FOUND
            /// </summary>
            public const string ERR_GENERAL_RECORD_NOT_FOUND = "errors.general.record.not.found";

            /// <summary>
            /// ERR_GENERAL_VALUE_NOT_FOUND
            /// </summary>
            public const string ERR_GENERAL_VALUE_NOT_FOUND = "error.general.value.not.found";

            /// <summary>
            /// ERR_GENERAL_CHANGE_PASSWORD_FAILED
            /// </summary>
            public const string ERR_GENERAL_CHANGE_PASSWORD_FAILED = "errors.general.change.password.failed";

            /// <summary>
            /// ERR_GENERAL_FIND_USED_PASSWORDS_FAILED
            /// </summary>
            public const string ERR_GENERAL_FIND_USED_PASSWORDS_FAILED = "errors.general.find.previously.used.password.failed";

            /// <summary>
            /// ERR_GENERAL_VALIDATE_PASSWORD_FAILED
            /// </summary>
            public const string ERR_GENERAL_VALIDATE_PASSWORD_FAILED = "errors.general.validate.password.failed";

            /// <summary>
            /// ERR_CONTAIN_USERNAME
            /// </summary>
            public const string ERR_CONTAIN_USERNAME = "errors.password.cannot.contain.username";

            /// <summary>
            /// ERR_SAME_CONSECUTIVE_CHARS
            /// </summary>
            public const string ERR_SAME_CONSECUTIVE_CHARS = "errors.password.cannot.contain.same.char.consecutively";

            /// <summary>
            /// ERR_PIN_SAME_CONSECUTIVE_CHARS
            /// </summary>
            public const string ERR_PIN_SAME_CONSECUTIVE_CHARS = "errors.pin.cannot.contain.same.char.consecutively";

            /// <summary>
            /// ERR_CONTIGUOUS_CHARS
            /// </summary>
            public const string ERR_CONTIGUOUS_CHARS = "errors.password.cannot.contain.contiguous.chars";

            /// <summary>
            /// ERR_PIN_CONTIGUOUS_CHARS
            /// </summary>
            public const string ERR_PIN_CONTIGUOUS_CHARS = "errors.pin.cannot.contain.contiguous.chars";

            /// <summary>
            /// ERR_CONTAIN_DIGIT
            /// </summary>
            public const string ERR_CONTAIN_DIGIT = "errors.password.must.contain.atleast.digits";

            /// <summary>
            /// ERR_CONTAINS_DICTIOANARY_WORD
            /// </summary>
            public const string ERR_CONTAINS_DICTIOANARY_WORD = "errors.password.cannot.contain.dictionary.word";

            /// <summary>
            /// ERR_MIXED_CASE_CHARS
            /// </summary>
            public const string ERR_MIXED_CASE_CHARS = "errors.password.must.contain.mixed.case.chars";

            /// <summary>
            /// ERR_CONTAINS_AT_LEAST_CHARS
            /// </summary>
            public const string ERR_CONTAINS_AT_LEAST_CHARS = "errors.password.must.contain.at.least.chars";

            /// <summary>
            /// ERR_CONTAINS_A_SPECIAL_CHAR
            /// </summary>
            public const string ERR_CONTAINS_A_SPECIAL_CHAR = "errors.password.must.contain.at.least.special.chars";

            /// <summary>
            /// ERR_PWD_PREVIOUSLY_USED
            /// </summary>
            public const string ERR_PWD_PREVIOUSLY_USED = "errors.password.previously.used";

            /// <summary>
            /// ERR_ACCNT_INACTIVE
            /// </summary>
            public const string ERR_ACCNT_INACTIVE = "errors.account.is.inactive";

            /// <summary>
            /// ERR_ACCNT_LOCKED
            /// </summary>
            public const string ERR_ACCNT_LOCKED = "errors.account.is.locked";

            /// <summary>
            /// ERR_ACCNT_NOT_LOCKED
            /// </summary>
            public const string ERR_ACCNT_NOT_LOCKED = "errors.account.is.not.locked";

            /// <summary>
            /// ERR_ACCNT_NOT_UNIQUE
            /// </summary>
            public const string ERR_ACCNT_NOT_UNIQUE = "errors.account.not.unique";

            /// <summary>
            /// ERR_ACCNT_PENDING_ACTIVATION
            /// </summary>
            public const string ERR_ACCNT_PENDING_ACTIVATION = "errors.account.pending.activation";

            /// <summary>
            /// ERR_ACCNT_CLOSED_OR_PENDING_CLOSEURE
            /// </summary>
            public const string ERR_ACCNT_CLOSED_OR_PENDING_CLOSURE = "errors.account.is.closed.or.pending.close";

            /// <summary>
            /// ERR_ACCNT_HAS_NO_SECURITY_QUESTIONS_ON_FILE
            /// </summary>
            public const string ERR_ACCNT_HAS_NO_SECURITY_QUESTIONS_ON_FILE = "errors.account.no.security.questions";

            /// <summary>
            /// ERR_ACCNT_IS_EXPIRED
            /// </summary>
            public const string ERR_ACCNT_IS_EXPIRED = "errors.account.is.expired";

            /// <summary>
            /// ERR_GENERAL_CANNOT_START_WITH_SPEC_CHAR
            /// </summary>
            public const string ERR_GENERAL_CANNOT_START_WITH_SPEC_CHAR = "errors.general.email.cannot.start.with.special.characters";

            /// <summary>
            /// ERR_GENERAL_VALUE_UNEXPECTED
            /// </summary>
            public const string ERR_GENERAL_VALUE_UNEXPECTED = "errors.general.value.unexpected";

            /// <summary>
            /// ERR_GENERAL_MUST_BE_STD_EMAIL_FRMT
            /// </summary>
            public const string ERR_GENERAL_MUST_BE_STD_EMAIL_FRMT = "errors.general.email.invalid.format";

            /// <summary>
            /// ERR_ADDRESS_AND_ADDID_NOT_ALLOWED
            /// </summary>
            public const string ERR_ADDRESS_AND_ADDID_NOT_ALLOWED = "errors.address.and.addressid.not.allowed";

            /// <summary>
            /// ERR_GENERAL_INVALID_POSTAL_CODE
            /// </summary>
            public const string ERR_GENERAL_INVALID_POSTAL_CODE = "errors.general.invalid.postal.code ";

            /// <summary>
            /// ERR_GENERAL_EMPTY_LIST_NOT_ALLOWED
            /// </summary>
            public const string ERR_GENERAL_EMPTY_LIST_NOT_ALLOWED = "errors.general.empty.list.not.allowed ";

            /// <summary>
            /// ERR_GENERAL_VALUE_DUPLICATE
            /// </summary>
            public const string ERR_GENERAL_VALUE_DUPLICATE = "errors.general.value.duplicate";

            /// <summary>
            /// ERR_GENERAL_NUMERIC_ONLY
            /// </summary>
            public const string ERR_GENERAL_NUMERIC_ONLY = "errors.general.numerics.only";

            /// <summary>
            /// ERR_INVALID_DATE_OF_BIRTH
            /// </summary>
            public const string ERR_INVALID_DATE_OF_BIRTH ="errors.general.invalid.dateofbirth";

            /// <summary>
            /// ERR_ACCOUNT_LOCKED
            /// </summary>
            public const string ERR_ACCOUNT_LOCKED= "errors.account.is.locked";

            /// <summary>
            /// ERR_INVALID_USER_OR_PASSWORD
            /// </summary>
            public const string ERR_INVALID_USER_OR_PASSWORD= "errors.invalid.user.or.password";

            /// <summary>
            /// ERR_EMAIL_OR_PHONE_REQUIRED
            /// </summary>
            public const string ERR_EMAIL_OR_PHONE_REQUIRED= "errors.email.or.phone.are.required";

            /// <summary>
            /// ERR_SECURITY_NOT_POSSIBLE
            /// </summary>
            public const string ERR_SECURITY_NOT_POSSIBLE = "errors.security.not.possible";

            /// <summary>
            /// ERR_SECURITY_ANSWER_MISMATCH
            /// </summary>
            public const string ERR_SECURITY_ANSWER_MISMATCH = "errors.security.answer.mismatch";

            /// <summary>
            /// ERR_PIN_CONTAIN_USERNAME
            /// </summary>
            public const string ERR_PIN_CONTAIN_USERNAME = "errors.pin.cannot.contain.username";

            /// <summary>
            /// MSG_AT_LEAST_ONE_VALUE_REQUIRED
            /// </summary>
            public const string MSG_AT_LEAST_ONE_VALUE_REQUIRED = "At least one value must be specified ";

            /// <summary>
            /// MSG_INVALID_FORMAT_DICTATED
            /// </summary>
            public const string MSG_INVALID_FORMAT_DICTATED = "Invalid format dictated by country";

            /// <summary>
            /// MSG_INVALID_STATE
            /// </summary>
            public const string MSG_INVALID_STATE = "Invalid state";

            /// <summary>
            /// MSG_INVALID_COUNTRY
            /// </summary>
            public const string MSG_INVALID_COUNTRY = "Invalid country";

            /// <summary>
            /// MSG_POSTAL_CODE_REQUIRED
            /// </summary>
            public const string MSG_POSTAL_CODE_REQUIRED = "If address is provided, postal code is required";

            /// <summary>
            /// MSG_STATE_REQUIRED
            /// </summary>
            public const string MSG_STATE_REQUIRED = "If address is provided, state is required";

            /// <summary>
            /// MSG_COUNTRY_REQUIRED
            /// </summary>
            public const string MSG_COUNTRY_REQUIRED = "If address is provided, country is required";

            /// <summary>
            /// MSG_CITY_REQUIRED
            /// </summary>
            public const string MSG_CITY_REQUIRED = "If address is provided, city is required";

            /// <summary>
            /// MSG_ADDRESS1_REQUIRED
            /// </summary>
            public const string MSG_ADDRESS1_REQUIRED = "If address is provided, address1 is required";

            /// <summary>
            /// MSG_ADDRESS2_REQUIRED
            /// </summary>
            public const string MSG_ADDRESS2_REQUIRED = "Address2 is required";

            /// <summary>
            /// MSG_REQUEST_CONTAINS_BOTH_ADDRESS_AND_ID
            /// </summary>
            public const string MSG_REQUEST_CONTAINS_BOTH_ADDRESS_AND_ID = "The request cannot contain both an �address� and an �addressId�";

            /// <summary>
            /// MSG_INVALID_NAME_TITLE
            /// </summary>
            public const string MSG_INVALID_NAME_TITLE = "Invalid name title";

            /// <summary>
            /// MSG_INVALID_CONTACT_TYPE
            /// </summary>
            public const string MSG_INVALID_CONTACT_TYPE = "Invalid contact type";

            /// <summary>
            /// MSG_INVALID_NAME_SUFFIX
            /// </summary>
            public const string MSG_INVALID_NAME_SUFFIX = "Invalid name suffix";

            /// <summary>
            /// MSG_INVALID_DATE_OF_BIRTH
            /// </summary>
            public const string MSG_INVALID_DATE_OF_BIRTH = "Invalid date of birth";

            /// <summary>
            /// MSG_INVALID_PHONE_TYPE
            /// </summary>
            public const string MSG_INVALID_PHONE_TYPE = "Invalid phone type";

            /// <summary>
            /// MSG_INVALID_CUSTOMER_ID
            /// </summary>
            public const string MSG_INVALID_CUSTOMER_ID = "Invalid customer Id";

            /// <summary>
            /// MSG_MUST_BE_STD_EMAIL_FRMT
            /// </summary>
            public const string MSG_MUST_BE_STD_EMAIL_FRMT = "Must be in a standard valid email format ";

            /// <summary>
            /// MSG_CANNOT_START_WITH_SPEC_CHAR
            /// </summary>
            public const string MSG_CANNOT_START_WITH_SPEC_CHAR =  "Cannot start with special characters";

            /// <summary>
            /// MSG_LOGIN_INACTIVE
            /// </summary>
            public const string MSG_LOGIN_INACTIVE = "Login is inactive/disabled";

            /// <summary>
            /// MSG_TEMPORARY_PASSWORD_IS_EXPIRED
            /// </summary>
            public const string MSG_TEMPORARY_PASSWORD_IS_EXPIRED = "Temporary password is expired";

            /// <summary>
            /// MSG_LOGIN_PENDING_ACTIVATION
            /// </summary>
            public const string MSG_LOGIN_PENDING_ACTIVATION = "Login is pending activation";

            /// <summary>
            /// MSG_LOGIN_NOT_LOCKED
            /// </summary>
            public const string MSG_LOGIN_NOT_LOCKED = "Login is not locked";

            /// <summary>
            /// MSG_LOGIN_LOCKED
            /// </summary>
            public const string MSG_LOGIN_LOCKED = "Login is locked";

            /// <summary>
            /// MSG_LOGIN_NOT_ACTIVE
            /// </summary>
            public const string MSG_LOGIN_NOT_ACTIVE = "Account credential is not in the active state";

            /// <summary>
            /// MSG_PWD_PREVIOUSLY_USED
            /// </summary>
            public const string MSG_PWD_PREVIOUSLY_USED =  "Password cannot match recently used passwords";

            /// <summary>
            /// MSG_CONTAINS_A_SPECIAL_CHAR
            /// </summary>
            public const string MSG_CONTAINS_A_SPECIAL_CHAR = "Password must contain special character(s)";

            /// <summary>
            /// MSG_CONTAINS_AT_LEAST_CHARS
            /// </summary>
            public const string MSG_CONTAINS_AT_LEAST_CHARS = "Password must contain letter(s)";

            /// <summary>
            /// MSG_MIXED_CASE_CHARS
            /// </summary>
            public const string MSG_MIXED_CASE_CHARS = "Password must contain mixed case letters";

            /// <summary>
            /// MSG_CONTAINS_DICTIOANARY_WORD
            /// </summary>
            public const string MSG_CONTAINS_DICTIOANARY_WORD = "Password can not contain dictionary word";

            /// <summary>
            /// MSG_CONTAINS_DIGITS
            /// </summary>
            public const string MSG_CONTAINS_DIGITS = "Password must contain digit(s)";

            /// <summary>
            /// MSG_CONTIGUOUS_CHARS
            /// </summary>
            public const string MSG_CONTIGUOUS_CHARS = "Password cannot contain contiguous characters";

            /// <summary>
            /// MSG_PIN_CONTIGUOUS_CHARS
            /// </summary>
            public const string MSG_PIN_CONTIGUOUS_CHARS = "Pin cannot contain contiguous characters";

            /// <summary>
            /// MSG_SAME_CONSECUTIVE_CHARS
            /// </summary>
            public const string MSG_SAME_CONSECUTIVE_CHARS = "Password cannot contain the same character consecutively";

            /// <summary>
            /// MSG_PIN_SAME_CONSECUTIVE_CHARS
            /// </summary>
            public const string MSG_PIN_SAME_CONSECUTIVE_CHARS = "Pin cannot contain the same character consecutively";

            /// <summary>
            /// MSG_PASSWORD_CAN_NOT_CONTAIN_USERNAME
            /// </summary>
            public const string MSG_PASSWORD_CAN_NOT_CONTAIN_USERNAME = "Password can not contain username in it";

            /// <summary>
            /// MSG_PIN_CAN_NOT_CONTAIN_USERNAME
            /// </summary>
            public const string MSG_PIN_CAN_NOT_CONTAIN_USERNAME = "Pin can not contain username in it";

            /// <summary>
            /// MSG_MAXIMUM_LENGTH_EXCEEDED
            /// </summary>
            public const string MSG_MAXIMUM_LENGTH_EXCEEDED = "Maximum length exceeded";

            /// <summary>
            /// MSG_MINIMUM_LENGTH_NOT_MET
            /// </summary>
            public const string MSG_MINIMUM_LENGTH_NOT_MET = "Minimum length not met";

            /// <summary>
            /// MSG_VALUE_CANNOT_REMOVED
            /// </summary>
            public const string MSG_VALUE_CANNOT_REMOVED = "Value cannot be removed";

            /// <summary>
            /// MSG_FIELD_IS_BLANK
            /// </summary>
            public const string MSG_FIELD_IS_BLANK = "Field is blank";

            /// <summary>
            /// MSG_CONTACT_NOT_FOUND
            /// </summary>
            public const string MSG_CONTACT_NOT_FOUND = "The contact not found";

            /// <summary>
            /// MSG_ADDRESS_NOT_FOUND
            /// </summary>
            public const string MSG_ADDRESS_NOT_FOUND = "The address record was not found";

            /// <summary>
            /// MSG_CUSTOMER_NOT_FOUND
            /// </summary>
            public const string MSG_CUSTOMER_NOT_FOUND = "The Customer not found";

            /// <summary>
            /// MSG_USERNAME_NOT_FOUND
            /// </summary>
            public const string MSG_USERNAME_NOT_FOUND = "The Username not found";

            /// <summary>
            /// MSG_ORGANIZATION_NOT_FOUND
            /// </summary>
            public const string MSG_ORGANIZATION_NOT_FOUND = "The Organization record not found";

            /// <summary>
            /// MSG_ADDRESS_ID_NULL
            /// </summary>
            public const string MSG_ADDRESS_ID_NULL = "AddressId is null";

            /// <summary>
            /// MSG_CUSTOMER_ID_NULL
            /// </summary>
            public const string MSG_CUSTOMER_ID_NULL = "CustomerId is null";

            /// <summary>
            /// MSG_VALUE_ALREADY_EXISTS
            /// </summary>
            public const string MSG_VALUE_ALREADY_EXISTS = "Value already exists";

            /// <summary>
            /// MSG_USERNAME_AND_PASSWORD_INVALID
            /// </summary>
            public const string MSG_USERNAME_AND_PASSWORD_INVALID = "The username or password is invalid";

            /// <summary>
            /// MSG_UNABLE_FIND_ACCOUNT
            /// </summary>
            public const string MSG_UNABLE_FIND_ACCOUNT = "Unable to find an account matching the provided data";

            /// <summary>
            /// MSG_UNABLE_FIND_UNIQUE_ACCOUNT
            /// </summary>
            public const string MSG_UNABLE_FIND_UNIQUE_ACCOUNT = "Unable to uniquely identify the account from the information provided";

            /// <summary>
            /// MSG_EMAIL_OR_PHONE_REQUIRED
            /// </summary>
            public const string MSG_EMAIL_OR_PHONE_REQUIRED = "At least email or phone must be provided";

            /// <summary>
            /// MSG_ACCOUNT_SECURITY_QA_MISSING
            /// </summary>
            public const string MSG_ACCOUNT_SECURITY_QA_MISSING = "Account has no security question answer pairs on file";

            /// <summary>
            /// MSG_SECURITY_QA_INCORRECT
            /// </summary>
            public const string MSG_SECURITY_QA_INCORRECT = "One or more of the provided security answers did not match the answers on the account";

            /// <summary>
            /// MSG_POSTAL_CODE_INVALID_FOR_COUNTRY
            /// </summary>
            public const string MSG_POSTAL_CODE_INVALID_FOR_COUNTRY = "Postal/Zip code is invalid for the specified country";

            /// <summary>
            /// MSG_POSTAL_CODE_INVALID_FORMAT_FOR_COUNTRY
            /// </summary>
            public const string MSG_POSTAL_CODE_INVALID_FORMAT_FOR_COUNTRY = "Postal/Zip code format is invalid for the specified country";

            /// <summary>
            /// MSG_INVALID_CUSTOMER_TYPE
            /// </summary>
            public const string MSG_INVALID_CUSTOMER_TYPE = "Invalid customer type";

            /// <summary>
            /// MSG_SECURITY_ANSWER_NOT_FOUND
            /// </summary>
            public const string MSG_SECURITY_ANSWER_NOT_FOUND = "Security answer record was not found";

            /// <summary>
            /// MSG_INVALID_PERSONAL_IDENTIFIER_TYPE
            /// </summary>
            public const string MSG_INVALID_PERSONAL_IDENTIFIER_TYPE = "Invalid personal identifier type";

            /// <summary>
            /// MSG_GENERAL_UNABLE_CREATE_ACCOUNT
            /// </summary>
            public const string MSG_GENERAL_UNABLE_CREATE_ACCOUNT = "Unable to enter new customer record to the system";

            /// <summary>
            /// MSG_GENERAL_UNABLE_CREATE_CONTACT
            /// </summary>
            public const string MSG_GENERAL_UNABLE_CREATE_CONTACT = "Unable to enter new contact record to the system";

            /// <summary>
            /// MSG_GENERAL_NONNUMERIC_CHARCTERS
            /// </summary>
            public const string MSG_GENERAL_NONNUMERIC_CHARCTERS ="Field contains Non-Numeric characters";

            /// <summary>
            /// MSG_SECURITY_ANSWER_REQUIRED
            /// </summary>
            public const string MSG_SECURITY_ANSWER_REQUIRED = "If security QA is provided, security answer is required";

            /// <summary>
            /// MSG_SECURITY_INVALID_QUESTION
            /// </summary>
            public const string MSG_SECURITY_INVALID_QUESTION = "Invalid security question";

            /// <summary>
            /// MSG_SECURITY_ANSWER_MAX_LENGTH_EXCEED
            /// </summary>
            public const string MSG_SECURITY_ANSWER_MAX_LENGTH_EXCEED = "Maximum length exceeded";

            /// <summary>
            /// MSG_PERSONAL_IDENTIFIER_VALUE_REQUIRED
            /// </summary>
            public const string MSG_PERSONAL_IDENTIFIER_VALUE_REQUIRED = "If personal identifier info is provided, personal identifier is required";

            /// <summary>
            /// MSG_PERSONAL_IDENTIFIER_TYPE_REQUIRED
            /// </summary>
            public const string MSG_PERSONAL_IDENTIFIER_TYPE_REQUIRED = "If personal identifier info is provided, personal identifier type is required";

            /// <summary>
            /// MSG_GENERAL_RECORD_ID_NOT_A_VALID_RECORD_ID
            /// </summary>
            public const string MSG_GENERAL_RECORD_ID_NOT_A_VALID_RECORD_ID = "The record id is not a valid GUID";

            /// <summary>
            /// MSG_GENERAL_TOKEN_TYPE_UNKNOWN
            /// </summary>
            public const string MSG_GENERAL_TOKEN_TYPE_UNKNOWN = "Invalid token type";

            /// <summary>
            /// Invalid requirement type used in customer/<customer-id>/notificationpreferences GET API Errors
            /// </summary>
            public const string MSG_REQ_TYPE_INVALID = "Invalid requirement type";

            /// <summary>
            /// Invalid requirement type used in customer/<customer-id>/notificationpreferences GET API Errors
            /// </summary>
            public const string MSG_CHANNEL_INVALID = "Invalid channel";

            /// <summary>
            /// ERR_ACCNT_CLOSED_OR_PENDING
            /// </summary>
            public const string MSG_ACCNT_CLOSED_OR_PENDING = "The account is closed or pending closed";

            /// <summary>
            ///  2-141. customer/<customer-id>/contact/<contact-id>/credentials/validate POST
            ///  Field: contact-id; Error = errors.general.record.not.found. The contact not found.
            /// </summary>
            public const string ERR_CONTACT_NOT_FOUND = "The contact not found.";


            /// <summary>
            /// 2-141. customer/<customer-id>/contact/<contact-id>/credentials/validate POST
            /// errors.invalid.user.or.password. The pin is invalid.
            /// </summary>
            public const string ERR_INVALID_PIN = "The pin is invalid.";

            /// <summary>
            /// 2-141. customer/<customer-id>/contact/<contact-id>/credentials/validate POST
            /// Invalid status action.
            /// </summary>
            public const string ERR_GENERAL_INVALID_STATUS = "Invalid status action.";
        }

        #region Password
        /// <summary>
        /// 
        /// </summary>
        public static class Password
        {

            /// <summary>
            /// PASSWORD_VALIDATION
            /// </summary>
            public const string PASSWORD_VALIDATION = "PasswordValidation";

            /// <summary>
            /// PASSWORD_MIN_LENGTH
            /// </summary>
            public const string PASSWORD_MIN_LENGTH = "PasswordMinLength";

            /// <summary>
            /// PASSWORD_MAX_LENGTH
            /// </summary>
            public const string PASSWORD_MAX_LENGTH = "PasswordMaxLength";

            /// <summary>
            /// PASSWORD_UPPER_ALPHA
            /// </summary>
            public const string PASSWORD_UPPER_ALPHA = "PasswordUpperAlpha";

            /// <summary>
            /// PASSWORD_LOWER_ALPHA
            /// </summary>
            public const string PASSWORD_LOWER_ALPHA = "PasswordLowerAlpha";

            /// <summary>
            /// PASSWORD_NUMERIC
            /// </summary>
            public const string PASSWORD_NUMERIC = "PasswordNumeric";

             /// <summary>
            /// PASSWORD_NON_ALPHANUMERIC
            /// </summary>
            public const string PASSWORD_NON_ALPHANUMERIC = "PasswordNonAlpha";

           /// <summary>
            /// PASSWORD_ALLOW_USERNAME
            /// </summary>
            public const string PASSWORD_ALLOW_USERNAME = "PasswordAllowUsername";

            /// <summary>
            /// PASSWORD_ALLOW_CONSECUTIVE_CHARS
            /// </summary>
            public const string PASSWORD_ALLOW_CONSECUTIVE_CHARS = "PasswordAllowConsecutiveChars";

            /// <summary>
            /// PASSWORD_ALLOW_CONTIGUOUS_CHARS
            /// </summary>
            public const string PASSWORD_ALLOW_CONTIGUOUS_CHARS = "PasswordAllowContiguousChars";

            /// <summary>
            /// PASSWORD_ALLOW_DICTIONARY_WORDS
            /// </summary>
            public const string PASSWORD_ALLOW_DICTIONARY_WORDS = "PasswordAllowDictionaryWords";

            /// <summary>
            /// TEMP_PASSWORD_DURATION_DAYS
            /// </summary>
            public const string TEMP_PASSWORD_DURATION_DAYS = "TempPasswordDuartionDays";

            /// <summary>
            /// NUMBER_OF_PASSWORDS_ARCHIVE
            /// </summary>
            public const string NUMBER_OF_PASSWORDS_ARCHIVE = "NumberOfPasswordsToArchive";
        }

        /// <summary>
        /// Pin
        /// </summary>
        public static class Pin
        {
            /// <summary>
            /// PIN_VALIDATION
            /// </summary>
            public const string PIN_VALIDATION = "PinValidation";

            /// <summary>
            /// PIN_MIN_LENGTH
            /// </summary>
            public const string PIN_MIN_LENGTH = "PinMinLength";

            /// <summary>
            /// PIN_MAX_LENGTH
            /// </summary>
            public const string PIN_MAX_LENGTH = "PinMaxLength";

            /// <summary>
            /// PIN_NUMERIC_ONLY
            /// </summary>
            public const string PIN_NUMERIC_ONLY = "PinNumericOnly";
        }

        /// <summary>
        /// Email
        /// </summary>
        public static class Email
        {
            /// <summary>
            /// EMAIL_FORMAT_VALIDATION
            /// </summary>
            public const string EMAIL_FORMAT_VALIDATION = "ValidateEmailFormat";

            /// <summary>
            /// EMAIL_MATCH_REGEX
            /// </summary>
            public const string EMAIL_MATCH_REGEX = "EmailMatchRegex";
        }
        #endregion

        #region MemoryCache
        /// <summary>
        /// MemoryCache
        /// </summary>
        public static class MemoryCache
        {
            /// <summary>
            /// CACHE_EXPIRATION_HOURS
            /// </summary>
            public static string CACHE_EXPIRATION_HOURS = "CacheExpirationHours";

            /// <summary>
            /// NIS_API_BASE_URL
            /// </summary>
            public static string NIS_API_BASE_URL = "NISApiBaseURL";

            /// <summary>
            /// NIS_API_COMP_REG_PATH
            /// </summary>
            public static string NIS_API_COMP_REG_PATH = "NISCompleteRegistrationPath";

            /// <summary>
            /// NIS_API_USERNAME
            /// </summary>
            public static string NIS_API_USERNAME = "NISApiUsername";

            /// <summary>
            /// NIS_API_PASSWORD
            /// </summary>
            public static string NIS_API_PASSWORD = "NISApiPassword";

            /// <summary>
            /// NIS_API_DEVICE
            /// </summary>
            public static string NIS_API_DEVICE = "NISApiDevice";

            /// <summary>
            /// NIS_API_SEARCH_TOKEN_PATH
            /// </summary>
            public static string NIS_API_SEARCH_TOKEN_PATH = "SearchTokenPath";
        }

        #endregion MemoryCache

        /// <summary>
        /// Common
        /// </summary>
        public static class Common
        {
            /// <summary>
            /// gmaildotcom
            /// </summary>
            public const string gmaildotcom = "gmail.com";

            /// <summary>
            /// SearchWildCharacter
            /// </summary>
            public static readonly string SearchWildCharacter = "*";
        }

        /// <summary>
        /// TransitAccountConstants
        /// </summary>
        public static class TransitAccountConstants
        {
            /// <summary>
            /// ACCOUNT_ID
            /// </summary>
            public const string ACCOUNT_ID = "AccountId";

            /// <summary>
            /// SUBSYSTEM
            /// </summary>
            public const string SUBSYSTEM = "Subsystem";


            /// <summary>
            /// PLUGIN_CONTEXT_NOT_PASSED
            /// </summary>
            public const string PLUGIN_CONTEXT_NOT_PASSED = "Plugin Error: LocalContext not passed.";
            /*
             * Account Status
             */
            /// <summary>
            /// STATUS_ACTION_NAME
            /// </summary>
            public const string STATUS_ACTION_NAME = "cub_TransitAccountStatusByAccountId";

            /// <summary>
            /// ACCOUNT_STATUS_PLUGIN_NAME
            /// </summary>
            public const string ACCOUNT_STATUS_PLUGIN_NAME = "TransitAccountStatusByIdPlugin";

            /// <summary>
            /// ACCOUNT_STATUS_OUTPUT_PARAM
            /// </summary>
            public const string ACCOUNT_STATUS_OUTPUT_PARAM = "AccountStatus";

            /// <summary>
            /// START_DATE_INPUT_PARAM
            /// </summary>
            public const string START_DATE_INPUT_PARAM = "StartDate";

            /// <summary>
            /// END_DATE_INPUT_PARAM
            /// </summary>
            public const string END_DATE_INPUT_PARAM = "EndDate";

            /*
             * Account Balance History
             */

            /// <summary>
            /// BALANCE_HISTORY_ACTION_NAME
            /// </summary>
            public const string BALANCE_HISTORY_ACTION_NAME = "cub_GlobalBalanceHistoryAction";

            /// <summary>
            /// BALANCE_HISTORY_PLUGIN_NAME
            /// </summary>
            public const string BALANCE_HISTORY_PLUGIN_NAME = "GlobalBalanceHistoryAction";

            /// <summary>
            /// BALANCE_HISTORY_OUTPUT_PARAM
            /// </summary>
            public const string BALANCE_HISTORY_OUTPUT_PARAM = "AccountBalanceHistory";

            /*
             * Account Travel History
             */
            /// <summary>
            /// TRAVEL_HISTORY_ACTION_NAME
            /// </summary>
            public const string TRAVEL_HISTORY_ACTION_NAME = "cub_GlobalTravelHistoryAction";

            /// <summary>
            /// TRAVEL_HISTORY_PLUGIN_NAME
            /// </summary>
            public const string TRAVEL_HISTORY_PLUGIN_NAME = "GlobalTravelHistoryAction";


            /// <summary>
            /// TRAVEL_HISTORY_OUTPUT_PARAM
            /// </summary>
            public const string TRAVEL_HISTORY_OUTPUT_PARAM = "AccountTravelHistory";
        }

        /// <summary>
        /// GlobalMasterSearchCardConstants
        /// </summary>
        public static class GlobalMasterSearchCardConstants
        {
            /// <summary>
            /// PLUGIN_NAMe
            /// </summary>
            public const string PLUGIN_NAMe = "GlobalMasterSearchCard";


            /// <summary>
            /// ACTION_NAME
            /// </summary>
            public const string ACTION_NAME = "cub_GlobalMasterSearchCard";

            /// <summary>
            /// PARAM_TOKEN_TYPE
            /// </summary>
            public const string PARAM_TOKEN_TYPE = "TokenType";

            /// <summary>
            /// PARAM_SUB_SYSTEM
            /// </summary>
            public const string PARAM_SUB_SYSTEM = "SubSystem";

            /// <summary>
            /// PARAM_BANK_CARD_NUMBER
            /// </summary>
            public const string PARAM_BANK_CARD_NUMBER = "BankCardNumber";

            /// <summary>
            /// PARAM_CARD_EXPIRATION_MONTH
            /// </summary>
            public const string PARAM_CARD_EXPIRATION_MONTH = "CardExpirationMonth";

            /// <summary>
            /// PARAM_CARD_EXPIRATION_YEAR
            /// </summary>
            public const string PARAM_CARD_EXPIRATION_YEAR = "CardExpirationYear";

            /// <summary>
            /// PARAM_RETURN
            /// </summary>
            public const string PARAM_RETURN = "ReturnResult";

            /// <summary>
            /// ERROR_PARAM_TOKEN_TYPE_MISSING
            /// </summary>
            public const string ERROR_PARAM_TOKEN_TYPE_MISSING = "ERROR: Required Paramter value is missing Token Type ";

            /// <summary>
            /// ERROR_PARAM_SUB_SYSTEM_MISSING
            /// </summary>
            public const string ERROR_PARAM_SUB_SYSTEM_MISSING = "ERROR: Required Paramter value is missing Sub System";

            /// <summary>
            /// ERROR_PARAM_BANK_CARD_NUMBER_MISSING
            /// </summary>
            public const string ERROR_PARAM_BANK_CARD_NUMBER_MISSING = "ERROR: Required Paramter value is missing Bank Card Number";

            /// <summary>
            /// ERROR_PARAM_CARD_EXPIRATION_MONTH_MISSING
            /// </summary>
            public const string ERROR_PARAM_CARD_EXPIRATION_MONTH_MISSING = "ERROR: Required Paramter value is missing Card Expiration Month";

            /// <summary>
            /// ERROR_PARAM_CARD_EXPIRATION_YEAR_MISSING
            /// </summary>
            public const string ERROR_PARAM_CARD_EXPIRATION_YEAR_MISSING = "ERROR: Required Paramter value is missing Card Expiration Year";
        }

        /// <summary>
        /// GlobalMasterSearchContactConstants
        /// </summary>
        public static class GlobalMasterSearchContactConstants
        {
            /// <summary>
            /// PLUGIN_NAME
            /// </summary>
            public const string PLUGIN_NAME = "GlobalMasterSearchContact";

            /// <summary>
            /// ACTION_NAME
            /// </summary>
            public const string ACTION_NAME = "cub_GlobalMasterSearchContact";

            /// <summary>
            /// PARAM_CONTACT_FIRST_NAME
            /// </summary>
            public const string PARAM_CONTACT_FIRST_NAME = "ContactFirstName";

            /// <summary>
            /// PARAM_CONTACT_LAST_NAME
            /// </summary>
            public const string PARAM_CONTACT_LAST_NAME = "ContactLastName";

            /// <summary>
            /// PARAM_CONTACT_EMAIL
            /// </summary>
            public const string PARAM_CONTACT_EMAIL = "ContactEmail";

            /// <summary>
            /// PARAM_CONTACT_PHONE
            /// </summary>
            public const string PARAM_CONTACT_PHONE = "ContactPhoneNumber";

            /// <summary>
            /// PARAM_RETURN
            /// </summary>
            public const string PARAM_RETURN = "ReturnContacts";

        }

        /// <summary>
        /// GlobalMasterSearchCaseConstants
        /// </summary>
        public static class GlobalMasterSearchCaseConstants
        {
            /// <summary>
            /// PLUGIN_NAME
            /// </summary>
            public const string PLUGIN_NAME = "GlobalMasterSearchCase";


            /// <summary>
            /// ACTION_NAME
            /// </summary>
            public const string ACTION_NAME = "cub_GlobalMasterSeacrhCase";

            /// <summary>
            /// PARAM_CASE_NUMBER
            /// </summary>
            public const string PARAM_CASE_NUMBER = "CaseNumber";

            /// <summary>
            /// PARAM_RETURN
            /// </summary>
            public const string PARAM_RETURN = "ReturnCases";

            /// <summary>
            /// ERROR_PARAM_CASE_NUMBER_MISSING
            /// </summary>
            public const string ERROR_PARAM_CASE_NUMBER_MISSING = "ERROR: Required Paramter value is missing Case Number ";
        }

        /// <summary>
        /// GlobalGetUserInfoConstants
        /// </summary>
        public static class GlobalGetUserInfoConstants
        {
            /// <summary>
            /// PLUGIN_NAME
            /// </summary>
            public const string PLUGIN_NAME = "GlobalGetUserInfo";

            /// <summary>
            /// ACTION_NAME
            /// </summary>
            public const string ACTION_NAME = "cub_GlobalGetUserInfo";

            /// <summary>
            /// PARAM_USERID
            /// </summary>
            public const string PARAM_USERID = "UserID"; // input - not used yet

            /// <summary>
            /// PARAM_USERNAME
            /// </summary>
            public const string PARAM_USERNAME = "Username";

            /// <summary>
            /// PARAM_ROLES
            /// </summary>
            public const string PARAM_ROLES = "Roles";

            /// <summary>
            /// PARAM_APPVERSION
            /// </summary>
            public const string PARAM_APPVERSION = "AppVersion";

            /// <summary>
            /// PARAM_SYSTEMNAME
            /// </summary>
            public const string PARAM_SYSTEMNAME = "SystemName";

            /// <summary>
            /// PARAM_CLIENTNAME
            /// </summary>
            public const string PARAM_CLIENTNAME = "ClientName";

            /// <summary>
            /// PARAM_DOMAINNAME
            /// </summary>
            public const string PARAM_DOMAINNAME = "DomainName";

            /// <summary>
            /// PARAM_BUSINESSUNIT
            /// </summary>
            public const string PARAM_BUSINESSUNIT = "BusinessUnit";

            /// <summary>
            /// PARAM_CURRENCY
            /// </summary>
            public const string PARAM_CURRENCY = "Currency";

            /// <summary>
            /// PARAM_RECORDID
            /// </summary>
            public const string PARAM_RECORDID = "RecordID";

            /// <summary>
            /// PARAM_SVCVERSION
            /// </summary>
            public const string PARAM_SVCVERSION = "ServiceVersion";
        }

        /// <summary>
        /// GlobalGetGlobalsAttributeValueConstants
        /// </summary>
        public static class GlobalGetGlobalsAttributeValueConstants
        {
            /// <summary>
            /// PLUGIN_NAME
            /// </summary>
            public const string PLUGIN_NAME = "GlobalGetGlobalsAttributeValueAction";

            /// <summary>
            /// ACTION_NAME
            /// </summary>
            public const string ACTION_NAME = "cub_GlobalGetGlobalsAttributeValue";

            /// <summary>
            /// GLOBAL_PARAM
            /// </summary>
            public const string GLOBAL_PARAM = "Global";

            /// <summary>
            /// GLOBAL_DETAIL_PARAM
            /// </summary>
            public const string GLOBAL_DETAIL_PARAM = "GlobalDetail";

            /// <summary>
            /// ATTRIBUTE_VALUE_PARAM
            /// </summary>
            public const string ATTRIBUTE_VALUE_PARAM = "AttributeValue";
        }

        /// <summary>
        /// GlobalCustomerRegisterConstants
        /// </summary>
        public static class GlobalCustomerRegisterConstants
        {
            /// <summary>
            /// ACCOUNT_TYPES_FOR_UI
            /// </summary>
            public const string ACCOUNT_TYPES_FOR_UI = "ACCOUNT_TYPES_FOR_UI";

            /// <summary>
            /// CONTACT_TYPES_FOR_UI
            /// </summary>
            public const string CONTACT_TYPES_FOR_UI = "CONTACT_TYPES_FOR_UI";

            /// <summary>
            /// COUNTRIES_FOR_UI
            /// </summary>
            public const string COUNTRIES_FOR_UI = "COUNTRIES_FOR_UI";

            /// <summary>
            /// STATES_FOR_UI
            /// </summary>
            public const string STATES_FOR_UI = "STATES_FOR_UI";

            /// <summary>
            /// SECURITY_QUESTION_FOR_UI
            /// </summary>
            public const string SECURITY_QUESTION_FOR_UI = "SECURITY_QUESTION_FOR_UI";
        }

        /// <summary>
        /// GlobalCallingFromConstants
        /// </summary>
        public static class GlobalCallingFromConstants
        {
            /// <summary>
            /// CALLING_FROM_WEB
            /// </summary>
            public const string CALLING_FROM_WEB = "CALLING_FROM_WEB";

            /// <summary>
            /// CALLING_FROM_IVR
            /// </summary>
            public const string CALLING_FROM_IVR = "CALLING_FROM_IVR";

            /// <summary>
            /// CALLING_FROM_MOBILE
            /// </summary>
            public const string CALLING_FROM_MOBILE = "CALLING_FROM_MOBILE";
        }

        /// <summary>
        /// GlobalCustomerRegistrationConstants
        /// </summary>
        public static class GlobalCustomerRegistrationConstants
        {
            /// <summary>
            /// PLUGIN_NAME
            /// </summary>
            public const string PLUGIN_NAME = "GlobalCustomerRegistration";

            /// <summary>
            /// ACTION_NAME
            /// </summary>
            public const string ACTION_NAME = "cub_GlobalCustomerRegistration";

            /// <summary>
            /// INPUT_PARAMETER_NAME
            /// </summary>
            public const string INPUT_PARAMETER_NAME = "Input";

            /// <summary>
            /// OUTPUT_PARAMETER_CUSTOMER_ID_NAME
            /// </summary>
            public const string OUTPUT_PARAMETER_CUSTOMER_ID_NAME = "CustomerID";

            /// <summary>
            /// OUTPUT_PARAMETER_CONTACT_ID_NAME
            /// </summary>
            public const string OUTPUT_PARAMETER_CONTACT_ID_NAME = "ContactID";
        }

        #region Log4net
        /// <summary>
        /// Log4Net
        /// </summary>
        public static class Log4Net
        {
            /// <summary>
            /// LOG_TABLE_GENERAL
            /// </summary>
            public const string LOG_TABLE_GENERAL = "general";

            /// <summary>
            /// ORG_UNKNOWN
            /// </summary>
            public const string ORG_UNKNOWN = "UnknownOrg";
        }
        #endregion
    }
}
