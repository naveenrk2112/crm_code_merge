/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUB.MSD.Model
{

    /// <summary>
    /// ZipPostalCodeModel
    /// </summary>
    public class ZipPostalCodeModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// ZipPostal
        /// </summary>
        public string ZipPostal { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public KeyValuePair<Guid, string> Country { get; set; }

        /// <summary>
        /// State
        /// </summary>
        public KeyValuePair<Guid, string> State { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// CodeType
        /// </summary>
        public string CodeType { get; set; }
    }
}
