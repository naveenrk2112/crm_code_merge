/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Tooling.Connector;

namespace ConsoleApp
{
    //for use with the Console App

    public static class Global
    {
        public static IOrganizationService ReturnService( )
        {
            //This functionality needs to be overloaded so that it can build the connection from a url 

            // See this post about the format of connection strings: https://community.dynamics.com/crm/b/dynamicscrmtipoftheday/archive/2016/01/14/tip-556-rumors-about-microsoft-xrm-client-death-are-exaggerated
            //      CrmServiceClient conn = new CrmServiceClient(
            //          "ServiceUri=https://orgname.contoso.com/orgname;" + 
            //          "AuthType=IFD;Domain=anything;" + 
            //          "UserName=george@contoso.com;Password=pass@word1;" + 
            //          "LoginPrompt=Never;");
            //      - Url must be in the form of https://orgname.contoso.com/orgname. For on-premises and IFD deployments the connector expects orgname to be at the end and looks like it does not make any attempt to deduce orgname from the server url.
            //      - Domain name must be specified but it�s not passed via claims, so it can be anything. Really any non-empty string o__O
            //      - Username must be UPN. If it�s not, then, since domain name is not passed it, ADFS 3.0 throws a fit (ADFS 2.0 assumes the domain)

            String connectionString = ConfigurationManager.ConnectionStrings[1].ConnectionString;  //build the connection string based upon request from url or context of plugin / workflow

            var connection = new CrmServiceClient(connectionString);
            return (IOrganizationService)connection.OrganizationWebProxyClient != null ? (IOrganizationService)connection.OrganizationWebProxyClient : (IOrganizationService)connection.OrganizationServiceProxy;
        }

        //this is just some temp code to ensure I can set the callerId when testing from the console app
        public static OrganizationServiceProxy ReturnOrganizationServiceProxy()
        {
            //This functionality needs to be overloaded so that it can build the connection from a url 

            // See this post about the format of connection strings: https://community.dynamics.com/crm/b/dynamicscrmtipoftheday/archive/2016/01/14/tip-556-rumors-about-microsoft-xrm-client-death-are-exaggerated
            //      CrmServiceClient conn = new CrmServiceClient(
            //          "ServiceUri=https://orgname.contoso.com/orgname;" + 
            //          "AuthType=IFD;Domain=anything;" + 
            //          "UserName=george@contoso.com;Password=pass@word1;" + 
            //          "LoginPrompt=Never;");
            //      - Url must be in the form of https://orgname.contoso.com/orgname. For on-premises and IFD deployments the connector expects orgname to be at the end and looks like it does not make any attempt to deduce orgname from the server url.
            //      - Domain name must be specified but it�s not passed via claims, so it can be anything. Really any non-empty string o__O
            //      - Username must be UPN. If it�s not, then, since domain name is not passed it, ADFS 3.0 throws a fit (ADFS 2.0 assumes the domain)

            String connectionString = ConfigurationManager.ConnectionStrings[1].ConnectionString;  //build the connection string based upon request from url or context of plugin / workflow

            var connection = new CrmServiceClient(connectionString);
            //return (IOrganizationService)connection.OrganizationWebProxyClient != null ? (IOrganizationService)connection.OrganizationWebProxyClient : (IOrganizationService)connection.OrganizationServiceProxy;
            return connection.OrganizationServiceProxy;
        }
    }
}
