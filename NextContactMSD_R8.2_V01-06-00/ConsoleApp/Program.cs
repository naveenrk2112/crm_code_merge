/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUB.MSD.Logic;
//using ConsoleApp.PluginHelpers;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using CUB.MSD.Model;
using System.Data;
using System.Data.Common;
//using System.Data.EntityClient;
//using System.Data.Metadata.Edm;
//using System.Data.Objects;
using System.Data.SqlClient;
using System.Net;
using System.IO;
//using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using MergeOption = Microsoft.Xrm.Sdk.Client.MergeOption;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ConsoleApp
{
    class WSMSDResponse : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private List<WSMSDFault> errorsMessages;
        public bool Success { get; set; } = true;
        public List<WSMSDFault> ErrorsMessages
        {
            get
            { return errorsMessages; }
            set
            {
               errorsMessages = value;
               Success = errorsMessages.Count ==0;
            }
        }
       
    }

    public class WSMSDFault
    {
        public string fieldName { get; set; }
        public string errorKey { get; set; }
        public string errorMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="errorKey"></param>
        /// <param name="errorMessage"></param>
        public WSMSDFault(string field_Name, string error_Key, string error_Message)
        {
            fieldName = field_Name;
            errorKey = error_Key;
            errorMessage = error_Message;
        }
    }



    internal class Program
    {
        private IOrganizationService _service;
        private static string _orgName;

        /// <summary>
        /// Base64Decode
        /// </summary>
        /// <param name="encryptedValue"></param>
        /// <returns>decoded string</returns>
        public static string Base64Decode(string encryptedValue)
        {
            string return_string = null;

            try
            {
                byte[] decodedBytes = Convert.FromBase64String(encryptedValue);
                return_string = new ASCIIEncoding().GetString(decodedBytes);
            }
            catch (Exception e)
            {
                //log the error
            }

            return return_string;
        }

        /// <summary>
        /// Base 64 Encode
        /// </summary>
        /// <param name="plainText">Plain text</param>
        /// <returns>Base 64 encoded</returns>
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private static void Main(string[] args)
        {
            // breakthebuild

            IOrganizationService service = Global.ReturnService();

            //  NISApiLogic nisApi = new NISApiLogic(service);
            //var result = nisApi.CustomerFundingSourcePostCCClear("2B9634F0-3F29-E711-80EA-005056813E0D", "5555555555557777", "0120", "Bill Smith", "12345","First Street","US","1",false);
            // var result = nisApi.CustomerFundingSourcePostCCRef("2B9634F0-3F29-E711-80EA-005056813E0D", "2", "1111", "0120", "Bill Smith", "Visa", "1", false);
            //var result = nisApi.CustomerFundingSourcePostDDClear("2B9634F0-3F29-E711-80EA-005056813E0D", "Bill Smith", "000123456789", "11000-000", "C", "Chase", "0", false);
            //  var result = nisApi.CustomerFundingSourcePatchCC("2B9634F0-3F29-E711-80EA-005056813E0D", "1", "507508", "0120", "John Q Smith", "11 Elm", "US", "92008", "1", true);

            // var result = nisApi.CustomerFundingSourceDelete ("2B9634F0-3F29-E711-80EA-005056813E0D", "20");



            // MC: 5105105105105100  Visa: 4012888888881881
            //   public CubResponse<WSCustomerFundingSourcePostResponse> CustomerFundingSourcePost(string customerId, string credit_clear_pan, string credit_clear_cardExpiryMMYY, string credit_clear_nameOnCard, string credit_clear_postalcode, string credit_clear_address1, string credit_clear_country,
            //string credit_ref_pgCardId, string credit_ref_maskedPan, string credit_ref_cardExpiryMMYY, string credit_ref_nameOnCard, string credit_ref_creditCardType,
            //string debit_clear_nameOnAccount, string debit_clear_bankAccountNumber, string debit_clear_bankRoutingNumber, string debit_clear_accountType, string debit_clear_financialInstitutionName,
            //string debit_ref_paymentType, int debit_ref_paymentAmount,
            //string billingAddressId, bool? setAsPrimary)

            //test encode/decode user (demo/)
            //var userE = Base64Encode("svc.msdyn.wcfwebsrv");
            //var userD = Base64Decode(userE);

            //////test encode/decode pword (demo)
            //var pwordE = Base64Encode("0SodageochEmicalpD5$");
            //var pwordD = Base64Decode(pwordE);

            //////test encode/decode domain (demo)
            //var domainE = Base64Encode("DEMO");
            //var domainD = Base64Decode(domainE);

            //test encode/decode pword (dev)
            //var encoded = Base64Encode("bnUVi14bpJOo4KGV0RKj");
            //var decoded = Base64Decode(encoded);

            //test encode/ decode user(eil /)
            var userE = Base64Encode("svc.cra.nyc.eil");
            var userD = Base64Decode(userE);

            //test encode/decode pword (eil)
            var encoded2 = Base64Encode("LNfmrNataQPeuWq%Xr1IQZ");
            var decoded2 = Base64Decode(encoded2);

            //test encode/decode domain (eil)
            var domainE = Base64Encode("ctslab");
            var domainD = Base64Decode(domainE);

            //test encode/decode user (pil)
            //var userE = Base64Encode("svc_msdyn-wcfweb");
            //var userD = Base64Decode(userE);

            ////test encode/decode pword (pil)
            //var pwordE = Base64Encode("Pr0d1nTla8@cT$");
            //var pwordD = Base64Decode(pwordE);

            //////test encode/decode domain (pil)
            //var domainE = Base64Encode("pil");
            //var domainD = Base64Decode(domainE);

           
            //logic
            // lookup cub_country.cub_countryid = contact.cub_countryid and fetch cub_country.cub_countrycode, set contact.address1_country to value
            // lookup cub_stateorprovince.cub_stateorprovinceid = contact.cub_stateorprovinceid and fetch cub_stateorprovince.cub_abbreviation, set contact.address1_stateorprovince to value
            // set contact.address1_postalcode to contact.cub_postalcodeidname

            //   OrganizationRequest req = new OrganizationRequest("cub_GlobalCheckEmailAction");
            //    req["EmailAddress"] = "timmy.klooster@cubic.com";
            //    OrganizationResponse response = service.Execute(req);

            //WhoAmIRequest request = new WhoAmIRequest();
            //WhoAmIResponse response = (WhoAmIResponse)service.Execute(request);
            //if (response != null)
            //{
            //    var userId = response.UserId;
            //    var requestConfig = new cub_GlobalCheckEmailActionRequest()
            //    {
            //        // Target = new EntityReference("systemuser", userId),
            //    };

            //    var responseConfig = (cub_GlobalCheckEmailActionResponse)service.Execute(requestConfig);
            //    string ret = responseConfig.DuplicateID;

            //}

            //req["Target"] = new EntityReference("none", new Guid(""));

            //ContactLogic contactLogic = new ContactLogic(service);
            //var ret = contactLogic.GetCountryById(new Guid("61F3898E-D8C0-E611-80E2-005056814178"));
            //var countryCode = ret.cub_alpha2;
            //var ret2 = contactLogic.GetStateById(new Guid("79FB342C-DDC0-E611-80E0-005056813F0C"));
            //var stateCode = ret2.cub_abbreviation;


            // CardLogic cardLogic = new CardLogic(service);

            // EmailLogic emailLogic = new EmailLogic(service);
            //var ret = EmailLogic.ValidateEmailAddress("tim.klooster@gmail.com");

            // var ret2 = cardLogic.SearchToken("ABP","Bankcard", "4024007132811455","02","18");

            // var ret3 = emailLogic.ValidateEmailAddressNotInUseOnOtherContact("tim.smith@gmail.com");

            //CaseLogic caseLogic = new CaseLogic(service);
            //EntityCollection e =  caseLogic.SearchByCaseId("CAS-00000-N3L2L7");
            //string ret = caseLogic.CastEntityCollectionToString(e);


            return;
        }
    }
}
