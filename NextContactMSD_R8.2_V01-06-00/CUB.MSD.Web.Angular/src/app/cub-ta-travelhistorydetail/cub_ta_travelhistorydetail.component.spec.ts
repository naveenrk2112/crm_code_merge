import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cub_TA_TravelHistoryDetailComponent } from './cub_ta_travelhistorydetail.component';

describe('Cub_TA_TravelHistoryDetailComponent', () => {
    pending(); // TODO: remove when implementing
    let component: Cub_TA_TravelHistoryDetailComponent;
    let fixture: ComponentFixture<Cub_TA_TravelHistoryDetailComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [Cub_TA_TravelHistoryDetailComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_TA_TravelHistoryDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
