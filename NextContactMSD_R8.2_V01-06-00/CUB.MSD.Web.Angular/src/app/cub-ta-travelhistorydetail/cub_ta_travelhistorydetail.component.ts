﻿/**
 * This component is outdated and unused.
 * Please refer to the cub-ta-tripshistory folder for most recent version.
 */

import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_TA_TravelHistoryDetailService, TATravelHistoryDetail, TATravelHistoryPresence, TATravelPayment } from '../services/cub_ta_travelhistorydetail.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { DataTableModule, SharedModule, LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';

//Used to call service for TA travel history detail
@Component({
    selector: 'cub_ta_travelhistorydetail',
    templateUrl: './cub_ta_travelhistorydetail.component.html',
    providers: [Cub_TA_TravelHistoryDetailService]
})
export class Cub_TA_TravelHistoryDetailComponent implements OnInit {
    //temp for testing
    startDateControl: any;
    travelHistoryDetail: any;
    searchControl: any;
    /* Filter values */
    startDateFilter: string;
    endDateFilter: string;
    searchFilter: string;
    rowsPerPage: number;
    disableSearch: boolean = true;
    @Output() onCancel = new EventEmitter<void>();

    @Input() transitaccountId: string;
    @Input() subsystemId: string;
    @Input() tripId: string

    detailResults: any;
    travelHistoryTable: any[];
    travelPresence: TATravelHistoryPresence[];
    travelPayment: TATravelPayment[];
    travelPaymentSource: any[];
    totalRecordCount: number = 0;
    detailsLoaded: boolean = false;

    constructor(
        private _cub_ta_travelHistoryDetailService: Cub_TA_TravelHistoryDetailService,
        private _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {

        if (this.transitaccountId == '' || this.subsystemId == '' || this.tripId == '') {
            this._cub_dataService.onApplicationError("Error: transit account id or subsystem id or trip id not passed.")
            return;
        }

        const done = this._loadingScreen.add('Loading detail');
        this._cub_ta_travelHistoryDetailService.getTravelHistoryDetail(this.transitaccountId, this.subsystemId, this.tripId)
            .then((data: any) => {
                this.detailResults = JSON.parse(data.Body);
            })
            .catch(err => {
                this._cub_dataService.prependCurrentError('Error getting travel history details');
                this.onCancelClicked();
            })
            .then(() => this.detailsLoaded = true)
            .then(() => done());
    } 

    onCancelClicked() {
        this.onCancel.emit();
    }
}