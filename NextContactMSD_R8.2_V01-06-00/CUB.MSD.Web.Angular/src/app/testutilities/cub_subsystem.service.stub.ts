﻿export class Cub_SubsystemServiceStub {
    public static _subsystemData: any = {};
    getSubsystemStatus(...foo) {
        return Promise.resolve(Cub_SubsystemServiceStub._subsystemData);
    }
    getCachedSubsystemStatus(...foo) {
        return this.getSubsystemStatus();
    }
    getCasesRelatedToSubsystemId(...foo) {
        return Promise.resolve({});
    }
    refreshSubsystemStatus(...foo) {
        return this.getSubsystemStatus();
    }
}