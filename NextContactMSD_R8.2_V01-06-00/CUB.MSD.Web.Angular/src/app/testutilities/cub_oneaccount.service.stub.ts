﻿// TODO: add ability to customize method return values as with other stubbed services

export class Cub_OneAccountServiceStub {
    LinkOneAccountToSubsystem(...foo) {
        return Promise.resolve(null);
    }

    getBalanceHistory(...foo) {
        return Promise.resolve([]);
    }

    retrieveBalanceHistoryDetail(...foo) {
        return Promise.resolve({});
    }

    getAddresses(foo) {
        return Promise.resolve([]);
    }

    getStates(foo) {
        return Promise.resolve([]);
    }

    addFundingSource(...foo) {
        return Promise.resolve({ Success: true });
    }

    updateFundingSource(...foo) {
        return Promise.resolve(null);
    }

    checkZipPostalCode(...foo) {
        return Promise.resolve([]);
    }

    getOneAccountDetails(...foo) {
        return Promise.resolve([]);
    }

    getCachedOneAccountSummary(...foo) {
        return this.getOneAccountDetails();
    }

    refreshOneAccountSummary(...foo) {
        return this.getOneAccountDetails();
    }
}