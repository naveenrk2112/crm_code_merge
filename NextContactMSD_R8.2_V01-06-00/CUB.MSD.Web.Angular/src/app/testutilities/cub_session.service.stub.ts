﻿import { Session } from '../model';

export class Cub_SessionServiceStub {
    public static SESSION: Session = {};

    isActive: boolean = false;
    session: Session = {};

    getCachedSession() {
        this.session = Cub_SessionServiceStub.SESSION;
        return Promise.resolve(this.session);
    }

    refreshSession() {
        this.session = Cub_SessionServiceStub.SESSION;
        return Promise.resolve(this.session);
    }

    createSession(...foo) {
        this.session = Cub_SessionServiceStub.SESSION;
        return Promise.resolve(this.session);
    }

    updateSession(...foo) {
        this.session = Cub_SessionServiceStub.SESSION;
        return Promise.resolve(this.session);
    }

    endSession() {
        this.session = {};
        return Promise.resolve();
    }
}