﻿export class Cub_CustomerVerificationServiceStub {
    getAddress = () => Promise.resolve({
        addressId: '00000000-0000-0000-0000-000000000009',
        city: 'Springfield',
        state: 'IL',
        street1: '742 Evergreen Terrace',
        zipCode: '12345'
    });
    getBirthDate = () => Promise.resolve('5/12/1956');
    getSecurityQuestions = () => Promise.resolve([
        {
            securityQuestion: 'Who shot Mr. Burns?',
            securityAnswer: 'Maggie'
        }, {
            securityQuestion: 'What is your wife\'s maiden name',
            securityAnswer: 'Bouvier'
        }
    ]);
    getUsernameAndPin = () => Promise.resolve({
        username: 'HomerSimpson',
        pin: '0000'
    });
}