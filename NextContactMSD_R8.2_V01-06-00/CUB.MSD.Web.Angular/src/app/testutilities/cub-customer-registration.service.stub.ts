﻿export class Cub_CustomerRegistrationServiceStub {
    public static CUSTOMER_ID: string = 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX';

    searchCustomer(...foo) {
        return Promise.resolve([]);
    }

    getPhoneTypes() {
        return Promise.resolve([]);
    }

    getCountries() {
        return Promise.resolve([]);
    }

    getSecurityQuestions() {
        return Promise.resolve([]);
    }

    getContactTypes() {
        return Promise.resolve([]);
    }

    getAccountTypes() {
        return Promise.resolve([]);
    }

    getStates() {
        return Promise.resolve([]);
    }

    registerCustomer(...foo) {
        return Promise.resolve(Cub_CustomerRegistrationServiceStub.CUSTOMER_ID);
    }

    checkZipPostalCode(...foo) {
        return Promise.resolve([]);
    }

    checkUsername(...foo) {
        return Promise.resolve({});
    }
}