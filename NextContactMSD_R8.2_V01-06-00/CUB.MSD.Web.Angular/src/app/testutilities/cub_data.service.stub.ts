﻿import { Observable } from 'rxjs/Observable';
import { Cub_Globals, Cub_GridMap } from '../model';

export class Cub_DataServiceStub {
    /* Editable values */
    public static _currentUserId = 'USER_ID';
    public static _constants = {
        'ContactVerificationDetails': {
            0: 'Name',
            1: 'Address',
            2: 'Phone 1',
            3: 'Phone 2',
            4: 'Phone 3',
            5: 'Email',
            6: 'Username',
            7: 'PIN',
            8: 'Date of Birth'
        },
        'CUB.MSD.Web.Angular': {
            'CacheExpirationLimit': 1,
            'CacheName': 'cub_cache',
            'MaxSecurityQuestions': 3,
            'NotificationHistory.NumRows': 10,
            'NotificationHistory.StartDateOffset': 30,
            'NumDetailsToVerify': 3,
            'NumQuestionsToVerify': 1,
            'OrderHistory.NumRows': 10,
            'TA.TravelHistory.NumRows': 10,
            'TA.TravelHistory.StartDateOffset': 30,
        },
        'NISApi': {
            'CustomerOrderHistory.OrderStatus.Options': JSON.stringify({
                'OSOption1': 'Option One',
                'OSOption2': 'Option Two'
            }),
            'CustomerOrderHistory.OrderType.Options': JSON.stringify({
                'OTOption1': 'Option One',
                'OTOption2': 'Option Two'
            }),
            'CustomerOrderHistory.PaymentStatus.Options': JSON.stringify({
                'PSOption1': 'Option One',
                'PSOption2': 'Option Two'
            })
        }
    };
    public static _gridConfig = {};

    /* Stubbed values */
    get Globals() { return Observable.of(Cub_DataServiceStub._constants) as Observable<Cub_Globals>; }
    get GridConfigs() { return Observable.of(Cub_DataServiceStub._gridConfig) as Observable<Cub_GridMap>; }
    get currentUserId() { return Cub_DataServiceStub._currentUserId; }
    set currentUserId(value) { Cub_DataServiceStub._currentUserId = value; }
    apiServerUrl = 'http://fakedomain.com';
    busyScreen = {
        busyText: '',
        icon: '',
        busyCount: 0
    };
    searchCardData;
    searchValues = {
        firstName: 'Fake',
        lastName: 'Person',
        email: 'fake@email.com',
        phone: '3125559879',
    };
    showCustomerVerification = false;
    selectedContactCustomerToVerify = {
        ContactId: '00000000-0000-0000-0000-000000000001',
        FirstName: 'Homer',
        LastName: 'Simpson',
        FullName: 'Homer Simpson',
        Email: 'hsimpson@springfieldnuclear.org',
        Phone1: '123-555-1234',
        Phone1Type: 'Home',
        Phone2: '123-555-9876',
        Phone2Type: 'Mobile',
        Phone3: null,
        Phone3Type: null,
    };
    tokens = [];
    errorScreen = {};
    cancelScreen = {};
    previousPage;

    /* Stubbed methods */
    incrementLoader(val: string = '') {
        ++this.busyScreen.busyCount;
        if (!!val) {
            this.busyScreen.busyText = val;
        }
    }
    decrementLoader() {
        --this.busyScreen.busyCount;
        expect(this.busyScreen.busyCount).toBeGreaterThanOrEqual(0, 'Cub_DataService.decrementLoader called too many times');
    }
    showError(foo) { }
    onApplicationError(...foo) { }
    prependCurrentError(foo) { }
    formatSortQueryParam(foo) {
        return '';
    }
    tryParse(foo) {
        let parsed = foo;
        try {
            parsed = JSON.parse(foo);
        } catch(e) { }
        return parsed;
    }
    getActivityTypeCodes() {
        return Promise.resolve([]);
    }
    redirectToMsdFormWithParameters(...foo) { }
    openMsdForm(...foo) { }
    guidsAreEqual(a, b) { return a == b; }
}
