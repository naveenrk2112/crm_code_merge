﻿export class Cub_WebApiServiceStub {
    public static _globals = [];

    public static _setGlobal(globalKey, globalDetailKey, value) {
        Cub_WebApiServiceStub._globals[globalKey] = Cub_WebApiServiceStub._globals[globalKey] || {};
        Cub_WebApiServiceStub._globals[globalKey][globalDetailKey] = value;
    }

    getGlobalDetailAttributeValue(globalKey, globalDetailKey) {
        if (Cub_WebApiServiceStub._globals[globalKey] == null
            || Cub_WebApiServiceStub._globals[globalKey][globalDetailKey] == null
        ) {
            return Promise.reject(`You have to call static method Cub_WebApiServiceStub._setGlobal('${globalKey}', '${globalDetailKey}', 'value') before the value would be retrieved`);
        }
        return Promise.resolve(Cub_WebApiServiceStub._globals[globalKey][globalDetailKey]);
    }
}