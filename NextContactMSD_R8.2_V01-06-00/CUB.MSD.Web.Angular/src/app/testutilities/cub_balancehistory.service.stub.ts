﻿import { Injectable } from '@angular/core';

@Injectable()
export class Cub_BalanceHistoryServiceStub {
    public static _balanceHistoryResponse: any;

    getBalanceHistory(customerId, ...foo) {
        //expect(customerId).toBeDefined('Customer ID not passed to getBalanceHistory');
        return Promise.resolve(Cub_BalanceHistoryServiceStub._balanceHistoryResponse);
    }

    getOneAccountDetails(accountId) {
        return Promise.resolve(null);
    }

    retrieveBalanceHistoryDetail(accountId, journalId) {
        return Promise.resolve([]);
    }
}