﻿import { Injectable } from '@angular/core';

@Injectable()
export class Cub_TA_TripsHistoryServiceStub {
    public static _tripsHistoryResponse: any;

    getTripsHistory(queryParams) {
        //expect(customerId).toBeDefined('Customer ID not passed to getBalanceHistory');
        return Promise.resolve(Cub_TA_TripsHistoryServiceStub._tripsHistoryResponse);
    }
}