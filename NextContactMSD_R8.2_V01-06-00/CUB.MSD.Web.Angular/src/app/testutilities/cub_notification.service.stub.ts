﻿import { Contact, Channel, NotificationDetail, NotificationRecord } from '../model';

/**
 * This is a stub class for testing anything that uses Cub_NotificationService.
 * Control the data used and outputted by this service by setting the static
 * values of _channels, _contacts, _detail, and _notifications before running
 * or many tests.
 */

export class Cub_NotificationServiceStub {
    /* Editable values */
    public static _contacts: Contact[] = [];
    public static _channels: Channel[] = [];
    public static _detail: NotificationDetail;
    public static _notifications: NotificationRecord[] = [];

    customerId: string;
    customerNotificationResults: NotificationRecord[];
    resendContactId: string;
    selected: NotificationRecord;
    selectedDetail: NotificationDetail;
    totalRecordCount: number;
    getContactsRelatedToCustomer(foo) {
        return Promise.resolve(Cub_NotificationServiceStub._contacts);
    }
    getEnabledNotificationChannels() {
        return Promise.resolve(Cub_NotificationServiceStub._channels);
    }
    getCustomerNotifications(params) {
        expect(params.customerId).toBeDefined();
        this.customerNotificationResults = Cub_NotificationServiceStub._notifications;
        this.totalRecordCount = Cub_NotificationServiceStub._notifications.length;
        return Promise.resolve(Cub_NotificationServiceStub._notifications);
    }
    getCustomerNotificationDetail() {
        this.selectedDetail = Cub_NotificationServiceStub._detail;
        this.resendContactId = Cub_NotificationServiceStub._detail.contactId;
        return Promise.resolve(Cub_NotificationServiceStub._detail);
    }
    resend = (...foo) => Promise.resolve();
}