﻿export class Cub_CacheServiceStub {
    public static _storage: any = {};
    public static _placeholder: any = 'PLACEHOLDER';

    private _get(key) {
        return Cub_CacheServiceStub._storage[key];
    }

    private _set(key, value) {
        Cub_CacheServiceStub._storage[key] = value;
    }

    retrieve<T>(key: string, fn?: () => Promise<T>) {
        const val = this._get(key) as T;
        if (fn == null) {
            return Promise.resolve(val || null);
        }
        if (val == null) {
            return fn();
        }
        return val;
    }

    retrieveSync<T>(key: string) {
        return this._get(key) as T;
    }

    refresh<T>(key: string, fn?: () => Promise<T>) {
        if (fn == null) {
            this._set(key, null);
            return Promise.resolve(null);
        }
        return fn()
            .then(val => {
                this._set(key, val);
                return val;
            })
    }

    storePlaceholder(key: string) {
        this._set(key, Cub_CacheServiceStub._placeholder);
    }

    store(key: string, data: any) {
        this._set(key, data);
    }

    remove(key: string) {
        delete Cub_CacheServiceStub._storage[key];
    }

    prune() {
        Cub_CacheServiceStub._storage = {};
    }
}