﻿export class Cub_TransitAccountServiceStub {
    public static _activities: any = [];
    public static _transitAccount: any = {
        Id: 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX',
        Name: '330000000000',
        LogicalName: 'cub_transitaccount'
    };
    public static _tapsHistoryResponse: any;

    getActivities(...foo) {
        return Promise.resolve(Cub_TransitAccountServiceStub._activities);
    }
    getTransitAccount(...foo) {
        return Promise.resolve(Cub_TransitAccountServiceStub._transitAccount);
    }
    getTapsHistory(...foo) {
        return Promise.resolve(Cub_TransitAccountServiceStub._tapsHistoryResponse);
    }
}