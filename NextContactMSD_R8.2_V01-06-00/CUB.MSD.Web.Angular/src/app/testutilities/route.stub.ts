﻿import { Params } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

/**
 * Override ActivatedRoute with this class in tests to mock query params.
 * Currently implemented:
 * - ActivatedRoute.queryParams
 * - ActivatedRoute.snapshot.queryParams
 */
export class ActivatedRouteStub {
    private static _state = {
        queryParams: new BehaviorSubject<Params>({})
    };

    /**
     * Set/check query params with ActivatedRouteStub._queryParams 
     */
    public static get _queryParams() {
        return ActivatedRouteStub._state.queryParams.getValue();
    }
    public static set _queryParams(values: Params) {
        ActivatedRouteStub._state.queryParams.next(values);
    }

    // Stubbed methods/values used by ActivatedRoute

    get queryParams() {
        return ActivatedRouteStub._state.queryParams.asObservable();
    }

    get queryParamsMap() {
        return {
            has: value => ActivatedRouteStub._queryParams[value] != null,
            get: value => ActivatedRouteStub._queryParams[value]
        }
    }

    get snapshot() {
        return {
            queryParams: ActivatedRouteStub._queryParams
        };
    }
}

export class RouterStub {
    public static URL: string = '';

    get url(): string { return RouterStub.URL; }

    navigate(...foo) { }
}
