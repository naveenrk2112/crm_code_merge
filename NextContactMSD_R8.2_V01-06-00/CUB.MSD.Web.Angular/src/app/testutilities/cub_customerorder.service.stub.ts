﻿import { Injectable } from '@angular/core';

import { WSGetCustomerOrderHistoryResponse } from '../model';

@Injectable()
export class Cub_CustomerOrderServiceStub {
    public static _orderHistoryResponse: WSGetCustomerOrderHistoryResponse;

    getOrderHistory(customerId, ...foo) {
        expect(customerId).toBeDefined('Customer ID not passed to getOrderHistory');
        return Promise.resolve(Cub_CustomerOrderServiceStub._orderHistoryResponse);
    }
    getOrderHistoryDetail(...foo) {
        throw new Error('getOrderHistoryDetail method not stubbed');
    }
    getOrder(...foo) {
        throw new Error('getOrder method not stubbed');
    }
    resolveBalance(...foo) {
        return Promise.resolve({});
    }
}