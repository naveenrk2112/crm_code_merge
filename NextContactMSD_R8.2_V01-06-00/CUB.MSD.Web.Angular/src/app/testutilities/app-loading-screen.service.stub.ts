﻿export class AppLoadingScreenServiceStub {
    add(foo) {
        let completed = false;
        return () => {
            expect(completed).toBeFalsy('Loading screen message removal function called more than once');
            completed = true;
        };
    }

    clear() { }
}