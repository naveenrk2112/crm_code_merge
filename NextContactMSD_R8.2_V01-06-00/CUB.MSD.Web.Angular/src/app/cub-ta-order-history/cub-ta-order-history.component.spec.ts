import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubTaOrderHistoryComponent } from './cub-ta-order-history.component';

describe('CubTaOrderHistoryComponent', () => {
  let component: CubTaOrderHistoryComponent;
  let fixture: ComponentFixture<CubTaOrderHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubTaOrderHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubTaOrderHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
