import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CubOrderService } from '../services/cub-order.service';
import * as Model from '../model';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import * as moment from 'moment';
import * as _ from 'lodash';
import { LazyLoadEvent, SortMeta } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'cub-cub-ta-order-history',
  templateUrl: './cub-ta-order-history.component.html',
  styleUrls: ['./cub-ta-order-history.component.css'],
  providers: [CubOrderService, Cub_TransitAccountService]
})
export class CubTaOrderHistoryComponent implements OnInit {
    transitAccountId: string;
    subsystemtId: string;
    userId: string = ""; 
    orderNumberControl: Model.Cub_Textbox = {
        id: "1",
        label: "Order Number",
        includeLabel: true
    };
    realtimeFlag: boolean = false;
    orderHistory: Array<object> = [];
    orderHistorySorted: Array<object> = [];
    totalCount: number = 0;
    orderTypeOptions: Model.Cub_MultiDropdownOption[] = [];
    orderTypeValues: string;
    orderStatusOptions: Model.Cub_MultiDropdownOption[] = [];
    orderStatusValues: string;
    orderConfigDataTypes: any;

    disableSearch: boolean = false;
    initialPass: boolean = true;
    rowsPerPage: number = 5; //default
    initComplete: boolean = false;
    dateRangeOffset: number = 30;

    /* Filter values */
    startDateFilter: string;
    endDateFilter: string;

    /*Pagination and Sorting params*/
    multiSortMeta: SortMeta[] = [];
    sortBy: string;
    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_orderService: CubOrderService,
        private _cub_transitaccountService: Cub_TransitAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { 
        this.multiSortMeta.push({
            field: 'startDateTime',
            order: -1
        });
        this.sortBy = this._cub_dataService.formatSortQueryParam(this.multiSortMeta);
    }

    ngOnInit() {
        // Get Transit Account ID from MSD form
        const nameControl = parent.Xrm.Page.getAttribute('cub_name');
        if (nameControl && nameControl.getValue()) {
            this.transitAccountId = nameControl.getValue();
        }
        // Get Subsystem ID from MSD form
        const subsystemControl = parent.Xrm.Page.getAttribute('cub_subsystemid');
        if (subsystemControl && subsystemControl.getValue()) {
            this.subsystemtId = subsystemControl.getValue();
        }

        // Load configurable data
        const globals$ = this._cub_dataService.Globals.first(globals => !!globals['CUB.MSD.Web.Angular']);
        const grids$ = this._cub_dataService.GridConfigs.first(configs => !!configs['TransitAccount.OrderHistory']);
        const orderConfigTypes$ = this._cub_orderService.getOrderConfigDataTypesFromCache();
        const done = this._loadingScreen.add();
        Observable.forkJoin(globals$, grids$, orderConfigTypes$)
            .finally(() => done())
            .subscribe(([globals, grids, orderConfigTypes]) => {
                this.dateRangeOffset = globals['CUB.MSD.Web.Angular']['TransitAccount.OrderHistory.StartDateOffSet'] as number;
                const grid = grids['TransitAccount.OrderHistory'];
                this.rowsPerPage = grid.rowsToDisplay;

                // Filling Order Types
                this.orderTypeOptions = orderConfigTypes.orderTypes.map(ot => ({
                    value: ot.type,
                    label: ot.shortDescription
                }));
                // Filling Order Status
                this.orderStatusOptions = orderConfigTypes.orderStatuses.map(ot => ({
                    value: ot.status,
                    label: ot.shortDescription
                }));

                this.getOrderHistory();
            });
    }

    //Handle grid loading on paging
    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.disableSearch = false;
            this.getOrderHistory();
        }
    }

    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableSearch = false;
    }

    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableSearch = false;
    }

    onSearchClicked() {
        this.getOrderHistory();
    }

    /**
    * Generic onChange function that enables the search button.
    * Fires when a dropdown control changes. BREAK HERE
    */
    onMultiDropdownChanged(event, control) {
        this[control] = event.join();
        this.disableSearch = false;
    }

    onEnterKey(...foo) { }

    onRowDblClick(...foo) { }

    getOrderHistory() {
        let userId = this._cub_dataService.currentUserId;
        let transitAccountId = this.transitAccountId;
        let subSystemId = this.subsystemtId;
        let orderId = this.orderNumberControl.value || null;
        let startDateTime = this.startDateFilter || null;
        let endDateTime = this.endDateFilter || null;
        let orderType = this.orderTypeValues;
        let orderStatus = this.orderStatusValues;
        let realtimeFlag = this.realtimeFlag;
        let sortBy = this.sortBy || null;
        let offset = (this.pagingOffset > 0) ? this.pagingOffset : 0;
        let limit = this.rowsPerPage;
        // Call the MVC application to get Order History
        const done = this._loadingScreen.add('Loading Order History...');
        this._cub_transitaccountService.getOrderHistory({
            userId: userId,
            transitAccountId: transitAccountId,
            subSystemId: subSystemId,
            orderId: orderId,
            startDateTime: startDateTime,
            endDateTime: endDateTime,
            orderType: orderType,
            orderStatus: orderStatus,
            realtimeResults: realtimeFlag,
            sortBy: sortBy,
            offset: offset,
            limit: limit
        })
            .then((data) => {
                if (data) {
                    var response = JSON.parse(data.Body);
                    if (response.orders) {
                        this.orderHistory = response.orders;
                        this.orderHistorySorted = response.orders;
                        this.totalRecordCount = response.totalCount;
                        this.disableSearch = false;
                    }
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting Transit Account Order History');
            })
            .then(() => this.initComplete = true)
            .then(() => done());
    }
}
