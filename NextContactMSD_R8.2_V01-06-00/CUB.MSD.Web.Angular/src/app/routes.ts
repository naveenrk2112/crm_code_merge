﻿import { Routes } from '@angular/router';

import { AppLandingComponent } from './app-landing.component';
import { Cub_CustomerRegistrationComponent } from './cub-customerregistration/cub_customerregistration.component';
import { Cub_CustomerSearchComponent } from './cub-customersearch/cub_customersearch.component';
import { Cub_MasterSearchContactComponent } from './cub-mastersearch.contact/cub_mastersearch.contact.component';
import { Cub_MasterSearchTokenComponent } from './cub-mastersearch.token/cub_mastersearch.token.component';
import { Cub_NotificationHistoryComponent } from './cub-notificationhistory/cub_notificationhistory.component';
import { Cub_NotificationPreferencesComponent } from './cub-notificationpreferences/cub_notificationpreferences.component';
import { Cub_OAM_TravelHistoryComponent } from './cub-oam-travelhistory/cub_oam_travelhistory.component';
import { Cub_OneAccountBalancesComponent } from './cub-oneaccount-balances/cub_oneaccountbalances.component';
import { Cub_OneAccountBalanceHistoryComponent } from './cub-oneaccount-balancehistory/cub_oneaccount_balancehistory.component';
import { Cub_SubsystemComponent } from './cub-subsystem/cub_subsystem.component';
import { Cub_SubsystemAccountsComponent } from './cub-subsystemaccounts/cub_subsystemaccounts.component';
import { Cub_SubsystemDelinkCustomerComponent } from './cub-subsystem-delink-customer/cub-subsystem-delink-customer.component';
import { Cub_SubsystemAccountTknBlockUnblockComponent } from './cub-subsystemaccount_tkn_block_unblock/cub_subsytemaccounttkn_block_unblock.component';
import { Cub_TokensComponent } from './cub-tokens/cub_tokens.component';
import { Cub_TA_TapsHistoryComponent } from './cub-ta-tapshistory/cub_ta_tapshistory.component';
import { Cub_TA_TripsHistoryComponent } from './cub-ta-tripshistory/cub_ta_tripshistory.component';
import { Cub_TA_TravelHistoryComponent } from './cub-ta-travelhistory/cub_ta_travelhistory.component';
import { Cub_TA_TravelHistoryDetailComponent } from './cub-ta-travelhistorydetail/cub_ta_travelhistorydetail.component';
import { Cub_TA_StatusHistoryComponent } from './cub-ta-statushistory/cub_ta_statushistory.component';
import { Cub_TA_Token_StatusHistoryComponent } from './cub-ta-token-statushistory/cub_ta_token_statushistory.component';
import { Cub_TA_BalanceHistoryJournalComponent } from './cub-ta-balancehistory-journal/cub-ta-balancehistory-journal.component';
import { Cub_FundingsourceComponent } from './cub-fundingsource/cub-fundingsource.component';
import { Cub_MasterSearchOrderComponent } from './cub-master-search-order/cub-master-search-order.component';
import { Cub_CustomerOrderHistoryComponent } from './cub-customer-orderhistory/cub-customer-orderhistory.component';
import { Cub_AddFundingSourceComponent } from './cub-add-funding-source/cub-add-funding-source.component';
import { Cub_CustomerOrderhistoryDetailComponent } from './cub-customer-orderhistory-detail/cub-customer-orderhistory-detail.component';
import { Cub_CustomerOrderComponent } from './cub-customer-order/cub-customer-order.component';
import { Cub_EditFundingSourceComponent } from './cub-edit-fundingsource/cub-edit-fundingsource.component';
import { CubCustomerActivitiesComponent } from './cub-customer-activities/cub-customer-activities.component';
import { Cub_ProxyRouteComponent } from './cub-proxy-route/cub-proxy-route.component';
import { CubTaBankcardChargeHistoryComponent } from './cub-ta-bankcard-charge-history/cub-ta-bankcard-charge-history.component';
import { CubTaBankcardChargeAggregationComponent } from './cub-ta-bankcard-charge-aggregation/cub-ta-bankcard-charge-aggregation.component';
import { CubTaActivitiesComponent } from './cub-ta-activities/cub-ta-activities.component';
import { CubTaOrderHistoryComponent } from './cub-ta-order-history/cub-ta-order-history.component';
import { Cub_CustomerVerificationComponent } from './cub-customerverification/cub_customerverification.component';
import { CubTaAdjustValuesComponent } from './cub-ta-adjust-values/cub-ta-adjust-values.component';
import { Cub_SessionDetailsComponent } from './cub-session-details/cub-sessiondetails.component';
import { Cub_OrderDetailsComponent } from './cub-order-details/cub-orderdetails.component';


// Tolling
import { Tam_VehiclesComponent } from './tam-vehicles/tam-vehicles.component';
import { Tam_TranspondersComponent } from './tam-transponders/tam-transponders.component';
import { Tam_AccountInformationComponent } from './tam-accountinformation/tam-accountinformation.component';
import { Tam_BalancesComponent } from './tam-balances/tam-balances.component';
import { Tam_AddVehiclesComponent } from './tam-add-vehicles/tam-add-vehicles.component';
import { Tam_AddTranspondersComponent } from './tam-add-transponders/tam-add-transponders.component';
import { Tam_TransactionsComponent } from './tam-transactions/tam-transactions.component';

const _sessionChildRoute = {
    path: 'Session',
    outlet: 'session',
    component: Cub_ProxyRouteComponent,
    children: [{ path: '', loadChildren: 'app/cub-session/cub_session.module#Cub_SessionModule' }]
};

export const CubRoutes: Routes = [
    { path: 'CustomerSearch', component: Cub_CustomerSearchComponent, children: [_sessionChildRoute] },
    { path: 'CustomerRegistration', component: Cub_CustomerRegistrationComponent, children: [_sessionChildRoute] },
    { path: 'MasterSearchContact', component: Cub_MasterSearchContactComponent, children: [_sessionChildRoute] },
    { path: 'MasterSearchToken', component: Cub_MasterSearchTokenComponent, children: [_sessionChildRoute] },
    { path: 'MasterSearchOrder', component: Cub_MasterSearchOrderComponent, children: [_sessionChildRoute] },
    { path: 'NotificationHistory', component: Cub_NotificationHistoryComponent },
    { path: 'NotificationPreferences', component: Cub_NotificationPreferencesComponent },
    { path: 'OneAccountBalances', component: Cub_OneAccountBalancesComponent },
    { path: 'OneAccountBalanceHistory', component: Cub_OneAccountBalanceHistoryComponent },
    { path: 'OAM_TravelHistory', component: Cub_OAM_TravelHistoryComponent },
    { path: 'Session', loadChildren: 'app/cub-session/cub_session.module#Cub_SessionModule' },
    { path: 'Subsystem', component: Cub_SubsystemComponent },
    { path: 'SubsystemAccounts', component: Cub_SubsystemAccountsComponent },
    { path: 'SubsystemAccountsDelink', component: Cub_SubsystemDelinkCustomerComponent },
    { path: 'SubsystemAccountsBlockUnblock', loadChildren: 'app/cub-subsystem-acct-block-unblock/cub_subsytemaccount_block_unblock.module#Cub_SubsystemAcctBlockUnblockModule' },
    { path: 'SubsystemAccountTknBlockUnblock', component: Cub_SubsystemAccountTknBlockUnblockComponent },
    { path: 'TA_TravelHistory', component: Cub_TA_TravelHistoryComponent },
    { path: 'TA_TravelHistoryDetail', component: Cub_TA_TravelHistoryDetailComponent },
    { path: 'Tokens', component: Cub_TokensComponent },
    { path: 'UnableToDeleteAddress', loadChildren: 'app/cub-unabletodeleteaddress/cub_unabletodeleteaddress.module#Cub_UnableToDeleteAddressModule' },
    { path: 'UpdateAddress', loadChildren: 'app/cub-updateaddress/cub_updateaddress.module#Cub_UpdateAddressModule' },
    { path: 'Fundingsource', component: Cub_FundingsourceComponent },
    { path: 'AddingFundingsource', component: Cub_AddFundingSourceComponent },
    { path: 'CustomerOrderHistory', component: Cub_CustomerOrderHistoryComponent },
    { path: 'CustomerOrderHistoryDetail', component: Cub_CustomerOrderhistoryDetailComponent },
    { path: 'CustomerOrder', component: Cub_CustomerOrderComponent },
    { path: 'EditFundingsource', component: Cub_EditFundingSourceComponent },
    { path: 'TAM_Transponders', component: Tam_TranspondersComponent },
    { path: 'TAM_AccountInformation', component: Tam_AccountInformationComponent },
    { path: 'TAM_Vehicles', component: Tam_VehiclesComponent },
    { path: 'TAM_Balances', component: Tam_BalancesComponent },
    { path: 'TAM_Add_Vehicles', component: Tam_AddVehiclesComponent },
    { path: 'TAM_Add_Transponders', component: Tam_AddTranspondersComponent },
    { path: 'TAM_Transactions', component: Tam_TransactionsComponent },
    { path: 'TA_TapsHistory', component: Cub_TA_TapsHistoryComponent },
    { path: 'TA_TripsHistory', component: Cub_TA_TripsHistoryComponent },
    { path: 'TA_StatusHistory', component: Cub_TA_StatusHistoryComponent },
    { path: 'TA_Token_StatusHistory', component: Cub_TA_Token_StatusHistoryComponent },
    { path: 'TA_BalanceHistoryJournal', component: Cub_TA_BalanceHistoryJournalComponent },
    { path: 'TA_BankCardChargeHistory', component: CubTaBankcardChargeHistoryComponent },
    { path: 'TA_BankCardChargeAggregation', component: CubTaBankcardChargeAggregationComponent },
    { path: 'CustomerActivities', component: CubCustomerActivitiesComponent },
    { path: 'TaSubsystemActivities', component: CubTaActivitiesComponent },
    { path: 'TA_OrderHistory', component: CubTaOrderHistoryComponent },
    { path: 'ResolveBalance', loadChildren: 'app/cub-resolve-balance/cub_resolve_balance.module#Cub_ResolveBalanceModule' },
    { path: 'Verification', component: Cub_CustomerVerificationComponent },
    { path: 'TA_AdjustValues', component: CubTaAdjustValuesComponent },
    { path: 'SessionDetails', component: Cub_SessionDetailsComponent },
    { path: 'OrderDetails', component: Cub_OrderDetailsComponent },
    { path: '', component: AppLandingComponent },
    //{path: '**', component: PageNotFoundComponent} //TODO: Add a 404 component
];
