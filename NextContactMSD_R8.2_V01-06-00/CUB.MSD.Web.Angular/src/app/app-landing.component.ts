import { Component } from '@angular/core';

//TODO: We can put a page header here, and this is where everything should originate
@Component({
    selector: 'app-landing',
    template: `<div class="header center" style="font-size: 18px;">
                    <h1>Landing Page For Development</h1><br>
                    <p>Please select a page to go to:</p>
                    <a routerLink="/CustomerSearch" routerLinkActive="active">Customer Search</a><br>
                    <a routerLink="/CustomerRegistration" routerLinkActive="active">Customer Registration</a><br>
                    <a routerLink="/CustomizableGridTestPage" routerLinkActive="active">Customizable Grid Test Page</a><br>
                    <a routerLink="/MasterSearchContact" routerLinkActive="active">Master Search Contact</a><br>
                    <a routerLink="/MasterSearchToken" routerLinkActive="active">Master Search Token</a><br>
                    <a routerLink="/NotificationHistory" routerLinkActive="active">Notification History</a><br>
                    <a routerLink="/NotificationPreferences" routerLinkActive="active">Notification Preferences</a><br>
                    <a routerLink="/Subsystem" routerLinkActive="active">Subsystem</a><br>
                    <a routerLink="/UnableToDeleteAddress" routerLinkActive="active">Unable To Delete Address</a><br>
                    <a routerLink="/UpdateAddress" routerLinkActive="active">Update Address</a><br>
                    <a routerLink="/SubsystemAccounts" routerLinkActive="active">Subsystem Accounts</a><br>
                    <a routerLink="/SubsystemAccountsDelink" routerLinkActive="active">Subsystem Accounts Delink</a><br>
                    <a routerLink="/SubsystemAccountsBlockUnblock" routerLinkActive="active">Subsystem Accounts Block/Unblock</a><br>
                    <a routerLink="/Tokens" routerLinkActive="active">Tokens</a><br>
                    <a routerLink="/OneAccountBalances" routerLinkActive="active">OneAccount Balances</a><br>
                    <a routerLink="/OneAccountBalanceHistory" routerLinkActive="active">OneAccount Balance History</a><br>
                    <a routerLink="/OAM_TravelHistory" routerLinkActive="active">OneAccount Travel History</a><br>
                    <a routerLink="/TA_TravelHistory" routerLinkActive="active">Transit Account Travel History</a><br>
                    <a routerLink="/TA_TravelHistoryDetail" routerLinkActive="active">Transit Account Travel History Detail</a><br>
                    <a routerLink="/Fundingsource" routerLinkActive="active">Funding Source</a><br>
                    <a routerLink="/SessionInfo" routerLinkActive="active">Session Info/Bar</a><br>
                    <a routerLink="/TAM_AccountInformation" routerLinkActive="active">TAM Account Information</a><br>
                    <a routerLink="/TAM_Vehicles" routerLinkActive="active">TAM Vehicles</a><br>
                    <a routerLink="/TAM_Transponders" routerLinkActive="active">TAM Transponders</a><br>
                    <a routerLink="/TAM_Balances" routerLinkActive="active">TAM Balances</a><br>
                    <a routerLink="/TAM_AddVehicles" routerLinkActive="active">TAM Add Vehicles</a><br>
                    <a routerLink="/TAM_AddTransponders" routerLinkActive="active">TAM Add Transponders</a><br>
                    <a routerLink="/CustomerOrderHistory" routerLinkActive="active">Order History</a><br>
                    <a routerLink="/TA_TapsHistory" routerLinkActive="active">Transit Account Taps History</a><br>
                    <a routerLink="/TA_TripsHistory" routerLinkActive="active">Transit Account Trips History</a><br>
                    <a routerLink="/TA_StatusHistory" routerLinkActive="active">Transit Account Status History</a><br>
                    <a routerLink="/TA_BankCardChargeHistory" routerLinkActive="active">Transit Account BankCard Charge History</a><br>
                    <a routerLink="/TA_BalanceHistoryJournal" routerLinkActive="active">Transit Account Balance History Journal</a><br>
                    <a routerLink="/TA_BankCardChargeAggregation" routerLinkActive="active">Transit Account BankCard Charge Aggregation</a><br>
                    <a routerLink="/TA_OrderHistory" routerLinkActive="active">Transit Account Order History</a><br>
                    <a routerLink="/CustomerActivities" routerLinkActive="active">CustomerActivities</a><br>
                    <a routerLink="/TaSubsystemActivities" routerLinkActive="active">Transit Account Subsystem Activities</a><br>
               </div>
`
})
export class AppLandingComponent { }