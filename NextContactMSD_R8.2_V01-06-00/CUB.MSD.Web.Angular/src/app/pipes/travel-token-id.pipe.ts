import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { TransitAccountToken } from '../model';

@Pipe({
    name: 'travelTokenId'
})
export class TravelTokenIdPipe implements PipeTransform {
    transform(value: any): string {
        if (!value || !value.tokenInfo) {
            return null;
        }
        let t: TransitAccountToken = value;

        switch (t.tokenInfo.tokenType) {
            case 'Bankcard':
                return t.tokenInfo.maskedPAN;

            case 'Smartcard':
                return t.tokenInfo.serialNumber;

            case 'Mobile':
                return t.tokenInfo.serializedMobileToken;

            default:
                return null;
        }
    }
}

@NgModule({
    declarations: [TravelTokenIdPipe],
    exports: [TravelTokenIdPipe]
})
export class TravelTokenIdPipeModule { }