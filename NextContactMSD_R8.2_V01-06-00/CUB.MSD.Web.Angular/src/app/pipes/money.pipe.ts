﻿import { NgModule, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'money'
})
export class MoneyPipe implements PipeTransform {
    /**
     * Converts a value to a dollar amount.
     * @param value
     */
    transform(value: any): any {
        let num = +value / 100;
        if (!num) {
            return '$0.00';
        }
        const sign = num < 0 ? '-' : '';
        num = Math.abs(num);
        return sign + '$' + num.toFixed(2);
    }
}

@NgModule({
    declarations: [MoneyPipe],
    exports: [MoneyPipe]
})
export class MoneyPipeModule { }