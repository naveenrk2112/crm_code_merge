import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { MomentDatePipe } from './moment-date.pipe';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';

//this comment also shouldn't be committed

describe('MomentDatePipe', () => {
    let comp: MomentDatePipe;
    let fixture: ComponentFixture<MomentDatePipe>;
    let dataService: Cub_DataService;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [
                {provide: Cub_DataService, useClass: Cub_DataServiceStub }
            ],
            declarations: [
                Cub_DataService
            ]          
        }).compileComponents();
    }));

    it('create an instance', () => {
        const pipe = new MomentDatePipe(dataService);
        expect(pipe).toBeTruthy();
    });
});
