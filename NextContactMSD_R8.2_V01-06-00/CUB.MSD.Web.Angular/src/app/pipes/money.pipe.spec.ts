﻿import { MoneyPipe } from './money.pipe';

describe('MoneyPipe', () => {
    it('create an instance', () => {
        const pipe = new MoneyPipe();
        expect(pipe).toBeTruthy();
    });

    it('should format a dollar amount', () => {
        const pipe = new MoneyPipe();
        const dollar = 100;
        const formatted = '$1.00';
        const output = pipe.transform(dollar);
        expect(output).toEqual(formatted);
    });

    it('should interpret a string value', () => {
        const pipe = new MoneyPipe();
        const dollar = '100';
        const formatted = '$1.00';
        const output = pipe.transform(dollar);
        expect(output).toEqual(formatted);
    });

    it('should interpret a negative amount', () => {
        const pipe = new MoneyPipe();
        const debt = '-225';
        const formatted = '-$2.25';
        const output = pipe.transform(debt);
        expect(output).toEqual(formatted);
    });

    it('should return "$0.00" if it cannot interpret', () => {
        const pipe = new MoneyPipe();
        const output = pipe.transform(null);
        expect(output).toEqual('$0.00');
    });
});
