import { NgModule, Pipe, PipeTransform } from '@angular/core';

import { Cub_DataService } from '../services/cub_data.service';

import * as moment from 'moment-timezone';

@Pipe({
    name: 'momentDate'
})
export class MomentDatePipe implements PipeTransform {
    constructor(
        private _cub_dataService: Cub_DataService
    ) { }    

    transform(value: any, outputFormat: any = 'M/D/YYYY h:mm A', inputFormat: any = null): any {         
        return new Promise((resolve, reject) => {
            this._cub_dataService.Globals
                .first(globals => !!globals['CUB.MSD.Web.Angular'])
                .subscribe(globals => {
                    let timezone = globals['CUB.MSD.Web.Angular']['DefaultTimeZone'] as string;
                    let m = !!inputFormat ? moment(value, inputFormat) : moment(value);
                    let returnValue = '\u2014';
                    // use MSD earliest supported datetime to check for "null" dates
                    if (!m.isValid() || m.isBefore('1753-01-01T00:00:00Z')) {
                        resolve(returnValue);
                    }
                    returnValue = !!timezone ? m.tz(timezone).format(outputFormat) : m.format(outputFormat);
                    resolve(returnValue);
                });        
        });
    }
}

@NgModule({
    declarations: [MomentDatePipe],
    exports: [MomentDatePipe]
})
export class MomentDatePipeModule { }