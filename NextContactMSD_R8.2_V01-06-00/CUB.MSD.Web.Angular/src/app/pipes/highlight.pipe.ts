﻿import { Pipe, PipeTransform } from '@angular/core';

import * as _ from 'lodash';

@Pipe({
    name: 'highlight'
})
export class HighlightPipe implements PipeTransform {
    transform(input: string, search: string, placeholder?: string): string {
        if (!input || !search || !search.trim()) {
            return input;
        }

        // escape these regex special characters, ex. '(' => '\('
        search = search.replace(/[\-\[\]\/\{\}\(\)\+\?\.\\\^\$\|]/g, '\\$&');
        search = search.replace(/\*/g, '.*');

        if (placeholder) {
            const placeholderMatch = new RegExp(placeholder, 'g'); // ex. '_' => /_/g
            search = search.replace(placeholderMatch, '.?');
        }

        const searchExpression = new RegExp(search, 'i');
        const results = input.match(searchExpression);

        if (!results) {
            return input;
        }

        let resultLength = results[0].length;
        let highlightedValue = input.substr(0, results.index);
        highlightedValue += '<mark>';
        highlightedValue += input.substr(results.index, resultLength);
        highlightedValue += '</mark>';
        highlightedValue += input.substr(results.index + resultLength);
        
        return highlightedValue;
    }
}