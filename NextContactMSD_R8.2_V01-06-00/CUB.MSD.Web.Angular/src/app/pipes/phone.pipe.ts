﻿import { Pipe, PipeTransform } from '@angular/core';
import { conformToMask } from 'angular2-text-mask';

@Pipe({
    name: 'phone'
})
export class PhonePipe implements PipeTransform {    
    transform(value: any): any {
        var formatMaskUs = ['(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        var formatMaskUs11 = ['1', ' ', '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];        
        if (!value ||
            typeof value !== 'string') {
            return value;
        }
        return conformToMask(value, formatMaskUs, { guide: false }).conformedValue;
    }
}
