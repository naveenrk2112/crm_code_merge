import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cub_TA_StatusHistoryComponent } from './cub_ta_statushistory.component';

describe('Cub_TA_StatusHistoryComponent', () => {
    let component: Cub_TA_StatusHistoryComponent;
    let fixture: ComponentFixture<Cub_TA_StatusHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [Cub_TA_StatusHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Cub_TA_StatusHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
