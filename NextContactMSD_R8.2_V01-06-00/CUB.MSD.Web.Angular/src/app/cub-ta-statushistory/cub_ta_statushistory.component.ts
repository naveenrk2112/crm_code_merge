import { Component, Input, OnInit, NgZone, OnDestroy, HostListener, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as Model from '../model';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_TA_StatusHistoryService } from '../services/cub_ta_statushistory.service';
import * as moment from 'moment';
import * as lodash from 'lodash';
import { DataTableModule, LazyLoadEvent, SharedModule, SortMeta } from 'primeng/primeng';


@Component({
  selector: 'cub_ta_statushistory',
  templateUrl: './cub_ta_statushistory.component.html',
  providers: [Cub_TA_StatusHistoryService]
})
export class Cub_TA_StatusHistoryComponent implements OnInit {

    @Input() subsystemId: string = "";
    @Input() transitAccountId: string = "";
    @Output() closeClicked = new EventEmitter<boolean>();

    statusHistory: Array<object> = [];
    rowsPerPage: number = 5; //default
    initComplete: boolean = false;
    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cub_TA_StatusHistoryService: Cub_TA_StatusHistoryService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        //const queryParams = this.route.snapshot.queryParamMap;
        //if (queryParams.has('transitAccountId')) {
        //    this.transitAccountId = queryParams.get('transitAccountId');
        //}
        //if (queryParams.has('subSystemId')) {
        //    this.subsystemId = queryParams.get('subSystemId');
        //}

        this.transitAccountId = parent.Xrm.Page.getAttribute('cub_name').getValue();
        this.subsystemId = parent.Xrm.Page.getAttribute('cub_subsystemid').getValue();

        this._cub_dataService.GridConfigs
            .first(configs => !!configs['TA.StatusHistory'])
            .subscribe((configs) => {
                var grid = configs['TA.StatusHistory'];
                this.rowsPerPage = grid.rowsToDisplay;
                this.getStatusHistory();
            });
    }
    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.getStatusHistory();
        }
    }

    //Retrieve paged-portion of status history data
    getStatusHistory() {
      
       // let subSystemId = this.subsystemId;
       // let transitAccountId = this.transitAccountId;
        let offset = (this.pagingOffset > 0) ? this.pagingOffset : 1;  // NOTE:  DUE TO A BUG IN NIS THE OFFSET PARM NEEDS TO BE 1 NOT 0 FOR FIRST PAGE!!!!
        let limit = this.rowsPerPage;

        const done = this._loadingScreen.add('Loading Status History...');

        this._cub_TA_StatusHistoryService.getStatusHistory({
            subsystemId: this.subsystemId,
            transitaccountId: this.transitAccountId,
            offset: offset,
            limit: limit
        })
            .then((data) => {
                if (data) {
                    var statusHistory = JSON.parse(data.Body);
                    this.statusHistory = statusHistory.lineItems;
                    this.totalRecordCount = statusHistory.totalCount;
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting status history');
            })
            .then(() => this.initComplete = true)
            .then(() => done());
    }

    close() {
        this.closeClicked.emit(true);
    }

}
