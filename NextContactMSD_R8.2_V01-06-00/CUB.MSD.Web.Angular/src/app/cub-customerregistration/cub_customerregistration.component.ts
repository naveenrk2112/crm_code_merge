import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Cub_CustomerRegistrationService } from '../services/cub_customerregistration.service';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_SessionService } from '../services/cub-session.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import {
    Cub_Button,
    Cub_OptionSelect,
    Cub_OptionSelectOption,
    Cub_Textbox,
    Cub_Dropdown,
    Cub_OptionSelectAdditionalOption,
    Cub_DropdownOption,
    VerificationResult,
    Cub_Globals
} from '../model';

import * as lodash from 'lodash';
import * as $ from 'jquery';

@Component({
    selector: 'cub-customerregistration',
    templateUrl: './cub_customerregistration.component.html',
    providers: [
        Cub_CustomerRegistrationService,
        Cub_OneAccountService,
        Cub_SubsystemService
    ]
})
export class Cub_CustomerRegistrationComponent implements OnInit {
    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_customerRegistrationService: Cub_CustomerRegistrationService,
        private _loadingScreen: AppLoadingScreenService,
        private _cub_oneAccountService: Cub_OneAccountService,
        private _cub_sessionService: Cub_SessionService,
        private _cub_subsystemService: Cub_SubsystemService,
        private route: ActivatedRoute,
        private router: Router,
        private _renderer: Renderer2
    ) { }

    //For Customer Registration Form
    registrationControlsColumn1: Array<string> = [];
    registrationControlsColumn2: Array<string> = [];

    //To handle showing the Address verification warning before continuing
    addressVerificationWarning = {
        show: false,
        title: '',
        message: ''
    }

    //To handle showing the Address verification screen if more than one match is found
    addressVerificationScreen: {
        show: boolean, //show screen
        enteredAddress: { //The address to show as the entered address
            id?: string,
            line1: string,
            line2: string,
            city?: string,
            state?: string,
            zip: string
        }
        selectedAddress: Object, //The currently selected address (of verified addresses)
        verifiedAddresses: Array<Object> //The array of verified addresses to show (Same format as enteredAddress, just in an array of many)
    } = {
        show: false,
        selectedAddress: {},
        enteredAddress: {
            id: '',
            line1: '',
            line2: '',
            city: '',
            state: '',
            zip: ''
        },
        verifiedAddresses: []
    };

    //Holds zip id if confirmed
    zipId: string;

    //Whether the username should be foreced to be the same as the email
    isUsernameSameAsEmail: boolean = false;

    //Whether the address should be required to be filled in before proceeding
    isAddressRequired: boolean = false;

    //Max counts
    maxPhoneNumbers: number = 3;
    maxSecurityQuestions: number;
    minSecurityQuestions: number = 0;

    //globals for phone/security question
    phoneMask = {
        mask: [] as any,
        placeholderChar: '_' as string
    };
    phoneValidationRegEx: any = '';
    securityAnswerMaxLength = 0;

    //For Duplicates modal
    sortBy: string;
    sortOrder: string;
    sortOrderOptions: Array<Object>;
    sortOrderControl: Cub_Dropdown;

    rowsOnDuplicateModal: number;
    defaultContactType: any;

    //The results of the search are stored here for the duplicates popover to show, if needed
    duplicateSearch: {
        results: Array<Object>, //Duplicate results
        sortedResults: Array<Object>, //The sorted order to display them in
        show: boolean //show/hide duplicates popover
    } = {
        results: [],
        sortedResults: [],
        show: false
    };

    //The controls for the page (Note that we can add service calls for any of these 'hard coded' values that load on page load)
    registrationControls: any = {
        customerType: {
            type: "Dropdown",
            id: "customerType",
            label: "Customer Type",
            isRequired: true,
            errorMessage: 'Customer Type is required',
            fieldWidthClass: "form-field-width-75",
            spacerWidthClass: "form-field-width-25",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            options: []
        } as Cub_Dropdown,
        contactType: {
            type: "Dropdown",
            id: "contactType",
            label: "Contact Type",
            isRequired: true,
            errorMessage: 'Contact Type is required',
            fieldWidthClass: "form-field-width-75",
            spacerWidthClass: "form-field-width-25",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            options: []
        } as Cub_Dropdown,
        firstName: {
            type: "Textbox",
            id: "firstName",
            label: "First Name",
            isRequired: true,
            errorMessage: 'First Name is required',
            fieldWidthClass: "form-field-width-75",
            formFieldClass: 'form-fields-siblings',
            additionalControls: {
                middleInitial: {
                    type: "Textbox",
                    id: "middleInitial",
                    label: "MI",
                    fieldWidthClass: "form-field-width-25",
                    labelWidthClass: "label-width-50",
                    formFieldClass: 'form-fields-vertical',
                    maxLength: 1
                }
            },
            placeholder: ''
        } as Cub_Textbox,
        lastName: {
            type: "Textbox",
            id: "lastName",
            label: "Last Name",
            isRequired: true,
            errorMessage: 'Last Name is required',
            fieldWidthClass: "form-field-width-75",
            spacerWidthClass: "form-field-width-25",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
        } as Cub_Textbox,
        dateOfBirth: {
            type: "Textbox",
            id: "dateOfBirth",
            label: "Date Of Birth",
            validationMessage: "DOB is invalid",
            fieldWidthClass: "form-field-width-40",
            spacerWidthClass: "form-field-width-60",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            dateConfiguration: {
                showDatePicker: true,
            }
        } as Cub_Textbox,
        dateOfBirthDivider: {
            type: "Divider"
        },
        email: {
            type: "Textbox",
            id: "email",
            label: "Email",
            isRequired: true,
            errorMessage: "Email is required",
            validationMessage: "Email is invalid",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            value: '',
            placeholderChar: ' ',
        } as Cub_Textbox,
        phone1: {//Used for reference, this is handled by the addPhone function
        },
        addPhone: {
            type: "Button",
            id: 'addPhone',
            label: "Add Another Phone",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            class: 'button-secondary-text',
        } as Cub_Button,
        country: {
            type: "OptionSelect",
            id: "country",
            label: "Country",
            selectId: "countrySelect",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            dropdownFieldWidthClass: "form-field-width-60",
            value: '0',
            option1: {} as Cub_OptionSelectOption,
            option2: {} as Cub_OptionSelectOption,
            additionalOptions: [],
            labelWidthClass: '',
            errorMessage: '',
            isDisabled: false
        } as Cub_OptionSelect,
        addressline1: {
            type: "Textbox",
            id: "addressLine1",
            label: "Address",
            isRequired: false,
            errorMessage: "Address is required",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
        } as Cub_Textbox,
        addressline2: {
            type: "Textbox",
            id: "addressLine2",
            label: "",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
        } as Cub_Textbox,
        city: {
            type: "Textbox",
            id: "city",
            label: "City",
            isRequired: false,
            errorMessage: 'City is required',
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            isHidden: true
        } as Cub_Textbox,
        state: {
            type: "Dropdown",
            id: "state",
            label: "State",
            isRequired: false,
            errorMessage: 'State is required',
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            options: [],
            isHidden: true
        } as Cub_Dropdown,
        zip: {
            type: "Textbox",
            id: "zip",
            label: "ZIP",
            isRequired: false,
            errorMessage: 'ZIP is required',
            validationMessage: 'ZIP is invalid',
            fieldWidthClass: "form-field-width-30",
            spacerWidthClass: "form-field-width-70",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            placeholderChar: ' '
        } as Cub_Textbox,
        zipDivider: {
            type: "Divider"
        },
        username: {
            type: "Textbox",
            id: "username",
            label: "Username",
            errorMessage: 'Username is required',
            validationMessage: "Username is invalid",
            isRequired: true,
            fieldWidthClass: "form-field-width-75",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            placeholderChar: ' ',
            additionalControls: {
                usernameHelpText: {
                    type: "Help",
                    id: "usernameHelpText",
                    helpText: "Help",
                    popoverText: "Username must be at least 8 characters long and can contain letters (a-z), numbers (0-9), dashes (-), underscores (_), apostrophes ('), and periods (.).",
                    fieldWidthClass: 'form-field-width-25'
                }
            }
        } as Cub_Textbox,
        pin: {
            type: "Textbox",
            id: "pin",
            label: "PIN",
            isRequired: false,
            errorMessage: 'PIN is required',
            fieldWidthClass: "form-field-width-20",
            spacerWidthClass: "form-field-width-80",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings'
        } as Cub_Textbox,
        addSecurityQuestion: {
            type: "Button",
            id: 'addSecurityQuestion',
            label: "Add Another Question",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            class: 'button-secondary-text'
        } as Cub_Button
    };

    createCustomerButton: Cub_Button = {
        id: "CreateCustomer",
        label: "Create Customer",
        class: "button-primary",
        isDisabled: false
    };

    //Current counts
    public phoneCount: number = 0;
    public securityQuestionCount: number = 0;

    get selectedContact() {
        return this._cub_dataService.selectedContactCustomerToVerify;
    }
    set selectedContact(value: any) {
        this._cub_dataService.selectedContactCustomerToVerify = value;
    }
    redirectEntity: string;
    showVerification: boolean = false;
    postVerificationResult: VerificationResult;
    linkSubsystem: string;
    linkSubsystemAccountReference: string;

    ngOnInit() {
        let height = $('#navTabGroupDiv', top.document).css('height');
        $('#crmTopBar', top.document).hide();
        // NOTE: this currently gets overridden by MSD on initial page load
        $('#crmContentPanel', top.document).css('top', height);
        const bodyTag = document.getElementsByTagName('body')[0];
        this._renderer.addClass(bodyTag, 'grey-background');

        const queryParams = this.route.snapshot.queryParamMap;
        if (queryParams.has('subsystem') && queryParams.has('subsystemAccountReference')) {
            this.linkSubsystem = queryParams.get('subsystem');
            this.linkSubsystemAccountReference = queryParams.get('subsystemAccountReference');
        }

        this.router.navigate([
            '/CustomerRegistration',
            { outlets: { session: 'Session' } }
        ], { replaceUrl: true });

        const globalsDone = this._loadingScreen.add('Retrieving global data');
        this._cub_dataService.Globals
            .first(globals => !!globals.ValidateEmailFormat && !!globals.CustomerRegistration && !!globals.UsernameValidation && !!globals.PinValidation)
            .subscribe(globals => {
                this.loadConstants(globals);
                globalsDone();

                //Load metadata for form before making form available to the user
                var getStates = this._cub_customerRegistrationService.getStates();
                var getAccountTypes = this._cub_customerRegistrationService.getAccountTypes();
                var getContactTypes = this._cub_customerRegistrationService.getContactTypes();
                var getSecurityQuestions = this._cub_customerRegistrationService.getSecurityQuestions();
                var getCountries = this._cub_customerRegistrationService.getCountries();
                var getPhoneTypes = this._cub_customerRegistrationService.getPhoneTypes();

                //Execute all metadata promises
                const allDone = this._loadingScreen.add();
                Promise.all([getStates, getAccountTypes, getContactTypes, getSecurityQuestions, getCountries, getPhoneTypes])
                    .then(values => {
                        this.processStates(values[0]);
                        this.processAccountTypes(values[1]);
                        this.processContactTypes(values[2]);
                        this.processSecurityQuestions(values[3]);
                        this.processCountries(values[4]);
                        this.processPhoneTypes(values[5]);
                    })
                    .catch(error => {
                        this._cub_dataService.prependCurrentError("Error retrieving metadata");
                        this._cub_dataService.errorScreen.fatalState = true;
                    })
                    .then(() => allDone());
            });
    }

    loadConstants(globals: Cub_Globals) {
        //Username/Email configuration
        this.isUsernameSameAsEmail = (globals.UsernameValidation.UsernameRequireEmail === 1);

        //stop here for test
        this.isAddressRequired = (globals.CustomerRegistration.IsAddressRequired === 1);

        if (globals.ValidateEmailFormat.EmailMatchRegex) {
            //TODO: Fix Reg Ex
            //this.registrationControls.username.validationRegEx = new RegExp(globals.ValidateEmailFormat.EmailMatchRegex);
            //this.registrationControls.email.validationRegEx = new RegExp(globals.ValidateEmailFormat.EmailMatchRegex);
        }
        if (globals.CustomerRegistration.Email_MaxLength) {
            this.registrationControls.email.maxLength = globals.CustomerRegistration.Email_MaxLength;
        }
        if (globals.CustomerRegistration.Username_MaxLength) {
            this.registrationControls.username.maxLength = globals.CustomerRegistration.Username_MaxLength;
        }

        //Customer Type Configuration
        if (globals.CustomerRegistration.CustomerType_DefaultValue) {
            this.registrationControls.customerType.value = globals.CustomerRegistration.CustomerType_DefaultValue;
        }

        //Contact Type Configuration
        if (globals.CustomerRegistration.ContactType_DefaultValue) {
            this.registrationControls.contactType.value = globals.CustomerRegistration.ContactType_DefaultValue;
            this.defaultContactType = globals.CustomerRegistration.ContactType_DefaultValue;
        }

        //First Name Configuration
        if (globals.CustomerRegistration.FirstName_MaxLength) {
            this.registrationControls.firstName.maxLength = globals.CustomerRegistration.FirstName_MaxLength;
        }

        //Last Name Configuration
        if (globals.CustomerRegistration.LastName_MaxLength) {
            this.registrationControls.lastName.maxLength = globals.CustomerRegistration.LastName_MaxLength;
        }

        //DOB Configuration
        if (globals.CustomerRegistration.DOB_ValidationRegEx) {
            let pattern = globals.CustomerRegistration.DOB_ValidationRegEx as string;
            this.registrationControls.dateOfBirth.validationRegEx = new RegExp(pattern);
        }
        if (globals.CustomerRegistration.DOB_Placeholder) {
            this.registrationControls.dateOfBirth.placeholder = globals.CustomerRegistration.DOB_Placeholder;
        }
        if (globals.CustomerRegistration.DOB_DateFormat) {
            this.registrationControls.dateOfBirth.dateConfiguration.dateFormat = globals.CustomerRegistration.DOB_DateFormat;
        }
        if (globals.CustomerRegistration.DOB_Mask) {
            this.registrationControls.dateOfBirth.mask = [];
            let value = globals.CustomerRegistration.DOB_Mask as string;
            let splitMask = value.split('|');
            for (let i = 0; i < splitMask.length; i++) {
                let mask = splitMask[i];
                if (mask.length > 1) {
                    this.registrationControls.dateOfBirth.mask.push(new RegExp(mask));
                }
                else {
                    this.registrationControls.dateOfBirth.mask.push(mask);
                }
            }
        }

        //Phone Configuration
        if (globals.CustomerRegistration.Phone_ValidationRegEx) {
            let pattern = globals.CustomerRegistration.Phone_ValidationRegEx as string;
            this.phoneValidationRegEx = new RegExp(pattern);
        }
        if (globals.CustomerRegistration.Phone_Mask) {
            let value = globals.CustomerRegistration.Phone_Mask as string;
            const characterMaskList = value.split('|')
                .map(char => char.length > 1 ? new RegExp(char) : char);

            // For some reason the text-mask sets the input to "(___) ___-____"
            // if the first character doesn't match rather than leave it blank, so
            // we use this function to force a blank input if the first character is
            // unrelated to a phone number. This function fires when the input value 
            // changes. It accepts the updated input value and returns a list of character masks.
            // https://github.com/text-mask/text-mask/blob/master/componentDocumentation.md#mask-function
            const determineMask = (rawInput) => {
                if (!rawInput) {
                    return characterMaskList;
                }
                // first character should be 1-9 or satisfy the first character mask
                if (!rawInput[0].match(/[1-9]/)
                    && ((typeof characterMaskList[0] === 'string' && rawInput[0] !== characterMaskList[0])
                        || (characterMaskList[0] instanceof RegExp && !rawInput[0].match(characterMaskList[0])))
                ) {
                    return [/[1-9]/];
                }
                return characterMaskList;
            };
            // We have to reassign the object to update the mask field.
            this.phoneMask = Object.assign({}, this.phoneMask, { mask: determineMask });
        }

        //Address line 1/2 Configuration
        if (globals.CustomerRegistration.AddressLine1_MaxLength) {
            this.registrationControls.addressline1.maxLength = globals.CustomerRegistration.AddressLine1_MaxLength;
        }
        if (globals.CustomerRegistration.AddressLine2_MaxLength) {
            this.registrationControls.addressline2.maxLength = globals.CustomerRegistration.AddressLine2_MaxLength;
        }

        //City Configuration
        if (globals.CustomerRegistration.City_MaxLength) {
            this.registrationControls.city.maxLength = globals.CustomerRegistration.City_MaxLength;
        }

        //Zip Configuration
        if (globals.CustomerRegistration.Zip_MaxLength) {
            this.registrationControls.zip.maxLength = globals.CustomerRegistration.Zip_MaxLength;
        }
        if (globals.CustomerRegistration.Zip_ValidationRegEx) {
            let pattern = globals.CustomerRegistration.Zip_ValidationRegEx as string;
            this.registrationControls.zip.validationRegEx = new RegExp(pattern);
        }

        //PIN Configuration
        if (globals.PinValidation.PinMaxLength) {
            this.registrationControls.pin.maxLength = globals.PinValidation.PinMaxLength;
        }

        //Security QAs Configuration
        if (globals.CustomerRegistration.SecurityQuestions_Max) {
            this.maxSecurityQuestions = globals.CustomerRegistration.SecurityQuestions_Max as number;
        }
        if (globals.CustomerRegistration.SecurityQuestions_Minimum) {
            this.minSecurityQuestions = globals.CustomerRegistration.SecurityQuestions_Minimum as number;
        }
        if (globals.CustomerRegistration.SecurityAnswer_MaxLength) {
            this.securityAnswerMaxLength = globals.CustomerRegistration.SecurityAnswer_MaxLength as number;
        }

        this.afterLoading();
    }

    //After all data is pulled from service
    afterLoading() {
        this.registrationControls.firstName.value = this._cub_dataService.searchValues['firstName'];
        this.registrationControls.lastName.value = this._cub_dataService.searchValues['lastName'];
        this.registrationControls.phone1.value = this._cub_dataService.searchValues['phone'];
        this.registrationControls.email.value = this._cub_dataService.searchValues['email'];
     
        if (this.isUsernameSameAsEmail) {
            this.registrationControls.username.isDisabled = true;
            this.registrationControls.username.isRequired = false;
            this.registrationControls.username.value = this.registrationControls.email.value ? this.registrationControls.email.value.trim() : this.registrationControls.email.value;
        }

        if (this.isAddressRequired) {
            this.registrationControls.addressline1.isRequired = true;
            this.registrationControls.zip.isRequired = true;
            this.registrationControls.state.isRequired = true;
            this.registrationControls.city.isRequired = true;
            this.registrationControls.country.isRequired = true;
        }

        //Used for Duplicates popover
        this.sortBy = "BestMatch";
        this.sortOrder = "asc";
        this.sortOrderOptions = [
            { value: "BestMatch", name: "Best Match" },
            { value: "CustomerName", name: "Customer Name" },
            { value: "FullName", name: "Contact Full Name" },
            { value: "FirstName", name: " Contact First Name" },
            { value: "LastName", name: "Contact Last Name" },
            { value: "Email", name: "Email" }
        ];
        this.sortOrderControl = {
            formFieldClass: 'form-fields-vertical', //Form field class to use, eg. form-fields-siblings
            labelWidthClass: 'form-field-width-S', //Width of label
            fieldWidthClass: "form-field-width-XL", //Width of Control
            isTableControl: false,
            isSmall: true,
            id: 'sortOrderControl', //unique control id
            label: 'Sort by', //Label for control
            value: "BestMatch", //Value for control
            options: this.sortOrderOptions //Array of objects: {name: "Label Text", value: "Value"}
        } as Cub_Dropdown;

        //How many rows to show at once on the Duplicate popover
        this.rowsOnDuplicateModal = 3;

        this.registrationControlsColumn1 = ['customerType', 'contactType', 'firstName', 'lastName', 'dateOfBirth', 'dateOfBirthDivider', 'email', 'addPhone'];
        this.registrationControlsColumn2 = ['country', 'addressline1', 'addressline2', 'city', 'state', 'zip', 'zipDivider', 'username', 'pin', 'addSecurityQuestion'];

        this.validate(true);
    }

    //Promise resolve for getStates
    processStates(data: any) {
        this.registrationControls.state.options = [{
            value: "0",
            id: "0",
            name: "Select...",
            isDisabled: true
        } as Cub_DropdownOption];

        for (var i = 0; i < data.length; i++) {
            var state = data[i];
            this.registrationControls.state.options.push({
                value: state['Key'],
                id: state['Key'],
                name: state['Value']
            } as Cub_DropdownOption);
        }
    }

    //Promise resolve for getAccountTypes
    processAccountTypes(this: any, data: any) {
        for (var i = 0; i < data.length; i++) {
            var accountTypes = data[i];
            this.registrationControls.customerType.options.push({
                value: accountTypes['Key'],
                id: accountTypes['Key'],
                name: accountTypes['Value']
            } as Cub_DropdownOption);
        }
        if (data.length > 0) {
            this.registrationControls.customerType.value = data[0]['Key'];
        }
    }

    //Promise resolve for getContactTypes
    processContactTypes(this: any, data: any) {
        for (var i = 0; i < data.length; i++) {
            var contactType = data[i];
            if (contactType['Value'] == 'Primary') { // load primary only
                this.registrationControls.contactType.options.push({
                    value: contactType['Key'],
                    id: contactType['Key'],
                    name: contactType['Value']
                } as Cub_DropdownOption);
            }
        }
        if (data.length > 0) {
            this.registrationControls.contactType.value = this.defaultContactType;
            this.registrationControls.contactType.isDisabled = true;
        }
        
    }

    //Promise resolve for getSecurityQuestions
    processSecurityQuestions(this: any, data: any) {
        this._cub_customerRegistrationService.securityQuestionOptions = [{
            value: "0",
            id: "0",
            name: "Select...",
            isDisabled: true
        } as Cub_DropdownOption];

        for (var i = 0; i < data.length; i++) {
            var securityQuestion = data[i];
            this._cub_customerRegistrationService.securityQuestionOptions.push({
                value: securityQuestion['Key'],
                id: securityQuestion['Key'],
                name: securityQuestion['Value']
            } as Cub_DropdownOption);
        }
        this.addSecurityQuestion();
    }

    //Promise resolve for getCountries
    processCountries(this: any, data: any) {
        this.registrationControls.country.additionalOptions = [{
            value: "0",
            name: "Select...",
            isDisabled: true
        } as Cub_OptionSelectAdditionalOption];

        for (var i = 0; i < data.length; i++) {
            var country = data[i];
            if (i == 0) {
                this.registrationControls.country.option1 = {
                    value: country['Key'],
                    id: country['Key'],
                    name: country['Value'],
                    fieldWidthClass: 'form-field-width-15'
                } as Cub_OptionSelectOption
            }
            else if (i == 1) {
                this.registrationControls.country.option2 = {
                    value: country['Key'],
                    id: country['Key'],
                    name: country['Value'],
                    fieldWidthClass: 'form-field-width-20'
                } as Cub_OptionSelectOption
            }
            else {
                this.registrationControls.country.additionalOptions.push({
                    value: country['Key'],
                    id: country['Key'],
                    name: country['Value'],
                } as Cub_OptionSelectAdditionalOption);
            }
        }
        this.registrationControls.country.value = this.registrationControls.country.option1.value;
    }

    //Promise resolve for getPhoneTypes
    processPhoneTypes(this: any, data: any) {
        this._cub_customerRegistrationService.phoneTypeAdditionalOptions = [{
            value: "0",
            name: "Select...",
            isDisabled: true
        } as Cub_OptionSelectAdditionalOption];

        for (var i = 0; i < data.length; i++) {
            var phoneType = data[i];
            if (i == 0) {
                this._cub_customerRegistrationService.phoneTypeOption1 = {
                    value: phoneType['Key'],
                    id: phoneType['Key'],
                    name: phoneType['Value'],
                    fieldWidthClass: '30'
                } as Cub_OptionSelectOption
            }
            else if (i == 1) {
                this._cub_customerRegistrationService.phoneTypeOption2 = {
                    value: phoneType['Key'],
                    id: phoneType['Key'],
                    name: phoneType['Value'],
                    fieldWidthClass: '25'
                } as Cub_OptionSelectOption
            }
            else {
                this._cub_customerRegistrationService.phoneTypeAdditionalOptions.push({
                    value: phoneType['Key'],
                    id: phoneType['Key'],
                    name: phoneType['Value'],
                    //fieldWidth: '40'
                }) as Cub_OptionSelectAdditionalOption;
            }
        }
        this.addPhone(true);
    }

    //Handle adding a phone to the form (on load for the first and if button is clicked)
    addPhone(isFirst: boolean = false) {
        this.phoneCount++;
        this.registrationControls['phone' + this.phoneCount] = {
            type: "Textbox",
            id: 'phone' + this.phoneCount,
            isRequired: isFirst,
            label: "Phone",
            errorMessage: "Phone is required",
            validationMessage: "Phone is invalid or duplicates exist",
            fieldWidthClass: "form-field-width-40",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            value: isFirst ? this._cub_dataService.searchValues['phone'] : '',
            isRemovable: !isFirst,
            additionalControls: new Array(),
            placeholderChar: '_',
            mask: this.phoneMask.mask,
            validationRegEx: this.phoneValidationRegEx
        } as Cub_Textbox;
        this.registrationControls['phone' + this.phoneCount].additionalControls['phoneType' + this.phoneCount] = {
            type: "OptionSelect",
            id: "phoneType" + this.phoneCount,
            label: "",
            selectId: "phoneType" + + this.phoneCount + "Select",
            fieldWidthClass: "form-field-width-60",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            dropdownFieldWidthClass: "form-field-width-45",
            value: '0',
            option1: (lodash.cloneDeep(Object.assign({}, this._cub_customerRegistrationService.phoneTypeOption1, { id: 'phoneType' + this.phoneCount + 'Options' + this._cub_customerRegistrationService.phoneTypeOption1['id'] }))) as Cub_OptionSelectOption,
            option2: (lodash.cloneDeep(Object.assign({}, this._cub_customerRegistrationService.phoneTypeOption2, { id: 'phoneType' + this.phoneCount + 'Options' + this._cub_customerRegistrationService.phoneTypeOption2['id'] }))) as Cub_OptionSelectOption,
            additionalOptions: this._cub_customerRegistrationService.phoneTypeAdditionalOptions,
            labelWidthClass: ''
        } as Cub_OptionSelect;

        this.registrationControlsColumn1.splice(this.registrationControlsColumn1.indexOf('addPhone'), 0, 'phone' + this.phoneCount);
        this.registrationControls['phone' + this.phoneCount].additionalControls['phoneType' + this.phoneCount].value = this.registrationControls['phone' + this.phoneCount].additionalControls['phoneType' + this.phoneCount].option1.value;
    }

    //Handle adding a security question/answer to the form (on load for the first and if button is clicked)
    addSecurityQuestion() {
        this.securityQuestionCount++;
        this.registrationControls['securityQuestion' + this.securityQuestionCount] = {
            type: "Dropdown",
            id: 'securityQuestion' + this.securityQuestionCount,
            label: "Security Question",
            validationMessage: "Duplicates exist",
            errorMessage: "Security Question is required",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            value: '0',
            isRemovable: this.securityQuestionCount !== 1,
            isRequired: this.securityQuestionCount <= this.minSecurityQuestions,
            options: this._cub_customerRegistrationService.securityQuestionOptions,
            count: this.securityQuestionCount
        } as Cub_Dropdown;
        this.registrationControls['securityAnswer' + this.securityQuestionCount] = {
            type: "Textbox",
            id: 'securityAnswer' + this.securityQuestionCount,
            label: "Security Answer",
            errorMessage: "Security Answer is required",
            fieldWidthClass: "form-field-width-100",
            isTableControl: true,
            formFieldClass: 'form-fields-siblings',
            isRequired: this.securityQuestionCount <= this.minSecurityQuestions,
            value: '',
            maxLength: this.securityAnswerMaxLength
        } as Cub_Textbox;

        this.registrationControlsColumn2.splice(this.registrationControlsColumn2.indexOf('addSecurityQuestion'), 0, 'securityQuestion' + this.securityQuestionCount);
        this.registrationControlsColumn2.splice(this.registrationControlsColumn2.indexOf('addSecurityQuestion'), 0, 'securityAnswer' + this.securityQuestionCount);
    }

    //Handle anything that needs to attempt to run when focus is lost on an input
    inputFocusOut(controlData: any): void {
        if (controlData) {
            var newValue = controlData[0];
            var control = controlData[1];

            //Handle zip code validation/warnings/auto compllete
            if (!control.isInvalid && control.id == 'zip' && control.value) {
                const zipValidationDone = this._loadingScreen.add('Validating zip...');

                this._cub_customerRegistrationService.checkZipPostalCode(this.registrationControls)
                    .then((data: any) => {
                        if (data && data.length > 0) {
                            //If multiples found
                            if (data.length > 1) {
                                this.addressVerificationScreen.verifiedAddresses = [];
                                for (var key in data) {
                                    var address = data[key];
                                    this.addressVerificationScreen.verifiedAddresses.push({
                                        //TODO: Fill in rest of address from validation when implemented instead of what the user entered
                                        line1: this.registrationControls['addressline1'].value,
                                        line2: this.registrationControls['addressline2'].value,
                                        city: address['City'],
                                        state: address['State'].Value,
                                        stateId: address['State'].Key,
                                        zipId: address['ID'],
                                        zip: address['ZipPostal']
                                    });

                                }
                                this.addressVerificationScreen.enteredAddress = {
                                    line1: this.registrationControls['addressline1'].value,
                                    line2: this.registrationControls['addressline2'].value,
                                    zip: this.registrationControls['zip'].value,
                                }
                                this.addressVerificationScreen.show = true;
                            }
                            else {
                                //If one found
                                this.registrationControls['city'].value = data[0].City;
                                this.registrationControls['state'].value = data[0].State.Key;
                            }
                            this.zipId = data[0].ID;
                        }
                        else {
                            //If none found
                            this.addressVerificationWarning.title = 'Unable to verify zip code';
                            this.addressVerificationWarning.message = 'Please check the entered zip code and make changes if necessary.';
                            this.addressVerificationWarning.show = true;
                            this.zipId = '';
                            this.registrationControls.city.value = '';
                            this.registrationControls.state.value = '0';
                        }
                        this.registrationControls.city.isHidden = false;
                        this.registrationControls.state.isHidden = false;
                    })
                    .catch((error: any) => {
                        this._cub_dataService.prependCurrentError("Error checking zip code");
                    })
                    .then(() => zipValidationDone())
            }
            else if (control.id == 'zip' && !control.value) {
                this.registrationControls.city.isHidden = true;
                this.registrationControls.state.isHidden = true;
            }
            //For checking if a username (or email depending on configuration) is valid/available
            else if (!control.isInvalid && control.value && (control.id == 'username' && !this.isUsernameSameAsEmail || control.id == 'email' && this.isUsernameSameAsEmail)) {
                const validationDone = this._loadingScreen.add('Validating...');

                this._cub_customerRegistrationService.checkUsername(this.registrationControls)
                    .then((data: any) => {
                        if (!data.isUsernameAvailable || !data.isUsernameValid) {
                            if (this.isUsernameSameAsEmail) {
                                this.registrationControls.email.originalErrorMessage = this.registrationControls.email.errorMessage;
                                this.registrationControls.email.errorMessage = data.usernameErrors && data.usernameErrors.length > 0 ? data.usernameErrors[0].message : 'Email is unavailable';
                                this.registrationControls.email.forceError = true;
                            }
                            else {
                                this.registrationControls.username.originalErrorMessage = this.registrationControls.username.errorMessage;
                                this.registrationControls.username.errorMessage = data.usernameErrors && data.usernameErrors.length > 0 ? data.usernameErrors[0].message : 'Username is unavailable';
                                this.registrationControls.username.forceError = true;
                            }
                        }
                        else {
                            if (this.isUsernameSameAsEmail) {
                                this.registrationControls.email.errorMessage = this.registrationControls.email.originalErrorMessage
                                this.registrationControls.email.forceError = false;
                            }
                            else {
                                this.registrationControls.username.errorMessage = this.registrationControls.username.originalErrorMessage
                                this.registrationControls.username.forceError = false;
                            }
                        }
                        this.validate(true);
                    })
                    .catch((error: any) => {
                        this._cub_dataService.prependCurrentError("Error checking username");
                    })
                    .then(() => validationDone());
            }
            //Handle duplicate phone numbers
            else if (control.id.startsWith('phone') && control.value) {
                var phones = [];

                lodash.each(this.registrationControlsColumn1, (o) => {
                    if (o && o.startsWith('phone')) {
                        phones.push(o);
                    }
                });

                lodash.each(phones, (o) => {
                    this.registrationControls[o].forceInvalid = false;
                    this.registrationControls[o].showErrorMessage = false;
                })

                var duplicateResults = lodash.filter(phones, (value, index, iteratee) => {
                    return lodash.filter(phones, (o) => {
                        return this.registrationControls[o].value == this.registrationControls[value].value;
                    }).length > 1;
                });

                lodash.each(duplicateResults, (o) => {
                    this.registrationControls[o].forceInvalid = true;
                });
            }
        }
    }

    //Handle button clicks
    buttonClicked(data: any): void {
        if (data) {
            var newValue = data[0];
            var control = data[1];
            //When "Add Phone" is clicked
            if (control.id == 'addPhone') {
                if (this.phoneCount != this.maxPhoneNumbers) {
                    this.addPhone();
                }

                control.isHidden = this.phoneCount == this.maxPhoneNumbers;
            }
            //When "Add Security Question" is clicked
            else if (control.id == 'addSecurityQuestion') {
                if (this.securityQuestionCount < this.maxSecurityQuestions) {
                    this.addSecurityQuestion();
                }

                // hide the Add button if max are currently displayed
                control.isHidden = this.securityQuestionCount == this.maxSecurityQuestions;
            }
        }
    }

    //When a Phone Number or Security question/answer "X" is clicked
    removedClicked(controlIds: any): void {
        if (controlIds) {
            if (controlIds[0].startsWith('phone')) { //Phone removable
                this.registrationControlsColumn1.splice(this.registrationControlsColumn1.indexOf(controlIds[0]), 1)
                this.phoneCount--;
                this.registrationControls.addPhone.isHidden = this.phoneCount == this.maxPhoneNumbers;
            }
            else if (controlIds[0].startsWith('securityQuestion')) { //Security Question/Answer removable
                this.registrationControlsColumn2.splice(this.registrationControlsColumn2.indexOf(controlIds[0]), 2)
                this.securityQuestionCount--;
                this.registrationControls.addSecurityQuestion.isHidden = this.securityQuestionCount == this.maxSecurityQuestions;
            }
        }
    }

    //When any data is changed
    inputChange(data: any): void {
        var newValue = data[0]
        var control = data[1];
        //Copy email to username if option bit is flipped
        if (this.isUsernameSameAsEmail && control.id == 'email') {
            this.registrationControls.username.value = newValue ? newValue.trim() : newValue;
        }

        //Handle duplicate security questions
        if (control.id.startsWith('securityQuestion') && control.value) {
            var securityQuestions = [];

            lodash.each(this.registrationControlsColumn2, (o) => {
                if (o && o.startsWith('securityQuestion')) {
                    securityQuestions.push(o);
                }
            });

            lodash.each(securityQuestions, (o) => {
                this.registrationControls[o].forceInvalid = false;
                this.registrationControls[o].showErrorMessage = false;
            })

            var duplicateResults = lodash.filter(securityQuestions, (value, index, iteratee) => {
                return lodash.filter(securityQuestions, (o) => {
                    return this.registrationControls[o].value == this.registrationControls[value].value;
                }).length > 1;
            });

            lodash.each(duplicateResults, (o) => {
                this.registrationControls[o].forceInvalid = true;
            });
        }

        this.validate(true);
    }

    //When "Create Customer" button is clicked
    createButtonClicked(event: any) {
        var isValidated = this.validate(null, true);
        if (!isValidated) {
            return;
        }
        this.createCustomerButton.isDisabled = true;
        const done = this._loadingScreen.add("Searching for duplicates...");
        this._cub_customerRegistrationService.searchCustomer(this.registrationControls)
            .then((data: any) => {
                if (data && data.length > 0) {
                    this.duplicateSearch.results = data;
                    this.duplicateSearch.sortedResults = data;
                    this.duplicateSearch.show = true;
                }
                else {
                    this.registerCustomer();
                }
            })
            .catch((error: any) => {
                this._cub_dataService.onApplicationError(error);
                this.duplicateSearch.sortedResults = [];
                this.duplicateSearch.results = [];
            })
            .then(() => done())
            .then(() => this.createCustomerButton.isDisabled = false);
    }

    //When the enter button is pressed on a focused control
    onEnterPress(event: any) {
        this.createButtonClicked(null);
    }

    registerCustomer() {
        const done = this._loadingScreen.add('Creating Customer...');
        var securityQAs = [];

        var questions = lodash.pickBy(this.registrationControlsColumn2, function (value, key) {
            return lodash.startsWith(value, 'securityQuestion');
        })

        for (var key in questions) {
            var question = this.registrationControls[questions[key]];
            var answer = this.registrationControls['securityAnswer' + question.count];
            if (question && question.value && question.value != 0 && answer && answer.value) {
                securityQAs.push({
                    Key: question.value,
                    Value: answer.value
                });
            }
        }

        var phone1 = this.registrationControls['phone1'];
        var phone2 = this.registrationControls['phone2'];
        var phone3 = this.registrationControls['phone3'];

        var customer = {
            AccountType: this.registrationControls['customerType'].value,
            ContactType: this.registrationControls['contactType'].value,
            FirstName: this.registrationControls['firstName'].value,
            LastName: this.registrationControls['lastName'].value,
            MiddleNameInitial: this.registrationControls['firstName'].additionalControls['middleInitial'].value,
            BirthDate: this.registrationControls['dateOfBirth'].value,
            Email: this.registrationControls['email'].value,
            Phone1Type: phone1 && phone1.value ? phone1.additionalControls['phoneType1'].value : null,
            Phone1: phone1 ? phone1.value : '',
            Phone2Type: phone2 && phone2.value ? phone2.additionalControls['phoneType2'].value : null,
            Phone2: phone2 ? phone2.value : '',
            Phone3Type: phone3 && phone3.value ? phone3.additionalControls['phoneType3'].value : null,
            Phone3: phone3 ? phone3.value : '',
            Address1: this.registrationControls['addressline1'].value,
            Address2: this.registrationControls['addressline2'].value,
            City: this.registrationControls['city'].value,
            State: this.registrationControls['state'].value,
            ZipPostalCodeId: this.zipId,
            Zip: this.registrationControls['zip'].value,
            Country: this.registrationControls['country'].value,
            SecurityQAs: securityQAs,
            PIN: this.registrationControls['pin'].value,
            UserName: this.registrationControls['username'].value
        }

        this._cub_customerRegistrationService.registerCustomer(customer)
            .then((data: any) => {
                if (data == null) {
                    return Promise.reject('No response from registration request');
                }
                if (typeof data === 'object') {
                    //Array of validation errors returned
                    for (var key in data) {
                        this._cub_dataService.showError('Validation error: ' + data[key]['Key'] + ' - ' + data[key]['Value']);
                    }
                }
                else {
                    this.customerCreated(data);
                }
            })
            .catch((error: any) => this._cub_dataService.onApplicationError(error))
            .then(() => done());
    }

    customerCreated(customerId: string) {
        this.redirectEntity = 'account';
        let clearLoadingMessage;
        let optionalLinkPromise = Promise.resolve();

        if (this.linkSubsystem && this.linkSubsystemAccountReference) {
            optionalLinkPromise = optionalLinkPromise.then(() => {
                clearLoadingMessage = this._loadingScreen.add('Linking subsystem account to customer');
                return this._cub_oneAccountService.getCachedOneAccountSummary(customerId);
            }).then((summary: any) => {
                if (summary && summary.oneAccountId != null) {
                    return this._cub_oneAccountService.LinkOneAccountToSubsystem(summary.oneAccountId,
                        this.linkSubsystem, this.linkSubsystemAccountReference, this.linkSubsystemAccountReference);
                }
            });
        }

        optionalLinkPromise
            .then(() => this.loadSessionAndRedirect(customerId))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => clearLoadingMessage());
    }

    //Cancel button clicked
    cancelButtonClicked() {
        this._cub_dataService.cancelScreen.cancelButtonRoute = "/MasterSearchContact";
        this._cub_dataService.cancelScreen.show = true;
    }

    //When acknowledging the address verification warning
    addressVerificationWarningClicked(event: any) {
        this.addressVerificationWarning.show = false;
    }

    //When selecting a different verified address, update reference in case user confirms it
    addressVerificationChanged(event: any) {
        this.addressVerificationScreen.selectedAddress = lodash.pickBy(this.addressVerificationScreen.verifiedAddresses, function (value, key) {
            return value.zipId == event.target.id;
        });
        this.addressVerificationScreen.selectedAddress = this.addressVerificationScreen.selectedAddress[Object.keys(this.addressVerificationScreen.selectedAddress)[0]];
    }

    //After confirming address
    addressVerifiedClicked(event: any) {
        if (this.addressVerificationScreen.selectedAddress) {
            this.registrationControls.state.value = this.addressVerificationScreen.selectedAddress['stateId'];
            this.registrationControls.city.value = this.addressVerificationScreen.selectedAddress['city'];
        }
        this.addressVerificationScreen.selectedAddress = {};
        this.addressVerificationScreen.show = false;
    }

    //Deciding to use the entered address as-is
    addressEnteredClicked(event: any) {
        this.addressVerificationScreen.selectedAddress = {};
        this.registrationControls.state.value = '0';
        this.registrationControls.city.value = '';
        this.addressVerificationScreen.show = false;
    }

    //Handle sorting of duplicates popover
    onSortDropdownChanged(event: any) {
        if (!event || event.length <= 0) {
            return;
        }
        var selectedValue = event[0];
        this.sortResults(selectedValue, true);
    }

    sortResults(field: string, inAscendingOrder: boolean) {
        if (field.toLocaleLowerCase() === "BestMatch".toLocaleLowerCase()) {
            this.duplicateSearch.sortedResults = this.duplicateSearch.results;
            return;
        }
        if (!this.duplicateSearch.results ||
            !this.duplicateSearch.results[0] ||
            !this.duplicateSearch.results[0][field]) {
            console.log("Error while sorting results");
            return;
        }
        var sortOrder = 'desc'
        if (inAscendingOrder) {
            sortOrder = 'asc';
        }
        this.duplicateSearch.sortedResults = lodash.orderBy(this.duplicateSearch.results, [field], ['asc']);
    }

    //If a duplicate's 'verify' is clicked, load verification popover
    verifyContact(record: any, redirectEntity: string) {
        this.selectedContact = record;
        this.redirectEntity = redirectEntity;
        this.showVerification = true;
    }

    //Handle clicking ignore duplicates
    ignoreDuplicatesClicked(event: any) {
        this.duplicateSearch.show = false;
        this.registerCustomer();
    }

    //Handle clicking cancel duplicates
    cancelDuplicatesClicked(event: any) {
        this.duplicateSearch.show = false;
    }

    //Handle how required/validation erorrs are shown to the user
    validate(clear: boolean = false, showMessage: boolean = false) {
        var isValidated = true;
        var controlKeys = this.registrationControlsColumn1.concat(this.registrationControlsColumn2);

        for (var key in controlKeys) {
            var control = this.registrationControls[controlKeys[key]];
            var newValue = control.value;
            switch (control.type) {
                case "Textbox":
                    {
                        //Means that this error was set outside of this function, so invalid
                        if (control.forceError) {
                            isValidated = false;
                            control.isErrored = true;
                            control.isInvalid = false;
                            control.showErrorMessage = true;
                        }
                        //Means that this error was set outside of this function, so invalid
                        else if (control.forceInvalid) {
                            isValidated = false;
                            control.isInvalid = true;
                            control.showErrorMessage = true;
                        }
                        else if (control.isRequired && !newValue && !clear) {
                            control.isErrored = true;
                            control.isInvalid = true;
                            control.showErrorMessage = showMessage;
                            isValidated = false;
                        }
                        else if (control.validationRegEx && newValue && !control.validationRegEx.test(newValue)) {
                            control.isInvalid = true;
                            control.isErrored = false;
                            control.showErrorMessage = showMessage;
                            isValidated = false;
                        }
                        else {
                            control.isErrored = false;
                            control.isInvalid = false;
                            control.showErrorMessage = false;
                        }
                        break;
                    }
                case "Dropdown":
                case "OptionSelect":
                    {
                        //Means that this error was set outside of this function, so invalid
                        if (control.forceError) {
                            isValidated = false;
                            control.isErrored = true;
                            control.showErrorMessage = true;
                        }
                        //Means that this error was set outside of this function, so invalid
                        else if (control.forceInvalid) {
                            isValidated = false;
                            control.isInvalid = true;
                            control.showErrorMessage = true;
                        }
                        else if (control.isRequired && (!newValue || newValue == '0') && !clear) {
                            control.isErrored = true
                            control.showErrorMessage = showMessage;
                            isValidated = false;
                        }
                        else {
                            control.isErrored = false;
                            control.showErrorMessage = false;
                        }
                        break;
                    }
            }
        }

        return isValidated;
    }

    resolveVerification(event: VerificationResult) {
        this.showVerification = false;
        this.postVerificationResult = event;
        if (event !== VerificationResult.CANCEL) {
            this.loadSessionAndRedirect();
        }
    }

    loadSessionAndRedirect(newRecordId?: string) {
        let recordId;
        if (newRecordId) {
            recordId = newRecordId;
        } else {
            recordId = this.selectedContact[this.redirectEntity === 'contact' ? 'ContactId' : 'AccountId'];
        }

        // force next calls to either OAM summary or subsystem summary to return most recent data
        if (this.linkSubsystem && this.linkSubsystemAccountReference) {
            this._cub_subsystemService.clearCachedSubsystemStatus(this.linkSubsystem, this.linkSubsystemAccountReference);
            this._cub_oneAccountService.clearCachedOneAccountSummary(newRecordId || this.selectedContact['AccountId']);
        }

        const done = this._loadingScreen.add('Loading session');
        let optionalSessionPromise: Promise<any> = Promise.resolve();

        if (!this._cub_sessionService.isActive) {
            optionalSessionPromise = optionalSessionPromise.then(() => {
                return newRecordId
                    ? this._cub_sessionService.createSession('account', newRecordId)
                    : this._cub_sessionService.createSession('contact', this.selectedContact['ContactId']);
            });
        } else if (!this._cub_sessionService.session.isRegistered) {
            const updatedSession = {
                id: this._cub_sessionService.session.sessionId,
                cub_isregistered: true,
                cub_isverified: this.postVerificationResult === VerificationResult.VERIFIED,
                cub_isprimarycontact: true // TODO: is this updated server-side? if not, figure out how to determine
            };
            if (newRecordId) {
                updatedSession['cub_customerid'] = { id: newRecordId };
            } else {
                updatedSession['cub_customerid'] = { id: this.selectedContact['AccountId'] };
                updatedSession['cub_contactid'] = { id: this.selectedContact['ContactId'] };
            }
            optionalSessionPromise = optionalSessionPromise.then(() => this._cub_sessionService.updateSession(updatedSession));
        }

        optionalSessionPromise
            .then(() => this._cub_dataService.openMsdForm(this.redirectEntity, recordId))
            .catch((error) => this._cub_dataService.onApplicationError(error))
            .then(() => done());
    }
}