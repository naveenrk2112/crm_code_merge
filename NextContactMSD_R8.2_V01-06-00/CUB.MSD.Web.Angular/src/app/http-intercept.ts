﻿import { Injectable, Injector } from '@angular/core';
import { HttpErrorResponse, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { Cub_DataService } from './services/cub_data.service';
import { WSErrorResponse, CubError } from './model';

/**
 * Applies headers to all outgoing HTTP requests to prevent IE from caching responses.
 */
@Injectable()
export class AntiCachingRequestInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        req = req.clone({
            setHeaders: {
                'Cache-Control': 'no-cache',
                'Pragma': 'no-cache'
            }
        });
        return next.handle(req);
    }
}

/**
 * Angular v4.4.4 HttpClient throws error on successful response with no body,
 * so we need this to correct it. NOTE: this must come before ResponseErrorInterceptor
 */
@Injectable()
export class EmptyResponseBodyErrorInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        return next.handle(req)
            .catch((err: HttpErrorResponse) => {
                if (err.status >= 200 && err.status < 300) {
                    const res = new HttpResponse({
                        body: null,
                        headers: err.headers,
                        status: err.status,
                        statusText: err.statusText,
                        url: err.url
                    });

                    return Observable.of(res);
                } else {
                    return Observable.throw(err);
                }
            });
    }
}

/**
 * Displays HTTP response errors to the user.
 */
@Injectable()
export class ResponseErrorInterceptor implements HttpInterceptor {
    constructor(private _injector: Injector) { }

    formatNisData(hdr: any): string {
        if (!hdr) {
            return null;
        }

        let value = [];
        if (hdr.result) {
            value.push(hdr.result);
        }
        if (hdr.fieldName) {
            value.push(hdr.fieldName)
        }
        if (hdr.errorKey) {
            value.push(hdr.errorKey);
        }
        if (hdr.errorMessage) {
            value.push(hdr.errorMessage);
        }

        return value.join(' - ');
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        return next.handle(req)
            .catch(err => {
                let cubError = new CubError(err);

                if (cubError.handled || !(err instanceof HttpErrorResponse)) {
                    return Observable.throw(cubError);
                }

                // We cannot reference Cub_DataService in the constructor as that
                // creates a cyclic dependency, so we obtain it here.
                const _cub_dataService = this._injector.get(Cub_DataService);

                if (!err.status) {
                    _cub_dataService.showError('The client failed to reach the web service', true);
                    cubError.handled = true;
                    return Observable.throw(cubError);
                }

                let wsError: WSErrorResponse = _cub_dataService.tryParse(err.error);

                let message;
                if (wsError && wsError.Origin) {
                    let nisHdr = this.formatNisData(JSON.parse(wsError.Data));
                    message = `${wsError.Origin} error: (Status: ${err.status})`;
                    if (nisHdr || wsError.Message) {
                        message += '<br/>' + (nisHdr || wsError.Message);
                    }
                    message += '<br/>Log Ref.#: ' + wsError.CorrelationId;
                } else {
                    message = `Unexpected HTTP error: (Status: ${err.status}) ${err.statusText}`;
                    if (err.message) {
                        message += '<br/>' + err.message;
                    }
                }

                _cub_dataService.showError(message);
                console.error(message);
                if (wsError && wsError.StackTrace) {
                    console.error('Stacktrace:', wsError.StackTrace);
                }

                cubError.handled = true;
                return Observable.throw(cubError);
            });
    }
}
