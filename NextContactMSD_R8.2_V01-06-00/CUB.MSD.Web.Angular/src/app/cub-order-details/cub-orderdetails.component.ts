﻿import { Component, Input, Output, OnInit, EventEmitter, NgZone, OnDestroy } from '@angular/core';
import { Cub_DataService } from '../services/cub_data.service';
import { CubOrderService } from '../services/cub-order.service';
import { CubCustomerService } from '../services/cub_customer.service';
import { Cub_NotificationService } from '../services/cub_notification.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { ActivatedRoute } from '@angular/router';
import * as Model from '../model';

import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'cub-order-details',
    templateUrl: './cub-orderdetails.component.html',
    providers: [CubOrderService, CubCustomerService, Cub_NotificationService]
})
export class Cub_OrderDetailsComponent implements OnInit {
    @Input() orderId: number;
    @Input() subsystemId: string;
    @Input() customerId: string;
    @Input() realTime: boolean;
    @Output() onClose = new EventEmitter<void>();

    showHistoricalWarning: boolean = false;
    orderDetails: any;
    primaryContact: any;
    orderNotifications: any;
    detailsLoaded: boolean = false;
    notificationDetais: any;
    showNotificationDetails: boolean = false;
    showResendNotifications: boolean = false;
    showGenerateReceipt: boolean = false;
    editNotificationEmail: boolean = false;
    editNotificationPhone: boolean = false;
    resendNotificatiochannelError: boolean = false;
    selectedNotifiation: any;
    resendNotificationEmailRequired: boolean = false;
    resendNotificationPhoneRequired: boolean = false;
    generatePaymentReceiptSendTo: string;

    phoneMask = {
        mask: [] as any,
        placeholderChar: '_' as string
    };

    notifiationSendEmail: Model.Cub_Textbox = {
        type: "Textbox",
        id: "email-to",
        isRequired: this.resendNotificationEmailRequired,
        hideRequiredIndicator: false,
        errorMessage: "Email is required",
        label: "Email To",
        isVerticalStackedLabel: false        
    };


    notifiationSendPhone: Model.Cub_Textbox = {
        type: "Textbox",
        id: "send-sms-push-to",
        isRequired: this.resendNotificationPhoneRequired,
        hideRequiredIndicator: false,
        errorMessage: "Phone is required",
        label: "Send SMS/Push To",
        isVerticalStackedLabel: false
    };
    

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_orderService: CubOrderService,
        private _cub_customerService: CubCustomerService,
        private _cub_notificationService: Cub_NotificationService,
        private _loadingScreen: AppLoadingScreenService,
        private _zone: NgZone,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {

        if (!this.orderId)
        {
            this._cub_dataService.onApplicationError('No Order Id passed to modal');
            console.error('orderId paramater is required');
            return;
        }
        if (!this.customerId && this.subsystemId) {
            this._cub_dataService.onApplicationError('Either Customer Id or Subsystem Id is required');
            console.error('Either Customer Id or Subsystem Id is required');
            return;
            
        }
       
        const done = this._loadingScreen.add('Loading Order detail');
        this._cub_orderService.getCustomerOrderDetails(this.customerId, String(this.orderId), this.realTime)
            .then((data: any) => {
                this.orderDetails = JSON.parse(data.Body);
            })
            .catch(err => {
                this._cub_dataService.prependCurrentError('Error getting order details');
                this.onCloseClicked();
            })
            .then(() => this.detailsLoaded = true)
            .then(() => done());


        this.getCustomerPrimaryContact();
        this.getOrderNotifications();
        this.showHistoricalWarning = (String(this.realTime) === 'true');
       
    }    
    
    onCloseClicked() {
        this.onClose.emit();
    }
   
    getCustomerPrimaryContact() {       
        if (this.customerId) {
            this._cub_customerService.getPrimaryContact(this.customerId)
                .then((data: any) => {
                    this.primaryContact = data;
                })
        }     
    }

    getOrderNotifications() {
        this._cub_orderService.getOrderNotifications(String(this.orderId), 'All')
            .then((data: any) => {
                this.orderNotifications = JSON.parse(data.Body);
            })
            .catch(err => {
                this._cub_dataService.prependCurrentError('Error getting order notifications');               
            })
    }

    customerClicked(customerId: string) {
        parent.Xrm.Utility.openEntityForm('account', customerId);
        this.onClose.emit();
    }

    orderStatusHistoryClicked() {
        alert('Not Implemented. Waiting for back office API');
    }

    showResendNotificationsClicked(notification: any) {
        this.selectedNotifiation = notification;
        this.showResendNotifications = true;
    }

    closeResendNotificationsClicked() {
        this.showResendNotifications = false;
    }
    
    showGeneratePaymentReceiptClicked() {
        if (this.primaryContact) {
            this.generatePaymentReceiptSendTo = this.primaryContact.Email;
        }
        this.showGenerateReceipt = true;
    }

    closeGeneratePaymentReceiptClicked() {
        this.showGenerateReceipt = false;
    }

    sendGeneratePaymentReceiptClicked() {
        this._cub_orderService.generatePaymentReceipt(String(this.orderId), this.generatePaymentReceiptSendTo)
            .then(() => {
                this._cub_dataService.busyScreen.icon = 'check';
                this._cub_dataService.busyScreen.busyText = 'Generating Payment Receipt';
                ++this._cub_dataService.busyScreen.busyCount;
                setTimeout(() => {
                    --this._cub_dataService.busyScreen.busyCount;
                }, 500);
            })
            .catch(err => this._cub_dataService.prependCurrentError('Generating Payment Receipt error'))
            .then(() => this.showGenerateReceipt = false);
    }

    resendNotificationsClicked() {          
        this._cub_notificationService.resendNotifiation(this.selectedNotifiation, this.notifiationSendEmail.value, this.notifiationSendPhone.value,  'EMAIL')
            .then(() => {                   
                this._cub_dataService.busyScreen.icon = 'check';
                this._cub_dataService.busyScreen.busyText = 'Resent';
                ++this._cub_dataService.busyScreen.busyCount;
                setTimeout(() => {
                    --this._cub_dataService.busyScreen.busyCount;
                }, 500);
            })
            .catch(err => this._cub_dataService.prependCurrentError('Resending error'))
            .then(() =>this.showResendNotifications = false);
    }


    resendCustomerNotificationsClicked() {
        this._cub_notificationService.resendCustomerNotification(this.selectedNotifiation)
            .then(() => {
                this._cub_dataService.busyScreen.icon = 'check';
                this._cub_dataService.busyScreen.busyText = 'Resent';
                ++this._cub_dataService.busyScreen.busyCount;
                setTimeout(() => {
                    --this._cub_dataService.busyScreen.busyCount;
                }, 500);
            })
            .catch(err => this._cub_dataService.prependCurrentError('Resending error'))
            .then(() => {
                this.getOrderNotifications();
                this.showResendNotifications = false
            });
        
    }

    viewNotificationsClicked(notificationId: string) {
        this._cub_notificationService.getNotificationDetails(notificationId)
            .then((data: any) => {
                this.notificationDetais = JSON.parse(data.Body);
                this.showNotificationDetails = true;
            })
            .catch(err => {
                this._cub_dataService.prependCurrentError('Error getting notification details');
            })
       
    }

    actionMenuClicked(notification: any) {

    }

    closeNotificationsClicked() {
        this.showNotificationDetails = false;
    }

    /**
   * This function determines the text mask for the order Id input,
   * which should only allow numbers.
   * @param value the input field's raw value
   * @returns an array of (value.length + 1) decimal regexs
   */
    numMask(value: string) {
        const numPlace = /\d/;
        let result = [numPlace];

        if (!value) {
            return result;
        }

        const max = 100;
        const len = value.length;
        for (let i = 0; i < len && i < max; i++) {
            result.push(numPlace);
        }
        return result;
    }

}

