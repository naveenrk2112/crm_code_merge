﻿import { DebugElement } from '@angular/core';
import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Cub_ResolveBalanceModule } from './cub_resolve_balance.module';
import { Cub_ResolveBalanceComponent } from './cub-resolve-balance.component';

import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';
import { Cub_CustomerOrderServiceStub } from '../testutilities/cub_customerorder.service.stub';
import { Cub_SubsystemServiceStub } from '../testutilities/cub_subsystem.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';

describe('Cub_ResolveBalanceComponent', () => {
    let fixture: ComponentFixture<Cub_ResolveBalanceComponent>,
        comp: Cub_ResolveBalanceComponent,
        root: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [Cub_ResolveBalanceModule],
            providers: [
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
            ]
        }).overrideComponent(Cub_ResolveBalanceComponent, {
            set: {
                providers: [
                    { provide: Cub_CustomerOrderService, useClass: Cub_CustomerOrderServiceStub },
                    { provide: Cub_SubsystemService, useClass: Cub_SubsystemServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_ResolveBalanceComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
    });

    it('should compile pls', () => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    });
});