﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Cub_ResolveBalanceComponent } from './cub-resolve-balance.component';
import { MoneyPipeModule } from '../pipes/money.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: Cub_ResolveBalanceComponent }
        ]),
        MoneyPipeModule
    ],
    declarations: [Cub_ResolveBalanceComponent],
    exports: [Cub_ResolveBalanceComponent]
})
export class Cub_ResolveBalanceModule { }