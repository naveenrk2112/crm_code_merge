﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { ResolveBalanceArgs, WSOrderError, WSDebtCollectionInfo, WSDebtCollectResponse, CubError } from '../model';

import * as countdown from 'countdown';
import * as moment from 'moment';

export enum ResolveBalanceModalState {
    Available,
    Completed,
    AvailableWait,
    UnknownError,
    UnableToResolve
}

@Component({
    selector: 'cub-resolve-balance',
    templateUrl: './cub-resolve-balance.component.html',
    providers: [
        Cub_CustomerOrderService,
        Cub_SubsystemService
    ]
})
export class Cub_ResolveBalanceComponent implements OnInit {
    // Necessary so this enum can be used within the HTML template
    readonly _ResolveBalanceModalState = ResolveBalanceModalState;

    state: ResolveBalanceModalState;
    requestBody: ResolveBalanceArgs;
    debtCollectionInfo: WSDebtCollectionInfo;
    updatedDebtCollectionInfo: WSDebtCollectionInfo;
    email: string = '';
    nextAttemptRemainingTime: string;
    responseBody: WSDebtCollectResponse;

    constructor(
        private _cub_customerOrderService: Cub_CustomerOrderService,
        private _cub_subsystemService: Cub_SubsystemService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
        const queryParams = this._route.snapshot.queryParamMap;
        if (!queryParams.has('debtCollectionInfo')
            || !queryParams.has('requestBody')
        ) {
            console.error('No data passed to Resolve Balance modal');
            return;
        }

        this.requestBody = JSON.parse(queryParams.get('requestBody'));
        this.debtCollectionInfo = JSON.parse(queryParams.get('debtCollectionInfo'));

        // Set initial state
        if (this.debtCollectionInfo.action === 'Available' && this.debtCollectionInfo.amountDue > 0) {
            this.state = ResolveBalanceModalState.Available;
        } else if (this.debtCollectionInfo.action === 'AvailableWait') {
            const attempts = this.debtCollectionInfo.remainingAttemptsForDay;
            const seconds = this.debtCollectionInfo.attemptInSeconds;
            this.state = attempts === 0 || (attempts > 0 && seconds > 0)
                ? ResolveBalanceModalState.AvailableWait
                : ResolveBalanceModalState.UnknownError;
            this.computeRemainingTime(this.debtCollectionInfo);
        } else {
            this.state = ResolveBalanceModalState.UnknownError;
            console.error('Debt collection action should be Available or AvailableWait, instead got:', this.debtCollectionInfo.action);
        }
    }

    /**
     * Uses npm package countdown to create a readable countdown string
     * @param info
     */
    computeRemainingTime(info: WSDebtCollectionInfo) {
        if (info.attemptInSeconds) {
            const end = moment().add(info.attemptInSeconds, 'seconds').toDate();
            const units = countdown['DAYS'] | countdown['HOURS'] | countdown['MINUTES'];
            this.nextAttemptRemainingTime = countdown(null, end, units).toString();
        }
    }

    resolveBalanceClicked() {
        const done = this._loadingScreen.add('Submitting');
        if (this.email) {
            this.requestBody.unregisteredEmail = this.email;
        }

        this._cub_customerOrderService.resolveBalance(this.requestBody)
            .then(data => {
                if (data != null) {
                    this.responseBody = data;
                }
            })
            .then(() => this._cub_subsystemService.refreshSubsystemStatus(this.requestBody.subsystem, this.requestBody.subsystemAccountReference))
            .then((data: any) => {
                if (data) {
                    this.updatedDebtCollectionInfo = data.debtCollectionInfo
                    this.computeRemainingTime(this.updatedDebtCollectionInfo);
                }
            })
            .then(() => this.processResponse())
            .catch(err => {
                this.state = ResolveBalanceModalState.UnknownError;
                if (err instanceof CubError && err.handled) {
                    return;
                }
                console.error(err);
            })
            .then(() => done());
    }

    cancelClicked() {
        let shouldRefresh: boolean;
        switch (this.state) {
            case ResolveBalanceModalState.Completed:
            case ResolveBalanceModalState.UnableToResolve:
                shouldRefresh = true;
                break;
            case ResolveBalanceModalState.Available:
            case ResolveBalanceModalState.AvailableWait:
            case ResolveBalanceModalState.UnknownError:
            default:
                shouldRefresh = false;
        }

        window['Mscrm'].Utilities.setReturnValue({
            refresh: shouldRefresh
        });
        window['closeWindow']();
    }

    viewDetailsClicked() {
        window['Mscrm'].Utilities.setReturnValue({
            orderId: this.responseBody.orderId,
            refresh: true
        });
        window['closeWindow']();
    }

    processResponse() {
        if (this.responseBody == null || this.updatedDebtCollectionInfo == null) {
            this.state = ResolveBalanceModalState.UnknownError;
            console.error('Something went wrong while processing Resolve Balance Order response. Order response:', this.responseBody, '; refreshed Debt Collection Info:', this.updatedDebtCollectionInfo);
            return;
        }

        if (this.responseBody.responseCode === 'Completed') {
            this.state = ResolveBalanceModalState.Completed;
        } else {
            const attempts = this.updatedDebtCollectionInfo.remainingAttemptsForDay;
            const seconds = this.updatedDebtCollectionInfo.attemptInSeconds;
            this.state = attempts === 0 || (attempts > 0 && seconds > 0)
                ? ResolveBalanceModalState.UnableToResolve
                : ResolveBalanceModalState.UnknownError;
        }
    }
}