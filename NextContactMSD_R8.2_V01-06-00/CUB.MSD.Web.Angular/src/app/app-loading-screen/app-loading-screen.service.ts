﻿import { Injectable } from '@angular/core';
import { findIndex } from 'lodash';

@Injectable()
export class AppLoadingScreenService {
    private _messages: string[] = [];
    get messages() {
        return this._messages;
    }
    public default: string = 'Loading';

    /**
     * Add a new loading message and return a function that will remove the message when invoked.
     * @param message Optional loading message to display; defaults to AppLoadingScreenService.default.
     * @returns a function that removes the message when invoked.
     */
    add(message: string = this.default) {
        this._messages.push(message);
        let alreadyRemoved = false;
        return () => {
            if (!alreadyRemoved) {
                this.remove(message);
                alreadyRemoved = true;
            }
        };
    }

    /**
     * Find and remove a loading screen message.
     * @param message
     */
    private remove(message: string) {
        const index = findIndex(this._messages, m => m === message);
        if (index !== -1) {
            this._messages.splice(index, 1);
        }
    }

    /**
     * Remove all messages from queue.
     */
    clear() {
        let len = this._messages.length;
        while (len-- > 0) {
            this._messages.pop();
        }
    }
}