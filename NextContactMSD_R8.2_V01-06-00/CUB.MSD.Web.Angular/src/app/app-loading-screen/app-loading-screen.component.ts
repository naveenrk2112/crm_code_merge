﻿import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { AppLoadingScreenService } from './app-loading-screen.service';

@Component({
    selector: 'app-loading-screen',
    templateUrl: './app-loading-screen.component.html'
})
export class AppLoadingScreenComponent implements OnInit {
    constructor(
        private _loadingScreen: AppLoadingScreenService,
        private _router: Router
    ) { }

    get show() {
        return this._loadingScreen.messages.length > 0;
    }

    get currentMessage() {
        return this._loadingScreen.messages[0];
    }

    private _width = window.innerWidth;
    private _height = window.innerHeight;

    private useSmall: boolean = true;

    ngOnInit() {
        this._router.events
            .first(e => e instanceof NavigationEnd)
            .subscribe(() => this.resize());
    }

    resize() {
        if (!document.querySelector('app > div > router-outlet + *')) {
            return;
        }
        let screenIsHybrid = !!document.querySelector('app > div > router-outlet + * > .hybrid');
        this.useSmall = screenIsHybrid;
    }
}