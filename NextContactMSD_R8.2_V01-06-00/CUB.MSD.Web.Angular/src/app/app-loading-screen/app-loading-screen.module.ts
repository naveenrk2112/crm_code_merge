﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppLoadingScreenComponent } from './app-loading-screen.component';
import { AppLoadingScreenService } from './app-loading-screen.service';

@NgModule({
    imports: [BrowserModule],
    declarations: [AppLoadingScreenComponent],
    exports: [AppLoadingScreenComponent],
    providers: [AppLoadingScreenService]
})
export class AppLoadingScreenModule { }