﻿import { ComponentFixture, TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Cub_CustomerVerificationComponent } from './cub_customerverification.component';
import { Cub_SecurityOptionComponent } from '../controls/cub-securityoption/cub_securityoption.component';
import { Cub_AlertComponent } from '../controls/cub-alert/cub_alert.component';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_CustomerVerificationService } from '../services/cub_customerverification.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_CustomerVerificationServiceStub } from '../testutilities/cub_customerverification.service.stub';

import { filter, find } from 'lodash';

describe('Cub_CustomerVerificationComponent', () => {
    pending();
    let fixture: ComponentFixture<Cub_CustomerVerificationComponent>;
    let comp: Cub_CustomerVerificationComponent;
    let dataService: Cub_DataService;
    let service: Cub_CustomerVerificationService;
    let root: DebugElement;
    let modal;

    //beforeEach(async(() => {
    //    TestBed.configureTestingModule({
    //        imports: [
    //            FormsModule,
    //        ],
    //        declarations: [
    //            Cub_CustomerVerificationComponent,
    //            Cub_AlertComponent,
    //            Cub_SecurityOptionComponent
    //        ],
    //        providers: [
    //            { provide: Cub_DataService, useClass: Cub_DataServiceStub }
    //        ]
    //    }).overrideComponent(Cub_CustomerVerificationComponent, {
    //        set: {
    //            providers: [
    //                { provide: Cub_CustomerVerificationService, useClass: Cub_CustomerVerificationServiceStub }
    //            ]
    //        }
    //    }).compileComponents();
    //}));

    //beforeEach(() => {
    //    fixture = TestBed.createComponent(Cub_CustomerVerificationComponent);
    //    comp = fixture.componentInstance;
    //    root = fixture.debugElement;
    //    dataService = TestBed.get(Cub_DataService);
    //    service = root.injector.get(Cub_CustomerVerificationService);
    //    modal = root.nativeElement.querySelector('div.modal');
    //    comp.contact = {
    //        FirstName: null, LastName: null, Phone1: null, Phone1Type: null, Email: null
    //    };
    //});

    //afterAll(() => {
    //    modal.outerHTML = '';
    //});

    //it('should load verified cache and set verificationScope on init', () => {
    //    spyOn(dataService, 'loadCache');
    //    fixture.detectChanges();
    //    expect(dataService.loadCache).toHaveBeenCalled();
    //    expect(dataService.verificationScope).toEqual(comp);
    //});

    //it('should unload verified cache on destroy', () => {
    //    fixture.detectChanges();
    //    spyOn(dataService, 'unloadCache');
    //    fixture.destroy();
    //    expect(dataService.unloadCache).toHaveBeenCalled();
    //});

    //it('should be hidden by default', () => {
    //    fixture.detectChanges();
    //    expect(modal.className).not.toMatch('visible');
    //});

    //it('should hide if Cancel is clicked', () => {
    //    let cancelEl = root.nativeElement.querySelector('button.button-cancel');
    //    comp._cub_dataService.showCustomerVerification = true;
    //    fixture.detectChanges();
    //    expect(modal.className).toMatch('visible');
    //    cancelEl.click();
    //    fixture.detectChanges();
    //    expect(modal.className).not.toMatch('visible');
    //});

    //it('should call Cub_DataService.onVerified() if Not Verified is clicked', () => {
    //    spyOn(comp._cub_dataService, 'onVerified');
    //    comp.contact.ContactId = '00000000-0000-0000-0000-000000000001';
    //    modal.querySelector('button.button-primary-text').click();
    //    expect(comp._cub_dataService.onVerified).toHaveBeenCalledWith(false);
    //});

    //it('should display a placeholder text if there are no security questions', () => {
    //    comp._cub_dataService.showCustomerVerification = true;
    //    expect(comp.noSQs).toBe(false);
    //    comp.createSecurityQuestions([]);
    //    expect(comp.noSQs).toBe(true);
    //    fixture.detectChanges();
    //    expect(modal.querySelectorAll('ul.either-or li:first-child ul.checklist li.answer').length).toBe(1);
    //});

    //it('should call Cub_DataService.onVerified() if questions verified reaches 1', async(() => {
    //    spyOn(comp._cub_dataService, 'onVerified');
    //    comp.initialize();
    //    fixture.whenStable().then(() => {
    //        fixture.detectChanges();
    //        expect(comp.securityQuestions.length).toBe(2);
    //        expect(comp.securityQuestions[0].verified).toBe(false);
    //        expect(comp.securityQuestions[1].verified).toBe(false);
    //        modal.querySelector('#security-question-0-verified').click();
    //        expect(Array.from(comp.questionsVerified)).toContain(comp.securityQuestions[0].question);
    //        expect(comp._cub_dataService.onVerified).toHaveBeenCalledWith(true, comp.questionsVerified);
    //    });
    //}));

    //it('should call Cub_DataService.onVerified() if details verified reaches 3', async(() => {
    //    spyOn(comp._cub_dataService, 'onVerified');
    //    comp.initialize();
    //    fixture.whenStable().then(() => {
    //        fixture.detectChanges();
    //        expect(comp.contactDetails.length).toBe(9);
    //        expect(filter(comp.contactDetails, cd => cd.verified).length).toBe(0);

    //        modal.querySelector('#name-verified').click();
    //        expect(comp.detailsVerified.size).toBe(1);
    //        expect(Array.from(comp.detailsVerified)).toContain(find(comp.contactDetails, cd => cd.name.match('name')).question);
    //        expect(comp._cub_dataService.onVerified).not.toHaveBeenCalled();

    //        modal.querySelector('#email-verified').click();
    //        expect(comp.detailsVerified.size).toBe(2);
    //        expect(Array.from(comp.detailsVerified)).toContain(find(comp.contactDetails, cd => cd.name.match('email')).question);
    //        expect(comp._cub_dataService.onVerified).not.toHaveBeenCalled();

    //        modal.querySelector('#name-verified').click();
    //        expect(comp.detailsVerified.size).toBe(1);
    //        expect(Array.from(comp.detailsVerified)).not.toContain(find(comp.contactDetails, cd => cd.name.match('name')).question);
    //        expect(comp._cub_dataService.onVerified).not.toHaveBeenCalled();

    //        modal.querySelector('#phone-1-verified').click();
    //        expect(comp.detailsVerified.size).toBe(2);
    //        expect(Array.from(comp.detailsVerified)).toContain(find(comp.contactDetails, cd => cd.name.match('phone-1')).question);
    //        expect(comp._cub_dataService.onVerified).not.toHaveBeenCalled();

    //        modal.querySelector('#pin-verified').click();
    //        expect(comp.detailsVerified.size).toBe(3);
    //        expect(Array.from(comp.detailsVerified)).toContain(find(comp.contactDetails, cd => cd.name.match('pin')).question);
    //        expect(comp._cub_dataService.onVerified).toHaveBeenCalledWith(true, comp.detailsVerified);
    //    });
    //}));

    //it('should only display up to 3 security questions', async(() => {
    //    spyOn(service, 'getSecurityQuestions').and.returnValue(Promise.resolve([
    //        {
    //            securityQuestion: 'How many suburbs surround Chicago?',
    //            securityAnswer: 'A lot'
    //        }, {
    //            securityQuestion: 'How many neighborhoods comprise Chicago?',
    //            securityAnswer: 'A lot'
    //        }, {
    //            securityQuestion: 'How many bridges are there in Chicago?',
    //            securityAnswer: 'A lot'
    //        }, {
    //            securityQuestion: 'Will this question even show up?',
    //            securityAnswer: 'Nah'
    //        }
    //    ]));
    //    comp.initialize();
    //    fixture.whenStable().then(() => {
    //        fixture.detectChanges();
    //        expect(service.getSecurityQuestions).toHaveBeenCalled();
    //        expect(comp.securityQuestions.length).toBe(3);
    //        expect(modal.querySelectorAll('ul.either-or li:first-child ul.checklist li.answer').length).toBe(3);
    //    });
    //}));

    //it('#formatPhoneNumbers should set corresponding contact detail answers if present', () => {
    //    comp.contact.Phone1 = '2125551234';
    //    comp.contact.Phone2 = '3175559993';
    //    comp.contact.Phone3 = null;
    //    comp.formatPhoneNumbers();
    //    expect(comp.contact.Phone1).toMatch(/\([1-9]\d\d\) \d\d\d-\d\d\d\d/, 'Phone 1 test failed');
    //    expect(comp.contact.Phone2).toMatch(/\([1-9]\d\d\) \d\d\d-\d\d\d\d/, 'Phone 2 test failed');
    //    expect(comp.contact.Phone3).toBeNull('Phone 3 test failed');
    //});

    //it('#formatAddress should return array of address lines', () => {
    //    let ADDRESS = {
    //        addressId: '00000000-0000-0000-0000-000000000001',
    //        city: 'Springfield',
    //        state: 'IL',
    //        street1: '742 Evergreen Terrace',
    //        street2: null,
    //        street3: null,
    //        zipCode: '12345'
    //    };
    //    let result = comp.formatAddress(ADDRESS);
    //    expect(result).toEqual(['742 Evergreen Terrace', 'Springfield, IL 12345']);

    //    ADDRESS.street2 = 'Unit 1';
    //    result = comp.formatAddress(ADDRESS);
    //    expect(result).toEqual(['742 Evergreen Terrace', 'Unit 1', 'Springfield, IL 12345']);

    //    ADDRESS.street3 = 'No clue what goes here';
    //    result = comp.formatAddress(ADDRESS);
    //    expect(result).toEqual(['742 Evergreen Terrace', 'Unit 1', 'No clue what goes here', 'Springfield, IL 12345']);
    //});

    //describe('#initialize', () => {
    //    it('should set contact details from caller component', async(() => {
    //        spyOn(comp, 'formatPhoneNumbers'); // don't call this function
    //        comp.initialize();
    //        fixture.whenStable().then(() => {
    //            fixture.detectChanges();
    //            expect(comp.contact).toEqual(comp._cub_dataService.selectedContactCustomerToVerify);
    //            expect(modal.querySelectorAll('ul.either-or li:last-child ul.checklist li.answer').length).toBe(8);
    //        });
    //    }));

    //    it('should create security questions', async(() => {
    //        comp.initialize();
    //        fixture.whenStable().then(() => {
    //            fixture.detectChanges();
    //            expect(comp.securityQuestions.length).toBeGreaterThan(0);
    //            expect(comp.noSQs).toBe(false);
    //            expect(modal.querySelectorAll('ul.either-or li:first-child ul.checklist li.answer').length).toBe(2);
    //        });
    //    }));

    //    it('should add birth date detail', async(() => {
    //        spyOn(comp, 'addBirthDateDetail').and.callThrough();
    //        comp.initialize();
    //        fixture.whenStable().then(() => {
    //            fixture.detectChanges();
    //            expect(comp.addBirthDateDetail).toHaveBeenCalled();
    //            expect(modal.querySelector('#date-of-birth-verified')).toBeTruthy();
    //        });
    //    }));

    //    it('should add username and pin', async(() => {
    //        spyOn(comp, 'addUsernameAndPinDetails').and.callThrough();
    //        comp.initialize();
    //        fixture.whenStable().then(() => {
    //            fixture.detectChanges();
    //            expect(comp.addUsernameAndPinDetails).toHaveBeenCalled();
    //            expect(modal.querySelector('#pin-verified')).toBeTruthy();
    //        });
    //    }));

    //    it('should add address', async(() => {
    //        spyOn(comp, 'addAddressDetail').and.callThrough();
    //        comp.initialize();
    //        fixture.whenStable().then(() => {
    //            fixture.detectChanges();
    //            expect(comp.addAddressDetail).toHaveBeenCalled();
    //            expect(modal.querySelector('#address-verified')).toBeTruthy();
    //        });
    //    }));

    //    it('should reset security questions and contact details from previous contact', fakeAsync(() => {
    //        spyOn(comp._cub_dataService, 'onVerified');
    //        comp.initialize();
    //        tick();
    //        fixture.detectChanges();
    //        expect(comp.contactDetails.length).toBeGreaterThan(0);
    //        expect(comp.securityQuestions.length).toBeGreaterThan(0);

    //        let prevDetails = comp.contactDetails;
    //        let prevQuestions = comp.securityQuestions;
    //        comp._cub_dataService.selectedContactCustomerToVerify = {
    //            ContactId: '00000000-0000-0000-0000-000000000002',
    //            FullName: 'Mickey Mouse',
    //            Phone1: '5555551234',
    //            Phone1Type: 'Home',
    //            Phone2: null,
    //            Phone2Type: null,
    //            Phone3: null,
    //            Phone3Type: null,
    //            Email: 'mickey@disney.com',
    //        };
    //        spyOn(service, 'getSecurityQuestions').and.returnValue(Promise.resolve([
    //            {
    //                securityQuestion: 'What is your favorite Disney Pixar movie?',
    //                securityAnswer: 'Ratatouille'
    //            }
    //        ]));

    //        comp.initialize();
    //        tick();
    //        fixture.detectChanges();
    //        expect(comp.contactDetails.length).toBe(9);
    //        expect(comp.securityQuestions.length).toBe(1);
    //        expect(modal.querySelectorAll('ul.either-or li:first-child ul.checklist li.answer').length).toBe(1);
    //        expect(modal.querySelectorAll('ul.either-or li:last-child ul.checklist li.answer').length).toBe(7);
    //        expect(comp.contactDetails).not.toEqual(prevDetails);
    //        expect(comp.securityQuestions).not.toEqual(prevQuestions);
    //        expect(comp._cub_dataService.onVerified).not.toHaveBeenCalled();
    //    }));
    //});
});