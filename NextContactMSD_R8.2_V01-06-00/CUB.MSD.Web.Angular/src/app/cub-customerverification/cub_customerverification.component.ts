﻿// Component associated with customer verification modal dialog and logic

import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Cub_SessionService } from '../services/cub-session.service';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_CustomerVerificationService } from '../services/cub_customerverification.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { PhonePipe } from '../pipes/phone.pipe';
import { VerificationResult, Cub_Globals } from '../model';

import * as _ from 'lodash';
import { conformToMask } from 'angular2-text-mask';

interface SecurityOption {
    name: string,
    question: string,
    answer: string | string[],
    verified: boolean
}

interface Contact {
    FirstName: string,
    LastName: string,
    FullName?: string,
    ContactId?: string,
    BirthDate?: string,
    Phone1Type: string,
    Phone2Type?: string,
    Phone3Type?: string,
    Phone1: string,
    Phone2?: string,
    Phone3?: string,
    Email: string
}

interface Address {
    addressId: string,
    city: string,
    state: string,
    street1: string,
    street2?: string,
    street3?: string,
    zipCode: string
}

interface UsernameAndPin {
    username: string,
    pin: string
}


@Component({
    selector: 'cub-customerverification',
    templateUrl: './cub_customerverification.component.html',
    providers: [Cub_CustomerVerificationService]
})
export class Cub_CustomerVerificationComponent implements OnInit, OnDestroy {
    @Input() contact: Contact;
    @Output() onResolve = new EventEmitter<VerificationResult>();
    questionsVerified = new Set<string>(); // if verified, passed to Cub_DataService.onVerifiedBySecurityQuestion()
    detailsVerified = new Set<string>(); // if verified, passed to Cub_DataService.onVerifiedByContactInfo()
    alerts: string[] = [];
    noSQs: boolean = false; // if true, displays placeholder text under Security Questions
    securityQuestions: SecurityOption[] = [];
    contactDetails: SecurityOption[] = [];
    awaitingVerification: boolean = true;
    initComplete: boolean = false;
    verifiedDisplayTime: number = 500;
    maxSecurityQuestions: number = 3;
    contactVerificationDetails: {
        [key: string]: string | number
    };
    numDetailsToVerify: number = 3;
    numQuestionsToVerify: number = 1;
    isUnsupportedModal: boolean = false;
    phonePipe: PhonePipe;

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_customerVerificationService: Cub_CustomerVerificationService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute,
        private _cub_sessionService: Cub_SessionService
    ) {
        this.phonePipe = new PhonePipe();
    }

    /**
     * Initializes the customer verification modal window.
     */
    ngOnInit() {
        const queryParams = this._route.snapshot.queryParamMap;
        if (queryParams.has('contact')) {
            this.contact = JSON.parse(queryParams.get('contact'));
            this.isUnsupportedModal = true;
        }
        this._cub_customerVerificationService.contactId = this.contact.ContactId;

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'] && !!globals['ContactVerificationDetails'])
            .subscribe(globals => this.loadConstants(globals));

        let done = this._loadingScreen.add();
        this._cub_customerVerificationService.loadVerifiedContacts()
            .then(() => {
                if (this._cub_customerVerificationService.isVerified()) {
                    this.afterVerified();
                    return;
                }

                done();
                done = this._loadingScreen.add('Retrieving security questions');
                return Promise.all([
                    this._cub_customerVerificationService.getBirthDate(),
                    this._cub_customerVerificationService.getSecurityQuestions(),
                    this._cub_customerVerificationService.getAddress(),
                    this._cub_customerVerificationService.getUsernameAndPin()
                ]).then(([birthDate, securityQuestions, address, usernameAndPin]) => {
                    this.formatPhoneNumbers();
                    this.createContactSecurityDetails();
                    this.addBirthDateDetail(birthDate);
                    this.createSecurityQuestions(securityQuestions);
                    this.addAddressDetail(address);
                    this.addUsernameAndPinDetails(usernameAndPin);
                });
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    ngOnDestroy() {
        this._cub_customerVerificationService.unloadCache();
    }

    loadConstants(globals: Cub_Globals) {
        this.contactVerificationDetails = globals.ContactVerificationDetails;
        this.numDetailsToVerify = globals['CUB.MSD.Web.Angular'].NumDetailsToVerify as number;
        this.numQuestionsToVerify = globals['CUB.MSD.Web.Angular'].NumQuestionsToVerify as number
        this.maxSecurityQuestions = globals['CUB.MSD.Web.Angular'].MaxSecurityQuestions as number;
        this.verifiedDisplayTime = globals['CUB.MSD.Web.Angular'].VerifiedDisplayTimeInMillis as number;
    }

    // event handler for security questions
    questionChanged(data: any) {
        if (!data || !data.length || data.length !== 2) { return; }
        let newValue = data[0];
        var securityQuestion = data[1];
        securityQuestion.verified = newValue;
        this.updateQuestionsVerified();
    }

    // event handler for contact details
    detailChanged(data: any) {
        if (!data || !data.length || data.length !== 2) { return; }
        let newValue = data[0];
        var securityQuestion = data[1];
        securityQuestion.verified = newValue;
        this.updateDetailsVerified();
    }

    // Checks if the contact is verified by security questions
    updateQuestionsVerified() {
        this.securityQuestions.forEach(sq => sq.verified
            ? this.questionsVerified.add(sq.question)
            : this.questionsVerified.delete(sq.question)
        );
        if (this.questionsVerified.size >= this.numQuestionsToVerify) {
            this.onCustomerVerifiedBySecurityQuestion(this.questionsVerified);
        }
    }

    // Checks if the contact is verified by contact details
    updateDetailsVerified() {
        this.contactDetails.forEach(cd => cd.verified
            ? this.detailsVerified.add(cd.question)
            : this.detailsVerified.delete(cd.question)
        );
        if (this.detailsVerified.size >= this.numDetailsToVerify) {
            this.onCustomerVerifiedByContactInfo(this.detailsVerified);
        }
    }

    createContactSecurityDetails() {
        // sort by key and map to SecurityOptions
        this.contactDetails = _.chain(this.contactVerificationDetails)
            .toPairs()
            .sortBy(0)
            .fromPairs()
            .map((cd: string) => {
                return {
                    name: `${cd.toLowerCase().replace(/ /g, '-')}-verified`,
                    question: cd,
                    answer: null,
                    verified: false
                };
            })
            .value();

        // these values can be populated immediately;
        // all others are populated asynchronously
        this.contactDetails.forEach(cd => {
            if (cd.question === 'Name') {
                cd.answer = this.contact.FullName;
            }
            else if (cd.question === 'Phone 1') {
                cd.question = this.contact.Phone1Type;
                cd.answer = this.contact.Phone1;
            }
            else if (cd.question === 'Phone 2') {
                cd.question = this.contact.Phone2Type;
                cd.answer = this.contact.Phone2;
            }
            else if (cd.question === 'Phone 3') {
                cd.question = this.contact.Phone3Type;
                cd.answer = this.contact.Phone3;
            }
            else if (cd.question === 'Email') {
                cd.answer = this.contact.Email;
            }
        });
    }

    // WEB SERVICE CALLBACKS

    createSecurityQuestions(data: any[]) {
        if (!!data && data.length > 0) {
            this.noSQs = false;
            this.securityQuestions = _.chain(data)
                .take(this.maxSecurityQuestions)
                .map((qa, i) => ({
                    name: `security-question-${i}-verified`,
                    question: qa['securityQuestion'],
                    answer: qa['securityAnswer'],
                    verified: false
                }))
                .value();
        } else {
            this.noSQs = true;
        }
    }

    addAddressDetail(data: Address) {
        if (!!data) {
            let addressDetail = _.find(this.contactDetails, d => d.question === 'Address');
            if (!addressDetail) {
                console.error('Could not find address detail');
            } else {
                addressDetail.answer = this.formatAddress(data);
            }
        }
    }

    addBirthDateDetail(data: string) {
        if (!!data) {
            let birthDateDetail = _.find(this.contactDetails, d => d.question === 'Date of Birth');
            if (!birthDateDetail) {
                console.error('Could not find date of birth detail');
            } else {
                birthDateDetail.answer = data;
            }
        }
    }

    addUsernameAndPinDetails(data: UsernameAndPin) {
        if (!!data) {
            let pinDetail = _.find(this.contactDetails, d => d.question === 'PIN');
            if (!pinDetail) {
                console.error('Could not find PIN detail');
            } else {
                pinDetail.answer = data.pin;
            }

            // don't set username if it matches email
            let emailDetail = _.find(this.contactDetails, d => d.question === 'Email');
            if (!!emailDetail && emailDetail.answer === data.username) {
                return;
            }

            let usernameDetail = _.find(this.contactDetails, d => d.question === 'Username');
            if (!usernameDetail) {
                console.error('Could not find username detail');
            } else {
                usernameDetail.answer = data.username;
            }
        }
    }

    // END WEB SERVICE CALLBACKS

    formatAddress(a: Address): string[] {
        let result = [];
        result.push(a.street1);
        if (!!a.street2) {
            result.push(a.street2);
        }
        if (!!a.street3) {
            result.push(a.street3);
        }
        result.push(`${a.city}, ${a.state} ${a.zipCode}`);
        return result;
    }

    formatPhoneNumbers() {
        if (this.contact.Phone1) {
            this.contact.Phone1 = this.phonePipe.transform(this.contact.Phone1);
        }
        if (this.contact.Phone2) {
            this.contact.Phone2 = this.phonePipe.transform(this.contact.Phone2);
        }
        if (this.contact.Phone3) {
            this.contact.Phone3 = this.phonePipe.transform(this.contact.Phone3);
        }
    }

    onNotVerified() {
        const done = this._loadingScreen.add('Creating activity');
        this._cub_customerVerificationService.onNotVerified()
            .then(() => {
                if (this.isUnsupportedModal) {
                    window['Mscrm'].Utilities.setReturnValue(false);
                    window['closeWindow']();
                } else {
                    this.onResolve.emit(VerificationResult.NOT_VERIFIED);
                }
            })
            .then(() => done());
    }

    onCancel() {
        if (this.isUnsupportedModal) {
            window['closeWindow']();
        } else {
            this.onResolve.emit(VerificationResult.CANCEL);
        }
    }

    onCustomerVerified(verifiedQuestions?: Set<string>) {
        const done = this._loadingScreen.add('Creating activity');
        this._cub_customerVerificationService.onVerified(verifiedQuestions)
            .then(() => this.afterVerified())
            .then(() => done());
    }

    onCustomerVerifiedByContactInfo(verifiedQuestions?: Set<string>) {
        const done = this._loadingScreen.add('Creating activity');
        this._cub_customerVerificationService.onVerifiedByContactInfo(verifiedQuestions)
            .then(() => this.afterVerified())
            .then(() => done());
    }

    onCustomerVerifiedBySecurityQuestion(verifiedQuestions?: Set<string>) {
        const done = this._loadingScreen.add('Creating activity');
        this._cub_customerVerificationService.onVerifiedBySecurityQuestion(verifiedQuestions)
            .then(() => this.afterVerified())
            .then(() => done());
    }

    afterVerified() {
        this.awaitingVerification = false;
        setTimeout(() => {
            this.awaitingVerification = true;
            if (this.isUnsupportedModal) {
                window['Mscrm'].Utilities.setReturnValue(true);
                window['closeWindow']();
            } else {
                this.onResolve.emit(VerificationResult.VERIFIED);
            }
        }, this.verifiedDisplayTime);
    }
}