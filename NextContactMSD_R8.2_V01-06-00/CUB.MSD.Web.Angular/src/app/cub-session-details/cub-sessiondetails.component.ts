﻿import { Component, Input, Output, OnInit, EventEmitter, NgZone, OnDestroy } from '@angular/core';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_SessionService } from '../services/cub-session.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { ActivatedRoute } from '@angular/router';
import * as Model from '../model';

import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'cub-sessiondetails',
    templateUrl: './cub-sessiondetails.component.html',
    providers: [Cub_SessionService]
})
export class Cub_SessionDetailsComponent implements OnInit {
    sessionId: string;
    session: Model.Session;
    sessionActivities: Model.SessionActivities[];
    sessionCases: Model.SessionCases[];
    linkedSessions: Model.Session[];
    editNotes: boolean = false;
    sessionNotes: string = "";
    detailsLoaded: boolean = false;
    linkSessionId: string;
    lookupSession: Model.Session[];
    lookupSessionNumber: string = '';
    showConfirmRemoveSessionPage: boolean = false;
    showsearchSessionPage: boolean = false;
    sessionSearchHasResult: boolean = false;
    runSessionSearch: boolean = false;
    editTranscript: boolean = false;

    @Output() onCancel = new EventEmitter<void>();

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_sessionService: Cub_SessionService,
        private _loadingScreen: AppLoadingScreenService,
        private _zone: NgZone,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
       
        const queryParams = this._route.snapshot.queryParamMap;
        if (!queryParams.has('sessionId')
        ) {
            console.error('sessionId paramater is required');
            return;
        }
       
        this.sessionId = queryParams.get('sessionId');
        const done = this._loadingScreen.add('Loading session detail');
        this._cub_sessionService.getSessionDetails(this.sessionId)
            .then((data: any) => {
                this.session = data;
                this.sessionActivities = JSON.parse(this.session.activities);
                this.sessionCases = JSON.parse(this.session.cases);
                this.sessionNotes = this.session.notes;             
            })
            .catch(err => {
                this._cub_dataService.prependCurrentError('Error getting session details');
                this.onCancelClicked();
            })
            .then(() => this.detailsLoaded = true)
            .then(() => done());

        this.getlinkedSessions();
       
    }

    getlinkedSessions()
    {
        this._cub_sessionService.getLinkedSessions(this.sessionId)
            .then((data: any) => {
                this.linkedSessions = JSON.parse(data.Body);
            })
            .catch(err => {
                this._cub_dataService.prependCurrentError('Error getting linked sessions');
                this.onCancelClicked();
            });
    }

    addLinkedSession() {
        
        this._cub_sessionService.linkSessions(this.session.sessionId, this.lookupSession[0].sessionId)
            .then(() => {
                this.lookupSession = null;
                this.lookupSessionNumber = null;
                this.runSessionSearch = false;
                this.sessionSearchHasResult = false;
                this.showsearchSessionPage = false;
                this.getlinkedSessions();
            })
            .catch(err => this._cub_dataService.onApplicationError(err));
            //.catch((error: any) => {
            //    this._cub_dataService.prependCurrentError('Error adding session');
            //});
    }

    linkSessionCancel() {
        this.lookupSession = null;
        this.lookupSessionNumber = null;
        this.runSessionSearch = false;
        this.showsearchSessionPage = false;
        this.sessionSearchHasResult = false;
    }

    onCancelClicked() {
        this.onCancel.emit();
    }
    onCloseClicked() {
        window['closeWindow']();
    }

    customerClicked(customerId: string) {          
        parent.Xrm.Utility.openEntityForm('account', customerId);
        window['closeWindow']();   
    }

    contactClicked(contactId: string) {       
        parent.Xrm.Utility.openEntityForm('contact', contactId);
        window['closeWindow']();
    }

    onEditNotesClicked() {
        this.editNotes = true;
    }
    
    onNotesCancleClicked() {
        this.editNotes = false;
    }

    onEdiTranscriptClicked() {       
        this.editTranscript = true;
    }

    onTranscriptCancelClicked() {
        this.editTranscript = false;
    }


    onNotesSaveClicked() {
        
        this._cub_sessionService.updateNotes(this.session.sessionId, this.sessionNotes)
            .then(()=>{
                this.editNotes = false;
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error updating session notes');
            });
          
    }


    confirmSessionLinkDeletion(linkId: string) {
        this.showConfirmRemoveSessionPage = true;
        this.linkSessionId = linkId
    }
   

    onRemoveSessionClicked() {
         this._cub_sessionService.removeLinkedSession(this.linkSessionId)
            .then(() => {
                this.showConfirmRemoveSessionPage = false;
                this.linkSessionId = null;
                this.getlinkedSessions();
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error removing session');
                this.linkSessionId = null;
            });

    }

    searchSessionClicked()
    {
        
        this._cub_sessionService.searchSession(this.lookupSessionNumber)
            .then((data: any) => {
                this.runSessionSearch = true;
                if (data.Header.result == "Successful") {
                    this.lookupSession = JSON.parse(data.Body);
                    this.sessionSearchHasResult = true;
                } 
                else {
                    this.sessionSearchHasResult = false;
                   
                }
                
            })
            .catch(err => {
                this._cub_dataService.prependCurrentError('Error finding session');
                this.onCancelClicked();
            });
       
    }

    /**
   * This function determines the text mask for the session Id input,
   * which should only allow numbers.
   * @param value the input field's raw value
   * @returns an array of (value.length + 1) decimal regexs
   */
    numMask(value: string) {
        const numPlace = /\d/;
        let result = [numPlace];

        if (!value) {
            return result;
        }

        const max = 100;
        const len = value.length;
        for (let i = 0; i < len && i < max; i++) {
            result.push(numPlace);
        }
        return result;
    }

}

