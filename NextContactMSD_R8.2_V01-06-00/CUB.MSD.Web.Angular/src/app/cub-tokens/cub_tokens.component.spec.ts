﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DataTableModule, SharedModule } from 'primeng/primeng';

import { Cub_TokensComponent } from './cub_tokens.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import { Cub_RadiolistComponent } from '../controls/cub-radiolist/cub_radiolist.component';
import { Cub_RadioComponent } from '../controls/cub-radio/cub_radio.component';

import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';

import { MomentDatePipe } from '../pipes/moment-date.pipe';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_OneAccountServiceStub } from '../testutilities/cub_oneaccount.service.stub';
import { Cub_TransitAccountServiceStub } from '../testutilities/cub_transitaccount.service.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';

describe('CubTokensComponent', () => {
    let comp: Cub_TokensComponent;
    let fixture: ComponentFixture<Cub_TokensComponent>;
    let root: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                DataTableModule
            ],
            declarations: [
                Cub_TokensComponent,
                Cub_ControlLabelComponent,
                Cub_DropdownComponent,
                Cub_TooltipDirective,
                Cub_RadiolistComponent,
                Cub_RadioComponent,
                MomentDatePipe
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub }
            ]
        }).overrideComponent(Cub_TokensComponent, {
            set: {
                providers: [
                    { provide: Cub_OneAccountService, useClass: Cub_OneAccountServiceStub },
                    { provide: Cub_TransitAccountService, useClass: Cub_TransitAccountServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_TokensComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
    });

    it('Should create the component', () => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    });
});
