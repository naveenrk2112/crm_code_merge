﻿import { Component, OnInit, HostListener, OnDestroy, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';

import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
    selector: 'cub-tokens',
    templateUrl: './cub_tokens.component.html',
    providers: [
        Cub_OneAccountService,
        Cub_TransitAccountService
    ]
})
export class Cub_TokensComponent implements OnInit, OnDestroy {
    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_oneAccountService: Cub_OneAccountService,
        private _cub_transitAccountService: Cub_TransitAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute,
        private _zone: NgZone
    ) {
        parent['Cubic'] = parent['Cubic'] || {};
        parent['Cubic'].account = parent['Cubic'].account || {};
        parent['Cubic'].account.tokens = {
            component: this,
            zone: this._zone
        };
    }

    customerId: string;
    statusControl: Model.Cub_Radiolist = {
        id: 'status-control-radiolist',
        group: 'statusControl',
        label: '',
        value: 'status-all',
        isButton: true,
        controls: [
            {
                id: 'status-all',
                label: 'All',
                radioClass: 'form-field-width-25'
            },
            {
                id: 'status-active',
                label: 'Active',
                radioClass: 'form-field-width-25'
            },
            {
                id: 'status-inactive',
                label: 'Inactive',
                radioClass: 'form-field-width-25'
            }
        ]
    };
    subsystemDropdownControl: Model.Cub_Dropdown = {
        isTableControl: false,
        isSmall: false,
        id: 'subsystemDropdown',
        label: '',
        value: '',
        options: [],
        formFieldClass: '',
        fieldWidthClass: ''
    };
    tokens: Model.WSSubsystemAccountToken[] = [];
    filteredTokens: any[] = [];
    numberOfRows: number = 5; //Number of rows to show before paging

    externalSubsystemChange(accountId: string) {
        if (!accountId) {
            this.subsystemDropdownControl.value = '';
        }
        else {
            this.subsystemDropdownControl.value = accountId;
        }
        this.filterTokens(this.statusControl.value, this.subsystemDropdownControl.value);
    }

    filterTokens(status: string, subsystemAccountReference: string) {
        this.filteredTokens = _.filter(this.tokens, function (o) {
            debugger;
            var statusTruth = (!status || status == 'status-all' || status == o.status || (status == 'Inactive' && o.status != 'Active'));
            var subsystemTruth = (!subsystemAccountReference || o.subsystemAccountReference == subsystemAccountReference);
            return statusTruth && subsystemTruth;
        });
    }

    statusChange(event: any) {
        var status = '';
        if (event == 'status-active') {
            status = 'Active';
        }
        else if (event == 'status-inactive') {
            status = 'Inactive';
        }
        this.filterTokens(status, this.subsystemDropdownControl.value);
    }

    onSubsystemChange(event: any) {
        var selectedValue = event[0];
        this.emitSubsystemIdChange(selectedValue);
        this.filterTokens(this.statusControl.value, selectedValue);
    }

    onSubsystemIdClicked(event: any, token: Model.WSSubsystemAccountToken) {
        event.preventDefault();
        const done = this._loadingScreen.add('Retrieving transit account');
        this._cub_transitAccountService.getTransitAccount(token.subsystemAccountReference, token.subsystem, this.customerId)
            .then(transitAccount => {
                if (transitAccount && transitAccount.Id) {
                    parent.Xrm.Utility.openEntityForm('cub_transitaccount', transitAccount.Id);
                }
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    emitSubsystemIdChange(accountId: string) {
        try {
            parent['Cubic'].account.subsystemAccounts.zone.run(() => {
                parent['Cubic'].account.subsystemAccounts.component.externalSubsystemChange(accountId);
            });
        } catch (e) { }
    }

    extractTokens(data: Model.WSOneAccountSummaryResponse) {
        if (!data) {
            return;
        }
        if (!data.hdr && data.hdr.result !== 'Successful') {
            this._cub_dataService.onApplicationError(data.hdr || 'Unable to retrieve OneAccount summary');
            return;
        }

        this.subsystemDropdownControl.options = [
            { name: 'All', value: '' } as Model.Cub_DropdownOption
        ];

        // Here we want to extract all tokens from each linked account into a
        // flat list. While iterating through each linked account, we also populate
        // the dropdown and attach the subsystem and subsystem account reference
        // to each token. Finally, we sort all tokens by status and most-recently used.
        this.tokens = _.chain(data.linkedAccounts)
            .map(a => {
                this.subsystemDropdownControl.options.push({
                    name: `${a.subsystemInfo.subsystem} - ${a.subsystemAccountReference}`,
                    value: a.subsystemAccountReference
                } as Model.Cub_DropdownOption);
                return _.map(a.subsystemAccountDetailedInfo.tokens,
                    t => _.assign(t, {
                        subsystem: a.subsystemInfo.subsystem,
                        subsystemAccountReference: a.subsystemAccountReference
                    }));
            })
            .flatten()
            .orderBy<any>([
                t => t.status,
                t => t.tokenInfo.lastUseDTM
            ], ['asc', 'desc'])
            .value() as Model.WSSubsystemAccountToken[];

        //default no filter on load
        this.filterTokens('', '');
    }

    getTokens() {
        const done = this._loadingScreen.add('Retrieving tokens...');
        this._cub_oneAccountService.getCachedOneAccountSummary(this.customerId)
            .then(data => this.extractTokens(data))
            .then(() => {
                const top = _.find(this.subsystemDropdownControl.options, o => !!o.value);
                if (top) {
                    this.subsystemDropdownControl.value = top.value;
                    this.onSubsystemChange([top.value]);
                }
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    ngOnInit() {
        this.customerId = parent.Xrm.Page.data.entity.getId();
        this._cub_dataService.Globals.first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.numberOfRows = globals['CUB.MSD.Web.Angular']['OAM.Tokens.NumRows'] as number;
            });
        this.getTokens();
    }

    ngOnDestroy() {
        try {
            parent['Cubic'].account.tokens = null;
        } catch (e) { }
    }
}