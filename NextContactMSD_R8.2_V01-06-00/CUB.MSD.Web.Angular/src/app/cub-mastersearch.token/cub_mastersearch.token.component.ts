﻿import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Cub_MasterSearchService } from '../services/cub_mastersearch.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_CacheService } from '../services/cub-cache.service';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_SessionService } from '../services/cub-session.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';

import * as _ from 'lodash';
import * as $ from 'jquery';

interface SearchResultsToken {
    customerId?: string,
    Customer?: {
        Line1: string,
        Line2: string
    },
    OneAccount?: string,
    Token: {
        Line1: string,
        Line1Unformatted?: string
        Line2: string,
        Line3: string
    },
    Subsystem: {
        Line1: string,
        Line2: string
    },
    Account: {
        Line1: string,
        Line2: string
    }
}

@Component({
    selector: 'cub-mastersearchtoken',
    templateUrl: './cub_mastersearch.token.component.html',
    providers: [
        Cub_MasterSearchService,
        Cub_OneAccountService,
        Cub_SubsystemService,
        Cub_TransitAccountService
    ]
})
export class Cub_MasterSearchTokenComponent implements OnInit {
    bankCardNumberMask: {
        mask: Array<any>,
        placeholderChar: string
    } = {
        mask: [],
        placeholderChar: '_'
    };
    expirationDateMask: {
        mask: Array<any>,
        placeholderChar: string
    } = {
        mask: [],
        placeholderChar: '_'
    };
    defaultSubsystem: string = "";
    controls: Object = {};
    bankCardNumber: any = {};
    expirationDate: any = {};
    searchTokenButton: Model.Cub_Button = {
        id: "searchTokenButton",
        label: "Search",
        class: "button-primary",
        isDisabled: false
    };
    issueTokenButton: Model.Cub_Button = {
        id: "issueTokenButton",
        label: "Issue Token",
        class: "button-primary",
        isDisabled: false
    };

    displaySearchResults: boolean = false;
    rowsOnPage = 5;
    masterSearchFlydown = {
        id: 'masterSearchFlydown', //unique control id
        label: 'Master Search:', //Label for control
        value: "Token", //Value for control
        options: this._cub_dataService.masterSearchPages
    };
    tokenTypeDropdown: Model.Cub_Dropdown = {
        id: 'tokenType', //unique control id
        label: 'Token Type', //Label for control
        formFieldClass: 'form-fields-vertical', //Form field class to use, eg. form-fields-siblings
        labelWidthClass: 'form-field-width-M', //Width of label
        fieldWidthClass: "form-field-width-M", //Width of Control
        isTableControl: false,
        isRequired: true,
        options: [],
        value: null
    };
    subsystemDropdown: Model.Cub_Dropdown = {
        id: 'subSystem', //unique control id
        label: 'Subsystem', //Label for control
        formFieldClass: 'form-fields-vertical', //Form field class to use, eg. form-fields-siblings
        labelWidthClass: 'form-field-width-M', //Width of label
        fieldWidthClass: "form-field-width-M", //Width of Control
        isTableControl: false,
        isRequired: true,
        options: [],
        value: null
    };
    tokenResults: any[];
    subsystemResults: any[];
    displayAsLinkPage: boolean = false; //If page should be in the "Link" view
    oneAccountId: number; //The customer to link to
    customerId: string = ''; //the Dynamics Customer id
    customerName: string = ''; //The customer name
    //Contact attributes
    firstName: string = '';
    lastName: string = '';
    fullName: string = '';
    email: string = '';
    phone1: string = '';
    phone1Type: string = '';
    phone2: string = '';
    phone2Type: string = '';
    phone3: string = '';
    phone3Type: string = '';
    defaultTokenType: string;
    showLinkToTransitAccount: boolean = false;
    subsystemAccountReference: string = '';
    subsystemAccountNickname: string = '';

    constructor(
        private _cub_masterSearchService: Cub_MasterSearchService,
        private _cub_oneAccountService: Cub_OneAccountService,
        public _cub_dataService: Cub_DataService,
        private _cache: Cub_CacheService,
        private _cub_sessionService: Cub_SessionService,
        private _cub_subsystemService: Cub_SubsystemService,
        private _cub_transitAccountService: Cub_TransitAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private router: Router,
        private _renderer: Renderer2
    ) { }

    handleRequired(data: any) {
        var bankCardNumber = this.bankCardNumber;
        var expirationDate = this.expirationDate;
        if (bankCardNumber['valueHolding'] || expirationDate['value']) {
            bankCardNumber['isRequired'] = true;
            expirationDate['isRequired'] = true;
        }
        else {
            bankCardNumber['isRequired'] = false;
            expirationDate['isRequired'] = false;
        }

        if (this.bankCardNumber["valueHolding"] && this.expirationDate["value"]) {
         //   this.searchTokenButton.isDisabled = false;
        }
        else {
          //  this.searchTokenButton.isDisabled = true;
        }

        this.bankCardNumber["isErrored"] = false;
        this.bankCardNumber["showErrorMessage"] = false;
        this.bankCardNumber["isInvalid"] = false;
        this.expirationDate["isErrored"] = false;
        this.expirationDate["showErrorMessage"] = false;
        this.expirationDate["isInvalid"] = false;
    }

    ngOnInit() {
        let height = $('#navTabGroupDiv', top.document).css('height');
        $('#crmTopBar', top.document).hide();
        // NOTE: this currently gets overridden by MSD on initial page load
        $('#crmContentPanel', top.document).css('top', height);
        const bodyTag = document.getElementsByTagName('body')[0];
        this._renderer.addClass(bodyTag, 'grey-background');

        const queryParams = this.route.snapshot.queryParamMap;
        // if these values are passed, the user wishes to link a customer to a token
        if (queryParams.has('customerId')
            && queryParams.has('customerName')
            && queryParams.has('oneAccountId')
        ) {
            this.customerId = queryParams.get('customerId');
            this.customerName = queryParams.get('customerName');
            this.oneAccountId = +queryParams.get('oneAccountId');
            this.displayAsLinkPage = true;
        }

        this.router.navigate([
            '/MasterSearchToken',
            { outlets: { session: 'Session' } }
        ], { replaceUrl: true });

        this.bankCardNumber = {
            type: "Textbox",
            id: "bankCardNumber",
            class: "form-field-width-XL",
            isRequired: true,
            hideRequiredIndicator: true,
            errorMessage: "Bank Card is required",
            validationMessage: "Bank Card is invalid",
            label: "Bank Card #",
            isVerticalStackedLabel: true,
            value: '',
            onChange: this.handleRequired,
            placeholderChar: '_',
            valueOnBlur: '',
            maskOnBlur: ['X', 'X', 'X', 'X', '-', 'X', 'X', 'X', 'X', '-', 'X', 'X', 'X', 'X', '-', /\d/, /\d/, /\d/, /\d/] //TODO: Globals?
        } as Model.Cub_Textbox;
        this.expirationDate = {
            type: "Textbox",
            id: "expirationDate",
            class: "form-field-width-XS",
            isRequired: true,
            hideRequiredIndicator: true,
            errorMessage: "Expiration Date is required",
            validationMessage: "Expiration Date is invalid",
            label: "Expiration Date",
            isVerticalStackedLabel: true,
            value: this._cub_dataService.searchValues['expirationDate'] || '',
            onChange: this.handleRequired,
        } as Model.Cub_Textbox;

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'] && !!globals['MasterSearch'])
            .subscribe(globals => this.loadConstants(globals));

        const done = this._loadingScreen.add();
        this._cub_masterSearchService.getTokenTypes()
            .then((tokens: any[]) => {
                this.tokenResults = tokens;
                let defaultToken;
                let tokenOptions = _.map(tokens, t => {
                    if (t.isDefaultTokenType) {
                        defaultToken = t;
                        this.defaultTokenType = t.tokenType;
                    }
                    return {
                        value: t.tokenType,
                        name: t.tokenType
                    };
                });

                this.tokenTypeDropdown.options = tokenOptions;
                this.tokenTypeDropdown.value = defaultToken.tokenType;

                this.setRelatedSubsystemsControl(defaultToken);
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());

        this.handleRequired(null);
    }

    //When user clicks 'Cancel' in Link View
    // TODO: is this correct?
    cancelButtonClicked() {
        this._cub_dataService.previousPage = "";
        this.router.navigate(["/MasterSearchContact"]);
    }

    linkToCustomerClicked(record: SearchResultsToken) {
        const params = {
            subsystemAccountReference: record.Account.Line1,
            subsystemId: this.subsystemDropdown.value
        };
        this.router.navigate(["/MasterSearchContact"], { queryParams: params });
    }

    completeLinkProcess() {
        this.showLinkToTransitAccount = false;
        let subsystemId: string = this.subsystemDropdown.value;
        const done = this._loadingScreen.add(`Linking ${this.subsystemAccountNickname} to ${this.customerName}`);

        this._cub_oneAccountService.LinkOneAccountToSubsystem(this.oneAccountId, subsystemId, this.subsystemAccountReference, this.subsystemAccountNickname)
            .then(() => {
                this._cub_subsystemService.clearCachedSubsystemStatus(subsystemId, this.subsystemAccountReference);
                this._cub_oneAccountService.clearCachedOneAccountSummary(this.customerId);
                if (!this._cub_sessionService.isActive) {
                    return this._cub_sessionService.createSession('account', this.customerId);
                }
            })
            .then(() => this._cub_dataService.openMsdForm('account', this.customerId))
            .catch((error) => this._cub_dataService.onApplicationError(error))
            .then(() => done());
    }

    //When user clicks "Link to" in Link View on a record
    promptLinkDialog(record?: SearchResultsToken) {
        if (record) {
            this.subsystemAccountReference = record.Account.Line1;
        }

        this.showLinkToTransitAccount = true;
    }

    loadConstants(globals: Model.Cub_Globals) {
        //Table
        this.rowsOnPage = globals['CUB.MSD.Web.Angular']['MasterSearch.NumRows'] as number;

        //Text fields
        if (globals.MasterSearch.ExpirationDate_Placeholder) {
            this.expirationDate.placeholder = globals.MasterSearch.ExpirationDate_Placeholder;
        }
        if (globals.MasterSearch.BankCardNumber_ValidationRegEx) {
            let pattern = globals.MasterSearch.BankCardNumber_ValidationRegEx as string;
            this.bankCardNumber.validationRegEx = new RegExp(pattern);
        }
        if (globals.MasterSearch.BankCardNumber_Mask) {
            let value = globals.MasterSearch.BankCardNumber_Mask as string;
            let mask = _.map(value.split('|'), char => char.length > 1 ? new RegExp(char) : char);
            this.bankCardNumberMask = Object.assign({}, this.bankCardNumberMask, { mask: mask });
        }
        if (globals.MasterSearch.ExpirationDate_ValidationRegEx) {
            let pattern = globals.MasterSearch.ExpirationDate_ValidationRegEx as string;
            this.expirationDate.validationRegEx = new RegExp(pattern);
        }
        if (globals.MasterSearch.ExpirationDate_Mask) {
            let value = globals.MasterSearch.ExpirationDate_Mask as string;
            let mask = _.map(value.split('|'), char => char.length > 1 ? new RegExp(char) : char);
            this.expirationDateMask = Object.assign({}, this.expirationDateMask, { mask: mask });
        }
    }

    searchTokenButtonClicked(event: any): Promise<any> {
        this._cub_dataService.searchValues['subSystem'] = this.subsystemDropdown.value;
        this._cub_dataService.searchValues['tokenType'] = this.tokenTypeDropdown.value;
        if (this.tokenTypeDropdown.value &&
            this.subsystemDropdown.value &&
            this.bankCardNumber.valueHolding &&
            this.bankCardNumber.validationRegEx.test(this.bankCardNumber.valueHolding) &&
            this.expirationDate.value &&
            this.expirationDate.validationRegEx.test(this.expirationDate.value)
        ) {
            this.bankCardNumber.isErrored = false;
            this.bankCardNumber.showErrorMessage = false;
            this.bankCardNumber.isInvalid = false;
            this.expirationDate.isErrored = false;
            this.expirationDate.showErrorMessage = false;
            this.expirationDate.isInvalid = false;

            const done = this._loadingScreen.add('Searching...');
            return this._cub_masterSearchService.searchToken()
                .then(data => this.searchTokenCompleted(data))
                .catch(error => {
                    this._cub_dataService.onApplicationError(error);
                    this.searchTokenCompleted(null);
                })
                .then(() => done());;
        }
        else {
            if (!this.bankCardNumber.valueHolding) {
                this.bankCardNumber.isErrored = true;
                this.bankCardNumber.showErrorMessage = true;
            }
            else if (!this.bankCardNumber.validationRegEx.test(this.bankCardNumber.valueHolding)) {
                this.bankCardNumber.isInvalid = true;
                this.bankCardNumber.showErrorMessage = true;
            }
            else {
                this.bankCardNumber.isErrored = false;
                this.bankCardNumber.showErrorMessage = false;
                this.bankCardNumber.isInvalid = false;
            }

            if (!this.expirationDate.value) {
                this.expirationDate.isErrored = true;
                this.expirationDate.showErrorMessage = true;
            }
            else if (!this.expirationDate.validationRegEx.test(this.expirationDate.value)) {
                this.expirationDate.isInvalid = true;
                this.expirationDate.showErrorMessage = true;
            }
            else {
                this.expirationDate.isErrored = false;
                this.expirationDate.showErrorMessage = false;
                this.expirationDate.isInvalid = false;
            }
        }
        return Promise.resolve();
    }

    searchTransitAccount() {
        const done = this._loadingScreen.add('Searching for transit account');
        this._cub_subsystemService.getCachedSubsystemStatus(this.subsystemDropdown.value, this.subsystemAccountReference)
            .then(data => {
                if (!data) {
                    return Promise.reject('No transit account found');
                } else {
                    this.navigateToTransitAccount(this.subsystemAccountReference);
                }
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    searchTokenCompleted(data: any) {
        var tableData: SearchResultsToken[] = [];
        this.displaySearchResults = true;
        if (!data) {
            this._cub_dataService.masterSearchTokenResults = [];
            return;
        }

        for (var key in data['searchResults']) {
            let record = data['searchResults'][key];
            let customer;
            let customerId;
            try {
                customerId = record.customerData.customerId;
                const name = record.customerData.contacts[0].name;
                customer = {
                    Line1: name.firstName + ' ' + name.lastName,
                    Line2: record.customerData.customerType
                }
            } catch (e) {
                console.warn('Error accessing field customerData on token search result');
            }
            const oneAccount = record['oneAccountId'];
            var token = {
                Line1: record['subsystemAccountData'] && record['subsystemAccountData']['tokenInfo'] && record['subsystemAccountData']['tokenInfo']['maskedPAN']
                    ? this.formatBankCard(this.highlightText(record['subsystemAccountData']['tokenInfo']['maskedPAN'], this.bankCardNumber['value'] && this.bankCardNumber['value'].length >= 4 ? this.bankCardNumber['value'].substring(this.bankCardNumber['value'].length - 4, this.bankCardNumber['value'].length) : this.bankCardNumber['value']))
                    : null,
                Line1Unformatted: record['subsystemAccountData'] && record['subsystemAccountData']['tokenInfo'] && record['subsystemAccountData']['tokenInfo']['maskedPAN']
                    ? this.formatBankCard(this.bankCardNumber['value'] && this.bankCardNumber['value'].length >= 4 ? this.bankCardNumber['value'].substring(this.bankCardNumber['value'].length - 4) : this.bankCardNumber['value'])
                    : null,
                Line2: record['subsystemAccountData'] && record['subsystemAccountData']['tokenInfo'] && record['subsystemAccountData']['tokenInfo']['tokenType']
                    ? record['subsystemAccountData']['tokenInfo']['tokenType']
                    : null,
                Line3: record['subsystemAccountData'] && record['subsystemAccountData']['tokenStatusDescription'] ? record['subsystemAccountData']['tokenStatusDescription'] : null
            };
            var subSystem = {
                Line1: record['subsystemAccountData'] && record['subsystemAccountData']['subsystemAccountTypeDescription']
                    ? record['subsystemAccountData']['subsystemAccountTypeDescription']
                    : null,
                Line2: record['subsystemAccountData'] && record['subsystemAccountData']['subsystemAccountStatusDescription']
                    ? record['subsystemAccountData']['subsystemAccountStatusDescription']
                    : null,
            };
            var account = {
                Line1: record['subsystemAccountData'] && record['subsystemAccountData']['subsystemAccountReference']
                    ? record['subsystemAccountData']['subsystemAccountReference']
                    : null,
                Line2: record['subsystemAccountData'] && record['subsystemAccountData']['subsystemTokenTypeDescription']
                    ? record['subsystemAccountData']['subsystemTokenTypeDescription']
                    : null,
            };

            tableData.push({
                customerId: customerId,
                Customer: customer,
                OneAccount: oneAccount,
                Token: token,
                Subsystem: subSystem,
                Account: account
            } as SearchResultsToken);
        }

        this._cub_dataService.masterSearchTokenResults = tableData;
    }

    searchTransitAccountCompleted(data: any) {
        var tableData = [];

        if (data && data['tokens'] && data['tokens'].length > 0) {
            this._cub_dataService.showError("Accounts found: " + data['tokens'].length + ". Open details page here");
            this.displaySearchResults = false;
        }
        else {
            this._cub_dataService.masterSearchTokenResults = [];
            this.displaySearchResults = true;
        }
    }

    issueTokenButtonClicked(event: any) {
        if (this.subsystemDropdown.value && this.bankCardNumber.valueHolding && this.expirationDate.value) {
            const done = this._loadingScreen.add('Issuing Token');
            this._cub_subsystemService.OpenLoopToken(this.subsystemDropdown.value, this.bankCardNumber.valueHolding, this.expirationDate.value)
                .then(response => {
                    if (response.Header.result == "Successful") {
                        return this.searchTokenButtonClicked(null);
                    } else {
                        var header = JSON.parse(response.Header.errorMessage);
                        var msg = `Error: ${header.result} <br> Field: ${header.fieldName} <br> ErrorKey: ${header.errorKey} <br> ${header.errorMessage}`;
                        //return Promise.reject('Error trying to issue a token:' + msg);
                        this._cub_dataService.showError('Error trying to issue a token:<br>' + msg);
                    }
                })
                .catch(err => this._cub_dataService.onApplicationError(err))
                .then(() => done());
        } else {
            this._cub_dataService.showError("Subsystem, Card Number and Expiration Date are required!");
        }

        //TODO: Fill out when we get the page layout for the issue token page.
        //this._cub_dataService.previousPage = "/MasterSearchToken";
        //this.router.navigate(["/IssueToken"]);
    }

    onMasterSearchSelectDropdownChanged(event: any) {
        this._cub_dataService.previousPage = "/MasterSearchToken";
        this.router.navigate([`/MasterSearch${event[0]}`]);
    }

    onTokenTypeDropdownChanged(event: any) {
        if (event) {
            var newValue = event[0];
            this._cub_dataService.searchValues['tokenType'] = newValue;
            let token = _.find(this.tokenResults, t => t.tokenType === newValue);
            this.setRelatedSubsystemsControl(token);
        }
        this.handleRequired(event);
    }

    onSubsystemDropdownChanged(event: any) {
        if (event) {
            var newValue = event[0];
            this._cub_dataService.searchValues['subSystem'] = newValue;
        }
        this.handleRequired(event);
    }

    textChanged(data: any = null) {
        if (data) {
            var newValue = data[0];
            var textControl = data[1];

            if (textControl['id'] == 'bankCardNumber') {
                if (!newValue && textControl['valueHolding'])
                {
                    textControl['valueHolding'] = newValue;
                }
                textControl['valueOnBlur'] = newValue.length > 4 ? newValue.substring(newValue.length - 4, newValue.length) : newValue;
                this._cub_dataService.searchValues[textControl['id']] = textControl['valueHolding'];
            }
            else {
                this._cub_dataService.searchValues[textControl['id']] = newValue;
            }
        }
        this.handleRequired(data);
    }

    clearButtonClicked(event: any) {
        this._cub_dataService.searchValues["tokenType"] = this.defaultTokenType;
        this.tokenTypeDropdown.value = this.defaultTokenType;
        let token = _.find(this.tokenResults, t => t.tokenType === this.defaultTokenType);
        this.setRelatedSubsystemsControl(token);
        this._cub_dataService.searchValues["subSystem"] = this.defaultSubsystem;
        this._cub_dataService.searchValues["bankCardNumber"] = "";
        this.bankCardNumber.value = "";
        this.bankCardNumber.valueHolding = "";
        this.bankCardNumber.valueOnBlur = "";
        this._cub_dataService.searchValues["expirationDate"] = "";
        this.expirationDate.value = "";
        this._cub_dataService.searchValues["transitAccountNumber"] = "";
      //  this.searchTokenButton.isDisabled = true;
       
        this.subsystemAccountReference = null;
    }

    // TODO: a form validator would likely be simpler
    /**
     * This function determines the text mask for the transit account # input,
     * which should only allow numbers.
     * @param value the input field's raw value
     * @returns an array of (value.length + 1) decimal regexs
     */
    numMask(value: string) {
        const numPlace = /\d/;
        let result = [numPlace];

        if (!value) {
            return result;
        }

        const max = 100;
        const len = value.length;
        for (let i = 0; i < len && i < max; i++) {
            result.push(numPlace);
        }
        return result;
    }

    escapeRegExp(str: string) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    highlightText(text: any, highlight: any, placeholder: string = "_") {
        if (!text || !highlight) {
            return text;
        }

        var re = new RegExp(_.replace(_.replace(this.escapeRegExp(highlight), new RegExp(placeholder || '_', "g"), " ").trim() + ".*?", new RegExp(/\s/, "g"), "."), 'gi');
        var arr;
        var result = [];
        while ((arr = re.exec(text)) !== null) {
            result.push(arr.index);
        }

        var offset = 0;

        for (var key in result) {
            var match = result[key];
            text = this.stringSplice(text, match + highlight.length + offset, 0, '</mark>')
            text = this.stringSplice(text, match + offset, 0, '<mark>')
            offset += 13;
        }
        return text;
    }

    stringSplice(str, start, delCount, newSubStr) {
        return str.slice(0, start) + newSubStr + str.slice(start + Math.abs(delCount));
    }

    //Format credit card number
    //TODO: Do we have to make configurable, and if so, how?
    formatBankCard(card: any) {
        if (!card) {
            return "XXXX-XXXX-XXXX-";
        }

        return "XXXX-XXXX-XXXX-" + card;
    }

    setRelatedSubsystemsControl(token: any) {
        let defaultValue;
        let options = _.map(token.supportedSubsystems, (s: any) => {
            if (s.isDefault) {
                defaultValue = s.subsystem;
                this.defaultSubsystem = s.subsystem;
            }
            return {
                value: s.subsystem,
                name: s.subsystem
            };
        });
        this.subsystemDropdown.options = options;
        this.subsystemDropdown.value = defaultValue;
    }

    navigateToTransitAccount(subsystemAccountReference: string, customerId?: string) {
        const done = this._loadingScreen.add('Retrieving transit account');
        let transitAccount: Model.WSCubTransitAccount;
        this._cub_transitAccountService.getTransitAccount(subsystemAccountReference, this.subsystemDropdown.value, customerId)
            .then(result => {
                if (!result) {
                    return Promise.reject('No transit account found');
                }
                transitAccount = result;
                if (!this._cub_sessionService.isActive) {
                    return this._cub_sessionService.createSession(Model.cub_TransitAccountLogicalName, transitAccount.Id);
                } else if (!this._cub_sessionService.session.customerId || !this._cub_sessionService.session.transitAccountId) {
                    let updatedSession = {
                        id: this._cub_sessionService.session.sessionId
                    };
                    if (transitAccount.Customer) {
                        updatedSession['cub_customerid'] = { id: transitAccount.Customer.Id };
                        updatedSession['cub_isregistered'] = true;
                    } else {
                        updatedSession['cub_transitaccount'] = { id: transitAccount.Id };
                    }
                    return this._cub_sessionService.updateSession(updatedSession);
                }
            })
            .then(() => this._cub_dataService.openMsdForm(Model.cub_TransitAccountLogicalName, transitAccount.Id))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    customerClicked(record) {
        if (record != null && record.customerId != null) {
            this._cub_dataService.openMsdForm(Model.CustomerLogicalName, record.customerId);
        }
    }
}