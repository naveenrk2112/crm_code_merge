import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Tam_BalancesComponent } from './tam-balances.component';

describe('Tam_BalancesComponent', () => {
    let component: Tam_BalancesComponent;
    let fixture: ComponentFixture<Tam_BalancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [Tam_BalancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Tam_BalancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //TODO: change "xit" to "it" once ready to start unit testing, disabled for now due to causing unit tests failures as this is still being developed
  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
