import { Component, Input, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Tam_BalancesService, Balances } from '../services/tam_balances.service';
import { DataTableModule, SharedModule, LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'tam-balances',
  templateUrl: './tam-balances.component.html',
  providers: [Tam_BalancesService]
})
export class Tam_BalancesComponent implements OnInit {

    balancesDetail: any;
    /* Filter values */
    customerIdFilter: string;
//    rowsPerPage: number = 10;
//    awaitGlobals: boolean = true;

    @Input() customerId: string;
   
    constructor(
        private _tam_balancesservice: Tam_BalancesService,
        private _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
        const done = this._loadingScreen.add("Loading Balances...");
        let queryParams = this._route.snapshot.queryParams;
        this.customerId = queryParams['customerId'];

        this._tam_balancesservice.getAccountSummary(this.customerId)
            .then((data: any) => {
                if (data && data.accountSummary) {
                    this.balancesDetail = data.accountSummary;
                }
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error loading balances'))
            .then(() => done());
    }
}
