import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'cub-balances',
    templateUrl: './cub-balances.component.html'
})
export class Cub_BalancesComponent {
    @Input() balancesLabel: string = 'Balances';
    @Input() balances: any[] = [];

    @Output() addValue = new EventEmitter<any>();
    @Output() viewAutoloads = new EventEmitter<any>();
    @Output() adjustValue = new EventEmitter<any>();
}
