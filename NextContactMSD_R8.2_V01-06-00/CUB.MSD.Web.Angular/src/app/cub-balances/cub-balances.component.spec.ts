import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { Cub_BalancesComponent } from './cub-balances.component';
import { MoneyPipe } from '../pipes/money.pipe';

describe('CubBalancesComponent', () => {
  let component: Cub_BalancesComponent;
  let fixture: ComponentFixture<Cub_BalancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [ Cub_BalancesComponent, MoneyPipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Cub_BalancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have one balance', () => {
      var balance = {
          balance: 1000,
          autoload: false,
          nickname: 'Parking'
      };
      component.balances.push(balance);
      fixture.detectChanges();

      expect(fixture.nativeElement.querySelectorAll(".balance-blocks li").length).toEqual(1);
      expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[0].querySelectorAll("p")[0].innerHTML).toEqual("$10.00");
      expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[0].querySelectorAll("p")[1].innerHTML).toEqual("Parking");
  });

  it('should have two balances', () => {
      var balance1 = {
          balance: 1000,
          autoload: false,
          nickname: 'Parking'
      };
      var balance2 = {
          balance: 2000,
          autoload: false,
          nickname: 'Reserved'
      };
      component.balances.push(balance1);
      component.balances.push(balance2);
      fixture.detectChanges();

      expect(fixture.nativeElement.querySelectorAll(".balance-blocks li").length).toEqual(2);
      expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[0].querySelectorAll("p")[0].innerHTML).toEqual("$10.00");
      expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[0].querySelectorAll("p")[1].innerHTML).toEqual("Parking");
      expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[1].querySelectorAll("p")[0].innerHTML).toEqual("$20.00");
      expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[1].querySelectorAll("p")[1].innerHTML).toEqual("Reserved");
  });
});
