import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { DataTableModule, SharedModule } from 'primeng/primeng';
import { TextMaskModule } from 'angular2-text-mask';
import { NguiDatetimePickerModule, NguiDatetime } from '@ngui/datetime-picker';

import { Cub_CustomerOrderHistoryComponent } from './cub-customer-orderhistory.component';
import { Cub_CustomerOrderComponent } from '../cub-customer-order/cub-customer-order.component';
import { Cub_CustomerOrderhistoryDetailComponent } from '../cub-customer-orderhistory-detail/cub-customer-orderhistory-detail.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import { Cub_TextboxComponent } from '../controls/cub-textbox/cub_textbox.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_OptionSelectComponent } from '../controls/cub-optionselect/cub_optionselect.component';
import { Cub_HelpComponent } from '../controls/cub-help/cub_help.component';
import { Cub_ButtonComponent } from '../controls/cub-button/cub_button.component';
import { Cub_MultiDropdown } from '../controls/cub-multi-dropdown/cub-multi-dropdown.component';
import { Cub_DateRangeComponent } from '../controls/cub-date-range/cub-date-range.component';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { MomentDatePipe } from '../pipes/moment-date.pipe';
import { MoneyPipe } from '../pipes/money.pipe';

import { Cub_MenuToggleDirective } from '../cub-menu-toggle/cub-menu-toggle.directive';
import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';

import { WSGetCustomerOrderHistoryResponse } from '../model';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_CustomerOrderServiceStub } from '../testutilities/cub_customerorder.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

import * as moment from 'moment';

describe('Cub_CustomerOrderHistoryComponent', () => {
    const QUERY_PARAMS = {
        customerId: '00000000-0000-0000-0000-000000000001'
    };
    const ORDER_HISTORY_RESPONSE_BODY: WSGetCustomerOrderHistoryResponse = {
        hdr: {
            result: 'Successful'
        },
        orders: [
            {
                orderId: 1,
                orderDateTime: '2017-01-01T00:00:00Z',
                orderLastUpdate: '2017-01-01T00:00:00Z',
                orderType: 'type',
                orderTypeDescription: 'type descript',
                orderStatus: 'status',
                orderStatusDescription: 'status descript',
                orderTotalAmount: 100,
                paymentStatus: 'payment status',
                paymentStatusDescription: 'payment status descript'
            }, {
                orderId: 2,
                orderDateTime: '2017-01-02T00:00:00Z',
                orderLastUpdate: '2017-01-02T00:00:00Z',
                orderType: 'type',
                orderTypeDescription: 'type descript',
                orderStatus: 'status',
                orderStatusDescription: 'status descript',
                orderTotalAmount: 200,
                paymentStatus: 'payment status',
                paymentStatusDescription: 'payment status descript'
            }
        ],
        totalCount: 2
    };

    let component: Cub_CustomerOrderHistoryComponent;
    let fixture: ComponentFixture<Cub_CustomerOrderHistoryComponent>;
    let service: Cub_CustomerOrderService;
    let dataService: Cub_DataService;
    let root: DebugElement;
    let table;

    // frequently-invoked sequence of component initialization steps
    let INIT = () => {
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                DataTableModule,
                TextMaskModule,
                NguiDatetimePickerModule
            ],
            declarations: [
                Cub_CustomerOrderHistoryComponent,
                Cub_CustomerOrderComponent,
                Cub_CustomerOrderhistoryDetailComponent,
                Cub_DropdownComponent,
                Cub_TextboxComponent,
                Cub_ControlLabelComponent,
                Cub_OptionSelectComponent,
                Cub_HelpComponent,
                Cub_ButtonComponent,
                Cub_DateRangeComponent,
                MomentDatePipe,
                MoneyPipe,
                Cub_MultiDropdown,
                Cub_TooltipDirective,
                Cub_MenuToggleDirective
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
            ]
        }).overrideComponent(Cub_CustomerOrderHistoryComponent, {
            set: {
                providers: [
                    { provide: Cub_CustomerOrderService, useClass: Cub_CustomerOrderServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_CustomerOrderHistoryComponent);
        component = fixture.componentInstance;
        root = fixture.debugElement;
        service = root.injector.get(Cub_CustomerOrderService);
        dataService = TestBed.get(Cub_DataService);
        table = root.nativeElement;
        ActivatedRouteStub._queryParams = QUERY_PARAMS;
        Cub_CustomerOrderServiceStub._orderHistoryResponse = ORDER_HISTORY_RESPONSE_BODY;
    });

    it('should be created', fakeAsync(() => {
        INIT();
        expect(component).toBeTruthy();
    }));

    it('should set customer ID, initialize controls, and get order history on init', fakeAsync(() => {
        spyOn(service, 'getOrderHistory').and.callThrough();
        INIT();
        expect(component.customerId).toEqual(QUERY_PARAMS.customerId);
        expect(service.getOrderHistory).toHaveBeenCalled();
        expect(component.orderHistoryResults).toEqual(ORDER_HISTORY_RESPONSE_BODY.orders);
        expect(component.totalCount).toEqual(ORDER_HISTORY_RESPONSE_BODY.totalCount);
    }));

    it('#onLazyLoad should not call getOrders if component is not initialized', fakeAsync(() => {
        INIT();
        spyOn(component, 'getOrders');
        component.initComplete = false;
        component.onLazyLoad(null);
        expect(component.getOrders).not.toHaveBeenCalled();
    }));

    it('#onLazyLoad should fire when a paging button is clicked', fakeAsync(() => {
        Cub_DataServiceStub._constants['CUB.MSD.Web.Angular']['OrderHistory.NumRows'] = 1;
        INIT();
        tick();
        fixture.detectChanges();
        spyOn(component, 'onLazyLoad');
        table.querySelectorAll('.ui-paginator-page')[1].click();
        expect(component.onLazyLoad).toHaveBeenCalled();
    }));

    it('#onDropdownChanged should enable the apply button', fakeAsync(() => {
        INIT();
        let buttonEl = table.querySelector('#apply-button');
        expect(component.disableApply).toBeTruthy();
        expect(buttonEl.disabled).toBeTruthy();
        component.onDropdownChanged();
        fixture.detectChanges();
        expect(component.disableApply).toBeFalsy();
        expect(buttonEl.disabled).toBeFalsy();
    }));

    it('#onApplyButtonClicked should do nothing if initialization is not complete', fakeAsync(() => {
        INIT();
        spyOn(component, 'onApplyButtonClicked').and.callThrough();
        spyOn(component, 'getOrders');
        component.initComplete = false;
        component.disableApply = false;
        fixture.detectChanges();
        table.querySelector('#apply-button').click();
        expect(component.onApplyButtonClicked).toHaveBeenCalled();
        expect(component.getOrders).not.toHaveBeenCalled();
    }));

    it('#onApplyButtonClicked should do nothing if the button is disabled', fakeAsync(() => {
        INIT();
        spyOn(component, 'onApplyButtonClicked').and.callThrough();
        component.disableApply = true;
        fixture.detectChanges();
        table.querySelector('#apply-button').click();
        expect(component.onApplyButtonClicked).not.toHaveBeenCalled();
    }));

    it('#onApplyButtonClicked should call getOrders when clicked', fakeAsync(() => {
        INIT();
        spyOn(component, 'onApplyButtonClicked').and.callThrough();
        spyOn(component, 'getOrders');
        component.disableApply = false;
        fixture.detectChanges();
        table.querySelector('#apply-button').click();
        expect(component.onApplyButtonClicked).toHaveBeenCalled();
        expect(component.getOrders).toHaveBeenCalled();
    }));

    describe('#getOrders', () => {
        it('should not do anything if inputs are invalid', () => {
            spyOn(service, 'getOrderHistory');
            component.endDateFilter = '2017-01-01T23:59:59.999Z';
            component.startDateFilter = null;
            component.getOrders();
            expect(component.disableApply).toBeTruthy();
            expect(service.getOrderHistory).not.toHaveBeenCalled();
        });

        it('should set request params', fakeAsync(() => {
            INIT();
            spyOn(service, 'getOrderHistory').and.callThrough();
            const START = component.startDateFilter = '2017-01-01T00:00:00.000Z';
            const END = component.endDateFilter = '2017-12-31T23:59:59.999Z';
            const OFFSET = component.rowOffset;
            const LIMIT = component.rowsLimit;
            const STATUS = component.orderStatusFilter = '';
            const TYPE = component.orderTypeFilter = '';
            const PAYMENT_STATUS = component.paymentStatusFilter = '';
            component.getOrders();
            tick();
            expect(service.getOrderHistory).toHaveBeenCalledWith(QUERY_PARAMS.customerId, {
                startDate: START,
                endDate: END,
                offset: OFFSET,
                limit: LIMIT,
                orderType: TYPE,
                orderStatus: STATUS,
                paymentStatus: PAYMENT_STATUS
            });
        }));

        it('should not set certain filter params if they are unset', fakeAsync(() => {
            INIT();
            spyOn(service, 'getOrderHistory').and.callThrough();
            const START = component.startDateFilter = '2017-01-01T00:00:00.000Z';
            const END = component.endDateFilter = '2017-12-31T23:59:59.999Z';
            const OFFSET = component.rowOffset;
            const LIMIT = component.rowsLimit;
            const ORDER_TYPE = component.orderTypeFilter = null;
            const ORDER_STATUS = component.orderStatusFilter = null;
            const PAYMENT_STATUS = component.paymentStatusFilter = null;
            component.getOrders();
            tick();
            expect(service.getOrderHistory).toHaveBeenCalledWith(QUERY_PARAMS.customerId, {
                startDate: START,
                endDate: END,
                offset: OFFSET,
                limit: LIMIT,
                orderType: ORDER_TYPE,
                orderStatus: ORDER_STATUS,
                paymentStatus: PAYMENT_STATUS
            });
        }));
    });
});
