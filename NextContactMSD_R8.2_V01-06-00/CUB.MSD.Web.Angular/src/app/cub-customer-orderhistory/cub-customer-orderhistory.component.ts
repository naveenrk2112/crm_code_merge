import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CubOrderService } from '../services/cub-order.service';
import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';

import * as moment from 'moment-timezone';
import * as _ from 'lodash';
import { LazyLoadEvent, SortMeta } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';


@Component({
    selector: 'cub-customer-orderhistory',
    templateUrl: './cub-customer-orderhistory.component.html',
    providers: [CubOrderService,Cub_CustomerOrderService]
})
export class Cub_CustomerOrderHistoryComponent implements OnInit, AfterViewInit {
    orderNumberControl: Model.Cub_Textbox = {
        id: "1",
        label: "Order Number",
        includeLabel: true
    };
    realTimeControl: Model.Cub_Checkbox = {
        checkboxClass: 'fency-checkbox',
        isDisabled: false,
        id: 'isRealTime',
        name: 'isRealTime',
        label: 'Real Time',
        value: false,
        partial: false
    }

    customerId: string;
    orderHistoryResults: Model.WSOrderHistoryLineItem[] = [];
    totalCount: number = 0;
    orderTypeOptions: Model.Cub_MultiDropdownOption[];
    orderStatusOptions: Model.Cub_MultiDropdownOption[];
    paymentStatusOptions: Model.Cub_MultiDropdownOption[];
    orderTypeFilter: string = null;
    orderStatusFilter: string = null;
    paymentStatusFilter: string = null;
    startDateFilter: string;
    endDateFilter: string;
    startDateOffset: number = 30;
    disableApply: boolean = true;
    rowOffset: number = 0;
    rowsLimit: number = 10;
    initComplete: boolean = false;
    selectedOrderId: number;
    selectedOrder: any;
    showPaymentHistoryDetailModal: boolean = false;
    showOrderDetailModal: boolean = false;
    isRealTime: boolean = false;
    multiSortMeta: SortMeta[] = [];
    sortBy: string;

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_orderService: CubOrderService,
        private _cub_customerOrderService: Cub_CustomerOrderService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { 
        this.multiSortMeta.push({
            field: 'startDateTime',
            order: -1
        });
        this.sortBy = this._cub_dataService.formatSortQueryParam(this.multiSortMeta);
    }

    /**
     * Get customer ID from query params, create filter controls, and call web
     * service for initial data.
     */
    ngOnInit() {
        //this._cub_customerOrderService.getCachedOrderId()
        //    .then(orderId => {
        //        if (orderId != null && !isNaN(orderId)) {
        //            this.selectedOrderId = orderId;
        //            this.showOrderDetailModal = true;
        //        }
        //    });
        if (parent.Xrm.Page.data.entity.getEntityName() == 'account') {
            this.customerId = parent.Xrm.Page.data.entity.getId();
        }
        else {  //contact
            //this.contactId = parent.Xrm.Page.data.entity.getId();
            const customerControl = parent.Xrm.Page.getAttribute('parentcustomerid');
            if (customerControl && customerControl.getValue()) {
                this.customerId = customerControl.getValue()[0].id;
            }
        }
        
        // Load configurable data
        const globals$ = this._cub_dataService.Globals.first(globals => !!globals['CUB.MSD.Web.Angular']);
        const grids$ = this._cub_dataService.GridConfigs.first(configs => !!configs['Customer.OrderHistory']);
        const orderConfigTypes$ = this._cub_orderService.getOrderConfigDataTypesFromCache();
        const done = this._loadingScreen.add();
        Observable.forkJoin(globals$, grids$, orderConfigTypes$)
            .finally(() => done())
            .subscribe(([globals, grids, orderConfigTypes]) => {
                this.startDateOffset = globals['CUB.MSD.Web.Angular']['Customer.OrderHistory.StartDateOffSet'] as number;
                const grid = grids['Customer.OrderHistory'];
                this.rowsLimit = grid.rowsToDisplay;

                // Filling Order Types
                this.orderTypeOptions = orderConfigTypes.orderTypes.map(ot => ({
                    value: ot.type,
                    label: ot.shortDescription
                }));
                // Filling Order Status
                this.orderStatusOptions = orderConfigTypes.orderStatuses.map(ot => ({
                    value: ot.status,
                    label: ot.shortDescription
                }));
                 // Filling Payment  Status
                this.paymentStatusOptions = orderConfigTypes.paymentStatuses.map(pt => ({
                    value: pt.paymentStatusEnum,
                    label: pt.description
                   

                }));
                this.getOrders();
            });
    }

    /**
     * Set initComplete to true when page is done initializing. initComplete
     * is used as a check to prevent duplicate calls to get order history records
     * during component initialization because of PrimeNG grid's lazy load event.
     */
    ngAfterViewInit() {
        this.initComplete = true;
    }

    @HostListener('window:storage', ['$event'])
    onStorageChange(event: StorageEvent) {
        const pattern = new RegExp(this._cub_customerOrderService.orderIdKey + '$');
        if (event.key.match(pattern) && event.newValue != null) {
            const value = JSON.parse(event.newValue);
            if (!value || !value.data) {
                return;
            }
            this.selectedOrderId = +value.data;
            this.showOrderDetailModal = true;
            this.getOrders();
            this._cub_customerOrderService.clearCachedOrderId();
        }
    }

    parseFilterOptions(nisGlobals, globalDetailName): Model.Cub_MultiDropdownOption[] {
        let options: Model.Cub_MultiDropdownOption[] = [];
        try {
            let config: object = JSON.parse(nisGlobals[globalDetailName]);
            options = _.chain(config)
                .map((name, key) => ({
                    value: key,
                    label: name
                } as Model.Cub_MultiDropdownOption))
                .sortBy(o => o.value)
                .value();
        } catch (e) {
            this._cub_dataService.onApplicationError('Failure to parse config: ' + globalDetailName);
        }
        return options;
    }

    /**
     * Extracts parameters and calls the web service to retrieve order history records.
     */
    getOrders() {
        
        let userId = this._cub_dataService.currentUserId;
        let customerId = this.customerId;
        let orderId = this.orderNumberControl.value || null;
        let startDateTime = this.startDateFilter || null;
        let endDateTime = this.endDateFilter || null;
        let orderType = this.orderTypeFilter;
        let orderStatus = this.orderStatusFilter;
        let paymentStatus = this.paymentStatusFilter
        let sortBy = this.sortBy || null;
        let offset = (this.rowOffset > 0) ? this.rowOffset : 0;
        let limit = this.rowsLimit;
        let isRealTime = this.isRealTime
        // Call the MVC application to get Order History
        const done = this._loadingScreen.add('Loading Order History...');
        
        this._cub_customerOrderService.getOrderHistory({
            userId: userId,
            customerId: customerId,
            orderId: orderId,
            startDateTime: startDateTime,
            endDateTime: endDateTime,
            orderType: orderType,
            orderStatus: orderStatus,
            sortBy: sortBy,
            offset: offset,
            limit: limit,
            realTime: isRealTime
        })
            .then((data) => {
                if (data) {
                    var response = JSON.parse(data.Body);
                    if (response.orders) {
                        this.orderHistoryResults = response.orders;
                        this.totalCount = response.totalCount;
                        this.disableApply = false;
                    }
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting Transit Account Order History');
            })
            .then(() => this.initComplete = true)
            .then(() => done());
    }
    

    onMultiDropdownChange(selectedOptions: string[], filterName: string) {
        if (this[filterName] === undefined) {
            this._cub_dataService.onApplicationError('No filter with name "' + filterName + '" on Cub_CustomerOrderHistoryComponent!');
            return;
        }

        // if the join returns an empty string, use null instead
        this[filterName] = selectedOptions.join(',') || null;
        this.disableApply = false;
    }

    /**
     * Event handler for PrimeNG grid that calls getOrders(). This fires when
     * the user clicks the paging buttons.
     * @param event contains metadata related to paging
     */
    onLazyLoad(event: LazyLoadEvent) {        
        if (this.initComplete) {
            this.rowOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.disableApply = false;
            this.getOrders();
        }
    }

    /**
     * Generic event handler for dropdown filter controls that enables the
     * search button.
     */
    onDropdownChanged() {
        this.disableApply = false;
    }

    onMultiDropdownChanged(event, control) {
        this[control] = event.join();
        this.disableApply = false;
    }

    /**
     * Fires when the start date control changes. This sets the start date filter.
     */
    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableApply = false;
    }

    /**
     * Fires when the end date control changes. This sets the end date filter.
     */
    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableApply = false;
    }

    /**
     * Fires when the apply button is clicked. This calls getOrders().
     */
    onApplyButtonClicked() {
        if (this.initComplete && !this.disableApply) {
            this.getOrders();
        }
    }

    /**
     * Fires when a row is double clicked. Same as clicking "View Details".
     * @param event event.data contains the record
     */
    onRowDoubleClicked(event: any) {
        this.selectedOrderId = event.data.orderId;
        this.selectedOrder = event.data;
        this.showOrderDetailModal = true;
    }

    showPaymentHistoryDetail(order: Model.WSOrderHistoryLineItem) {
        this.selectedOrderId = order.orderId;
        this.selectedOrder = order;
        this.showPaymentHistoryDetailModal = true;
    }

    showOrderDetail(order: Model.WSOrderHistoryLineItem) {
        this.selectedOrderId = order.orderId;
        this.showOrderDetailModal = true;
    }

    hidePaymentHistoryDetail() {
        this.showPaymentHistoryDetailModal = false;
    }

    hideOrderDetail() {
        this.showOrderDetailModal = false;
    }
}