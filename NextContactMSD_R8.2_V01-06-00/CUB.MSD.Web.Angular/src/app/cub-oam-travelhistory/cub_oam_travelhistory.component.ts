﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Cub_OAM_TravelHistoryService, TravelHistorySearchCriteria } from '../services/cub_oam_travelhistory.service';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';

import { LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';
import { conformToMask } from 'angular2-text-mask';

@Component({
    selector: 'cub-travel',
    templateUrl: './cub_oam_travelhistory.component.html',
    providers: [
        Cub_OAM_TravelHistoryService,
        Cub_OneAccountService,
        Cub_TransitAccountService
    ]
})
export class Cub_OAM_TravelHistoryComponent implements OnInit {
    customerId: string;
    /* Search values and controls*/
    disableSearch: boolean = true;
    startDateFilter: string;
    endDateFilter: string;
    startDateOffset: number = 30;
    travelModeOptions: Model.Cub_MultiDropdownOption[];
    travelModeFilter: string;
    agencyOptions: Model.Cub_MultiDropdownOption[];
    agencyFilter: string;
    tripOptions: Model.Cub_MultiDropdownOption[];
    tripFilter: string;
    viewTypeControl: Model.Cub_Radiolist = {
        id: 'view-type-control',
        group: 'ViewType',
        label: 'View Type',
        value: 'view-type-Simple',
        isButton: true,
        controls: [
            {
                id: 'view-type-Simple',
                label: 'Simple',
            }, {
                id: 'view-type-Complete',
                label: 'Complete',
            }
        ]
    };

    showTravelHistoryDetail: boolean = false;

    travelHistoryResults: any[] = [];
    detailResults: any = null;

    /*Pagination and Sorting params*/
    multiSortMeta: SortMeta[] = [];
    sortBy: string;
    rowsPerPage: number = 10;
    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    constructor(
        public travelHistorySrvc: Cub_OAM_TravelHistoryService,
        public _cub_dataService: Cub_DataService,
        private _cub_oneAccountService: Cub_OneAccountService,
        private _cub_transitAccountService: Cub_TransitAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) {
        this.multiSortMeta.push({
            field: 'tripId',
            order: -1
        });
        this.sortBy = this._cub_dataService.formatSortQueryParam(this.multiSortMeta);
    }

    ngOnInit() {
        this.customerId = parent.Xrm.Page.data.entity.getId();
        const oneAccountControl = parent.Xrm.Page.getAttribute('cub_oneaccountid');
        if (oneAccountControl) {
            this.travelHistorySrvc.oneAccountId = oneAccountControl.getValue();
        }

        const done = this._loadingScreen.add('Retrieving Subsystem Data...');

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.rowsPerPage = globals['CUB.MSD.Web.Angular']['OAM.TravelHistory.NumRows'] as number;
                this.startDateOffset = globals['CUB.MSD.Web.Angular']['OAM.TravelHistory.StartDateOffset'] as number;
                let tripCategories = globals['CUB.MSD.Web.Angular']['TripCategories'] as string;
                this.tripOptions = _.chain(tripCategories.split(','))
                    .map(c => c.trim())
                    .sortBy()
                    .map(c => ({
                        value: c,
                        label: c
                    }))
                    .value();
            });

        this.getTravelHistory();
        this._cub_oneAccountService.getCachedOneAccountSummary(this.customerId)
            .then(data => this.extractLinkedAccountData(data))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    extractLinkedAccountData(data: any) {
        if (!data) {
            return;
        }
        if (!data.hdr || data.hdr.result !== 'Successful') {
            this._cub_dataService.onApplicationError(data.Message || 'Unable to retrieve OneAccount summary');
            return;
        }

        this.agencyOptions = _.chain(data.linkedAccounts)
            .filter((a: any) => a.subsystemInfo && a.subsystemInfo.authorityId != null)
            .map(a => ({
                value: a.subsystemInfo.authorityId,
                label: a.subsystemInfo.authorityDescription || a.subsystemInfo.authorityId
            }))
            .sortBy(a => a.label)
            .sortedUniqBy(a => a.value)
            .value();

        this.travelModeOptions = _.chain(data.linkedAccounts)
            .filter((a: any) => a.subsystemInfo && a.subsystemInfo.travelModes != null)
            .flatMap(a => a.subsystemInfo.travelModes)
            .sortBy(t => t.value)
            .sortedUniqBy(t => t.key)
            .map(t => ({
                value: t.key,
                label: t.value
            }))
            .value();
    }

    getTravelHistory(event?: LazyLoadEvent) {
        if (!this.travelHistorySrvc.oneAccountId) {
            return;
        }

        if (!!event) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
        }

        if (!!this.endDateFilter && moment(this.startDateFilter).isAfter(this.endDateFilter, 'hour')) {
            this._cub_dataService.onApplicationError('Invalid date range');
            this.disableSearch = true;
            return;
        }

        let params: TravelHistorySearchCriteria = {
            oneAccountId: this.travelHistorySrvc.oneAccountId,
            offset: (this.pagingOffset > 0) ? this.pagingOffset : 0,
            limit: this.rowsPerPage,
            sortBy: (!!this.sortBy) ? this.sortBy : null,
            viewType: this.viewTypeControl.value.replace('view-type-', '')
        };

        // apply date range filter
        if (!!this.startDateFilter) {
            params.startDateTime = this.startDateFilter;
            if (!!this.endDateFilter) {
                params.endDateTime = this.endDateFilter;
            }
        }

        // apply travel mode filter
        if (this.travelModeFilter) {
            params.travelMode = this.travelModeFilter;
        }

        // apply agency filter
        if (this.agencyFilter) {
            params.authorityId = this.agencyFilter;
        }

        // apply trip type filter
        if (this.tripFilter) {
            params.tripCategory = this.tripFilter;
        }

        const done = this._loadingScreen.add('Retrieving travel history');
        this.travelHistorySrvc.getTravelHistory(params)
            .then((data) => {
                if (data) {
                    this.travelHistoryResults = data.lineItems;
                    this.totalRecordCount = data.totalCount;
                }
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error getting travel history'))
            .then(() => done());
    }

    /**
     * Fires when the start date text is modified or a date is selected using the
     * date picker. This sets the start date filter used in getTravelHistory()
     * and determines whether the end date control should be disabled.
     */
    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableSearch = false;
    }

    /**
     * Fires when the end date text is modified or a date is selected using the
     * date picker. This sets the end date filter used in getNotifications().
     */
    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableSearch = false;
    }

    /**
    * Fires when the search button is clicked. Calls getNotifications().
    */
    onSearchClicked() {
        this.getTravelHistory();
        this.disableSearch = true;
    }

    /**
    * Fires when the enter key is pressed while either the start date, or
    * end date controls are in focus. Calls getTravelHistory() 
    */
    onEnterKey() {
        if (!this.disableSearch) {
            this.getTravelHistory();
            this.disableSearch = true;
        }
    }

    /**
    * Fires when a row is clicked within the travel history table. This
    * calls the service to get the selected travel history's detail, prepares
    * the data to display within the detail modal window.
    * @param event event.data is the selected travel history record
    */
    onRowDoubleClicked(event: any) {
        const done = this._loadingScreen.add();
        this.travelHistorySrvc.getTravelHistoryDetail(event.data.tripId)
            .then(data => {
                this.detailResults = data;
                this.showTravelHistoryDetail = true;
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error getting travel history detail'))
            .then(() => done());
    }

    /**
    * Hides the travel history detail modal window.
    */
    closeDetail() {
        this.showTravelHistoryDetail = false;
    }

    onTravelModeDropdownChanged(travelModes: string[]) {
        this.travelModeFilter = travelModes.join();
        this.disableSearch = false;
    }

    onAgencyDropdownChanged(agencies: string[]) {
        this.agencyFilter = agencies.join();
        this.disableSearch = false;
    }

    onTripTypeDropdownChanged(tripTypes: string[]) {
        this.tripFilter = tripTypes.join();
        this.disableSearch = false;
    }

    onViewTypeChanged() {
        this.disableSearch = false;
    }

    /**
     * Navigate to the Subsystem Landing page for the given record's transit account.
     * @param event
     * @param record
     */
    onSubsystemClicked(event: MouseEvent, record: any) {
        event.preventDefault();
        const done = this._loadingScreen.add('Retrieving transit account');
        this._cub_transitAccountService.getTransitAccount(record.subsystemAccountReference, record.subsystem, this.customerId)
            .then(transitAccount => this._cub_dataService.openMsdForm(Model.cub_TransitAccountLogicalName, transitAccount.Id))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }
}