import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cub_AddFundingSourceComponent } from './cub-add-funding-source.component';

describe('CubAddFundingSourceComponent', () => {
    // TODO: remove this when implemented
    pending();

    let component: Cub_AddFundingSourceComponent;
    let fixture: ComponentFixture<Cub_AddFundingSourceComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [Cub_AddFundingSourceComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_AddFundingSourceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
