﻿import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Cub_SessionService } from './cub-session.service';
import { Cub_DataService } from './cub_data.service';
import { Cub_CacheService } from './cub-cache.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_CacheServiceStub } from '../testutilities/cub-cache.service.stub';

import { Session } from '../model';

describe('Cub_SessionService', () => {
    let service: Cub_SessionService;
    let http: HttpTestingController;
    let dataService: Cub_DataService;

    function initModule(cachedSession?: Session) {
        Cub_CacheServiceStub._storage[`current-session-${Cub_DataServiceStub._currentUserId}`] = cachedSession;

        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: Cub_CacheService, useClass: Cub_CacheServiceStub },
                Cub_SessionService
            ]
        });

        http = TestBed.get(HttpTestingController);
        service = TestBed.get(Cub_SessionService);
        dataService = TestBed.get(Cub_DataService);
    }

    afterEach(() => {
        http.verify();
    })

    it('should always have a value for session', () => {
        initModule();
        service.session = null;
        expect(service.session).toBeDefined();
        expect(service.session).not.toBeNull();
        expect(service.session).toEqual({});
    });

    it('should retrieve a cached session automatically', () => {
        const id = 'SESSION_ID';
        initModule({
            sessionId: id
        });
        expect(service.session.sessionId).toEqual(id);
    });

    it('#createSession should reject if no user ID', async(() => {
        const prevUser = Cub_DataServiceStub._currentUserId;
        Cub_DataServiceStub._currentUserId = null;
        initModule();
        service.createSession(null, null)
            .catch(err => expect(err).toBeDefined())
            .then(() => Cub_DataServiceStub._currentUserId = prevUser);
    }));

    it('#createSession should return a session', async(() => {
        initModule();
        const id = '1';
        service.createSession(null, null)
            .then(result => {
                expect(result).toBeDefined();
                expect(result.sessionId).toEqual(id);
                expect(service.session.sessionId).toEqual(id);
            });
        http.expectOne(req => req.method === 'POST' && req.url.match(/Session\/CreateSession/))
            .flush({
                sessionId: id
            });
    }));

    it('#updateSession should reject if no user ID', async(() => {
        const prevUser = Cub_DataServiceStub._currentUserId;
        Cub_DataServiceStub._currentUserId = null;
        initModule();
        service.updateSession(null)
            .catch(err => expect(err).toBeDefined())
            .then(() => Cub_DataServiceStub._currentUserId = prevUser);
    }));

    it('#refreshSession should reject if no user ID', async(() => {
        const prevUser = Cub_DataServiceStub._currentUserId;
        Cub_DataServiceStub._currentUserId = null;
        initModule();
        service.refreshSession()
            .catch(err => expect(err).toBeDefined())
            .then(() => Cub_DataServiceStub._currentUserId = prevUser);
    }));

    it('#endSession should clear the current session', async(() => {
        const id = '1234';
        initModule({
            sessionId: id
        });
        service.endSession()
            .then(() => {
                expect(service.session.sessionId).toBeFalsy();
            });
        http.expectOne(req => req.method === 'POST' && req.url.match(/Session\/EndSession/))
            .flush(null, { status: 204, statusText: 'No Content' });
    }));
});