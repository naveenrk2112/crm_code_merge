﻿import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Cub_TransitAccountService } from './cub_transitaccount.service';
import { Cub_DataService } from './cub_data.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';

import * as Model from '../model';
import * as moment from 'moment';

describe('Cub_TransitAccountService', () => {
    const SUBSYSTEM_ID = 'Test-Subsystem';
    const TRANSIT_ACCOUNT_ID = 'Test-Transit-Account';
    let service: Cub_TransitAccountService;
    let dataService: Cub_DataService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                Cub_TransitAccountService,
                { provide: Cub_DataService, useClass: Cub_DataServiceStub }
            ]
        });

        service = TestBed.get(Cub_TransitAccountService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => http.verify());

    it('#getActivities should reject on HTTP error response', async(() => {
        service.getActivities('001234')
            .then(res => fail('getActivities should have rejected, but was successful'))
            .catch(err => expect(err).toBeDefined());
        http.expectOne(req => !!req.url.match('/TransitAccount/GetActivities'))
            .flush(null, { status: 500, statusText: 'TEST_SERVER_ERROR' });
    }));

    it('#getActivities should not error if no ID is passed', (done) => {
        service.getActivities(null)
            .then(res => done());
        http.expectOne(req => !!req.url.match('/TransitAccount/GetActivities'))
            .flush('good job', { status: 200, statusText: 'OK' });
    });

    it('#getActivities should resolve to an object', async(() => {
        service.getActivities('001234')
            .then(res => expect(res).toBeDefined());
        http.expectOne(req => !!req.url.match('/TransitAccount/GetActivities'))
            .flush({}, { status: 200, statusText: 'OK' });
    }));
});