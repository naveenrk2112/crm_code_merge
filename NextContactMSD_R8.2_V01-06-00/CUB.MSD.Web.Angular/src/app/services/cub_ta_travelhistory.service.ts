﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

/* Data model for request body of getTravelHistory() */
export interface TravelHistorySearchCriteria {
    accountId: string,
    subSystemId: string,
    startDateTime?: string,
    endDateTime?: string,
    travelMode?: string,
    tripCategory?: string,
    viewType?: string,
    travelPresenceIndicator?: string,
    tripStatus?: string,
    sortBy?: string,
    offset?: number,
    limit?: number
}

@Injectable()
export class Cub_TA_TravelHistoryService {
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getTravelHistory(queryParams: TravelHistorySearchCriteria) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/TransitAccount/GetTravelHistory', { params: params })
            .toPromise();
    }
}