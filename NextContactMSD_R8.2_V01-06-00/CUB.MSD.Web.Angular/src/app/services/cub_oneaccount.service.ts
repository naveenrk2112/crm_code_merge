﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';
import { Cub_CacheService } from './cub-cache.service';

import { head, orderBy } from 'lodash';

export interface OneAccountBalance {
    key: string;
    amount: number;
    autoload: boolean;
    label: string;
}

export interface OneAccountBalanceHistory {
    lineItems: OneAccountBalanceHistoryLineItem[],
    totalCount: number
}

export interface OneAccountBalanceHistoryLineItem {
    journalEntryId: number,
    entryDateTime: string,
    purseId: number,
    purseNickname: string,
    entryType: string,
    entryTypeDescription: string,
    entryStatus: string,
    entryStatusDescription: string,
    journalEntryAmount: number,
    availableBalance: number,
    endingBalance: number
}

@Injectable()
export class Cub_OneAccountService {
    public oneAccountId: string;
    public selected: OneAccountBalanceHistoryLineItem;
    readonly delinkDataKey = 'delinkData';

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService,
        private _cache: Cub_CacheService
    ) { }

    /**
     * This function links a Oneaccount record to a subsystem and transit account
     * @param customerid - AccountId in CRM - from MasterSearch Contact page
     * @param subsystemId - SubsystemId From master search token page
     * @param accountRef - Id of Transit Account - From master search tonken page
     * @param subsystemAccountNickname - Display name of the subsystem id from master search token page.
     */
    LinkOneAccountToSubsystem(customerid: number, subsystemId: string, accountRef: string, subsystemAccountNickname: string) {
        const body = {
            'oneaccountId': customerid,
            'subsystemId': subsystemId,
            'accountRef': accountRef,
            'subsystemAccountNickname': subsystemAccountNickname,
            'userId': this._cub_dataService.currentUserId
        };
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post<any>(`${this._cub_dataService.apiServerUrl}/OneAccount/LinkSubsystemToCustomeraccount`, JSON.stringify(body), { headers: headers })
            .toPromise()
            .then(response => this._cub_dataService.tryParse(response));
    }

    /**
   * This function checks if a Oneaccount record can be delinked from a subsystem and transit account
   * @param oneaccountId - OneAccount ID from master search token page
   * @param subsystemId - SubsystemId From master search token page
   * @param subsystemAccount - Display name of the subsystem id from master search token page.
   */
    CanBeDeLinkedOneAccountFromSubsystem(oneaccountId: string, subsystemId: string, subsystemAccount: string, customerId: string) {
        const temp = {
            oneAccountId: oneaccountId,
            subsystemId: subsystemId,
            subsystemAccount: subsystemAccount,
            customerId: customerId,
            userId: this._cub_dataService.currentUserId
        };
        let params = new HttpParams();
        for (let key in temp) {
            if (temp[key] != null) {
                params = params.set(key, temp[key]);
            }
        }

        var url = `${this._cub_dataService.apiServerUrl}/OneAccount/CanBeDelinked`;
        return this._http.get<any>(url, { params: params })
            .toPromise();

    }

    /**
    * This function links a Oneaccount record to a subsystem and transit account
    * @param oneaccountId - OneAccount ID from master search token page
    * @param subsystemId - SubsystemId From master search token page
    * @param subsystemAccount - Display name of the subsystem id from master search token page.
    * @param customerId - Id of Customer
    */
    DeLinkOneAccountFromSubsystem(oneaccountId: string, subsystemId: string, subsystemAccount: string, customerId: string) {
        const body = {
            'oneaccountId': oneaccountId,
            'subsystemId': subsystemId,
            'subsystemAccount': subsystemAccount,
            'customerId': customerId,
            'userId': this._cub_dataService.currentUserId
        };
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post<any>(`${this._cub_dataService.apiServerUrl}/OneAccount/DeLinkSubsystemFromCustomeraccount`, body, { headers: headers })
            .toPromise()
            .then(response => this._cub_dataService.tryParse(response));
    }

    /**
     * Get Balance History data for the Balance History Grid
     * @param oneAccountId - One Account ID on the customer record
     * @param startDateTime - The start time of balance history records to pull
     * @param endDateTime - THe end time of balance history records to pull
     * @param viewType - The view type selected by the user
     * @param financialTxnType - The transaction type selected by the user
     * @param sortBy - The sort by selected by the user, and whether ascending or not
     * @param offset - What offset to start on when pulling records
     * @param limit - The max number of records to pull
     */
    getBalanceHistory(accountId: string, startDateTime: string, endDateTime: string, viewType: string,
        purseId: string, financialTxnType: string, sortBy: string, offset: number, limit: number) {
        var params = new HttpParams()
            .set('oneAccountId', accountId)
            .set('startDateTime', startDateTime)
            .set('endDateTime', endDateTime)
            .set('viewType', viewType)
            .set('financialTxnType', financialTxnType)
            .set('sortBy', sortBy)
            .set('offset', offset.toString())
            .set('limit', limit.toString());

        if (purseId) {
            params.set('purseId', purseId);
        }

        var url = `${this._cub_dataService.apiServerUrl}/OneAccount/GetBalanceHistory`;
        return this._http.get<any>(url, { params: params })
            .toPromise();
    }

    /**
     * Get Balance History details for a specific balance history record
     * @param oneAccountId - One Account ID on the customer record
     * @param journalEntryId - The reference to the specific balance history record
    */
    retrieveBalanceHistoryDetail(oneAccountId: string, journalEntryId: string) {
        return this._http.get(this._cub_dataService.apiServerUrl +
            '/OneAccount/GetBalanceHistoryDetail?oneAccountId=' + oneAccountId + '&journalEntryId=' + journalEntryId)
            .toPromise();
    }

    getAddresses(accountId: string) {
        return this._http.get<any>(this._cub_dataService.apiServerUrl +
            '/OneAccount/GetAddressForDropdownList?accountId=' + accountId)
            .toPromise()
            .then(response => this._cub_dataService.tryParse(response));
    }

    getStates(country: string = null) {
        var url = this._cub_dataService.apiServerUrl + '/OneAccount/GetStates';
        if (country != null) {
            url = url + "?countryAlpha2=" + country;
        }
        return this._http.get(url)
            .toPromise();
    }

    addFundingSource(accountID: string, cardNumber: string, cardExpiryMMYY: string, nameOnCard: string,
        billingAddressId: string, isNewAddress: boolean,
        address1: string, address2: string, city: string, state: string, stateId: string, country: string, countryId: string,
        zipcode: string, zipcodeId: string, setAsPrimary: boolean) {
        var params = {
            'accountID': accountID,
            'cardNumber': cardNumber,
            'cardExpiryMMYY': cardExpiryMMYY,
            'nameOnCard': nameOnCard,
            'billingAddressId': billingAddressId,
            'isNewAddress': isNewAddress,
            'address1': address1,
            'address2': address2,
            'city': city,
            'State': state,
            'StateId': stateId,
            'zipcode': zipcode,
            'zipcodeId': zipcodeId,
            'country': country,
            'countryId': countryId,
            'setAsPrimary': setAsPrimary,
            'userId': this._cub_dataService.currentUserId
        };

        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.post<any>(this._cub_dataService.apiServerUrl + '/CustomerFundingSource/AddFundingSource', JSON.stringify(params), { headers: headers })
            .toPromise();
    }

    updateFundingSource(customerId: string, fundingSourceId: string, pgCardId: string, cardExpiryMMYY: string, nameOnCard: string, isNewAddress: boolean,
        address1: string, address2: string, city: string, state: string, stateId: string, country: string, countryId: string,
        zipcode: string, zipcodeId: string, billingAddressId: string, setAsPrimary: boolean) {
        var params = {
            'customerId': customerId,
            'fundingSourceId': fundingSourceId,
            'pgCardId': pgCardId,
            'cardExpiryMMYY': cardExpiryMMYY,
            'nameOnCard': nameOnCard,
            'isNewAddress': isNewAddress,
            'address1': address1,
            'address2': address2,
            'city': city,
            'State': state,
            'StateId': stateId,
            'zipcode': zipcode,
            'zipcodeId': zipcodeId,
            'country': country,
            'countryId': countryId,
            'billingAddressId': billingAddressId,
            'setAsPrimary': setAsPrimary
        };
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.patch<any>(this._cub_dataService.apiServerUrl + '/CustomerFundingSource/CustomerFundingSourcePatchCC', JSON.stringify(params), { headers: headers })
            .toPromise();
    }

    checkZipPostalCode(zipcode: string, country: string) {
        var zipInfo = {
            state: '', //No need to pass this in
            code: zipcode,
            country: country
        }

        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.post(this._cub_dataService.apiServerUrl + '/CustomerRegistration/CheckZipPostalCode', JSON.stringify(zipInfo), { headers: headers })
            .toPromise();
    }

    /**
     * Retrieve One Account Details for the Balance History grid (to load purses)
     * @param customerId - One Account ID on the customer record
    */
    getOneAccountDetails(customerId: string): Promise<any> {
        if (!customerId) {
            return Promise.reject('No Customer id provided');
        }

        let params = new HttpParams()
            .set('customerId', customerId)
            .set('returnSubsystemAccountDetailedInfo', 'true');

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/OneAccount/GetOneAccountSummary', { params })
            .toPromise();
    }

    getCachedOneAccountSummary(customerId: string): Promise<any> {
        if (!customerId) {
            return Promise.reject('No Customer ID provided');
        }

        const cacheKey = this.getOneAccountSummaryCacheKey(customerId);
        return this._cache.retrieve(cacheKey, () => this.getOneAccountDetails(customerId));
    }

    refreshOneAccountSummary(customerId: string): Promise<any> {
        const cacheKey = this.getOneAccountSummaryCacheKey(customerId);
        return this._cache.refresh(cacheKey, () => this.getOneAccountDetails(customerId));
    }

    clearCachedOneAccountSummary(customerId: string) {
        const key = this.getOneAccountSummaryCacheKey(customerId);
        this._cache.remove(key);
    }

    cacheDelinkData(data) {
        this._cache.store(this.delinkDataKey, data);
    }

    getCachedDelinkData() {
        return this._cache.retrieve<any>(this.delinkDataKey)
            .then(data => {
                this._cache.remove(this.delinkDataKey);
                return data;
            });
    }

    /**
     * Sort tokens by status (Active, Closed, Suspended, Terminated)
     * and by most-recently used, then take the first result.
     * @param tokens array of WSSubsystemAccountToken or WSTransitAccountToken
     */
    getDisplayToken(tokens: any[]) {
        const activeTokens = orderBy(tokens,
            [t => t.status, t => t.tokenInfo.lastUseDTM],
            ['asc', 'desc']);
        return head(activeTokens);
    }

    getOneAccountSummaryCacheKey(customerId: string) {
        return 'one-account-summary-' + this._cub_dataService.stripGuid(customerId);
    }
}
