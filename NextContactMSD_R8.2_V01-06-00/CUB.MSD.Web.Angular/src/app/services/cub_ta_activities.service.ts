import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

export interface TaActivitiesSearchCriteria{
    transitAccountId: string,
    subsystemId: string,
    customerId?: string,
    contactId?: string,
    startDateTime?: string,
    endDateTime?: string,
    type?: string,
    activitySubject: string,
    activityCategory: string,
    channel?: string,
    origin: string,
    globalTxnId: string,
    userName?: string,
    sortBy?: string,
    offset?: number,
    limit?: number,
    crmSessionId?: string
}

@Injectable()
export class CubTaActivitiesService {

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getActivities(queryParams: TaActivitiesSearchCriteria) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/TransitAccount/GetTASubsystemActivities', { params: params })
            .toPromise();
    }
}
