﻿import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';
import * as _ from 'lodash';
import * as moment from 'moment';

export interface TATravelTokenDisplay {
    tokenType: string,
    tokenNickname: string,
    lastUseDTM: Date | string,
    maskedPAN: string,
    cardExpiryMMYY: string,
    serialNumber: string,
    serializedMobileToken: string
}

export interface TAGPSCoordinate {
    longitude: number,
    latitude: number
}

export interface TATravelHistoryPresence {
    objectType: string,
    travelPresenceId: string,
    transactionDateTime: string,
    type: string,
    typeDescription: string,
    locationDescription: string,
    operator: string,
    operatorDescription: string,
    token: TATravelTokenDisplay,
    status: string,
    statusDescription: string,
    generatedCause: string,
    generatedCauseDescription: string,
    travelPresenceIndicator: string,
    isCorrectable: boolean,
    //WSTransitTravelPresence
    deviceId: string,
    transactionDescription: string,
    transactionStatus: string,
    transactionStatusDescription: string,
    stopPoint: string,
    routeNumber: string,
    zone: string,
    direction: string,
    sector: string,
    serviceType: string,
    gpsCoordinate: TAGPSCoordinate

}

export interface TATravelPaymentSource {
    id: number,
    dateTime: Date | string,
    amount: number,
    paymentSourceType: string,
    paymentType: string,
    paymentTypeDescription: string,
    paymentReferenceNumber: string,
    authReferenceNumber: string,
    fundingSourceInfo: string
}

export interface TATravelPayment {
    id: number,
    paymentDateTime: Date | string,
    paidFare: number,
    productDescription: string,
    payingSystemDescription: string,
    purseDescription: string,
    paymentSource: TATravelPaymentSource[]
}


export interface TATravelHistoryDetail {
    tripId: string,
    startDateTime: Date | string,
    endDateTime: Date | string,
    startLocationDescription: string,
    endLocationDescription: string,
    travelMode: string,
    travelModeDescription: string,
    token: TATravelTokenDisplay,
    productDescription: string,
    totalFare: number,
    unpaidFare: number,
    tripType: string,
    tripTypeDescription: string,
    tripStatus: string,
    tripStatusDescription: string,
    tripStatusDateTime: string,
    travelPresenceIndicator: string[],
    isCorrectable: boolean
}


//Used to call service for TA travel history detail
@Injectable()
export class Cub_TA_TravelHistoryDetailService {
    public travelHistoryDetailResults: Array<Object> = [];
    public selectedTravelHistory: Object = {};

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getTravelHistoryDetail(transitaccountId: string, subsystemId: string, tripId: string) {
        return this._http.get<any>(this._cub_dataService.apiServerUrl +
            '/TransitAccount/GetTravelHistoryDetail?transitaccountId=' + transitaccountId + '&subsystemId=' + subsystemId + '&tripId=' + tripId)
            .toPromise();

    }
}
