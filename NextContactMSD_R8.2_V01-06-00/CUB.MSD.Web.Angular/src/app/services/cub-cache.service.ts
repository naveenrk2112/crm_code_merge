﻿import { Injectable } from '@angular/core';

import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';

import { Cub_DataService } from './cub_data.service';

@Injectable()
export class Cub_CacheService {

    constructor(private _cub_dataService: Cub_DataService) {
        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this._expiration = +globals['CUB.MSD.Web.Angular']['CacheExpirationLimit'];
                this._prefix = globals['CUB.MSD.Web.Angular']['CacheName'] + ':';
                this._timeout = +globals['CUB.MSD.Web.Angular']['CacheTimeout'];
            });

        // Note: for whatever reason, placing this in the constructor causes it
        // to be called only once among multiple Angular instances.
        this.prune();
    }

    private _expiration: number = 5;
    private _prefix: string = 'cub_cache:';
    private _timeout: number = 10000;

    /**
     * Get a value from the cache. The value is returned immediately if it is
     * present in the cache. If it is not cached or has expired, get the value
     * by invoking the request function argument. If it appears that something
     * is currently caching the value, wait to be notified when it is ready.
     * @param key {string}
     * @param request {function} used when value has expired or is not cached;
     *      should resolve to the requested value
     * @returns a Promise that resolves to the requested data value
     */
    retrieve<T>(key: string, request?: () => Promise<T>): Promise<T> {
        const cacheValue = this._get(key);

        if (cacheValue == null || cacheValue.isStale(this._expiration)) {
            if (request == null) {
                this.remove(key);
                return Promise.resolve(null);
            }
            return this.makeRequest(key, request);
        }
        else if (cacheValue.isPlaceholder()) {
            return this.awaitValue<T>(key);
        }
        else {
            return Promise.resolve<T>(cacheValue.data);
        }
    }

    refresh<T>(key: string, request?: () => Promise<T>): Promise<T> {
        const cacheValue = this._get(key);

        if (cacheValue && cacheValue.isPlaceholder()) {
            return this.awaitValue<T>(key);
        }
        if (request == null) {
            this.remove(key);
            return Promise.resolve(null);
        }
        return this.makeRequest(key, request);
    }

    /**
     * Cache a temporary placeholder value for the given key and invoke the
     * provided request function to receive and cache the latest data value.
     * @param key {string}
     * @param request {function} should resolve to the requested value
     * @returns a Promise that resolves to the requested data value
     */
    private makeRequest<T>(key, request: () => Promise<T>): Promise<T> {
        this.storePlaceholder(key);
        return request()
            .then((data: T) => {
                if (data != null) {
                    this.store(key, data);
                } else {
                    this.remove(key);
                }
                return data;
            })
            .catch(err => {
                this.remove(key);
                return Promise.reject(err);
            });
    }

    /**
     * Listen for changes to the cache. Take the first event that matches the
     * key, and return it's new value.
     * @param key {string}
     * @returns a Promise that resolves to the requested data value
     */
    private awaitValue<T>(key: string): Promise<T> {
        let cacheKey = key;
        if (!key.startsWith(this._prefix)) {
            cacheKey = this._prefix + key;
        }
        return new Promise<T>((resolve, reject) => {
            Observable.fromEvent<StorageEvent>(window, 'storage')
                .first(event => event.key === cacheKey && !!event.oldValue && new CacheValue(event.oldValue).isPlaceholder())
                .timeout(this._timeout)
                .subscribe({
                    next: event => {
                        if (event.newValue == null) {
                            reject('Request failed in another component');
                            return;
                        }
                        const value = new CacheValue(event.newValue);
                        resolve(value.data as T);
                    },
                    error: err => reject(err)
                });
        });
    }

    /**
     * Retrieve a value from the cache synchronously if available, or null otherwise.
     * @param key
     */
    retrieveSync<T>(key: string) {
        let value = this._get(key);
        if (value == null) {
            return null;
        }
        if (value.isStale(this._expiration)) {
            this.remove(key);
            return null;
        }
        if (value.isPlaceholder()) {
            return null;
        }
        return value.data as T;
    }

    /**
     * Get a value from the cache.
     * @param key {string}
     * @returns an object containing the requested data and a timestamp of when
     *      the value was added
     */
    private _get(key: string): CacheValue {
        let cacheKey = key;
        if (!key.startsWith(this._prefix)) {
            cacheKey = this._prefix + key;
        }

        const cachedString = sessionStorage.getItem(cacheKey);
        if (!cachedString) {
            return null;
        }
        return new CacheValue(cachedString);
    }

    /**
     * Store a value to the cache. Passing no data argument equates to storing
     * a temporary placeholder value for the sake of alerting others that the
     * value is being retrieved.
     * @param key {string}
     * @param data {any} (Optional) the data to store. Do not use if caching
     *      a placeholder value
     */
    private _set(key: string, data?: any) {
        let cacheKey = key;
        if (!key.startsWith(this._prefix)) {
            cacheKey = this._prefix + key;
        }

        let value = new CacheValue();
        value.timestamp = moment();
        if (data != null) {
            value.data = data;
        }

        const serialized = value.jsonify();
        sessionStorage.setItem(cacheKey, serialized);
    }

    /**
     * Remove a cached value.
     * @param key {string}
     */
    remove(key: string) {
        let cacheKey = key;
        if (!key.startsWith(this._prefix)) {
            cacheKey = this._prefix + key;
        }
        sessionStorage.removeItem(cacheKey);
    }

    has(key: string) {
        const value = this._get(key);
        if (value == null || value.data == null) {
            return false;
        }
        if (value.isStale(this._expiration)) {
            this.remove(key);
            return false;
        }
        return true;
    }

    /**
     * Cache a temporary placeholder value when retrieving data externally to
     * be quickly updated with the full value once received.
     * @param key {string}
     */
    storePlaceholder(key: string) {
        this._set(key);
    }

    /**
     * Cache a value.
     * @param key {string}
     * @param data {any}
     */
    store(key: string, data: any) {
        this._set(key, data);
    }

    /**
     * Examine each cached value to determine if it is outdated, and remove any
     * old values.
     */
    prune() {
        const len = sessionStorage.length;
        for (let i = len - 1; i >= 0; i--) {
            const key = sessionStorage.key(i);
            if (key.startsWith(this._prefix)
                && this._get(key).isStale(this._expiration)
            ) {
                this.remove(key);
            }
        }
    }
}

class CacheValue {
    constructor(fromString?: string) {
        if (!fromString) {
            return;
        }

        const parsedValue = JSON.parse(fromString);
        if (!parsedValue.timestamp) {
            return;
        }
        this._timestamp = moment(parsedValue.timestamp);
        if (parsedValue.data != null) {
            this.data = parsedValue.data;
        }
    }

    data: any;
    private _timestamp: moment.Moment;
    set timestamp(value: any) {
        this._timestamp = moment(value);
    }
    get timestamp() {
        return this._timestamp.toISOString();
    }


    /**
     * Determine if this cached value is a temporary placeholder set elsewhere
     * that will momentarily be updated to hold the requested data value.
     * @returns true or false
     */
    isPlaceholder(): boolean {
        return this.data == null;
    }

    /**
     * Determine if this cached value is outdated by comparing its timestamp to
     * the current time.
     * @returns true or false
     */
    isStale(limit: number): boolean {
        const now = moment();
        const expiration = this._timestamp.add(limit, 'minutes');
        return now.isSameOrAfter(expiration, 'seconds');
    }

    /**
     * Return a JSON string representation of this CacheValue
     * @returns a JSON string
     */
    jsonify(): string {
        let value: any = {
            timestamp: this.timestamp
        };
        if (this.data != null) {
            value.data = this.data;
        }
        return JSON.stringify(value);
    }
}