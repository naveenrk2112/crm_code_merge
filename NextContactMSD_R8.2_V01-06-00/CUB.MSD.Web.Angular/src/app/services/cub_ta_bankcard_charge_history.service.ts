import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

export interface BankCardChargeHistorySearchCriteria {
    subSystem: string,
    accountId: string,
    startDateTime?: string,
    endDateTime?: string,
    dateType?: string,
    status?: string,
    response?: string,
    sortBy?: string,
    offset?: number,
    limit?: number
}

export interface BankcardChargeAggregationSearchCriteria {
    subSystem: string,
    accountId: string,
    paymentId: string,
}


@Injectable()
export class CubTaBankcardChargeHistoryService {

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getBankCardChargesHistory(queryParams: BankCardChargeHistorySearchCriteria) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }
        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/TransitAccount/getBankCardChargeHistory', { params: params })
            .toPromise();
    }

    getBankcardChargeAggregation(queryParams: BankcardChargeAggregationSearchCriteria) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }
        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/TransitAccount/getBankChargeAggregationDetails', { params: params })
            .toPromise();
    }
}
