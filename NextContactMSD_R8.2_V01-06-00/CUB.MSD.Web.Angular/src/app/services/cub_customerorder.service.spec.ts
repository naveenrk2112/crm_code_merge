﻿import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Cub_CustomerOrderService } from './cub_customerorder.service';
import { Cub_CacheService } from './cub-cache.service';
import { Cub_DataService } from './cub_data.service';
import { WSGetCustomerOrderHistoryResponse } from '../Model';

import { Cub_CacheServiceStub } from '../testutilities/cub-cache.service.stub';
import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';

import { ResolveBalanceArgs, WSDebtCollectResponse } from '../model';

import { keys } from 'lodash';

describe('Cub_CustomerOrderService', () => {
    const CUSTOMER_ID = '00000000-0000-0000-0000-000000000001';
    let service: Cub_CustomerOrderService;
    let dataService: Cub_DataService;
    let cacheService: Cub_CacheService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                Cub_CustomerOrderService,
                { provide: Cub_CacheService, useClass: Cub_CacheServiceStub },
                { provide: Cub_DataService, useClass: Cub_DataServiceStub }
            ]
        });

        service = TestBed.get(Cub_CustomerOrderService);
        cacheService = TestBed.get(Cub_CacheService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
        http.verify();
    });

    describe('#getOrderHistory', () => {
        it('should reject if customerId is null', fakeAsync(() => {
            let result, error;
            service.getOrderHistory(null)
                .then(data => result = data)
                .catch(err => error = err);
            tick();
            expect(result).toBeUndefined();
            expect(error).toBeDefined();
        }));

        it('should reject if response indicates error', fakeAsync(() => {
            let result, error;
            service.getOrderHistory(CUSTOMER_ID)
                .then(data => result = data)
                .catch(err => error = err);
            http.expectOne(req => !!req.urlWithParams.match('/CustomerOrder/GetOrderHistory'))
                .flush(null, { status: 500, statusText: 'TEST_SERVER_ERROR' });
            tick();
            expect(result).toBeUndefined();
            expect(error).toBeDefined();
        }));

        it('should resolve to an object containing a list of order history records and a total count', async(() => {
            const RESPONSE: WSGetCustomerOrderHistoryResponse = {
                hdr: {
                    result: 'Successful'
                },
                orders: [
                    {
                        orderId: 1,
                        orderDateTime: 'fake date',
                        orderType: 'type',
                        orderTypeDescription: 'type descript',
                        orderStatus: 'status',
                        orderStatusDescription: 'status descript',
                        orderTotalAmount: 100,
                        paymentStatus: 'payment status',
                        paymentStatusDescription: 'payment status descript'
                    }, {
                        orderId: 2,
                        orderDateTime: 'fake date',
                        orderType: 'type',
                        orderTypeDescription: 'type descript',
                        orderStatus: 'status',
                        orderStatusDescription: 'status descript',
                        orderTotalAmount: 200,
                        paymentStatus: 'payment status',
                        paymentStatusDescription: 'payment status descript'
                    }
                ],
                totalCount: 2
            };

            service.getOrderHistory(CUSTOMER_ID)
                .then(data => expect(data).toEqual(RESPONSE));
            http.expectOne(req => !!req.urlWithParams.match('/CustomerOrder/GetOrderHistory'))
                .flush(RESPONSE);
        }));
    });

    describe('#resolveBalance', () => {
        let REQUEST_BODY: ResolveBalanceArgs = {
            subsystem: 'FAKE',
            subsystemAccountReference: '110000000001',
        };
        let RESPONSE: WSDebtCollectResponse = {
            orderId: '1234',
            responseCode: 'Completed',
            paymentResults: []
        };

        afterEach(() => cacheService.prune());

        it('should return a debt collect response object on success', fakeAsync(() => {
            let result;
            service.resolveBalance(REQUEST_BODY)
                .then(data => result = data);
            tick();
            http.expectOne(req => !!req.url.match('/CustomerOrder/GetDebtCollection'))
                .flush({ Body: JSON.stringify(RESPONSE) });
            tick();
            expect(result).toEqual(RESPONSE);
        }));

        it('should reject on HTTP failure', fakeAsync(() => {
            let result;
            let error;
            service.resolveBalance(REQUEST_BODY)
                .then(data => result = data)
                .catch(err => error = err);
            tick();
            http.expectOne(req => !!req.url.match('/CustomerOrder/GetDebtCollection'))
                .flush(null, { status: 500, statusText: 'TEST_SERVER_ERROR' });
            tick();
            expect(result).toBeUndefined('Resolve Balance should have failed');
            expect(error).toBeDefined();
        }));

        it('should cache a temp unique ID on first request', fakeAsync(() => {
            let request = REQUEST_BODY;
            delete request.clientRefId;
            expect(request.clientRefId).toBeUndefined();
            service.resolveBalance(request)
                .then(data => { });
            tick();
            http.expectOne(req => {
                return !!req.url.match('/CustomerOrder/GetDebtCollection')
                    && req.body != null
                    && req.body['clientRefId'] != null;
            }).flush({ Body: JSON.stringify(RESPONSE) });
            tick();
            expect(keys(Cub_CacheServiceStub._storage).length).toBe(1, 'no temp unique ID cached!');
        }));

        it('should use the cached temp unique ID on successive requests', fakeAsync(() => {
            let requestId, secondRequestId;
            service.resolveBalance(REQUEST_BODY)
                .then(data => { });
            tick();

            http.expectOne(req => {
                if (!!req.url.match('/CustomerOrder/GetDebtCollection')
                    && req.body != null
                    && req.body['clientRefId']) {
                    requestId = req.body['clientRefId'];
                    return true;
                }
                return false;
            }).flush({ Body: JSON.stringify(RESPONSE) });
            tick();

            service.resolveBalance(REQUEST_BODY)
                .then(data => { });
            tick();

            http.expectOne(req => {
                if (!!req.url.match('/CustomerOrder/GetDebtCollection')
                    && req.body != null
                    && req.body['clientRefId']) {
                    secondRequestId = req.body['clientRefId'];
                    return true;
                }
                return false;
            }).flush({ Body: JSON.stringify(RESPONSE) });
            tick();

            expect(secondRequestId).toEqual(requestId);
        }));
    });
});