﻿import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Cub_MasterSearchService } from './cub_mastersearch.service';
import { Cub_DataService } from './cub_data.service';
import { Cub_CacheService } from './cub-cache.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_CacheServiceStub } from '../testutilities/cub-cache.service.stub';

describe('Cub_MasterSearchService', () => {
    let service: Cub_MasterSearchService;
    let dataService: Cub_DataService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                Cub_MasterSearchService,
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: Cub_CacheService, useClass: Cub_CacheServiceStub }
            ]
        });

        service = TestBed.get(Cub_MasterSearchService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => http.verify());

    it('#getAccountTypes should return an array of values', async(() => {
        const ACCOUNT_TYPES = [
            {
                Key: '00000000-0000-0000-0000-000000000001',
                Value: 'Individual'
            }, {
                Key: '00000000-0000-0000-0000-000000000002',
                Value: 'Group'
            }
        ];
        service.getAccountTypes()
            .then(data => expect(data).toEqual(ACCOUNT_TYPES));
        http.expectOne(req => !!req.urlWithParams.match(/CustomerRegistration\/GetAccountTypes/))
            .flush(ACCOUNT_TYPES);
    }));

    it('#getTokenTypes should resolve to an array of values', async(() => {
        const TOKEN_TYPES = {
            mappings: [
                { some: 'thing', goes: 'here' },
                { and: 'here', or: 'there' }
            ]
        };
        service.getTokenTypes()
            .then(data => {
                expect(data).toEqual(TOKEN_TYPES.mappings);
            });
        http.expectOne(req => !!req.url.match('/SubSystem/GetTokenTypeMapping'))
            .flush(TOKEN_TYPES);
    }));

    it('#searchCustomer should return an array of search results based on fields in Cub_DataService', async(() => {
        const SEARCH_RESULTS = [
            {
                ContactId: '00000000-0000-0000-0000-000000000001',
                FirstName: 'Tom',
                LastName: 'Fake',
                Email: '',
                Phone1: '',
                Phone2: '',
                Phone3: '',
                Match: 1
            }, {
                ContactId: '00000000-0000-0000-0000-000000000002',
                FirstName: 'Jerry',
                LastName: 'Fake',
                Email: '',
                Phone1: '',
                Phone2: '',
                Phone3: '',
                Match: 1
            }
        ];
        dataService.searchValues = {
            firstName: '*',
            lastName: 'Fake',
            email: '',
            phone: ''
        };

        service.searchCustomer()
            .then(data => {
                expect(dataService.masterSearchContactResults).toEqual(SEARCH_RESULTS);
                expect(data).toEqual(SEARCH_RESULTS);
            });
        http.expectOne(req => {
            let body = JSON.parse(req.body);
            return req.method === 'POST'
                && !!req.url.match('/MasterSearch/SearchContact')
                && body.contactSearchData.FirstName === dataService.searchValues['firstName']
                && body.contactSearchData.LastName === dataService.searchValues['lastName']
                && body.contactSearchData.Email === dataService.searchValues['email']
                && body.contactSearchData.Phone === dataService.searchValues['phone'];
        }).flush(SEARCH_RESULTS);
    }));

    it('#searchToken should resolve to an object containing search results based on fields in Cub_DataService', async(() => {
        const SEARCH_RESULTS = {
            searchResults: [{ some: 'data', goes: 'here' }],
            totalCount: 1
        };
        dataService.searchValues = {
            expirationDate: '11/22',
            subSystem: 'ABP',
            tokenType: 'Bankcard',
            bankCardNumber: '1234-5678-9012-3456'
        };

        service.searchToken()
            .then(data => expect(data).toEqual(SEARCH_RESULTS));
        http.expectOne(req => {
            let body = JSON.parse(req.body);
            return !!req.url.match('/MasterSearch/SearchCard')
                && body.model.SubSystem === dataService.searchValues['subSystem']
                && body.model.TokenType === dataService.searchValues['tokenType']
                && body.model.BankCardNumber === dataService.searchValues['bankCardNumber']
                && body.model.ExpirationMonth === dataService.searchValues['expirationDate'].slice(0, 2)
                && body.model.ExpirationYear === dataService.searchValues['expirationDate'].slice(3);
        }).flush(SEARCH_RESULTS);
    }));
});