﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Cub_DataService } from './cub_data.service';
import { MvcGenericResponse, TapsHistorySearchCriteria, WSCubTransitAccount } from '../model';

export interface TransitAccountOrderHistorySearchCriteria {
    userId: string,
    transitAccountId: string,
    subSystemId: string,
    startDateTime?: string,
    endDateTime?: string,
    orderType?: string,
    orderStatus?: string,
    paymentStatus?: string,
    orderId?: string,
    realtimeResults?: boolean,
    userName?: string,
    sortBy?: string,
    offset?: number,
    limit?: number
}

@Injectable()
export class Cub_TransitAccountService {
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getActivities(transitAccountId: string) {
        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/TransitAccount/GetActivities?transitAccountid=' + transitAccountId)
            .toPromise();
    }

    /**
     * HTTP request to web service to retrieve an MSD Transit Account record
     * @param transitAccountId
     * @param customerId should be null if Transit Account is not linked to a OneAccount
     */
    getTransitAccount(transitAccountId: string, subsystemId: string, customerId?: string) {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        const body = {
            transitAccountId: transitAccountId,
            subsystemId: subsystemId,
            customerId: customerId || '',
            userId: this._cub_dataService.currentUserId
        };
        return this._http.post<WSCubTransitAccount>(this._cub_dataService.apiServerUrl + '/TransitAccount/GetTransitAccount', body, { headers })
            .toPromise();
    }

    getTravelModes(subsystem: string): Promise<any[]> {
        const url = `${this._cub_dataService.apiServerUrl}/TransitAccount/GetTravelModes?subsystem=${subsystem}`;
        return this._http.get<MvcGenericResponse>(url)
            .toPromise()
            .then(response => this._cub_dataService.tryParse(response.Body))
            .then(data => data.travelModes);
    }

    getTravelOperators(subsystem: string): Promise<any[]> {
        const url = `${this._cub_dataService.apiServerUrl}/TransitAccount/GetTravelOperators?subsystem=${subsystem}`;
        return this._http.get<MvcGenericResponse>(url)
            .toPromise()
            .then(response => this._cub_dataService.tryParse(response.Body))
            .then(data => data.operators);
    }

    getTapTypes(subsystem: string): Promise<any[]> {
        const url = `${this._cub_dataService.apiServerUrl}/TransitAccount/GetTapTypes?subsystem=${subsystem}`;
        return this._http.get<MvcGenericResponse>(url)
            .toPromise()
            .then(response => this._cub_dataService.tryParse(response.Body))
            .then(data => data.tapTypes);
    }

    getTapStatuses(subsystem: string): Promise<any[]> {
        const url = `${this._cub_dataService.apiServerUrl}/TransitAccount/GetTapStatuses?subsystem=${subsystem}`;
        return this._http.get<MvcGenericResponse>(url)
            .toPromise()
            .then(response => this._cub_dataService.tryParse(response.Body))
            .then(data => data.tapStatuses);
    }

    getOrderHistory(queryParams: TransitAccountOrderHistorySearchCriteria) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }
        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/TransitAccount/GetOrderHistory', { params: params })
            .toPromise();
    }

    getTapsHistory(queryParams: TapsHistorySearchCriteria) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/TransitAccount/GetTravelHistoryTaps', { params: params })
            .toPromise()
            .then((results) => {
                var body = this._cub_dataService.tryParse(results.Body);
                const len = body.lineItems.length;
                for (let i = 0; i < len; i++) {
                    const token = body.lineItems[i].token;
                    body.lineItems[i].tokenType = token.tokenType;
                    body.lineItems[i].token = token.maskedPAN || token.serialNumber || token.serializedMobileToken || '';
                }
                return Promise.resolve(body);
            });
    }
}