import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

@Injectable()
export class Tam_TransactionsService {

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getTransactions(customerId: string) {
        return this._http.get(this._cub_dataService.apiServerUrl +
            '/Tolling/GetTransactionHistory?customerId=' + customerId)
            .toPromise();
    }
}
