﻿import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

export interface CreditCardReference {
    cardExpiryMMYY: string,
    creditCardType: string,
    maskedPan: string,
    nameOnCard: string,
    pgCardId: string
}

export interface DirectDebitReference {
    nameOnAccount: string,
    accountType: string
    financialInstitutionName: string
}

export interface FundingSourceExt {
    billingAddressId: string,
    creditCard: CreditCardReference,
    directDebit: DirectDebitReference,
    fundingSourceId: number,
    setAsPrimary: boolean,
    status: string,
    statusDescription: string,
    billingAddress: string
}

export interface FundingSourceList {
    fundingSource: FundingSourceExt[]
}

@Injectable()
export class Cub_FundingSourceService {
    public fundingSourceResults: Array<Object> = [];
    public selectedFundingSource: Object = {};

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getFundingSources(accountId: string) {
        return this._http.get(this._cub_dataService.apiServerUrl +
            '/CustomerFundingSource/CustomerFundingSourceGet?customerId=' + accountId)
            .toPromise();
    }

    getFundingSourceById(accountId: string, fundingSourceId: string) {
        return this._http.get(this._cub_dataService.apiServerUrl +
            '/CustomerFundingSource/CustomerFundingSourceGetById?customerId=' + accountId + "&fundingSourceId=" + fundingSourceId)
            .toPromise();
    }

    deleteFundingSource(accountId: string, fundingSourceId: string) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const params = new HttpParams()
            .set('customerId', accountId)
            .set('fundingsourceId', fundingSourceId)
            .set('userId', this._cub_dataService.currentUserId);

        return this._http.delete(this._cub_dataService.apiServerUrl +
            '/CustomerFundingSource/CustomerFundingSourceDelete',
            { headers, params })
            .toPromise()
            .then((data: any) => {
                if (data) {
                    let errorMessage = this._cub_dataService.tryParse(data.errorMessage)
                    return Promise.reject(errorMessage);
                }
            });
    }

    setPrimaryFundingSource(accountId: string, fundingSourceId: string, billingAddressId: string) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const body = {
            'customerId': accountId,
            'fundingsourceId': fundingSourceId,
            'billingAddressId': billingAddressId,
            'userId': this._cub_dataService.currentUserId
        };
        return this._http.patch(this._cub_dataService.apiServerUrl +
            '/CustomerFundingSource/CustomerFundingSourceSetAsPrimary',
            JSON.stringify(body),
            { headers: headers })
            .toPromise();
    }

    updateFundingSource(customerId: string, fundingSourceId: string, pgCardId: string, cardExpiryMMYY: string, nameOnCard: string, isNewAddress: boolean,
        address1: string, address2: string, city: string, state: string, stateId: string, country: string, countryId: string,
        zipcode: string, zipcodeId: string, billingAddressId: string, billingAddress: string, setAsPrimary: boolean) {
        var params = {
            'customerId': customerId,
            'fundingSourceId': fundingSourceId,
            'pgCardId': pgCardId,
            'cardExpiryMMYY': cardExpiryMMYY,
            'nameOnCard': nameOnCard,
            'isNewAddress': isNewAddress,
            'address1': address1,
            'address2': address2,
            'city': city,
            'State': state,
            'StateId': stateId,
            'zipcode': zipcode,
            'zipcodeId': zipcodeId,
            'country': country,
            'countryId': countryId,
            'billingAddressId': billingAddressId,
            'billingAddress': billingAddress,
            'setAsPrimary': setAsPrimary,
            'userId': this._cub_dataService.currentUserId
        };

        var parms = JSON.stringify(params);

        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.patch(this._cub_dataService.apiServerUrl + '/CustomerFundingSource/CustomerFundingSourcePatchCC', JSON.stringify(params),
            { headers: headers })
            .toPromise();
    }
}
