﻿import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Cub_UpdateAddressService } from './cub_updateaddress.service';
import { Cub_DataService } from './cub_data.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';

import { WSAddress } from '../model';

describe('Cub_UpdateAddressService', () => {
    const CUSTOMER_ID = '00000000-0000-0000-0000-000000000001';
    const ADDRESS: WSAddress = {
        address1: '1600 Pennsylvania Ave.',
        address2: '',
        city: 'Washington',
        stateId: '00000000-0000-0000-0000-000000000004',
        postalCodeId: '00000000-0000-0000-0000-000000000005',
        countryId: '00000000-0000-0000-0000-000000000006',
        addressId: '00000000-0000-0000-0000-000000000008'
    };
    const CONTACTS = [
        '00000000-0000-0000-0000-000000000002',
        '00000000-0000-0000-0000-000000000003'
    ];
    const FUNDING_SOURCES = [
        '1234',
        '5678'
    ];

    let service: Cub_UpdateAddressService;
    let dataService: Cub_DataService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                Cub_UpdateAddressService,
                { provide: Cub_DataService, useClass: Cub_DataServiceStub }
            ]
        });

        service = TestBed.get(Cub_UpdateAddressService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => http.verify());
    
    it('#update should post to parent on success', fakeAsync(() => {
        const RESPONSE = {
            Success: true
        };
        spyOn(service, 'postToParent');
        service.update(CUSTOMER_ID, ADDRESS, CONTACTS, FUNDING_SOURCES);
        http.expectOne(req => {
            let body = req.body;
            return !!req.url.match('/CustomerAddress/AddressUpdate')
                && body.customerId === CUSTOMER_ID
                && body.addressId === ADDRESS.addressId
                && body.requestBody.address === ADDRESS
                && body.requestBody.contactsUpdated === CONTACTS
                && body.requestBody.fundingSourcesUpdated === FUNDING_SOURCES;
        }).flush(RESPONSE);
        tick();
        expect(service.postToParent).toHaveBeenCalledWith({ action: 'success' });
    }));

    it('#update should reject if the response indicates error', fakeAsync(() => {
        const RESPONSE = 'TEST ERROR';
        spyOn(service, 'postToParent').and.callFake(data => {
            expect(data.action).toEqual('error');
            expect(data.error).toBeDefined();
        });

        service.update(CUSTOMER_ID, ADDRESS, CONTACTS, FUNDING_SOURCES);
        http.expectOne(req => !!req.url.match('/CustomerAddress/AddressUpdate'))
            .flush(RESPONSE, { status: 500, statusText: 'ERROR' });
        tick();
        expect(service.postToParent).toHaveBeenCalled();
    }));
});