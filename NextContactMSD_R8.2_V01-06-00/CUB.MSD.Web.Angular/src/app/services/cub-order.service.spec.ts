import { TestBed, inject } from '@angular/core/testing';

import { CubOrderService } from './cub-order.service';

describe('CubOrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CubOrderService]
    });
  });

  it('should be created', inject([CubOrderService], (service: CubOrderService) => {
    expect(service).toBeTruthy();
  }));
});
