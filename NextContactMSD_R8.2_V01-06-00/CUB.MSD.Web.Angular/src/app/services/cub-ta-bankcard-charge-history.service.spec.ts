import { TestBed, inject } from '@angular/core/testing';

import { CubTaBankcardChargeHistoryService } from './cub_ta_bankcard_charge_history.service';

describe('CubTaBankcardChargeHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CubTaBankcardChargeHistoryService]
    });
  });

  it('should be created', inject([CubTaBankcardChargeHistoryService], (service: CubTaBankcardChargeHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
