﻿import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Cub_CustomerSearchService } from './cub_customersearch.service';
import { Cub_DataService } from './cub_data.service';

import { Cub_DataServiceStub} from '../testutilities/cub_data.service.stub';

describe('Cub_CustomerSearchService', () => {
    let service: Cub_CustomerSearchService;
    let dataService: Cub_DataService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                Cub_CustomerSearchService,
                { provide: Cub_DataService, useClass: Cub_DataServiceStub }
            ]
        })

        service = TestBed.get(Cub_CustomerSearchService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => http.verify());

    it('#searchCustomer should return an array of objects', async(() => {
        const RESULTS = [
            {
                ContactId: '00000000-0000-0000-0000-000000000001',
                FirstName: 'Johnny',
                LastName: 'Bravo',
                Email: '',
                Phone1: '',
                Phone2: '',
                Phone3: '',
                Match: 0
            }, {
                ContactId: '00000000-0000-0000-0000-000000000002',
                FirstName: 'Timmy',
                LastName: 'Turner',
                Email: '',
                Phone1: '',
                Phone2: '',
                Phone3: '',
                Match: 0
            }
        ];
        let result;
        service.searchCustomer()
            .then(data => {
                expect(data).toEqual(RESULTS);
            });
        let request = http.expectOne(dataService.apiServerUrl + '/CustomerRegistration/SearchContact');
        request.flush(JSON.stringify(RESULTS), {
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }));
});