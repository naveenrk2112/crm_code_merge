﻿import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';
import * as Model from '../model';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as uuid from 'uuid/v4';
@Injectable()
export class Cub_NotificationService {
    public customerId: string;
    public customerNotificationPreferenceResults: Array<Object> = [];
    public selectedDetail: Model.NotificationDetail;
    public customerNotificationResults: Model.NotificationRecord[] = [];
    public selected: Model.NotificationRecord;
    public resendContactId: string;
    public totalRecordCount: number;

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    /**
     * Gets customer notification prefrences for a given customer, contact and channel (channel is optional)
     * @param customerId Customer (accountid) to retrieve notification preferences for 
     * @param contactId Contact to retrieve notification preferences for
     * @param channel (Optional) adds ability to filter channels down.
     */
    getCustomerNotificationPreferences(customerId: string, contactId: string, channel: string) {
        var params = new HttpParams()
            //.set('ContactType', this._cub_dataservice.searchValues["contactType"])
            .set('customerId', customerId)
            .set('contactId', contactId)
            .set('channel', channel);

        return this._http.get<object[]>(`${this._cub_dataService.apiServerUrl}/CustomerNotification/GetCustomerNotificationPreferences`, { params: params })
            .toPromise()
            .then(response => response || [])
            .then(data => {
                this.customerNotificationPreferenceResults = data;
                return data;
            });
    }

    /**
     * Gets all enabled notification channels for this organization.
     * @returns a Promise to resolve to an array of Channels
     */
    getEnabledNotificationChannels(): Promise<Model.Channel[]> {
        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/CustomerNotification/GetConfigNotificationChannel?enabled=true`)
            .toPromise()
            .then(data => data.channels);
    }

    /**
     * Function used to get all related contacts for a customerid.
     * @param customerId customerId (accountId) on which to retrieve data.
     * @returns a Promise to resolve to an array of Contacts
     */
    getContactsRelatedToCustomer(customerId: string): Promise<Model.Contact[]> {
        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/CustomerNotification/GetCustomerContacts?customerId=${customerId}`)
            .toPromise();
    }

    /**
     * Gets all notification records fitting the search criteria provided.
     * @param params search filter criteria passed as the request body
     * @returns a Promise to resolve to an array of Notifications
     */
    getCustomerNotifications(queryParams: Model.NotificationSearchCriteria): Promise<Model.NotificationRecord[]> {
        if (!this.customerId) {
            return Promise.reject('Cannot get customer notifications, no customer ID');
        }
        if (!queryParams) {
            return Promise.reject('Cannot get customer notifications, no query parameters');
        }

        let params = new HttpParams();
        for (let qp in queryParams) {
            params = params.set(qp, queryParams[qp]);
        }

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/CustomerNotification/GetCustomerNotification`, { headers, params })
            .toPromise()
            .then(data => {
                this.customerNotificationResults = data.notifications;
                this.totalRecordCount = data.totalCount;
                return data.notifications;
            });
    }

    /**
     * Gets the detail of the currently selected notification.
     * @returns a Promise to resolve to a NotificationDetail
     */
    getCustomerNotificationDetail(): Promise<Model.NotificationDetail> {
        if (!this.customerId) {
            return Promise.reject('Cannot get notification detail, no customer ID');
        }
        if (!this.selected) {
            return Promise.reject('Cannot get notification detail, no notification selected');
        }

        let params = new HttpParams()
            .set('customerId', this.customerId)
            .set('notificationId', this.selected.notificationId.toString());

        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/CustomerNotification/GetCustomerNotificationDetail`, { params })
            .toPromise()
            .then(data => {
                this.selectedDetail = {
                    alternateMessage: data.alternateMessage,
                    message: data.message,
                    contactId: data.contact.contactId
                };
                this.resendContactId = this.selectedDetail.contactId;
                return this.selectedDetail;
            });
    }

    updateNotificiationPreferencesForContact(requestData: Object) {
        var params = {
            'requestData': requestData,
            'userId': this._cub_dataService.currentUserId
        };

        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post(`${this._cub_dataService.apiServerUrl}/CustomerNotification/CustomerNotificationPreferencesPatch`, JSON.stringify(params), { headers: headers })
            .toPromise();
    }

    /**
     * Resends the currently selected notification along the specified channel
     * @param channel the notification channel along which to resend
     * @returns a Promise to resolve on success
     */
    resend(channel: string) {
        if (!this.customerId || !this.selected || !this.selectedDetail || !this.resendContactId || !channel) {
            return Promise.reject('Insufficient data');
        }

        if (!this.selected.allowResend) {
            return Promise.reject('You cannot resend this notification');
        }

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' }),
            params = {
                customerId: this.customerId,
                contactId: this.resendContactId,
                notificationId: this.selected.notificationId,
                notificationType: this.selected.type,
                allowReformat: true,
                channel: channel,
                clientRefId: this.resendContactId,
                userId: this._cub_dataService.currentUserId
            };

        return this._http.post<any>(`${this._cub_dataService.apiServerUrl}/CustomerNotification/CustomerNotificationResend`, JSON.stringify(params), { headers: headers })
            .toPromise()
            .then((data) => {
                if (data && !data.Success)
                {
                    if (Array.isArray(data.Errors))
                    {
                        var errObj = JSON.parse(data.Errors[0].errorMessage);
                        this._cub_dataService.onApplicationError(errObj.errorMessage);
                    }
                }
            });
    }

    /**
  * Resends customer notification
  * @param notfication the notification needs to be resend
  * @returns a Promise to resolve on success
  */
    resendCustomerNotification(notification: any) {
        if (!notification || !notification.customerId || !notification.contactId || !notification.channel) {
            return Promise.reject('Insufficient data');
        }

        if (!notification.allowResend) {
            return Promise.reject('You cannot resend this notification');
        }

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' }),
            params = {
                customerId: notification.customerId ,
                contactId: notification.contactId ,
                notificationId: notification.notificationId,
                notificationType: notification.type,
                allowReformat: true,
                channel: notification.channel,
                clientRefId: this.generateClientRefId(),
                userId: this._cub_dataService.currentUserId
            };

        return this._http.post<any>(`${this._cub_dataService.apiServerUrl}/CustomerNotification/CustomerNotificationResend`, JSON.stringify(params), { headers: headers })
            .toPromise();
    }

    /**
     * Gets all notification records fitting the search criteria provided.
     * @param params search filter criteria passed as the request body
     * @returns a Promise to resolve to an array of Notifications
     */
    getNotifications(queryParams: Model.NotificationsFilters) {
        if (!queryParams) {
            return Promise.reject('Cannot get notifications, no query parameters');
        }

        let params = new HttpParams();
        for (let qp in queryParams) {
            params = params.set(qp, queryParams[qp]);
        }

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/Notification/GetNotifications`, { headers, params })
            .toPromise();
    }

    /**
     * Gets notification details for given notification id.
     * @param params search filter criteria passed as the request body
     * @returns a Promise to resolve to an array of Notifications
     */
    getNotificationDetails(notificationId: string) {
        if (!notificationId) {
            return Promise.reject('No Notification Id provided');
        }

        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var params = new HttpParams()
            .set('notificationId', notificationId)

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Notification/GetNotificationDetails', { headers: headers, params: params })
            .toPromise();
    }

   
    resendNotifiation(notification: any, email: string, phone: string, channel: string) {
        if (!notification || !channel) {
            return Promise.reject('Insufficient data');
        }

        if (!notification.allowResend) {
            return Promise.reject('You cannot resend this notification');
        }

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' }),
            params = {
                notificationId: notification.notificationId,
                notificationType: notification.type,
                channels: channel,
                email: email,
                smsPhone: phone,
                userId: this._cub_dataService.currentUserId,
                clientRefId: this.generateClientRefId()
            };

        return this._http.post<any>(`${this._cub_dataService.apiServerUrl}/Order/ResendOrderNotification`, JSON.stringify(params), { headers: headers })
            .toPromise();
    }

    generateClientRefId() {
        return `MSD:${uuid()}`;
    }
}