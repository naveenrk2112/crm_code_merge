﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';
import { Cub_CacheService } from './cub-cache.service';

import { assign, map } from 'lodash'
import * as moment from 'moment';

@Injectable()
export class Cub_SubsystemService {
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService,
        private _cache: Cub_CacheService
    ) { }

    private readonly _debtCollectionDataKey = 'debtCollectionInfo';

    private getSubsystemStatusCacheKey(subsystemId: string, transitAccountId: string) {
        return `${subsystemId}-${transitAccountId}-status`;
    }

    /**
     * Calls the web controller to retrieve data for a given transit account and subsystem
     * @param accountId transit account ID
     * @param subSystem subsystem
     * @returns a promise that resolves to an object containing lists of balances, tokens, and passes
     */
    getSubsystemStatus(subsystemId: string, transitAccountId: string): Promise<any> {
        if (!subsystemId) {
            return Promise.reject('No subsystem ID');
        }
        if (!transitAccountId) {
            return Promise.reject('No transit account ID');
        }

        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/TransitAccount/GetAccountStatus?accountId=${transitAccountId}&subSystem=${subsystemId}`)
            .toPromise();
    }

    getCachedSubsystemStatus(subsystemId: string, transitAccountId: string): Promise<any> {
        if (!subsystemId) {
            return Promise.reject('No subsystem ID');
        }
        if (!transitAccountId) {
            return Promise.reject('No transit account ID');
        }

        const key = this.getSubsystemStatusCacheKey(subsystemId, transitAccountId);
        return this._cache.retrieve(key, () => this.getSubsystemStatus(subsystemId, transitAccountId));
    }

    refreshSubsystemStatus(subsystemId: string, transitAccountId: string): Promise<any> {
        const key = this.getSubsystemStatusCacheKey(subsystemId, transitAccountId);
        return this._cache.refresh(key, () => this.getSubsystemStatus(subsystemId, transitAccountId));
    }

    clearCachedSubsystemStatus(subsystemId: string, transitAccountId: string) {
        const key = this.getSubsystemStatusCacheKey(subsystemId, transitAccountId);
        this._cache.remove(key);
    }

    /**
     * This API is used to block the subsystem account.
     * @param subsystemId Unique identifier for the subsystem where the travel token is registered.
     * @param accountNumber Account number of the travel token associated within the given subsystem.
     * @param reasonCode Reason code for the blocking.
     * @param notes Notes for the adjustment.
     * @No data is returned by this method. Common headers and http response codes are returned.
     */
    blockSubsystemTransitAccount(subsystemId: string, accountNumber: string, reasonCode: string, notes: string ){
        if (!subsystemId) {
            return Promise.reject('No Subsystem Id');
        }
        if (!accountNumber) {
            return Promise.reject('No Account Number');
        }
        if (!reasonCode) {
            return Promise.reject('No Reason Code');
        }

        var body = {
            subsystemId: subsystemId,
            accountNumber: accountNumber,
            reasonCode: reasonCode,
            notes: notes
        }
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var url = `${this._cub_dataService.apiServerUrl}/SubSystem/BlockSubsystemTransitAccount`;
        return this._http.post<any>(url, JSON.stringify(body), { headers: headers })
            .toPromise();
    }

    /**
    * This API is used to unblock the subsystem account.
    * @param subsystemId Unique identifier for the subsystem where the travel token is registered.
    * @param accountNumber Account number of the travel token associated within the given subsystem.
    * @param reasonCode Reason code for the blocking.
    * @param notes Notes for the adjustment.
    * @No data is returned by this method. Common headers and http response codes are returned.
    */
    unblockSubsystemTransitAccount(subsystemId: string, accountNumber: string, reasonCode: string, notes: string) {
        if (!subsystemId) {
            return Promise.reject('No Subsystem Id');
        }
        if (!accountNumber) {
            return Promise.reject('No Account Number');
        }
        if (!reasonCode) {
            return Promise.reject('No Reason Code');
        }

        var body = {
            subsystemId: subsystemId,
            accountNumber: accountNumber,
            reasonCode: reasonCode,
            notes: notes
        }
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var url = `${this._cub_dataService.apiServerUrl}/SubSystem/UnblockSubsystemTransitAccount`;
        return this._http.post<any>(url, JSON.stringify(body), { headers: headers })
            .toPromise();
    }

    /**
 * This API is used to block the subsystem account.
 * @param subsystemId Unique identifier for the subsystem where the travel token is registered.
 * @param accountNumber Account number of the travel token associated within the given subsystem.
 * @param reasonCode Reason code for the blocking.
 * @param notes Notes for the adjustment.
 * @No data is returned by this method. Common headers and http response codes are returned.
 */
 tokenAction(transitAccountId: string, subsystemId: string, travelToken: string, customerId: string, reasonCode: string, notes: string, tokenAction: string) {
        if (!subsystemId) {
            return Promise.reject('No Subsystem Id');
        }
        if (!travelToken) {
            return Promise.reject('No Travel Token');
        }
        if (!reasonCode) {
            return Promise.reject('No Reason Code');
        }

        var body = {
            transitAccountId: transitAccountId,
            subsystemId: subsystemId,
            travelToken: travelToken,
            customerId: customerId,
            reasonCode: reasonCode,
            notes: notes,
            tokenAction: tokenAction
        }
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var url = `${this._cub_dataService.apiServerUrl}/SubSystem/TokenAction`;
        return this._http.post<any>(url, JSON.stringify(body), { headers: headers })
            .toPromise();
    }

    /**
     * Calls the web controller to retrieve cases related to subsystemids
     * @param subsystemId subsystem
     * @returns a promise that resolves to an object containing list of related cases that have same subsystemid
     */
    getCasesRelatedToSubsystemId(subsystemId: string): Promise<any> {
        if (!subsystemId) {
            return Promise.reject('No Subsystem ID');
        }
        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/SubSystem/GetCasesRelatedToSubsystemId?subsystemId=${subsystemId}`)
            .toPromise();
    }

    /**
     * Call the Subsystem.TravelTokenCanBeLinked action
     * @param subsystemId subsystem
     * @param bankCardNumber bank card number
     * @param expirationDate expiration date
     */
    OpenLoopToken(subsystemId: string, bankCardNumber: string, expirationDate: string) {
        bankCardNumber = bankCardNumber.replace(/\-/g, '')
        const splitDate = this._cub_dataService.searchValues["expirationDate"].split('/');
        let ExpirationMonth: string = splitDate[0]
        let ExpirationYear: string = splitDate[1];
        let this_ = this;

        return this._cub_dataService.encryptBankcard(bankCardNumber, ExpirationMonth, ExpirationYear)
            .then((response) => {
                var body = {
                    'tokenType': "Bankcard",
                    'subsystemId': subsystemId,
                    "encryptedToken": response.encryptedToken,
                    "nonce": response.nonce,
                    "autoCreateFlag": true
                };

                const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
                var url = `${this._cub_dataService.apiServerUrl}/SubSystem/TravelTokenCanBeLinked`;
                return this_._http.post<any>(url, JSON.stringify(body), { headers: headers })
                    .toPromise();
            });
    }

    cacheDebtCollectionData(data) {
        this._cache.store(this._debtCollectionDataKey, data);
    }

    getCachedDebtCollectionData() {
        return this._cache.retrieve(this._debtCollectionDataKey)
            .then(data => {
                this._cache.remove(this._debtCollectionDataKey);
                return data;
            });
    }

    getReasonCodes() {
        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/CustomerService/GetReasonCodes?active=true`)
            .toPromise();
    }

    GetContactNameEmailById(contactId) {
        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/Contact/GetContactNameEmailById?contactId=${contactId}`)
            .toPromise();
    }

    postOrderAdjustment(data: any) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post(this._cub_dataService.apiServerUrl + '/CustomerOrder/PostOrderAdjustment', data, { headers })
            .toPromise();
    }

    postTravelCorrection(data: any) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post(this._cub_dataService.apiServerUrl + '/CustomerOrder/PostTravelCorrection', data, { headers })
            .toPromise();
    }

    getlocations(subsystem, operator) {
        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/TransitAccount/GetLocations?subsystem=${subsystem}&serviceOperator=${operator}`)
            .toPromise();
    }
}