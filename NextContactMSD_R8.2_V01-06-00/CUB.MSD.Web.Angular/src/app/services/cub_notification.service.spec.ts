﻿import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Cub_NotificationService } from './cub_notification.service';
import { Cub_DataService } from './cub_data.service';
import { reduce } from 'lodash';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';

describe('Cub_NotificationService', () => {
    let service: Cub_NotificationService;
    let dataService: Cub_DataService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                Cub_NotificationService
            ]
        });

        service = TestBed.get(Cub_NotificationService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => http.verify());

    it('#getCustomerNotificationPreferences should return an array of objects', async(() => {
        const NOTIFICATION_PREFERENCES_DATA = [
            {
                "contactId": "320e7b9c-a12e-e711-80e6-005056814569",
                "preferences": [
                    {
                        "category": { "description": "Account management categories.", "name": "AccountManagement" },
                        "subcategory": { "description": "Alerts having to do with balance.", "name": "BalanceAlert" },
                        "channels": [
                            { "channel": "EMAIL", "enabled": true, "reqType": "DEFAULT" },
                            { "channel": "PUSHNOTIFICATION", "enabled": false, "reqType": "OPTIONAL" },
                            { "channel": "SMS", "enabled": false, "reqType": "OPTIONAL" }
                        ],
                        "notificationDescription": "Password Reset Verification",
                        "notificationType": "PasswordResetVerification",
                        "optIn": true
                    }
                ]
            }
        ];
        let customerId = '2B9634F0-3F29-E711-80EA-005056813E0D';
        let contactId = 'b5262c34-ce29-e711-80e6-005056814569';
        let channel = '';
        service.getCustomerNotificationPreferences(customerId, contactId, channel)
            .then(data => expect(data).toEqual(NOTIFICATION_PREFERENCES_DATA));
        http.expectOne(req => !!req.url.match('/CustomerNotification/GetCustomerNotificationPreferences'))
            .flush(NOTIFICATION_PREFERENCES_DATA);
    }));

    it('#getContactsRelatedToCustomer should return an array of contacts', async(() => {
        const CONTACTS = [
            {
                ContactId: '00000000-0000-0000-0000-000000000001',
                FirstName: 'Tom',
                LastName: 'Fake'
            }, {
                ContactId: '00000000-0000-0000-0000-000000000002',
                FirstName: 'Jerry',
                LastName: 'Fake'
            }
        ];
        let customerId = '2B9634F0-3F29-E711-80EA-005056813E0D';
        service.getContactsRelatedToCustomer(customerId)
            .then(data => expect(data).toEqual(CONTACTS));
        http.expectOne(req => !!req.url.match('/CustomerNotification/GetCustomerContacts'))
            .flush(CONTACTS);
    }));

    it('#getEnabledNotificationChannels should resolve to an array of channels', async(() => {
        const RESPONSE = {
            hdr: {
                result: 'Successful'
            },
            channels: [
                {
                    channel: 'EMAIL',
                    enabled: true
                }
            ]
        };
        service.getEnabledNotificationChannels()
            .then(data => expect(data).toEqual(RESPONSE.channels));
        http.expectOne(req => !!req.url.match('/CustomerNotification/GetConfigNotificationChannel'))
            .flush(RESPONSE);
    }));

    it('#getEnabledNotificationChannels should reject if the web service errors', async(() => {
        const RESPONSE = {
            hdr: {
                result: 'Error'
            }
        };

        // simulate web service failure
        service.getEnabledNotificationChannels()
            .then(data => fail('getEnabledNotificationChannels should have thrown a server error'))
            .catch(err => expect(err).toBeDefined());
        http.expectOne(req => !!req.url.match('/CustomerNotification/GetConfigNotificationChannel'))
            .flush(null, { status: 500, statusText: 'TEST_SERVER_ERROR' });
    }));

    describe('#getCustomerNotifications', () => {
        let PARAMS;

        beforeEach(() => {
            service.customerId = '00000000-0000-0000-0000-000000000001';
            PARAMS = {
                customerId: service.customerId
            };
        });

        it('should resolve to an array of notifications', async(() => {
            const RESPONSE = {
                hdr: {
                    result: 'Successful'
                },
                notifications: [
                    {
                        notificationId: 1,
                        contactName: 'Name',
                        createDateTime: '/Date(123412341234)/',
                        notificationReference: '1234',
                        type: 'Notification',
                        description: 'Notification',
                        channel: 'EMAIL',
                        recipient: 'You',
                        subject: 'Subject',
                        status: 'SENT',
                        sendDateTime: '/Date(123412341234)/',
                        failDateTime: '/Date(123412341234)/',
                        allowResend: true
                    }
                ]
            };
            service.getCustomerNotifications(PARAMS)
                .then(data => {
                    expect(data).toEqual(RESPONSE.notifications);
                    expect(service.customerNotificationResults).toEqual(RESPONSE.notifications);
                });
            http.expectOne(req => !!req.url.match('/CustomerNotification/GetCustomerNotification'))
                .flush(RESPONSE);
        }));

        it('should reject if customerId is unset', async(() => {
            service.customerId = null;
            service.getCustomerNotifications(null)
                .then(data => fail('getCustomerNotifications should have rejected'))
                .catch(err => expect(err).toBeDefined());
        }));

        it('should reject if params are unset', async(() => {
            service.getCustomerNotifications(null)
                .then(data => fail('getCustomerNotifications should have rejected'))
                .catch(err => expect(err).toBeDefined());
        }));

        it('should reject if the web service returns error', async(() => {
            service.getCustomerNotifications(PARAMS)
                .then(data => fail('getCustomerNotifications should have failed with a server error'))
                .catch(err => expect(err).toBeDefined());
            http.expectOne(req => !!req.url.match('/CustomerNotification/GetCustomerNotification'))
                .flush(null, { status: 500, statusText: 'TEST_SERVER_ERROR' });
        }));
    });

    describe('#getCustomerNotificationDetail', () => {
        beforeEach(() => {
            service.customerId = '00000000-0000-0000-0000-000000000002';
            service.selected = {
                notificationId: 1,
                contactName: 'Name',
                createDateTime: '/Date(123412341234)/',
                notificationReference: '1234',
                type: 'Notification',
                description: 'Notification',
                channel: 'EMAIL',
                recipient: 'You',
                subject: 'Subject',
                status: 'SENT',
                sendDateTime: '/Date(123412341234)/',
                failDateTime: '/Date(123412341234)/',
                allowResend: true
            };
        });

        it('should resolve to a NotificationDetail', async(() => {
            const NOTIFICATION_DETAIL = {
                hdr: {
                    result: 'Successful'
                },
                alternateMessage: 'Sample email',
                message: '<html><body><p>Sample email</p></body></html>',
                contact: {
                    contactId: '00000000-0000-0000-0000-000000000001'
                }
            };
            let results;
            service.getCustomerNotificationDetail()
                .then(data => {
                    expect(data).toEqual(jasmine.any(Object));
                    expect(data.alternateMessage).toEqual(NOTIFICATION_DETAIL.alternateMessage);
                    expect(data.message).toEqual(NOTIFICATION_DETAIL.message);
                    expect(data.contactId).toEqual(NOTIFICATION_DETAIL.contact.contactId);
                    expect(service.selectedDetail).toEqual(jasmine.any(Object));
                    expect(service.selectedDetail.alternateMessage).toEqual(NOTIFICATION_DETAIL.alternateMessage);
                    expect(service.selectedDetail.message).toEqual(NOTIFICATION_DETAIL.message);
                    expect(service.selectedDetail.contactId).toEqual(NOTIFICATION_DETAIL.contact.contactId);
                });
            http.expectOne(req => !!req.url.match('/CustomerNotification/GetCustomerNotificationDetail'))
                .flush(NOTIFICATION_DETAIL);
        }));

        it('should reject if customerId is unset', async(() => {
            service.customerId = null;
            service.getCustomerNotificationDetail()
                .then(data => fail('getCustomerNotificationDetail should have rejected'))
                .catch(err => expect(err).toBeDefined());
        }));

        it('should reject if no notification is selected', async(() => {
            service.selected = null;
            service.getCustomerNotificationDetail()
                .then(data => fail('getCustomerNotificationDetail should have rejected'))
                .catch(err => expect(err).toBeDefined());
        }));

        it('should reject if the web service returns error', async(() => {
            service.getCustomerNotificationDetail()
                .then(data => fail('getCustomerNotificationDetail should have rejected with a server error'))
                .catch(err => expect(err).toBeDefined());
            http.expectOne(req => !!req.url.match('/CustomerNotification/GetCustomerNotificationDetail'))
                .flush(null, { status: 500, statusText: 'TEST_SERVER_ERROR' });
        }));

        it('should reject if the web service returns OK but NIS returns error', async(() => {
            const RESPONSE = {
                hdr: {
                    result: 'Error'
                }
            };
            service.getCustomerNotificationDetail()
                .then(data => fail('getCustomerNotificationDetail should have rejected with a NIS error'))
                .catch(err => expect(err).toBeDefined());
            http.expectOne(req => !!req.url.match('/CustomerNotification/GetCustomerNotificationDetail'))
                .flush(RESPONSE);
        }));
    });

    describe('#resend', () => {
        let CHANNEL;

        beforeEach(() => {
            service.customerId = '00000000-0000-0000-0000-000000000001';
            service.resendContactId = '00000000-0000-0000-0000-000000000002';
            service.selected = {
                notificationId: 1,
                contactName: 'Name',
                createDateTime: '/Date(123412341234)/',
                notificationReference: '1234',
                type: 'Notification',
                description: 'Notification',
                channel: 'EMAIL',
                recipient: 'You',
                subject: 'Subject',
                status: 'SENT',
                sendDateTime: '/Date(123412341234)/',
                failDateTime: '/Date(123412341234)/',
                allowResend: true
            };
            service.selectedDetail = {
                alternateMessage: 'Sample email',
                message: '<html><body><p>Sample email</p></body></html>',
                contactId: '00000000-0000-0000-0000-000000000002'
            }
            CHANNEL = 'EMAIL';
        });

        it('should resolve on success', async(() => {
            const RESPONSE = {
                Success: true
            };
            service.resend(CHANNEL)
                .then(data => expect(data).toBeDefined());
            http.expectOne(req => {
                let body = JSON.parse(req.body);
                return !!req.url.match('/CustomerNotification/CustomerNotificationResend')
                    && body.customerId === service.customerId
                    && body.contactId === service.resendContactId
                    && body.clientRefId === service.resendContactId
                    && body.notificationId === service.selected.notificationReference
                    && body.notificationType === service.selected.type
                    && body.allowReformat === true
                    && body.channel === CHANNEL;
            }).flush(RESPONSE);
        }));

        it('should reject if customerId is unset', async(() => {
            service.customerId = null;
            service.resend(CHANNEL)
                .then(data => fail('resend should have rejected'))
                .catch(err => expect(err).toBeDefined());
        }));

        it('should reject if the resend contactId is unset', async(() => {
            service.resendContactId = null;
            service.resend(CHANNEL)
                .then(data => fail('resend should have rejected'))
                .catch(err => expect(err).toBeDefined());
        }));

        it('should reject if no notification is selected', async(() => {
            service.selected = null;
            service.resend(CHANNEL)
                .then(data => fail('resend should have rejected'))
                .catch(err => expect(err).toBeDefined());
        }));

        it('should reject if notification detail is unset', async(() => {
            service.selectedDetail = null;
            service.resend(CHANNEL)
                .then(data => fail('resend should have rejected'))
                .catch(err => expect(err).toBeDefined());
        }));

        it('should reject if channel is unset', async(() => {
            service.resend(null)
                .then(data => fail('resend should have rejected'))
                .catch(err => expect(err).toBeDefined());
        }));

        it('should reject if web service returns error', async(() => {
            service.resend(CHANNEL)
                .then(data => fail('resend should have rejected with a server error'))
                .catch(err => expect(err).toBeDefined());
            http.expectOne(req => !!req.url.match('/CustomerNotification/CustomerNotificationResend'))
                .flush(null, { status: 500, statusText: 'TEST_SERVER_ERROR' });
        }));
    });
});