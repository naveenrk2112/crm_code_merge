﻿import { APP_INITIALIZER } from '@angular/core';
import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Cub_DataService } from './cub_data.service';
import { Cub_WebApiService } from './cub-webapi.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { Cub_WebApiServiceStub } from '../testutilities/cub-webapi.service.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

import { isEqual } from 'lodash';

describe('Cub_DataService', () => {
    let service: Cub_DataService;
    let http: HttpTestingController;

    const xrm = {
        Page: {
            context: {
                getClientUrl: () => 'http://fake.dynamics',
                getUserId: () => '{00000000-0000-0000-0000-000000000001}',
                getVersion: () => '8.2'
            }
        },
        Utility: {
            openEntityForm: (...args) => { }
        }
    }

    const API_URL = 'http://fakeApiUrl';
    const GLOBAL1 = 'Global1';
    const GLOBAL1_DETAIL1 = 'Detail1';
    const GLOBAL1_DETAIL2 = 'Detail2';
    const GLOBAL2 = 'Global2';
    const GLOBAL2_DETAIL1 = 'Detail3';
    const GLOBAL2_DETAIL2 = 'Detail4';
    const CONSTANTS1 = {
        Key: GLOBAL1,
        Value: [
            { Key: GLOBAL1_DETAIL1, Value: 'stringVal' },
            { Key: GLOBAL1_DETAIL2, Value: 123 },
        ]
    };
    const CONSTANTS2 = {
        Key: GLOBAL2,
        Value: [
            { Key: GLOBAL2_DETAIL1, Value: -21 },
            { Key: GLOBAL2_DETAIL2, Value: 'another string' },
        ]
    };

    beforeEach(() => {
        window.Xrm = xrm as Xrm.XrmStatic;
        parent.Xrm = xrm as Xrm.XrmStatic;

        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                Cub_DataService,
                { provide: Cub_WebApiService, useClass: Cub_WebApiServiceStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
            ]
        });

        service = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
        http.verify();
    });

    it('#executeOnLoad should update apiServerUrl', async(() => {
        const URL = 'http://success.url';
        Cub_WebApiServiceStub._setGlobal('CUB.MSD.WEB', 'WEB.APP.URL', URL);

        expect(service.apiServerUrl).not.toEqual(URL);
        const prevUrl = service.apiServerUrl;
        service.executeOnLoad().then(() => {
            http.expectNone(req => req.url.match(prevUrl));
            http.match(req => req.url.match(URL))
                .map(req => req.flush(null, { status: 204, statusText: 'No content' }));
            expect(service.apiServerUrl).toEqual(URL);
        });
    }));

    it('#augmentConstants should modify globals and alert observers', () => {
        let subscription = service.Globals.subscribe(g => expect(g).toEqual({}));
        subscription.unsubscribe();

        service.augmentConstants(CONSTANTS1, CONSTANTS2);
        subscription = service.Globals.subscribe(g => {
            expect(g[GLOBAL1]).toBeDefined();
            expect(g[GLOBAL1][GLOBAL1_DETAIL1]).toBeDefined();
            expect(g[GLOBAL1][GLOBAL1_DETAIL2]).toBeDefined();
            expect(g[GLOBAL2]).toBeDefined();
            expect(g[GLOBAL2][GLOBAL2_DETAIL1]).toBeDefined();
            expect(g[GLOBAL2][GLOBAL2_DETAIL2]).toBeDefined();
        });
        subscription.unsubscribe();
    });

    it('#fetchConstants should initiate an HTTP GET request for each argument', () => {
        pending();
        service.fetchConstants();
        let req1 = http.expectOne(API_URL + '/Home/GetConstants?key=' + GLOBAL1);
        let req2 = http.expectOne(API_URL + '/Home/GetConstants?key=' + GLOBAL2);
        req1.flush(CONSTANTS1.Value);
        req2.flush(CONSTANTS2.Value);
        let subscription = service.Globals.subscribe(g => {
            if (isEqual(g, {})) {
                return;
            }
            expect(g[GLOBAL1]).toBeDefined();
            expect(g[GLOBAL1][GLOBAL1_DETAIL1]).toBeDefined();
            expect(g[GLOBAL1][GLOBAL1_DETAIL2]).toBeDefined();
            expect(g[GLOBAL2]).toBeDefined();
            expect(g[GLOBAL2][GLOBAL2_DETAIL1]).toBeDefined();
            expect(g[GLOBAL2][GLOBAL2_DETAIL2]).toBeDefined();
        });
        subscription.unsubscribe();
    });
});