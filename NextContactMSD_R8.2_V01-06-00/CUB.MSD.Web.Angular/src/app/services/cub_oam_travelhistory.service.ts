﻿import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';
import * as _ from 'lodash';
import * as moment from 'moment';

/* Data model for request body of getTravelHistory() */
export interface TravelHistorySearchCriteria {
    oneAccountId: string,
    startDateTime?: string,
    endDateTime?: string,
    authorityId?: string,
    tripCategory?: string,
    travelMode?: string,
    viewType?: string,
    sortBy?: string,
    offset?: number,
    limit?: number
}

//Used to call service for OAM Travel History
@Injectable()
export class Cub_OAM_TravelHistoryService {
    public oneAccountId: string;
    private requestInProgress: boolean = false;
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    /**
    * Gets travel history for a given customer, everything but accountId is optional
    * @param params search filter criteria passed as the request body
    * @returns a Promise to resolve to an array of Travel History
    */
    getTravelHistory(queryParams: TravelHistorySearchCriteria): Promise<any> {
        if (this.requestInProgress) {
            return Promise.resolve();
        }
        if (!queryParams) {
            return Promise.reject('No query parameters set');
        }
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }

        this.requestInProgress = true;
        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/OneAccount/GetTravelHistory`, { params: params })
            .finally(() => this.requestInProgress = false)
            .toPromise();
    }

    /**
    * Gets the detail of the currently selected travel history.
    * @returns a Promise to resolve to a TravelHistoryDetail
    */
    getTravelHistoryDetail(tripId): Promise<any> {
        if (!this.oneAccountId || !tripId) {
            return Promise.reject('No trip selected');
        }
        let params = new HttpParams()
            .set('oneAccountId', this.oneAccountId)
            .set('tripId', tripId);

        return this._http.get<any>(`${this._cub_dataService.apiServerUrl}/OneAccount/GetTravelHistoryDetail`, { params })
            .toPromise();
    }
}