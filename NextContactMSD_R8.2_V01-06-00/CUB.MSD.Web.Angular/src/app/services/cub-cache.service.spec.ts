﻿import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';

import { Cub_CacheService } from './cub-cache.service';
import { Cub_DataService } from './cub_data.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';

describe('Cub_CacheService', () => {
    let service: Cub_CacheService;
    let dataService: Cub_DataService

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                Cub_CacheService,
                { provide: Cub_DataService, useClass: Cub_DataServiceStub }
            ]
        });

        service = TestBed.get(Cub_CacheService);
    });

    afterEach(() => {
        sessionStorage.clear();
    });

    it('#retrieve should invoke request function if value not cached', async(() => {
        const KEY = 'test-key-1';
        const VALUE = 'success!';
        const len = sessionStorage.length;
        let count = 0;
        const request = () => new Promise<string>(resolve => {
            ++count;
            setTimeout(() => resolve(VALUE), 500);
        });

        service.retrieve(KEY, request)
            .then(data => {
                expect(sessionStorage.length - len).toEqual(1, 'Value not added to cache');
                expect(count).toEqual(1, 'Request function not invokes');
                expect(data).toEqual(VALUE, 'Results do not match');
            });
    }));

    it('#retrieve should only invoke request function once for the same value', (done) => {
        const KEY = 'test-key-2';
        const VALUE = 'way to go, pal';
        const len = sessionStorage.length;
        let count = 0;
        const request = () => new Promise<string>(resolve => {
            ++count;
            setTimeout(() => resolve(VALUE), 500);
        });

        // make actual request
        service.retrieve(KEY, request).then(data => {
            expect(sessionStorage.length - len).toEqual(1, 'Value not added to cache');
            expect(count).toEqual(1, 'Request function not invoked');
            expect(data).toEqual(VALUE, 'Results do not match');
        });

        // establish event handler
        service.retrieve(KEY, request).then(data => {
            expect(sessionStorage.length - len).toBeLessThan(2, 'Value added to cache more than once');
            expect(count).toBeLessThan(2, 'Request function invoked more than once');
            expect(data).toEqual(VALUE, 'Results do not match');
        });

        // immediately return value
        setTimeout(() => {
            service.retrieve(KEY, request).then(data => {
                expect(sessionStorage.length - len).toBeLessThan(2, 'Value added to cache more than once');
                expect(count).toBeLessThan(2, 'Request function invoked more than once');
                expect(data).toEqual(VALUE, 'Results do not match');
                done();
            });
        }, 1000);
    });

    it('#retrieve should alert pending listeners if initial request fails', (done) => {
        const KEY = 'test-key-3';
        const len = sessionStorage.length;
        let count = 0;
        const request = () => new Promise<any>((resolve, reject) => {
            ++count;
            setTimeout(() => reject('ERROR'), 500);
        });

        let output1 = false;
        service.retrieve(KEY, request).then(data => {
            fail('First retrieve should fail');
        }).catch(err => {
            output1 = true;
        });

        let output2 = false;
        service.retrieve(KEY, request).then(data => {
            fail('Second retrieve should return error after first fails');
        }).catch(err => {
            output2 = true;
            done();
        });
        //expect(output1).toBeTruthy('First retrieve should have failed');
        //expect(output2).toBeTruthy('Second retrieve should have failed');
        //expect(count).toEqual(1, 'Request function invoked an incorrect number of times');
        //expect(sessionStorage.length).toEqual(len, 'No value should be cached');
    });
});