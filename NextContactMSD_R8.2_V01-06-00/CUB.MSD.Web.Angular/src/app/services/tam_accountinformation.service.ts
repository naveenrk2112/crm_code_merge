﻿import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

@Injectable()
export class Tam_AccountInformationService {

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getAccountSummary(customerId: string) {
        return this._http.get(this._cub_dataService.apiServerUrl +
            '/Tolling/GetAccountSummary?customerId=' + customerId)
            .toPromise();
    }
}
