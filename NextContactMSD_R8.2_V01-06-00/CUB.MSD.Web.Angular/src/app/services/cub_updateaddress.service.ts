﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';
import { WSAddress } from '../model';

@Injectable()
export class Cub_UpdateAddressService {
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    /**
     * Since this component is intended to render in an IFrame within a modal,
     * use this to communicate with the IFrame's parent's message listener.
     * @param data must contain an action string: {
     *     action: 'some action defined in updateAddress.html web resource',
     *     ...optional data to be passed to parent
     * }
     */
    postToParent(data: object) {
        parent.postMessage(JSON.stringify(data), '*');
    }

    /**
     * Send a POST request to the web controller to update this address for the
     * provided dependencies.
     * @param customerId
     * @param address
     * @param contacts
     * @param fundingSources
     */
    update(customerId: string, address: WSAddress, contacts: string[], fundingSources: string[]) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' }),
            body = {
                customerId,
                addressId: address.addressId,
                requestBody: {
                    address,
                    contactsUpdated: contacts,
                    fundingSourcesUpdated: fundingSources
                },
                userId: this._cub_dataService.currentUserId
            };

        this._http.post<any>(this._cub_dataService.apiServerUrl + '/CustomerAddress/AddressUpdate', body, { headers })
            .toPromise()
            .then(data => ({
                action: 'success'
            }))
            .catch(err => ({
                action: 'error',
                error: err
            }))
            .then(returnVal => this.postToParent(returnVal));
    }
}