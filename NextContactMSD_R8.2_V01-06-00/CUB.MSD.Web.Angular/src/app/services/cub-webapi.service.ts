﻿import { Injectable } from '@angular/core';

import { map } from 'lodash';
import * as moment from 'moment';

@Injectable()
export class Cub_WebApiService {
    constructor() {
        if (window['WebAPI']) {
            this._webApi = new window['WebAPI']('8.2');
            this._isAvailable = true;
        }
    }

    private _webApi: any;
    private _isAvailable: boolean = false;

    getGlobalDetailAttributeValue(globalKey, globalDetailKey): Promise<string> {
        if (!this._isAvailable) {
            return Promise.reject('WebAPI is unavailable');
        }

        const fetchXml = `
            <fetch version='1.0' output-format='xml-platform' mapping='logical'>
                <entity name='cub_globaldetails'>
                    <attribute name='cub_attributevalue' />
                    <filter type='and'>
                        <condition attribute='cub_enabled' operator='eq' value='1' />
                        <condition attribute='cub_name' operator='eq' value='${globalDetailKey}' />
                    </filter>
                    <link-entity name='cub_globals' from='cub_globalsid' to='cub_globalid'>
                        <filter type='and'>
                            <condition attribute='cub_enabled' operator='eq' value='1' />
                            <condition attribute='cub_name' operator='eq' value='${globalKey}' />
                        </filter>
                    </link-entity>
                </entity>
            </fetch>`;

        return new Promise<any>((resolve, reject) => {
            this._webApi.fetch('cub_globaldetailses', fetchXml)
                .then(result => resolve(result), err => reject(err));
        })
            .then(result => {
                if (!result || !result.value || !result.value.length) {
                    return Promise.reject(`Unable to find global detail ${globalKey} - ${globalDetailKey}`);
                }
                return result.value[0]['cub_attributevalue'];
            });
    }
}