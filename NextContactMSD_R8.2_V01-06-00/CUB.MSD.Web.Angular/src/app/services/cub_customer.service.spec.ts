import { TestBed, inject } from '@angular/core/testing';

import { CubCustomerService } from './cub_customer.service';

describe('CubCustomerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CubCustomerService]
    });
  });

  it('should be created', inject([CubCustomerService], (service: CubCustomerService) => {
    expect(service).toBeTruthy();
  }));
});
