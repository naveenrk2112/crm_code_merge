﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Cub_CacheService } from './cub-cache.service';
import { Cub_DataService } from './cub_data.service';
import * as Model from '../model';

import * as moment from 'moment';

@Injectable()
export class Cub_SessionService {
    constructor(
        private _http: HttpClient,
        private _cache: Cub_CacheService,
        private _cub_dataService: Cub_DataService
    ) {
        // When the session service is injected by a component, load up the
        // cached session automatically before the component initializes.
        this.session = this._cache.retrieveSync<Model.Session>(this.sessionKey);
    }

    private _session: Model.Session = {};
    get session() { return this._session; }
    set session(s: Model.Session) { this._session = Object.assign({}, s); }

    get isActive() { return !!this.session.sessionId; }

    readonly sessionKey = `current-session-${this._cub_dataService.currentUserId}`;

    //Attempt to retrieve an active session
    private retrieveSession(): Promise<Model.Session> {
        const params = new HttpParams()
            .set('userId', this._cub_dataService.currentUserId);

        return this._http.get<Model.Session>(this._cub_dataService.apiServerUrl + '/Session/RetrieveSession', { params: params })
            .toPromise();
    }

    getCachedSession(): Promise<Model.Session> {
        if (!this._cub_dataService.currentUserId) {
            return Promise.reject('Cannot get cached session, no user ID');
        }
        return this._cache.retrieve<any>(this.sessionKey, () => this.retrieveSession())
            .then(session => {
                this.session = session;
                return session;
            });
    }

    refreshSession(): Promise<Model.Session> {
        if (!this._cub_dataService.currentUserId) {
            return Promise.reject('Cannot refresh session, no user ID');
        }
        return this._cache.refresh<any>(this.sessionKey, () => this.retrieveSession())
            .then(session => {
                this.session = session;
                return session;
            });
    }

    createSession(entityName: string, recordId: string): Promise<Model.Session> {
        if (!this._cub_dataService.currentUserId) {
            return Promise.reject('Cannot create session, no user ID');
        }

        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const body = {
            entityName: entityName,
            recordId: recordId,
            userId: this._cub_dataService.currentUserId,
            channelName: "CRM"
        };

        return this._http.post<any>(this._cub_dataService.apiServerUrl + '/Session/CreateSession', body, { headers: headers })
            .do(data => {
                if (data) {
                    this.session = data;
                    this._cache.store(this.sessionKey, data);
                }
            })
            .toPromise();
    }

    /**
     * Update the current session.
     * @param session an object containing the MSD attribute values to update
     */
    updateSession(session: any): Promise<Model.Session> {
        if (!this._cub_dataService.currentUserId) {
            return Promise.reject('Cannot update session, no user ID');
        }

        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const body = {
            userId: this._cub_dataService.currentUserId,
            session: session
        };
        return this._http.post<Model.Session>(this._cub_dataService.apiServerUrl + '/Session/Update', body, { headers: headers })
            .toPromise()
            .then(data => {
                if (data) {
                    this.session = data;
                    this._cache.store(this.sessionKey, data);
                }
                return this.session;
            });
    }

    //Transfer an active session to another CSR (user)
    transferSessionInfo(sessionInfoId: string, userId: string, transferToId: string) {
        if (!sessionInfoId) {
            return Promise.reject('No Session id provided');
        }
        if (!userId) {
            return Promise.reject('No System User id provided');
        }
        if (!transferToId) {
            return Promise.reject('No Transfer To id provided');
        }

        var body = {
            'sessionId': sessionInfoId,
            'userId': userId,
            'transferToId': transferToId,
            'callerId': this._cub_dataService.currentUserId
        };

        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.post<any>(this._cub_dataService.apiServerUrl + '/Session/TransferSession', body, { headers: headers })
            .toPromise();
    }

    //Close (end) an active session
    endSession(): Promise<null> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const body = {
            sessionId: this.session.sessionId,
            userId: this._cub_dataService.currentUserId
        };
        return this._http.post<null>(this._cub_dataService.apiServerUrl + '/Session/EndSession', body, { headers: headers })
            .do(() => {
                this.session = {};
                this._cache.remove(this.sessionKey);
            })
            .toPromise();
    }

    //Retrieve a list of active users (minus the user making the call)
    retrieveActiveUsers(systemuserId: string): Promise<Model.ActiveUsers> {
        if (!systemuserId) {
            return Promise.reject('No System User id provided');
        }
        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var params = new HttpParams()
            .set('userId', systemuserId);

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Session/RetrieveActiveUsers', { headers: headers, params: params })
            .toPromise();
    }

    //Get session details
    getSessionDetails(sessionId: string): Promise<Model.Session> {
       
        if (!sessionId) {
            return Promise.reject('No session Id provided');
        }
        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var params = new HttpParams()
            .set('sessionId', sessionId);

       return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Session/GetSessionDetails', { headers: headers, params: params })
            .toPromise();       

    }

    searchSession(sessionNumber: string): Promise<Model.Session> {
        if (!sessionNumber) {
            return Promise.reject('No session number provided');
        }
        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var params = new HttpParams()
            .set('sessionNumber', sessionNumber);

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Session/SearchSession', { headers: headers, params: params })
            .toPromise();

    }

    //Get session details
    getLinkedSessions(sessionId: string) {

        if (!sessionId) {
            return Promise.reject('No session Id provided');
        }
        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var params = new HttpParams()
            .set('sessionId', sessionId);

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Session/GetLinkedSessions', { headers: headers, params: params })
            .toPromise();

    }


    updateNotes(sessionInfoId: string, notes: string) {
        if (!sessionInfoId) {
            return Promise.reject('No Session id provided');
        }       

        var body = {
            'userId': this._cub_dataService.currentUserId,
            'sessionId': sessionInfoId,
            'notes': notes
        };

        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.post<any>(this._cub_dataService.apiServerUrl + '/Session/UpdateNotes', body, { headers: headers })
            .toPromise();
    }

    linkSessions(parentSessionId: string, childSessionId: string) {
        if (!childSessionId) {
            return Promise.reject('No Child Session id provided');
        }
        var body = {
            'userId': this._cub_dataService.currentUserId,
            'parentSession': parentSessionId,
            'childSession': childSessionId
        };

        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.post<any>(this._cub_dataService.apiServerUrl + '/Session/LinkSessions', body, { headers: headers })
            .toPromise();
    }

    removeLinkedSession(sessionLinkId: string) {
        if (!sessionLinkId) {
            return Promise.reject('No Session id provided');
        }
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const params = new HttpParams()
            .set('linkSessionId', sessionLinkId)
            .set('userId', this._cub_dataService.currentUserId);

        return this._http.delete(this._cub_dataService.apiServerUrl +
            '/Session/RemoveLinkedSession',
            { headers, params })
            .toPromise()
            .then((data: any) => {
                if (data) {
                    let errorMessage = this._cub_dataService.tryParse(data.errorMessage)
                    return Promise.reject(errorMessage);
                }
            });
    }
}