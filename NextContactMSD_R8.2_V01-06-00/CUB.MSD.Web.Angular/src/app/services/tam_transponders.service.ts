﻿import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

export interface Transponder {
    id: number,
    type: string, // product
    tagId: string,
    sno: string,
    licPlate: string,
    customerId: string,
    createdDate: string,
    uidCreated: number,
    uidUpdated: number,
    isActive: string
}

export interface TransponderList {
    vehicles: Transponder[]
}

@Injectable()
export class Tam_TranspondersService {
    
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getTransponders(customerId: string) {
        return this._http.get(this._cub_dataService.apiServerUrl +
            '/Tolling/GetTransponders?customerId=' + customerId)
            .toPromise();
    }
}
