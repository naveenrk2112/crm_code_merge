﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

/* Data model for request body of getTripsHistory() */
export interface TripsHistorySearchCriteria {
    subSystemId: string,
    accountId: string,
    startDateTime?: string,
    endDateTime?: string,
    tripStatus?: string,
    travelMode?: string,
    tripCategory?: string,
    viewType?: string,
    sortBy?: string,
    offset?: number,
    limit?: number
}

@Injectable()
export class Cub_TA_TripsHistoryService {
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getTripsHistory(queryParams: TripsHistorySearchCriteria) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/TransitAccount/GetTravelHistory', { params: params })
            .toPromise();
    }
}