﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

/* Data model for request body of getStatusHistory() */
export interface TokenStatusHistorySearchCriteria {
    subsystemId: string,
    tokenId: string,
    offset?: number,
    limit?: number
}

@Injectable()
export class Cub_TA_Token_StatusHistoryService {
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getStatusHistory(queryParams: TokenStatusHistorySearchCriteria) {

        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/TransitAccount/GetTokenStatusHistory', { params: params })
            .toPromise();
    }
}