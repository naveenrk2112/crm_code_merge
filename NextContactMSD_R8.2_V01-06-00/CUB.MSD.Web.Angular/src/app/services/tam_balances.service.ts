﻿import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

export interface Balances {
    postapaidBalance: number,
    lastInvPayments: number,
    lastTripPost: number,
    currentBalance: number,
    lastInvBalance: number,
    prepaidBalance: number,
    lastInvTolls: number,
    paymentPlanBalance: number,
    lastInvAdjustments: number,
    transponderDeposit: number,
    tagCount: number,
    plateCount: number,
    collectionBalance: number,
    lastPaymentPost: number,
    violationBalance: number
}

@Injectable()
export class Tam_BalancesService {

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getAccountSummary(customerId: string) {
        return this._http.get(this._cub_dataService.apiServerUrl +
            '/Tolling/GetAccountSummary?customerId=' + customerId)
            .toPromise();
    }
}
