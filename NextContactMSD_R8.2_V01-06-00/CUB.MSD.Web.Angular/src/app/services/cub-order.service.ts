import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Cub_CacheService } from './cub-cache.service';
import { Cub_DataService } from './cub_data.service';
import * as Model from '../model';

import * as uuid from 'uuid/v4';

@Injectable()
export class CubOrderService {

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService,
        private _cache: Cub_CacheService
    ) { }

    getOrderConfigDataTypesFromCache() {
        return this._cache.retrieve("ODER_CONFIG_DATA_TYPES", () => this.getOrderConfigDatatypes());
    }

    private getOrderConfigDatatypes() {
        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Order/GetOrderConfigDataTypes')
            .toPromise()
            .then((configData) => {
                var json = JSON.parse(configData.Body)
                return json;
            });
    }

    /**
 * Sends a GET request to the web controller for a list of order history
 * records. 
 * @param filters an object containing optional filter parameters
 * @returns a promise that resolves to an object containing a list of order
 * history records and a total count field for table paging.
 */
    getOrderHistory(filters: any = {}): Promise<Model.WSGetCustomerOrderHistoryResponse> {
        let params = new HttpParams();
        for (let qp in filters) {
            if (filters[qp] != null) {
                params = params.set(qp, filters[qp]);
            }
        }

        // TODO: verify the above works with NIS before deleting
        //.set('startDate', filters.startDate)
        //.set('endDate', filters.endDate)
        //.set('orderType', filters.orderType)
        //.set('orderStatus', filters.orderStatus)
        //.set('sortBy', null)
        //.set('offset', filters.offset)
        //.set('limit', filters.limit);

        return this._http.get<Model.WSGetCustomerOrderHistoryResponse>(this._cub_dataService.apiServerUrl + '/Order/GetOrderHistory', { params })
            .toPromise();
    }

    /**
 * Sends a GET request to the web controller for a list of order history
 * records. This rejects if orderId is not given or if the response
 * indicates an error.
 * @param orderId
 * @returns a promise that resolves to an object containing a list of order
 * history records and a total count field for table paging.
 */
    getOrderHistoryById(orderId: string): Promise<Model.WSGetCustomerOrderHistoryResponse> {
        if (!orderId) {
            return Promise.reject('No Order id provided');
        }

        let params = new HttpParams().set('orderId', orderId);

        return this._http.get<Model.WSGetCustomerOrderHistoryResponse>(this._cub_dataService.apiServerUrl + '/Order/GetOrderHistory', { params })
            .toPromise();
    }


    //Get order details
    getCustomerOrderDetails(customerId: string, orderId: string, realTime: boolean) {
        
        if (!customerId) {
            return Promise.reject('No customer Id provided');
        }

        if (!orderId) {
            return Promise.reject('No order Id provided');
        }
        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var params = new HttpParams()
            .set('customerId', customerId)
            .set('orderId', orderId)
            .set('realtimeResults', String(realTime));

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/CustomerOrder/GetOrderHistoryDetail', { headers: headers, params: params })
            .toPromise();

    }
    
    //Get order details
    getOrderNotifications(orderId: string, status: string) {

        if (!orderId) {
            return Promise.reject('No Order Id provided');
        }

        if (!status) {
            status = 'All';
        }

        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var params = new HttpParams()
            .set('orderId', orderId)
            .set('status', status);

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Order/GetOrderNotification', { headers: headers, params: params })
            .toPromise();

    }

    generatePaymentReceipt(orderId: string, email: string)
    {
        if (!orderId) {
            return Promise.reject('No Order Id provided');
        }     


        let headers = new HttpHeaders({ 'Content-Type': 'application/json' }),
            params = {
                orderId: orderId,
                unregisteredEmail: email,
                userId: this._cub_dataService.currentUserId
            };

        return this._http.post<any>(`${this._cub_dataService.apiServerUrl}/Order/OrderPaymentReceipt`, JSON.stringify(params), { headers: headers })
            .toPromise();
    }
}
