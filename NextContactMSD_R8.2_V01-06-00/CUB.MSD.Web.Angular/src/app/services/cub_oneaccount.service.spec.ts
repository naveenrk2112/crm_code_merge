﻿import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpResponse } from '@angular/common/http';

import { Cub_OneAccountService } from './cub_oneaccount.service';
import { Cub_DataService } from './cub_data.service';
import { Cub_CacheService } from './cub-cache.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_CacheServiceStub } from '../testutilities/cub-cache.service.stub';

describe('Cub_OneAccountService', () => {
    let service: Cub_OneAccountService;
    let dataService: Cub_DataService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                Cub_OneAccountService,
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: Cub_CacheService, useClass: Cub_CacheServiceStub }
            ]
        });

        service = TestBed.get(Cub_OneAccountService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => http.verify());

    it('#LinkOneAccountToSubsystem should return on success', done => {
        const LINK_FAILURE = 
            {
                "hdr": {
                    "result": "DataValidationError",
                    "uid": "ff14aa8d-f0f0-4d53-baf6-a3cd461155bd",
                    "fieldName": "subsystemAccountReference",
                    "errorKey": "errors.subsystem.account.already.linked.to.this.oneaccount",
                    "errorMessage": ""
                }
            };
        let result;
        let customerId = 140;
        let subsystemId = 'ABP';
        let accountRef = '330000000001';
        let subsystemAccountNickname = 'Bankcard ****-****-3456';
        service.LinkOneAccountToSubsystem(customerId, subsystemId, accountRef, subsystemAccountNickname)
            .then(data => done())
            .catch(err => fail('LinkOneAccountToSubsystem should have succeeded, but instead failed with ' + err));
        http.expectOne(req => !!req.url.match('/OneAccount/LinkSubsystemToCustomeraccount'))
            .flush(null, { status: 204, statusText: 'Success' });
    });
});