﻿import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Cub_CustomerVerificationService } from './cub_customerverification.service';
import { Cub_DataService } from './cub_data.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';

describe('Cub_CustomerVerificationService', () => {
    let service: Cub_CustomerVerificationService;
    let dataService: Cub_DataService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                Cub_CustomerVerificationService,
                { provide: Cub_DataService, useClass: Cub_DataServiceStub }
            ]
        });

        service = TestBed.get(Cub_CustomerVerificationService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);

        service.contactId = 'DFE01121-D1BE-4CCE-94A0-8C4BE83B2F43';
    });

    afterEach(() => http.verify());

    it('should reject a call if selected customer\'s ContactId is null', async(() => {
        service.contactId = null;
        service.getAddress()
            .then(data => fail('The call was not rejected'))
            .catch(err => expect(err).toBeDefined());
    }));

    it('#getAddress should return an address object', async(() => {
        const ADDRESS = {
            addressId: "03F9B8A0-3BA3-417F-A1FB-6C5AC35407D2",
            city: 'Chicago',
            state: 'IL',
            street1: '21 Jump St.',
            street2: null,
            street3: null,
            zipCode: '60606'
        };
        service.getAddress()
            .then(data => expect(data).toEqual(ADDRESS));
        http.expectOne(req => !!req.urlWithParams.match('/CustomerVerification/GetAddress'))
            .flush(ADDRESS);
    }));

    it('#getBirthDate should return a birth date string', async(() => {
        const BIRTH_DATE = '6/3/1987';
        service.getBirthDate()
            .then(data => expect(data).toEqual(BIRTH_DATE));
        http.expectOne(req => !!req.urlWithParams.match('/CustomerVerification/GetBirthDate'))
            .flush(BIRTH_DATE);
    }));

    it('#getSecurityQuestions should return an array of security question objects', async(() => {
        const SECURITY_QUESTIONS = [
            {
                securityQuestion: "Is this question 1?",
                securityAnswer: "This is answer 1",
                securityQuestionId: "1d08aa6c-ce29-e711-80e6-005056814569",
                securityAnswerId: "0dc9bea8-2330-e711-80e6-005056814569"
            }, {
                securityQuestion: "Is this question 2?",
                securityAnswer: "This is answer 2",
                securityQuestionId: "c56807d3-2330-e711-80e6-005056814569",
                securityAnswerId: "c66807d3-2330-e711-80e6-005056814569"
            }
        ];
        service.getSecurityQuestions()
            .then(data => expect(data).toEqual(SECURITY_QUESTIONS));
        http.expectOne(req => !!req.urlWithParams.match('/CustomerVerification/GetSecurityQA'))
            .flush(SECURITY_QUESTIONS);
    }));

    it('#getUsernameAndPin should return a username/pin object', async(() => {
        const USERNAME_AND_PIN = {
            username: 'fakeUsername',
            pin: '0000'
        };
        service.getUsernameAndPin()
            .then(data => expect(data).toEqual(USERNAME_AND_PIN));
        http.expectOne(req => !!req.urlWithParams.match('/CustomerVerification/GetUsernameAndPin'))
            .flush(USERNAME_AND_PIN);
    }));
});