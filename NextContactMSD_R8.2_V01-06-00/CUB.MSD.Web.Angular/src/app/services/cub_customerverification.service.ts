﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Cub_CacheService } from './cub-cache.service';
import { Cub_DataService } from './cub_data.service';
import { Cub_SessionService } from '../services/cub-session.service';
import * as Model from '../model';

import * as moment from 'moment';

@Injectable()
export class Cub_CustomerVerificationService {
    verified: Model.VerifiedCache;
    timeoutInMillis: number = 60000;
    cacheInterval: any;
    cacheTimeout: any;
    contactId: string;
    subjectVerified: string = 'Verified';
    subjectNotVerified: string = 'Not Verified';
    readonly cacheKey = 'verified-contacts';

    constructor(
        private _http: HttpClient,
        private _cache: Cub_CacheService,
        private _cub_dataService: Cub_DataService,
        private _cub_sessionService: Cub_SessionService
    ) { }

    /**
     * Returns true if the given contact GUID is present in the verified cache,
     * false otherwise.
     * @param contactId a string representation of a contact's ID
     */
    isVerified(): boolean {
        return this.verified.contacts.indexOf(this._cub_dataService.stripGuid(this.contactId)) !== -1;
    }

    /**
     * Creates a new MSD cub_verification activity and then redirects the page.
     * Use Cub_DataService.onVerified(false) to specify contact is unverified.
     * @param verified true if verified, false if unverified
     * @param verifiedQuestions optional set of strings of verified questions
     */
    onVerified(verifiedQuestions: Set<string>) {
        let subject = this.subjectVerified;
        let body = Array.from(verifiedQuestions).join(', ');
        let args = {
            userId: this._cub_dataService.currentUserId,
            contactId: this.contactId,
            subject,
            body
        };
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post(`${this._cub_dataService.apiServerUrl}/CustomerVerification/CreateVerificationActivity`, args, { headers })
            .toPromise()
            .then(response => {
                this.verified.contacts.push(this._cub_dataService.stripGuid(this.contactId));
                this.storeVerifiedContacts();
            });
    }

    onVerifiedByContactInfo(verifiedQuestions: Set<string>) {
        let subject = this.subjectVerified;
        let body = Array.from(verifiedQuestions).join(', ');
        let args = {
            userId: this._cub_dataService.currentUserId,
            sessionId: this._cub_sessionService.session.sessionId,
            contactId: this.contactId,
            subject,
            notes: body
        };
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post(`${this._cub_dataService.apiServerUrl}/Activity/ContactVerificationByContactInfo`, args, { headers })
            .toPromise()
            .then(response => {
                this.verified.contacts.push(this._cub_dataService.stripGuid(this.contactId));
                this.storeVerifiedContacts();
            });
    }

    onVerifiedBySecurityQuestion(verifiedQuestions: Set<string>) {
        let subject = this.subjectVerified;
        let body = Array.from(verifiedQuestions).join(', ');
        let args = {
            userId: this._cub_dataService.currentUserId,
            sessionId: this._cub_sessionService.session.sessionId,
            contactId: this.contactId,
            subject,
            notes: body
        };
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post(`${this._cub_dataService.apiServerUrl}/Activity/ContactVerificationBySecurityQuestion`, args, { headers })
            .toPromise()
            .then(response => {
                this.verified.contacts.push(this._cub_dataService.stripGuid(this.contactId));
                this.storeVerifiedContacts();
            });
    }

    onNotVerified() {
        let subject = this.subjectNotVerified;
        let body = 'Unverified';
        let args = {
            userId: this._cub_dataService.currentUserId,
            sessionId: this._cub_sessionService.session.sessionId,
            contactId: this.contactId,
            subject,
            notes: body
        };
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post(`${this._cub_dataService.apiServerUrl}/Activity/ContactVerificationUnverified`, args, { headers })
            .toPromise();
    }

    /**
     * Retrieve a list of recently verified contact IDs from MSD
     */
    reset() {
        return this.getVerifiedContacts()
            .then((data: string[]) => {
                this.verified = {
                    start: moment(),
                    contacts: data || []
                };
            })
            .catch(err => {
                this.verified = {
                    start: moment(),
                    contacts: []
                };
            })
            .then(() => this.storeVerifiedContacts());
    }

    /**
     * Saves the verified contacts cache to the browser session storage.
     */
    storeVerifiedContacts() {
        if (!this.cacheKey) {
            throw new Error('MSD Global "CacheName" is undefined!');
        }

        let copy = {
            start: this.verified.start.toISOString(),
            verified: this.verified.contacts
        };
        this._cache.store(this.cacheKey, copy);
    }

    /**
     * Retrieve recently-verified contacts and stablish a timed interval for
     * resetting the cache of recently verified contacts.
     */
    loadVerifiedContacts(): Promise<void> {
        return this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .toPromise()
            .then(globals => {
                this.subjectVerified = globals['CUB.MSD.Web.Angular'].SubjectVerified as string;
                this.subjectNotVerified = globals['CUB.MSD.Web.Angular'].SubjectNotVerified as string;
                this.timeoutInMillis = globals['CUB.MSD.Web.Angular'].RecentVerificationsOffsetInMinutes as number * 60000;

                return this._cache.retrieve(this.cacheKey,
                    () => this.getVerifiedContacts()
                        .then(contactIds => ({
                            start: moment().toISOString(),
                            verified: contactIds || []
                        })));
            })
            .then(cachedData => {
                this.verified = {
                    start: moment(cachedData.start),
                    contacts: cachedData.verified
                };
                let now = moment();

                let timeout = this.verified.start.add(this.timeoutInMillis, 'ms');
                if (timeout.isBefore(now)) {
                    return this.reset().then(() => {
                        this.cacheInterval = setInterval(() => this.reset(), this.timeoutInMillis);
                    });
                } else {
                    // this ensures the cache cannot persist longer than the reset period
                    let nextResetInMillis = timeout.diff(now);
                    this.cacheTimeout = setTimeout(() => {
                        this.reset();
                        this.cacheInterval = setInterval(() => this.reset(), this.timeoutInMillis);
                    }, nextResetInMillis);
                }
            });
    }

    unloadCache() {
        this.storeVerifiedContacts();
        clearTimeout(this.cacheTimeout);
        clearInterval(this.cacheInterval);
    }

    getVerifiedContacts(): Promise<string[]> {
        return this._http.get(`${this._cub_dataService.apiServerUrl}/CustomerVerification/GetRecentlyVerifiedContactIds?userId=${this._cub_dataService.currentUserId}`)
            .toPromise();
    }

    private _get(requestName: string): Promise<any> {
        if (!this.contactId) {
            return Promise.reject(`Cannot request ${requestName}, no contact ID provided`);
        }
        return this._http.get(`${this._cub_dataService.apiServerUrl}/CustomerVerification/${requestName}?contactId=${this.contactId}`)
            .toPromise();
    }

    getSecurityQuestions() {
        return this._get('GetSecurityQA');
    }

    getAddress() {
        return this._get('GetAddress');
    }

    getBirthDate() {
        return this._get('GetBirthDate');
    }

    getUsernameAndPin() {
        return this._get('GetUsernameAndPin');
    }
}