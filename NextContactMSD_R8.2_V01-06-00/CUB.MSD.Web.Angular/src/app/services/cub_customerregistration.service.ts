﻿import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import * as lodash from 'lodash';

import { Cub_DataService } from './cub_data.service';
import { Cub_OptionSelectAdditionalOption, Cub_OptionSelectOption, Cub_DropdownOption } from '../model';

//Used to call service for Customer Registration
@Injectable()
export class Cub_CustomerRegistrationService {

    constructor(private _http: HttpClient,
        private _cub_dataService: Cub_DataService) {
    }

    currentlyRegistering: boolean = false;

    //Common collection of phone types (for adding new ones)
    phoneTypeOption1: Cub_OptionSelectOption = {
        name: '',
        value: '',
        id: '',
        fieldWidthClass: ''
    };
    phoneTypeOption2: Cub_OptionSelectOption = {
        name: '',
        value: '',
        id: '',
        fieldWidthClass: ''
    };
    phoneTypeAdditionalOptions: Cub_OptionSelectAdditionalOption[];

    //Common collection of security questions (for adding new ones)
    securityQuestionOptions: Cub_DropdownOption[] = [];

    //Call to search customer
    searchCustomer(registrationControls: any) {
        const body = {
            contactSearchData: {
                'FirstName': registrationControls['firstName'].value,
                'LastName': registrationControls['lastName'].value,
                'Email': registrationControls['email'].value,
                'Phone': registrationControls['phone1'].value
            },
            userId: this._cub_dataService.currentUserId
        };

        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.post<any>(this._cub_dataService.apiServerUrl + '/CustomerRegistration/SearchContact', JSON.stringify(body), { headers: headers })
            .toPromise()
            .then((response) => this._cub_dataService.tryParse(response))
            .then(data => {
                let firstName = (registrationControls['firstName'].value || '').toLowerCase(),
                    lastName = (registrationControls['lastName'].value || '').toLowerCase(),
                    email = (registrationControls['email'].value || '').toLowerCase(),
                    phone = (registrationControls['phone1'].value || '').replace(/\D/g, '');

                for (let i = 0; i < data.length; ++i) {
                    data[i].Match = 0;
                    if (firstName === data[i].FirstName.toLowerCase() || lastName === data[i].LastName.toLowerCase()) {
                        ++data[i].Match;
                    }
                    if (email === data[i].Email.toLowerCase()) {
                        ++data[i].Match;
                    }
                    if (phone === data[i].Phone1.toLowerCase() || phone === data[i].Phone2.toLowerCase() || phone === data[i].Phone3.toLowerCase()) {
                        ++data[i].Match;
                    }

                }
                return lodash.orderBy(data, ['Match'], ['desc']);
            });
    }

    //Metadata calls to the API
    getPhoneTypes() {
        return this._get('PhoneTypes');
    }

    getCountries() {
        return this._get('Countries');
    }

    getSecurityQuestions() {
        return this._get('SecurityQuestions');
    }

    getContactTypes() {
        return this._get('ContactTypes');
    }

    getAccountTypes() {
        return this._get('AccountTypes');
    }

    getStates() {
        return this._get('States');
    }

    private _get(name: string) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.get(`${this._cub_dataService.apiServerUrl}/CustomerRegistration/Get${name}`, { headers: headers })
            .toPromise();
    }

    registerCustomer(customer: Object) {
        if (this.currentlyRegistering) {
            return Promise.resolve(null);
        }
        this.currentlyRegistering = true;
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const body = {
            model: customer,
            userId: this._cub_dataService.currentUserId
        };
        return this._http.post(this._cub_dataService.apiServerUrl + '/CustomerRegistration/Register', JSON.stringify(body), { headers: headers })
            .toPromise()
            .then((data) => {
                this.currentlyRegistering = false;
                return Promise.resolve(data);
            })
           
    }

    checkZipPostalCode(registrationControls: any) {
        let zipInfo = {
            state: '', //No need to pass this in
            code: registrationControls['zip'].value,
            country: registrationControls['country'].value,
            userId: this._cub_dataService.currentUserId
        }
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post(this._cub_dataService.apiServerUrl + '/CustomerRegistration/CheckZipPostalCode', JSON.stringify(zipInfo), { headers: headers })
            .toPromise();
    }

    checkUsername(registrationControls: any) {
        let usernameInfo = {
            username: registrationControls['username'].value,
            userId: this._cub_dataService.currentUserId
        }
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post(this._cub_dataService.apiServerUrl + '/CustomerRegistration/CheckUsername', JSON.stringify(usernameInfo), { headers: headers })
            .toPromise();
    }
}