﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { SortMeta } from 'primeng/primeng';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as _ from 'lodash';
import * as moment from 'moment';

import { environment } from '../../environments/environment';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_WebApiService } from './cub-webapi.service';
import * as Model from '../model';

import { pem2jwk } from 'pem-jwk';
import { JWE, util } from 'node-jose';

@Injectable()
export class Cub_DataService {
    constructor(
        private _http: HttpClient,
        private _loadingScreen: AppLoadingScreenService,
        private _webApi: Cub_WebApiService
    ) { }

    jwe_encrypt_publicKey: string;
    jwe_encrypt_content_algorithm: string;
    jwe_encrypt_algorithm: string;

    //For loading popover
    busyScreen: Model.BusyScreen = {
        busyCount: 0,
        busyText: "Preparing Screen...",
        buttonText: '',
        buttonRoute: '',
        icon: 'loading',
        smallLoader: false
    };

    //For 'Are you sure?' cancel popover
    cancelScreen: Model.CancelScreen = {
        show: false,
        cancelButtonRoute: ''
    };

    //For error popover (uses same model as 'Are you sure?')
    errorScreen: Model.ErrorScreen = {
        show: false,
        errors: [],
        route: null,
        fatalState: false,
        errorTotal: 0
    };

    //Url for Server API
    //"http://localhost:12665"; local debug //"http://usdced-msdyn01.cts.cubic.cub:12663"; //"http://usdced-msdyn01.cts.cubic.cub:12663"; // "http://localhost:12663"
    public apiServerUrl: string = environment.CUB_MSD_WEB_URL;

    public masterSearchPages: Model.Cub_FlydownOption[] = [
        //{ value: "Case", name: "Case" },
        { value: "Contact", name: "Contact" },
        //{ value: "Order", name: "Order" },
        { value: "Token", name: " Token" }
    ];
    //public searchParams: object = {};
    public masterSearchContactResults: object[] = [];
    public masterSearchTokenResults: object[] = []; //Each property of an object is a column in the grid, which then has an object with Line1, Line2, and possibly Line3
    public masterSearchOrderResults: object[] = [];
    public sortedMasterSearchContactResults: object[] = [];
    public selectedContactCustomerToVerify: object = {};
    public currentUserId: string = ''; //Used by some pages to know in what context it's running in (eg. know who to assign the CRM record to when created/updates)
    public previousPage: string = '';

    //Used to pass search values from Customer Search to Customer Registration
    private _searchValues: Model.MasterSearchValues = {};
    get searchValues() { return this._searchValues; }
    set searchValues(sv) { this._searchValues = Object.assign({}, sv); }

    /**
     * Globals are made available to Angular both synchronously and asynchronously.
     * Components do not know whether or not a global is already processed, and so
     * we use the observer pattern to alert components when globals change.
     * RxJS BehaviorSubject stores the state of our globals. Since we represent globals
     * as an object of objects, the state begins as {}. When Angular
     * receives globals, it augments the state with the new values and alerts any
     * observers of the change. If an observer (component) subscribes to the subject
     * (Globals) after they are loaded, they will receive the most recent value
     * synchronously.
     *
     * _globals alerts observers of changes and store global state;
     * Globals should be subscribed to by a component or service to receive globals;
     *
     * If subscribing to Globals, make sure to UNSUBSCRIBE when done! More info here:
     * http://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
     */
    private _globals = new BehaviorSubject<Model.Cub_Globals>({});
    public get Globals() {
        return this._globals.asObservable();
    }

    private _gridConfigs = new BehaviorSubject<Model.Cub_GridMap>({});
    public get GridConfigs() {
        return this._gridConfigs.asObservable();
    }

    /**
     * Executed during app initialization. Returns a Promise that resolves
     * once the web service URL is set. On error retrieving the URL, updates
     * the temp loading message before rethrowing the error.
     */
    executeOnLoad(): Promise<void> {
        let promise = Promise.resolve() as Promise<any>;

        if (window.Xrm) {
            this.currentUserId = this.stripGuid(Xrm.Page.context.getUserId());
            if (environment.REQUEST_URL_WITHIN_MSD) {
                promise = promise.then(() => this._webApi.getGlobalDetailAttributeValue('CUB.MSD.WEB', 'WEB.APP.URL'))
                    .then(value => this.apiServerUrl = value);
            }
        } else {
            console.warn('Unable to get current user ID, no access to Xrm');
        }

        if (window['cubicConfig']) {
            // served by web service, deprecated
            this.apiServerUrl = window['cubicConfig']['apiBaseUrl'];
            this.augmentConstants(...window['cubicConfig']['globals']);
            this.augmentGridConfigs(...window['cubicConfig']['gridConfigs']);
        } else {
            promise = promise.then(() => {
                this.fetchConstants();
                this.getGridConfigs();
            });
        }

        /*
         * Getting the JWE encrypting constants
         */
        promise = promise.then(() => {
            this.Globals
                .first(globals => !!globals['CUB.MSD.Web.Angular'])
                .subscribe(globals => {
                    this.jwe_encrypt_publicKey = globals['CUB.MSD.Web.Angular']['JWE.Encrypt.PublicKey'] as string;
                    this.jwe_encrypt_content_algorithm = globals['CUB.MSD.Web.Angular']['JWE.Encrypt.ContentAlgorithm'] as string;
                    this.jwe_encrypt_algorithm = globals['CUB.MSD.Web.Angular']['JWE.Encrypt.Algorithm'] as string;
                });
        });

        return promise
            .catch(err => {
                let header = document.getElementById('header-before-load');
                header.innerHTML = '<h1>Error initializing application. Open browser developer tools for more details.</h1>';
                return Promise.reject(err);
            });
    }

    sortMasterSearchContactResults(field: string, inAscendingOrder: boolean) {
        if (field.toLocaleLowerCase() === "BestMatch".toLocaleLowerCase()) {
            this.sortedMasterSearchContactResults = this.masterSearchContactResults;
            return;
        }
        if (!this.masterSearchContactResults ||
            !this.masterSearchContactResults[0] ||
            !this.masterSearchContactResults[0][field]) {
            console.log("Error while sorting results, master Search Contact results is null or has no field with that name.");
            return;
        }
        var sortOrder = 'desc'
        if (inAscendingOrder) {
            sortOrder = 'asc';
        }
        this.sortedMasterSearchContactResults = _.orderBy(this.masterSearchContactResults, [field], [sortOrder]);
    }

    showError(error: string, fatal: boolean = false) {
        this.errorScreen.show = true;
        this.errorScreen.errors.push(error);
        this.errorScreen.errorTotal++;

        if (fatal) {
            this.errorScreen.fatalState = true;
        }
    }

    /**
     * Displays the busy screen by incrementing its counter.
     * @param message optional message to display on screen;
     * defaults to 'Preparing Screen...'
     * @deprecated
     */
    incrementLoader(message: string = null, smallLoader: boolean = false) {
        console.warn('Cub_DataService.incrementLoader() is deprecated. Please use AppLoadingScreenService.add() instead');
        if (!!message) {
            this.busyScreen.busyText = message;
        }
        this.busyScreen.buttonRoute = '';
        this.busyScreen.icon = 'loading';
        this.busyScreen.buttonText = '';
        this.busyScreen.busyCount++;
        this.busyScreen.smallLoader = smallLoader;
    }

    /**
     * Decrements the busy screen counter. This will hide it if it reaches 0.
     * @deprecated
     */
    decrementLoader() {
        this.busyScreen.busyCount--;
    }

    /**
     * Notifies Angular's parent web resource in MSD to open an entity form.
     * See https://msdn.microsoft.com/en-us/library/jj602956.aspx#BKMK_OpenEntityForm
     * for details on setting params.
     * @param entityLogicalName logical name
     * @param recordId if omitted or null, opens a new record form
     * @param params an object of additional parameters to pass to the record form.
     */
    openMsdForm(entityLogicalName: string, recordId?: string, params?: object) {
        entityLogicalName = entityLogicalName.toLowerCase();
        if (entityLogicalName === 'customer') {
            entityLogicalName = 'account';
        }
        parent.Xrm.Utility.openEntityForm(entityLogicalName, recordId, params as Xrm.Utility.FormOpenParameters);
    }

    /**
     * Gets all Global values from the web service.
     */
    fetchConstants() {
        const allDone = this._loadingScreen.add('Loading configurable data');
        this._http.get<any>(this.apiServerUrl + '/Home/GetAllConstants')
            .finally(() => allDone())
            .subscribe(data => {
                let globals = this.tryParse(data.Body);
                this.augmentConstants(...globals);
            }, (err) => this.onApplicationError(err));
    }

    /**
     * Given a list of globals, merges them into the current globals state and
     * alert any observers of the new state.
     * @param globals comma-separated globals represented as key-value pairs,
     * the format returned by the web service.
     */
    augmentConstants(...globals: { Key: string, Value: any[] }[]) {
        let dict = this._globals.getValue();
        const globalsLen = globals.length;
        for (let i = 0; i < globalsLen; ++i) {
            let global = globals[i];
            const globalLen = global.Value.length;
            dict[global.Key] = dict[global.Key] || {};
            for (let j = 0; j < globalLen; ++j) {
                let detail = global.Value[j];
                dict[global.Key][detail.Key] = detail.Value;
            }
        }
        this._globals.next(dict);
    }

    /**
     * Attempts to display NIS error(s) as custom alert dialog(s), and logs to console
     * @param err
     */
    onApplicationError(err: any) {
        let cubError = new Model.CubError(err);
        if (cubError.handled) {
            return;
        }

        const prefix = 'App error: ';
        this.showError(prefix + cubError.error.message);
        console.error(prefix, cubError.error);
        cubError.handled = true;
        return cubError;
    }

    prependCurrentError(message: any) {
        if (typeof message !== 'string') {
            let cubError = new Model.CubError(message);
            if (!cubError.handled) {
                this.onApplicationError(cubError);
            }
            return;
        }

        let prefix = message;
        if (!prefix.match(/[-:]\s*$/)) {
            prefix += ': ';
        }
        const len = this.errorScreen.errors.length;
        let current = this.errorScreen.errors[len - 1];
        this.errorScreen.errors[len - 1] = prefix + current;
    }

    /**
     * Formats PrimeNG multi-column sort data to NIS specification query param,
     * e.g. "type.asc,createDateTime.desc"
     * @param columns An array of column sort data
     */
    formatSortQueryParam(columns: SortMeta[]): string {
        return _.map(columns, c => `${c.field}.${c.order === 1 ? 'asc' : 'desc'}`).join(',');
    }

    /**
     * Attempt to parse a JSON string into an object or array.
     * @param data
     * @returns The parsed value. On failure, returns the original value
     */
    tryParse(data: any) {
        if (typeof data !== 'string') {
            return data;
        }
        // JSON.parse() only throws SyntaxError if arg cannot be parsed.
        // Var parsed maintains its original value on failed parse
        let parsed = data;
        try {
            parsed = JSON.parse(data);
        } catch (e) { }
        return parsed;
    }

    getGridConfigs() {
        return this._http.get<any>(this.apiServerUrl + '/Home/GetGridConfigurations')
            .subscribe(next => {
                let records = this.tryParse(next);
                this.augmentGridConfigs(...records);
            });
    }

    augmentGridConfigs(...configs: Model.Cub_CustomizableGrid[]) {
        let prev = this._gridConfigs.getValue();
        for (let i = 0; i < configs.length; i++) {
            const config = configs[i];
            prev[config.id] = config;
        }
        this._gridConfigs.next(prev);
    }

    getCellValue(record: any, ...properties: string[]): any {
        if (!properties) {
            return 'ERROR';
        }
        let i;
        for (i = 0; i < properties.length && !_.has(record, properties[i]); i++);
        return _.get(record, properties[i], null);
    }

    /**
     * Fill a CubDropdown with option from a global setting
     * @param globals Globals component
     * @param dropdownSettingsName Name of the dropdown settings with the dropdown content on Globals
     * @param dropdownComponent Dropdown component
     */
    public fillCubDropdown(dropdownComponent, globals, dropdownSettingsName) {
        var optionsString = globals['CUB.MSD.Web.Angular'][dropdownSettingsName] as string;
        var options = optionsString ? optionsString.split(';') : [];
        for (var i = 0; i < options.length; i++) {
            if (options[i].indexOf(':') === -1)
                continue;
            var keyValuePair = options[i].split(':');
            if (keyValuePair.length !== 2) continue;
            dropdownComponent.options.push({
                value: keyValuePair[1],
                name: keyValuePair[0]
            } as Model.Cub_DropdownOption);
        }
        if (dropdownComponent.options && dropdownComponent.options.length) {
            dropdownComponent.value = dropdownComponent.options[0].value;
        }
    }

    /**
     * Fill a CubMultiDropdown with option from a global setting
     * @param globals Globals component
     * @param dropdownSettingsName Name of the dropdown settings with the dropdown content on Globals
     * @param dropdownComponent Dropdown component
     */
    public fillCubMultiDropdown(dropdownComponent, globals, dropdownSettingsName) {
        var responseOptionsString = globals['CUB.MSD.Web.Angular'][dropdownSettingsName] as string;
        var responseOptions = responseOptionsString ? responseOptionsString.split(';') : [];
        for (var i = 0; i < responseOptions.length; i++) {
            if (responseOptions[i].indexOf(':') === -1) continue;

            var keyValuePair = responseOptions[i].split(':');
            if (keyValuePair.length !== 2) continue;
            dropdownComponent.options.push({
                label: keyValuePair[0],
                value: keyValuePair[1],
                checked: true
            } as Model.Cub_MultiDropdownOption);
        }
    }

    guidsAreEqual(first: string, second: string): boolean {
        if (first == null || second == null) {
            return false; //TODO: think of something better
        }
        const a = first.replace(/[-{}]/g, '').toLowerCase();
        const b = second.replace(/[-{}]/g, '').toLowerCase();
        return a === b;
    }

    getGlobalsAttributeValue(globalName, globalValue): Promise<string> {
        const actionName = 'cub_GlobalGetGlobalsAttributeValue';
        if (!parent.Xrm || !parent.Xrm.Page || !parent.Xrm.Page.context) {
            console.warn(`Unable to call MSD custom action "${actionName}", cannot access Xrm`);
            return Promise.resolve(null);
        }
        const url = parent.Xrm.Page.context.getClientUrl();
        const fullVersion = parent.Xrm.Page.context.getVersion();
        const version = _.take(fullVersion.split('.'), 2).join('.');
        const path = `/api/data/v${version}/`;
        const body = {
            'Global': globalName,
            'GlobalDetail': globalValue
        };
        const headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'OData-MaxVersion': '4.0',
            'OData-Version': '4.0'
        });
        return this._http.post<any>(url + path + actionName, body, { headers })
            .toPromise()
            .then(response => response.AttributeValue);
    }

    /**
     * Convert a GUID to a stripped-down format.
     * Ex. '{00000000-AAAA-BBBB-CCCC-0123456789AB}' -> '00000000aaaabbbbcccc0123456789ab'
     * @param id
     */
    stripGuid(id: string) {
        if (id == null) {
            return id;
        }
        return id.replace(/[\{\-\}]/g, '').toLowerCase();
    }

    /**
     * Get the nonce code from the NIS API
     * The nonce is required for all encrypted calls
     */
    getPaymentRepositoryNonce(): Promise<any> {
        return this._http.get<any>(this.apiServerUrl + '/PaymentRepository/GetNonce')
            .toPromise();
    }

    /**
     * Encrypt the bank card information using JWE
     * The nonce code is required
     * @param cardNumber
     * @param expirationMonth
     * @param expirationYear
     */
    encryptBankcard(cardNumber: string, expirationMonth: string, expirationYear: string):
        Promise<any> {
        // Convert the year from YY to 20YY
        expirationYear = "20" + expirationYear;
        let this_ = this;
        let encryptPromise: Promise<any> = new Promise(function (resolve, reject) {
            this_.getPaymentRepositoryNonce()
                .then((nonceResponse) => {
                    var publicKey = atob(this_.jwe_encrypt_publicKey);
                    var jwk = pem2jwk(publicKey);
                    var nonceResponseBody = JSON.parse(nonceResponse.Body);
                    cardNumber = cardNumber.replace(/-/g, ''); // Remove the '-' from the credit card number
                    let payload = {
                        "cardNumber": cardNumber,
                        "expirationMonth": expirationMonth,
                        "expirationYear": expirationYear,
                        "cardholderName": "TestName",
                        "nonce": nonceResponseBody.nonce
                    };
                    const input = util.asBuffer(JSON.stringify(payload));
                    JWE.createEncrypt(
                        { format: 'compact', "contentAlg": this_.jwe_encrypt_content_algorithm },
                        { "alg": this_.jwe_encrypt_algorithm, "kty": jwk.kty, "n": jwk.n, "e": jwk.e })
                        .update(input, "utf8")
                        .final()
                        .then((response) => {
                            var encryptedData = {
                                encryptedToken: response,
                                nonce: nonceResponseBody.nonce
                            };
                            resolve(encryptedData);
                        }, (error) => {
                            reject(error);
                        });
                });
        });

        return encryptPromise;
    }
}