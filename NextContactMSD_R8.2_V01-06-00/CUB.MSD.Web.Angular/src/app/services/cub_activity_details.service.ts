import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

export interface ActivityDetailParams{
    customerId: string,
    transitaccountId: string,
    subsystemId: string,
    activityId: string,
    contactId?: string,
    contact?: string
    actionType?: string,
    location?: string,
    device?: string,
    sourceIpAddress?: string,
    type?: string,
    activitySubject?: string,
    activityCategory?: string,
    createDateTime?: string,
    channel?: string,
    origin?: string,
    globalTxnId?: string,
    transitAccountId?: string,
    subsystemDescription?: string,
    trackingInfo?: string,
    userName?: string,
    activityData?: string,
    getActivityDetailsFor: string
}

export interface WSName
{
    name: string,
    Value: string
}

export interface WSActivityInfo
{
    
}

export interface TrackingInfo
{
    trackingId: string,
    trackingType: string
}

@Injectable()
export class CubActivityDetailsService {

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getActivityDetails(queryParams: ActivityDetailParams) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Activity/GetActivityDetails', { params: params })
            .toPromise();
    }

}
