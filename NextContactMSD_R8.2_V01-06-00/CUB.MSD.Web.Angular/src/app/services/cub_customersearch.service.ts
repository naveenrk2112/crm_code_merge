﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';
import { orderBy } from 'lodash';

@Injectable()
export class Cub_CustomerSearchService {

    constructor(private _http: HttpClient,
        private _cub_dataService: Cub_DataService) {
    }

    //Call to search customer
    searchCustomer() {
        const body = {
            contactSearchData: {
                'ContactType': this._cub_dataService.searchValues['customerType'],
                'FirstName': this._cub_dataService.searchValues['firstName'],
                'LastName': this._cub_dataService.searchValues['lastName'],
                'Email': this._cub_dataService.searchValues['email'],
                'Phone': this._cub_dataService.searchValues['phone']
            },
            userId: this._cub_dataService.currentUserId
        };

        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this._http.post<any>(this._cub_dataService.apiServerUrl + '/CustomerRegistration/SearchContact', body, { headers: headers })
            .toPromise()
            .then((response) => this._cub_dataService.tryParse(response))
            .then(data => {
                let firstName = (this._cub_dataService.searchValues["firstName"] || '').toLowerCase(),
                    lastName = (this._cub_dataService.searchValues["lastName"] || '').toLowerCase(),
                    email = (this._cub_dataService.searchValues["email"] || '').toLowerCase(),
                    phone = (this._cub_dataService.searchValues["phone"] || '').replace(/\D/g, '');

                for (let i = 0; i < data.length; ++i) {
                    data[i].Match = 0;
                    if (firstName === data[i].FirstName.toLowerCase() || lastName === data[i].LastName.toLowerCase()) {
                        ++data[i].Match;
                    }
                    if (email === data[i].Email.toLowerCase()) {
                        ++data[i].Match;
                    }
                    if (phone === data[i].Phone1.toLowerCase() || phone === data[i].Phone2.toLowerCase() || phone === data[i].Phone3.toLowerCase()) {
                        ++data[i].Match;
                    }

                }
                return orderBy(data, ['Match'], ['desc']);
            });
    }
}