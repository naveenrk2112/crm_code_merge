﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_CacheService } from './cub-cache.service';
import { Cub_DataService } from './cub_data.service';
import { orderBy } from 'lodash';

export class BankcardTravelToken {
    constructor(tokenType: string) {
        this.tokenType = tokenType;
    }
    tokenType: string
    encryptedToken: string;
    nonce: string;
}

export class EncryptionDetails {
    publickeyModulus: string;
    publickeyExponent: string;
}

//Used to call service for Master Search
@Injectable()
export class Cub_MasterSearchService {
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService,
        private _cache: Cub_CacheService
    ) { }

    private readonly _linkDataKey = 'masterSearchLinkData';

    searchCustomer() {
        const body = {
            contactSearchData: {
                //'ContactType': this._cub_dataservice.searchValues["contactType"],
                'FirstName': this._cub_dataService.searchValues["firstName"],
                'LastName': this._cub_dataService.searchValues["lastName"],
                'Email': this._cub_dataService.searchValues["email"],
                'Phone': this._cub_dataService.searchValues["phone"]
            },
            userId: this._cub_dataService.currentUserId
        };

        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._http.post<any>(`${this._cub_dataService.apiServerUrl}/MasterSearch/SearchContact`, JSON.stringify(body), { headers: headers })
            .toPromise()
            .then((response) => this._cub_dataService.tryParse(response))
            .then(data => {
                let firstName = (this._cub_dataService.searchValues["firstName"] || '').toLowerCase(),
                    lastName = (this._cub_dataService.searchValues["lastName"] || '').toLowerCase(),
                    email = (this._cub_dataService.searchValues["email"] || '').toLowerCase(),
                    phone = (this._cub_dataService.searchValues["phone"] || '').replace(/\D/g, '');

                for (let i = 0; i < data.length; ++i) {
                    data[i].Match = 0;
                    if (firstName === data[i].FirstName.toLowerCase() || lastName === data[i].LastName.toLowerCase()) {
                        ++data[i].Match;
                    }
                    if (email === data[i].Email.toLowerCase()) {
                        ++data[i].Match;
                    }
                    if (phone === data[i].Phone1.toLowerCase() || phone === data[i].Phone2.toLowerCase() || phone === data[i].Phone3.toLowerCase()) {
                        ++data[i].Match;
                    }

                }
                this._cub_dataService.masterSearchContactResults = orderBy(data, ['Match'], ['desc']);
                return data;
            });
    }

    searchToken() {
        let this_ = this;
        let TokenType: string = this._cub_dataService.searchValues["tokenType"];
        let SubSystem: string = this._cub_dataService.searchValues["subSystem"];
        let BankCardNumber: string = this._cub_dataService.searchValues["bankCardNumber"];
        const splitDate = this._cub_dataService.searchValues["expirationDate"].split('/');
        let ExpirationMonth: string = splitDate[0]
        let ExpirationYear: string = splitDate[1];
        const body = {
            model: {
                'TokenType': TokenType,
                'SubSystem': SubSystem,
                'BankCardNumber': BankCardNumber,
                "ExpirationMonth": ExpirationMonth,
                "ExpirationYear": ExpirationYear
            },
            userId: this._cub_dataService.currentUserId
        };
        if (this._cub_dataService.searchValues["tokenType"] != "Bankcard") {
            const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
            return this._http.post<any>(`${this._cub_dataService.apiServerUrl}/MasterSearch/SearchCard`, JSON.stringify(body), { headers: headers })
                .toPromise()
                .then((response) => this_._cub_dataService.tryParse(response));
        } else {
            return this_._cub_dataService.encryptBankcard(body.model.BankCardNumber, body.model.ExpirationMonth, body.model.ExpirationYear)
                .then((response) => {
                    var body = {
                        model: {
                            'TokenType': TokenType,
                            'SubSystem': SubSystem,
                            "encryptedToken": response.encryptedToken,
                            "nonce": response.nonce
                        }
                    };
                    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
                    return this_._http.post<any>(`${this._cub_dataService.apiServerUrl}/MasterSearch/SearchCard`, JSON.stringify(body), { headers: headers })
                        .toPromise()
                        .then((response) => this_._cub_dataService.tryParse(response));
                });
        }
    }

    getAccountTypes() {
        return this._http.get(this._cub_dataService.apiServerUrl + '/CustomerRegistration/GetAccountTypes')
            .toPromise();
    }

    getTokenTypes() {
        return this._cache.retrieve('tokenTypes',
            () => this._http.get(this._cub_dataService.apiServerUrl + '/SubSystem/GetTokenTypeMapping')
                .toPromise()
                .then((response) => this._cub_dataService.tryParse(response))
                .then(data => data['mappings']));
    }

    getCachedLinkData() {
        return this._cache.retrieve(this._linkDataKey)
            .then(data => {
                this._cache.remove(this._linkDataKey);
                return data;
            });
    }

    cacheLinkData(data) {
        this._cache.store(this._linkDataKey, data);
    }
}