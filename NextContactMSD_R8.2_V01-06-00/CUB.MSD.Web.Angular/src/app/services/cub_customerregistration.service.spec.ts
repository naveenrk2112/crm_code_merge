﻿import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Cub_CustomerRegistrationService } from './cub_customerregistration.service';
import { Cub_DataService } from './cub_data.service';
import { isEqual } from 'lodash';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';

describe('Cub_CustomerRegistrationService', () => {
    let service: Cub_CustomerRegistrationService;
    let dataService: Cub_DataService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                Cub_CustomerRegistrationService
            ]
        });

        service = TestBed.get(Cub_CustomerRegistrationService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => http.verify());

    it('#searchCustomer should resolve to an array of search results based on the argument passed to it', async(() => {
        const SEARCH_RESULTS = [
            {
                ContactId: '00000000-0000-0000-0000-000000000001',
                FirstName: 'Tom',
                LastName: 'Fake',
                Email: '',
                Phone1: '',
                Phone2: '',
                Phone3: '',
                Match: 1
            }, {
                ContactId: '00000000-0000-0000-0000-000000000002',
                FirstName: 'Jerry',
                LastName: 'Fake',
                Email: '',
                Phone1: '',
                Phone2: '',
                Phone3: '',
                Match: 1
            }
        ];
        const SEARCH_VALUES = {
            firstName: { value: '*' },
            lastName: { value: 'Fake' },
            email: { value: 'fake@email.com' },
            phone1: { value: '3125559879' }
        }

        service.searchCustomer(SEARCH_VALUES)
            .then(data => expect(data).toEqual(SEARCH_RESULTS));

        let request = http.expectOne(req => {
            let body = JSON.parse(req.body);
            console.log(body.Email === SEARCH_VALUES.email.value);
            return req.method === 'POST'
                && !!req.urlWithParams.match(/CustomerRegistration\/SearchContact/)
                && body.contactSearchData.FirstName === SEARCH_VALUES.firstName.value
                && body.contactSearchData.LastName === SEARCH_VALUES.lastName.value
                && body.contactSearchData.Email === SEARCH_VALUES.email.value
                && body.contactSearchData.Phone === SEARCH_VALUES.phone1.value;
        });

        request.flush(SEARCH_RESULTS);
    }));

    it('#getPhoneTypes should resolve to an array of key-value pairs', async(() => {
        const PHONE_TYPES = [
            {
                Key: 100000001,
                Value: 'Payphone'
            }, {
                Key: 100000002,
                Value: 'Rotary'
            }
        ];

        service.getPhoneTypes()
            .then(data => expect(data).toEqual(PHONE_TYPES));
        http.expectOne(dataService.apiServerUrl + '/CustomerRegistration/GetPhoneTypes')
            .flush(PHONE_TYPES);
    }));

    it('#getCountries should resolve to an array of key-value pairs', async(() => {
        const COUNTRIES = [
            {
                Key: '00000000-0000-0000-0000-000000000001',
                Value: 'Czechoslovakia'
            }, {
                Key: '00000000-0000-0000-0000-000000000002',
                Value: 'Union of Soviet Socialist Republics'
            }
        ];

        service.getCountries()
            .then(data => expect(data).toEqual(COUNTRIES));
        http.expectOne(dataService.apiServerUrl + '/CustomerRegistration/GetCountries')
            .flush(COUNTRIES);
    }));

    it('#getSecurityQuestions should resolve to an array of key-value pairs', async(() => {
        const SECURITY_QUESTIONS = [
            {
                Key: '00000000-0000-0000-0000-000000000001',
                Value: 'What is your favorite transportation system?'
            }, {
                Key: '00000000-0000-0000-0000-000000000002',
                Value: 'What is your favorite form of precipitation?'
            }
        ];

        service.getSecurityQuestions()
            .then(data => expect(data).toEqual(SECURITY_QUESTIONS));
        http.expectOne(dataService.apiServerUrl + '/CustomerRegistration/GetSecurityQuestions')
            .flush(SECURITY_QUESTIONS);
    }));

    it('#getContactTypes should resolve to an array of key-value pairs', async(() => {
        const CONTACT_TYPES = [
            {
                Key: '00000000-0000-0000-0000-000000000001',
                Value: 'Primary'
            }, {
                Key: '00000000-0000-0000-0000-000000000002',
                Value: 'Secondary'
            }
        ];

        service.getContactTypes()
            .then(data => expect(data).toEqual(CONTACT_TYPES));
        http.expectOne(dataService.apiServerUrl + '/CustomerRegistration/GetContactTypes')
            .flush(CONTACT_TYPES);
    }));

    it('#getAccountTypes should resolve to an array of key-value pairs', async(() => {
        const ACCOUNT_TYPES = [
            {
                Key: '00000000-0000-0000-0000-000000000001',
                Value: 'Individual'
            }, {
                Key: '00000000-0000-0000-0000-000000000002',
                Value: 'Hivemind'
            }
        ];

        service.getAccountTypes()
            .then(data => expect(data).toEqual(ACCOUNT_TYPES));
        http.expectOne(dataService.apiServerUrl + '/CustomerRegistration/GetAccountTypes')
            .flush(ACCOUNT_TYPES);
    }));

    it('#getStates should resolve to an array of key-value pairs', async(() => {
        const STATES = [
            {
                Key: '00000000-0000-0000-0000-000000000001',
                Value: 'LU - Luna'
            }, {
                Key: '00000000-0000-0000-0000-000000000002',
                Value: 'NA - New Canada'
            }
        ];

        service.getStates()
            .then(data => expect(data).toEqual(STATES));
        http.expectOne(dataService.apiServerUrl + '/CustomerRegistration/GetStates')
            .flush(STATES);
    }));

    it('#checkZipPostalCode should resolve to an array of objects', async(() => {
        const ADDRESS_RESULTS = [
            {
                ID: '00000000-0000-0000-0000-000000000001',
                City: 'Philadelphia',
                State: {
                    Key: '00000000-0000-0000-0000-000000000002',
                    Value: 'Pennsylvania'
                },
                Country: {
                    Key: '00000000-0000-0000-0000-000000000003',
                    Value: 'United States of America'
                }
            }
        ];
        const VALUES = {
            zip: { value: 15001 },
            country: { value: '00000000-0000-0000-0000-000000000003' }
        };

        service.checkZipPostalCode(VALUES)
            .then(data => expect(data).toEqual(ADDRESS_RESULTS));
        http.expectOne(req => {
            let body = JSON.parse(req.body);
            return !!req.urlWithParams.match(/CustomerRegistration\/CheckZipPostalCode/)
                && body.state === ''
                && body.code === VALUES.zip.value
                && body.country === VALUES.country.value;
        }).flush(ADDRESS_RESULTS);
    }));

    it('#checkUsername should resolve to an object', async(() => {
        const RESPONSE = {
            isTrue: true,
            isNotFalse: false,
            values: []
        };
        const VALUES = {
            username: { value: 'stockemail@hotmail.com' }
        };

        service.checkUsername(VALUES)
            .then(data => expect(data).toEqual(RESPONSE));
        http.expectOne(req => {
            return !!req.urlWithParams.match(/CustomerRegistration\/CheckUsername/)
                && JSON.parse(req.body).username === VALUES.username.value;
        }).flush(RESPONSE);
    }));

    it('#registerCustomer should POST a new customer from an objcet argument and resolve to a string', async(() => {
        const RESPONSE = '00000000-0000-0000-0000-000000000001';
        let customer = { some: 'thing', goes: 'here' };

        service.registerCustomer(customer)
            .then(data => expect(data).toEqual(RESPONSE));
        http.expectOne(req => {
            return !!req.urlWithParams.match(/CustomerRegistration\/Register/)
                && isEqual(JSON.parse(req.body)['model'], customer);
        }).flush(RESPONSE);
    }));
});