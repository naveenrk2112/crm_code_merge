import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

export interface CustomerActivitiesSearchCriteria{
    customerId: string,
    contactId: string,
    startDateTime?: string,
    endDateTime?: string,
    type?: string,
    activitySubject: string,
    activityCategory: string,
    channel?: string,
    origin: string,
    globalTxnId: string,
    userName?: string,
    sortBy?: string,
    offset?: number,
    limit?: number,
    crmSessionId?: string
}

@Injectable()
export class CubCustomerService {

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getActivities(queryParams: CustomerActivitiesSearchCriteria) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Customer/GetActivities', { params: params })
            .toPromise();
    }

    getContactsForVoidTrip(customerId) {
        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Customer/getContactsForVoidTrip?customerId=' + customerId)
            .toPromise();
    }

    getPrimaryContact(customerId) {
        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/Customer/GetPrimaryContact?customerId=' + customerId)
            .toPromise();
    }
}
