﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Cub_CacheService } from './cub-cache.service';
import { Cub_DataService } from './cub_data.service';
import * as Model from '../model';

import * as uuid from 'uuid/v4';
export interface CustomerOrderHistorySearchCriteria {
    userId: string,
    customerId: string,
    startDateTime?: string,
    endDateTime?: string,
    orderType?: string,
    orderStatus?: string,
    paymentStatus?: string,
    orderId?: string,
    userName?: string,
    sortBy?: string,
    offset?: number,
    limit?: number,
    realTime?: boolean
}


@Injectable()
export class Cub_CustomerOrderService {
    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService,
        private _cache: Cub_CacheService
    ) { }

    readonly orderIdKey = 'orderId';

    /**
     * Sends a GET request to the web controller for a list of order history
     * records. This rejects if customerId is not given or if the response
     * indicates an error.
     * @param customerId
     * @param filters an object containing optional filter parameters
     * @returns a promise that resolves to an object containing a list of order
     * history records and a total count field for table paging.
     */
    getOrderHistory(queryParams: CustomerOrderHistorySearchCriteria) {
        let params = new HttpParams();
        for (let qp in queryParams) {
            if (queryParams[qp] != null) {
                params = params.set(qp, queryParams[qp]);
            }
        }
        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/CustomerOrder/GetOrderHistory', { params })
            .toPromise();

        //return this._http.get<Model.WSGetCustomerOrderHistoryResponse>(this._cub_dataService.apiServerUrl + '/CustomerOrder/GetOrderHistory', { params })
        //    .toPromise();
    }

    getOrderHistoryDetail(customerId: string, orderId: string) {
        if ((!customerId) || (!orderId)) {
            return Promise.reject('No Customer/Order id provided');
        }
        let params = new HttpParams()
            .set('customerId', customerId)
            .set('orderId', orderId);
        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/CustomerOrder/GetOrderHistoryDetail', { params: params })
            .toPromise();
    }

    getOrder(customerId: string, orderId: string) {
        if ((!customerId) || (!orderId)) {
            return Promise.reject('No Customer/Order id provided');
        }
        var params = new HttpParams()
            .set('customerId', customerId)
            .set('orderId', orderId);

        return this._http.get<any>(this._cub_dataService.apiServerUrl + '/CustomerOrder/GetOrder', { params: params })
            .toPromise();
    }

    resolveBalance(body: Model.ResolveBalanceArgs): Promise<Model.WSDebtCollectResponse> {
        const cacheKey = `${body.subsystem}-${body.subsystemAccountReference}-resolve-balance-ref-id`;
        return this._cache.retrieve<string>(cacheKey)
            .then(id => {
                let clientRefId = id;
                if (clientRefId == null) {
                    clientRefId = uuid();
                    this._cache.store(cacheKey, clientRefId);
                }
                return clientRefId;
            })
            .then(clientRefId => {
                body.clientRefId = clientRefId;
                const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
                return this._http.post(this._cub_dataService.apiServerUrl + '/CustomerOrder/GetDebtCollection', body, { headers })
                    .toPromise();
            })
            .then((response: any) => response['Body'])
            .then(data => this._cub_dataService.tryParse(data) as Model.WSDebtCollectResponse);
    }

    cacheOrderId(orderId) {
        this._cache.store(this.orderIdKey, +orderId);
    }

    getCachedOrderId() {
        return this._cache.retrieve<number>(this.orderIdKey)
            .then(data => {
                this.clearCachedOrderId();
                return data;
            });
    }

    clearCachedOrderId() {
        this._cache.remove(this.orderIdKey);
    }
}