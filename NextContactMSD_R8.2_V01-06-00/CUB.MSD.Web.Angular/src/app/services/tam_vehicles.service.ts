﻿import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Cub_DataService } from './cub_data.service';

export interface Vehicle {
    color: string,
    year: number,
    uidCreated: number,
    origin: string,
    axles: string,
    plateNum: string,
    type: string,
    isActive: string,
    tsUpdated: string,
    customerId: string,
    tsCreated: string,
    model: string,
    id: string,
    state: string,
    licensePlateNo: string,
    uidUpdated: number,
    make: string
}

export interface VehicleList {
    vehicles: Vehicle[]
}


@Injectable()
export class Tam_VehiclesService {

    constructor(
        private _http: HttpClient,
        private _cub_dataService: Cub_DataService
    ) { }

    getVehicles(customerId: string) {
        return this._http.get(this._cub_dataService.apiServerUrl +
            '/Tolling/GetVehicles?customerId=' + customerId)
            .toPromise();
    }

}
