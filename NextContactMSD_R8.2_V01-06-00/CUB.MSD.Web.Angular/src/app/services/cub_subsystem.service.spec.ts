﻿import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Cub_SubsystemService } from './cub_subsystem.service';
import { Cub_DataService } from './cub_data.service';
import { Cub_CacheService } from './cub-cache.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_CacheServiceStub } from '../testutilities/cub-cache.service.stub';

import * as Model from '../model';
import * as moment from 'moment';

describe('Cub_SubsystemService', () => {
    const SUBSYSTEM_ID = 'Test-Subsystem';
    const TRANSIT_ACCOUNT_ID = 'Test-Transit-Account';
    let service: Cub_SubsystemService;
    let dataService: Cub_DataService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                Cub_SubsystemService,
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: Cub_CacheService, useClass: Cub_CacheServiceStub }
            ]
        });

        service = TestBed.get(Cub_SubsystemService);
        dataService = TestBed.get(Cub_DataService);
        http = TestBed.get(HttpTestingController);
    });

    afterEach(() => http.verify());

    it('#getSubsystemStatus should reject if transit account or subsystem are unspecified', async(() => {
        service.getSubsystemStatus(null, null)
            .then(data => fail('getSubsystemStatus should have rejected'))
            .catch(err => expect(err).toBeDefined());
    }));

    it('#getSubsystemStatus should reject if the web controller responds with an error', async(() => {
        service.getSubsystemStatus(SUBSYSTEM_ID, TRANSIT_ACCOUNT_ID)
            .then(data => fail('getSubsystemStatus should have rejected with a server error'))
            .catch(err => expect(err).toBeDefined());
        http.expectOne(req => !!req.url.match('/TransitAccount/GetAccountStatus'))
            .flush(null, { status: 500, statusText: 'TEST_SERVER_ERROR' });
    }));

    it('#getSubsystemStatus should return an object containing balance, pass, and token lists', async(() => {
        const RESPONSE = {
            hdr: {
                result: 'Successful'
            },
            passes: [],
            purses: [],
            tokens: []
        };
        service.getSubsystemStatus(SUBSYSTEM_ID, TRANSIT_ACCOUNT_ID)
            .then(data => {
                expect(data).toBeDefined();
                expect(data.passes.length).toEqual(RESPONSE.passes.length);
                expect(data.purses.length).toEqual(RESPONSE.purses.length);
                expect(data.tokens.length).toEqual(RESPONSE.tokens.length);
            });
        http.expectOne(req => !!req.url.match('/TransitAccount/GetAccountStatus'))
            .flush(RESPONSE);
    }));

    it('#getSubsystemStatus should return data', async(() => {
        const RESPONSE = {
            hdr: {
                result: 'Successful'
            },
            passes: [
                {
                    deactivatedDateTime: '2017-01-01T00:00:00Z',
                    createdDateTime: '2017-01-01T00:00:00Z',
                    endDateTime: '2017-01-01T00:00:00Z',
                    expirationDateTime: '2017-01-01T00:00:00Z',
                    startDateTime: '2017-01-01T00:00:00Z',
                    deactivatedReason: 'TestReason',
                    durationType: 'TestType',
                    fareProductTypeId: 'TestFareProductTypeId',
                    passDescription: 'TestDescription',
                    passSKU: 'TestSKU',
                    passSerialNbr: 'TestSerialNumber',
                    remainingDuration: 100,
                    supportsAutoload: false
                }
            ],
            purses: [
                {
                    balance: 100,
                    purseRestrictionDescription: 'Test'
                }
            ],
            tokens: [
                {
                    status: 'TestStatus',
                    statusChangeDateTime: '2017-01-01T00:00:00Z',
                    tokenInfo: {
                        maskedPAN: '1234',
                        tokenNickname: 'TestToken'
                    },
                    tokenType: 'TestType',
                    tokenLastUsageDetails: {
                        device: 'TestDevice',
                        lastUsageStatus: 'TestStatus',
                        location: 'TestLocation',
                        transactionDateTime: '2017-01-01T00:00:00Z'
                    }
                }
            ]
        };

        service.getSubsystemStatus(SUBSYSTEM_ID, TRANSIT_ACCOUNT_ID)
            .then(data => {
                expect(data).toBeDefined();
                expect(data.purses.length).toEqual(RESPONSE.purses.length);
                expect(data.passes.length).toEqual(RESPONSE.passes.length);
                expect(data.tokens.length).toEqual(RESPONSE.tokens.length);
            });
        http.expectOne(req => !!req.url.match('/TransitAccount/GetAccountStatus'))
            .flush(RESPONSE);
    }));
});