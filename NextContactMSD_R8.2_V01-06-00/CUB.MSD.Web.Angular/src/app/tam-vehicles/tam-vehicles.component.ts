import { Component, Input, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { Tam_VehiclesService, Vehicle, VehicleList } from '../services/tam_vehicles.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { DataTableModule, SharedModule, LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'tam-vehicles',
  templateUrl: './tam-vehicles.component.html',
  providers: [Tam_VehiclesService]
})
export class Tam_VehiclesComponent implements OnInit {

    VehiclesArray: any[];
    vehicleDetail: Vehicle[];
    /* Filter values */
    customerIdFilter: string;
    rowsPerPage: number = 10;

    @Input() customerId: string;
    showAddVehicles: boolean = false;

    constructor(
        private _tam_vehiclesservice: Tam_VehiclesService,
        private _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
      const done = this._loadingScreen.add("Loading Vehicles...");
      let queryParams = this._route.snapshot.queryParams;
      this.customerId = queryParams['customerId'];

      // temp for fake NIS
      //this.customerId = '123';

      this._cub_dataService.Globals
          .first(globals => !!globals['CUB.MSD.Web.Angular'])
          .subscribe(globals => {
              this.rowsPerPage = globals['CUB.MSD.Web.Angular']['TAM.Vehicles.NumRows'] as number;
          });

      this._tam_vehiclesservice.getVehicles(this.customerId)
          .then((data: any) => {
              if (data && data.vehicles) {
                  this.vehicleDetail = data.vehicles;
              }
          })
          .catch(err => this._cub_dataService.prependCurrentError('Error getting vehicles'))
          .then(() => done());
  }

  cancelAddVehicle(event) {
      this.showAddVehicles = false;
      if (event === false) {
          const done = this._loadingScreen.add();
          this._tam_vehiclesservice.getVehicles(this.customerId)
              .then((data: any) => {
                  if (data && data.vehicles) {
                      this.VehiclesArray = data.vehicles;
                  }
              })
              .catch(err => this._cub_dataService.prependCurrentError('Error getting vehicles'))
              .then(() => done());
      }
  }

  addVehicleClicked() {
      this.showAddVehicles = true;
  }

}
