import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cub_EditFundingSourceComponent } from './cub-edit-fundingsource.component';

describe('Cub_EditFundingSourceComponent', () => {
    let component: Cub_EditFundingSourceComponent;
    let fixture: ComponentFixture<Cub_EditFundingSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [Cub_EditFundingSourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Cub_EditFundingSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should be created', () => {
    expect(component).toBeTruthy();
  });
});
