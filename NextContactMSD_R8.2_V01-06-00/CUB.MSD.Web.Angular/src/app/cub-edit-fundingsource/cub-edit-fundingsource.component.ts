import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_FundingSourceService, FundingSourceExt, FundingSourceList } from '../services/cub_fundingsource.service';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_Button, Cub_Textbox, Cub_Dropdown, Cub_Checkbox } from '../model';
import * as lodash from 'lodash';

@Component({
    selector: 'cub-edit-fundingsource',
    templateUrl: './cub-edit-fundingsource.component.html',
    providers: [Cub_OneAccountService, Cub_FundingSourceService]
})
export class Cub_EditFundingSourceComponent implements OnInit {

    @Input() accountId: string;
    @Input() fundingSourceId: string;
    @Output() cancelClicked = new EventEmitter<boolean>();

    controls: any;
    addresses: any;
    states: any;
    isNewAddress: boolean = true;
    zipcodeId: string;
    countryId: string;
    isPrimary: boolean;
    show: boolean;
    fundingSource: FundingSourceExt;

    controlsOrder: string[] = ["status", "paymentType", "creditCardType", "creditCardNumber", "creditCardName", "billingAddress",
        "address1", "address2", "city", "state"];

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_oneaccountService: Cub_OneAccountService,
        private _cub_fundingSourceService: Cub_FundingSourceService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        if (this.accountId == null || this.fundingSourceId == null) {
            return;
        }
        this.zipcodeId = null;
        this.countryId = null;
        this.createControls();
        this.initPaymentType();
        this.getStates("US");
        this.isPrimary = true;
        this.show = true;

        const done = this._loadingScreen.add();
        let getAddresses = this._cub_oneaccountService.getAddresses(this.accountId);
        let getCustomer = this._cub_fundingSourceService.getFundingSourceById(this.accountId, this.fundingSourceId);
        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => this.processConstants(globals));

        Promise.all([getAddresses, getCustomer])
            .then(([addresses, customer]) => {
                this.processAddresses(addresses);
                this.processCustomer(customer);
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError("Error loading edit funding sources: ");
            })
            .then(() => done());
    }

    processAddresses(addressData: any) {
        if (addressData) {
            this.addresses = addressData;
            console.log(this.addresses);
            this.controls.billingAddress.options = [
                {
                    value: "0",
                    id: "0",
                    name: "Enter New Address"
                }
            ];
            for (var i = 0; i < this.addresses.length; i++) {
                var address = this.addresses[i];
                this.controls.billingAddress.options.push({
                    value: address['id'],
                    id: address['id'],
                    name: address['text']
                });
            }
        }
    }

    processCustomer(customer: any) {
        console.log(customer);
        if (customer) {
            if (customer.fundingSource) {
                this.fundingSource = customer.fundingSource;
                console.log(this.addresses.length);
                for (var i = 0; i < this.addresses.length; i++) {
                    var address = this.addresses[i];
                    if (address.id === this.fundingSource.billingAddressId) {
                        this.controls.billingAddress.value = address.id;
                        this.isNewAddress = false;
                        break;
                    }
                }
                this.controls.creditCardName.value = this.fundingSource.creditCard.nameOnCard;
                console.log(this.controls.creditCardName.value);
                this.controls.creditCardNumber.value =
                    "**** **** **** " + this.fundingSource.creditCard.maskedPan;
                this.controls.creditCardNumber.isDisabled = true;
                this.controls.creditCardExpDate.value = this.fundingSource.creditCard.cardExpiryMMYY;
                this.controls.creditCardType.value = this.fundingSource.creditCard.creditCardType;
                this.controls.creditCardType.isDisabled = true;
                this.controls.status.value = this.fundingSource.status;
                this.controls.status.isDisabled = true;
            }
        }
    }

    processConstants(globals: any) {
        //Address line 1/2 Configuration
        if (globals.CustomerRegistration.AddressLine1_MaxLength) {
            this.controls.address1.maxLength = globals.CustomerRegistration.AddressLine1_MaxLength;
        }
        if (globals.CustomerRegistration.AddressLine2_MaxLength) {
            this.controls.address2.maxLength = globals.CustomerRegistration.AddressLine2_MaxLength;
        }
        //City Configuration
        if (globals.CustomerRegistration.City_MaxLength) {
            this.controls.city.maxLength = globals.CustomerRegistration.City_MaxLength;
        }
        //Zip Configuration
        if (globals.CustomerRegistration.Zip_MaxLength) {
            this.controls.zipcode.maxLength = globals.CustomerRegistration.Zip_MaxLength;
        }
        if (globals.CustomerRegistration.Zip_ValidationRegEx) {
            this.controls.zipcode.validationRegEx = new RegExp(globals.CustomerRegistration.Zip_ValidationRegEx);
        }
    }

    getStates(country: string) {
        const done = this._loadingScreen.add();
        this.states = this._cub_oneaccountService.getStates(country)
            .then((data: any) => {
                if (data) {
                    this.states = data;
                    this.controls.state.options = [{
                        value: "0",
                        id: "0",
                        name: "Select..."
                    }];
                    for (var i = 0; i < this.states.length; i++) {
                        var state = this.states[i];
                        this.controls.state.options.push({
                            value: state['Key'],
                            id: state['Key'],
                            name: state['Value']
                        });
                    }
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError("Error getting States");
            })
            .then(() => done());
    }

    initPaymentType() {
        var creditCard = {
            value: "1",
            name: "Credit Card"
        };
        this.controls.paymentType.options.push(creditCard);
        this.controls.paymentType.value = creditCard.value;
    }

    getAddress(oneAccountId: string) {
        const done = this._loadingScreen.add();
        this._cub_oneaccountService.getAddresses(oneAccountId)
            .then((data: any) => {
                if (data) {
                    this.addresses = data;
                    console.log(this.addresses);

                    this.controls.billingAddress.options = [{
                        value: "0",
                        id: "0",
                        name: "Enter New Address"
                    }];
                    for (var i = 0; i < this.addresses.length; i++) {
                        var address = this.addresses[i];
                        this.controls.billingAddress.options.push({
                            value: address['id'],
                            id: address['id'],
                            name: address['text']
                        });
                    }
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError("Error getting address");
            })
            .then(() => done());
    }

    billingAddressOnChange() {
        this.isNewAddress = ("0" === this.controls.billingAddress.value);
    }

    validateRequiredControl(control: any) {
        control.isErrored = !control.value;
        control.showErrorMessage = control.isErrored;
    }

    validateCreditCardNumber() {
        this.validateRequiredControl(this.controls.creditCardNumber);
    }

    validateCreditCardExpDate() {
        this.validateRequiredControl(this.controls.creditCardExpDate);
        if (this.controls.creditCardExpDate.value) {
            if (!this.controls.creditCardExpDate.validationRegEx.test(this.controls.creditCardExpDate.value)) {
                this.controls.creditCardExpDate.isInvalid = true;
                this.controls.creditCardExpDate.showErrorMessage = true;
            }
        }
    }

    validateCreditCardName() {
        this.validateRequiredControl(this.controls.creditCardName);
    }

    validateAddress1() {
        this.validateRequiredControl(this.controls.address1);
    }

    validateAddress2() {

    }

    validateCity() {
        this.validateRequiredControl(this.controls.city);
    }

    validateZipcode() {
        var newValue = this.controls.zipcode.value;
        var isValid = true;
        if (newValue) {
            if (this.controls.zipcode.validationRegEx && !this.controls.zipcode.validationRegEx.test(newValue)) {
                this.controls.zipcode.isInvalid = true;
                this.controls.zipcode.isErrored = false;
                this.controls.zipcode.showErrorMessage = true;
                isValid = false;
            }
        } else {
            this.controls.zipcode.isErrored = true;
            this.controls.zipcode.isInvalid = false;
            this.controls.zipcode.showErrorMessage = true;
            isValid = false;
        }
        if (!isValid) {
            this.controls.city.value = "";
            this.controls.state.value = "0";
        } else {
            this.controls.zipcode.isInvalid = false;
            this.controls.zipcode.isErrored = false;
            this.controls.zipcode.showErrorMessage = false;
        }
    }

    validateAllFields() {
        this.validateCreditCardNumber();
        this.validateCreditCardExpDate();
        this.validateCreditCardName();
        if (this.isNewAddress) {
            this.validateAddress1();
            this.validateAddress2();
            this.validateCity();
            this.validateZipcode();
        } else {
            this.controls.address1.isInvalid = false;
            this.controls.address1.isErrored = false;
            this.controls.address1.isInvalid = false;
            this.controls.address2.isErrored = false;
            this.controls.city.isInvalid = false;
            this.controls.city.isErrored = false;
            this.controls.zipcode.isInvalid = false;
            this.controls.zipcode.isErrored = false;
        }
    }

    retrieveCityAndState() {
        this.validateZipcode();
        if (this.controls.zipcode.isErrored || this.controls.zipcode.isInvalid) {
            return;
        }
        const done = this._loadingScreen.add('Validating zip...');
        this._cub_oneaccountService.checkZipPostalCode(this.controls.zipcode.value, "")
            .then((data: any) => {
                if (!data || data.length == 0) {
                    this.controls.zipcode.isInvalid = true;
                    this.controls.zipcode.showErrorMessage = true;
                    this.controls.city.value = "";
                    this.controls.state.value = "0";
                }
                else {
                    this.controls.city.value = data[0].City;
                    this.controls.city.isInvalid = false;
                    this.controls.city.isErrored = false;
                    this.controls.city.showErrorMessage = false;
                    this.controls.state.value = data[0].State.Key;
                    this.zipcodeId = data[0].ID;
                    this.countryId = data[0].Country.Key;
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError("Error checking zip code");
            })
            .then(() => done());
    }

    validateForm() {
        this.validateAllFields();
        for (let controlName of this.controlsOrder) {
            var control = this.controls[controlName];
            if (control.isInvalid || control.isErrored) {
                return false;
            }
        }
        return true;
    }

    save() {
        try {
            this.doSave();
        } catch (e) {
            this._cub_dataService.showError('Error updating funding source: ' + e);
        }
    }

    doSave() {
        if (!this.validateForm()) {
            return;
        }

        var pgCardId = this.fundingSource.creditCard.pgCardId;
        var billingAddressId = this.fundingSource.billingAddressId;
        var creditCardExpDate = this.controls.creditCardExpDate.value;
        var creditCardName = this.controls.creditCardName.value;
        var billingAddress = this.controls.billingAddress.value;
        var address1 = this.controls.address1.value;
        var address2 = this.controls.address2.value;
        var city = this.controls.city.value;
        var stateId = this.controls.state.value;
        var state = this.controls.state.text;
        var zipcode = this.controls.zipcode.value;

        //@todo: Deal with country text: US
        const done = this._loadingScreen.add('Updating funding source...');
        this._cub_fundingSourceService.updateFundingSource(this.accountId, this.fundingSourceId, pgCardId, creditCardExpDate, creditCardName,
            this.isNewAddress, address1, address2, city, state, stateId, 'US', this.countryId, zipcode, this.zipcodeId, billingAddressId, billingAddress, this.isPrimary)
            .then((data: any) => {
                //@todo: return the previous form
                location.reload();
                this.cancelClicked.emit(false);
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error updating funding source');
                // ATTENTION: This reloads the address if it is a new address. This way if something fail after a new address is created
                // The user will have the opportunity to select the created address and re-try
                // Example, duplicated funding source (Credit card info) and NIS will return an error message indicating that.
                if (this.isNewAddress) {
                    this.getAddress(this.accountId);
                }
            })
            .then(() => done());
    }


    cancel() {
        this.cancelClicked.emit(true);
    }

    createControls() {
        this.controls = {
            status: {
                type: "Textbox",
                id: "status",
                label: "Type",
                isRequired: true,
                isTableControl: true,
                errorMessage: "Status is required",
                value: ''
            } as Cub_Textbox,
            paymentType: {
                type: "Dropdown",
                id: "paymentType",
                label: "Payment Type",
                isRequired: true,
                isTableControl: true,
                errorMessage: 'Payment Type is required',
                options: [],
                value: "1"
            } as Cub_Dropdown,
            creditCardType: {
                type: "Textbox",
                id: "creditCardType",
                label: "Type",
                isRequired: true,
                isTableControl: true,
                errorMessage: "Card Type is required",
                value: ''
            } as Cub_Textbox,
            creditCardNumber: {
                type: "Textbox",
                id: "creditCardNumber",
                isRequired: true,
                isTableControl: true,
                errorMessage: "Credit Card # is required",
                value: '',
            } as Cub_Textbox,
            creditCardExpDate: {
                type: "Textbox",
                id: "creditCardExpDate",
                formFieldClass: 'form-fields-vertical',
                validationRegEx: new RegExp('^0[1-9]|^(11)|^(12)[0-9][0-9]$'),
                isRequired: true,
                errorMessage: "Expiration date is required",
                validationMessage: "Invalid expriration date",
                maxLength: 4,
                value: '',
            } as Cub_Textbox,
            creditCardName: {
                type: "Textbox",
                id: "creditCardName",
                label: "Name on Card",
                isRequired: true,
                isTableControl: true,
                errorMessage: "Name on Card is required",
                value: '',
            } as Cub_Textbox,
            billingAddress: {
                type: "Dropdown",
                id: "billingAddress",
                label: "Billing Address",
                isRequired: true,
                isTableControl: true,
                errorMessage: 'Billing Address is required',
                onChange: this.billingAddressOnChange,
                options: []
            } as Cub_Dropdown,
            address1: {
                type: "Textbox",
                id: "address",
                label: "Address",
                isRequired: true,
                isTableControl: true,
                errorMessage: "Address is required",
                value: '',
            } as Cub_Textbox,
            address2: {
                type: "Textbox",
                id: "address2",
                label: "",
                isTableControl: true,
                value: '',
            } as Cub_Textbox,
            city: {
                type: "Textbox",
                id: "city",
                label: "City",
                maxLength: 25,
                isRequired: true,
                isTableControl: true,
                errorMessage: "City is required",
                value: '',
            } as Cub_Textbox,
            state: {
                type: "Dropdown",
                id: "billingAddress",
                label: "Billing Address",
                isRequired: true,
                isTableControl: true,
                errorMessage: 'Billing Address is required',
                options: [],
            } as Cub_Dropdown,
            zipcode: {
                type: "Textbox",
                id: "zipcode",
                label: "Zip Code",
                isRequired: true,
                errorMessage: "Zipcode is required",
                validationMessage: "Invalid Zipcode",
                value: '',
            } as Cub_Textbox,
        }
    }
}
