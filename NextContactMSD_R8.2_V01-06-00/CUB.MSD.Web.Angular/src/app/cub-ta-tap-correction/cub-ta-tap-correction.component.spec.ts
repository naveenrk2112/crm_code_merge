import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubTaTapCorrectionComponent } from './cub-ta-tap-correction.component';

describe('CubTaTapCorrectionComponent', () => {
  let component: CubTaTapCorrectionComponent;
  let fixture: ComponentFixture<CubTaTapCorrectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubTaTapCorrectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubTaTapCorrectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
