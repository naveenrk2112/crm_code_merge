﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { Cub_UnableToDeleteAddressComponent } from './cub_unabletodeleteaddress.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            { path: '', component: Cub_UnableToDeleteAddressComponent }
        ])
    ],
    declarations: [Cub_UnableToDeleteAddressComponent],
    exports: [RouterModule]
})
export class Cub_UnableToDeleteAddressModule { }