﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterModule, Routes, Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from "@angular/router/testing";
import { APP_BASE_HREF, Location } from '@angular/common';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_CustomerVerificationComponent } from '../cub-customerverification/cub_customerverification.component';
import { Cub_AlertComponent } from '../controls/cub-alert/cub_alert.component';
import { Cub_SecurityOptionComponent } from '../controls/cub-securityoption/cub_securityoption.component';
import { Observable } from 'rxjs/Observable';

import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Cub_UnableToDeleteAddressComponent } from './cub_unabletodeleteaddress.component';

import { ActivatedRouteStub } from '../testutilities/route.stub';

describe('CubUnableToDeleteAddressComponent', () => {

    let comp: Cub_UnableToDeleteAddressComponent;
    let fixture: ComponentFixture<Cub_UnableToDeleteAddressComponent>;
    let root: DebugElement;
    let location: Location;

    function setQueryParams(data: object) {
        ActivatedRouteStub._queryParams = data;
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                RouterTestingModule.withRoutes([{ path: 'UnableToDeleteAddress', component: Cub_UnableToDeleteAddressComponent }])
            ],
            declarations: [
                Cub_UnableToDeleteAddressComponent,
                Cub_CustomerVerificationComponent,
                Cub_AlertComponent,
                Cub_SecurityOptionComponent
            ],
            providers: [Cub_DataService,
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: APP_BASE_HREF, useValue: '/' }]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(Cub_UnableToDeleteAddressComponent);
            comp = fixture.componentInstance;
            root = fixture.debugElement;
        });
    }));

    it('Should create the component', () => {
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it('Should load one record', fakeAsync(() => {
        setQueryParams({record1: 'abc|123|456|1' });

        tick();
        fixture.detectChanges();

        expect(fixture.nativeElement.querySelectorAll(".list-update > li").length).toBe(1);
        expect(fixture.nativeElement.querySelectorAll(".list-update > li")[0].querySelectorAll("li")[0].innerText.trim()).toBe("456");
        expect(fixture.nativeElement.querySelectorAll(".list-update > li > button > ul > li a").length).toBe(1);
    }));

    it('Should load three records', fakeAsync(() => {
        setQueryParams({ record1: 'aaa|123|456|1', record2: 'ccc|aaa|bbb|1', record3: 'bbb|111|789|1' });

        tick();
        fixture.detectChanges();

        expect(fixture.nativeElement.querySelectorAll(".list-update > li").length).toBe(3);
        expect(fixture.nativeElement.querySelectorAll(".list-update > li")[0].querySelectorAll("li")[0].innerText.trim()).toBe("456");
        expect(fixture.nativeElement.querySelectorAll(".list-update > li")[1].querySelectorAll("li")[0].innerText.trim()).toBe("bbb");
        expect(fixture.nativeElement.querySelectorAll(".list-update > li")[2].querySelectorAll("li")[0].innerText.trim()).toBe("789");
        expect(fixture.nativeElement.querySelectorAll(".list-update > li > button > ul > li a").length).toBe(3);
    }));

    it('Should load one record with no update', fakeAsync(() => {
        setQueryParams({ record1: 'abc|123|456|0' });
    
        tick();
        fixture.detectChanges();

        expect(fixture.nativeElement.querySelectorAll(".list-update > li").length).toBe(1);
        expect(fixture.nativeElement.querySelectorAll(".list-update > li")[0].querySelectorAll("li")[0].innerText.trim()).toBe("456");
        expect(fixture.nativeElement.querySelectorAll(".list-update > li > button > ul > li a").length).toBe(0);
    }));
});
