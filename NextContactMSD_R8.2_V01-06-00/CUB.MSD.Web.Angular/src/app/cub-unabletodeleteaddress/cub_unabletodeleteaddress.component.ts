﻿import {Component, Input, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'cub-unabletodeleteaddress',
    templateUrl: './cub_unabletodeleteaddress.component.html'
})
export class Cub_UnableToDeleteAddressComponent implements OnInit {
    constructor(
        private route: ActivatedRoute,
        private router: Router,
    ) { }

    dependencies: Array<Object> = []; //Each object contains type, id, title, allowUpdate

    updateClicked(event: any, record: any) {
        var data = {
            action: record.type == 'contact' ? 'openContact' : 'openFundingSource',
            id: record.id
        }
        parent.postMessage(JSON.stringify(data), '*');
    }

    closeModal(event: any) {
        var data = {
            action: 'closeModal'
        }
        parent.postMessage(JSON.stringify(data), '*');
    }

    ngOnInit() {
        this.route.queryParams.subscribe((params: Params) => {
            this.dependencies = [];
            var i = 1;
            while (true) {
                var record = params['record' + i];
                if (!record) {
                    break;
                }

                var details = record.split('|');
                if (details.length != 4) {
                    break;
                }

                this.dependencies.push({
                    type: details[0],
                    id: details[1],
                    title: details[2],
                    allowUpdate: details[3]
                });
                i++;
            }
        });
    }
}