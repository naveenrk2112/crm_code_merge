import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubTaVoidTripComponent } from './cub-ta-voidtrip.component';

describe('CubTaVoidTripComponent', () => {
    let component: CubTaVoidTripComponent;
    let fixture: ComponentFixture<CubTaVoidTripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [CubTaVoidTripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(CubTaVoidTripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
