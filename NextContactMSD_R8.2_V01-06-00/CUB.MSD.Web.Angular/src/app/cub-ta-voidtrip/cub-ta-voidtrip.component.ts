import { Component, OnInit, Input, Output, EventEmitter, HostListener, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { CubCustomerService } from '../services/cub_customer.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as uuid from 'uuid/v4';
import { Observable } from 'rxjs/Observable';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import emailMask from 'text-mask-addons/dist/emailMask';

@Component({
  selector: 'cub-ta-voidtrip',
  templateUrl: './cub-ta-voidtrip.component.html',
  styleUrls: ['./cub-ta-voidtrip.component.css'],
  providers: [Cub_SubsystemService, CubCustomerService]
})
export class CubTaVoidTripComponent implements OnInit {

    @Input() transitaccountId: string;
    @Input() subsystemId: string;
    @Input() tripId: string
    @Input() customerId: string;
    @Output() onCancel = new EventEmitter<void>();

    reasonCodeTypes: any = null;
    reasonCodeTypeSelected: any = null;
    reasonCodeTypeId: string = null;

    reasonCodeSelected: any = null;

    contacts: any = null;
    contactSelected: any = null;

    unregisteredEmail: string = null;
    notes: string = "";
    
    orderNumber: string = null;
    responseCode: string = null;
    errors: any = null;

    editEmail: boolean = false;
    editPhone: boolean = false;

    isFormValid: boolean = false;

    showFinalMessage: boolean = false;
    clientRefId: string = null;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cub_SubsystemService: Cub_SubsystemService,
        public _cub_CustomerService: CubCustomerService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
        if (this.customerId != null) {
            this.customerId = this.customerId.replace('{', '').replace('}', '');
        }

        this.clientRefId = this.generateClientRefId();
        // Loading Globals
        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.reasonCodeTypeId = globals['CUB.MSD.Web.Angular']['ReasonCodeTypeIdForVoidTrip'] as string;
                this.getReasonCodeTypes();
            });
        // Getting customer contacts
        if (this.customerId) {
            this._cub_CustomerService.getContactsForVoidTrip(this.customerId)
                .then((response: any) => {
                    var body = JSON.parse(response);
                    this.contacts = body;
                    console.log(this.contacts);
                })
                .catch((error: any) => {
                    this._cub_dataService.prependCurrentError('Error getting contacts');
                });
        }
    }

    getReasonCodeTypes() {
        const done = this._loadingScreen.add('Getting Reason Codes...');
        this._cub_SubsystemService.getReasonCodes()
            .then((data) => {
                if (data) {
                    var body = JSON.parse(data.Body);
                    this.reasonCodeTypes = body.reasonCodeTypes;
                    this.reasonCodeTypeSelected = this.reasonCodeTypes.find(rct => rct.typeId == this.reasonCodeTypeId);
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting reason Codes');
            })
            .then(() => done());
    } 

    closeFinalMessage() {
        this.onCancel.emit();
    }

    generateClientRefId() {
        return `MSD:${uuid()}`.toUpperCase();
    }

    validateForm() {
        // The reason Coce is always required;
        let valid: boolean = this.reasonCodeSelected != null &&
                             this.reasonCodeSelected.reasonCodeId != null;

        // Notes is required depending of the selected Reason
        valid = valid &&
            ((!this.reasonCodeSelected.notesMandatoryFlag) ||
                (this.notes != null && this.notes.length != 0));
        /*
         * If the account have a customer, a contact is required
         * If the account doesn't have a customer, the Unregister Email is required
         */
        valid = valid &&
                ((this.customerId == null) ||
            (this.customerId != null && this.contactSelected != null && this.contactSelected.Email));
        this.isFormValid = valid;
    }

    onReasonCodeChanged() {
        this.validateForm();
    }

    onContactChanged() {
        this.validateForm();
    }

    onEditEmail() {
        if (this.contactSelected)
            this.editEmail = !this.editEmail;
    }

    onEditPhone() {
        if (this.contactSelected)
            this.editPhone = !this.editPhone;
    }

    onVoidTripButtonClicked() {
        let request: any = {};
        request.clientRefId = this.clientRefId;
        if (this.customerId) {
            request.customerId = this.customerId;
        }
        if (this.unregisteredEmail) {
            request.unregisteredEmail = this.unregisteredEmail;
        }
        let correction: any = {};
        correction.subsystem = this.subsystemId;
        correction.subsystemAccountReference = this.transitaccountId;
        correction.reasonCode = String(this.reasonCodeSelected.reasonCodeId);
        correction.notes = this.notes;
        correction.correctionLineItemType = "TransitAccountTripVoid";
        correction.transactionId = String(this.tripId);
        let correctionLineItems: any = [];
        let correctionLineItem: any = {};
        correctionLineItem.correction = correction;
        correctionLineItems.push(correctionLineItem);
        request.correctionLineItems = correctionLineItems;

        const done = this._loadingScreen.add('Procesing Void Trip...');
        this._cub_SubsystemService.postTravelCorrection(request)
            .then((response: any) => {
                var body = JSON.parse(response.Body);
                this.orderNumber = body.orderId;
                this.errors = body.errors;
                this.responseCode = body.responseCode;
                // Reset the Client Ref Id if the call is completed without errors
                this.clientRefId = this.generateClientRefId();
                this.showFinalMessage = true;
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error posting the Void Tip');
                this.validateForm();
            })
            .then(() => done());
    }
}
