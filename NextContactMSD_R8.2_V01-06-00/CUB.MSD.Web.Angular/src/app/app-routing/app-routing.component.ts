import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';

@Component({
    selector: 'app',
    templateUrl: './app-routing.component.html'
})
export class AppRoutingComponent {
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public _cub_dataService: Cub_DataService
    ) { }

    preventScroll(): boolean {
        return this._cub_dataService.cancelScreen.show
            || this._cub_dataService.errorScreen.show
            || this._cub_dataService.busyScreen.busyCount > 0;
    }

    cancelScreenYesButtonClicked(entry: any): void {
        this._cub_dataService.cancelScreen.show = false;
        this.router.navigate([this._cub_dataService.cancelScreen.cancelButtonRoute]);
    }

    cancelScreenNoButtonClicked(entry: any): void {
        this._cub_dataService.cancelScreen.show = false;
    }

    continueButtonClicked(entry: any): void {
        this._cub_dataService.errorScreen.errors.pop();
        if (this._cub_dataService.errorScreen.errors.length == 0) {
            this._cub_dataService.errorScreen.errorTotal = 0;
            this._cub_dataService.errorScreen.show = false;
            if (this._cub_dataService.errorScreen.fatalState) {
                location ? location.reload() : window.location.reload();
            }
        }
        
    }

    busyScreenButtonClicked(entry: any): void {
        this._cub_dataService.busyScreen.busyCount--;
        this.router.navigate([this._cub_dataService.busyScreen.buttonRoute]);
    }

    showErrorCount() {
        return (`${this._cub_dataService.errorScreen.errors.length} / ${this._cub_dataService.errorScreen.errorTotal}`);
    }
}