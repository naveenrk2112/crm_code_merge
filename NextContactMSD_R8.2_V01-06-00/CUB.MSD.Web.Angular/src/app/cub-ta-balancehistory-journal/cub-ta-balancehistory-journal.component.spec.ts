import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cub_TA_BalanceHistoryJournalComponent } from './cub-ta-balancehistory-journal.component';

describe('Cub_TA_BalanceHistoryJournalComponent', () => {
    let component: Cub_TA_BalanceHistoryJournalComponent;
    let fixture: ComponentFixture<Cub_TA_BalanceHistoryJournalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [Cub_TA_BalanceHistoryJournalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Cub_TA_BalanceHistoryJournalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
