import { Component, Input, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as Model from '../model';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_TA_BalanceHistoryJournalService } from '../services/cub_ta_balancehistoryjournal.service';
import { Cub_CustomizableGridComponent } from '../controls/cub-customizablegrid/cub_customizablegrid.component';

import { LazyLoadEvent } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'cub-ta-balancehistory-journal',
    templateUrl: './cub-ta-balancehistory-journal.component.html',
    styleUrls: ['./cub-ta-balancehistory-journal.component.css'],
    providers: [Cub_TA_BalanceHistoryJournalService]
})
export class Cub_TA_BalanceHistoryJournalComponent implements OnInit {
    @Input() subsystemId: string = "";
    @Input() transitAccountId: string = "";

    @ViewChild(Cub_CustomizableGridComponent)
    grid: Cub_CustomizableGridComponent;

    balanceHistory: Array<object> = [];
    sortedBalanceHistory: Array<object> = [];
    initialPass: boolean = true;
    rowsPerPage: number = 5; //default
    pagingOffset: number = 0;
    totalRecordCount: number = 0;
    initComplete: boolean = false;
    dateOffset: number = 0;
    disableSearch: boolean = true;
    reversedShow: boolean = false;

    entryTypeControl: {
        value: string,
        options: Array<Model.Cub_MultiDropdownOption>
    };
    entrySubTypeControl: Model.Cub_Dropdown = {
        label: 'Journal Entry Subtype',
        id: 'entrySubTypeControl',
        options: []
    };
    purseTypeControl: {
        value: string,
        options: Array<Model.Cub_MultiDropdownOption>
    };
    showReversedControl: {
        value: string,
        options: Array<Model.Cub_Checkbox>
    };

    /* Filter values */
    startDateFilter: string;
    endDateFilter: string;

    /*Pagination and Sorting params*/
    sortBy: string;

    showDetail: boolean = false;
    selectedPaymentId: any = null;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cub_TA_BalanceHistoryService: Cub_TA_BalanceHistoryJournalService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private _changeDetector: ChangeDetectorRef
    ) { }

    ngOnInit() {
        if (!this.transitAccountId) {
            this.transitAccountId = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
        }
        if (!this.subsystemId) {
            const subsystemControl = parent.Xrm.Page.getAttribute('cub_subsystemid');
            if (subsystemControl) {
                this.subsystemId = subsystemControl.getValue();
            }
        }
        
        this.entryTypeControl = {
            value: '',
            options: []
        };
      
        this.purseTypeControl = {
            value: '',
            options: []
        };

        //Load globals for the page
        const globals$ = this._cub_dataService.Globals.first(globals => !!globals['CUB.MSD.Web.Angular']);
        const grids$ = this._cub_dataService.GridConfigs.first(configs => !!configs['TA.BalanceHistory']);
        const done = this._loadingScreen.add();
        Observable.forkJoin(globals$, grids$)
            .finally(() => done())
            .subscribe(([globals, grids]) => {
                this.dateOffset = globals['CUB.MSD.Web.Angular']['TA.BalanceHistoryJournal.StartDateOffset'] as number;

                var entryTypeOptionsString = globals['CUB.MSD.Web.Angular']['TA.BalanceHistoryJournal.EntryTypeOptions'] as string;
                var entryTypeOptions = entryTypeOptionsString ? entryTypeOptionsString.split(';') : [];
                for (var i = 0; i < entryTypeOptions.length; i++) {
                    if (entryTypeOptions[i].indexOf(':') === -1) continue;

                    var keyValuePair = entryTypeOptions[i].split(':');
                    if (keyValuePair.length !== 2) continue;
                    this.entryTypeControl.options.push({
                        label: keyValuePair[0],
                        value: keyValuePair[1],
                        checked: true
                    } as Model.Cub_MultiDropdownOption);
                }

                this._cub_dataService.fillCubDropdown(this.entrySubTypeControl, globals, 'TA.BalanceHistoryJournal.EntrySubTypeOptions');
                var purseTypeOptionsString = globals['CUB.MSD.Web.Angular']['TA.BalanceHistoryJournal.PurseType'] as string;
                var purseTypeOptions = purseTypeOptionsString ? purseTypeOptionsString.split(';') : [];
                for (var i = 0; i < purseTypeOptions.length; i++) {
                    if (purseTypeOptions[i].indexOf(':') === -1) continue;

                    var keyValuePair = purseTypeOptions[i].split(':');
                    if (keyValuePair.length !== 2) continue;
                    this.purseTypeControl.options.push({
                        label: keyValuePair[0],
                        value: keyValuePair[1],
                        checked: true
                    } as Model.Cub_MultiDropdownOption);
                }

                let grid = grids['TA.BalanceHistory'];
                this.rowsPerPage = grid.rowsToDisplay;
                this.sortBy = grid.defaultSort;
                this.getBalanceHistory();
            });
    }

    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.disableSearch = false;
            this.getBalanceHistory();
        }
    }

    //Retrieve paged-portion of status history data
    getBalanceHistory() {
        let subSystemId = this.subsystemId;
        let accountId = this.transitAccountId;
        let entryType = this.entryTypeControl.value || null;
        let entrySubType = this.entrySubTypeControl.value || null;
        let purseType = this.purseTypeControl.value || null;
        let reversedShow = this.reversedShow;
        let offset = (this.pagingOffset > 0) ? this.pagingOffset : 0;
        let limit = this.rowsPerPage;
        let sortBy = this.sortBy || null;

        const done = this._loadingScreen.add('Loading Balance History...');
        this._cub_TA_BalanceHistoryService.getBalanceHistory({
            subSystemId: subSystemId,
            accountId: accountId,
            startDateTime: this.startDateFilter,
            endDateTime: this.endDateFilter,
            entryType: entryType,
            entrySubType: entrySubType,
            purseType: purseType,
            reversal: reversedShow,
            sortBy: sortBy,
            offset: offset,
            limit: limit
        })
            .then((data) => {
                if (data) {
                    var balanceHistory = JSON.parse(data.Body);
                    this.balanceHistory = balanceHistory.lineItems;
                    this.sortedBalanceHistory = balanceHistory.lineItems;
                    this.totalRecordCount = balanceHistory.totalCount;
                    this.disableSearch = true;
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting balance history');
            })
            .then(() => this.initComplete = true)
            .then(() => done());
    }

    /**
   * Fires when the start date text is modified or a date is selected using the
   * date picker. This sets the start date filter used in getBalanceHistory()
   * and determines whether the end date control should be disabled.
   */
    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableSearch = false;
    }

    /**
    * Fires when the end date text is modified or a date is selected using the
    * date picker. This sets the end date filter used in getBalanceHistory().
    */
    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableSearch = false;
    }

    onResetClicked() {
        this.entryTypeControl.options = [].concat(this.entryTypeControl.options);
        //this.entrySubTypeControl.options = [].concat(this.entrySubTypeControl.options);
        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this._cub_dataService.fillCubDropdown(this.entrySubTypeControl, globals, 'TA.BalanceHistoryJournal.EntrySubTypeOptions');
            });

        this.purseTypeControl.options = [].concat(this.purseTypeControl.options);

        const temp = this.dateOffset;
        this.dateOffset = null;
        this._changeDetector.detectChanges(); //necessary to reset date filter for now
        this.dateOffset = temp;

        this.sortBy = this.grid.control.defaultSort;
        this.grid.reset();
        this.getBalanceHistory();
    }

    /**
    * Fires when the refresh button is clicked.
    * This then calls getTapsHistory().
    */
    onRefreshClicked() {
        this.getBalanceHistory();
    }

    /**
    * Fires when the enter key is pressed while any of search, start date, or
    * end date controls are in focus. Calls getTapsHistory() and shifts focus
    * to the search control.
    */
    onEnterKey() {
        if (!this.disableSearch) {
            this.getBalanceHistory();
            this.disableSearch = true;
        }
    }

    /**
    * Fires when the search button is clicked.
    * This validates and then calls getTapsHistory().
    */
    onSearchClicked() {
        this.getBalanceHistory();
    }

    /**
    * Generic onChange function that enables the search button. Fires when a
    * dropdown control changes.
    */
    onMultiDropdownChanged(event, control) {
        this[control].value = event;
        this.disableSearch = false;
    }

    onEntrySubTypeChanged(event: any) {
        this.disableSearch = false;
    }
    /**
    * onChange function that enables the search button. Fires when 
    * the checkbox control changes.
    */
    onReversedChange() {
        this.disableSearch = false;
    }

    /**
     * Fired then the Show Details button from the custom action is clicked
     * @param journalEntryId
     * @param retrievalRefNbr
     */
    onViewDetailsActionClicked(paymentId) {
        this.selectedPaymentId = paymentId;
        this.showDetail = true;
    }

    hideDetail() {
        this.showDetail = false;
    }
}