import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { DataTableModule, SharedModule } from 'primeng/primeng';
import { TextMaskModule } from 'angular2-text-mask';
import { NguiDatetimePickerModule, NguiDatetime } from '@ngui/datetime-picker';

import { Cub_OneAccountBalanceHistoryComponent } from './cub_oneaccount_balancehistory.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import { Cub_TextboxComponent } from '../controls/cub-textbox/cub_textbox.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_OptionSelectComponent } from '../controls/cub-optionselect/cub_optionselect.component';
import { Cub_HelpComponent } from '../controls/cub-help/cub_help.component';
import { Cub_ButtonComponent } from '../controls/cub-button/cub_button.component';
import { Cub_MultiDropdown } from '../controls/cub-multi-dropdown/cub-multi-dropdown.component';
import { Cub_DateRangeComponent } from '../controls/cub-date-range/cub-date-range.component';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { MomentDatePipe } from '../pipes/moment-date.pipe';
import { MoneyPipe } from '../pipes/money.pipe';

import { Cub_MenuToggleDirective } from '../cub-menu-toggle/cub-menu-toggle.directive';
import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';

import { WSGetCustomerOrderHistoryResponse } from '../model';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_BalanceHistoryServiceStub } from '../testutilities/cub_balancehistory.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

import * as moment from 'moment';

describe('Cub_OneAccountBalanceHistoryComponent', () => {
    const QUERY_PARAMS = {
        oneAccountId: '00000000-0000-0000-0000-000000000001'
    };
    const BALANCE_HISTORY_DETAILS_RESPONSE_BODY: any = {
        "hdr": {
            "result": "Successful",
        },
        "entryDateTime": "2017-10-23T15:48:53.1183116Z",
        "purseId": "string 20 purseId 88",
        "purseNickname": "string 30 FakeNIS Purse     30!",
        "entryType": "string20 Entry Type1",
        "entryTypeDescription": "string 30 Fake NIS Entry   30!",
        "entryStatus": "string20 EntryStatus",
        "entryStatusDescription": "string30 EntryStatus Desc-tion",
        "journalEntryAmount": 2000,
        "availableBalance": 11000,
        "endingBalance": 12000,
        "financialTransaction": {
            "financialTransactionId": 201,
            "financialTransactionType": "str20 financial type",
            "authorizationReferenceNumber": "str20ReferenceNumber",
            "externalReferenceNumber": "string 40 External Reference Number  40!",
            "transactionDateTime": "2017-10-23T20:36:53.1183116Z",
            "transactionDescription": "string 100 Transaction Description",
            "transactionAmount": 2000
        },
        "allocatedAmount": 1500,
        "unallocatedAmount": 500,
        "journalAllocations": [
            {
                "journalEntryId": 100,
                "allocatedAmount": 1500,
                "entryAmount": 1500,
                "entryType": "Charge",
                "entryTypeDescription": "Charge description",
                "entryDateTime": "2017-10-23T15:48:53.1183116Z",
                "financialTransaction": {
                    "financialTransactionId": 199,
                    "financialTransactionType": "str20 financial type",
                    "authorizationReferenceNumber": "str20ReferenceNumber",
                    "externalReferenceNumber": "string 40 External Reference Number  40!",
                    "transactionDateTime": "2017-10-23T20:36:53.1183116Z",
                    "transactionDescription": "string 100 Transaction Description",
                    "transactionAmount": 1500
                }
            },
            {
                "journalEntryId": 200,
                "allocatedAmount": 500,
                "entryAmount": 500,
                "entryType": "Charge",
                "entryTypeDescription": "Charge description",
                "entryDateTime": "2017-10-23T15:48:53.1183116Z",
                "financialTransaction": {
                    "financialTransactionId": 200,
                    "financialTransactionType": "str20 financial type",
                    "authorizationReferenceNumber": "str20ReferenceNumber",
                    "externalReferenceNumber": "string 40 External Reference Number  40!",
                    "transactionDateTime": "2017-10-23T20:36:53.1183116Z",
                    "transactionDescription": "string 100 Transaction Description",
                    "transactionAmount": 500
                }
            }
        ]
    }

    const BALANCE_HISTORY_RESPONSE_BODY: any = {
        hdr: {
            result: 'Successful'
        },
        "totalCount": 3,
        "lineItems": [
            {
                "journalEntryId": 100,
                "entryDateTime": "2017-10-23T06:09:36.3823873Z",
                "purseId": 1,
                "purseNickname": "Default Purse",
                "entryType": "Charge",
                "entryTypeDescription": "entry type description",
                "entrySubType": "SubType",
                "entrySubTypeDescription": "entry subtype description",
                "entryStatus": "some entry status",
                "entryStatusDescription": "entry status description",
                "journalEntryAmount": 100,
                "availableBalance": 500,
                "endingBalance": 400
            },
            {
                "journalEntryId": 200,
                "entryDateTime": "2017-10-23T06:09:36.3823873Z",
                "purseId": 1,
                "purseNickname": "Default Purse",
                "entryType": "Charge",
                "entryTypeDescription": "entry type description",
                "entrySubType": "SubType2",
                "entrySubTypeDescription": "entry subtype description",
                "entryStatus": "some entry status",
                "entryStatusDescription": "entry status description",
                "journalEntryAmount": 100,
                "availableBalance": 400,
                "endingBalance": 300
            },
            {
                "journalEntryId": 300,
                "entryDateTime": "2017-10-23T06:09:36.3823873Z",
                "purseId": 1,
                "purseNickname": "Default Purse",
                "entryType": "Charge",
                "entryTypeDescription": "entry type description",
                "entrySubType": "SubType3",
                "entrySubTypeDescription": "entry subtype description",
                "entryStatus": "some entry status",
                "entryStatusDescription": "entry status description",
                "journalEntryAmount": 100,
                "availableBalance": 300,
                "endingBalance": 200
            }
        ]
    };

    let component: Cub_OneAccountBalanceHistoryComponent;
    let fixture: ComponentFixture<Cub_OneAccountBalanceHistoryComponent>;
    let service: Cub_OneAccountService;
    let dataService: Cub_DataService;
    let root: DebugElement;
    let table;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                DataTableModule,
                TextMaskModule,
                NguiDatetimePickerModule
            ],
            declarations: [
                Cub_OneAccountBalanceHistoryComponent,
                Cub_DropdownComponent,
                Cub_TextboxComponent,
                Cub_ControlLabelComponent,
                Cub_OptionSelectComponent,
                Cub_HelpComponent,
                Cub_ButtonComponent,
                Cub_DateRangeComponent,
                MomentDatePipe,
                MoneyPipe,
                Cub_MultiDropdown,
                Cub_TooltipDirective,
                Cub_MenuToggleDirective
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
            ]
        }).overrideComponent(Cub_OneAccountBalanceHistoryComponent, {
            set: {
                providers: [
                    { provide: Cub_OneAccountService, useClass: Cub_BalanceHistoryServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_OneAccountBalanceHistoryComponent);
        component = fixture.componentInstance;
        root = fixture.debugElement;
        service = root.injector.get(Cub_OneAccountService);
        dataService = TestBed.get(Cub_DataService);
        table = root.nativeElement;
        ActivatedRouteStub._queryParams = QUERY_PARAMS;
        Cub_BalanceHistoryServiceStub._balanceHistoryResponse = BALANCE_HISTORY_RESPONSE_BODY;
    });

    it('should be created', (() => {
        fixture.detectChanges();
        expect(component).toBeTruthy();
    }));

    it('should set customer ID, initialize controls, and get balance history on init', fakeAsync(() => {
        spyOn(service, 'getBalanceHistory').and.callThrough();
        fixture.detectChanges();
        expect(component['oneAccountId']).toEqual(QUERY_PARAMS.oneAccountId);
        expect(component.viewTypeControl).toBeDefined();
        expect(component.financialTransactionTypeControl).toBeDefined();
        expect(component.purseControl).toBeDefined();
        expect(service['getBalanceHistory']).toHaveBeenCalled();
        expect(component['balanceHistory']).toEqual(BALANCE_HISTORY_RESPONSE_BODY.lineItems);
        expect(component['balanceHistoryTotalCount']).toEqual(BALANCE_HISTORY_RESPONSE_BODY.totalCount);
    }));

    it('#onLazyLoad should not call getBalanceHistory if component is not initialized', fakeAsync(() => {
        fixture.detectChanges();
        spyOn(component, 'getBalanceHistory');
        component.initComplete = false;
        component.onLazyLoad(null);
        expect(component.getBalanceHistory).not.toHaveBeenCalled();
    }));

    it('#onLazyLoad should fire when a paging button is clicked', fakeAsync(() => {
        Cub_DataServiceStub._constants['CUB.MSD.Web.Angular']['OAM.BalanceHistory.NumRows'] = 1;
        fixture.detectChanges();
        spyOn(component, 'onLazyLoad');
        table.querySelectorAll('.ui-paginator-page')[0].click();
        expect(component.onLazyLoad).toHaveBeenCalled();
    }));

    it('#onDropdownChanged should enable the apply button', fakeAsync(() => {
        fixture.detectChanges();
        let buttonEl = table.querySelector('#search-button');
        expect(component.disableSearch).toBeTruthy();
        expect(buttonEl.disabled).toBeTruthy();
        component.onDropdownChanged();
        fixture.detectChanges();
        expect(component.disableSearch).toBeFalsy();
        expect(buttonEl.disabled).toBeFalsy();
    }));

    describe('#getBalanceHistory', () => {
        it('should not do anything if inputs are invalid', () => {
            spyOn(service, 'getBalanceHistory');
            component.endDateFilter = '2017-01-01T23:59:59.999Z';
            component.startDateFilter = null;
            component.getBalanceHistory();
            expect(component.disableSearch).toBeTruthy();
            expect(service.getBalanceHistory).not.toHaveBeenCalled();
        });

        it('should set request params', fakeAsync(() => {
            fixture.detectChanges();
            spyOn(service, 'getBalanceHistory').and.callThrough();
            const START = component.startDateFilter = '2017-01-01T00:00:00.000Z';
            const END = component.endDateFilter = '2017-12-31T23:59:59.999Z';
            const OFFSET = component.pagingOffset;
            const LIMIT = component.rowsPerPage;
            const VIEW_TYPE = component.viewTypeControl.value = '1';
            const FXN_TYPE = component.financialTransactionTypeControl.value = '2';
            const SORT_BY = component.sortBy = 'purseId|desc';
            const PURSE = component.purseControl.value = '3';
            component.getBalanceHistory();
            tick();
            expect(service.getBalanceHistory).toHaveBeenCalledWith(QUERY_PARAMS.oneAccountId,
                START,
                END,
                VIEW_TYPE,
                PURSE,
                FXN_TYPE,
                SORT_BY,
                OFFSET,
                LIMIT
            );
        }));

        it('should not set certain filter params if they are unset', fakeAsync(() => {
            fixture.detectChanges();
            spyOn(service, 'getBalanceHistory').and.callThrough();
            const START = component.startDateFilter = '2017-01-01T00:00:00.000Z';
            const END = component.endDateFilter = '2017-12-31T23:59:59.999Z';
            const OFFSET = component.pagingOffset;
            const LIMIT = component.rowsPerPage;
            const VIEW_TYPE = component.viewTypeControl.value = null;
            const FXN_TYPE = component.financialTransactionTypeControl.value = null;
            const PURSE = component.purseControl.value = null;
            const SORT_BY = component.sortBy = null;
            component.getBalanceHistory();
            tick();
            expect(service.getBalanceHistory).toHaveBeenCalledWith(QUERY_PARAMS.oneAccountId,
                START,
                END,
                VIEW_TYPE,
                PURSE,
                FXN_TYPE,
                SORT_BY,
                OFFSET,
                LIMIT
            );
        }));
    });
});
