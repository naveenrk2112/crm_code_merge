import { Component, Input, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Cub_OneAccountService, OneAccountBalanceHistoryLineItem } from '../services/cub_oneaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';
import { Cub_DataService } from '../services/cub_data.service';
import * as moment from 'moment';
import * as lodash from 'lodash';
import { DataTableModule, LazyLoadEvent, SharedModule, SortMeta } from 'primeng/primeng';

@Component({
    selector: 'cub_oneaccount_balancehistory',
    templateUrl: './cub_oneaccount_balancehistory.component.html',
    providers: [Cub_OneAccountService]
})
export class Cub_OneAccountBalanceHistoryComponent implements OnInit {

    @Input() oneAccountId: string = "";

    balanceHistory: OneAccountBalanceHistoryLineItem[];
    sortedBalanceHistory: OneAccountBalanceHistoryLineItem[];
    balanceHistoryTotalCount: number;
    balanceHistoryDetail: any;

    purseControl: Model.Cub_Dropdown;
    accountBalanceControl: Model.Cub_Dropdown;
    viewTypeControl: Model.Cub_Dropdown;
    financialTransactionTypeControl: Model.Cub_Dropdown;

    disableSearch: boolean = true;

    showBalanceHistoryDetail: boolean = false;
    initialPass: boolean = true;
    rowsPerPage: number = 5;
    initComplete: boolean = false;

    /* Filter values */
    startDateFilter: string;
    endDateFilter: string;
    startDateOffset: number = 30;

    /*Pagination and Sorting params*/
    multiSortMeta: SortMeta[] = [];
    sortBy: string;
    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cub_oneAccountService: Cub_OneAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) {
        this.multiSortMeta.push({
            field: 'transactionId',
            order: -1
        });
        this.sortBy = this._cub_dataService.formatSortQueryParam(this.multiSortMeta);
    }

    ngOnInit() {
        //TODO: How is this suppose to work and where am I supposed to get the purses from?
        this.purseControl = {
            id: 'purseControl',
            formFieldClass: 'form-field-drop-down',
            fieldWidthClass: 'form-field-width-L',
            label: 'Purse:',
            value: '',
            options: []
        };

        this.viewTypeControl = {
            id: 'viewTypeControl',
            formFieldClass: 'form-field-drop-down',
            fieldWidthClass: 'form-field-width-L',
            label: 'View Type:',
            value: 'simple',
            options: []
        };

        this.financialTransactionTypeControl = {
            id: 'financialTransactionTypeControl',
            formFieldClass: 'form-field-drop-down',
            fieldWidthClass: 'form-field-width-L',
            label: 'Transaction Type:',
            value: 'PurseLoad',
            options: []
        };

        if (!this.oneAccountId) {
            const oneAccountControl = parent.Xrm.Page.getAttribute('cub_oneaccountid');
            if (oneAccountControl) {
                this.oneAccountId = oneAccountControl.getValue();
            }
        }

        const done = this._loadingScreen.add();

        //Load globals for the page
        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.WEb.Angular'])
            .subscribe(globals => {
                this.rowsPerPage = globals['CUB.MSD.Web.Angular']['OAM.BalanceHistory.NumRows'] as number;
                this.startDateOffset = globals['CUB.MSD.Web.Angular']['OAM.BalanceHistory.StartDateOffset'] as number;

                var viewTypeOptionsString = globals['CUB.MSD.Web.Angular']['OAM.BalanceHistory.ViewTypeOptions'] as string;
                var viewTypeOptions = viewTypeOptionsString ? viewTypeOptionsString.split(';') : [];
                for (var i = 0; i < viewTypeOptions.length; i++) {
                    if (viewTypeOptions[i].indexOf(':') === -1) continue;

                    var keyValuePair = viewTypeOptions[i].split(':');
                    if (keyValuePair.length !== 2) continue;
                    this.viewTypeControl.options.push({
                        name: keyValuePair[0],
                        value: keyValuePair[1]
                    });
                }

                var financialTxnTypeString = globals['CUB.MSD.Web.Angular']['OAM.BalanceHistory.FinancialTxnType'] as string;
                var financialTxnTypeOptions = financialTxnTypeString ? financialTxnTypeString.split(';') : [];
                for (var i = 0; i < financialTxnTypeOptions.length; i++) {
                    if (financialTxnTypeOptions[i].indexOf(':') === -1) continue;

                    var keyValuePair = financialTxnTypeOptions[i].split(':');
                    if (keyValuePair.length !== 2) continue;
                    this.financialTransactionTypeControl.options.push({
                        name: keyValuePair[0],
                        value: keyValuePair[1]
                    });
                }
            });

        const customerId = parent.Xrm.Page.data.entity.getId();
        this._cub_oneAccountService.getCachedOneAccountSummary(customerId)
            .then(data => {
                if (data.purse) {
                    const purses = lodash.map(data.purse, (p: any) => ({
                        name: p.nickName,
                        value: p.purseId
                    }));
                    this.purseControl.options = purses;
                    this.purseControl.value = purses.length > 0 ? purses[0].value : '';
                }
            })
            .then(() => this.getBalanceHistory())
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    //Handle grid loading on paging
    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.disableSearch = false;
            this.getBalanceHistory();
        }
    }

    getBalanceHistory() {
        let accountId = this.oneAccountId;
        let startDateTime = this.startDateFilter || '';
        let endDateTime = startDateTime && this.endDateFilter ? this.endDateFilter : '';
        let viewType = this.viewTypeControl.value;
        let purseId = this.purseControl.value || null;
        let financialTxnType = this.financialTransactionTypeControl.value;
        let offset = (this.pagingOffset > 0) ? this.pagingOffset : 0;
        let limit = this.rowsPerPage;

        const done = this._loadingScreen.add("Loading OneAccount Balance History...");

        this._cub_oneAccountService.getBalanceHistory(accountId, startDateTime, endDateTime, viewType, purseId, financialTxnType, this.sortBy, offset, limit)
            .then((balanceHistory) => {
                if (balanceHistory) {
                    this.balanceHistoryTotalCount = balanceHistory.totalCount;
                    this.balanceHistory = balanceHistory.lineItems;
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting balance history');
            })
            .then(() => this.initComplete = true)
            .then(() => this.disableSearch = true)
            .then(() => done());
    }

    /**
     * Fires when the start date text is modified or a date is selected using the
     * date picker. This sets the start date filter used in getBalanceHistory()
     * and determines whether the end date control should be disabled.
     */
    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableSearch = false;
    }

    /**
     * Fires when the end date text is modified or a date is selected using the
     * date picker. This sets the end date filter used in getBalanceHistory().
     */
    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableSearch = false;
    }

    /**
     * Fires when the enter key is pressed while any of search, start date, or
     * end date controls are in focus. Calls getBalanceHistory().
     */
    onEnterKey() {
        if (!this.disableSearch) {
            this.getBalanceHistory();
            this.disableSearch = true;
        }
    }

    /**
    * Generic onChange function that enables the search button. Fires when a
    * dropdown control changes.
    */
    onDropdownChanged() {
        this.disableSearch = false;
    }

    sortResults(field: string, inAscendingOrder: boolean) {
        if (field.toLocaleLowerCase() === "BestMatch".toLocaleLowerCase() || this.balanceHistory.length === 0) {
            this.sortedBalanceHistory = this.balanceHistory;
            return;
        }

        var sortOrder = inAscendingOrder ? 'asc' : 'desc';

        this.sortedBalanceHistory = lodash.orderBy(this.balanceHistory, [field], [sortOrder]);
    }

    ///**
    // * Hides the balance detail modal window.
    // */
    closeDetail() {
        this.showBalanceHistoryDetail = false;
    }

    /**
    * Fires when a row is clicked within the balance history table. This
    * calls the service to get the selected balance history's detail, prepares
    * the data to display within the detail modal window, and resets the resend
    * modal window controls.
    * @param event event.data is the selected balance record
    */
    onRowDoubleClicked(event: any) {
        let balanceHistory = <OneAccountBalanceHistoryLineItem>event.data;

        // The selected balance detail data is already loaded.
        if (!!this._cub_oneAccountService.selected && balanceHistory.journalEntryId === this._cub_oneAccountService.selected.journalEntryId) {
            this.showBalanceHistoryDetail = true;
            return;
        }

        this._cub_oneAccountService.selected = balanceHistory;
        const done = this._loadingScreen.add("Loading Balance Details...");

        this._cub_oneAccountService.retrieveBalanceHistoryDetail(this.oneAccountId, balanceHistory.journalEntryId.toString())
            .then((data: any) => {
                if (data) {
                    this.balanceHistoryDetail = data;
                    this.showBalanceHistoryDetail = true;
                }
            }).catch(() => {
                this._cub_dataService.prependCurrentError('Error loading balance details');
            })
            .then(() => done());
    }
}