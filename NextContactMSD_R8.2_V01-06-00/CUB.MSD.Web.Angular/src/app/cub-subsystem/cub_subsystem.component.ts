﻿import { Component, OnInit, HostListener, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';

import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'cub-subsystem',
    templateUrl: './cub_subsystem.component.html',
    providers: [Cub_SubsystemService]
})
export class Cub_SubsystemComponent implements OnInit, OnDestroy {
    transitAccountId: string;
    subsystemId: string;
    tokenId: string;
    customerName: string;
    contactName: string;
    summary: any;
    showStatusHistory: boolean = false;
    showTokenStatusHistory: boolean = false;
    showConfirmBlockTerminatePage: boolean = false;
    block: boolean = true;
    terminate: boolean = true;

    selectedTokenIndex: number = 0;

    passes: Model.TransitAccountPass[];
    tokens: Model.TransitAccountToken[];
    passesTokenControl: Model.Cub_Dropdown = {
        id: 'passesTokenControl',
        value: 'passes-token-all',
        options: [
            {
                name: 'All',
                value: 'passes-token-all'
            }
        ]
    };
    passesStatusControl: Model.Cub_Radiolist = {
        id: 'passesStatusControl',
        group: 'passesStatusControl',
        value: 'passes-status-all',
        isButton: true,
        controls: [
            {
                id: 'passes-status-all',
                label: 'All',
                radioClass: 'form-field-width-25'
            }, {
                id: 'passes-status-active',
                label: 'Active',
                radioClass: 'form-field-width-35'
            }, {
                id: 'passes-status-inactive',
                label: 'Inactive',
                radioClass: 'form-field-width-40'
            }
        ]
    };
    passesStatusFilter: boolean;

    constructor(
        public _cub_dataService: Cub_DataService,
        private _cub_subsystemService: Cub_SubsystemService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute,
        private _zone: NgZone
    ) {
        parent['Cubic'] = parent['Cubic'] || {};
        parent['Cubic'].cub_transitaccount = parent['Cubic'].cub_transitaccount || {};
        parent['Cubic'].cub_transitaccount.subsystem = {
            component: this,
            zone: this._zone
        };
    }

    ngOnDestroy() {
        try {
            parent['Cubic'].cub_transitaccount.subsystem = null;
        } catch (e) { }
    }

    ngOnInit() {
        this.getFormFieldValues();

        const subsystemDone = this._loadingScreen.add('Loading Subsystem...');
        this._cub_subsystemService.getCachedSubsystemStatus(this.subsystemId, this.transitAccountId)
            .then(data => this.extractData(data))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => subsystemDone());
    }

    refresh() {
        this.customerName = null;
        this.contactName = null;
        this.getFormFieldValues();

        const done = this._loadingScreen.add('Refreshing');
        this._cub_subsystemService.refreshSubsystemStatus(this.subsystemId, this.transitAccountId)
            .then(data => this.extractData(data))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    getFormFieldValues() {
        this.transitAccountId = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
        this.subsystemId = parent.Xrm.Page.getAttribute('cub_subsystemid').getValue();
        if (parent.Xrm.Page.getAttribute('cub_customer')
            && parent.Xrm.Page.getAttribute('cub_customer').getValue()) {
            this.customerName = parent.Xrm.Page.getAttribute('cub_customer').getValue()[0].name;
        }
        if (parent.Xrm.Page.getAttribute('cub_contact')
            && parent.Xrm.Page.getAttribute('cub_contact').getValue()) {
            this.contactName = parent.Xrm.Page.getAttribute('cub_contact').getValue()[0].name;
        }
    }

    extractData(data: any) {
        
        this.summary = data;
        this.setDebtCollectionInfo(data.debtCollectionInfo);
        this.setSubsystemAccountStatus(data.accountStatus);
        this.passes = _.map(data.passes, (p: any) => {
            const deactivated = moment(p.deactivatedDateTime);
            return _.assign<Model.TransitAccountPass>(p, {
                activeStatus: deactivated.isValid() && moment().isBefore(deactivated)
                    ? 'Active'
                    : 'Inactive'
            });
        });
        this.tokens = _.map(data.tokens, t => {
            return _.assign<Model.TransitAccountToken>(t, {
                passesControl: {
                    id: this._cub_dataService.getCellValue(t, 'tokenInfo.maskedPAN', 'tokenInfo.serialNumber', 'tokenInfo.serializedMobileToken'),
                    label: this.passes.length.toString()
                }
            })
        });
        this.selectedTokenIndex = 0;
    }

    setDebtCollectionInfo(debtCollectionInfo: Model.WSDebtCollectionInfo) {
        parent['Cubic'] = parent['Cubic'] || {};
        parent['Cubic'].cub_transitaccount = parent['Cubic'].cub_transitaccount || {};
        parent['Cubic'].cub_transitaccount.debtCollectionInfo = debtCollectionInfo;
        parent.Xrm.Page.ui.refreshRibbon();
    }

    setSubsystemAccountStatus(accStatus: string) {
        parent['Cubic'].cub_transitaccount = parent['Cubic'].cub_transitaccount || {};
        parent['Cubic'].cub_transitaccount.accountStatus = accStatus;
        parent.Xrm.Page.ui.refreshRibbon();
    }
    /**
     * Called whenever passes or the passes table controls change. This returns
     * a filtered list of passes based on the filter controls, and this return
     * value is passed directly to the PrimeNG datatable.
     * @returns filtered list of passes
     */
    getPassesForTable(): Model.TransitAccountPass[] {
        return _.filter(this.passes, p => (this.passesTokenControl.value === 'passes-token-all')
            && (this.passesStatusControl.value === 'passes-status-all' || p.isActive === this.passesStatusFilter));
    }

    /**
     * Selects this token's passes column
     * @param event event.data contains the token object
     */
    onTokenRowClicked(token: any) {
        const index = _.findIndex(this.tokens, t => _.isEqual(t, token));
        this.selectedTokenIndex = index;
        //document.getElementById(event.data.passesControl.id).click();
    }

    /**
     * Fires when a token table row's passes column is clicked.
     * @param tokenId the selected token passes control identifier
     */
    onTokenPassesChanged(tokenId: any) {
        console.log('passes clicked', tokenId);
    }

    /**
     * Fires when the passes table status control changes. This updates the
     * passes status filter.
     */
    onPassesStatusChanged() {
        if (this.passesStatusControl.value === 'passes-status-active') {
            this.passesStatusFilter = true;
        } else if (this.passesStatusControl.value === 'passes-status-inactive') {
            this.passesStatusFilter = false;
        }
    }

    customerClicked() {
        const customerId = parent.Xrm.Page.getAttribute('cub_customer').getValue()[0].id;
        parent.Xrm.Utility.openEntityForm('account', customerId);
    }

    contactClicked() {
        const contactId = parent.Xrm.Page.getAttribute('cub_contact').getValue()[0].id;
        parent.Xrm.Utility.openEntityForm('contact', contactId);
    }

    statusClicked() {
        this.showStatusHistory = true;
    }

    hideStatusHistory() {
        this.showStatusHistory = false;
    }

    tokenStatusClicked(record: any) {
        let subsystemAtt = parent.Xrm.Page.getAttribute('cub_subsystemid ');
        if (subsystemAtt && subsystemAtt.getValue() && subsystemAtt.getValue().length != 0) {
            this.subsystemId = subsystemAtt.getValue()[0].id;
        }
        this.tokenId = record ? record.tokenId : null;
        this.showTokenStatusHistory = true;
    }

    hideTokenStatusHistory() {
        this.showTokenStatusHistory = false;
    }

    onAddValue(balance) {
        console.log('add value clicked:' + balance);
    }

    onViewAutoloads(balance) {
        console.log('view autoloads clicked:', balance);
    }

    onAdjustValue(balance) {
        let dialogOptions = new parent.Xrm['DialogOptions']();
        let purses = this.summary.purses;
        // Trying to get the customer ID
        let customerAtt = parent.Xrm.Page.getAttribute('cub_customer');
        let customerId: string = null;
        if (customerAtt && customerAtt.getValue() && customerAtt.getValue().length != 0) {
            customerId = customerAtt.getValue()[0].id;
        }
        // Trying to get the contact ID
        let contactAtt = parent.Xrm.Page.getAttribute('cub_contact');
        let contactId: string = null;
        if (contactAtt && contactAtt.getValue() && contactAtt.getValue().length != 0) {
            contactId = contactAtt.getValue()[0].id;
        }

        let requestBody = JSON.stringify({
            transitAccountId: this.transitAccountId,
            subsystemId: this.subsystemId,
            customerId: customerId,
            contactId: contactId,
            purseRestriction: balance.purseRestriction,
            purseType: balance.purseType
        });
        let data = encodeURIComponent(JSON.stringify({
            route: 'TA_AdjustValues',
            queryParams: [
                'purses=', JSON.stringify(purses),
                '&requestBody=', requestBody
            ].join('')
        }));
        let modalUrl = window.location.origin + window.location.pathname + '?Data=' + data;
        dialogOptions.width = 500;
        dialogOptions.height = 800;

        parent.Xrm['Internal'].openDialog(modalUrl, dialogOptions, null, null, (response) => {
            if (response == null) {
                return;
            }
            try {
                parent['Cubic'].cub_transitaccount.subsystem.zone.run(() => {
                    parent['Cubic'].cub_transitaccount.subsystem.component.onModalClose(response);
                });
            } catch (e) {
                console.warn('Unable to handle modal close callback', e);
            }
        });
    }


    activeOrSuspended(record)
    {
        if (record.status == 'Active') return 'Active';
        if (record.status == 'Suspended') return 'Suspended';
        return 'Active or Suspended'
    }

    blockTermClicked(record: any, block: boolean, terminate: boolean) {
        let subsystemAtt = parent.Xrm.Page.getAttribute('cub_subsystemid ');
        if (subsystemAtt && subsystemAtt.getValue() && subsystemAtt.getValue().length != 0) {
            this.subsystemId = subsystemAtt.getValue()[0].id;
        }

        this.tokenId = record ? record.tokenId : null;

        let customerAtt = parent.Xrm.Page.getAttribute('cub_customerid');
        let customerId: string = null;
        if (customerAtt && customerAtt.getValue() && customerAtt.getValue().length != 0) {
            customerId = customerAtt.getValue()[0].id;
        }

        this.block = block;
        this.terminate = terminate;
        this.showConfirmBlockTerminatePage = true;
    }

    onModalClose(response) {
        this.showConfirmBlockTerminatePage = false;
        if (response['refresh']) {
            this.refresh();
        }
    }
}