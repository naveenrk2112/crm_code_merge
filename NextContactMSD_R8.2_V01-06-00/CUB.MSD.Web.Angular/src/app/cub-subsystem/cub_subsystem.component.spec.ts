﻿import { ComponentFixture, TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { DataTableModule } from 'primeng/primeng';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { TextMaskModule } from 'angular2-text-mask';
import * as moment from 'moment';
import { find } from 'lodash';
import * as Model from '../model';

import { Cub_SubsystemComponent } from './cub_subsystem.component';
import { Cub_BalancesComponent } from '../cub-balances/cub-balances.component';
import { Cub_ButtonComponent } from '../controls/cub-button/cub_button.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_HelpComponent } from '../controls/cub-help/cub_help.component';
import { Cub_OptionSelectComponent } from '../controls/cub-optionselect/cub_optionselect.component';
import { Cub_RadioComponent } from '../controls/cub-radio/cub_radio.component';
import { Cub_RadiolistComponent } from '../controls/cub-radiolist/cub_radiolist.component';
import { Cub_TextboxComponent } from '../controls/cub-textbox/cub_textbox.component';
import { Cub_TA_StatusHistoryComponent } from '../cub-ta-statushistory/cub_ta_statushistory.component';
import {Cub_CustomizableGridComponent } from '../controls/cub-customizablegrid/cub_customizablegrid.component';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { MomentDatePipe } from '../pipes/moment-date.pipe';
import { MoneyPipe } from '../pipes/money.pipe';
import { PhonePipe } from '../pipes/phone.pipe';
import { TravelTokenIdPipe } from '../pipes/travel-token-id.pipe';

import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';
import { Cub_MenuToggleDirective } from '../cub-menu-toggle/cub-menu-toggle.directive';

import { ActivatedRouteStub } from '../testutilities/route.stub';
import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_SubsystemServiceStub } from '../testutilities/cub_subsystem.service.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

describe('Cub_SubsystemComponent', () => {
    let fixture: ComponentFixture<Cub_SubsystemComponent>,
        comp: Cub_SubsystemComponent,
        service: Cub_SubsystemService,
        dataService: Cub_DataService,
        root: DebugElement,
        table;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                DataTableModule,
                NguiDatetimePickerModule,
                TextMaskModule
            ],
            declarations: [
                Cub_SubsystemComponent,
                Cub_BalancesComponent,
                Cub_DropdownComponent,
                Cub_RadioComponent,
                Cub_RadiolistComponent,
                MomentDatePipe,
                MoneyPipe,
                PhonePipe,
                TravelTokenIdPipe,
                Cub_TooltipDirective,
                Cub_TextboxComponent,
                Cub_ButtonComponent,
                Cub_ControlLabelComponent,
                Cub_HelpComponent,
                Cub_OptionSelectComponent,
                Cub_MenuToggleDirective,
                Cub_TA_StatusHistoryComponent,
                Cub_CustomizableGridComponent
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
            ]
        }).overrideComponent(Cub_SubsystemComponent, {
            set: {
                providers: [
                    { provide: Cub_SubsystemService, useClass: Cub_SubsystemServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        // TODO: use xrm-mock-generator or something else
        parent.Xrm = {
            Page: {
                data: {
                    entity: {
                        getPrimaryAttributeValue: () => ''
                    }
                },
                getAttribute: (key) => ({
                    getValue: () => null
                }),
                ui: {
                    refreshRibbon: () => { }
                }
            },
            Utility: {
                openEntityForm: (...foo) => { }
            }
        } as any;
        fixture = TestBed.createComponent(Cub_SubsystemComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
        service = root.injector.get(Cub_SubsystemService);
        dataService = TestBed.get(Cub_DataService);
        table = root.nativeElement.querySelector('body.white-background > div.hybrid');
        Cub_SubsystemServiceStub._subsystemData = {
            passes: [],
            purses: [],
            tokens: []
        };
    });

    it('should initialize and render', fakeAsync(() => {
        spyOn(service, 'getSubsystemStatus').and.callThrough();
        spyOn(comp, 'getPassesForTable').and.callThrough();
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        expect(comp.passesStatusControl).toBeDefined();
        expect(comp.passesStatusControl.value).toBeTruthy();
        expect(comp.passesStatusControl.controls.length).toBeGreaterThan(0);
        expect(comp.passesTokenControl).toBeDefined();
        expect(comp.passesTokenControl.value).toBeTruthy();
        expect(comp.passesTokenControl.options.length).toBeGreaterThan(0);
        expect(service.getSubsystemStatus).toHaveBeenCalled();
        expect(comp.passes).toBeDefined();
        expect(comp.summary).toBeDefined();
        expect(comp.tokens).toBeDefined();
        expect(comp.getPassesForTable).toHaveBeenCalled();
    }));

    it('#getPassesForTable should return all passes by default', fakeAsync(() => {
        Cub_SubsystemServiceStub._subsystemData.passes = [
            {
                deactivatedDateTime: '2017-01-01T00:00:00Z',
                createdDateTime: '2017-01-01T00:00:00Z',
                endDateTime: '2017-01-01T00:00:00Z',
                expirationDateTime: '2017-01-01T00:00:00Z',
                startDateTime: '2017-01-01T00:00:00Z',
                deactivatedReason: 'TestReason1',
                durationType: 'TestType1',
                fareProductTypeId: 'TestFareProductTypeId1',
                passDescription: 'TestDescription1',
                passSKU: 'TestSKU1',
                passSerialNbr: 'TestSerialNumber1',
                remainingDuration: 100,
                supportsAutoload: false,
                isActive: false
            }, {
                deactivatedDateTime: '2017-11-01T00:00:00Z',
                createdDateTime: '2017-11-01T00:00:00Z',
                endDateTime: '2017-11-01T00:00:00Z',
                expirationDateTime: '2017-11-01T00:00:00Z',
                startDateTime: '2017-11-01T00:00:00Z',
                deactivatedReason: 'TestReason2',
                durationType: 'TestType2',
                fareProductTypeId: 'TestFareProductTypeId2',
                passDescription: 'TestDescription2',
                passSKU: 'TestSKU2',
                passSerialNbr: 'TestSerialNumber2',
                remainingDuration: 100,
                supportsAutoload: false,
                isActive: true
            }
        ];
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        let filteredPasses = comp.getPassesForTable();
        expect(filteredPasses).toEqual(Cub_SubsystemServiceStub._subsystemData.passes);
    }));

    it('#getPassesForTable should filter passes on status', fakeAsync(() => {
        Cub_SubsystemServiceStub._subsystemData.passes = [
            {
                deactivatedDateTime: '2017-01-01T00:00:00Z',
                createdDateTime: '2017-01-01T00:00:00Z',
                endDateTime: '2017-01-01T00:00:00Z',
                expirationDateTime: '2017-01-01T00:00:00Z',
                startDateTime: '2017-01-01T00:00:00Z',
                deactivatedReason: 'TestReason1',
                durationType: 'TestType1',
                fareProductTypeId: 'TestFareProductTypeId1',
                passDescription: 'TestDescription1',
                passSKU: 'TestSKU1',
                passSerialNbr: 'TestSerialNumber1',
                remainingDuration: 100,
                supportsAutoload: false,
                isActive: false
            }, {
                deactivatedDateTime: '2017-11-01T00:00:00Z',
                createdDateTime: '2017-11-01T00:00:00Z',
                endDateTime: '2017-11-01T00:00:00Z',
                expirationDateTime: '2017-11-01T00:00:00Z',
                startDateTime: '2017-11-01T00:00:00Z',
                deactivatedReason: 'TestReason2',
                durationType: 'TestType2',
                fareProductTypeId: 'TestFareProductTypeId2',
                passDescription: 'TestDescription2',
                passSKU: 'TestSKU2',
                passSerialNbr: 'TestSerialNumber2',
                remainingDuration: 100,
                supportsAutoload: false,
                isActive: true
            }
        ];
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        comp.passesStatusControl.value = 'passes-status-active';
        comp.onPassesStatusChanged();
        let filteredPasses = comp.getPassesForTable();
        expect(filteredPasses.length).toBe(1);
        expect(filteredPasses[0].passSerialNbr).toEqual(Cub_SubsystemServiceStub._subsystemData.passes[1].passSerialNbr);
    }));

    it('#onPassesStatusChanged should update the passes grid status filter', fakeAsync(() => {
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        expect(comp.passesStatusFilter).toBeUndefined();
        comp.passesStatusControl.value = 'passes-status-active';
        comp.onPassesStatusChanged();
        expect(comp.passesStatusFilter).toBe(true);
        comp.passesStatusControl.value = 'passes-status-inactive';
        comp.onPassesStatusChanged();
        expect(comp.passesStatusFilter).toBe(false);
    }));
});