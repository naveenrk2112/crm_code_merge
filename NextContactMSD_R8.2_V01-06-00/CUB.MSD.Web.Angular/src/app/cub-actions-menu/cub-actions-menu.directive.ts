﻿import { Directive, Input, OnChanges, SimpleChanges, ElementRef, Renderer2 } from '@angular/core';

import { Cub_CustomizableGridAction } from '../model';

import * as _ from 'lodash';

/**
 * This directive should be placed somewhere within the body of a grid column.
 * It takes a row object of record data and an array of custom grid actions,
 * and it determines which actions to display for the current row.
 *
 * The actions enabled for the row are copied to the row object's "msdActions" field.
 */
@Directive({
    selector: 'cub-actions-menu'
})
export class Cub_ActionsMenuDirective implements OnChanges {
    constructor(private _elRef: ElementRef, private _renderer: Renderer2) { }

    @Input() row: any;
    @Input() actions: Cub_CustomizableGridAction[];
    private parentCell: Element;

    /**
     * So long as both row and actions are defined, determine which actions to
     * include for the row and update the actions CSS class
     * @param changes
     */
    ngOnChanges(changes: SimpleChanges) {
        if ((changes.row || changes.actions)
            && this.row && this.actions
        ) {
            this.addActionsToRow();
            this.setActionsClass();
        }
    }

    /**
     * Only include actions that satisfy the condition (if there is one).
     * If there is no condition, it should be included by default.
     */
    addActionsToRow() {
        this.row.msdActions = _.filter(this.actions,
            a => this.checkActionCondition(a.condition));
    }

    /**
     * If an action has a condition function, execute it against the currect row
     * data and return the boolean result. If no condition, default to true. If
     * the condition can't be tested, exclude the action.
     * @param condition {boolean|string} if boolean, is returned; if string, is either a global function name or a stringified function
     * @returns {boolean} true if the row data satisfies the condition, false otherwise
     */
    checkActionCondition(condition: string) {
        if (condition == null) {
            return true;
        }
        if (condition.length < 6) {
            const lowercase = condition.toLowerCase();
            if (lowercase === 'true' || lowercase === 'false') {
                return JSON.parse(lowercase);
            }
        }

        let conditionFn;
        let winScope = window;
        // check each window object for a globally-defined function name
        do {
            if (_.has(winScope, condition)) {
                conditionFn = _.get(winScope, condition);
            }
            winScope = winScope.parent;
        } while (conditionFn == null && winScope !== winScope.parent);

        // assume condition is a stringified function
        if (conditionFn == null) {
            conditionFn = eval('(' + condition + ')');
        }

        if (typeof conditionFn === 'function') {
            return conditionFn.call(null, this.row);
        } else {
            console.warn('Unable to execute custom action condition function. Custom action conditions should be functions defined in a web resource or specified inline. The custom action will be hidden');
            return false;
        }
    }

    /**
     * Based on how many actions are available for a row, set the appropriate CSS class for the table cell
     */
    setActionsClass() {
        const len = this.row.msdActions.length;
        let numActions;
        if (len < 1) {
            numActions = 'actions-zero';
        } else if (len === 1) {
            numActions = 'actions-one';
        } else if (len > 1) {
            numActions = 'actions-multiple';
        }

        if (!this.parentCell) {
            // Element.closest requires npm polyfill library 'element-closest' for IE.
            this.parentCell = this._elRef.nativeElement.closest('td.actions');
            if (!this.parentCell) {
                console.warn('Cannot find ancestor element td.actions');
                return;
            }
        }

        this._renderer.removeClass(this.parentCell, 'actions-zero');
        this._renderer.removeClass(this.parentCell, 'actions-one');
        this._renderer.removeClass(this.parentCell, 'actions-multiple');
        this._renderer.addClass(this.parentCell, numActions);
    }
}