﻿import { Component, OnInit, HostListener, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';

import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
    selector: 'cub-subsystemaccounts',
    templateUrl: './cub_subsystemaccounts.component.html',
    providers: [
        Cub_CustomerOrderService,
        Cub_OneAccountService,
        Cub_SubsystemService,
        Cub_TransitAccountService
    ]
})
export class Cub_SubsystemAccountsComponent implements OnInit, OnDestroy {
    showConfirmDelinkPage: boolean = false;
    showSaveModal: boolean = false;
    showErrorsModal: boolean = false;
    oneaccountId: any;
    subsystemId: any;
    accountRef: any;
    subsystemAccount: any;
    selectedAccount: any;

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_customerOrderService: Cub_CustomerOrderService,
        private _cub_oneAccountService: Cub_OneAccountService,
        private _cub_subsystemService: Cub_SubsystemService,
        private _cub_transitAccountService: Cub_TransitAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute,
        private _zone: NgZone
    ) {
        parent['Cubic'] = parent['Cubic'] || {};
        parent['Cubic'].account = parent['Cubic'].account || {};
        parent['Cubic'].account.subsystemAccounts = {
            component: this,
            zone: this._zone
        };
    }

    customerId: string;
    selectedValue: any;
    subsystemAccounts: any[] = [];
    numberOfRows: number = 5; //Number of rows to show before paging

    externalSubsystemChange(subsystemAccountReference: any) {
        this.selectedValue = subsystemAccountReference;
    }

    changeSubsystemId(subsystemAccountReference: any) {
        if (this.selectedValue !== subsystemAccountReference) {
            this.selectedValue = subsystemAccountReference;
        } else {
            this.selectedValue = null;
        }

        try {
            parent['Cubic'].account.tokens.zone.run(() => {
                parent['Cubic'].account.tokens.component.externalSubsystemChange(this.selectedValue);
            });
        } catch (e) { }
    }

    rowClicked(event: any) {
        this.changeSubsystemId(event.data.subsystemAccountReference);
    }

    subsystemIdClicked(event: any, data: any) {
        event.preventDefault();

        const done = this._loadingScreen.add('Retrieving transit account');
        this._cub_transitAccountService.getTransitAccount(data.subsystemAccountReference, data.subsystemInfo.subsystem, this.customerId)
            .then(transitAccount => {
                if (transitAccount && transitAccount.Id) {
                    parent.Xrm.Utility.openEntityForm('cub_transitaccount', transitAccount.Id);
                }
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    extractAccounts(data: any) {
        if (!data) {
            return;
        }
        if (!data.hdr || data.hdr.result !== 'Successful') {
            this._cub_dataService.onApplicationError(data.hdr || 'Unable to retrieve OneAccount summary');
            return;
        }

        // Here we attach a purse balance and display token, and then 
        // we sort by account status and most-recently used token.
        this.subsystemAccounts = _.chain(data.linkedAccounts)
            .map<any, any>(account => {
                const mainPurse = _.find<any>(account.subsystemAccountDetailedInfo.purses,
                    p => p.purseType === 'StoredValue' && p.purseRestriction === 'Unrestricted');
                const displayToken = this._cub_oneAccountService.getDisplayToken(account.subsystemAccountDetailedInfo.tokens);
                return _.assign(account, {
                    balance: mainPurse ? mainPurse.balance : 0,
                    displayToken: displayToken
                });
            })
            .orderBy<any>([
                a => a.subsystemAccountDetailedInfo.accountStatus,
                a => a.displayToken.tokenInfo.lastUseDTM
            ], ['asc', 'desc'])
            .value();

        const top = _.head(this.subsystemAccounts);
        if (top) {
            this.changeSubsystemId(top.subsystemAccountReference);
        }
    }

    delinkClicked(record: any) {
        this.subsystemId = record.subsystemInfo ? record.subsystemInfo.subsystem : null;
        this.subsystemAccount = record.subsystemAccountReference;
        this.showConfirmDelinkPage = true;
    }

    blockUnblockClicked(record: any, block: boolean = true) {

        if (record.subsystemInfo == null || record.subsystemInfo.subsystem === null) {
            this._cub_dataService.onApplicationError('Subsystem missing!');
            return;
        }

        if (record.subsystemAccountReference === null) {
            this._cub_dataService.onApplicationError('Subsystem account missing!');
            return;
        }

        let dialogOptions = new parent.Xrm['DialogOptions']();

        let data = encodeURIComponent(JSON.stringify({
            route: 'SubsystemAccountsBlockUnblock',
            queryParams: [
                'subsystemId=', record.subsystemInfo.subsystem,
                '&subsystemAccount=', record.subsystemAccountReference,
                '&block=', block
            ].join('')
        }));
        let modalUrl = window.location.origin + window.location.pathname + '?Data=' + data;
        dialogOptions.width = 360;
        dialogOptions.height = 320;

        parent.Xrm['Internal'].openDialog(modalUrl, dialogOptions, null, null, (response) => {
            if (response == null) {
                return;
            }
            try {
                parent['Cubic'].account.subsystemAccounts.zone.run(() => {
                    parent['Cubic'].account.subsystemAccounts.component.onModalClose(response);
                });
            } catch (e) {
                console.warn('Unable to handle modal close callback', e);
            }
        });
    }

    ngOnInit() {
        this.customerId = parent.Xrm.Page.data.entity.getId();
        const oneAccountControl = parent.Xrm.Page.getAttribute('cub_oneaccountid');
        if (oneAccountControl) {
            this.oneaccountId = oneAccountControl.getValue();
        }

        const done = this._loadingScreen.add('Retrieving accounts...');

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.numberOfRows = globals['CUB.MSD.Web.Angular']['OAM.SubsystemAccounts.NumRows'] as number;
            });

        this._cub_oneAccountService.getCachedOneAccountSummary(this.customerId)
            .then(data => this.extractAccounts(data))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    actionMenuClicked(record: any) {
        this.selectedValue = null;
        this.changeSubsystemId(record.subsystemAccountReference);
    }

    resolveBalanceClicked(record: any) {
        let dialogOptions = new parent.Xrm['DialogOptions']();
        let debt = JSON.stringify(record.subsystemAccountDetailedInfo.debtCollectionInfo);
        let requestBody = JSON.stringify({
            subsystem: record.subsystemInfo.subsystem,
            subsystemAccountReference: record.subsystemAccountReference,
            customerId: parent.Xrm.Page.data.entity.getId(),
        });
        let data = encodeURIComponent(JSON.stringify({
            route: 'ResolveBalance',
            queryParams: [
                'debtCollectionInfo=', debt,
                '&requestBody=', requestBody
            ].join('')
        }));
        let modalUrl = window.location.origin + window.location.pathname + '?Data=' + data;
        dialogOptions.width = 400;
        dialogOptions.height = 296;

        parent.Xrm['Internal'].openDialog(modalUrl, dialogOptions, null, null, (response) => {
            if (response == null) {
                return;
            }
            try {
                parent['Cubic'].account.subsystemAccounts.zone.run(() => {
                    parent['Cubic'].account.subsystemAccounts.component.onModalClose(response);
                });
            } catch (e) {
                console.warn('Unable to handle modal close callback', e);
            }
        });
    }

    onModalClose(response) {
        this.showConfirmDelinkPage = false;
        if (response['refresh']) {
            this.issueRefresh();
        }
        if (response['orderId'] != null) {
            this.showOrderDetail(response['orderId']);
        }
    }

    issueRefresh() {
        const done = this._loadingScreen.add('Refreshing');
        this._cub_oneAccountService.refreshOneAccountSummary(this.customerId)
            .then(data => this.extractAccounts(data))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());

        try {
            parent['Cubic'].account.balances.zone.run(() => {
                parent['Cubic'].account.balances.component.getBalances();
            });
            parent['Cubic'].account.tokens.zone.run(() => {
                parent['Cubic'].account.tokens.component.getTokens();
            });
            parent['Cubic'].session.zone.run(() => {
                parent['Cubic'].session.component.refresh(false);
            })
        } catch (e) {
            this._cub_dataService.onApplicationError(e);
        }
    }

    showOrderDetail(orderId) {
        this._cub_customerOrderService.cacheOrderId(orderId);
        const tabControl = parent.Xrm.Page.ui.tabs.get('order_history_tab');

        if (!tabControl || !tabControl.getVisible()) {
            console.warn('Unable to navigate to Order Detail, could not access Order History tab');
            return;
        }
        tabControl.setDisplayState('expanded');
        tabControl.setFocus();
    }

    ngOnDestroy() {
        try {
            parent['Cubic'].account.subsystemAccounts = null;
        } catch (e) { }
    }
}