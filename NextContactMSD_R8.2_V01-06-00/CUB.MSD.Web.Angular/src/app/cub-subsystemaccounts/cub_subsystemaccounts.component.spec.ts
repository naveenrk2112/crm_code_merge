﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DataTableModule, SharedModule } from 'primeng/primeng';

import { Cub_SubsystemAccountsComponent } from './cub_subsystemaccounts.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';

import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';

import { MomentDatePipe } from '../pipes/moment-date.pipe';
import { MoneyPipe } from '../pipes/money.pipe';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_OneAccountServiceStub } from '../testutilities/cub_oneaccount.service.stub';
import { Cub_TransitAccountServiceStub } from '../testutilities/cub_transitaccount.service.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';

import { Cub_SubsystemAccount } from '../model';

describe('Cub_SubsystemAccountsComponent', () => {
    let comp: Cub_SubsystemAccountsComponent;
    let fixture: ComponentFixture<Cub_SubsystemAccountsComponent>;
    let root: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                DataTableModule
            ],
            declarations: [
                Cub_SubsystemAccountsComponent,
                Cub_ControlLabelComponent,
                Cub_DropdownComponent,
                MomentDatePipe,
                MoneyPipe,
                Cub_TooltipDirective
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub }
            ]
        }).overrideComponent(Cub_SubsystemAccountsComponent, {
            set: {
                providers: [
                    { provide: Cub_OneAccountService, useClass: Cub_OneAccountServiceStub },
                    { provide: Cub_TransitAccountService, useClass: Cub_TransitAccountServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_SubsystemAccountsComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;

        comp.subsystemAccounts = [];
    });

    it('Should create the component', () => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    });

    it('Should have one subsystem account', () => {
        let now = new Date();
        comp.subsystemAccounts.push({
            subsystem: 'TestSubsystem',
            agency: 'TestAgency',
            subsystemId: '111',
            status: 'Active',
            balance: 5.60,
            lastUsed: now,
            tokens: 3
        } as Cub_SubsystemAccount);

        fixture.detectChanges();
        expect(true).toBeTruthy();
    });
});
