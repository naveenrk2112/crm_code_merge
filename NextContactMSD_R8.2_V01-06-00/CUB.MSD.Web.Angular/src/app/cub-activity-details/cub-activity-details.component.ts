import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as Model from '../model';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { CubActivityDetailsService, ActivityDetailParams } from '../services/cub_activity_details.service';
import { CubCustomerService } from '../services/cub_customer.service';
import { Cub_NotificationService } from '../services/cub_notification.service';

import * as moment from 'moment';
import * as lodash from 'lodash';
import { LazyLoadEvent, SortMeta } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'cub-activity-details',
  templateUrl: './cub-activity-details.component.html',
  styleUrls: ['./cub-activity-details.component.css'],
  providers: [CubActivityDetailsService, CubCustomerService, Cub_NotificationService]
})
export class CubActivityDetailsComponent implements OnInit {

    @Input() selectedActivityId: string = "";
    @Input() transitaccountId: string = "";
    @Input() subsystemId: string = "";
    @Input() customerId: string = "";
    @Input() contactId: string = "";
    @Input() figureOutCustomerForTransitAccount: boolean = false;
    @Input() figureOutContactForTransitAccount: boolean = false;
    @Output() onCancel = new EventEmitter<void>();

    detailsLoaded: boolean = true;
    detailResults: any;

    sortedActivities: Array<object> = [];
    customerNotificationResults: Model.NotificationRecord[] = [];

    globalTxnId: string;
    isAuditActivity: boolean;

    /*Pagination and Sorting params*/
    //multiSortMeta: SortMeta[] = [];
    sortBy: string;
    sortNotificationsBy: string = "status.desc";
    pagingOffset: number = 0;
    pagingNotificationOffset: number = 0;
    totalRecordCount: number = 0;
    totalNotificationsCount: number = 0;

    rowsPerPage: number = 10; //default
    initComplete: boolean = false;
    dateRangeOffset: number = 89; // will include end date for a total of 90 days

    customerName: string;
    contactName: string;
    createDateFormatted: string;
    getActivityDetailsFor: string;
    
    constructor(
        private _cubActivityDetailsService: CubActivityDetailsService,
        private _cub_dataService: Cub_DataService,
        public _cub_customerService: CubCustomerService,
        public _cub_notificationService: Cub_NotificationService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {

        if (parent.Xrm.Page.data.entity.getEntityName() == 'account') {
            this.getActivityDetailsFor = parent.Xrm.Page.data.entity.getEntityName();
            if (this.customerId == "")
            {
                this.customerId = parent.Xrm.Page.data.entity.getId();
            }
            this.customerName = parent.Xrm.Page.getAttribute('name').getValue();
            const contactControl = parent.Xrm.Page.getAttribute('primarycontactid');
            if (contactControl && contactControl.getValue()) {
                this.contactId = contactControl.getValue()[0].id;
            }
        }
        else if (parent.Xrm.Page.data.entity.getEntityName() == 'contact') { 
            this.getActivityDetailsFor = 'account';
            if (this.contactId == "")
            {
                this.contactId = parent.Xrm.Page.data.entity.getId();
            }
            const customerControl = parent.Xrm.Page.getAttribute('parentcustomerid');
            if (customerControl && customerControl.getValue()) {
                this.customerId = customerControl.getValue()[0].id;
            }
            this.contactName = parent.Xrm.Page.getAttribute('fullname').getValue();
        }
        else if (parent.Xrm.Page.data.entity.getEntityName() == 'cub_transitaccount') { 
            this.getActivityDetailsFor = parent.Xrm.Page.data.entity.getEntityName();
            if (this.transitaccountId == "") {
                this.transitaccountId = parent.Xrm.Page.data.entity.getId();
            }
            if (this.figureOutCustomerForTransitAccount)
            {
                const customerControl = parent.Xrm.Page.getAttribute('cub_customer');
                if (customerControl && customerControl.getValue()) {
                    this.customerId = customerControl.getValue()[0].id;
                }
            }
            if (this.figureOutContactForTransitAccount)
            {
                const contactControl = parent.Xrm.Page.getAttribute('cub_contact');
                if (contactControl && contactControl.getValue()) {
                    this.contactId = contactControl.getValue()[0].id;
                }
            }

            this.subsystemId = parent.Xrm.Page.getAttribute('cub_subsystemid').getValue();
        }

        if (this.selectedActivityId == '') {
            this._cub_dataService.onApplicationError("Error: activityId not passed.")
            return;
        }

        if (this.transitaccountId == '' && this.subsystemId == '' && this.customerId == '') {
            this._cub_dataService.onApplicationError("Error: transit account id or subsystem id or customer Id id not passed.")
            return;
        }
        if (this.transitaccountId != '' && this.subsystemId == '' && this.customerId == '') {
            this._cub_dataService.onApplicationError("Error: missing subsystem id")
            return;
        }

        const done = this._loadingScreen.add('Loading detail');

        this._cubActivityDetailsService.getActivityDetails(
            {
                customerId: this.customerId,
                transitaccountId: this.transitaccountId,
                subsystemId: this.subsystemId,
                activityId: this.selectedActivityId,
                getActivityDetailsFor: this.getActivityDetailsFor
            })
            .then((data: any) => {
                this.detailResults = JSON.parse(data.Body);
                this.globalTxnId = this.detailResults.globalTxnId;
                this.isAuditActivity = this.detailResults.activityCategory.toLowerCase() == "audit";
                this.createDateFormatted = moment(this.detailResults.createDateTime).format('MM/DD/YYYY h:mm A');
                this.getActivities();
                this.getNotifications();
            })
            .catch(err => {
                this._cub_dataService.prependCurrentError('Error getting activity details');
                this.onCancelClicked();
            })
            .then(() => this.detailsLoaded = true)
            .then(() => done());
        
    }

    onCancelClicked() {
        this.onCancel.emit();
    }

    //Handle grid loading on paging
    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.getActivities();
        }
    }

    onNotificationLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingNotificationOffset = event.first;
            this.sortNotificationsBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.getNotifications();
        }
    }

    getActivities() {
        let customerId = this.customerId;
        let contactId = null;
        let startDateTime =  null;
        let endDateTime =  null;
        let type = null;
        let activitySubject = null;
        let activityCategory = null;
        let channel = null;
        let origin = null;
        let globalTxnId = this.globalTxnId;
        let userName = null;
        let sortBy = null;
        let offset = (this.pagingOffset > 0) ? this.pagingOffset : 0;
        let limit = this.rowsPerPage;

        const done = this._loadingScreen.add('Loading Activities...');
        this._cub_customerService.getActivities({
            customerId: customerId,
            contactId: contactId,
            startDateTime: startDateTime,
            endDateTime: endDateTime,
            type: type,
            activitySubject: activitySubject,
            activityCategory: activityCategory,
            channel: channel,
            origin: origin,
            globalTxnId: globalTxnId,
            userName: userName,
            sortBy: sortBy,
            offset: offset,
            limit: limit
        })
            .then((data) => {
                if (data) {
                    var activitiesResponse = JSON.parse(data.Body);
                    if (activitiesResponse.activities) {
                        this.prepareActivityDataContent(activitiesResponse.activities);
                        this.sortedActivities = activitiesResponse.activities;
                        this.totalRecordCount = activitiesResponse.totalCount;
                    }
                }
            })
            .catch(err => this._cub_dataService.prependCurrentError(err))
            .then(() => this.initComplete = true)
            .then(() => done());
    }

    /**
     * Assemble all search criteria and call the service to get notifications.
     * If the date range is invalid, displays an error and exits;
     */
    getNotifications() {

        let params: Model.NotificationsFilters = {
            globalTxnId: this.globalTxnId,
            offset: this.pagingNotificationOffset,
            limit: this.rowsPerPage,
            sortBy: this.sortNotificationsBy
        };
        
        const done = this._loadingScreen.add('Loading Notifications...');
        this._cub_notificationService.getNotifications(params)
            .then(data => {
                var notificationsResponse = JSON.parse(data.Body)
                if (notificationsResponse.notifications)
                {
                    this.customerNotificationResults = notificationsResponse.notifications;
                    this.totalNotificationsCount = notificationsResponse.totalCount;
                }

            })
            .catch(err => this._cub_dataService.prependCurrentError(err))
            .then(() => done());
    }


    /**
     * Concatenates the content, contentMap and targetDetails field to contentCell, contentMapCell and targetDetailsCell respectivily.
     * The fields contentCell, contentMapCell and targetDetailsCell are the ones used by the grid to show data.
     * @param activities
     */
    prepareActivityDataContent(activities) {
        for (var i = 0; i < activities.length; i++) {
            var activity = activities[i];
            /*
             * activityData
             */
            var contentCell = "";
            if (activity.activityData && activity.activityData.content) {
                for (var j = 0; j < activity.activityData.content.length; j++) {
                    var activitydata = activity.activityData.content[j];
                    contentCell += `${activitydata.key} : ${activitydata.value}<br>`;
                }
                activities[i].contentCell = contentCell;
            }
            /*
             * contentMap
             */
            var fields = "";
            var oldValues = "";
            var newValues = "";
            if (activity.activityData && activity.activityData.contentMap) {
                for (var j = 0; j < activity.activityData.contentMap.length; j++) {
                    var contentMap = activity.activityData.contentMap[j];
                    fields += `${contentMap.key}<br>`;
                    oldValues += `${contentMap.oldValue}<br>`;
                    newValues += `${contentMap.newValue}<br>`;
                }
                activities[i].fields = fields;
                activities[i].oldValues = oldValues;
                activities[i].newValues = newValues;
            }
            /*
             * targetDetails
             */
            var targetDetailsCell = "";
            if (activity.activityData && activity.activityData.contentMap) {
                for (var j = 0; j < activity.activityData.contentMap.length; j++) {
                    var targetDetails = activity.activityData.targetDetails[j];
                    targetDetailsCell += `${targetDetails.key}: ${targetDetails.value}<br>`;
                }
                activities[i].targetDetailsCell = targetDetailsCell;
            }
        }
    }

}
