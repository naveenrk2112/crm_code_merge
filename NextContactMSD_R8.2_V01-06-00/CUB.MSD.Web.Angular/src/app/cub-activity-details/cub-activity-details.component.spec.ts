import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubActivityDetailsComponent } from './cub-activity-details.component';

describe('CubActivityDetailsComponent', () => {
  let component: CubActivityDetailsComponent;
  let fixture: ComponentFixture<CubActivityDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubActivityDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubActivityDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
