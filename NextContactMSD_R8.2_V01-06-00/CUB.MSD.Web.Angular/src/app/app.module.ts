﻿//External Libraries
import { NgModule, Injectable, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataTableModule, SharedModule, TooltipModule, InputTextModule, InputMaskModule, InputTextareaModule, DropdownModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpInterceptor, HTTP_INTERCEPTORS, HttpRequest, HttpEvent, HttpHandler } from '@angular/common/http';
import { NguiDatetimePickerModule, NguiDatetime } from '@ngui/datetime-picker';
import { RouterModule } from '@angular/router';
import { TextMaskModule } from 'angular2-text-mask';

import { CubRoutes } from './routes';
import { AntiCachingRequestInterceptor, EmptyResponseBodyErrorInterceptor, ResponseErrorInterceptor } from './http-intercept';

// Local Modules
import { AppLoadingScreenModule } from './app-loading-screen/app-loading-screen.module';
import { MomentDatePipeModule } from './pipes/moment-date.pipe';
import { MoneyPipeModule } from './pipes/money.pipe';
import { TravelTokenIdPipeModule } from './pipes/travel-token-id.pipe';
import { Cub_DropdownModule } from './controls/cub-dropdown/cub_dropdown.component';

//Application Components
import { AppLandingComponent } from './app-landing.component';
import { AppRoutingComponent } from './app-routing/app-routing.component';
import { Cub_BalancesComponent } from './cub-balances/cub-balances.component';
import { Cub_CustomerRegistrationComponent } from './cub-customerregistration/cub_customerregistration.component';
import { Cub_CustomerSearchComponent } from './cub-customersearch/cub_customersearch.component';
import { Cub_CustomerVerificationComponent } from './cub-customerverification/cub_customerverification.component';
import { Cub_MasterSearchContactComponent } from './cub-mastersearch.contact/cub_mastersearch.contact.component';
import { Cub_MasterSearchTokenComponent } from './cub-mastersearch.token/cub_mastersearch.token.component';
import { Cub_NotificationHistoryComponent } from './cub-notificationhistory/cub_notificationhistory.component';
import { Cub_NotificationPreferencesComponent } from './cub-notificationpreferences/cub_notificationpreferences.component';
import { Cub_OAM_TravelHistoryComponent } from './cub-oam-travelhistory/cub_oam_travelhistory.component';
import { Cub_OneAccountBalancesComponent } from './cub-oneaccount-balances/cub_oneaccountbalances.component';
import { Cub_OneAccountBalanceHistoryComponent } from './cub-oneaccount-balancehistory/cub_oneaccount_balancehistory.component';
import { Cub_SubsystemComponent } from './cub-subsystem/cub_subsystem.component';
import { Cub_SubsystemAccountsComponent } from './cub-subsystemaccounts/cub_subsystemaccounts.component';
import { Cub_SubsystemDelinkCustomerComponent } from './cub-subsystem-delink-customer/cub-subsystem-delink-customer.component';
import { Cub_SubsystemAccountTknBlockUnblockComponent } from './cub-subsystemaccount_tkn_block_unblock/cub_subsytemaccounttkn_block_unblock.component';
import { Cub_TokensComponent } from './cub-tokens/cub_tokens.component';
import { Cub_TA_TapsHistoryComponent } from './cub-ta-tapshistory/cub_ta_tapshistory.component';
import { Cub_TA_TripsHistoryComponent } from './cub-ta-tripshistory/cub_ta_tripshistory.component';
import { Cub_TA_TravelHistoryComponent } from './cub-ta-travelhistory/cub_ta_travelhistory.component';
import { Cub_TA_TravelHistoryDetailComponent } from './cub-ta-travelhistorydetail/cub_ta_travelhistorydetail.component';
import { Cub_TA_StatusHistoryComponent } from './cub-ta-statushistory/cub_ta_statushistory.component';
import { Cub_TA_Token_StatusHistoryComponent } from './cub-ta-token-statushistory/cub_ta_token_statushistory.component';
import { Cub_TA_BalanceHistoryJournalComponent } from './cub-ta-balancehistory-journal/cub-ta-balancehistory-journal.component';
import { Cub_FundingsourceComponent } from './cub-fundingsource/cub-fundingsource.component';
import { Cub_MasterSearchOrderComponent } from './cub-master-search-order/cub-master-search-order.component';
import { Cub_CustomerOrderHistoryComponent } from './cub-customer-orderhistory/cub-customer-orderhistory.component';
import { Cub_AddFundingSourceComponent } from './cub-add-funding-source/cub-add-funding-source.component';
import { Cub_CustomerOrderhistoryDetailComponent } from './cub-customer-orderhistory-detail/cub-customer-orderhistory-detail.component';
import { Cub_CustomerOrderComponent } from './cub-customer-order/cub-customer-order.component';
import { Cub_EditFundingSourceComponent } from './cub-edit-fundingsource/cub-edit-fundingsource.component';
import { CubCustomerActivitiesComponent } from './cub-customer-activities/cub-customer-activities.component';
import { Cub_ProxyRouteComponent } from './cub-proxy-route/cub-proxy-route.component';
import { CubTaBankcardChargeHistoryComponent } from './cub-ta-bankcard-charge-history/cub-ta-bankcard-charge-history.component';
import { CubTaBankcardChargeAggregationComponent } from './cub-ta-bankcard-charge-aggregation/cub-ta-bankcard-charge-aggregation.component';
import { CubTaActivitiesComponent } from './cub-ta-activities/cub-ta-activities.component';
import { CubTaOrderHistoryComponent } from './cub-ta-order-history/cub-ta-order-history.component';
import { CubTaAdjustValuesComponent } from './cub-ta-adjust-values/cub-ta-adjust-values.component';
import { Cub_SessionDetailsComponent } from './cub-session-details/cub-sessiondetails.component';
import { CubTaVoidTripComponent } from './cub-ta-voidtrip/cub-ta-voidtrip.component';
import { CubTaTapCorrectionComponent } from './cub-ta-tap-correction/cub-ta-tap-correction.component';
import { CubTaVoidTapComponent } from './cub-ta-void-tap/cub-ta-void-tap.component';
import { Cub_OrderDetailsComponent } from './cub-order-details/cub-orderdetails.component';


// Tolling
import { Tam_VehiclesComponent } from './tam-vehicles/tam-vehicles.component';
import { Tam_TranspondersComponent } from './tam-transponders/tam-transponders.component';
import { Tam_AccountInformationComponent } from './tam-accountinformation/tam-accountinformation.component';
import { Tam_BalancesComponent } from './tam-balances/tam-balances.component';
import { Tam_AddVehiclesComponent } from './tam-add-vehicles/tam-add-vehicles.component';
import { Tam_AddTranspondersComponent } from './tam-add-transponders/tam-add-transponders.component';
import { Tam_TransactionsComponent } from './tam-transactions/tam-transactions.component';

//Directives
import { Cub_ActionsMenuDirective } from './cub-actions-menu/cub-actions-menu.directive';
import { Cub_TooltipDirective } from './cub-tooltip/cub-tooltip.directive';
import { Cub_MenuToggleDirective } from './cub-menu-toggle/cub-menu-toggle.directive';

//Services
import { Cub_DataService } from './services/cub_data.service';
import { Cub_CacheService } from './services/cub-cache.service';
import { Cub_SessionService } from './services/cub-session.service';
import { Cub_WebApiService } from './services/cub-webapi.service';

//Custom Control Components
import { Cub_AlertComponent } from './controls/cub-alert/cub_alert.component';
import { Cub_ButtonComponent } from './controls/cub-button/cub_button.component';
import { Cub_CheckboxComponent } from './controls/cub-checkbox/cub_checkbox.component';
import { Cub_CheckboxlistComponent } from './controls/cub-checkboxlist/cub_checkboxlist.component';
import { Cub_ControlLabelComponent } from './controls/cub-controllabel/cub_controllabel.component';
import { Cub_ControlsColumnComponent } from './controls/cub-controlscolumn/cub_controlscolumn.component';
import { Cub_ControlSpacerComponent } from './controls/cub-controlspacer/cub_controlspacer.component';
import { Cub_CustomizableGridComponent } from './controls/cub-customizablegrid/cub_customizablegrid.component';
import { Cub_DateRangeComponent } from './controls/cub-date-range/cub-date-range.component';
import { Cub_FlydownComponent } from './controls/cub-flydown/cub_flydown.component';
import { Cub_HelpComponent } from './controls/cub-help/cub_help.component';
import { Cub_MultiDropdown } from './controls/cub-multi-dropdown/cub-multi-dropdown.component';
import { Cub_OptionSelectComponent } from './controls/cub-optionselect/cub_optionselect.component';
import { Cub_RadioComponent } from './controls/cub-radio/cub_radio.component';
import { Cub_RadiolistComponent } from './controls/cub-radiolist/cub_radiolist.component';
import { Cub_SecurityOptionComponent } from './controls/cub-securityoption/cub_securityoption.component';
import { Cub_TextboxComponent } from './controls/cub-textbox/cub_textbox.component';

//Pipes
import { PhonePipe } from './pipes/phone.pipe';
import { HighlightPipe } from './pipes/highlight.pipe';
import { CubActivityDetailsComponent } from './cub-activity-details/cub-activity-details.component';

export function dataServiceFactory(dataService: Cub_DataService) {
    return () => dataService.executeOnLoad();
}

@NgModule({
    imports: [
        RouterModule.forRoot(CubRoutes, { useHash: true }),
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        DataTableModule,
        InputTextModule,
        InputMaskModule,
        InputTextareaModule,
        DropdownModule,
        TextMaskModule,
        NguiDatetimePickerModule,
        TooltipModule,
        AppLoadingScreenModule,
        MomentDatePipeModule,
        MoneyPipeModule,
        Cub_DropdownModule,
        TravelTokenIdPipeModule
    ],
    declarations: [
        AppRoutingComponent,
        AppLandingComponent,
        Cub_BalancesComponent,
        Cub_CustomerRegistrationComponent,
        Cub_CustomerSearchComponent,
        Cub_CustomerVerificationComponent,
        Cub_CustomizableGridComponent,
        Cub_MasterSearchContactComponent,
        Cub_MasterSearchTokenComponent,
        Cub_NotificationHistoryComponent,
        Cub_NotificationPreferencesComponent,
        Cub_OneAccountBalanceHistoryComponent,
        Cub_SubsystemComponent,
        Cub_SubsystemAccountTknBlockUnblockComponent,
        Cub_SecurityOptionComponent,
        Cub_SubsystemAccountsComponent,
        Cub_TA_TravelHistoryComponent,
        Cub_TA_TravelHistoryDetailComponent,
        Cub_TokensComponent,
        Cub_OneAccountBalancesComponent,
        Cub_OAM_TravelHistoryComponent,
        Cub_AlertComponent,
        Cub_ButtonComponent,
        Cub_CheckboxComponent,
        Cub_CheckboxlistComponent,
        Cub_ControlLabelComponent,
        Cub_ControlsColumnComponent,
        Cub_ControlSpacerComponent,
        Cub_DateRangeComponent,
        Cub_FlydownComponent,
        Cub_HelpComponent,
        Cub_OptionSelectComponent,
        Cub_RadioComponent,
        Cub_RadiolistComponent,
        Cub_TextboxComponent,
        PhonePipe,
        HighlightPipe,
        Cub_FundingsourceComponent,
        Cub_AddFundingSourceComponent,
        Cub_MasterSearchOrderComponent,
        Cub_CustomerOrderHistoryComponent,
        Cub_CustomerOrderhistoryDetailComponent,
        Cub_CustomerOrderComponent,
        Cub_EditFundingSourceComponent,
        Cub_ActionsMenuDirective,
        Cub_TooltipDirective,
        Cub_MenuToggleDirective,
        Cub_MultiDropdown,
        Tam_VehiclesComponent,
        Tam_TranspondersComponent,
        Tam_AccountInformationComponent,
        Tam_BalancesComponent,
        Tam_AddVehiclesComponent,
        Tam_AddTranspondersComponent,
        Tam_TransactionsComponent,
        Cub_TA_TapsHistoryComponent,
        Cub_TA_TripsHistoryComponent,
        CubCustomerActivitiesComponent,
        Cub_TA_StatusHistoryComponent,
        Cub_TA_Token_StatusHistoryComponent,
        Cub_SubsystemDelinkCustomerComponent,
        CubTaBankcardChargeHistoryComponent,
        Cub_ProxyRouteComponent,
        Cub_TA_BalanceHistoryJournalComponent,
        CubTaBankcardChargeAggregationComponent,
        CubTaActivitiesComponent,
        CubTaOrderHistoryComponent,
        CubTaAdjustValuesComponent,
        CubActivityDetailsComponent,
        CubTaVoidTripComponent,
        Cub_SessionDetailsComponent,
        CubTaTapCorrectionComponent,
        CubTaVoidTapComponent,
        Cub_OrderDetailsComponent
    ],
    exports: [RouterModule],
    providers: [
        Cub_DataService,
        Cub_CacheService,
        Cub_SessionService,
        Cub_WebApiService,
        { provide: HTTP_INTERCEPTORS, useClass: AntiCachingRequestInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ResponseErrorInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: EmptyResponseBodyErrorInterceptor, multi: true },
        { provide: APP_INITIALIZER, useFactory: dataServiceFactory, deps: [Cub_DataService], multi: true }
    ],
    bootstrap: [AppRoutingComponent]
})
export class AppModule { }