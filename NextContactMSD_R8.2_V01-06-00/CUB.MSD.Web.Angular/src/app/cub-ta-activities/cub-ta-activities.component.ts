import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as Model from '../model';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { CubTaActivitiesService } from '../services/cub_ta_activities.service';

import * as moment from 'moment';
import * as lodash from 'lodash';
import { LazyLoadEvent, SortMeta } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'cub-cub-ta-activities',
  templateUrl: './cub-ta-activities.component.html',
  styleUrls: ['./cub-ta-activities.component.css', '../../svg-temp.css'], // TODO: remove svg-temp.css when refresh icon updated
  providers: [CubTaActivitiesService]
})
export class CubTaActivitiesComponent implements OnInit {

    @Input() transitAccountId: string = "";
    @Input() subsystemId: string = "";
    @Input() customerId: string = "";
    @Input() contactId: string = "";
    activities: Array<object> = [];
    sortedActivities: Array<object> = [];

    acitvityByControl: Model.Cub_Textbox = {
        id: "1",
        label: "Acitvity By",
        includeLabel: true
    };

    groupIdControl: any;

    activityTypeOptions: Model.Cub_MultiDropdownOption[];
    channelControlOptions: Model.Cub_MultiDropdownOption[];
    activityCategoryOptions: Model.Cub_MultiDropdownOption[];
    activitySubjectOptions: Model.Cub_MultiDropdownOption[];

    activityTypeFilter: string = null;
    channelFilter: string = null;
    activityCategoryFilter: string = null;
    activitySubjectFilter: string = null;

    disableSearch: boolean = false;
    initialPass: boolean = true;
    rowsPerPage: number = 5; //default
    initComplete: boolean = false;
    dateRangeOffset: number = 30;

    /* Filter values */
    startDateFilter: string;
    endDateFilter: string;

    /*Pagination and Sorting params*/
    multiSortMeta: SortMeta[] = [];
    sortBy: string;
    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    showDetail: boolean = false;
    selectedActivityId: any = null;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cubTaActivitiesService: CubTaActivitiesService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute
    ) {
        this.multiSortMeta.push({
            field: 'startDateTime',
            order: -1
        });
        this.multiSortMeta.push({
            field: 'tripId',
            order: -1
        });
        this.sortBy = this._cub_dataService.formatSortQueryParam(this.multiSortMeta);
    }

    ngOnInit() {

        this.groupIdControl = {
            isTableControl: false,
            formFieldClass: 'form-fields-siblings',
            fieldWidthClass: 'form-field-width-80',
            includeLabel: true,
            id: 'groupIdControl',
            label: 'Group ID',
            value: '',
            placeholder: 'Enter a Group ID (GlobalTxnId) #...',
            maxLength: 40
        };

        const queryParams = this.route.snapshot.queryParamMap;

        this.transitAccountId = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();

        const subsystemControl = parent.Xrm.Page.getAttribute('cub_subsystemid');
        if (subsystemControl && subsystemControl.getValue()) {
            this.subsystemId = subsystemControl.getValue();
        }

        const customerControl = parent.Xrm.Page.getAttribute('cub_customer');
        if (customerControl && customerControl.getValue()) {
            this.customerId = customerControl.getValue()[0].id;
        }

        const contactControl = parent.Xrm.Page.getAttribute('cub_contact');
        if (contactControl && contactControl.getValue()) {
            this.contactId = contactControl.getValue()[0].id;
        }

        //Load globals for the page
        const globals$ = this._cub_dataService.Globals.first(globals => !!globals['CUB.MSD.Web.Angular']);
        const grids$ = this._cub_dataService.GridConfigs.first(configs => !!configs['Customer.Activity']);
        const done = this._loadingScreen.add();
        Observable.forkJoin(globals$, grids$)
            .finally(() => done())
            .subscribe(([globals, grids]) => {
                let angularGlobals = globals['CUB.MSD.Web.Angular'];

                // Activity Types control
                this.activityTypeOptions = this.parseFilterOptions(angularGlobals, 'Customer.Activity.Types');

                // Channel Control
                this.channelControlOptions = this.parseFilterOptions(angularGlobals, 'Customer.Activity.Channels');

                // Activity Category
                this.activityCategoryOptions = this.parseFilterOptions(angularGlobals, 'Customer.Activity.Category');

                // Activity Subject
                this.activitySubjectOptions = this.parseFilterOptions(angularGlobals, 'Customer.Activity.Subject');

                // Set start/end date filter initial value
                this.dateRangeOffset = angularGlobals['BackOffice.Activities.StartDateOffset'] as number;
                this.startDateFilter = moment().subtract(this.dateRangeOffset, 'days').format('YYYY-MM-DD[T23:59:59.999Z]');
                this.endDateFilter = moment().startOf('day').format('YYYY-MM-DD[T23:59:59.999Z]');

                let grid = grids['Customer.Activity'];
                this.rowsPerPage = grid.rowsToDisplay;
                this.getActivities();
            });
    }

    //Handle grid loading on paging
    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.disableSearch = false;
            this.getActivities();
        }
    }

    onSearchClicked() {
        this.getActivities();
    }

    getActivities() {
        let transitAccountId = this.transitAccountId;
        let subsystemId = this.subsystemId;
        let customerId = this.customerId;
        let contactId = this.contactId;
        let startDateTime = this.startDateFilter || null;
        let endDateTime = this.endDateFilter || null;
        let type = this.activityTypeFilter;
        let activitySubject = this.activitySubjectFilter;
        let activityCategory = this.activityCategoryFilter;
        let channel = this.channelFilter; 
        let origin = null;
        let globalTxnId = this.groupIdControl.value;
        let userName = this.acitvityByControl.value || null;
        let sortBy = this.sortBy || null;
        let offset = (this.pagingOffset > 0) ? this.pagingOffset : 0;
        let limit = this.rowsPerPage;

        const done = this._loadingScreen.add('Loading Activities...');
        this._cubTaActivitiesService.getActivities({
            transitAccountId: transitAccountId,
            subsystemId: subsystemId,
            //customerId: customerId,
            //contactId: contactId,
            startDateTime: startDateTime,
            endDateTime: endDateTime,
            type: type,
            activitySubject: activitySubject,
            activityCategory: activityCategory,
            channel: channel,
            origin: origin,
            globalTxnId: globalTxnId,
            userName: userName,
            sortBy: sortBy,
            offset: offset,
            limit: limit
        })
            .then((data) => {
                if (data) {
                    var activitiesResponse = JSON.parse(data.Body);
                    if (activitiesResponse.activities) {
                        this.prepareActivityDataContent(activitiesResponse.activities);
                        this.activities = activitiesResponse.activities;
                        this.sortedActivities = activitiesResponse.activities;
                        this.totalRecordCount = activitiesResponse.totalCount;
                        this.disableSearch = false;
                    }
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting customer activities');
            })
            .then(() => this.initComplete = true)
            .then(() => done());
    }

    /**
     * Concatenates the content, contentMap and targetDetails field to contentCell, contentMapCell and targetDetailsCell respectivily.
     * The fields contentCell, contentMapCell and targetDetailsCell are the ones used by the grid to show data.
     * @param activities
     */
    prepareActivityDataContent(activities) {
        for (var i = 0; i < activities.length; i++) {
            var activity = activities[i];
            /*
             * activityData
             */
            var contentCell = "";
            if (activity.activityData && activity.activityData.content) {
                for (var j = 0; j < activity.activityData.content.length; j++) {
                    var activitydata = activity.activityData.content[j];
                    contentCell += `${activitydata.key} : ${activitydata.value}<br>`;
                }
                activities[i].contentCell = contentCell;
            }
            /*
             * contentMap
             */
            var fields = "";
            var oldValues = "";
            var newValues = "";
            if (activity.activityData && activity.activityData.contentMap) {
                for (var j = 0; j < activity.activityData.contentMap.length; j++) {
                    var contentMap = activity.activityData.contentMap[j];
                    fields += `${contentMap.key}<br>`;
                    oldValues += `${contentMap.oldValue}<br>`;
                    newValues += `${contentMap.newValue}<br>`;
                }
                activities[i].fields = fields;
                activities[i].oldValues = oldValues;
                activities[i].newValues = newValues;
            }
            /*
             * targetDetails
             */
            var targetDetailsCell = "";
            if (activity.activityData && activity.activityData.contentMap) {
                for (var j = 0; j < activity.activityData.contentMap.length; j++) {
                    var targetDetails = activity.activityData.targetDetails[j];
                    targetDetailsCell += `${targetDetails.key}: ${targetDetails.value}<br>`;
                }
                activities[i].targetDetailsCell = targetDetailsCell;
            }
        }
    }

    onMultiDropdownChange(selectedOptions: string[], filterName: string) {
        if (this[filterName] === undefined) {
            this._cub_dataService.onApplicationError('No filter with name "' + filterName + '" on cub-ta-activities!');
            return;
        }

        // if the join returns an empty string, use null instead
        this[filterName] = selectedOptions.join(',') || null;
        this.disableSearch = false;
    }

    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableSearch = false;
    }

    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableSearch = false;
    }

    parseFilterOptions(nisGlobals, globalDetailName): Model.Cub_MultiDropdownOption[] {
        let options: Model.Cub_MultiDropdownOption[] = [];
        try {
            let config: object = JSON.parse(nisGlobals[globalDetailName]);
            options = lodash.chain(config)
                .map((name, key) => ({
                    value: key,
                    label: name
                } as Model.Cub_MultiDropdownOption))
                .sortBy(o => o.value)
                .value();
        } catch (e) {
            this._cub_dataService.onApplicationError('Failure to parse config: ' + globalDetailName);
        }
        return options;
    }

    onGroupIdChanged(...foo) { }

    onEnterKey(...foo) { }

    onMultiDropdownChanged(...foo) { }

    /**
    * Fires when a row is double clicked. 
    * @param event event.data is the selected record
    */
    onRowDblClick(event: any) {
        this.selectedActivityId = event.data.activityId;
        this.showDetail = true;
    }

    hideDetail() {
        this.showDetail = false;
    }


}
