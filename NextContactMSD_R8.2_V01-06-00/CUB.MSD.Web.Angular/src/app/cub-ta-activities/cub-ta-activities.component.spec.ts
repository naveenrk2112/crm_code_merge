import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubTaActivitiesComponent } from './cub-ta-activities.component';

describe('CubTaActivitiesComponent', () => {
  let component: CubTaActivitiesComponent;
  let fixture: ComponentFixture<CubTaActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubTaActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubTaActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
