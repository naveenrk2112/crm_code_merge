import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DataTableModule, SharedModule, TooltipModule } from 'primeng/primeng';
import { Cub_CustomerOrderhistoryDetailComponent } from './cub-customer-orderhistory-detail.component';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { APP_BASE_HREF, Location } from '@angular/common';
import { RouterTestingModule } from "@angular/router/testing";

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

//Directives
import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';

import { MomentDatePipe } from '../pipes/moment-date.pipe';
import { MoneyPipe } from '../pipes/money.pipe';

describe('CubCustomerOrderhistoryDetailComponent', () => {
  let component: Cub_CustomerOrderhistoryDetailComponent;
  let fixture: ComponentFixture<Cub_CustomerOrderhistoryDetailComponent>;

  beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
            HttpClientTestingModule,
            DataTableModule,
            TooltipModule,
            RouterTestingModule.withRoutes([{ path: 'CustomerOrder', component: Cub_CustomerOrderhistoryDetailComponent }]),
        ],
        declarations: [
            Cub_CustomerOrderhistoryDetailComponent,
            Cub_TooltipDirective,
            MomentDatePipe,
            MoneyPipe
        ],
        providers: [
            { provide: Cub_DataService, useClass: Cub_DataServiceStub },
            { provide: ActivatedRoute, useClass: ActivatedRouteStub },
            { provide: APP_BASE_HREF, useValue: '/' },
            {provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Cub_CustomerOrderhistoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
