import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_Button, Cub_Textbox, Cub_Dropdown, Cub_Checkbox } from '../model';
import * as lodash from 'lodash';

@Component({
    selector: 'cub-customer-orderhistory-detail',
    templateUrl: './cub-customer-orderhistory-detail.component.html',
    styleUrls: ['./cub-customer-orderhistory-detail.component.css'],
    providers: [Cub_CustomerOrderService]
})
export class Cub_CustomerOrderhistoryDetailComponent implements OnInit {

    @Input() accountId: string;
    @Input() orderId: string;
    @Input() order = {};
    @Output() onClose = new EventEmitter<void>();

    orderHistoryDetailResults: any;
    orderHistoryDetailsPayments: any[];
    orderHistoryDetailsPaymentsLength: number;

    constructor(
        private _cub_customerOrderService: Cub_CustomerOrderService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.order = this.order || {};
        this.orderHistoryDetailResults = {};
        let queryParams = this.route.snapshot.queryParams;
        if (this.accountId == null || this.orderId == null) {
            this.accountId = queryParams['accountId'];
            this.orderId = queryParams['orderId'];

            if (this.accountId == null || this.orderId == null) {
                return;
            }
        }

        const done = this._loadingScreen.add();
        this._cub_customerOrderService.getOrderHistoryDetail(this.accountId, this.orderId)
            .then((data: any) => {
                if (data) {
                    this.orderHistoryDetailResults = data;
                    this.orderHistoryDetailsPayments = data.paymentDetails;
                    if (this.orderHistoryDetailsPayments) {
                        this.orderHistoryDetailsPaymentsLength = this.orderHistoryDetailsPayments.length;
                    }
                } else {
                    this.onClose.emit();
                }
            })
            .catch((error: any) => {
                this.onClose.emit();
            })
            .then(() => done());
    }

    closeDetail() {
        this.onClose.emit();
    }
}
