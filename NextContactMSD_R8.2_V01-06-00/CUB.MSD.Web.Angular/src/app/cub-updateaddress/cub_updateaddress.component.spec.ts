﻿import { ComponentFixture, TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { DebugElement, Injectable } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Cub_UpdateAddressComponent } from './cub_updateaddress.component';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_UpdateAddressService } from '../services/cub_updateaddress.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

import { findIndex, shuffle } from 'lodash';

// Testing Services
class Cub_UpdateAddressServiceStub {
    postToParent = (foo) => {
        expect(foo).toBeDefined('No argument passed to postToParent');
        expect(foo.action).toBeDefined('No action specified in postToParent');
    }
    update = (customerId, address, contacts, fundingSources) => {
        expect(customerId).toBeTruthy();
        expect(address).toBeTruthy();
        expect(contacts).toBeTruthy();
        expect(fundingSources).toBeTruthy();
    }
}

// Testing Constants
const CUSTOMER_ID = '00000000-0000-0000-0000-000000000001';
const ADDRESS = {
    addressId: '00000000-0000-0000-0000-000000000002',
    address1: '123 Sesame St',
    address2: '',
    city: 'New York',
    stateId: '00000000-0000-0000-0000-000000000003',
    postalCodeId: '00000000-0000-0000-0000-000000000004',
    countryId: '00000000-0000-0000-0000-000000000005'
};
const CONTACTS = [
    {
        contactId: '00000000-0000-0000-0000-000000000006',
        name: { firstName: 'Abby', lastName: 'Adams' }
    }, {
        contactId: '00000000-0000-0000-0000-000000000007',
        name: { firstName: 'Billy', lastName: 'Barnes' }
    }
];
const FUNDING_SOURCES = [
    {
        fundingSourceId: '0001',
        creditCard: {
            creditCardType: 'VISA',
            maskedPan: '9876'
        }
    }, {
        fundingSourceId: '0002',
        directDebit: {
            accountType: 'Checking',
            bankAccountNumber: '43218765-02'
        }
    }
]

describe('Cub_UpdateAddressComponent', () => {
    let fixture: ComponentFixture<Cub_UpdateAddressComponent>,
        comp: Cub_UpdateAddressComponent,
        service: Cub_UpdateAddressService,
        root: DebugElement,
        modal;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [Cub_UpdateAddressComponent],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
            ]
        }).overrideComponent(Cub_UpdateAddressComponent, {
            set: {
                providers: [
                    { provide: Cub_UpdateAddressService, useClass: Cub_UpdateAddressServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        ActivatedRouteStub._queryParams = {
            Data: encodeURIComponent(JSON.stringify({
                customerId: CUSTOMER_ID,
                address: ADDRESS,
                contactDependencies: CONTACTS,
                fundingSourceDependencies: FUNDING_SOURCES
            }))
        };
        fixture = TestBed.createComponent<Cub_UpdateAddressComponent>(Cub_UpdateAddressComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
        service = root.injector.get(Cub_UpdateAddressService);
        modal = root.nativeElement.querySelector('div.modal');
    });

    afterAll(() => {
        modal.outerHTML = '';
    });

    it('should do nothing if no query params are passed in', () => {
        ActivatedRouteStub._queryParams = {};
        spyOn(service, 'postToParent');
        fixture.detectChanges();
        expect(comp.address).toBeUndefined();
        expect(comp.options.length).toBe(0);
        expect(service.postToParent).toHaveBeenCalled();
    });

    it('should parse customer, address, and dependencies data from the ActivatedRoute query params', fakeAsync(() => {
        spyOn(service, 'postToParent');
        fixture.detectChanges();
        expect(comp.address).toEqual(ADDRESS);
        expect(comp.customerId).toEqual(CUSTOMER_ID);
        expect(comp.options.length).toEqual(CONTACTS.length + FUNDING_SOURCES.length);
        expect(service.postToParent).not.toHaveBeenCalled();
    }));

    it('should mark all dependencies for update by default', fakeAsync(() => {
        fixture.detectChanges();
        expect(comp.allSelected).toBe(true);
        expect(comp.options.length).toEqual(CONTACTS.length + FUNDING_SOURCES.length);
        expect(findIndex(comp.options, o => !o.update)).toEqual(-1, 'Not all dependencies are marked for update');
        expect(modal.querySelector('button#option-all p').innerHTML).toEqual('Deselect All');
        expect(modal.querySelectorAll('li>input:checked').length).toEqual(CONTACTS.length + FUNDING_SOURCES.length);
    }));

    it('should toggle a dependency\'s update status when it is clicked', fakeAsync(() => {
        fixture.detectChanges();
        expect(comp.options[0].update).toBe(true);
        let optionEl = modal.querySelector('li>input');
        expect(optionEl.checked).toBe(true);

        optionEl.click();
        fixture.detectChanges();
        expect(comp.options[0].update).toBe(false);
        expect(optionEl.checked).toBe(false);

        optionEl.click();
        fixture.detectChanges();
        expect(comp.options[0].update).toBe(true);
        expect(optionEl.checked).toBe(true);
    }));

    it('should disable the update button if all options are unselected', fakeAsync(() => {
        fixture.detectChanges();
        spyOn(comp, 'updateClicked');
        let updateEl = modal.querySelector('.button-primary');
        let allEl = modal.querySelector('#option-all');
        let optionEl = modal.querySelector('input');
        expect(comp.updateIsDisabled()).toBe(false, 'Update is not disabled');
        expect(updateEl.disabled).toBe(false, 'Update is not disabled');

        allEl.click();
        fixture.detectChanges();
        expect(updateEl.disabled).toBe(true, 'Update should now be disabled');
        updateEl.click();
        expect(comp.updateClicked).not.toHaveBeenCalled();

        optionEl.click();
        fixture.detectChanges();
        expect(updateEl.disabled).toBe(false, 'Update should now be enabled');
    }));

    it('should select/unselect all options when Select All is toggled', fakeAsync(() => {
        fixture.detectChanges();
        expect(comp.allSelected).toBe(true);
        let selectAllEl = modal.querySelector('#option-all');
        expect(selectAllEl.innerHTML.trim()).toEqual('<p>Deselect All</p>');

        selectAllEl.click();
        fixture.detectChanges();
        expect(comp.allSelected).toBe(false);
        expect(comp.updateIsDisabled()).toBe(true);
        expect(selectAllEl.innerHTML.trim()).toEqual('<p>Select All</p>');
        expect(modal.querySelectorAll('input:checked').length).toBe(0, 'At least one <input> is still checked');

        selectAllEl.click();
        fixture.detectChanges();
        expect(comp.allSelected).toBe(true);
        expect(selectAllEl.innerHTML.trim()).toEqual('<p>Deselect All</p>');

        modal.querySelector('input').click();
        fixture.detectChanges();
        expect(comp.allSelected).toBe(false);
        expect(comp.updateIsDisabled()).toBe(false);
        expect(selectAllEl.innerHTML.trim()).toEqual('<p>Select All</p>');
    }));

    it('should post a cancel message to it\'s parent when cancel button is clicked', () => {
        fixture.detectChanges();
        spyOn(service, 'postToParent');
        let cancelEl = modal.querySelector('.button-cancel');
        cancelEl.click();
        expect(service.postToParent).toHaveBeenCalledWith({ action: 'cancel' });
    });

    it('should call Cub_UpdateAddressService.update with empty lists if all dependencies are checked', fakeAsync(() => {
        fixture.detectChanges();
        let updateEl = modal.querySelector('.button-primary');

        updateEl.click();

        modal.querySelector('input').click();
        fixture.detectChanges();
        updateEl.click();
    }));
});