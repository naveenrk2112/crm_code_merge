﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_UpdateAddressService } from '../services/cub_updateaddress.service';
import * as Model from '../model';

import * as _ from 'lodash';

@Component({
    selector: 'cub-updateaddress',
    templateUrl: './cub_updateaddress.component.html',
    providers: [Cub_UpdateAddressService]
})
export class Cub_UpdateAddressComponent implements OnInit {
    options: Model.AddressDependency[] = [];
    address: Model.WSAddress;
    customerId: string;
    allSelected: boolean = true;

    constructor(
        private _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private _cub_updateAddressService: Cub_UpdateAddressService,
        private _route: ActivatedRoute
    ) { }

    /**
     * Checks the query params for data and parses a list of dependencies
     * and the address fields.
     */
    ngOnInit() {
        const queryParams = this._route.snapshot.queryParams;
        if (!queryParams['Data']) {
            this._cub_updateAddressService.postToParent({
                action: 'error',
                error: 'No data passed to update address component'
            });
            return;
        }

        let data = JSON.parse(decodeURIComponent(queryParams['Data']));
        this.customerId = data.customerId;
        this.address = data.address;
        this._cub_dataService.currentUserId = data.currentUserId;

        // sort contacts by name and then map to checkbox controls
        let contacts: Model.AddressDependency[] = _.chain(data.contactDependencies)
            .sortBy([
                c => c.name.lastName,
                c => c.name.firstName
            ])
            .map((c: any) => ({
                key: c.contactId,
                value: c.name.firstName + ' ' + c.name.lastName,
                type: 'contact',
                update: true
            }))
            .value();

        // map funding sources to checkbox controls and then sort by display value
        let fundingSources: Model.AddressDependency[] = _.chain(data.fundingSourceDependencies)
            .map((f: any) => {
                let val = 'UNDEFINED';
                if (!!f.creditCard && !!f.creditCard.maskedPan) {
                    val = f.creditCard.creditCardType + ' ' + f.creditCard.maskedPan;
                } else if (!!f.directDebit && !!f.directDebit.bankAccountNumber) {
                    val = f.directDebit.accountType + ' ' + f.directDebit.bankAccountNumber;
                }
                return {
                    key: f.fundingSourceId,
                    value: val,
                    type: 'fundingSource',
                    update: true
                };
            })
            .sortBy(f => f.value)
            .value();

        this.options = contacts.concat(fundingSources);
    }

    /**
     * Fires when a dependency checkbox is clicked; flip the dependency's update
     * field and recheck if all dependencies are selected
     */
    onChange(option: Model.AddressDependency) {
        option.update = !option.update;
        if (!option.update) {
            this.allSelected = false;
        } else if (_.findIndex(this.options, o => !o.update) === -1) {
            this.allSelected = true;
        }
    }

    /**
     * Fires when the Select All checkbox is clicked; flip its value and
     * set each dependency to the new value
     */
    onChangeAll() {
        this.allSelected = !this.allSelected;
        this.options.forEach(o => o.update = this.allSelected);
    }

    /**
     * the user should not be able to click the UPDATE button if none of the
     * dependencies are selected
     */
    updateIsDisabled() {
        return _.findIndex(this.options, o => o.update) === -1;
    }

    /**
     * Tell the parent to close
     */
    cancelClicked() {
        this._cub_updateAddressService.postToParent({
            action: 'cancel'
        });
    }

    /**
     * Pulls all dependency keys selected and calls update. If all dependencies
     * are selected, empty lists are sent instead (per NIS specification). The
     * result is relayed up to the parent HTML web resource within MSD.
     */
    updateClicked() {
        let contacts: string[] = [],
            fundingSources: string[] = [];

        if (!this.allSelected) {
            this.options.forEach(o => {
                if (o.update) {
                    (o.type === 'contact') ? contacts.push(o.key) : fundingSources.push(o.key);
                }
            });
        }

        this._loadingScreen.add('Updating...');
        this._cub_updateAddressService.update(this.customerId, this.address, contacts, fundingSources);
    }
}