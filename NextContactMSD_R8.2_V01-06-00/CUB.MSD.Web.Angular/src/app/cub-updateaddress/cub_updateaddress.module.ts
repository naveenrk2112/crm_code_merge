﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { Cub_UpdateAddressComponent } from './cub_updateaddress.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            { path: '', component: Cub_UpdateAddressComponent }
        ])
    ],
    declarations: [Cub_UpdateAddressComponent],
    exports: [RouterModule]
})
export class Cub_UpdateAddressModule { }