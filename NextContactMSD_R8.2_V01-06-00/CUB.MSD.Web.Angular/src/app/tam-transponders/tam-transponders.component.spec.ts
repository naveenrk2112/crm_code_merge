import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';

import { Tam_TranspondersComponent } from './tam-transponders.component';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';

describe('Tam_TranspondersComponent', () => {
    let component: Tam_TranspondersComponent;
    let fixture: ComponentFixture<Tam_TranspondersComponent>;

  beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
            DataTableModule,
            HttpClientTestingModule
        ],
        declarations: [
            Tam_TranspondersComponent
        ],
        providers: [
            { provide: Cub_DataService, useClass: Cub_DataServiceStub },
            { provide: ActivatedRoute, useClass: ActivatedRouteStub },
            { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
        ]
      }).compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Tam_TranspondersComponent);
      component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
