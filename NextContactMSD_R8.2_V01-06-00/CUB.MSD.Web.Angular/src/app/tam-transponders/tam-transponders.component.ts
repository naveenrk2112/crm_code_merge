import { Component, Input, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { Tam_TranspondersService, Transponder, TransponderList } from '../services/tam_transponders.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { DataTableModule, SharedModule, LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'tam-transponders',
  templateUrl: './tam-transponders.component.html',
  providers: [Tam_TranspondersService]
})
export class Tam_TranspondersComponent implements OnInit {

    TranspondersArray: any[];
    transponderDetail: Transponder[];
    /* Filter values */
    customerIdFilter: string;
    rowsPerPage: number = 10;

    @Input() customerId: string;
    showAddTransponders: boolean = false;

    constructor(
        private _tam_transpondersservice: Tam_TranspondersService,
        private _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
        const done = this._loadingScreen.add("Loading Transponders...");
        let queryParams = this._route.snapshot.queryParams;
        this.customerId = queryParams['customerId'];
        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.rowsPerPage = globals['CUB.MSD.Web.Angular']['TAM.Transponders.NumRows'] as number;
            });

        this._tam_transpondersservice.getTransponders(this.customerId)
            .then((data: any) => {
                if (data && data.transponders) {
                    this.transponderDetail = data.transponders;
                }
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error loading transponders'))
            .then(() => done());
    }

    cancelAddTransponder(event) {
        this.showAddTransponders = false;
        if (event === false) {
            const done = this._loadingScreen.add();
            this._tam_transpondersservice.getTransponders(this.customerId)
                .then((data: any) => {
                    if (data && data.transponders) {
                        this.TranspondersArray = data.vehicles;
                    }
                })
                .catch(err => this._cub_dataService.prependCurrentError('Error loading transponders'))
                .then(() => done());
        }
    }

    addTransponderClicked() {
        this.showAddTransponders = true;
    }


}
