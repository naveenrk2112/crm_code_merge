import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubTaAdjustValuesComponent } from './cub-ta-adjust-values.component';

describe('CubTaAdjustValuesComponent', () => {
  let component: CubTaAdjustValuesComponent;
  let fixture: ComponentFixture<CubTaAdjustValuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubTaAdjustValuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubTaAdjustValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
