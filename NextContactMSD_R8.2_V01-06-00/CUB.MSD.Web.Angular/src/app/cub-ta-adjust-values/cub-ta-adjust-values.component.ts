import { Component, OnInit, Input, Output, EventEmitter, HostListener, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as uuid from 'uuid/v4';
import { Observable } from 'rxjs/Observable';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import emailMask from 'text-mask-addons/dist/emailMask';

@Component({
    selector: 'cub-ta-adjust-values',
    templateUrl: './cub-ta-adjust-values.component.html',
    styleUrls: ['./cub-ta-adjust-values.component.css'],
    providers: [Cub_SubsystemService]
})
export class CubTaAdjustValuesComponent implements OnInit {

    numberMask = createNumberMask({
        prefix: '',
        suffix: '',
        allowDecimal: true,
        decimalLimit: 2,
        allowNegative: true
    });

    transitAccountId: string = null;
    isFormValid: boolean = false;
    subsystemId: string = null;
    customerId: string = null;
    contactId: string = null;
    initialPurseRestriction: string = null;
    initialPurseType: string = null;
    inputParameters: any = null;
    contactEmail: string = null;
    contactName: string = null;
    unregisteredEmail: string = null;
    notes: string = "";
    purses: any;
    filteredPurses: any = null;
    reasonCodeTypes: any;
    reasonCodeTypeSelected: any = null;
    reasonCodeTypeId: string = null; 
    reasonCodeSelected: any = null;
    operatorSelected: any = null;
    clientRefId: string = null;
    showFinalMessage: boolean = false;
    orderNumber: string = null;
    responseCode: string = null;
    errors: any = null;

    showAllPurses: boolean = true;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cub_SubsystemService: Cub_SubsystemService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        // Loading Globals
        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.reasonCodeTypeId = globals['CUB.MSD.Web.Angular']['ReasonCodeTypeIdForAdjustValue'] as string;
                this.getReasonCodeTypes();
            });
        this.clientRefId = this.generateClientRefId();
        const queryParams = this._route.snapshot.queryParamMap;
        if (!queryParams.has('purses')
            || !queryParams.has('requestBody')
        ) {
            console.error('purses and requestBody paramaters are required');
            return;
        }
        this.purses = JSON.parse(queryParams.get('purses'));
        this.inputParameters = JSON.parse(queryParams.get('requestBody'));
        this.transitAccountId = this.inputParameters.transitAccountId;
        this.subsystemId = this.inputParameters.subsystemId;
        this.customerId = this.inputParameters.customerId;
        this.contactId = this.inputParameters.contactId;
        this.initialPurseRestriction = this.inputParameters.purseRestriction;
        this.initialPurseType = this.inputParameters.purseType;
        // Filtering by the initial Purse
        this.filteredPurses = this.purses.filter(p => p.purseRestriction == this.initialPurseRestriction && p.purseType == this.initialPurseType);
        this.getContactInfo();
    }

    getReasonCodeTypes() {
        const done = this._loadingScreen.add('Getting Reason Codes...');
        this._cub_SubsystemService.getReasonCodes()
            .then((data) => {
                if (data) {
                    var body = JSON.parse(data.Body);
                    this.reasonCodeTypes = body.reasonCodeTypes;
                    this.reasonCodeTypeSelected = this.reasonCodeTypes.find(rct => rct.typeId == this.reasonCodeTypeId);
                    this.onReasonCodeChanged();
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting reason Codes');
            })
            .then(() => done());
    } 

    getContactInfo() {
        if (this.contactId) {
            const done = this._loadingScreen.add('Getting Contact information...');
            this._cub_SubsystemService.GetContactNameEmailById(this.contactId)
                .then((data) => {
                    var contact = JSON.parse(data);
                    this.contactName = `${contact.FirstName} ${contact.LastName}`;
                    this.contactEmail = contact.Email;
                })
                .catch((error: any) => {
                    this._cub_dataService.prependCurrentError('Error getting Contact info');
                })
                .then(() => done());
        }
    }

    onAdjustValueButtonClicked() {
        const done = this._loadingScreen.add('Procesing Adjustment Values...');
        let request: any = {};
        request.clientRefId = this.clientRefId;
        if (this.customerId) {
            request.customerId = this.customerId;
        }
        if (this.unregisteredEmail) {
            request.unregisteredEmail = this.unregisteredEmail;
        }
        request.reasonCode = this.reasonCodeSelected.reasonCodeId;
        request.financiallyResponsibleOperatorId = this.operatorSelected.financiallyResponsibleOperatorId;
        request.notes = this.notes;
        //request.isApproved = true; //@todo: Not implemented yet, part of phase two
        request.productLineItems = [];
        for (let p of this.purses) {
            if (p.adjustmentValue && p.adjustmentValue != 0) {
                let product: any = {
                    product: {
                        adjustmentValue: this.getValueForNIS(p.adjustmentValue),
                        productLineItemType: "AdjustSubsystemAccountValue", // @todo: the only type working so far
                        subsystem: this.subsystemId,
                        subsystemAccountReference: this.transitAccountId,
                        purseType: p.purseType,
                        purseRestriction: p.purseRestriction
                    }
                };
                request.productLineItems.push(product);
            }
        }
        this._cub_SubsystemService.postOrderAdjustment(request)
            .then((response: any) => {
                var body = JSON.parse(response.Body);
                this.orderNumber = body.orderId;
                this.errors = body.errors;
                this.responseCode = body.responseCode;
                // Reset the Client Ref Id if the call is completed without errors
                this.clientRefId = this.generateClientRefId();
                this.showFinalMessage = true;
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error posting the adjustment order');
                this.validateForm();
            })
            .then(() => done());
    }

    getValueForNIS(value) {
        var nisValue = parseFloat(value.replace(',', '')) * 100;
        var newValue = _.padStart(nisValue.toString(), 4, '0');
        return newValue;
    }

    generateClientRefId() {
        return `MSD:${uuid()}`;
    }

    closeFinalMessage() {
        window['Mscrm'].Utilities.setReturnValue({
            refresh: true
        });
        window['closeWindow']();
    }

    validateForm() {
        // Reason Code Type, Reason Code and Operator are required
        var reasonCodeAndOperadorValid =
            this.reasonCodeSelected != null &&
            this.reasonCodeSelected.reasonCodeId != null &&
            this.operatorSelected != null &&
            this.operatorSelected.financiallyResponsibleOperatorId != null;
        /*
         * Checking if at least one adjustment value is provided and
         * if each value is inside the limit for the selected reason code
         */
        var atLeastOneAdjustmentValue = false;
        var allValuesInInterval = true;
        if (this.reasonCodeSelected) {
            let min: number = this.reasonCodeSelected.minAmount;
            let max: number = this.reasonCodeSelected.maxAmount;
            for (let p of this.filteredPurses) {
                if (p.adjustmentValue && parseInt(this.getValueForNIS(p.adjustmentValue)) != 0) {
                    atLeastOneAdjustmentValue = true;
                    // Only check if the Min/Max are provided. They are optional
                    if (min != null && max != null) {
                        var valueForNIS = parseInt(this.getValueForNIS(p.adjustmentValue));
                        p.adjustmentValueOutOfInterval = valueForNIS < min || valueForNIS > max;
                        allValuesInInterval = !p.adjustmentValueOutOfInterval;
                    }
                }
            }
        }
        /*
         * Checking only if the Reason Code notesMandatoryFlag is true.
         */
        let notesValidation: boolean =
            (this.reasonCodeSelected && !this.reasonCodeSelected.notesMandatoryFlag) ||
            (this.notes != null && this.notes.length != 0);
        // Combining all validations
        this.isFormValid = reasonCodeAndOperadorValid && atLeastOneAdjustmentValue && allValuesInInterval && notesValidation;
    }

    onReasonCodeChanged() {
        /*
         * Setting the Operator if there is only one.
         */
        if (this.reasonCodeSelected.financiallyResponsibleOperators &&
            this.reasonCodeSelected.financiallyResponsibleOperators.length == 1) {
            this.operatorSelected = this.reasonCodeSelected.financiallyResponsibleOperators[0];
        } else {
            this.operatorSelected = null;
        }
        this.validateForm();
    }

    onShowAllPursesClicked() {
        if (this.showAllPurses) {
            this.filteredPurses = this.purses;
        } else {
            this.filteredPurses = this.purses.filter(p => this.showAllPurses || p.adjustmentValue);
        }
        this.showAllPurses = !this.showAllPurses;
    }
}
