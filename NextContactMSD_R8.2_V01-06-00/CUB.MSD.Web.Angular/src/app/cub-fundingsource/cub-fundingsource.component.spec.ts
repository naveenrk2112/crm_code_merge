import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cub_FundingsourceComponent } from './cub-fundingsource.component';
import { Cub_MenuToggleDirective } from '../cub-menu-toggle/cub-menu-toggle.directive';

describe('Cub_FundingsourceComponent', () => {
  let component: Cub_FundingsourceComponent;
  let fixture: ComponentFixture<Cub_FundingsourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
            Cub_FundingsourceComponent,
            Cub_MenuToggleDirective
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Cub_FundingsourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    //TODO: Test is not completed, please complete test after dev of component and remove pending() call
    pending();
    expect(component).toBeTruthy();
  });
});
