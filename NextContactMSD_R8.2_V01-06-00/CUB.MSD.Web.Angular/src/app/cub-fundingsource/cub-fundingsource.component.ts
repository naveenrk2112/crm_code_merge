import { Component, Input, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_FundingSourceService, FundingSourceExt, FundingSourceList } from '../services/cub_fundingsource.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { DataTableModule, SharedModule, LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as Model from '../model';

@Component({
  selector: 'cub-fundingsource',
  templateUrl: './cub-fundingsource.component.html',
  providers: [Cub_FundingSourceService, Cub_OneAccountService]
})
export class Cub_FundingsourceComponent implements OnInit {

    fundingSourceArray: any[];
    /* Filter values */
    accountIdFilter: string;
    rowsPerPage: number = 10;
    
    @Input() accountId: string;
    fundingSourceDetail: FundingSourceExt[];
    showDeletePage: boolean = false;
    visaNum: any;
    fundingSourceId: any;
    testKey: any;
    billingAddressKey: any;
    showSaveModal: boolean = false;
    showAddFundingSource: boolean = false;
    showEditFundingSource: boolean = false;
    isNewAddress: boolean = false;
    zipcodeId: string;
    countryId: string;
    isPrimary: boolean;
    fsGridControl: Model.Cub_CustomizableGrid;
    //awaitingGridConfig: boolean = true;

    constructor(
        private _cub_fundingSourceService: Cub_FundingSourceService,
        private _cub_dataService: Cub_DataService,
        private _cub_oneaccountService: Cub_OneAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.accountId = parent.Xrm.Page.data.entity.getId();

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.rowsPerPage = globals['CUB.MSD.Web.Angular']['FundingSources.NumRows'] as number;
                this.getFundingSources();
            });
    }

    getFundingSources() {
        const done = this._loadingScreen.add("Loading Funding Sources..."); 
        this._cub_fundingSourceService.getFundingSources(this.accountId)
            .then((data: any) => {
                if (data && data.fundingSources) {
                    this.fundingSourceDetail = data.fundingSources;
                }
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error getting funding sources'))
            .then(() => done());
    }

    editFundingSourceClicked(record: any) {
      //  this.accountId = record.accountId;
        this.fundingSourceId = record.fundingSourceId; 
        this.showEditFundingSource = true;
    }

    cancelEditFundingSource() {
        this.showEditFundingSource = false;
    }

    cancelAddFundingSource(event) {
        this.showAddFundingSource = false;
        if (event === false) {
            this.getFundingSources();
        }
    }

    addFundingSourceClicked() {
        this.showAddFundingSource = true;
    }

    setAsPrimaryClicked(record: any) {
        const done = this._loadingScreen.add('Updating Funding Source...'); 
        this.fundingSourceId = record.fundingSourceId;
        this.billingAddressKey = record.billingAddressId;

        this._cub_fundingSourceService.setPrimaryFundingSource(this.accountId, this.fundingSourceId, this.billingAddressKey)
            .then(() => this.getFundingSources())
            .catch(err => this._cub_dataService.prependCurrentError('Error setting primary funding source'))
            .then(() => done());
    }

    viewAutoloadsClicked(event: any) {

    }

    deleteFundingSourceClicked(record: any) {
        this.visaNum = record.creditCard.maskedPan;  
        this.fundingSourceId = record.fundingSourceId;
        this.billingAddressKey = record.billingAddressId;
        this.showDeletePage = true;
    }

    deleteFundingSourceConfirmed() {
        this.showDeletePage = false;
        const done = this._loadingScreen.add('Deleting Funding Source...');
        this._cub_fundingSourceService.deleteFundingSource(this.accountId, this.fundingSourceId)
            .then(() => this._cub_fundingSourceService.getFundingSources(this.accountId)) 
            .then((data: any) => {
                this.showSaveModal = true; 
                //hide save dialog after 2.5 seconds.
                setTimeout(() => { this.showSaveModal = false; }, 2500);
                if (data && data.fundingSources) {
                    this.fundingSourceDetail = data.fundingSources;
                }
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    cancelDelete() {
        this.showDeletePage = false;
    }
}


