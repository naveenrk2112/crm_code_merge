﻿import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_NotificationService } from '../services/cub_notification.service'
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Category, SubCategory, NotificationPreference, Channel, Contact } from '../model';
import * as Model from '../model';

import * as _ from 'lodash';

function saveNotificationPreferences(context: Xrm.Page.SaveEventContext) {
    (context.getEventArgs() as Xrm.Page.SaveEventArguments).preventDefault();
    window['cubicNotificationPreferences'].zone.run(() => {
        window['cubicNotificationPreferences'].component.handleExternalSave();
    });
}

@Component({
    selector: 'cub-notificationpreferences',
    templateUrl: './cub_notificationpreferences.component.html',
    providers: [Cub_NotificationService]
})
export class Cub_NotificationPreferencesComponent implements OnInit, OnDestroy {
    customerId: string = '';
    contactId: string = '';
    channel: string = '';
    allContactsSelected: boolean = false;
    selectAllButtonText: string = "Select All";
    showSaveModal: boolean = false;
    showApplyPreferencesToContacts: boolean = false;
    searchButton: Model.Cub_Button = {
        id: '',
        label: '',
        class: '',
        isDisabled: false
    };
    applyButton: Model.Cub_Button = {
        id: "applyButton",
        label: "Apply",
        class: "button-primary",
        isDisabled: true
    };
    cancelButton: Model.Cub_Button = {
        id: "cancelButton",
        label: "Cancel",
        class: "button-primary",
        isDisabled: false
    };
    phoneForSMSRadioList: Object = {
        controls: [
            {
                id: 'phone-for-sms-push-1id',
                name: 'phone-for-sms-push-1',
                label: '312-555-1234',
                radioClass: 'form-field-hybrid-radio',
            },{
                id: 'phone-for-sms-push-2id',
                name: 'phone-for-sms-push-2',
                label: '630-555-9753',
                radioClass: 'form-field-hybrid-radio',
            }
        ],
        value: '',
        group: 'phone-for-sms-push',
        label: 'Phone for SMS'
    };
    notificationmethodCheckboxList: {
        controls: Object,
        controlsOrder: Array<string>,
        label: string
    } = {
        controls:
        {
            'Email': {
                name: 'Email',
                isDisabled: true,
                value: true,
                label: 'Email',
                checkboxClass: 'icon-checkbox-hybrid'
            } as Model.Cub_Checkbox ,
            'SMS': {
                name: 'SMS',
                value: true,
                label: 'SMS',
                checkboxClass: 'icon-checkbox-hybrid',
                isDisabled: false
            } as Model.Cub_Checkbox,
            'Push': {
                name: 'Push',
                value: true,
                label: 'Push',
                checkboxClass: 'icon-checkbox-hybrid',
                isDisabled: false
            } as Model.Cub_Checkbox
        },
        controlsOrder: ['Email', 'SMS', 'Push'],
        label: 'Notification Methods'
    };
    
    //Extracted from notification preferences data
    contactIdData: string = '';
    firstName: string = '';
    lastName: string = '';
    email: string = '';
    smsPhone: string = '';
    mandatory: boolean = false;
    rawDataFromWebservice: Object = {};
    preferencesData: Object[] = [];
    notificationPreferencesData: Array<Category> = [];
    expandAllEnabled: boolean = true;
    collapseAllEnabled: boolean = true;
    contactsRelatedToCustomer: Contact[];
    constructor(
        private _cub_notificationService: Cub_NotificationService,        
        public _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private router: Router,
        private _zone: NgZone
    ) { 
        window['cubicNotificationPreferences'] = {
            component: this,
            zone: this._zone
        };
    }

    /**
     * Function handler for the change event of the radiolist control.
     * @param event data about the event that is firing
     */
    onPhoneForSMSRadioListChanged(event: any): void {
        //console.log(event);
    }

    /**
     * Function handler for the category checkbox
     * @param checkbox checkbox where the value is changing
     * @param category category object associated with the checkbox
     */
    onCategoryCheckboxChanged(checkbox: Model.Cub_Checkbox, category): void {
        const subcategoriesLength = category.subcategories.length;
        for (let i = 0; i < subcategoriesLength; i++) {
            let subcategory = category.subcategories[i];
            let subcategoryCheckboxControl = subcategory[checkbox.name + 'Checkbox'];
            if (subcategoryCheckboxControl && !subcategoryCheckboxControl.isDisabled) {
                subcategoryCheckboxControl.value = checkbox.value;
                subcategoryCheckboxControl.partial = false;
            }

            const notificationsLength = subcategory.notifications.length;
            for (let j = 0; j < notificationsLength; j++) {
                let notification = subcategory.notifications[j];
                let checkboxControl = notification[checkbox.name + 'Checkbox'] as Model.Cub_Checkbox;
                if (!checkboxControl || checkboxControl.isDisabled) {
                    continue;
                }
                checkboxControl.value = checkbox.value;
                let channel = _.find(notification.channels, (c: any) => checkbox.name.startsWith(c.channel.toLowerCase()));
                channel.enabled = checkbox.value;
            }
        }
    }

    /**
     * Function handler for the subcategory checkbox
     * @param checkbox checkbox where the value is changing
     * @param category category object associated with the checkbox
     * @param subcategory subcategory object associated with the checkbox
     */
    onSubcategoryCheckboxChanged(checkbox: Model.Cub_Checkbox, category: Category, subcategory: SubCategory): void {
        let channelName: string = checkbox.name;
        const len = subcategory.notifications.length;
        for (let i = 0; i < len; i++) {
            let notification = subcategory.notifications[i];
            let checkboxControl = notification[checkbox.name + 'Checkbox'] as Model.Cub_Checkbox;
            if (!checkboxControl || checkboxControl.isDisabled) {
                continue;
            }
            checkboxControl.value = checkbox.value;
            let channel = _.find(notification.channels, (c: any) => channelName.startsWith(c.channel.toLowerCase()));
            channel.enabled = checkbox.value;
        }
        let allSubcategoriesChecked = this.areAllSubcategoriesChecked(category, channelName);
        let allSubcategoriesUnchecked = this.areAllSubcategoriesUnchecked(category, channelName);
        let isPartialCategoryCheck: boolean = !allSubcategoriesChecked && !allSubcategoriesUnchecked;
        this.setCategoryCheck(category, channelName, allSubcategoriesChecked, isPartialCategoryCheck);
    }

    /**
     * Function handler for the notification checkbox
     * @param checkbox checkbox where the value is changing
     * @param category category object associated with the checkbox
     * @param subcategory subcategory object associated with the checkbox
     * @param notification subcategory object associated with the checkbox
     */
    onNotificationCheckboxChanged(checkbox: Model.Cub_Checkbox, category: Category, subcategory: SubCategory, notification: NotificationPreference) {
        notification[checkbox.name + 'Checkbox'].value = checkbox.value;
        let channel: Channel = _.find(notification.channels, (c: any) => checkbox.name.startsWith(c.channel.toLowerCase()));
        let channelName = channel.channel.toLowerCase();
        let allNotificationsChecked = this.areAllNotificationsChecked(subcategory, channelName);
        let allNotificationsUnchecked = this.areAllNotificationsUnchecked(subcategory, channelName);
        let isPartialSubcategoryCheck: boolean = !allNotificationsChecked && !allNotificationsUnchecked;
        this.setSubcategoryCheck(subcategory, channelName, allNotificationsChecked, isPartialSubcategoryCheck);
        let allSubcategoriesChecked = this.areAllSubcategoriesChecked(category, channelName);
        let allSubcategoriesUnchecked = this.areAllSubcategoriesUnchecked(category, channelName);
        let isPartialCategoryCheck: boolean = !allSubcategoriesChecked && !allSubcategoriesUnchecked;
        this.setCategoryCheck(category, channelName, allSubcategoriesChecked, isPartialCategoryCheck);
    }

    /**
     * This function sorts the array of notification preferences by category then subcategory then description
     * so the notifications are sorted the way we want to display on the page
     * @param notificationPreferences array to sort
     */
    sortPreferences(notificationPreferences: Array<any>):Array<any> {
        let sortedPreferences = _.sortBy(
            notificationPreferences, [
                (i) => { return (i.category && i.category.description ? i.category.description.toLowerCase() : '') || '' },
                (i) => { return (i.subcategory && i.subcategory.description ? i.subcategory.description.toLowerCase() : '') || '' },
                (i) => { return i.notificationDescription ? i.notificationDescription.toLowerCase() : '' }
            ]);
        return sortedPreferences;
    }

    /**
     * This function is used to populate the missing channels in the data coming back from the webservice 
     * to allow checkboxes to be used and bound
     * @param notification
     */
    populateMissingChannelsOnNotification(notification: NotificationPreference): void {        
        let foundEmail = !!notification.channels.find(channel => channel.channel.toLowerCase() === 'email');
        let foundSMS = !!notification.channels.find(channel => channel.channel.toLowerCase() === 'sms');
        let foundPush = !!notification.channels.find(channel => channel.channel.toLowerCase() === 'pushnotification');
        if (!foundEmail) {
            notification.channels.push({
                channel: "EMAIL",
                enabled: false,
                reqType: "OPTIONAL"
            });
        }
        if (!foundPush) {
            notification.channels.push({
                channel: "PUSHNOTIFICATION",
                enabled: false,
                reqType: "OPTIONAL"
            });
        }
        if (!foundSMS) {
            notification.channels.push({
                channel: "SMS",
                enabled: false,
                reqType: "OPTIONAL"
            });
        }
    }

    /**
     * This function sets up the Category Checkboxes with default values.
     * @param category category on which to add the default checkboxes
     */
    setupCategoryCheckboxes(category: Category): void {
        let categoryEmailCheckboxControl: Model.Cub_Checkbox = {
            checkboxClass: 'fancy-checkbox',
            isDisabled: false,
            id: category.categoryName + '-emailCheckbox',
            name: 'email',
            label: '',
            partial: true,
            value: true
        };

        let categorySmsCheckboxControl: Model.Cub_Checkbox = {
            checkboxClass: 'fancy-checkbox',
            isDisabled: false,
            id: category.categoryName + '-smsCheckbox',
            name: 'sms',
            label: '',
            partial: true,
            value: true
        };

        let categoryPushCheckboxControl: Model.Cub_Checkbox = {
            checkboxClass: 'fancy-checkbox',
            isDisabled: false,
            id: category.categoryName + '-pushCheckbox',
            name: 'pushnotification',
            label: '',
            partial: true,
            value: true
        };
        category.emailCheckbox = categoryEmailCheckboxControl;
        category.smsCheckbox = categorySmsCheckboxControl;
        category.pushnotificationCheckbox = categoryPushCheckboxControl;
    }
    
    /**
     * This function sets up the subcategory Checkboxes with default values.
     * @param subCategory subcategory on which to add the default checkboxes
     */
    setupSubcategoryCheckboxes(subCategory: SubCategory): void {
        let subcategoryEmailCheckboxControl: any = {
            checkboxClass: 'fancy-checkbox',
            isDisabled: false,
            id: subCategory.subcategoryName + '-emailCheckbox',
            name: 'email',
            label: '',
            partial: true,
            value: true
        };

        let subcategorySmsCheckboxControl: any = {
            checkboxClass: 'fancy-checkbox',
            isDisabled: false,
            id: subCategory.subcategoryName + '-smsCheckbox',
            name: 'sms',
            label: '',
            partial: true,
            value: true
        };

        let subcategoryPushCheckboxControl: any = {
            checkboxClass: 'fancy-checkbox',
            isDisabled: false,
            id: subCategory.subcategoryName + '-pushCheckbox',
            name: 'pushnotification',
            label: '',
            partial: true,
            value: true
        };
        subCategory.emailCheckbox = subcategoryEmailCheckboxControl;
        subCategory.smsCheckbox = subcategorySmsCheckboxControl;
        subCategory.pushnotificationCheckbox = subcategoryPushCheckboxControl;
    }

    /**
     * This function takes a notification and a channel and creates checkboxes on the notification for that channel
     * @param notification notification on which to create the checkbox
     * @param channel determines the type of checkbox to add to the notification
     */
    createCheckboxControlFromNotificationAndChannel(notification: NotificationPreference, channel: Channel): any {
        let checkboxSuffix: string = '';
        switch (channel.channel.toLowerCase()) {
            case 'email':
                checkboxSuffix = '-emailCheckbox';
                break;
            case 'sms':
                checkboxSuffix = '-smsCheckbox';
                break;
            case 'pushnotification':
                checkboxSuffix = '-pushCheckbox';
                break;
            default:
                return {};
        }
        let reqType = channel.reqType.toLowerCase();
        let checkboxControl: Model.Cub_Checkbox = {
            checkboxClass: 'fancy-checkbox',
            isDisabled:
            (reqType === "disabled") ||
            (reqType === "default") ||
            (reqType === "mandatory") ||
            (reqType === "not allowed"),
            id: [notification.notificationType, checkboxSuffix].join(''),
            name: channel.channel.toLowerCase(),
            label: '',
            value: channel.enabled,
            partial: false
        };
        return checkboxControl;
    }

    /**
     * This function takes a notification and creates checkboxes for each of the channels
     * @param notification notification on which to setup checkboxes
     */
    setupChannelCheckboxes(notification: NotificationPreference): void {
        let emailChannel: Channel =
            notification.channels.find(channel => channel.channel.toLowerCase() === 'email');    
        if (emailChannel) {
            let emailCheckboxControl = this.createCheckboxControlFromNotificationAndChannel(notification, emailChannel) as Model.Cub_Checkbox;
            notification.emailCheckbox = emailCheckboxControl;
        }
        let smsChannel: Channel =
            notification.channels.find(channel => channel.channel.toLowerCase() === 'sms');  
        if (smsChannel) {
            let smsCheckboxControl = this.createCheckboxControlFromNotificationAndChannel(notification, smsChannel) as Model.Cub_Checkbox;
            notification.smsCheckbox = smsCheckboxControl;
        }
        let pushChannel: Channel =
            notification.channels.find(channel => channel.channel.toLowerCase() === 'pushnotification');
        if (pushChannel) {
            let pushCheckboxControl =
                this.createCheckboxControlFromNotificationAndChannel(notification, pushChannel) as Model.Cub_Checkbox;
            notification.pushnotificationCheckbox = pushCheckboxControl;
        }
    }

    /**
     * This will setup the way the checkbox is supposed to look depending on
     *  * If all children are checked or unchecked,
     *  * If children are neither all checked nor all unchecked, it will make it a partial check box
     *  * It will disable the checkbox if the isDiabled = true
     *  * If isNotification it will stop the ability of the control to be a partial check
     * @param checkbox Checkbox on which to act
     * @param allChecked Are all children of this checkbox checked?
     * @param allUnchecked Are all children of this checkbox unchecked?
     * @param isDisabled Should the control be disabled?
     * @param isNotification Is this control a notification control (Category and or subcategory null?) Notifications can't be partial checkboxes
     */
    setCheckboxCheckedStyle(
        checkbox: Model.Cub_Checkbox,
        allChecked: boolean,
        allUnchecked: boolean,
        isDisabled: boolean,
        isNotification: boolean = false): void {
        if (!checkbox)
        {
            return;
        }
        if (!isNotification &&
            !allChecked &&
            !allUnchecked) {
            checkbox.partial = true;
            checkbox.value = false;
        }
        else if (allChecked) {
            checkbox.partial = false;
            checkbox.value = true;
        }
        else if (allUnchecked) {
            checkbox.partial = false;
            checkbox.value = false;
        }
        checkbox.isDisabled = isDisabled;
    }

    /**
     * This function sets up the category for use in the prepare data method.
     * @param categories Array of categories already processed
     * @param categoryData data to turn into a category if it doesn't already exist
     */
    prepareCategory(categories: Array<any>, categoryData: any): Category {
        let cat = categoryData['category'];
        let catName: string = cat && cat['name'] ? cat['name'].toString() : 'zzzz';

        let foundCategory = categories.find(categ => categ.categoryName === catName);
        //If the category is found and not null, return it and exit the function
        if (!!foundCategory) {        
            return foundCategory;
        };

        //otherwise setup the category object.
        let catDescription: string = cat && cat['description'] ? cat['description'].toString() : '';
        let category: Category = {
            categoryName: catName,
            categoryDescription: catDescription,
            isExpanded: true,
            isNotification: false,
            subcategories: []
        }
        this.setupCategoryCheckboxes(category);
        return category;
    }

    /**
     * This function sets up the category for use in the prepare data method.
     * @param category Category associated with the subcategory
     * @param subcategoryData Data to extract into a subcategory
     */
    prepareSubcategory(category: Category, subcategoryData: any): SubCategory {
        let subcat = subcategoryData['subCategory'];
        let subcatName = subcat && subcat['name'] ? subcat['name'] : 'zzzz';

        //If the subcategory is found and not null, return it and exit the function.
        let foundSubCategory = category.subcategories.find(
            subcategory => subcategory.subcategoryName === subcatName);
        if (!!foundSubCategory) {
            return foundSubCategory;
        }

        //otherwise, setup the subcategory
        let subcatDescription = subcat && subcat['description'] ? subcat['description'] : '';
        let subcategoryIndex = 0;
        let subcategoryLength = category.subcategories.length;
        let subCategory: SubCategory = {
            subcategoryName: subcatName,
            subcategoryDescription: subcatDescription,
            isNotification: false,
            isExpanded: true,
            notifications: []
        };
        this.setupSubcategoryCheckboxes(subCategory)
        return subCategory;
    }

    /**
     * Sets up the Notification object based on generic data passed in from the webservice
     * @param notificationData Notification Data on which to prepare
     */
    prepareNotification(notificationData: any): NotificationPreference {
        let notification: NotificationPreference = {
            notificationType: notificationData['notificationType'],
            notificationDescription: notificationData['notificationDescription'],
            optIn: notificationData['optIn'],
            channels: notificationData['channels']
        }
        //TODO: Currently we can't make this call.  We have to wait until after 
        //this.populateMissingChannelsOnNotification(notification);
        this.setupChannelCheckboxes(notification);
        return notification;
    }

    /**
     * This function takes a subcategory and adds all notifications that are related to it
     * @param subCategory Subcategory on which to act
     * @param notificationData Notification data on which to process and add to subcategory
     */
    processNotificationsOnSubcategory(subCategory:SubCategory, notificationData: any): NotificationPreference {
        let notification: NotificationPreference = this.prepareNotification(notificationData);

        subCategory.notifications.push(notification);
        subCategory.notifications = _.sortBy(
            subCategory.notifications,
            (item) => {
                return item.notificationDescription ? item.notificationDescription.toLowerCase() : '';
            })

        let allEmailNotificationsChecked = this.areAllNotificationsChecked(subCategory, 'email');
        let allEmailNotificationsUnchecked = this.areAllNotificationsUnchecked(subCategory, 'email');
        let allEmailNotificationsDisabled = this.areAllNotificationsDisabled(subCategory, 'email');
        let anyEmailNotificationCheckboxExist = this.areAnyNotificationCheckboxesExisting(subCategory, 'email');
        let allSmsNotificationsChecked = this.areAllNotificationsChecked(subCategory, 'sms');
        let allSmsNotificationsUnchecked = this.areAllNotificationsUnchecked(subCategory, 'sms');
        let allSmsNotificationsDisabled = this.areAllNotificationsDisabled(subCategory, 'sms');
        let anySmsNotificationCheckboxExist = this.areAnyNotificationCheckboxesExisting(subCategory, 'sms');
        let allPushNotificationsChecked = this.areAllNotificationsChecked(subCategory, 'pushnotification');
        let allPushNotificationsUnchecked = this.areAllNotificationsUnchecked(subCategory, 'pushnotification');
        let allPushNotificationsDisabled = this.areAllNotificationsDisabled(subCategory, 'pushnotification');
        let anyPushNotificationsCheckboxExist = this.areAnyNotificationCheckboxesExisting(subCategory, 'pushnotification');

        this.setCheckboxCheckedStyle(
            subCategory.emailCheckbox,
            allEmailNotificationsChecked,
            allEmailNotificationsUnchecked,
            allEmailNotificationsDisabled);
        this.setCheckboxCheckedStyle(
            subCategory.smsCheckbox,
            allSmsNotificationsChecked,
            allSmsNotificationsUnchecked,
            allSmsNotificationsDisabled);
        this.setCheckboxCheckedStyle(
            subCategory.pushnotificationCheckbox,
            allPushNotificationsChecked,
            allPushNotificationsUnchecked,
            allPushNotificationsDisabled);
        if (!anyEmailNotificationCheckboxExist) {
            subCategory.emailCheckbox = null;
        }
        if (!anySmsNotificationCheckboxExist) {
            subCategory.smsCheckbox = null;
        }
        if (!anyPushNotificationsCheckboxExist) {
            subCategory.pushnotificationCheckbox = null;
        }
        return notification;
    }

    /**
     * Function takes a category, subcategory and notification and it will process the category to setup the
     * checkboxes on it
     * @param category Category on which to act
     * @param subCategory Subcategory, if not found on category, it will push and resort the subcategories
     * @param notification Notification related to this iteration through which will be added,
     *  but if there is no category or subcategory above it, copy the checkboxes up to where the parent
     *  isn't blank OR make it the parent itself
     */
    processCategory(category: Category, subCategory: SubCategory, notification: NotificationPreference) {
        // If category and subcategory are blank, copy notification up to category and make it a notification.
        if (category.categoryName === 'zzzz' && subCategory.subcategoryName === 'zzzz') {
            category.categoryName = notification.notificationType;
            category.categoryDescription = notification.notificationDescription;
            category.emailCheckbox = notification.emailCheckbox;
            category.pushnotificationCheckbox = notification.pushnotificationCheckbox;
            category.smsCheckbox = notification.smsCheckbox;
            category.isNotification = true;
        }
        // If Category is null and the subcategory isn't, copy subcategory up to category
        else if (category.categoryName === 'zzzz' && subCategory.subcategoryName != 'zzzz') {
            category.categoryName = subCategory.subcategoryName;
            category.categoryDescription = subCategory.subcategoryDescription;
            category.emailCheckbox = subCategory.emailCheckbox;
            category.smsCheckbox = subCategory.smsCheckbox;
            category.pushnotificationCheckbox = subCategory.pushnotificationCheckbox;
        }
        // If category isn't null but the subcategory is, copy notification up to subcategory
        else if (category.categoryName !== 'zzzz' && subCategory.subcategoryName === 'zzzz') {
            subCategory.subcategoryName = notification.notificationType;
            subCategory.subcategoryDescription = notification.notificationDescription;
            subCategory.isNotification = true;
            subCategory.emailCheckbox = notification.emailCheckbox;
            subCategory.pushnotificationCheckbox = notification.pushnotificationCheckbox;
            subCategory.smsCheckbox = notification.smsCheckbox;
        }
        let foundSubCategory = category.subcategories.find(subcateg => subcateg.subcategoryName === subCategory.subcategoryName);
        // If subcategory isn't found, push it in and resort the subcategory array alphabetically
        if (!foundSubCategory) {
            category.subcategories.push(subCategory);
            category.subcategories = _.sortBy(
                category.subcategories,
                (item) => { return item.subcategoryDescription ? item.subcategoryDescription.toLowerCase() : '' })
        }
        let allEmailSubcategoriesChecked = this.areAllSubcategoriesChecked(category, 'email');
        let allEmailSubcategoriesUnchecked = this.areAllSubcategoriesUnchecked(category, 'email');
        let allEmailSubcategoriesDisabled = this.areAllSubcategoriesDisabled(category, 'email');
        let anyEmailSubcategoryCheckboxesExist = this.areAnySubcategoryCheckboxesExisting(category, 'email');
        let allSmsSubcategoriesUnchecked = this.areAllSubcategoriesUnchecked(category, 'sms');
        let allSmsSubcategoriesChecked = this.areAllSubcategoriesChecked(category, 'sms');
        let allSmsSubcategoriesDisabled = this.areAllSubcategoriesDisabled(category, 'sms');
        let anySmsSubcategoryCheckboxesExist = this.areAnySubcategoryCheckboxesExisting(category, 'sms');
        let allPushSubcategoriesChecked = this.areAllSubcategoriesChecked(category, 'pushnotification');
        let allPushSubcategoriesUnchecked = this.areAllSubcategoriesUnchecked(category, 'pushnotification');
        let allPushSubcategoriesDisabled = this.areAllSubcategoriesDisabled(category, 'pushnotification');
        let anyPushSubcategoryCheckboxesExist = this.areAnySubcategoryCheckboxesExisting(category, 'pushnotification');
        // set the checkbox style based on if all children are checked, all children are unchecked, 
        // if there is a mixed-check situation make it a partial or amke it disabled
        this.setCheckboxCheckedStyle(
            category.emailCheckbox,
            allEmailSubcategoriesChecked,
            allEmailSubcategoriesUnchecked,
            allEmailSubcategoriesDisabled,
            category.isNotification);
        this.setCheckboxCheckedStyle(
            category.smsCheckbox,
            allSmsSubcategoriesChecked,
            allSmsSubcategoriesUnchecked,
            allSmsSubcategoriesDisabled,
            category.isNotification);
        this.setCheckboxCheckedStyle(
            category.pushnotificationCheckbox,
            allPushSubcategoriesChecked,
            allPushSubcategoriesUnchecked,
            allPushSubcategoriesDisabled,
            category.isNotification);
        if (!anyEmailSubcategoryCheckboxesExist) {
            category.emailCheckbox = null;
        }
        if (!anySmsSubcategoryCheckboxesExist) {
            category.smsCheckbox = null;
        }
        if (!anyPushSubcategoryCheckboxesExist) {
            category.pushnotificationCheckbox = null;
        }
    }

    /**
     * Function used to change data from webservice data to a format we can more easily use.
     * @param notificationPreferences Data from webservices of the notification preferences
     */
    prepareNotificationPreferenceData(notificationPreferences: any) {
        let gridPreferenceData: Array<Category> = [];
        gridPreferenceData = _.reduce(
            notificationPreferences,
            (categories, val) => {
                let category: Category = this.prepareCategory(categories, val);
                let foundCategory = !!categories.find(categ => categ.categoryName === category.categoryName);
                let subCategory: SubCategory = this.prepareSubcategory(category, val);
                let notification = this.processNotificationsOnSubcategory(subCategory, val);
                this.processCategory(category, subCategory, notification);


                if (!foundCategory) {
                    categories.push(category);
                }

                return categories;
            }, []);
        gridPreferenceData.forEach(category => {
            if (category.categoryName === 'zzzz') {
                category.categoryName = '';
            }
            category.subcategories.forEach(subcategory => {
                if (subcategory.subcategoryName === 'zzzz') {
                    subcategory.subcategoryName = '';
                }
            });
        });
        gridPreferenceData = _.sortBy(gridPreferenceData, (i) => { return (i.categoryDescription) || '' });
        return gridPreferenceData;
    }

    repackNotificationPreferenceDataForWebserviceSave(notificationPreferences: Category[], contacts: Array<Contact>): Object {
        //take nice structure and repack it for use in webservice.
        //traverse from category -> Subcategory -> Notification and repack
        let repackedData: Object[] = [];
        let categoryData: Object = {};
        let subcategoryData: Object = {};
        notificationPreferences.forEach(category => {
            category.subcategories.forEach(subcategory => {
                subcategory.notifications.forEach(notification => {
                    if (!category.isNotification) {
                        categoryData = {
                            description: category.categoryDescription,
                            name: category.categoryName
                        };
                    }
                    if (!subcategory.isNotification) {
                        subcategoryData = {
                            description: subcategory.subcategoryDescription,
                            name: subcategory.subcategoryName
                        };
                    }
                    //if any of the channels are enabled, we need to set opt-in to true
                    let optIn: boolean = false;
                    notification.channels.forEach(channel => {
                        if (channel.enabled) {
                            optIn = true;
                        }
                    });
                    let notificationData = {
                        channels: notification.channels,
                        notificationType: notification.notificationType,
                        optIn: optIn //Changed after conversation with James, if a channel is checked, then we need to set opt-in to true.
                    };
                    repackedData.push(notificationData);
                });
            });
        });
        let contactsData: Object[] = [];
        contacts.forEach(contact => {
            contactsData.push({
                contactId: contact.contactId,
                preferences: repackedData
            });
        })
        return {
            customerId: this.customerId,
            contacts: contactsData
        };
    }


    /**
     * Set data on subcategory based on if the value is checked,
     * or if it is a partial check (children are mixed-check)
     * @param subcategory Subcategory on which the checkbox resides
     * @param channel Type of checkbox to change
     * @param checkedValue Value of the checkbox
     * @param isPartialCheck Should the control display as a partial checkbox?
     */
    setSubcategoryCheck(subcategory: SubCategory, channel: string, checkedValue: boolean, isPartialCheck: boolean) {
        switch (channel) {
            case 'email':
                subcategory.emailCheckbox.value = checkedValue;
                subcategory.emailCheckbox.partial = isPartialCheck;
                break;
            case 'pushnotification':
                subcategory.pushnotificationCheckbox.value = checkedValue;
                subcategory.pushnotificationCheckbox.partial = isPartialCheck;
                break;
            case 'sms':
                subcategory.smsCheckbox.value = checkedValue;
                subcategory.smsCheckbox.partial = isPartialCheck;
                break;
            default:
                break;
        }
    }

    /**
     * Set data on category based on if the value is checked,
     * or if it is a partial check (children are mixed-check)
     * @param category Category on which the checkbox resides
     * @param channel Type of checkbox to change
     * @param checkedValue Value of the checkbox
     * @param isPartialCheck Should the control display as a partial checkbox?
     */
    setCategoryCheck(category: Category, channel: string, checkedValue: boolean, isPartialCheck: boolean) {
        switch (channel) {
            case 'email':
                category.emailCheckbox.value = checkedValue;
                category.emailCheckbox.partial = isPartialCheck;
                break;
            case 'pushnotification':
                category.pushnotificationCheckbox.value = checkedValue;
                category.pushnotificationCheckbox.partial = isPartialCheck;
                break;
            case 'sms':
                category.smsCheckbox.value = checkedValue;
                category.smsCheckbox.partial = isPartialCheck;
                break;
            default:
                break;
        }
    }

    /**
     * Function used to determine if all children subcategories on a category are checked
     * @param category category on which to check the children subcategories
     * @param channelType type of checkbox to check
     */
    areAllSubcategoriesChecked(category: Category, channelType: string) {
        let subcategoryIndex = 0;
        let subcategoriesLength = category.subcategories.length;
        let allChecked = true;
        for (; subcategoryIndex < subcategoriesLength; subcategoryIndex++) {
            let subcategory = category.subcategories[subcategoryIndex];            
            if (subcategory[channelType + 'Checkbox'] &&
                !subcategory[channelType + 'Checkbox'].value) {
                allChecked = false;
                break;
            }
        }
        return allChecked;
    }

    /**
     * Function used to determine if all children subcategories on a category are disabled
     * @param category category on which to check the children subcategories
     * @param channelType type of checkbox to check
     */
    areAllSubcategoriesDisabled(category: Category, channelType: string) {
        let subcategoryIndex = 0;
        let subcategoriesLength = category.subcategories.length;
        let allDisabled = true;
        for (; subcategoryIndex < subcategoriesLength; subcategoryIndex++) {
            let subcategory = category.subcategories[subcategoryIndex];
            if (subcategory[channelType + 'Checkbox'] &&
                !subcategory[channelType + 'Checkbox'].isDisabled) {
                allDisabled = false;
                break;
            }
        }
        return allDisabled;
    }

    /**
     * Function used to determine if all children subcategories on a category are expanded
     * @param category category on which to check the children subcategories
     * @param channelType type of checkbox to check
     */
    areAllSubcategoriesExpanded(category: Category, channelType: string) {
        let subcategoryIndex = 0;
        let subcategoriesLength = category.subcategories.length;
        let allDisabled = true;
        for (; subcategoryIndex < subcategoriesLength; subcategoryIndex++) {
            let subcategory = category.subcategories[subcategoryIndex];
            if (!subcategory.isExpanded) {
                allDisabled = false;
                break;
            }
        }
        return allDisabled;
    }

    /**
     * Function used to determine if all children subcategories on a category are unchecked
     * @param category category on which to check the children subcategories
     * @param channelType type of checkbox to check
     */
    areAllSubcategoriesUnchecked(category: Category, channelType: string) {
        let subcategoryIndex = 0;
        let subcategoriesLength = category.subcategories.length;
        let allUnchecked = true;
        for (; subcategoryIndex < subcategoriesLength; subcategoryIndex++) {
            let subcategory = category.subcategories[subcategoryIndex];
            if (subcategory[channelType + 'Checkbox'] &&
                (subcategory[channelType + 'Checkbox'].value ||
                subcategory[channelType + 'Checkbox'].partial)) {
                allUnchecked = false;
                break;
            }
        }
        return allUnchecked;
    }

    /**
     * Function used to determine if all children notifications on a subcategory are disabled
     * @param subcategory subcategory on which to check the children notifications
     * @param channelType type of checkbox to check
     */
    areAnySubcategoryCheckboxesExisting(category: Category, channelType: string): boolean {
        let subcategoryIndex = 0;
        let subcategoriesLength = category.subcategories.length;
        let anyCheckboxesExist = false;
        for (; subcategoryIndex < subcategoriesLength; subcategoryIndex++) {
            let subcategory = category.subcategories[subcategoryIndex];
            if (subcategory[channelType + 'Checkbox']) {
                anyCheckboxesExist = true;
                break;
            }
        }
        return anyCheckboxesExist;
    }

    /**
     * Function used to determine if all children notifications on a subcategory are checked
     * @param subcategory subcategory on which to check the children notifications
     * @param channelType type of checkbox to check
     */
    areAllNotificationsChecked(subcategory: SubCategory, channelType: string) {
        let notificationIndex = 0;
        let notificationsLength = subcategory.notifications.length;
        let allChecked = true;
        for (; notificationIndex < notificationsLength; notificationIndex++) {
            let notification = subcategory.notifications[notificationIndex];
            if (notification[channelType + 'Checkbox'] &&
                !notification[channelType + 'Checkbox'].value) {
                allChecked = false;
                break;
            }
        }
        return allChecked;
    }

    /**
     * Function used to determine if all children notifications on a subcategory are disabled
     * @param subcategory subcategory on which to check the children notifications
     * @param channelType type of checkbox to check
     */
    areAllNotificationsDisabled(subcategory: SubCategory, channelType: string) {
        let notificationIndex = 0;
        let notificationsLength = subcategory.notifications.length;
        let allDisabled = true;
        for (; notificationIndex < notificationsLength; notificationIndex++) {
            let notification = subcategory.notifications[notificationIndex];
            if (notification[channelType + 'Checkbox'] &&
                !notification[channelType + 'Checkbox'].isDisabled) {
                allDisabled = false;
                break;
            }
        }
        return allDisabled;
    }

    /**
     * Function used to determine if all children notifications on a subcategory are unchecked
     * @param subcategory subcategory on which to check the children notifications
     * @param channelType type of checkbox to check
     */
    areAllNotificationsUnchecked(subcategory: SubCategory, channelType: string) {
        let notificationIndex = 0;
        let notificationsLength = subcategory.notifications.length;
        let allUnchecked = true;
        for (; notificationIndex < notificationsLength; notificationIndex++) {
            let notification = subcategory.notifications[notificationIndex];
            if (notification[channelType + 'Checkbox'] &&
                notification[channelType + 'Checkbox'].value) {
                allUnchecked = false;
                break;
            }
        }
        return allUnchecked;
    }

    /**
     * Function used to determine if all children notifications on a subcategory are disabled
     * @param subcategory subcategory on which to check the children notifications
     * @param channelType type of checkbox to check
     */
    areAnyNotificationCheckboxesExisting(subcategory: SubCategory, channelType: string): boolean {
        let notificationIndex = 0;
        let notificationsLength = subcategory.notifications.length;
        let anyCheckboxesExist = false;
        for (; notificationIndex < notificationsLength; notificationIndex++) {
            let notification = subcategory.notifications[notificationIndex];
            if (notification[channelType + 'Checkbox']){
                anyCheckboxesExist = true;
                break;
            }
        }
        return anyCheckboxesExist;
    }

    /**
     * This will save the contactNotification preferences already on the page for a specific contactId
     * @param contacts
     */
    saveContactNotificationPreferences(...contacts: Contact[]) {
        //send Data to save to webservice
        let dataToSave = this.repackNotificationPreferenceDataForWebserviceSave(this.notificationPreferencesData, contacts);
        let names = _.map(contacts, c => `${c.firstName} ${c.lastName}`).join(', ');
        const done = this._loadingScreen.add(`Saving Notification Preferences for ${names}`);
        this._cub_notificationService.updateNotificiationPreferencesForContact(dataToSave)
            .then((data) => {
                this.showSaveModal = true;
                setTimeout(() => { this.showSaveModal = false; }, 2500);
            })
            .catch(error => this._cub_dataService.onApplicationError(error))
            .then(() => this.saveContactForm())
            .then(() => done());
    }

    //Event functions
    /**
     * This function is used by Apply Preferences to Different Contact button.
     * @param event data about the event firing
     */
    onApplyPreferencesToDifferentContactClick(event: any) {
        const done = this._loadingScreen.add('Retrieving Related Contacts');
        this._cub_notificationService.getContactsRelatedToCustomer(this.customerId)
            .then((result: Array<Contact>) => {
                this.contactsRelatedToCustomer = result;
                this.contactsRelatedToCustomer.forEach(contact => {
                    contact.selected = false;
                    contact.contactCheckbox = {
                        checkboxClass: 'regular-checkbox',
                        isDisabled: false,
                        id: contact.contactId,
                        name: [contact.firstName, contact.lastName].join(''),
                        label: [contact.firstName, " ", contact.lastName].join(''),
                        value: contact.selected,
                        partial: false
                    } as Model.Cub_Checkbox;
                })
            })
            .catch((err) => this._cub_dataService.prependCurrentError('Error applying preferences to different contact'))
            .then(() => done());
        //Open Modal Dialog and select related contacts to apply values to
        this.showApplyPreferencesToContacts = true;        
    }

    /**
     *  This function handles the expand all buton click. It expands all of the li elements for
     *  the categories and subcategories
     * @param event data about the event firing
     */
    onExpandAllClicked(event: any) {
        let categoryIndex = 0;
        let categoriesLength = this.notificationPreferencesData.length;
        for (; categoryIndex < categoriesLength; categoryIndex++) {
            let category = this.notificationPreferencesData[categoryIndex];
            category.isExpanded = true;
            let subcategoryIndex = 0;
            let subcategoriesLength = category.subcategories.length;
            for (; subcategoryIndex < subcategoriesLength; subcategoryIndex++) {
                let subcategory = category.subcategories[subcategoryIndex];
                subcategory.isExpanded = true;
            }
        }
        this.expandAllEnabled = false;
        this.collapseAllEnabled = true;
    }

    /**
     *  This function handles the collapse all buton click. It collapses all of the li elements for
     *  the categories and subcategories so only top level categories are shown
     * @param event data about the event firing
     */
    onCollapseAllClicked(event: any) {
        let categoryIndex = 0;
        let categoriesLength = this.notificationPreferencesData.length;
        for (; categoryIndex < categoriesLength; categoryIndex++) {
            let category = this.notificationPreferencesData[categoryIndex];
            category.isExpanded = false;
            let subcategoryIndex = 0;
            let subcategoriesLength = category.subcategories.length;
            for (; subcategoryIndex < subcategoriesLength; subcategoryIndex++) {
                let subcategory = category.subcategories[subcategoryIndex];
                subcategory.isExpanded = false;
            }
        }
        this.expandAllEnabled = true;
        this.collapseAllEnabled = false;
    }

    /**
     * onCollapsibleClicked - Handles toggling the flyout row visibility when the row is clicked.
     * @param event data about the event firing
     * @param collapsibleRecord record so we can change the isExpanded value.
     */
    onCollapsibleClicked(event: any, collapsibleRecord: any) {
        if (!collapsibleRecord || collapsibleRecord.isNotification) {
            return;
        }        
        collapsibleRecord.isExpanded = !collapsibleRecord.isExpanded;
        this.expandAllEnabled = true;
        this.collapseAllEnabled = true;
    }

    /**
     * Handles click for select all button in the apply preferences to other contacts modal dialog.
     * If all values are selected, change name to "Deselect all"
     * @param event
     */
    onSelectAllButtonClicked(event: any) {
        this.allContactsSelected = !this.allContactsSelected;
        this.selectAllButtonText = this.allContactsSelected ? "Deselect All" : "Select All";
        this.applyButton.isDisabled = !this.allContactsSelected;
        this.contactsRelatedToCustomer.forEach(contact => {
            contact.contactCheckbox.value = this.allContactsSelected;
        });
    }

    /**
     * This function handles the apply button clicked on the apply preferences to other contacts modal.
     * When clicked, it will apply the current preferences to the selected contacts.
     * @param event
     */
    onApplyButtonClicked(event: any) {
        let selectedContacts = _.filter(this.contactsRelatedToCustomer, function (contact: Contact) { return contact.contactCheckbox.value; });
        this.saveContactNotificationPreferences(...selectedContacts);
        this.showApplyPreferencesToContacts = false;
    }

    /**
     * This closes the apply preferences to other contacts model window.
     * @param event
     */
    onCancelButtonClicked(event: any) {
        this.showApplyPreferencesToContacts = false;
    }

    /**
     * This function handles what happens when the checkbox is changed in the apply preferences to other contacts model window.
     * If all are selected, change select all button label to "Deselect All", otherwise set to "Select All"
     * When one is checked, enable apply button, otherwise, disable it.
     * @param event
     * @param contact
     */
    onContactCheckboxChanged(event: any, contact: Contact) {
        let allSelected = true;
        let noneSelected = true;
        this.contactsRelatedToCustomer.forEach(relatedContact => {
            if (!relatedContact.contactCheckbox.value) {
                allSelected = false;
            }
        });
        this.contactsRelatedToCustomer.forEach(relatedContact => {
            if (relatedContact.contactCheckbox.value) {
                noneSelected = false;
            }
        });
        this.allContactsSelected = allSelected;
        this.selectAllButtonText = this.allContactsSelected ? "Deselect All" : "Select All";
        this.applyButton.isDisabled = noneSelected;
    }

    /**
     * Handles save event from CRM
     */
    handleExternalSave() {
        let contact: Contact = {
            contactId: this.contactId,
            firstName: this.firstName,
            lastName: this.lastName
        }
        this.saveContactNotificationPreferences(contact);    
    }

    saveContactForm() {
        parent.Xrm.Page.data.entity.removeOnSave(saveNotificationPreferences);
        parent.Xrm.Page.data.entity.save();
        parent.Xrm.Page.data.entity.addOnSave(saveNotificationPreferences);
    }

    /**
     * This function is called on the initialization of the Angular app for this page.
     * the querystring parameters are pulled and used to query contact data necessary for the page to work
     */
    ngOnInit() {
        //this.phoneForSMSRadioList['value'] = _.first<any>(this.phoneForSMSRadioList['controls']).value;
        this.channel = '';

        this.contactId = parent.Xrm.Page.data.entity.getId();
        const customerControl = parent.Xrm.Page.getAttribute('parentcustomerid');
        if (customerControl && customerControl.getValue()) {
            this.customerId = customerControl.getValue()[0].id;
        }

        parent.Xrm.Page.data.entity.addOnSave(saveNotificationPreferences);

        const getPreferencesDone = this._loadingScreen.add();
        this._cub_notificationService.getCustomerNotificationPreferences(this.customerId, this.contactId, this.channel)
            .then((data) => {
                this.rawDataFromWebservice = data;
                let contact = data['contacts'][0];
                //this.contactId = contact['contactid'];
                this.firstName = contact['firstName'];
                this.lastName = contact['lastName'];
                this.email = contact['email'];
                this.smsPhone = contact['smsPhone'];
                this.mandatory = contact['mandatory'];
                this.preferencesData = contact['preferences'];
                this.preferencesData = this.sortPreferences(this.preferencesData);
                this.notificationPreferencesData = this.prepareNotificationPreferenceData(this.preferencesData);
            })
            .catch(error => this._cub_dataService.prependCurrentError('Error loading customer notification preferences'))
            .then(() => getPreferencesDone());

        const getChannelsDone = this._loadingScreen.add();
        this._cub_notificationService.getEnabledNotificationChannels()
            .then((channelData) => {
                //Sets the channels option checkboxes to if they're enabled or not.
                channelData.forEach(channel => {
                    switch (channel.channel.toLowerCase()) {
                        case 'email':
                            this.notificationmethodCheckboxList.controls['Email'].value = channel.enabled;
                            break;
                        case 'sms':
                            this.notificationmethodCheckboxList.controls['SMS'].value = channel.enabled;
                            break;
                        case 'pushnotification':
                            this.notificationmethodCheckboxList.controls['Push'].value = channel.enabled;
                            break;
                        default:
                            break;
                    }
                });
            })
            .catch(error => this._cub_dataService.prependCurrentError('Error getting enabled notification channels'))
            .then(() => getChannelsDone()); 
    }

    ngOnDestroy() {
        window['cubicNotificationPreferences'] = null;
    }
}