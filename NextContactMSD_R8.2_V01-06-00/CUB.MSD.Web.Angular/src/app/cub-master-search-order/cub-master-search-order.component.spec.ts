import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Cub_MasterSearchOrderComponent } from './cub-master-search-order.component';
//import { Cub_SessionInfoComponent } from '../cub-sessioninfo/cub_sessioninfo.component';
import { Cub_SessionModule } from '../cub-session/cub_session.module';
import { Cub_FlydownComponent } from '../controls/cub-flydown/cub_flydown.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';
import { Cub_SessionService } from '../services/cub-session.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

describe('Cub_MasterSearchOrderComponent', () => {
    let component: Cub_MasterSearchOrderComponent;
    let fixture: ComponentFixture<Cub_MasterSearchOrderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                HttpClientTestingModule,
                Cub_SessionModule
            ],
            declarations: [
                Cub_MasterSearchOrderComponent,
                //Cub_SessionInfoComponent,
                Cub_FlydownComponent,
                Cub_DropdownComponent
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        ActivatedRouteStub._queryParams = {
            recordId: '00000000-0000-0000-0000-000000000001',
            currentUserId: '00000000-0000-0000-0000-000000000002'
        }
        fixture = TestBed.createComponent(Cub_MasterSearchOrderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
