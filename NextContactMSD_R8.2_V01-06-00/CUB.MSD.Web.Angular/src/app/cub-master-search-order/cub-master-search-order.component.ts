import { Component, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { CubOrderService } from '../services/cub-order.service';
import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_DateRangeComponent } from '../controls/cub-date-range/cub-date-range.component';
import { Cub_MultiDropdown } from '../controls/cub-multi-dropdown/cub-multi-dropdown.component';
import * as moment from 'moment-timezone';
import * as Model from '../model';

import * as $ from 'jquery';

@Component({
    selector: 'cub-master-search-order',
    templateUrl: './cub-master-search-order.component.html',
    providers: [Cub_CustomerOrderService, CubOrderService]
})
export class Cub_MasterSearchOrderComponent implements OnInit {

    @ViewChild(Cub_DateRangeComponent)
    dateRange: Cub_DateRangeComponent;

    @ViewChildren(Cub_MultiDropdown)
    multiDropdowns: QueryList<Cub_MultiDropdown>;

    masterSearchFlydown: Model.Cub_Flydown = {
        id: 'master-search-flydown',
        label: 'Master Search:',
        value: 'Order',
        options: []
    };

    displayAsLinkPage: boolean = false;
    dateOffset: number;
    typeControl: Model.Cub_MultiDropdownOption[] = [];
    statusControl: Model.Cub_MultiDropdownOption[] = [];

    orderNumber: Model.Cub_Textbox;
    searchOrderButton: Model.Cub_Button = {
        id: "searchOrderButton",
        label: "Search",
        class: "button-primary",
        isDisabled: true
    };
    searchOrderNumberButton: Model.Cub_Button = {
        id: "searchOrderNumberButton",
        label: "Search",
        class: "button-primary",
        isDisabled: true
    };

    /* Filter values */
    orderHistoryResults: Model.WSOrderHistoryLineItem[] = [];
    totalCount: number = 0;
    startDateFilter: string;
    endDateFilter: string;
    get selectedCustomer(): any { return this._cub_dataService.selectedContactCustomerToVerify; }
    set selectedCustomer(value: any) { this._cub_dataService.selectedContactCustomerToVerify = value; }
    redirectEntity: string;
    showVerification: boolean = false;
    rowOffset: number = 0;
    rowsLimit: number = 10;
    rowCountToShow: number;
    disableApply: boolean = true;
    displaySearchResults: boolean = false;

    orderTotals: any[];
    orderResults: any[];
    orderResultsLineItems: any[];
    orderResultsLineItemsLength: number;
    order: any;

    readonly TEMP = 'INCOMING';

    constructor(
        public _cub_dataService: Cub_DataService,
        private _cub_orderService: CubOrderService,
        private _cub_customerOrderService: Cub_CustomerOrderService,
        private _loadingScreen: AppLoadingScreenService,
        private _router: Router,
        private _route: ActivatedRoute
    ) { }

    handleRequired(data: any) {
        var orderNumber = this.orderNumber;

        if (orderNumber['value']) {
            orderNumber['isRequired'] = true;
        }
        else {
            orderNumber['isRequired'] = false;
        }

        if (this.typeControl["value"] || this.statusControl["value"]) {
            this.searchOrderButton.isDisabled = false;
        }
        else {
            this.searchOrderButton.isDisabled = true;
        }

        if (this.orderNumber["value"]) {
            this.searchOrderNumberButton.isDisabled = false;
            this.searchOrderButton.isDisabled = true;
            this.dateRange.reset;
            this.multiDropdowns.forEach(md => md.reset());
        }
        else {
            this.searchOrderNumberButton.isDisabled = true;
        }

        this.orderNumber["isErrored"] = false;
        this.orderNumber["showErrorMessage"] = false;
        this.orderNumber["isInvalid"] = false;
    }

    ngOnInit() {
        let height = $('#navTabGroupDiv', top.document).css('height');
        $('#crmTopBar', top.document).hide();
        // NOTE: this currently gets overridden by MSD on initial page load
        $('#crmContentPanel', top.document).css('top', height);

        this._router.navigate([
            '/MasterSearchOrder',
            { outlets: { session: 'Session' } }
        ], { replaceUrl: true });

        this.masterSearchFlydown.options = this._cub_dataService.masterSearchPages;

        this._cub_dataService.Globals
            .first(globals => !!globals['MasterSearch'])
            .subscribe(globals => {
                this.dateOffset = globals['MasterSearch']['OrderMasterSearchDateOffset'] as number;
                this.rowsLimit = globals['MasterSearch']['OrderMasterSearchLimit'] as number;
                this.rowCountToShow = globals['MasterSearch'][''] as number;
            });

        this.orderNumber = {
            type: "Textbox",
            id: "orderNumber",
            class: "form-field-width-XL",
            isRequired: true,
            hideRequiredIndicator: true,
            errorMessage: "Order # is required",
            validationMessage: "Order # is invalid",
            label: "Order #",
            isVerticalStackedLabel: true,
            value: this._cub_dataService.searchValues['orderNumber'] || '',
            validationRegEx: /^\d+$/, //TODO: Globals?
            onChange: this.handleRequired
        } as Model.Cub_Textbox;

        this._cub_orderService.getOrderConfigDataTypesFromCache()
            .then(configData => {
                // Filling Order Types
                    this.typeControl = configData.orderTypes.map(ot => ({
                            value: ot.id,
                            label: ot.shortDescription }))
                
                // Filling Order Status
                    this.statusControl = configData.orderStatuses.map(ot => ({
                        value: ot.id,
                        label: ot.shortDescription  }))
            })
    }

    //TODO: implement this and figure out unit testing Router
    onFlydownChanged([pageValue, control]: [string, Model.Cub_Flydown]) {
        this._router.navigate(['/MasterSearch' + pageValue]);
    }

    clearButtonClicked(event: any) {
        this._cub_dataService.searchValues["orderNumber"] = "";
        this.orderNumber.value = "";
        this.dateRange.reset();
        this.multiDropdowns.forEach(md => md.reset());
        this.searchOrderButton.isDisabled = true;
        this.searchOrderNumberButton.isDisabled = true;
    }

    textChanged(data: any = null) {
        if (data) {
            var newValue = data[0];
            var textControl = data[1];
        }
        this.handleRequired(data);
    }


    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.handleRequired(startDate);
    }

    /**
     * Fires when the end date text is modified or a date is selected using the
     * date picker. This sets the end date filter used in getBalanceHistory().
     */
    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.handleRequired(endDate);
    }

    /**
    * Generic onChange function that enables the search button. Fires when a
    * dropdown control changes.
    */
    onMultiDropdownChanged(event, control) {
        this[control].value = event;
        this.handleRequired(event);
    }

    customerClicked(record) {
        this.selectedCustomer = record;
        this.redirectEntity = 'account';
        this._cub_dataService.previousPage = "/MasterSearchOrder";
        this.showVerification = true;
    }

    searchOrderButtonClicked(event: any) {
        let startDate = this.startDateFilter;
        let endDate = this.endDateFilter;
        if (startDate && !endDate) {
            endDate = moment.tz(startDate, 'GMT').format('YYYY-MM-DD[T23:59:59.999Z]');
        }
        if (endDate && !startDate) {
            startDate = moment.tz(endDate, 'GMT').format('YYYY-MM-DD[T00:00:00.000Z]');
        }

        let params: any = {
            startDate: startDate,
            endDate: endDate,
            offset: this.rowOffset,
            limit: this.rowsLimit,
            orderType: this.typeControl,
            orderStatus: this.statusControl
        };

        const done = this._loadingScreen.add();
        this._cub_customerOrderService.getOrderHistory(params)
            .then(data => {
                this.disableApply = true;
                if (data) {
                    this.orderHistoryResults = data.orders;
                    this.totalCount = data.totalCount;
                }
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    searchOrderNumberButtonClicked() {
        const done = this._loadingScreen.add();
        this._cub_customerOrderService.getOrder(null, this.orderNumber.value)
            .then((data: any) => {
                if (data) {
                    this.displaySearchResults = true;
                    //this.orderResults = data;
                    this._cub_dataService.masterSearchOrderResults = data.order;
                    //this.order = this.orderHistoryResults =  data.order;
                    //this.orderResultsLineItems = data.order.lineItems;
                    if (data.order.lineItems) {
                        this.orderResultsLineItemsLength = data.order.lineItems.length;
                    }
                    this.orderTotals = [{
                        itemsSubtotalAmount: data.order.itemsSubtotalAmount,
                        orderTotalAmount: data.order.orderTotalAmount
                    }];
                }
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());}


    //displaySearchResults() { }

    /**
    * Validates filter inputs, extracts parameters, and calls the web service
    * to retrieve order history records.
    */
    getOrders() {
        // GET /customer/<customer-id>/orderhistory requires that both date filters
        // are set. If one is not set, set it to the same date as the other
        
    }


}
