import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { DelinkCustomerArgs, CubError } from '../model';

import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
    selector: 'cub-subsystem-delink-customer',
    templateUrl: './cub-subsystem-delink-customer.component.html',
    providers: [
        Cub_OneAccountService
    ]
})
export class Cub_SubsystemDelinkCustomerComponent implements OnInit {
    showConfirmDelinkPage: boolean = true;
    showSaveModal: boolean = false;
    showErrorsModal: boolean = false;
    requestBody: DelinkCustomerArgs;

    @Input() subsystemAccount: string;
    @Input() subsystemId: string;
    @Input() customerId: string;
    @Input() oneaccountId: string = '';
    @Output() onClose = new EventEmitter<any>();

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_oneAccountService: Cub_OneAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private _router: Router,
        private _route: ActivatedRoute
    ) { }

    selectedValue: any;
    delinkErrors: any[] = [];
    delinkErrorMessages: any[] = [];

    delinkClicked(record: any) {
        this.showConfirmDelinkPage = true;
    }

    delinkConfirmed() {
        let shouldRefresh: boolean;
        this.showConfirmDelinkPage = false;
        const check = this._loadingScreen.add('Checking if account can be delinked...');

        // FDS 3.4.14 - first check if canBeDelinked
        this._cub_oneAccountService.CanBeDeLinkedOneAccountFromSubsystem(this.oneaccountId, this.subsystemId, this.subsystemAccount, this.customerId)
            .then((data: any) => {
                if (data && data.delinkErrors && data.delinkErrors.length != 0) {
                    this.delinkErrors = data.delinkErrors;
                    // Loop through delinkErrors array and format error messages - future create error translation module
                    for (var i = 0; i < this.delinkErrors.length; i++) {
                        if (this.delinkErrors[i].key == 'errors.subsystem.account.active.autoload') {
                            this.delinkErrors[i].message = 'The account has an active or pending autoload enrollment.';
                        }
                        else if (this.delinkErrors[i].key == 'errors.subsystem.account.low.balance') {
                            this.delinkErrors[i].message = 'The account has a negative balance.';
                        }
                        else {  // catch other erros
                            this.delinkErrors[i].message = this.delinkErrors[i].key;
                        }
                    }

                    this.showErrorsModal = true;
                    return;
                }
                if (!data.hdr || data.hdr.result !== 'Successful') {
                    return Promise.reject(data.hdr || 'Unable to delink transit account.');
                }
                // Delink if no errors
                const delink = this._loadingScreen.add('Delinking Transit Account...');
                this._cub_oneAccountService.DeLinkOneAccountFromSubsystem(this.oneaccountId, this.subsystemId, this.subsystemAccount, this.customerId)
                    .then((data: any) => {
                        this.showSaveModal = true;
                        //hide save dialog after 2.5 seconds.
                        setTimeout(() => {
                            this.showSaveModal = false;
                            this.closeDialog(true);
                        }, 2500);
                    })
                    .catch(err => this._cub_dataService.onApplicationError(err))
                    .then(() => delink());
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => check());
    }

    cancelDelink() {
        this.showConfirmDelinkPage = false;
        this.closeDialog(false);
    }

    okErrors() {
        this.showErrorsModal = false;
        this.showConfirmDelinkPage = false;
        this.closeDialog(false);
    }

    closeDialog(shouldRefresh: boolean) {
        const result = {
            refresh: shouldRefresh
        };
        if(this._router.url.match(/^\/SubsystemAccountsDelink/)){
            window['Mscrm'].Utilities.setReturnValue(result);
            window['closeWindow']();
        } else {
            this.onClose.emit(result);
        }
    }
    
    ngOnInit() {
        const queryParams = this._route.snapshot.queryParamMap;
        if (queryParams.has('customerId')
            && queryParams.has('subsystemId')
            && queryParams.has('subsystemAccount')
        ) {
            this.customerId = queryParams.get('customerId');
            this.subsystemId = queryParams.get('subsystemId');
            this.subsystemAccount = queryParams.get('subsystemAccount');
        }

        if (!this.customerId) {
            this._cub_dataService.onApplicationError('No customer ID');
        } else if (!this.subsystemAccount || !this.subsystemId) {
            this._cub_dataService.onApplicationError('No subsystem ID or subsystem account');
        } else {
            this.showConfirmDelinkPage = true;
        }
    }
}
