import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cub_SubsystemDelinkCustomerComponent } from './cub-subsystem-delink-customer.component';

describe('CubSubsystemDelinkCustomerComponent', () => {
    let component: Cub_SubsystemDelinkCustomerComponent;
    let fixture: ComponentFixture<Cub_SubsystemDelinkCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [Cub_SubsystemDelinkCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Cub_SubsystemDelinkCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
