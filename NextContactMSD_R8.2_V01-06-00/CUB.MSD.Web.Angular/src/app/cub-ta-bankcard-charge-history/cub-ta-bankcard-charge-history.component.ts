import { Component, Input, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import * as Model from '../model';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { CubTaBankcardChargeHistoryService } from '../services/cub_ta_bankcard_charge_history.service';
import { Cub_DateRangeComponent } from '../controls/cub-date-range/cub-date-range.component';
import { Cub_MultiDropdown } from '../controls/cub-multi-dropdown/cub-multi-dropdown.component';
import * as lodash from 'lodash';
import * as moment from 'moment';
import { LazyLoadEvent, SortMeta } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'cub_ta_bankcard_charge_history',
    templateUrl: './cub-ta-bankcard-charge-history.component.html',
    styleUrls: ['../../svg-temp.css'], // TODO: remove when refresh icon updated
    providers: [CubTaBankcardChargeHistoryService]
})
export class CubTaBankcardChargeHistoryComponent implements OnInit {

    @ViewChild(Cub_DateRangeComponent)
    dateRange: Cub_DateRangeComponent;

    @ViewChildren(Cub_MultiDropdown)
    multiDropdowns: QueryList<Cub_MultiDropdown>;

    transitAccountId: string = "";
    subsystemId: string = "";
    paymentId: string = "";

    chargesHistory: Array<object> = [];
    sortedChargesHistory: Array<object> = [];
    dateOffset: number;

    showAggregation: boolean = false;

    dateTypeControl: Model.Cub_Dropdown = {
        label: 'Date Type',
        id: 'dateTypeControl',
        options: []
    };

    responseControlOptions: Model.Cub_MultiDropdownOption[];
    statusControlOptions: Model.Cub_MultiDropdownOption[];

    responseControlFilter: string = null;
    statusControlFilter: string = null;

    disableSearch: boolean = false;
    showReset: boolean = false;

    initialPass: boolean = true;
    rowsPerPage: number = 5; //default
    initComplete: boolean = false;

    /* Filter values */
    startDateFilter: string;
    endDateFilter: string;

    /*Pagination and Sorting params*/
    sortConfig: SortMeta[] = [];
    sortBy: string;
    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cubTaBankcardChargeHistoryService: CubTaBankcardChargeHistoryService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.transitAccountId = parent.Xrm.Page.getAttribute('cub_name').getValue();
        this.subsystemId = parent.Xrm.Page.getAttribute('cub_subsystemid').getValue();

        const globals$ = this._cub_dataService.Globals.first(globals => !!globals['CUB.MSD.Web.Angular']);
        const grids$ = this._cub_dataService.GridConfigs.first(configs => !!configs['TransitAccount.BankCardChargeHistory']);
        const done = this._loadingScreen.add();
        Observable.forkJoin(globals$, grids$)
            .finally(() => done())
            .subscribe(([globals, grids]) => {

                let angularGlobals = globals['CUB.MSD.Web.Angular'];

                this.dateOffset = globals['CUB.MSD.Web.Angular']['TA.Bankcard.Charge.History.StartDateOffset'] as number;
                this.startDateFilter = moment().subtract(this.dateOffset, 'days').format('YYYY-MM-DD[T23:59:59.999Z]');
                this.endDateFilter = moment().startOf('day').format('YYYY-MM-DD[T23:59:59.999Z]');

                // Date Type control
                this._cub_dataService.fillCubDropdown(this.dateTypeControl, globals, 'TA.Bankcard.Charge.History.DateType.Options');

                // Response control
                this.responseControlOptions = this.parseFilterOptions(angularGlobals, 'TA.Bankcard.Charge.History.Response.Options');

                // Status control
                this.statusControlOptions = this.parseFilterOptions(angularGlobals, 'TA.Bankcard.Charge.History.Status.Options');

                this.dateTypeControl.value = globals['CUB.MSD.Web.Angular']['TA.Bankcard.Charge.History.DateType.Default'] as string;
                this.setDefaultSortByDateType();

                const grid = grids['TransitAccount.BankCardChargeHistory'];
                this.rowsPerPage = grid.rowsToDisplay;
                this.sortBy = grid.defaultSort;
                this.getChargesHistory();
            });
    }

    //Handle grid loading on paging
    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.sortConfig = event.multiSortMeta;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.disableSearch = false;
            this.getChargesHistory();
        }
    }

    onSearchClicked() {
        this.getChargesHistory();
    }

    getChargesHistory() {
        let subSystemId = this.subsystemId;
        let accountId = this.transitAccountId;
        let dateType = this.dateTypeControl.value;
        let status = this.statusControlFilter;
        let response = this.responseControlFilter;
        let sortBy = this.sortBy || null;
        let offset = (this.pagingOffset > 0) ? this.pagingOffset : 0;
        let limit = this.rowsPerPage;

        const done = this._loadingScreen.add('Loading Bankcard Charges History...');

        this._cubTaBankcardChargeHistoryService.getBankCardChargesHistory({
            subSystem: subSystemId,
            accountId: accountId,
            startDateTime: this.startDateFilter,
            endDateTime: this.endDateFilter,
            dateType: dateType,
            status: status,
            response: response,
            sortBy: sortBy,
            offset: offset,
            limit: limit
        })
            .then((data) => {
                if (data) {
                    var data = JSON.parse(data.Body);
                    this.chargesHistory = data.lineItems;
                    this.sortedChargesHistory = data.lineItems;
                    this.totalRecordCount = data.totalCount;
                    this.disableSearch = true;
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting bankcard charges history');
            })
            .then(() => this.initComplete = true)
            .then(() => done());
    }

    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableSearch = false;
        this.showReset = this.initComplete;
    }

    /**
     * Fires when the end date text is modified or a date is selected using the
     * date picker. This sets the end date filter used in getBalanceHistory().
     */
    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableSearch = false;
        this.showReset = this.initComplete;
    }

    /**
    * Generic onChange function that enables the search button. Fires when a
    * dropdown control changes.
    */
    onMultiDropdownChange(selectedOptions: string[], filterName: string) {
        if (this[filterName] === undefined) {
            this._cub_dataService.onApplicationError('No filter with name "' + filterName + '" on bankcard charge history component!');
            return;
        }

        // if the join returns an empty string, use null instead
        this[filterName] = selectedOptions.join(',') || null;
        this.disableSearch = false;
    }

    /**
    * Fires when a row is clicked within the taps history table. This
    * calls the service to get the select tap history's detail, prepares
    * the data to display within the detail modal window, and resets the resend
    * modal window controls.
    * @param event event.data is the selected balance record
    */
    onRowDblClick(event: any) {
        this.paymentId = event.data.paymentId
        this.showAggregation = true;
    }

    onDateTypeControlChanged(event: any) {
        this.setDefaultSortByDateType();
        this.disableSearch = false;
        this.showReset = this.initComplete;
    }

    hideAggregation() {
        this.showAggregation = false;
    }

    setDefaultSortByDateType() {
        this.sortConfig = [{
            field: this.dateTypeControl.value === 'dateSettled' ? 'settledDateTime' : 'openedDateTime',
            order: -1
        }, {
            field: 'paymentId',
            order: 1
        }];
        this.sortBy = this._cub_dataService.formatSortQueryParam(this.sortConfig);
    }

    reset() {
        this.dateRange.reset();
        this.multiDropdowns.forEach(md => md.reset());

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.dateTypeControl.value = globals['CUB.MSD.Web.Angular']['TA.Bankcard.Charge.History.DateType.Default'] as string;
                this.setDefaultSortByDateType();
                this.showReset = false;
            });
    }

    parseFilterOptions(nisGlobals, globalDetailName): Model.Cub_MultiDropdownOption[] {
        let options: Model.Cub_MultiDropdownOption[] = [];
        try {
            let config: object = JSON.parse(nisGlobals[globalDetailName]);
            options = lodash.chain(config)
                .map((name, key) => ({
                    value: key,
                    label: name
                } as Model.Cub_MultiDropdownOption))
                .sortBy(o => o.value)
                .value();
        } catch (e) {
            this._cub_dataService.onApplicationError('Failure to parse config: ' + globalDetailName);
        }
        return options;
    }
}


