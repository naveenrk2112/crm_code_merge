import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubTaBankcardChargeHistoryComponent } from './cub-ta-bankcard-charge-history.component';

describe('CubTaBankcardChargeHistoryComponent', () => {
  let component: CubTaBankcardChargeHistoryComponent;
  let fixture: ComponentFixture<CubTaBankcardChargeHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubTaBankcardChargeHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubTaBankcardChargeHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
