﻿import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Cub_MasterSearchService } from '../services/cub_mastersearch.service';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_CacheService } from '../services/cub-cache.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_SessionService } from '../services/cub-session.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import * as Model from '../model';

import * as _ from 'lodash';
import * as $ from 'jquery';

@Component({
    selector: 'cub-mastersearchcontact',
    templateUrl: './cub_mastersearch.contact.component.html',
    providers: [
        Cub_MasterSearchService,
        Cub_OneAccountService,
        Cub_SubsystemService
    ]
})
export class Cub_MasterSearchContactComponent implements OnInit {
    subsystemAccountReference: string;
    subsystemAccountNickname: string;
    subsystemId: string;
    displayAsLinkPage: boolean = false;
    showLinkDialog = false;

    phoneMask = {
        mask: [] as any,
        placeholderChar: '_' as string
    };
    cancelLinkButton: Model.Cub_Button = {
        id: 'cancelLinkButton',
        label: 'Cancel',
        class: 'button-secondary-text',
        isDisabled: false
    }
    defaultCustomerType: string = "";
    controls: Object = {};
    firstName: Model.Cub_Textbox = {
        type: "Textbox",
        id: "firstName",
        isRequired: true,
        hideRequiredIndicator: true,
        errorMessage: "First name is required",
        label: "First Name",
        isVerticalStackedLabel: true,
        value: ""
    };
    lastName: Model.Cub_Textbox = {
        type: "Textbox",
        id: "lastName",
        isRequired: true,
        hideRequiredIndicator: true,
        errorMessage: "Last name is required",
        label: "Last Name",
        isVerticalStackedLabel: true,
        value: ""
    };
    phone: Model.Cub_Textbox = {
        type: "Textbox",
        id: "phone",
        label: "Phone",
        isVerticalStackedLabel: true,
        value: "",
        placeholderChar: '_'
    };
    email: Model.Cub_Textbox = {
        type: "Textbox",
        id: "email",
        label: "Email",
        isVerticalStackedLabel: true,
        value: ""
    };
    searchButton: Model.Cub_Button = {
        id: "searchButton",
        label: "Search",
        class: "button-primary",
        isDisabled: true
    };
    masterSearchFlydown: Model.Cub_Flydown = {
        id: 'masterSearchFlydown', //unique control id
        label: 'Master Search:', //Label for control
        value: "Contact", //Value for control
        options: this._cub_dataService.masterSearchPages
    };
    customerTypeDropdown: Model.Cub_Dropdown = {
        id: 'customerTypeDropdon', //unique control id
        label: 'Customer Type', //Label for control
        formFieldClass: 'form-fields-vertical', //Form field class to use, eg. form-fields-siblings
        labelWidthClass: 'form-field-width-M', //Width of label
        fieldWidthClass: "form-field-width-M", //Width of Control
        isTableControl: false,
        options: [],
        value: null
    };
    displaySearchResults: boolean = false;
    rowsOnPage: number = 5;
    sortBy = "BestMatch";
    sortOrder = "asc";
    sortOrderControl: Model.Cub_Dropdown = {
        formFieldClass: 'form-fields-vertical', //Form field class to use, eg. form-fields-siblings
        labelWidthClass: 'form-field-width-M', //Width of label
        fieldWidthClass: "form-field-width-XL", //Width of Control
        isTableControl: false,
        isSmall: true,
        id: 'sortOrderControl', //unique control id
        label: 'Sort by', //Label for control
        value: "BestMatch", //Value for control
        options: [ //Array of objects: {name: "Label Text", value: "Value"}
            { value: "BestMatch", name: "Best Match" },
            { value: "AccountName", name: "Customer Name" },
            { value: "FullName", name: "Contact Full Name" },
            { value: "FirstName", name: " Contact First Name" },
            { value: "LastName", name: "Contact Last Name" },
            { value: "Email", name: "Email" }]
    };
    showVerification: boolean = false;
    postVerificationResult: Model.VerificationResult;
    redirectEntity: string;
    navigateToToken: boolean = false;
    private readonly _cacheKey = 'master-search-contact';

    get selectedContact(): any { return this._cub_dataService.selectedContactCustomerToVerify; }
    set selectedContact(value: any) { this._cub_dataService.selectedContactCustomerToVerify = value; }
    phoneValue: string = '';

    constructor(
        private _cub_oneAccountService: Cub_OneAccountService,
        private _cub_masterSearchService: Cub_MasterSearchService,
        public _cub_dataService: Cub_DataService,
        private _cache: Cub_CacheService,
        private _cub_sessionService: Cub_SessionService,
        private _cub_subsystemService: Cub_SubsystemService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private router: Router,
        private _renderer: Renderer2
    ) { }

    handleRequired() {
        var phone = this.phone;
        var email = this.email;
        var firstName = this.firstName;
        var lastName = this.lastName
        if (phone["value"] || email["value"]) {
            firstName["isRequired"] = false;
            lastName["isRequired"] = false;
        }
        else if (this.firstName["value"] || lastName["value"]) {
            firstName["isRequired"] = true;
            lastName["isRequired"] = true;
        }
        else {
            firstName["isRequired"] = false;
            lastName["isRequired"] = false;
        }

        if (this.firstName.value.trim() == '' && this.lastName.value.trim() == '' && this.email.value.trim() == ''
            && (this.phone.value.trim() == '' || this.phone.value.trim() == '(___) ___-____')) {
            this.searchButton.isDisabled = true;
        }
        else if (this.firstName["value"] || this.lastName["value"] ||
            this.phone["value"] ||
            this.email["value"]) {
            this.searchButton.isDisabled = false;
        }
        else {
            this.searchButton.isDisabled = true;
        }

        this.firstName["isErrored"] = false;
        this.firstName["showErrorMessage"] = false;
        this.lastName["isErrored"] = false;
        this.lastName["showErrorMessage"] = false;
    }

    ngOnInit() {
        let height = $('#navTabGroupDiv', top.document).css('height');
        $('#crmTopBar', top.document).hide();
        // NOTE: this currently gets overridden by MSD on initial page load
        $('#crmContentPanel', top.document).css('top', height);
        const bodyTag = document.getElementsByTagName('body')[0];
        this._renderer.addClass(bodyTag, 'grey-background');

        const queryParams = this.route.snapshot.queryParamMap;
        // if these values are passed, the user wishes to link a subsystem account to a customer
        if (queryParams.has('subsystemId') && queryParams.has('subsystemAccountReference')) {
            this.subsystemId = queryParams.get('subsystemId');
            this.subsystemAccountReference = queryParams.get('subsystemAccountReference');
            this.displayAsLinkPage = true;
        }

        this.router.navigate([
            '/MasterSearchContact',
            { outlets: { session: 'Session' } }
        ], { replaceUrl: true });

        this.tryRetrieveCachedInputs();

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'] && !!globals.MasterSearch)
            .subscribe(globals => this.loadConstants(globals));

        const done = this._loadingScreen.add();
        this._cub_masterSearchService.getAccountTypes()
            .then(value => {
                this.processAccountTypes(value);
            })
            .catch(error => this._cub_dataService.onApplicationError(error))
            .then(() => done());
    }

    loadConstants(globals: Model.Cub_Globals) {
        //Table
        if (globals['MasterSearch']['PageSize']) {
            this.rowsOnPage = globals['MasterSearch']['PageSize'] as number;
        }

        //Dropdowns
        if (globals.MasterSearch.CustomerType_DefaultValue) {
            this._cub_dataService.searchValues["customerType"] = globals.MasterSearch.CustomerType_DefaultValue as string;
            this.customerTypeDropdown.value = globals.MasterSearch.CustomerType_DefaultValue as string;
            this.defaultCustomerType = globals.MasterSearch.CustomerType_DefaultValue as string;
        }

        //Search Fields
        if (globals.MasterSearch.FirstName_MaxLength) {
            this.firstName.maxLength = globals.MasterSearch.FirstName_MaxLength as number;
        }
        if (globals.MasterSearch.LastName_MaxLength) {
            this.lastName.maxLength = globals.MasterSearch.LastName_MaxLength as number;
        }
        if (globals.MasterSearch.Email_MaxLength) {
            this.email.maxLength = globals.MasterSearch.Email_MaxLength as number;
        }
        if (globals.MasterSearch.Phone_Mask) {
            let value = globals.MasterSearch.Phone_Mask as string;
            const characterMaskList = value.split('|')
                .map(char => char.length > 1 ? new RegExp(char) : char);

            // For some reason the text-mask sets the input to "(___) ___-____"
            // if the first character doesn't match rather than leave it blank, so
            // we use this function to force a blank input if the first character is
            // unrelated to a phone number. This function fires when the input value 
            // changes. It accepts the updated input value and returns a list of character masks.
            // https://github.com/text-mask/text-mask/blob/master/componentDocumentation.md#mask-function
            const determineMask = (rawInput) => {
                if (!rawInput) {
                    return characterMaskList;
                }
                // first character should be 1-9 or satisfy the first character mask
                if (!rawInput[0].match(/[1-9]/)
                    && ((typeof characterMaskList[0] === 'string' && rawInput[0] !== characterMaskList[0])
                        || (characterMaskList[0] instanceof RegExp && !rawInput[0].match(characterMaskList[0])))
                ) {
                    return [/[1-9]/];
                }
                return characterMaskList;
            };
            // We have to reassign the phoneMask to update the mask field.
            this.phoneMask = Object.assign({}, this.phoneMask, { mask: determineMask });
        }
    }

    //Promise resolve for getAccountTypes
    processAccountTypes(data: any) {
        let options = [];
        for (var i = 0; i < data.length; i++) {
            var accountTypes = data[i];
            options.push({
                value: accountTypes['Key'],
                name: accountTypes['Value']
            });
        }
        this.customerTypeDropdown.options = options;
        if (!this.customerTypeDropdown.value) {
            this.customerTypeDropdown.value = data.length > 0 ? data[0]['Key'] : null;
        }
    }

    searchButtonClicked() {
        if ((this.firstName['value'] && !this.lastName['value'])
            || (this.lastName['value'] && !this.firstName['value'])) {
            this.firstName['isRequired'] = true;
            this.lastName['isRequired'] = true;
            this.firstName["isErrored"] = !this.firstName["value"];
            this.firstName["showErrorMessage"] = !this.firstName["value"];
            this.lastName["isErrored"] = !this.lastName["value"];
            this.lastName["showErrorMessage"] = !this.lastName["value"];
        }
        else if ((this.firstName['value'] && this.lastName['value']) || this.phone['value'] || this.email['value']) {
            this.sortBy = "BestMatch"; //reset sorting;
            const done = this._loadingScreen.add('Searching...');
            this._cub_masterSearchService.searchCustomer()
                .then(() => this.searchCompleted())
                .catch(err => this._cub_dataService.onApplicationError(err))
                .then(() => done());
        }
    }

    searchCompleted() {
        this.displaySearchResults = true;
        this._cub_dataService.sortMasterSearchContactResults(this.sortBy, true);
    }

    /**
     * Navigate to Customer Search route in such a way as to preserve
     * the session outlet (if present)
     */
    createNewCustomerButtonClicked() {
        this.cacheFormInputs();
        this._cub_dataService.previousPage = this.router.url;
        this.router.navigate(['/CustomerSearch']);
    }

    onSortDropdownChanged(event: any) {
        if (!event || event.length <= 0) {
            console.log('Missing event information from dropdown event')
            return;
        }
        var selectedValue = event[0];
        this._cub_dataService.sortMasterSearchContactResults(selectedValue, true);
    }

    onCustomerTypeDropdownChanged(event: any) {
        this._cub_dataService.onApplicationError('Not yet implemented');
    }

    onMasterSearchSelectDropdownChanged(event: any) {
        this._cub_dataService.previousPage = "/MasterSearchContact";
        this.cacheFormInputs();
        this.router.navigate([`/MasterSearch${event[0]}`]);
    }

    clearButtonClicked(event: any) {
        this._cub_dataService.searchValues["customerType"] = this.defaultCustomerType;
        this.customerTypeDropdown.value = this.defaultCustomerType;
        this._cub_dataService.searchValues["firstName"] = "";
        this.firstName["value"] = "";
        this._cub_dataService.searchValues["lastName"] = "";
        this.lastName["value"] = "";
        this._cub_dataService.searchValues["phone"] = "";
        this.phone["value"] = "";
        this._cub_dataService.searchValues["email"] = "";
        this.email["value"] = "";
        this.searchButton.isDisabled = true;
        this.selectedContact = null;
    }

    textChanged(data: any = null) {
        if (data) {
            var newValue = data[0];
            var textControl = data[1];
            this._cub_dataService.searchValues[textControl['id']] = newValue;
        }
        this.handleRequired();
    }

    contactClicked(record) {
        this.selectedContact = record;
        this.redirectEntity = 'contact';
        this._cub_dataService.previousPage = "/MasterSearchContact";
        this.showVerification = true;
    }

    linkToContactClicked() {
        this._cub_dataService.previousPage = "/MasterSearchContact";
        // Verify first, then navigate
        this.navigateToToken = true;
        this.showVerification = true;
    }

    cancelLinkButtonClicked() {
        this._cub_dataService.previousPage = "";
        this.cacheFormInputs();
        this.router.navigate(["/MasterSearchToken"]);
    }

    promptVerification() {
        this.redirectEntity = 'account';
        this._cub_dataService.previousPage = "/MasterSearchContact";
        this.showVerification = true;
    }

    resolveVerification(event: Model.VerificationResult) {
        this.showVerification = false;
        this.postVerificationResult = event;
        if (event === Model.VerificationResult.CANCEL) {
            return;
        }

        if (this.displayAsLinkPage) {
            this.showLinkDialog = true;
        } else if (this.navigateToToken) {
            this.cacheFormInputs();
            this.router.navigate(['/MasterSearchToken'], {
                queryParams: {
                    customerId: this.selectedContact.AccountId,
                    customerName: this.selectedContact.FullName,
                    oneAccountId: this.selectedContact.OneAccountId
                }
            });
        } else {
            this.loadSessionAndRedirect(false);
        }
    }

    completeLinkProcess() {
        const done = this._loadingScreen.add('Linking');
        this._cub_oneAccountService.LinkOneAccountToSubsystem(this.selectedContact['OneAccountId'], this.subsystemId, this.subsystemAccountReference, this.subsystemAccountNickname)
            .then(() => this.loadSessionAndRedirect(true))
            .catch((error) => this._cub_dataService.onApplicationError(error))
            .then(() => done());
    }

    loadSessionAndRedirect(refreshData: boolean) {
        const done = this._loadingScreen.add('Loading session');

        if (refreshData) {
            this._cub_oneAccountService.clearCachedOneAccountSummary(this.selectedContact.AccountId);
            if (this.subsystemId && this.subsystemAccountReference) {
                this._cub_subsystemService.clearCachedSubsystemStatus(this.subsystemId, this.subsystemAccountReference);
            }
        }

        let chain = Promise.resolve() as Promise<any>;
        if (!this._cub_sessionService.isActive) {
            chain = chain.then(() => this._cub_sessionService.createSession('contact', this.selectedContact['ContactId']));
        } else if (!this._cub_sessionService.session.isRegistered) {
            const updatedSessionFields = {
                id: this._cub_sessionService.session.sessionId,
                cub_contactid: { id: this.selectedContact.ContactId },
                cub_customerid: { id: this.selectedContact.AccountId },
                cub_isregistered: true,
                cub_isverified: this.postVerificationResult === Model.VerificationResult.VERIFIED,
                cub_isprimarycontact: true // TODO: is this handled in the backend? either remove from here or figure out how to determine
            };
            chain = chain.then(() => this._cub_sessionService.updateSession(updatedSessionFields));
        }

        chain
            .then(() => {
                this.cacheFormInputs();
                let id = this.redirectEntity === 'account' ? this.selectedContact.AccountId : this.selectedContact.ContactId;
                this._cub_dataService.openMsdForm(this.redirectEntity, id);
            })
            .catch((error) => this._cub_dataService.onApplicationError(error))
            .then(() => done());
    }

    tryRetrieveCachedInputs() {
        const fields = this._cache.retrieveSync<Model.MasterSearchContactCache>(this._cacheKey);
        if (fields) {
            this._cub_dataService.searchValues.customerType = fields.customerType;
            this._cub_dataService.searchValues.firstName = fields.firstName;
            this._cub_dataService.searchValues.lastName = fields.lastName;
            this._cub_dataService.searchValues.phone = fields.phone;
            this._cub_dataService.searchValues.email = fields.email;

            this.customerTypeDropdown.value = fields.customerType;
            this.firstName.value = fields.firstName;
            this.lastName.value = fields.lastName;
            this.phone.value = fields.phone;
            this.email.value = fields.email;
        }
        this.handleRequired();
    }

    cacheFormInputs() {
        const fields: Model.MasterSearchContactCache = {
            customerType: this.customerTypeDropdown.value,
            firstName: this.firstName.value,
            lastName: this.lastName.value,
            phone: this.phone.value,
            email: this.email.value
        };
        this._cache.store(this._cacheKey, fields);
    }

    customerClicked(record) {
        if (record != null && record.AccountId != null) {
            this._cub_dataService.openMsdForm(Model.CustomerLogicalName, record.AccountId);
        }
    }
}