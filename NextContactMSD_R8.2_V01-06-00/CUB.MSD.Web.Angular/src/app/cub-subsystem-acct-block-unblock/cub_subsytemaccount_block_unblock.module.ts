﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Cub_SubsystemAccountBlockUnblockComponent } from './cub_subsytemaccount_block_unblock.component';
import { Cub_DropdownModule } from '../controls/cub-dropdown/cub_dropdown.component';
import { MoneyPipeModule } from '../pipes/money.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        Cub_DropdownModule,
        RouterModule.forChild([
            { path: '', component: Cub_SubsystemAccountBlockUnblockComponent }
        ]),
        MoneyPipeModule
    ],
    declarations: [Cub_SubsystemAccountBlockUnblockComponent],
    exports: [Cub_SubsystemAccountBlockUnblockComponent]
})
export class Cub_SubsystemAcctBlockUnblockModule { }