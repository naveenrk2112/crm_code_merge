﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { BlockUnblockAccountArgs, WSBlockUnblockAccountResponse, CubError, Cub_DropdownOption } from '../model';

import * as _ from 'lodash';
import * as Model from '../model';

@Component({
    selector: 'cub-subsystem-acct-block-unblock',
    templateUrl: './cub_subsytemaccount_block_unblock.component.html',
    providers: [ Cub_SubsystemService ]
})
export class Cub_SubsystemAccountBlockUnblockComponent implements OnInit {
    requestBody: BlockUnblockAccountArgs;
    responseBody: WSBlockUnblockAccountResponse;
    reasonCodeControl: Model.Cub_Dropdown;
    notes: string = "";

    showNotesError: boolean = false;
    showSaveModal: boolean = false;
    showErrorsModal: boolean = false;

    blockReasonCodeDesc: string = "";
    unblockReasonCodeDesc: string = "";
    reasonCodeTypes: any;
    reasonCodeTypeId: any;
    reasonCodeSelected: any
    reasonCodeDescription: any;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cub_subsystemService: Cub_SubsystemService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
        const queryParams = this._route.snapshot.queryParamMap;
        if (!queryParams.has('subsystemId')
            || !queryParams.has('subsystemAccount')
            || !queryParams.has('block')
        ) {
            this._cub_dataService.onApplicationError('No data passed to Block\Unblock modal');
            return;
        }

        this.requestBody = {
            subsystemId: queryParams.get('subsystemId'),
            subsystemAccount: queryParams.get('subsystemAccount'),
            block: JSON.parse(queryParams.get('block'))
        };

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.blockReasonCodeDesc = globals['CUB.MSD.Web.Angular']['ReasonCode_BlockAccount'] as string;
                this.unblockReasonCodeDesc = globals['CUB.MSD.Web.Angular']['ReasonCode_UnblockAccount'] as string;

                if (!this.blockReasonCodeDesc) {
                    this.blockReasonCodeDesc = "BlockAccount"; }

                if (!this.unblockReasonCodeDesc) {
                    this.unblockReasonCodeDesc = "UnblockAccount";
                }

                this.reasonCodeDescription = this.requestBody.block ? this.blockReasonCodeDesc : this.unblockReasonCodeDesc; 
                this.getReasonCodeTypes();
            });

        //"Block" : "Unblock";
        this.reasonCodeControl = {
            id: 'reasonCodeControl',
            formFieldClass: 'form-field-drop-down',
            fieldWidthClass: 'form-field-width-L',
            errorMessage: 'Reason Code is required.',
            isRequired: true,
            value: '',
            options: []
        };
    }

    getReasonCodeTypes() {
        this._cub_subsystemService.getReasonCodes()
            .then((data) => {
                if (data) {
                    this.reasonCodeTypes = JSON.parse(data.Body);
                }
            })
            .then(() => {
                this.reasonCodeControl.options = [{
                    value: "0",
                    id: "0",
                    name: "Select...",
                    isDisabled: true,
                    notesRequired: false
                } as Cub_DropdownOption];

                this.reasonCodeControl.options = this.reasonCodeControl.options.concat(_.find<any>(
                    this.reasonCodeTypes.reasonCodeTypes,
                    rct => rct['name'] === this.reasonCodeDescription).reasonCodes.map(rc => ({
                        value: rc.reasonCodeId,
                        name: rc.description,
                        notesRequired: rc.notesMandatoryFlag
                    })));
                //if (this.reasonCodeControl.options.length > 0) {
                //    this.reasonCodeSelected = this.reasonCodeControl.options[0];}
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting reason Codes');
            });
    } 

    onReasonCodeChanged() {
        this.reasonCodeSelected = _.find<any>(this.reasonCodeControl.options, ['value', parseInt(this.reasonCodeControl.value)]);  
        this.reasonCodeControl.isErrored = false;     
    }

    submitBlockClicked() {
        if (!this.validateSubmit()) return;

        this._cub_subsystemService.blockSubsystemTransitAccount(this.requestBody.subsystemId, this.requestBody.subsystemAccount, this.reasonCodeSelected.value, this.notes)
            .then(data => {
                if (data != null) {
                    if (data.Header.errorMessage) {
                        this.showErrorsModal = true;
                    }
                    else {
                        this.showSaveModal = true;
                    }
                }
            })
            .catch(err => {
                if (err instanceof CubError && err.handled) {
                    return;
                }
                this._cub_dataService.onApplicationError(err);
            })
    }

    validateSubmit() {
        if (this.reasonCodeSelected == null) {
            this.reasonCodeControl.showErrorMessage = true;
            this.reasonCodeControl.isErrored = true;
            return false;
        }

        if (this.reasonCodeSelected.notesRequired && !this.notes) {
            this.showNotesError = true;
            return false
        }
        else
        {
            this.showNotesError = false;
            return true;
        }
    }

    submitUnBlockClicked() {
        if (!this.validateSubmit()) return;

        this._cub_subsystemService.unblockSubsystemTransitAccount(this.requestBody.subsystemId, this.requestBody.subsystemAccount, this.reasonCodeSelected.value, this.notes)
            .then(data => {
                if (data != null) {
                    this.responseBody = data;
                    if (data.Header.errorMessage) {
                        this.showErrorsModal = true;
                    }
                    else {
                        this.showSaveModal = true;
                    }
                }
            })
            .catch(err => {
                if (err instanceof CubError && err.handled) {
                    return;
                }
                this._cub_dataService.onApplicationError(err);
            })
    }

    cancelClicked() {
        this.showSaveModal = false;
        let shouldRefresh: boolean = false;

        window['Mscrm'].Utilities.setReturnValue({
            refresh: shouldRefresh
        });
        window['closeWindow']();
    }

    okErrors() {
        this.showErrorsModal = false;
        let shouldRefresh: boolean = false;

        window['Mscrm'].Utilities.setReturnValue({
            refresh: shouldRefresh
        });
        window['closeWindow']();
    }

    okClicked() {
        this.processResponse(); 
    }

    processResponse() {
        if (this.responseBody == null) {
            let error = ['Something went wrong while attempting to ', this.reasonCodeDescription, ' transit account ', this.requestBody.subsystemAccount, '.'].join('');
            this._cub_dataService.onApplicationError(error);
            return;
       }

        if (this.responseBody.Header.result === 'Successful') {
            window['Mscrm'].Utilities.setReturnValue({
                refresh: true
            });
            window['closeWindow']();
        }
    }
}