/**
 * This class extends the PrimeNG Tooltip directive to reduce clutter in our
 * HTML templates by applying default tooltip properties. If the element has
 * no inner content, it sets the tooltip text as the content.
 * 
 * Usage:
 * <span cubTooltip="duplicated text"></span>
 * <p cubTooltip="tooltip text">different paragraph text</p>
 * 
 * PrimeNG Tooltip references:
 * https://www.primefaces.org/primeng/#/tooltip
 * https://github.com/primefaces/primeng/blob/master/src/app/components/tooltip/tooltip.ts
 */

import { Directive, Input, ElementRef, Renderer2, OnChanges, SimpleChanges } from '@angular/core';
import { Tooltip, DomHandler } from 'primeng/primeng';

@Directive({
    selector: '[cubTooltip]',
    providers: [DomHandler]
})
export class Cub_TooltipDirective extends Tooltip implements OnChanges {
    constructor(
        public el: ElementRef,
        public domHandler: DomHandler,
        public renderer: Renderer2
    ) { 
        super(el, domHandler, renderer);
        this.tooltipPosition = 'top';
        this.showDelay = 1000;
        this.hideDelay = 200;
    }

    private setInnerContent: boolean = false;

    @Input() set cubTooltip(content: string) {
        this.text = content;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.cubTooltip) {
            if (changes.cubTooltip.firstChange && !this.el.nativeElement.innerHTML.trim()) {
                this.setInnerContent = true;
            }
            if (this.setInnerContent) {
                this.renderer.setProperty(this.el.nativeElement, 'innerHTML', '');
                let innerText = this.renderer.createText(this.text);
                this.renderer.appendChild(this.el.nativeElement, innerText);
            }
        }
    }
}
