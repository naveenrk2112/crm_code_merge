import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Tam_AddTranspondersComponent } from './tam-add-transponders.component';

describe('Tam_AddTranspondersComponent', () => {
    let component: Tam_AddTranspondersComponent;
    let fixture: ComponentFixture<Tam_AddTranspondersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [Tam_AddTranspondersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Tam_AddTranspondersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
