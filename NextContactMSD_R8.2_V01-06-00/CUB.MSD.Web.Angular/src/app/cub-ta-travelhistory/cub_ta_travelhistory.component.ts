﻿/**
 * This component is outdated and unused.
 * Please refer to the cub-ta-tripshistory folder for most recent version.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Cub_TA_TravelHistoryService, TravelHistorySearchCriteria } from '../services/cub_ta_travelhistory.service';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';

import { LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'cub-ta-travelhistory',
    templateUrl: './cub_ta_travelhistory.component.html',
    providers: [Cub_TA_TravelHistoryService]
})
export class Cub_TA_TravelHistoryComponent implements OnInit {
    transitAccountId: string = "";
    subsystemId: string = "";
    disableSearch: boolean = true;
    travelHistoryResults: any[] = [];
    selectedDetailId: any = null;
    showDetail: boolean = false;

    startDateControl = {
        placeholder: 'MM/DD/YYYY',
        formFieldClass: 'form-field-text', //Form field class to use, eg. form-fields-siblings
        fieldWidthClass: 'form-field-width-100', //Width of field (eg. form-field-width-100)
        spacerWidthClass: '', //Width of space to right of field (eg. form-field-width-100)
        isVerticalStackedLabel: false, //Should we display this control with a label on top of the control?
        id: 'startDateControl', //unique control id
        value: moment().subtract(30, 'days').format('MM/DD/YYYY'), //Value for control
        mask: [/[01]/, /\d/, '/', /[0-3]/, /\d/, '/', /[12]/, /\d/, /\d/, /\d/], //Mask to use for formatting
        placeholderChar: '_', //default mask to show for empty spaces
        vaidationRegEx: '[01]{0,1}[0-9]{1}\/[0123]{0,1}[0-9]{1}\/[19,20]{2}[\d]{2}', //Validate entered data
        dateConfiguration: {
            showDatePicker: true, //Shows a popup date picker
            dateFormat: 'MM/DD/YYYY' //Format of date popover input
        },
    };
    startDateFilter: string;
    endDateControl = {
        placeholder: 'MM/DD/YYYY',
        formFieldClass: 'form-field-text', //Form field class to use, eg. form-fields-siblings
        fieldWidthClass: 'form-field-width-100', //Width of field (eg. form-field-width-100)
        spacerWidthClass: '', //Width of space to right of field (eg. form-field-width-100)
        isVerticalStackedLabel: false, //Should we display this control with a label on top of the control?
        id: 'endDateControl', //unique control id
        value: moment().format('MM/DD/YYYY'), //Value for control
        mask: [/[01]/, /\d/, '/', /[0-3]/, /\d/, '/', /[12]/, /\d/, /\d/, /\d/], //Mask to use for formatting
        placeholderChar: '_', //default mask to show for empty spaces
        vaidationRegEx: '[01]{0,1}[0-9]{1}\/[0123]{0,1}[0-9]{1}\/[19,20]{2}[\d]{2}', //Validate entered data
        dateConfiguration: {
            showDatePicker: true, //Shows a popup date picker
            dateFormat: 'MM/DD/YYYY' //Format of date popover input
        },
        isDisabled: true
    };
    endDateFilter: string;
    travelModeOptions: Model.Cub_MultiDropdownOption[];
    travelModeFilter: string;
    tripOptions: Model.Cub_MultiDropdownOption[];
    tripTypeFilter: string;
    travelPresenceOptions: Model.Cub_MultiDropdownOption[];
    travelPresenceFilter: string;
    viewTypeControl: Model.Cub_Radiolist = {
        id: 'view-type-control',
        group: 'ViewType',
        label: 'View Type',
        value: 'view-type-Simple',
        isButton: true,
        controls: [
            {
                id: 'view-type-Simple',
                label: 'Simple',
            }, {
                id: 'view-type-Complete',
                label: 'Complete',
            }
        ]
    };
    tripStatusControl: Model.Cub_Radiolist = {
        id: 'trip-status-control',
        group: 'TripStatus',
        label: 'Trip Status',
        value: 'trip-status-All',
        isButton: true,
        controls: [
            {
                id: 'trip-status-All',
                label: 'All'
            }, {
                id: 'trip-status-Void',
                label: 'Void'
            }, {
                id: 'trip-status-Final',
                label: 'Final'
            }
        ]
    };

    /*Pagination and Sorting params*/
    multiSortMeta: SortMeta[] = [];
    sortBy: string;
    rowsPerPage: number = 10;
    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    constructor(
        public _cub_ta_travelHistoryService: Cub_TA_TravelHistoryService,
        public _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
        const queryParams = this._route.snapshot.queryParamMap;
        if (queryParams.has('transitAccountId')) {
            this.transitAccountId = queryParams.get('transitAccountId');
        }
        if (queryParams.has('subsystemId')) {
            this.subsystemId = queryParams.get('subsystemId');
        }

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => this.loadConstants(globals));
        // NOTE: no need to call getTravelHistory() here, it is called by the 
        // grid's lazy load event on init
    }

    loadConstants(globals: Model.Cub_Globals) {
        this.rowsPerPage = globals['CUB.MSD.Web.Angular']['TA.TravelHistory.NumRows'] as number;
        let offset = globals['CUB.MSD.Web.Angular']['TA.TravelHistory.StartDateOffset'] as number;
        this.startDateControl.value = moment().subtract(offset, 'days').format('MM/DD/YYYY');
        this.travelModeOptions = this.loadDropdownOptions(globals['CUB.MSD.Web.Angular']['TravelModes'] as string);
        this.tripOptions = this.loadDropdownOptions(globals['CUB.MSD.Web.Angular']['TripCategories'] as string);
        this.travelPresenceOptions = this.loadDropdownOptions(globals['CUB.MSD.Web.Angular']['TravelPresenceIndicators'] as string);
    }

    private loadDropdownOptions(globalOptionsValue: string) {
        if (globalOptionsValue == null) {
            return [];
        }
        return _.chain(globalOptionsValue.split(','))
            .map(c => c.trim())
            .sortBy()
            .map(c => ({
                value: c,
                label: c
            }))
            .value();
    }

    getTravelHistory(event?: LazyLoadEvent) {
        if (!!event) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
        }

        if (!!this.endDateFilter && moment(this.startDateFilter).isAfter(this.endDateFilter, 'hour')) {
            this._cub_dataService.showError('Invalid date range');
            this.disableSearch = true;
            return;
        }

        //this.accountId = '330000000258';
        //this.subsystem = 'ABP';

        if (this.transitAccountId == '' || this.subsystemId == '')
        {
            this._cub_dataService.onApplicationError("Error: account id and subsystem not passed.")
            return;
        }

        let params: TravelHistorySearchCriteria = {
            accountId: this.transitAccountId,
            subSystemId: this.subsystemId,
            offset: (this.pagingOffset > 0) ? this.pagingOffset : null,
            limit: this.rowsPerPage,
            sortBy: (!!this.sortBy) ? this.sortBy : null,
            viewType: this.viewTypeControl.value.replace('view-type-', '')
        };

        // apply date range filter
        if (!!this.startDateFilter) {
            params.startDateTime = this.startDateFilter;
            if (!!this.endDateFilter) {
                params.endDateTime = this.endDateFilter;
            }
        }

        // apply travel mode filter
        if (this.travelModeFilter) {
            params.travelMode = this.travelModeFilter;
        }

        // apply trip category filter
        if (this.tripTypeFilter) {
            params.tripCategory = this.tripTypeFilter;
        }

        // apply travel presence indicator filter
        if (this.travelPresenceFilter) {
            params.travelPresenceIndicator = this.travelPresenceFilter;
        }

        // apply trip status filter
        if (this.tripStatusControl.value !== 'trip-status-All') {
            params.tripStatus = this.tripStatusControl.value.replace('trip-status-', '');
        }

        const done = this._loadingScreen.add('Retrieving travel history');
        this._cub_ta_travelHistoryService.getTravelHistory(params)
            .then((data) => {
                if (data) {
                    this.travelHistoryResults = data.lineItems;
                    this.totalRecordCount = data.totalCount;
                }
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error getting travel history'))
            .then(() => done());
    }

    /**
    * Fires when the start date text is modified or a date is selected using the
    * date picker. This sets the start date filter used in getTravelHistory()
    * and determines whether the end date control should be disabled.
    */
    onStartDateControlChanged(event: any) {
        if (!this.startDateControl.value) {
            this.startDateFilter = null;
            this.endDateControl.value = '';
            this.endDateControl.isDisabled = true;
            this.endDateFilter = null;
            this.disableSearch = false;
        }
        else if (this.startDateControl.value.indexOf(this.startDateControl.placeholderChar) === -1) {
            this.endDateControl.isDisabled = false;
            this.startDateFilter = moment(this.startDateControl.value, this.startDateControl.placeholder)
                .format('YYYY-MM-DD[T00:00:00.000Z]');
            this.disableSearch = false;
        }
    }

    /**
    * Fires when the end date text is modified or a date is selected using the
    * date picker. This sets the end date filter used in getNotifications().
    */
    onEndDateControlChanged(event: any) {
        if (!this.endDateControl.value) {
            this.endDateFilter = null;
            this.disableSearch = false;
        } else if (this.endDateControl.value.indexOf(this.endDateControl.placeholderChar) === -1) {
            this.endDateFilter = moment(this.endDateControl.value, this.endDateControl.placeholder)
                .format('YYYY-MM-DD[T23:59:59.999Z]');
            this.disableSearch = false;
        }
    }

    onTravelModeDropdownChanged(travelModes: string[]) {
        this.travelModeFilter = travelModes.join();
        this.disableSearch = false;
    }

    onTripTypeDropdownChanged(tripTypes: string[]) {
        this.tripTypeFilter = tripTypes.join();
        this.disableSearch = false;
    }

    onTravelPresenceDropdownChanged(travelPresenceIndicators: string[]) {
        this.travelPresenceFilter = travelPresenceIndicators.join();
        this.disableSearch = false;
    }

    /**
   * Fires when the enter key is pressed while either the start date, or
   * end date controls are in focus. Calls getTravelHistory() 
   */
    onEnterKey() {
        if (!this.disableSearch) {
            this.getTravelHistory();
            this.disableSearch = true;
        }
    }

    /**
    * Generic onChange function that enables the search button. Fires when the
    * contact, channel, or status controls change.
    */
    onControlChanged() {
        this.disableSearch = false;
    }

    /**
    * Fires when the search button is clicked. Calls getNotifications().
    */
    onSearchClicked() {

        this.getTravelHistory();
        this.disableSearch = true;
    }

    onRowDoubleClicked(event: any) {
        this.selectedDetailId = event.data.tripId;
        this.showDetail = true;
    }

    hideDetail() {
        this.showDetail = false;
    }
}