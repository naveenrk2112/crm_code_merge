import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cub_TA_TravelHistoryComponent } from './cub_ta_travelhistory.component';

describe('Cub_TA_TravelHistoryComponent', () => {
    pending(); // TODO: remove when implementing
    let component: Cub_TA_TravelHistoryComponent;
    let fixture: ComponentFixture<Cub_TA_TravelHistoryComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [Cub_TA_TravelHistoryComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_TA_TravelHistoryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
