﻿/**
 * As of 12/20/2017, this component is necessary to lazy-load modules in named router outlets.
 * Named outlets: http://onehungrymind.com/named-router-outlets-in-angular-2/
 * Lazy-loading to named outlet: https://github.com/angular/angular/issues/12842#issuecomment-270836368
 */

import { Component } from '@angular/core';

@Component({
    selector: 'cub-proxy-route',
    template: '<router-outlet></router-outlet>'
})
export class Cub_ProxyRouteComponent { }