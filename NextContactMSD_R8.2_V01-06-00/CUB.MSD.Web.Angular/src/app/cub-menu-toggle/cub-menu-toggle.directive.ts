﻿/**
 * This directive adds a click event listener that collapses its child menu
 * when the user clicks outside of the menu.
 */

import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: 'cub-menu-toggle'
})
export class Cub_MenuToggleDirective {
    constructor(private _elRef: ElementRef) { }

    /**
     * The HTML menu <input> checkbox's ID attribute
     */
    @Input() inputId: string;

    @HostListener('document:click', ['$event'])
    hideOnClickOut(event: MouseEvent) {
        if (!this._elRef.nativeElement.contains(event.target)) {
            this._elRef.nativeElement.querySelector('#' + this.inputId).checked = false;
        }
    }
}