import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubTaVoidTapComponent } from './cub-ta-void-tap.component';

describe('CubTaVoidTapComponent', () => {
  let component: CubTaVoidTapComponent;
  let fixture: ComponentFixture<CubTaVoidTapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubTaVoidTapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubTaVoidTapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
