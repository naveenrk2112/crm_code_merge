import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubCustomerActivitiesComponent } from './cub-customer-activities.component';

describe('CubCustomerActivitiesComponent', () => {
  let component: CubCustomerActivitiesComponent;
  let fixture: ComponentFixture<CubCustomerActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubCustomerActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubCustomerActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
