﻿import { ComponentFixture, TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { DataTableModule, SharedModule } from 'primeng/primeng';
import { NguiDatetimePickerModule, NguiDatetime } from '@ngui/datetime-picker';
import { TextMaskModule } from 'angular2-text-mask';
import * as moment from 'moment';
import { find } from 'lodash';

import { Cub_NotificationHistoryComponent } from './cub_notificationhistory.component';
import { Cub_ButtonComponent } from '../controls/cub-button/cub_button.component';
import { Cub_CheckboxComponent } from '../controls/cub-checkbox/cub_checkbox.component';
import { Cub_CheckboxlistComponent } from '../controls/cub-checkboxlist/cub_checkboxlist.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_HelpComponent } from '../controls/cub-help/cub_help.component';
import { Cub_OptionSelectComponent } from '../controls/cub-optionselect/cub_optionselect.component';
import { Cub_RadioComponent } from '../controls/cub-radio/cub_radio.component';
import { Cub_RadiolistComponent } from '../controls/cub-radiolist/cub_radiolist.component';
import { Cub_TextboxComponent } from '../controls/cub-textbox/cub_textbox.component';
import { Cub_DateRangeComponent } from '../controls/cub-date-range/cub-date-range.component';

import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';

import { MomentDatePipe } from '../pipes/moment-date.pipe';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_NotificationService } from '../services/cub_notification.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { ActivatedRouteStub } from '../testutilities/route.stub';
import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_NotificationServiceStub } from '../testutilities/cub_notification.service.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

describe('Cub_NotificationHistoryComponent', () => {
    const CUSTOMER_FORM_QUERY_PARAMS = {
        customerId: '00000000-0000-0000-0000-000000000001'
    };
    const CONTACT_FORM_QUERY_PARAMS = {
        customerId: '00000000-0000-0000-0000-000000000001',
        contactId: '00000000-0000-0000-0000-000000000002'
    };
    let fixture: ComponentFixture<Cub_NotificationHistoryComponent>,
        comp: Cub_NotificationHistoryComponent,
        service: Cub_NotificationService,
        dataService: Cub_DataService,
        root: DebugElement,
        table;

    /* Default values returned by notification service */
    beforeAll(() => {
        Cub_NotificationServiceStub._channels = [
            {
                channel: 'EMAIL',
                enabled: true
            }, {
                channel: 'SMS',
                enabled: true
            }
        ];
        Cub_NotificationServiceStub._contacts = [
            {
                contactId: '00000000-0000-0000-0000-000000000001',
                firstName: 'John',
                lastName: 'Doe'
            }, {
                contactId: '00000000-0000-0000-0000-000000000002',
                firstName: 'Jane',
                lastName: 'Doe'
            }
        ];
        Cub_NotificationServiceStub._detail = {
            alternateMessage: 'This is an example alternate message',
            message: 'This is a message',
            contactId: '00000000-0000-0000-0000-000000000001'
        };
        Cub_NotificationServiceStub._notifications = [
            {
                notificationId: 1,
                contactName: 'John Doe',
                createDateTime: '2017-06-01T00:00:00Z',
                notificationReference: 'R0001',
                type: 'Type',
                description: '',
                channel: 'EMAIL',
                recipient: 'johndoe@example.com',
                subject: 'Test',
                status: 'SENT',
                sendDateTime: '2017-06-01T00:00:01Z',
                failDateTime: '0001-01-01T00:00:00Z',
                allowResend: true
            }, {
                notificationId: 2,
                contactName: 'Jane Doe',
                createDateTime: '2017-06-03T00:00:00Z',
                notificationReference: 'R0002',
                type: 'Type',
                description: '',
                channel: 'SMS',
                recipient: 'janedoe@example.com',
                subject: 'Test',
                status: 'SENT',
                sendDateTime: '2017-06-03T00:00:01Z',
                failDateTime: '0001-01-01T00:00:00Z',
                allowResend: true
            }
        ];
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                DataTableModule,
                TextMaskModule,
                NguiDatetimePickerModule
            ],
            declarations: [
                Cub_NotificationHistoryComponent,
                Cub_ButtonComponent,
                Cub_CheckboxComponent,
                Cub_CheckboxlistComponent,
                Cub_ControlLabelComponent,
                Cub_DropdownComponent,
                Cub_HelpComponent,
                Cub_OptionSelectComponent,
                Cub_RadioComponent,
                Cub_RadiolistComponent,
                Cub_TextboxComponent,
                Cub_DateRangeComponent,
                Cub_TooltipDirective,
                MomentDatePipe
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
            ]
        }).overrideComponent(Cub_NotificationHistoryComponent, {
            set: {
                providers: [
                    { provide: Cub_NotificationService, useClass: Cub_NotificationServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent<Cub_NotificationHistoryComponent>(Cub_NotificationHistoryComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
        service = root.injector.get(Cub_NotificationService);
        dataService = TestBed.get(Cub_DataService);
        table = root.nativeElement.querySelector('body > div.hybrid');
    });

    afterEach(() => {
        comp.showNotificationDetail = false;
        comp.showResend = false;
        fixture.detectChanges();
    });

    it('should initialize controls', fakeAsync(() => {
        ActivatedRouteStub._queryParams = CUSTOMER_FORM_QUERY_PARAMS;
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        // Component data values
        let startDate = moment().subtract(Cub_DataServiceStub._constants['CUB.MSD.Web.Angular']['NotificationHistory.StartDateOffset'], 'days');
        expect(service.customerId).toEqual(CUSTOMER_FORM_QUERY_PARAMS.customerId);
        expect(comp.selectedResendChannels.size).toBe(0, 'The set of selected resend channels should be empty');
        expect(comp.rowsPerPage).toEqual(Cub_DataServiceStub._constants['CUB.MSD.Web.Angular']['NotificationHistory.NumRows'], 'Rows per page does not match');
        expect(comp.statusControl.controls.length).toBe(3, 'Number of status options does not match');
        expect(comp.statusControl.value).toBe('status-all');
        expect(comp.searchControl.value).toEqual('');
        expect(comp.channelControl.value).toEqual('channel-all');
        expect(comp.channelControl.controls.length).toBe(3, 'Number of channel options does not match');
        expect(comp.multipleChannels).toBe(true, 'Should not have multiple channels');
        expect(comp.endDateFilter).toEqual(moment().format('YYYY-MM-DD[T23:59:59.999Z]'));

        // DOM rendered elements
        expect(table.querySelector('#' + comp.searchControl.id)).toBeTruthy('Search textbox not rendered');
        expect(table.querySelector('#' + comp.channelControl.id)).toBeTruthy('Channel select buttons not rendered');
        expect(table.querySelector('#' + comp.statusControl.id)).toBeTruthy('Status select buttons not rendered');
    }));

    it('Customer Landing Page should populate contact controls on init', fakeAsync(() => {
        ActivatedRouteStub._queryParams = CUSTOMER_FORM_QUERY_PARAMS;
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        expect(comp.contactControl.value).toEqual('contact-all');
        expect(comp.contactControl.options.length).toBe(3, 'Number of contact options does not match');
        expect(comp.resendContactControl.options.length).toBe(2, 'Number of resend contact options does not match');
        expect(comp.multipleContacts).toBe(true, 'Should indicate multiple contacts');
        expect(table.querySelector('#' + comp.contactControl.id)).toBeTruthy('Contact select dropdown did not render')
    }));

    it('Contact Landing Page should not populate contact controls on init', fakeAsync(() => {
        ActivatedRouteStub._queryParams = CONTACT_FORM_QUERY_PARAMS;
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        expect(comp.contactControl.value).toEqual(CONTACT_FORM_QUERY_PARAMS.contactId);
        expect(comp.contactControl.options.length).toBe(1, 'The only contact option should be the unused contact-all');
        expect(comp.resendContactControl.options.length).toBe(0, 'There should be no resend contact options');
        expect(comp.multipleContacts).toBe(false, 'Should indicate a single contact');
    }));

    it('should hide contact control if only one contact is returned', fakeAsync(() => {
        spyOn(service, 'getContactsRelatedToCustomer').and.returnValue(Promise.resolve([{
            contactId: '00000000-0000-0000-0000-000000000002',
            firstName: 'Paul',
            lastName: 'Blart'
        }]));
        ActivatedRouteStub._queryParams = CUSTOMER_FORM_QUERY_PARAMS;

        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        expect(comp.contactControl.value).toEqual('contact-all');
        expect(comp.contactControl.options.length).toBe(2, 'Number of contact options does not match');
        expect(comp.multipleContacts).toBe(false);
        expect(table.querySelector('#' + comp.contactControl.id)).toBeNull('Contact select dropdown should not render');
    }));

    describe('#getNotifications', () => {
        beforeEach(fakeAsync(() => {
            ActivatedRouteStub._queryParams = CONTACT_FORM_QUERY_PARAMS;
            fixture.detectChanges();
            tick();
            fixture.detectChanges();
        }));

        it('should apply customerId each time', fakeAsync(() => {
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.customerId).toEqual(CONTACT_FORM_QUERY_PARAMS.customerId);
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should not apply search filter if it is blank', fakeAsync(() => {
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.notificationReference).toBeUndefined();
                expect(params.type).toBeUndefined();
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should apply search filter to notificationReference if it contains a number', fakeAsync(() => {
            const VALUE = '1234';
            comp.searchControl.value = VALUE;
            comp.onSearchControlChanged([VALUE, comp.searchControl]);
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.notificationReference).toEqual(VALUE);
                expect(params.type).toBeUndefined();
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should apply search filter to notificationReference if it contains an asterisk', fakeAsync(() => {
            const VALUE = '*';
            comp.searchControl.value = VALUE;
            comp.onSearchControlChanged([VALUE, comp.searchControl]);
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.notificationReference).toEqual(VALUE);
                expect(params.type).toBeUndefined();
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should apply search filter to type if it contains neither numbers nor asterisks', fakeAsync(() => {
            const VALUE = 'foo';
            comp.searchControl.value = VALUE;
            comp.onSearchControlChanged([VALUE, comp.searchControl]);
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.type).toEqual(VALUE);
                expect(params.notificationReference).toBeUndefined();
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should not apply any date range filter if start date filter is not set', fakeAsync(() => {
            comp.startDateFilter = '';
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.startDate).toBeUndefined();
                expect(params.endDate).toBeUndefined();
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should not apply contactId if contact control value is contact-all', fakeAsync(() => {
            comp.contactControl.value = 'contact-all';
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.contactId).toBeUndefined();
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should apply contactId if contact control value is not set to contact-all', fakeAsync(() => {
            comp.contactControl.value = CONTACT_FORM_QUERY_PARAMS.contactId;
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.contactId).toEqual(CONTACT_FORM_QUERY_PARAMS.contactId);
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should not apply channel filter if channel control value is channel-all', fakeAsync(() => {
            comp.channelControl.value = 'channel-all';
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.channel).toBeUndefined();
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should apply channel filter if channel control value is not channel-all', fakeAsync(() => {
            comp.channelControl.value = comp.channelControl.controls[1].id;
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.channel).toEqual(comp.channelControl.controls[1].id.substr(8));
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should not apply status filter if status control value is status-all', fakeAsync(() => {
            comp.statusControl.value = 'status-all';
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.status).toBeUndefined();
                return Promise.resolve();
            });
            comp.getNotifications();
            tick();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should apply status filter if status control value is not status-all', fakeAsync(() => {
            comp.statusControl.value = comp.statusControl.controls[1].id;
            fixture.detectChanges();
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                expect(params.status).toEqual(comp.statusControl.controls[1].id.substr(7));
                return Promise.resolve();
            });
            comp.getNotifications();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
        }));

        it('should attempt to display an error to the user if the call fails', fakeAsync(() => {
            const ERROR = { result: 'Test' };
            spyOn(service, 'getCustomerNotifications').and.callFake((params) => {
                return Promise.reject(ERROR);
            });
            spyOn(dataService, 'prependCurrentError');
            comp.getNotifications();
            fixture.detectChanges();
            tick();
            fixture.detectChanges();
            expect(service.getCustomerNotifications).toHaveBeenCalled();
            expect(dataService.prependCurrentError).toHaveBeenCalledWith(ERROR);
        }));
    });

    it('#onSearchControlChanged should set the search filter when triggered by the search text input', fakeAsync(() => {
        ActivatedRouteStub._queryParams = CONTACT_FORM_QUERY_PARAMS;
        const VALUE = 'test value';
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        comp.searchControl.value = VALUE;
        comp.onSearchControlChanged([VALUE, comp.searchControl]);
        fixture.detectChanges();
        expect(comp.searchFilter).toEqual(VALUE);

        comp.searchControl.value = '';
        comp.onSearchControlChanged(['', comp.searchControl]);
        fixture.detectChanges();
        expect(comp.searchFilter).toEqual('');
    }));

    it('#onSearchControlChanged should call getNotifications() when triggered by the search button', fakeAsync(() => {
        ActivatedRouteStub._queryParams = CONTACT_FORM_QUERY_PARAMS;
        fixture.detectChanges();
        tick();
        comp.disableSearch = false;
        fixture.detectChanges();
        spyOn(comp, 'getNotifications');

        document.getElementById('search-button').click();
        fixture.detectChanges();
        expect(comp.getNotifications).toHaveBeenCalled();
    }));

    it('#onEnterKey should not do anything if the search button is disabled', fakeAsync(() => {
        ActivatedRouteStub._queryParams = CONTACT_FORM_QUERY_PARAMS;
        fixture.detectChanges();
        tick();
        comp.disableSearch = true;
        fixture.detectChanges();
        spyOn(comp, 'getNotifications');
        comp.onEnterKey();
        fixture.detectChanges();
        expect(comp.getNotifications).not.toHaveBeenCalled();
    }));

    it('#onRowClicked should prepare the notification detail modal window for an email message and prepare the resend notification modal window', fakeAsync(() => {
        ActivatedRouteStub._queryParams = CONTACT_FORM_QUERY_PARAMS;
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        spyOn(comp, 'resetSelectedResendChannels');
        spyOn(service, 'getCustomerNotificationDetail').and.callThrough();
        spyOn(comp, 'setEmailMessageHtml');
        spyOn(comp, 'setSmsPushMessageHtml');
        let notification = find(service.customerNotificationResults, n => n.channel === 'EMAIL');
        comp.onRowDoubleClicked({ data: notification });
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        expect(comp.resetSelectedResendChannels).toHaveBeenCalled();
        expect(service.getCustomerNotificationDetail).toHaveBeenCalled();
        expect(comp.setEmailMessageHtml).toHaveBeenCalled();
        expect(comp.setSmsPushMessageHtml).not.toHaveBeenCalled();
        expect(root.nativeElement.querySelector('body > div.hybrid.modal.visible')).toBeTruthy();
        comp.closeDetail();
    }));

    it('#onRowClicked should prepare the notification detail modal window for an SMS/Push message and prepare the resend notification modal window', fakeAsync(() => {
        ActivatedRouteStub._queryParams = CONTACT_FORM_QUERY_PARAMS;
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        spyOn(comp, 'resetSelectedResendChannels');
        spyOn(service, 'getCustomerNotificationDetail').and.callThrough();
        spyOn(comp, 'setEmailMessageHtml');
        spyOn(comp, 'setSmsPushMessageHtml');
        let notification = find(service.customerNotificationResults, n => n.channel !== 'EMAIL');
        comp.onRowDoubleClicked({ data: notification });
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        expect(comp.resetSelectedResendChannels).toHaveBeenCalled();
        expect(service.getCustomerNotificationDetail).toHaveBeenCalled();
        expect(comp.setEmailMessageHtml).not.toHaveBeenCalled();
        expect(comp.setSmsPushMessageHtml).toHaveBeenCalled();
        expect(comp.showNotificationDetail).toBe(true);
        expect(root.nativeElement.querySelector('body > div.hybrid.modal.visible')).toBeTruthy();
    }));

    it('#onRowClicked should reshow the notification detail modal window without making another service call if it is the same as the previous one', fakeAsync(() => {
        ActivatedRouteStub._queryParams = CONTACT_FORM_QUERY_PARAMS;
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        spyOn(comp, 'resetSelectedResendChannels');
        spyOn(service, 'getCustomerNotificationDetail').and.callThrough();
        spyOn(comp, 'setEmailMessageHtml');
        spyOn(comp, 'setSmsPushMessageHtml');
        let n = service.customerNotificationResults[0];
        service.selected = n;
        comp.onRowDoubleClicked({ data: n });
        fixture.detectChanges();
        expect(comp.resetSelectedResendChannels).not.toHaveBeenCalled();
        expect(service.getCustomerNotificationDetail).not.toHaveBeenCalled();
        expect(comp.setEmailMessageHtml).not.toHaveBeenCalled();
        expect(comp.setSmsPushMessageHtml).not.toHaveBeenCalled();
        expect(comp.showNotificationDetail).toBe(true);
        expect(root.nativeElement.querySelector('body > div.hybrid.modal.visible')).toBeTruthy();
    }));
});