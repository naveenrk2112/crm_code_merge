﻿import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_NotificationService } from '../services/cub_notification.service';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';
import { DataTableModule, SharedModule, LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'cub-notificationhistory',
    templateUrl: './cub_notificationhistory.component.html',
    providers: [Cub_NotificationService]
})
export class Cub_NotificationHistoryComponent implements OnInit {
    /* Form controls */
    statusControl: Model.Cub_Radiolist;
    channelControl: Model.Cub_Radiolist;
    contactControl: Model.Cub_Dropdown;
    searchControl: any;
    resendContactControl: Model.Cub_Dropdown;
    resendChannelControl: any;

    dateRangeOffset: number = 30;

    /* Filter values */
    startDateFilter: string;
    endDateFilter: string;
    searchFilter: string;

    /* Notification detail modal formatted fields */
    statusDate: string;
    statusTime: string;
    processedDate: string;
    processedTime: string;

    multipleChannels: boolean = false;
    multipleContacts: boolean = false;
    showNotificationDetail: boolean = false;
    showResend: boolean = false;
    disableSearch: boolean = true;
    selectedResendChannels: Set<string>;
    multiSortMeta: SortMeta[] = [];
    sortBy: string;
    rowsPerPage: number = 10;
    pagingOffset: number = 0;
    initComplete: boolean = false;

    customerNotificationResults: Model.NotificationRecord[] = [];
    totalRecordCount: number = 0;

    constructor(
        public _cub_notificationService: Cub_NotificationService,
        public _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) {
        this.selectedResendChannels = new Set<string>();
        this.multiSortMeta.push({
            field: 'sendDateTime',
            order: -1
        });
        this.sortBy = this._cub_dataService.formatSortQueryParam(this.multiSortMeta);
    }

    /**
     * Executes when component is first rendered. Initializes form controls and
     * makes first call to getNotifications().
     */
    ngOnInit() {
        const entityName = parent.Xrm.Page.data.entity.getEntityName();
        if (entityName === 'account') {
            this._cub_notificationService.customerId = parent.Xrm.Page.data.entity.getId();
        } else {
            const customerControl = parent.Xrm.Page.getAttribute('parentcustomerid');
            if (customerControl && customerControl.getValue()) {
                this._cub_notificationService.customerId = customerControl.getValue()[0].id;
            }
        }

        const selectedContact = entityName === 'contact'
            ? parent.Xrm.Page.data.entity.getId()
            : 'contact-all';
        this.initControls(selectedContact);
        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(constants => this.processConstants(constants));

        const done = this._loadingScreen.add();

        // Retrieve all contacts related to the customer. If contactId was 
        // passed as a query param then there is no reason to retrieve other
        // contact options
        let getChannels, getContacts;
        //if (!queryParams['contactId']) {
        if (entityName === 'account') {
            getContacts = this._cub_notificationService.getContactsRelatedToCustomer(this._cub_notificationService.customerId);
        }
        getChannels = this._cub_notificationService.getEnabledNotificationChannels();

        // if getContacts is undefined, it acts as a promise that immediately 
        // resolves, and contacts will be undefined
        Promise.all([getChannels, getContacts])
            .then(([channels, contacts]) => {
                this.processChannels(channels);
                if (!!contacts) {
                    this.processContacts(contacts);
                }
                this.getNotifications();
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error loading notification history'))
            .then(() => done())
            .then(() => this.initComplete = true);
    }

    /**
     * Callback that parses form controls for channel filtering and resending
     * @param channels an array of enabled channels
     */
    processChannels(channels: Model.Channel[]) {
        if (channels.length === 1) {
            this.channelControl.value = 'channel-' + channels[0].channel;
            this.multipleChannels = false;
        } else if (channels.length > 1) {
            this.multipleChannels = true;
        }

        this.channelControl.controls = _.concat<Model.Cub_Radio>(this.channelControl.controls,
            _.map(channels, c => ({
                id: 'channel-' + c.channel,
                label: c.channel === 'PUSHNOTIFICATION'
                    ? 'Push'
                    : _.capitalize(c.channel),
                radioClass: 'form-field-width-25'
            })));

        this.resendChannelControl.controlsOrder = _.map(channels, c => c.channel);
        this.resendChannelControl.controls = _.reduce(channels, (accu, c) => {
            accu[c.channel] = {
                name: 'resend-channel-' + c.channel,
                id: c.channel,
                label: c.channel === 'PUSHNOTIFICATION'
                    ? 'Push'
                    : _.capitalize(c.channel),
                checkboxClass: 'icon-checkbox-hybrid'
            };
            return accu;
        }, {});
    }

    /**
     * Callback that sets configurable properties for the notification history UI
     * @param constants an object containing any relevant constants
     */
    processConstants(constants: Model.Cub_Globals) {
        this.dateRangeOffset = constants['CUB.MSD.Web.Angular']['NotificationHistory.StartDateOffset'] as number;
        this.rowsPerPage = constants['CUB.MSD.Web.Angular']['NotificationHistory.NumRows'] as number;
    }

    /**
     * Callback that parses form controls related to contacts
     * @param contacts an array of MSD contacts
     */
    processContacts(contacts: Model.Contact[]) {
        let contactOptions: Model.Cub_DropdownOption[];

        this.multipleContacts = contacts.length > 1;
        contactOptions = _.map(contacts, c => ({
            name: `${c.firstName} ${c.lastName}`,
            value: c.contactId
        }));
        this.contactControl.options = this.contactControl.options.concat(contactOptions);
        this.resendContactControl.options = contactOptions;
    }

    /**
     * Assemble all search criteria and call the service to get notifications.
     * If the date range is invalid, displays an error and exits;
     */
    getNotifications() {
        let params: Model.NotificationSearchCriteria = {
            customerId: this._cub_notificationService.customerId,
            offset: this.pagingOffset,
            limit: this.rowsPerPage,
            sortBy: this.sortBy
        };

        // apply search bar filter as either a reference number or type;
        // search by notification reference if the filter contains a number or asterisk
        if (!!this.searchFilter) {
            if (this.searchFilter.match(/\d|\*/)) {
                params.notificationReference = this.searchFilter;
            } else {
                params.type = this.searchFilter;
            }
        }

        // apply date range filter
        if (!!this.startDateFilter) {
            params.startDate = this.startDateFilter;
            if (!!this.endDateFilter) {
                params.endDate = this.endDateFilter;
            }
        }

        // apply contact filter
        if (this.contactControl.value !== 'contact-all') {
            params.contactId = this.contactControl.value;
        }

        // apply channel filter
        if (this.channelControl.value !== 'channel-all') {
            params.channel = this.channelControl.value.substr(8);
        }

        // apply status filter
        if (this.statusControl.value !== 'status-all') {
            params.status = this.statusControl.value.substr(7);
        }

        const done = this._loadingScreen.add('Searching');
        this._cub_notificationService.getCustomerNotifications(params)
            .then(data => {
                this.customerNotificationResults = data;
                this.totalRecordCount = this._cub_notificationService.totalRecordCount;
            })
            .catch(err => this._cub_dataService.prependCurrentError(err))
            .then(() => done());
    }

    /**
     * Fires whenever the PrimeNG grid sort or page changes. This allows the
     * user to make a new request.
     * @param event contains sorting and paging info
     */
    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.disableSearch = false;
        }
    }

    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableSearch = false;
    }

    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableSearch = false;
    }

    /**
     * Fires when the search box text is modified or the search button is clicked.
     * This sets the search filter or calls getNotifications().
     * @param event A tuple whose first part is the form control value and whose second part is the control object
     */
    onSearchControlChanged([input, control]) {
        this.disableSearch = !input && !this.searchFilter;
        this.searchFilter = this.searchControl.value;
    }

    /**
     * Generic onChange function that enables the search button. Fires when the
     * contact, channel, or status controls change.
     */
    onControlChanged() {
        this.disableSearch = false;
    }

    /**
     * Fires when the search button is clicked. Calls getNotifications().
     */
    onSearchClicked() {
        if (this.initComplete) {
            this.getNotifications();
            this.disableSearch = true;
        }
    }

    /**
     * Fires when the enter key is pressed while any of search, start date, or
     * end date controls are in focus. Calls getNotifications() and shifts focus
     * to the search control.
     */
    onEnterKey() {
        if (this.initComplete && !this.disableSearch) {
            this.getNotifications();
            this.disableSearch = true;
            document.getElementById(this.searchControl.id).focus();
        }
    }

    /**
     * Fires when a row is clicked within the notification history table. This
     * calls the service to get the selected notification's detail, prepares
     * the data to display within the detail modal window, and resets the resend
     * modal window controls.
     * @param event event.data is the selected notification record
     */
    onRowDoubleClicked(event: any) {
        let notification = <Model.NotificationRecord>event.data;

        // The selected notification detail data is already loaded.
        if (!!this._cub_notificationService.selected && notification.notificationId === this._cub_notificationService.selected.notificationId) {
            this.showNotificationDetail = true;
            return;
        }

        this._cub_notificationService.selected = notification;
        this.resetSelectedResendChannels();
        this.resendChannelControl.controls[notification.channel].value = true;
        this.selectedResendChannels.add(notification.channel);

        const done = this._loadingScreen.add('Loading customer notification details');
        this._cub_notificationService.getCustomerNotificationDetail()
            .then(detail => {
                this.resendContactControl.value = detail.contactId;
                (notification.channel === 'EMAIL')
                    ? this.setEmailMessageHtml()
                    : this.setSmsPushMessageHtml();
                this.showNotificationDetail = true;
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error loading customer notification details'))
            .then(() => done());

        this.formatDetailDateTimes();
    }

    /**
     * Prepare the two date fields for notification detail modal window. Displays
     * the date and time corresponding to the notification's status.
     */
    formatDetailDateTimes() {
        let notification = this._cub_notificationService.selected,
            statusDateTime = moment(notification.status === 'SENT' ? notification.sendDateTime : notification.failDateTime),
            createDateTime = moment(notification.createDateTime);
        this.statusDate = statusDateTime.format('M/D/YYYY');
        this.statusTime = statusDateTime.format('h:mm A');
        this.processedDate = createDateTime.format('M/D/YYYY');
        this.processedTime = createDateTime.format('h:mm A');
    }

    /**
     * Hides the notification detail modal window.
     */
    closeDetail() {
        this.showNotificationDetail = false;
    }

    /**
     * Parses the HTML content of an email message and injects it into the
     * notification detail modal window.
     */
    setEmailMessageHtml() {
        let message = document.createElement('html'),
            div = document.createElement('div'),
            messageBody;
        message.innerHTML = this._cub_notificationService.selectedDetail.message;
        messageBody = message.querySelector('tbody');
        div.innerHTML = messageBody.innerHTML;
        document.getElementById('notification-detail-message').innerHTML = div.outerHTML;
    }

    /**
     * Parses the SMS or push notification message and injects it into the
     * notification detail modal window.
     */
    setSmsPushMessageHtml() {
        let div = document.createElement('div'),
            p = document.createElement('p');
        p.innerText = `"${this._cub_notificationService.selectedDetail.alternateMessage}"`;
        div.appendChild(p);
        document.getElementById('notification-detail-message').innerHTML = div.outerHTML;
    }

    /**
     * Fires on click of the notification detail modal window Resend button.
     * If the user can select from multiple channels and/or contacts, it displays
     * the resend notification modal window, otherwise it resends as-is.
     */
    promptResend() {
        this.showNotificationDetail = false;
        if (this.multipleChannels || this.multipleContacts) {
            this.showResend = true;
        } else {
            this.resend();
        }
    }

    /**
     * Fires when a resend notification modal window channel option is (un)checked.
     * This maintains the set of channels along which to resend the notification.
     * @param event the checkbox control object
     */
    onResendChannelChanged(event: any) {
        (event.value)
            ? this.selectedResendChannels.add(event.id)
            : this.selectedResendChannels.delete(event.id);
    }

    /**
     * Clears the set of channels for resending a notification, and unchecks all
     * the channel control checkboxes.
     */
    resetSelectedResendChannels() {
        this.selectedResendChannels.clear();
        this.resendChannelControl.controlsOrder.forEach(c => {
            this.resendChannelControl.controls[c].value = false;
        });
    }

    /**
     * Calls the service to resend the notification on all selected channels.
     * On error, attempts to display the error(s) as pop-up alert(s).
     */
    resend() {
        let channelPromises = _.map(Array.from(this.selectedResendChannels), c => this._cub_notificationService.resend(c));

        const done = this._loadingScreen.add('Resending...');
        Promise.all(channelPromises)
            .then(() => {
                this.showResend = false;
                this._cub_dataService.busyScreen.icon = 'check';
                this._cub_dataService.busyScreen.busyText = 'Resent';
                ++this._cub_dataService.busyScreen.busyCount;
                setTimeout(() => {
                    --this._cub_dataService.busyScreen.busyCount;
                }, 500);
            })
            .catch(err => this._cub_dataService.prependCurrentError('Resending error'))
            .then(() => done());
    }

    /**
     * Hides the resend notification modal window.
     */
    cancelResend() {
        this.showResend = false;
    }

    /**
     * Sets the contactId for resending a notification.
     */
    onResendContactChanged() {
        this._cub_notificationService.resendContactId = this.resendContactControl.value;
    }

    /**
     * Prepares all form controls
     */
    initControls(selectedContact) {
        this.statusControl = {
            id: 'status-control-radiolist',
            group: 'statusControl',
            label: 'Status',
            value: 'status-all',
            isButton: true,
            controls: [
                {
                    id: 'status-all',
                    label: 'All',
                    radioClass: 'form-field-width-25'
                }, {
                    id: 'status-sent',
                    label: 'Sent',
                    radioClass: 'form-field-width-25'
                }, {
                    id: 'status-failed',
                    label: 'Failed',
                    radioClass: 'form-field-width-25'
                }
            ]
        };
        this.searchControl = {
            isTableControl: false,
            formFieldClass: 'form-fields-siblings',
            fieldWidthClass: 'form-field-width-80',
            includeLabel: true,
            id: 'searchControl',
            label: 'Search',
            value: '',
            placeholder: 'Type or Reference #...',
            maxLength: 40
        };

        /* The following four controls' options require web service calls */

        // contactControl.value is set to a contact ID from the query params if
        // viewing on a contact landing page, otherwise it defaults to all for 
        // customer landing page
        this.contactControl = {
            formFieldClass: 'form-field-drop-down',
            fieldWidthClass: 'form-field-width-L',
            id: 'contact-control-dropdown',
            label: 'Contact',
            value: selectedContact,
            options: [
                {
                    name: 'All',
                    value: 'contact-all'
                }
            ]
        };
        this.channelControl = {
            id: 'channel-control-radiolist',
            group: 'channelControl',
            label: 'Channel',
            value: 'channel-all',
            isButton: true,
            controls: [
                {
                    id: 'channel-all',
                    label: 'All',
                    radioClass: 'form-field-width-25'
                }
            ]
        };
        this.resendContactControl = {
            formFieldClass: 'form-fields-siblings',
            fieldWidthClass: 'form-field-width-100',
            isTableControl: true,
            id: 'resend-contact-control-dropdown',
            label: 'Contact',
            value: '',
            options: []
        };
        this.resendChannelControl = {
            id: 'resend-channel-control-checkboxlist',
            noSubClass: true,
            controls: {},
            controlsOrder: []
        }
    }
}