import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_Button, Cub_Textbox, Cub_Dropdown, Cub_Checkbox } from '../model';
import { DataTableModule, SharedModule, TooltipModule } from 'primeng/primeng';
import * as lodash from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'cub-customer-order',
    templateUrl: './cub-customer-order.component.html',
    styleUrls: ['./cub-customer-order.component.css'],
    providers: [Cub_CustomerOrderService]
})
export class Cub_CustomerOrderComponent implements OnInit {

    @Input() accountId: string;
    @Input() orderId: string;
    @Output() onClose = new EventEmitter<void>();

    orderTotals: any[];
    orderResults: any[];
    orderResultsLineItems: any[];
    orderResultsLineItemsLength: number;
    order: any;

    constructor(
        private _cub_customerOrderService: Cub_CustomerOrderService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        let queryParams = this.route.snapshot.queryParams;
        if (this.accountId == null || this.orderId == null) {
            this.accountId = queryParams['accountId'];
            this.orderId = queryParams['orderId'];

            if (this.accountId == null || this.orderId == null) {
                return;
            }
        }

        const done = this._loadingScreen.add();
        this._cub_customerOrderService.getOrder(this.accountId, this.orderId)
            .then((data: any) => {
                if (data) {
                    this.orderResults = data;
                    this.order = data.order;
                    this.orderResultsLineItems = data.order.lineItems;
                    if (data.order.lineItems) {
                        this.orderResultsLineItemsLength = data.order.lineItems.length;
                    }
                    this.orderTotals = [{
                        itemsSubtotalAmount: data.order.itemsSubtotalAmount,
                        orderTotalAmount: data.order.orderTotalAmount
                    }];
                }
            })
            .catch((error: any) => {
                this.onClose.emit();
            })
            .then(() =>done());
    }

    closeDetail() {
        this.onClose.emit();
    }
}
