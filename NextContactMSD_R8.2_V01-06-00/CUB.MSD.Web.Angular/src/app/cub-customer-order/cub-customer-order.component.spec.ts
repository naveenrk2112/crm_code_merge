import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, Injectable } from '@angular/core';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { RouterTestingModule } from "@angular/router/testing";
import { AppRoutingComponent } from '../app-routing/app-routing.component';
import { AppLandingComponent } from '../app-landing.component';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_CustomerOrderComponent } from './cub-customer-order.component';
import { DataTableModule, SharedModule, TooltipModule } from 'primeng/primeng';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { APP_BASE_HREF, Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Cub_CustomerOrderService } from '../services/cub_customerorder.service';

import { ActivatedRouteStub } from '../testutilities/route.stub';
import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

//Directives
import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';

import { MomentDatePipe } from '../pipes/moment-date.pipe';
import { MoneyPipe } from '../pipes/money.pipe';

describe('CubCustomerOrderComponent', () => {
  let fixture: ComponentFixture<Cub_CustomerOrderComponent>,
      comp: Cub_CustomerOrderComponent,
      service: Cub_CustomerOrderService,
      dataService: Cub_DataService,
      root: DebugElement,
      http: HttpTestingController;

  beforeEach(async(() => {
      TestBed.configureTestingModule({
          imports: [
              HttpClientTestingModule,
              BrowserModule,
              FormsModule,
              ReactiveFormsModule,
              DataTableModule,
              TooltipModule,
              RouterTestingModule.withRoutes([{ path: 'CustomerOrder', component: Cub_CustomerOrderComponent }]),
          ],
          declarations: [
              Cub_CustomerOrderComponent,
              MomentDatePipe,
              MoneyPipe,
              Cub_TooltipDirective
          ],
          providers: [
              DataTableModule,
              { provide: Cub_DataService, useClass: Cub_DataServiceStub },
              { provide: ActivatedRoute, useClass: ActivatedRouteStub },
              { provide: APP_BASE_HREF, useValue: '/' },
              { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub }
          ]
      }).overrideComponent(Cub_CustomerOrderService, {
          set: {
              providers: [
                  { provide: Cub_CustomerOrderService, useClass: Cub_CustomerOrderService }
              ]
          }
        }).compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Cub_CustomerOrderComponent);
      comp = fixture.componentInstance;
      root = fixture.debugElement;
      service = root.injector.get(Cub_CustomerOrderService);
      dataService = TestBed.get(Cub_DataService);
  });

  it('should be created', () => {
      expect(comp).toBeTruthy();
  });
});
