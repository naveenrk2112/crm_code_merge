import { Component, Input, OnInit, ChangeDetectorRef, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as Model from '../model';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { Cub_CustomizableGridComponent } from '../controls/cub-customizablegrid/cub_customizablegrid.component';
import { Cub_DateRangeComponent } from '../controls/cub-date-range/cub-date-range.component';
import { Cub_MultiDropdown } from '../controls/cub-multi-dropdown/cub-multi-dropdown.component';

import * as moment from 'moment-timezone';
import * as _ from 'lodash';
import { LazyLoadEvent } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'cub_ta_tapshistory',
    templateUrl: './cub_ta_tapshistory.component.html',
    styleUrls: ['../../svg-temp.css'], // TODO: remove when refresh icon updated
    providers: [Cub_TransitAccountService]
})
export class Cub_TA_TapsHistoryComponent implements OnInit {

    //@todo: remove the values below
    @Input() transitAccountId: string = "";
    @Input() subsystemId: string = "";

    customerId: string = null;

    @ViewChild(Cub_CustomizableGridComponent)
    grid: Cub_CustomizableGridComponent;

    @ViewChild(Cub_DateRangeComponent)
    dateRange: Cub_DateRangeComponent;

    @ViewChildren(Cub_MultiDropdown)
    multiDropdowns: QueryList<Cub_MultiDropdown>;

    tapsHistory: any[] = [];
    dateOffset: number = 0;

    tapTypeOptions: Model.Cub_MultiDropdownOption[];
    tapStatusOptions: Model.Cub_MultiDropdownOption[];
    operatorOptions: Model.Cub_MultiDropdownOption[];
    travelModeOptions: Model.Cub_MultiDropdownOption[];
    tapTypeValue: string = '';
    tapStatusValue: string = '';
    operatorValue: string = '';
    travelModeValue: string = '';

    disableSearch: boolean = true;
    showReset: boolean = false;

    initialPass: boolean = true;
    rowsPerPage: number = 5; //default
    initComplete: boolean = false;

    /* Filter values */
    startDateFilter: string;
    endDateFilter: string;

    /*Pagination and Sorting params*/
    sortBy: string;
    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    showDetail: boolean = false;
    selectedTripId: any = null;

    showTapCorrection: boolean = false;
    showVoidTap: boolean = false;
    selectedTap: any = null;

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_transitAccountService: Cub_TransitAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private _changeDetector: ChangeDetectorRef
    ) { }

    ngOnInit() {
        if (!this.transitAccountId) {
            this.transitAccountId = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
        }
        if (!this.subsystemId) {
            const subsystemControl = parent.Xrm.Page.getAttribute('cub_subsystemid');
            if (subsystemControl) {
                this.subsystemId = subsystemControl.getValue();
            }
        }

        //Load configuration data for the page
        const globals$ = this._cub_dataService.Globals.first(globals => !!globals['CUB.MSD.Web.Angular']);
        const grids$ = this._cub_dataService.GridConfigs.first(configs => !!configs['TA.TapsHistory']);
        const travelModes$ = this._cub_transitAccountService.getTravelModes(this.subsystemId);
        const travelOperators$ = this._cub_transitAccountService.getTravelOperators(this.subsystemId);
        const tapTypes$ = this._cub_transitAccountService.getTapTypes(this.subsystemId);
        const tapStatuses$ = this._cub_transitAccountService.getTapStatuses(this.subsystemId);
        const done = this._loadingScreen.add();
        Observable.forkJoin(globals$, grids$, travelModes$, travelOperators$, tapTypes$, tapStatuses$)
            .finally(() => done())
            .subscribe(([globals, grids, travelModes, operators, tapTypes, tapStatuses]) => {
                this.dateOffset = globals['CUB.MSD.Web.Angular']['TA.TapsHistory.StartDateOffset'] as number;
                let grid = grids['TA.TapsHistory'];
                this.rowsPerPage = grid.rowsToDisplay;

                this.travelModeOptions = _.map(travelModes, t => ({
                    // 02-07-2018: NIS throws exception when sending travelModeId, but not travelModeEnum
                    value: t.travelModeEnum,//t.travelModeId,
                    label: t.travelModeDesc
                }));

                // operators may contain option 'All Operators', so we remove it
                this.operatorOptions = _.chain(operators)
                    .filter(o => !o.shortDesc.match(/^all([^a-z]|$)/i))
                    .map(o => ({
                        value: o.operatorId,
                        label: o.shortDesc
                    }))
                    .value();

                // 02-07-2018: NIS throws exception when sending tapTypeId, but not tapTypeName
                this.tapTypeOptions = _.map(tapTypes, t => ({
                    value: t.tapTypeName,//t.tapTypeId,
                    label: t.tapTypeName
                }));

                this.tapStatusOptions = _.map(tapStatuses, t => ({
                    value: t.tapStatusId,
                    label: t.tapStatusDescription
                }));

                this.getTapsHistory();
            });
    }

    //Handle grid loading on paging
    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.disableSearch = false;
            this.getTapsHistory();
        }
    }

    //Retrieve paged-portion of tap history data
    getTapsHistory() {
        let offset = (this.pagingOffset > 0) ? this.pagingOffset : 0;

        const done = this._loadingScreen.add('Loading Taps History...');

        const params = {
            accountId: this.transitAccountId,
            subSystem: this.subsystemId,
            operatorList: this.operatorValue,
            startDateTime: this.startDateFilter,
            endDateTime: this.endDateFilter,
            travelMode: this.travelModeValue,
            status: this.tapStatusValue,
            eventList: this.tapTypeValue,
            sortBy: this.sortBy,
            offset: offset,
            limit: this.rowsPerPage
        };

        this._cub_transitAccountService.getTapsHistory(params)
            .then((data) => {
                if (data) {
                    this.tapsHistory = data.lineItems;
                    this.totalRecordCount = data.totalCount;
                    this.disableSearch = true;
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting taps history');
            })
            .then(() => this.initComplete = true)
            .then(() => done());
    }

    /**
     * Fires when the start date text is modified or a date is selected using the
     * date picker. This sets the start date filter used in getBalanceHistory()
     * and determines whether the end date control should be disabled.
     */
    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableSearch = false;
        this.showReset = this.initComplete;
    }

    /**
     * Fires when the end date text is modified or a date is selected using the
     * date picker. This sets the end date filter used in getBalanceHistory().
     */
    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableSearch = false;
        this.showReset = this.initComplete;
    }

    /**
     * Fires when the enter key is pressed while any of search, start date, or
     * end date controls are in focus. Calls getTapsHistory() and shifts focus
     * to the search control.
     */
    onEnterKey() {
        if (!this.disableSearch) {
            this.getTapsHistory();
            this.disableSearch = true;
        }
    }

    /**
    * Fires when the reset button is clicked.
    * This resets filters/sort and then calls getTapsHistory().
    */
    reset() {
        this.dateRange.reset();
        this.multiDropdowns.forEach(md => md.reset());

        this.sortBy = this.grid.control.defaultSort;
        this.grid.reset()
            .then(() => this.showReset = false);
    }

    /**
    * Generic onChange function that enables the search button. Fires when a
    * dropdown control changes.
    */
    onMultiDropdownChanged(event, fieldName) {
        this[fieldName] = event;
        this.disableSearch = false;
        this.showReset = this.initComplete;
    }

    /**
    * Fires when a row is clicked within the taps history table. This
    * calls the service to get the select tap history's detail, prepares
    * the data to display within the detail modal window, and resets the resend
    * modal window controls.
    * @param event event.data is the selected balance record
    */
    onRowDblClick(event: any) {
        this.selectedTripId = event.data.tripId;
        this.showDetail = true;
    }

    hideDetail() {
        this.showDetail = false;
    }

    hideTapCorrection() {
        this.showTapCorrection = false;
        //this.getTapsHistory();
    }

    hideVoidTap() {
        this.showVoidTap = false;
        //this.getTapsHistory();
    }

    onCorrectTapActionClicked(tapEntry) {
        if (parent.Xrm.Page.getAttribute('cub_customer').getValue()) {
            this.customerId = parent.Xrm.Page.getAttribute('cub_customer').getValue()[0].id;
        }
        this.selectedTap = tapEntry;
        this.showTapCorrection = true;
    }

    onVoidTapActionClicked(tapEntry) {
        if (parent.Xrm.Page.getAttribute('cub_customer').getValue()) {
            this.customerId = parent.Xrm.Page.getAttribute('cub_customer').getValue()[0].id;
        }
        this.selectedTap = tapEntry;
        this.showVoidTap = true;
    }
}