import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { DataTableModule, SharedModule } from 'primeng/primeng';
import { TextMaskModule } from 'angular2-text-mask';
import { NguiDatetimePickerModule, NguiDatetime } from '@ngui/datetime-picker';

import { Cub_CustomizableGridComponent } from '../controls/cub-customizablegrid/cub_customizablegrid.component'
import { Cub_TA_TapsHistoryComponent } from '../cub-ta-tapshistory/cub_ta_tapshistory.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import { Cub_TextboxComponent } from '../controls/cub-textbox/cub_textbox.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_OptionSelectComponent } from '../controls/cub-optionselect/cub_optionselect.component';
import { Cub_HelpComponent } from '../controls/cub-help/cub_help.component';
import { Cub_ButtonComponent } from '../controls/cub-button/cub_button.component';
import { Cub_MultiDropdown, } from '../controls/cub-multi-dropdown/cub-multi-dropdown.component';
import { Cub_DateRangeComponent } from '../controls/cub-date-range/cub-date-range.component';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { MomentDatePipe } from '../pipes/moment-date.pipe';
import { MoneyPipe } from '../pipes/money.pipe';
import { PhonePipe } from '../pipes/phone.pipe';

import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';
import { Cub_MenuToggleDirective } from '../cub-menu-toggle/cub-menu-toggle.directive';

import { WSGetCustomerOrderHistoryResponse } from '../model';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_TransitAccountServiceStub } from '../testutilities/cub_transitaccount.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';

import * as moment from 'moment';

describe('Cub_TransitAccountTapsHistoryComponent', () => {
    pending();
    const TAPS_HISTORY_RESPONSE_BODY: any = {
        Body: '{"totalCount":1,"lineItems":[{"tapId":"100","tripId":"200","operator":"Operator","operatorDescription":"MTA","transactionDateTime":"2017-11-13T18:25:43.511Z","recordedDateTime":"2017-11-13T18:25:43.511Z","event":"Event","eventDescription":"Tap","type":"Type","typeDescription":"Entry","travelMode":"TravelMode","travelModeDescription":"Rail","token":{"tokenType":"Bankcard","maskedPAN":"1111"},"deviceId":"BMV00001","stopPoint":"Stop Point","stopPointDescription":"Roosevekt Ave & 74th St.","zone":"300","routeNumber":"400","status":"Status","statusDescription":"Device Approved","isCorrectable":true,"processingStatus":"Unmatched","travelPresenceIndicator":"Manual-Filled"}]}'
    };

    let component: Cub_TA_TapsHistoryComponent;
    let fixture: ComponentFixture<Cub_TA_TapsHistoryComponent>;
    let service: Cub_TransitAccountService;
    let dataService: Cub_DataService;
    let root: DebugElement;
    let table;

    // frequently-invoked sequence of component initialization steps
    let INIT = () => {
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                DataTableModule,
                TextMaskModule,
                NguiDatetimePickerModule
            ],
            declarations: [
                Cub_MenuToggleDirective,
                Cub_TA_TapsHistoryComponent,
                Cub_CustomizableGridComponent,
                Cub_DropdownComponent,
                Cub_TextboxComponent,
                Cub_ControlLabelComponent,
                Cub_OptionSelectComponent,
                Cub_HelpComponent,
                Cub_ButtonComponent,
                Cub_DateRangeComponent,
                MomentDatePipe,
                MoneyPipe,
                PhonePipe,
                Cub_MultiDropdown,
                Cub_TooltipDirective
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub }
            ]
        }).overrideComponent(Cub_TA_TapsHistoryComponent, {
            set: {
                providers: [
                    { provide: Cub_TransitAccountService, useClass: Cub_TransitAccountServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_TA_TapsHistoryComponent);
        component = fixture.componentInstance;
        root = fixture.debugElement;
        service = root.injector.get(Cub_TransitAccountService);
        dataService = TestBed.get(Cub_DataService);
        table = root.nativeElement;
        Cub_TransitAccountServiceStub._tapsHistoryResponse = TAPS_HISTORY_RESPONSE_BODY;
    });

    it('should be created', fakeAsync(() => {
        INIT();
        expect(component).toBeTruthy();
    }));

    it('initial load of data', fakeAsync(() => {
        component.subsystemId = '123';
        component.transitAccountId = '456';
        INIT();
        tick();
        fixture.whenStable().then(() => {
            expect(component.tapsHistory).toEqual(JSON.parse(TAPS_HISTORY_RESPONSE_BODY.Body).lineItems);
            expect(component.totalRecordCount).toEqual(JSON.parse(TAPS_HISTORY_RESPONSE_BODY.Body).totalCount);
        });

    }));
});
