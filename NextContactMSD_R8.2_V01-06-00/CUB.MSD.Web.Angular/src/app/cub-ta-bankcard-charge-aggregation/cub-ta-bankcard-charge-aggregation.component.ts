import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import * as Model from '../model';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { CubTaBankcardChargeHistoryService } from '../services/cub_ta_bankcard_charge_history.service';

import * as moment from 'moment';
import * as lodash from 'lodash';
import { LazyLoadEvent} from 'primeng/primeng';

@Component({
  selector: 'cub-ta-bankcard-charge-aggregation',
  templateUrl: './cub-ta-bankcard-charge-aggregation.component.html',
  providers: [CubTaBankcardChargeHistoryService]
})
export class CubTaBankcardChargeAggregationComponent implements OnInit {
    @Input() subsystemId: string = "";
    @Input() transitAccountId: string = "";
    @Input() paymentId: string = "";
    @Output() closeClicked = new EventEmitter();

    bankcardCharges: any;
    aggregatedEntries: any;

    rowsPerPage: number = 5; //default
    initComplete: boolean = false;

    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cubTaBankcardChargeHistoryService: CubTaBankcardChargeHistoryService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        const queryParams = this.route.snapshot.queryParamMap;
        if (queryParams.has('transitAccountId')) {
            this.transitAccountId = queryParams.get('transitAccountId');
        }
        if (queryParams.has('subsystemId')) {
            this.subsystemId = queryParams.get('subsystemId');
        }
        if (queryParams.has('paymentId')) {
            this.paymentId = queryParams.get('paymentId');
        }
        this._cub_dataService.GridConfigs
            .first(configs => !!configs['TransitAccount.BankCardChargeAggregation'])
            .subscribe((configs) => {
                var grid = configs['TransitAccount.BankCardChargeAggregation'];
                this.rowsPerPage = grid.rowsToDisplay;
            });
    }

    onLazyLoad(event: LazyLoadEvent) {
        this.getBankcardChargeAggregation();
    }

    getBankcardChargeAggregation() {
        const done = this._loadingScreen.add('Loading Bankcard Charge Aggregation...');
        this._cubTaBankcardChargeHistoryService.getBankcardChargeAggregation({
            subSystem: this.subsystemId,
            accountId: this.transitAccountId,
            paymentId: this.paymentId
        })
        .then((data) => {
            if (data) {
                var data = JSON.parse(data.Body);
                this.aggregatedEntries = data.aggregatedEntries;

                for (var i = 0; i < this.aggregatedEntries.length; i++) {
                    this.aggregatedEntries[i]["tripStartDateTime"] = moment(this.aggregatedEntries[i]["tripStartDateTime"]).format('MM/DD/YYYY h:mm A');
                    this.aggregatedEntries[i]["tripEndDateTime"] = moment(this.aggregatedEntries[i]["tripEndDateTime"]).format('MM/DD/YYYY h:mm A');
                }

                this.bankcardCharges = data.bankcardCharge;
            }
        })
        .catch((error: any) => {
            this._cub_dataService.prependCurrentError('Error getting bankcard charge Aggregation');
            })
        .then(() => this.initComplete = true)
        .then(() => done());
    }

    close() {
        this.closeClicked.emit();
    }
}
