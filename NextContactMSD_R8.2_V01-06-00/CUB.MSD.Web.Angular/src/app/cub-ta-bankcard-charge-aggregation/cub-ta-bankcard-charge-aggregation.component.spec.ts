import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubTaBankcardChargeAggregationComponent } from './cub-ta-bankcard-charge-aggregation.component';

describe('CubTaBankcardAggregationComponent', () => {
  let component: CubTaBankcardChargeAggregationComponent;
  let fixture: ComponentFixture<CubTaBankcardChargeAggregationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubTaBankcardChargeAggregationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubTaBankcardChargeAggregationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
