﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes, Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from "@angular/router/testing";
import { AppRoutingComponent } from '../app-routing/app-routing.component';
import { AppLandingComponent } from '../app-landing.component';
import { APP_BASE_HREF, Location } from '@angular/common';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_CustomerVerificationComponent } from '../cub-customerverification/cub_customerverification.component';
import { Cub_AlertComponent } from '../controls/cub-alert/cub_alert.component';
import { Cub_SecurityOptionComponent } from '../controls/cub-securityoption/cub_securityoption.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import { Cub_BalancesComponent } from '../cub-balances/cub-balances.component';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';

import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Cub_OneAccountBalancesComponent } from './cub_oneaccountbalances.component';

class MockActivatedRoute {
    queryParams = Observable.of({});
}

describe('CubOneAccountBalancesComponent', () => {

    let comp: Cub_OneAccountBalancesComponent;
    let fixture: ComponentFixture<Cub_OneAccountBalancesComponent>;
    let root: DebugElement;
    let location: Location;
    let route: MockActivatedRoute;

    function setQueryParams(data: object) {
        route.queryParams = Observable.of(data);
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpModule,
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                RouterTestingModule.withRoutes([{ path: 'OneAccountBalances', component: Cub_OneAccountBalancesComponent }]),
                DataTableModule
            ],
            declarations: [
                Cub_OneAccountBalancesComponent,
                AppRoutingComponent,
                AppLandingComponent,
                Cub_CustomerVerificationComponent,
                Cub_AlertComponent,
                Cub_SecurityOptionComponent,
                Cub_ControlLabelComponent,
                Cub_DropdownComponent,
                Cub_BalancesComponent
            ],
            providers: [
                Cub_DataService,
                { provide: ActivatedRoute, useClass: MockActivatedRoute },
                { provide: APP_BASE_HREF, useValue: '/' }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_OneAccountBalancesComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
        route = <MockActivatedRoute>TestBed.get(ActivatedRoute);
    });

    it('Should create the component', () => {
        //TODO: Create more tests
        pending();
        const app = fixture.debugElement.componentInstance;

        expect(app).toBeTruthy();
    });

    it('Should be one balance', () => {
        //TODO: Create more tests
        pending();
        comp.balances.push({
            balance: 560,
            nickname: 'Parking',
            autoload: false
        });

        fixture.detectChanges();
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li").length).toEqual(1);
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[0].querySelector("p.balance").innerHTML).toEqual("$5.60");
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li button.balance-block")[0].classList.contains("balance-positive")).toBeTruthy();
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[0].querySelector("span.restriction p").innerHTML).toEqual("Parking");
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li button.balance-block")[0].classList.contains("autoload-no")).toBeTruthy();
    });

    it('Should be three balances', () => {
        //TODO: Create more tests
        pending();
        comp.balances.push({
            balance: 560,
            nickname: 'Parking',
            autoload: false
        });
        comp.balances.push({
            balance: 0,
            nickname: 'Transit',
            autoload: false
        });
        comp.balances.push({
            balance: -200,
            nickname: 'Test',
            autoload: true
        });

        fixture.detectChanges();
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li").length).toEqual(3);
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[0].querySelector("p.balance").innerHTML).toEqual("$5.60");
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li button.balance-block")[0].classList.contains("balance-positive")).toBeTruthy();
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[0].querySelector("span.restriction p").innerHTML).toEqual("Parking");
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li button.balance-block")[0].classList.contains("autoload-no")).toBeTruthy();
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[1].querySelector("p.balance").innerHTML).toEqual("$0.00");
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li button.balance-block")[1].classList.contains("balance-neutral")).toBeTruthy();
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[1].querySelector("span.restriction p").innerHTML).toEqual("Transit");
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li button.balance-block")[1].classList.contains("autoload-no")).toBeTruthy();
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[2].querySelector("p.balance").innerHTML).toEqual("-$2.00");
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li button.balance-block")[2].classList.contains("balance-negative")).toBeTruthy();
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li")[2].querySelector("span.restriction p").innerHTML).toEqual("Test");
        expect(fixture.nativeElement.querySelectorAll(".balance-blocks li button.balance-block")[2].classList.contains("autoload-yes")).toBeTruthy();
    });
});
