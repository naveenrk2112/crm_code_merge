﻿import { Component, OnDestroy, OnInit, NgZone } from '@angular/core';

import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

@Component({
    selector: 'cub-oneaccountbalances',
    templateUrl: './cub_oneaccountbalances.component.html',
    providers: [Cub_OneAccountService]
})
export class Cub_OneAccountBalancesComponent implements OnInit, OnDestroy {
    balances: any[] = [];

    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_oneAccountBalancesService: Cub_OneAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private _zone: NgZone
    ) { 
        parent['Cubic'] = parent['Cubic'] || {};
        parent['Cubic'].account = parent['Cubic'].account || {};
        parent['Cubic'].account.balances = {
            component: this,
            zone: this._zone
        };
    }

    ngOnInit() {
        this.getBalances();
    }

    getBalances() {
        const customerId = parent.Xrm.Page.data.entity.getId();
        const done = this._loadingScreen.add('Retrieving OneAccount balances...');
        this._cub_oneAccountBalancesService.getCachedOneAccountSummary(customerId)
            .then(data => this.balances = data ? data.purse : [])
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    ngOnDestroy() {
        try {
            parent['Cubic'].account.balances = null;
        } catch (e) { }
    }

    onAddValue(balance) {
        console.log('add value clicked:', balance);
    }

    onViewAutoloads(balance) {
        console.log('view autoloads clicked:', balance);
    }
}