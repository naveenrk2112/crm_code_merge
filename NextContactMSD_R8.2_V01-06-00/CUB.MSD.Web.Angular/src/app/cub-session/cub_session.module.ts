﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { Cub_SessionComponent } from './cub-session.component';
import { MomentDatePipeModule } from '../pipes/moment-date.pipe';
import { TravelTokenIdPipeModule } from '../pipes/travel-token-id.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: Cub_SessionComponent }
        ]),
        MomentDatePipeModule,
        TravelTokenIdPipeModule
    ],
    declarations: [Cub_SessionComponent],
    exports: [Cub_SessionComponent]
})
export class Cub_SessionModule { }