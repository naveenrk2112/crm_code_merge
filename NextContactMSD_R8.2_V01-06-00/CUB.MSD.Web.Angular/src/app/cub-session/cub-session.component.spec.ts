﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { Cub_SessionModule } from './cub_session.module';
import { Cub_SessionComponent } from './cub-session.component';
import { Cub_SessionService } from '../services/cub-session.service';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_SessionServiceStub } from '../testutilities/cub_session.service.stub';
import { Cub_OneAccountServiceStub } from '../testutilities/cub_oneaccount.service.stub';
import { Cub_SubsystemServiceStub } from '../testutilities/cub_subsystem.service.stub';
import { Cub_TransitAccountServiceStub } from '../testutilities/cub_transitaccount.service.stub';
import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';
import { RouterStub } from '../testutilities/route.stub';

describe('Cub_SessionComponent', () => {
    let fixture: ComponentFixture<Cub_SessionComponent>,
        comp: Cub_SessionComponent,
        service: Cub_SessionService,
        dataService: Cub_DataService,
        root: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                Cub_SessionModule,
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: Cub_SessionService, useClass: Cub_SessionServiceStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub },
                { provide: Router, useClass: RouterStub }
            ]
        }).overrideComponent(Cub_SessionComponent, {
            set: {
                providers: [
                    { provide: Cub_OneAccountService, useClass: Cub_OneAccountServiceStub },
                    { provide: Cub_SubsystemService, useClass: Cub_SubsystemServiceStub },
                    { provide: Cub_TransitAccountService, useClass: Cub_TransitAccountServiceStub }
                ]
            }
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(Cub_SessionComponent);
            comp = fixture.componentInstance;
            root = fixture.debugElement;
            service = root.injector.get(Cub_SessionService);
            dataService = TestBed.get(Cub_DataService);
        });
    }));

    it('should create the component', async(() => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(comp).toBeTruthy();
        });
    }));
});
