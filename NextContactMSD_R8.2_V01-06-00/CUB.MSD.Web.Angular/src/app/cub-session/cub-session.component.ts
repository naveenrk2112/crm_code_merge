﻿import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_SessionService } from '../services/cub-session.service';
import { Cub_OneAccountService } from '../services/cub_oneaccount.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { Cub_TransitAccountService } from '../services/cub_transitaccount.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import * as Model from '../model';

import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'cub-session',
    templateUrl: './cub-session.component.html',
    providers: [
        Cub_OneAccountService,
        Cub_SubsystemService,
        Cub_TransitAccountService
    ]
})
export class Cub_SessionComponent implements OnInit, OnDestroy {
    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_sessionService: Cub_SessionService,
        private _cub_oneAccountService: Cub_OneAccountService,
        private _cub_subsystemService: Cub_SubsystemService,
        private _cub_transitAccountService: Cub_TransitAccountService,
        private _loadingScreen: AppLoadingScreenService,
        private _zone: NgZone
    ) {
        parent['Cubic'] = parent['Cubic'] || {};
        parent['Cubic'].session = {
            component: this,
            zone: this._zone
        };
    }

    get isActive() { return this._cub_sessionService.isActive; }
    isStandalone: boolean = true; // false if the session bar is loaded within a component like Master Search
    ivrHistoryShowAll: boolean = false;
    messagesShowAll: boolean = false;
    alertsShowAll: boolean = false;

    get session() { return this._cub_sessionService.session; }
    set session(s) { this._cub_sessionService.session = s; }
    currentRecordId: string;
    currentEntityName: string;

    oneAccountData: Model.WSOneAccountSummaryResponse;
    subsystemData: Model.WSSubsystemStatusResponse;

    readonly sessionOutOfContextMessage = 'This page is outside the context of the current session. Click OK to start a new session. Click Cancel to stay on this page or return to the previous page without ending the current session.';

    ngOnInit() {
        if (!parent.Xrm.Page.data || !parent.Xrm.Page.data.entity) {
            this.isStandalone = false;
            this.finishInit(false);
            return;
        }

        this.isStandalone = true;
        this.currentEntityName = parent.Xrm.Page.data.entity.getEntityName();
        this.currentRecordId = parent.Xrm.Page.data.entity.getId();

        if (this.checkSessionContextIsRelevant()) {
            this.finishInit(true);
            return;
        }

        // If the current record is unrelated to the session context, prompt
        // the user to end the session and begin a new one, otherwise finish
        // initialization as usual.
        const message = this.sessionOutOfContextMessage;
        parent.Xrm.Utility.confirmDialog(message,
            () => parent['Cubic'].session.zone.run(() => {
                // This callback has no reference to `this` so we use comp
                const comp = parent['Cubic'].session.component as Cub_SessionComponent;
                const done = comp.tryAddLoadingMessage('Ending session');
                comp._cub_sessionService.endSession()
                    .then(() => comp.startSession())
                    .then(() => comp.authenticateContact())
                    .catch(err => comp._cub_dataService.onApplicationError(err))
                    .then(() => done());
            }),
            () => parent['Cubic'].session.zone.run(() =>
                parent['Cubic'].session.component.finishInit(false)));
    }

    ngOnDestroy() {
        try {
            parent['Cubic'].session = null;
        } catch (e) { }
    }

    /**
     * Refresh the session bar
     * @param forceRefresh if true, make HTTP requests for data, otherwise check cache first
     */
    refresh(forceRefresh: boolean) {
        const done = this.tryAddLoadingMessage('Refreshing');
        let getSession = forceRefresh
            ? this._cub_sessionService.refreshSession()
            : this._cub_sessionService.getCachedSession();

        getSession
            .then(() => this.extractFormData())
            .then(() => this.loadData(forceRefresh))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    /**
     * Determine the current record. If no active session, gather as much data
     * as possible from the record's form.
     */
    extractFormData() {
        if (!parent.Xrm.Page.data || !parent.Xrm.Page.data.entity) {
            this.isStandalone = false;
            return;
        }
        this.isStandalone = true;

        this.currentEntityName = parent.Xrm.Page.data.entity.getEntityName();
        this.currentRecordId = parent.Xrm.Page.data.entity.getId();

        if (this.isActive) {
            return;
        }

        if (this.currentEntityName === 'account') {
            this.session.customerId = this.currentRecordId;
            const fullName: string = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
            this.session.customerFirstName = fullName.trim().split(' ')[0].trim();
            this.session.customerLastName = fullName.trim().split(' ')[1].trim();
            this.session.isRegistered = true;
            this.session.isPrimaryContact = true;
        }
        else if (this.currentEntityName === 'contact') {
            this.session.contactFirstName = parent.Xrm.Page.getAttribute('firstname').getValue();
            this.session.contactLastName = parent.Xrm.Page.getAttribute('lastname').getValue();
            this.session.contactId = this.currentRecordId;
            this.session.customerId = parent.Xrm.Page.getAttribute('parentcustomerid').getValue()[0].id;
            const fullName: string = parent.Xrm.Page.getAttribute('parentcustomerid').getValue()[0].name;
            this.session.customerFirstName = fullName.trim().split(' ')[0].trim();
            this.session.customerLastName = fullName.trim().split(' ')[1].trim();
            this.session.isRegistered = true;
            this.session.isPrimaryContact = false;
        }
        else if (this.currentEntityName === 'cub_transitaccount') {
            this.session.transitAccountId = this.currentRecordId;
            if (parent.Xrm.Page.getAttribute('cub_customer').getValue()) {
                this.session.customerId = parent.Xrm.Page.getAttribute('cub_customer').getValue()[0].id;
                const fullName: string = parent.Xrm.Page.getAttribute('cub_customer').getValue()[0].name;
                this.session.customerFirstName = fullName.trim().split(' ')[0].trim();
                this.session.customerLastName = fullName.trim().split(' ')[1].trim();
                this.session.isRegistered = true;
                this.session.isPrimaryContact = true;
            } else {
                this.session.subsystem = parent.Xrm.Page.getAttribute('cub_subsystemid').getValue();
                this.session.subsystemAccountReference = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
            }
        }
    }

    /**
     * If the session bar has either a registered customer or an unregistered
     * subsystem account in context, populate the session bar with its summary.
     * @param forceRefresh if true, make HTTP requests for data, otherwise check cache first
     */
    loadData(forceRefresh: boolean) {
        let loadMessage = forceRefresh ? 'Refreshing' : 'Loading';
        loadMessage += this.session.isRegistered ? ' OneAccount' : ' Subsystem';
        loadMessage += ' data';
        const done = this.tryAddLoadingMessage(loadMessage);

        // If the session context is a registered customer or unregistered subsystem account,
        // we request & process either OneAccount summary or Subsystem status, respectively.
        let queryPromise;
        if (this.session.isRegistered && this.session.customerId) {
            queryPromise = forceRefresh
                ? this._cub_oneAccountService.refreshOneAccountSummary(this.session.customerId)
                : this._cub_oneAccountService.getCachedOneAccountSummary(this.session.customerId);

            queryPromise = queryPromise.then(data => this.extractOneAccountData(data));
        }
        else if (!this.session.isRegistered && this.session.subsystem && this.session.subsystemAccountReference) {
            queryPromise = forceRefresh
                ? this._cub_subsystemService.refreshSubsystemStatus(this.session.subsystem, this.session.subsystemAccountReference)
                : this._cub_subsystemService.getCachedSubsystemStatus(this.session.subsystem, this.session.subsystemAccountReference);

            queryPromise = queryPromise.then(data => this.extractSubsystemData(data));
        }
        else {
            done();
            return;
        }

        queryPromise
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    /**
     * Assign a display token for each Linked Account, and sort Linked Accounts
     * @param data
     */
    extractOneAccountData(data: Model.WSOneAccountSummaryResponse) {
        this.oneAccountData = data;
        if (!this.oneAccountData.linkedAccounts) {
            return;
        }

        const len = this.oneAccountData.linkedAccounts.length;
        for (let i = 0; i < len; i++) {
            const tokens = this.oneAccountData.linkedAccounts[i].subsystemAccountDetailedInfo.tokens;
            this.oneAccountData.linkedAccounts[i].displayToken = this._cub_oneAccountService.getDisplayToken(tokens);
        }

        // Sort accounts by status (Active, Closed, Suspended, Terminated) and
        // by most-recently used token.
        this.oneAccountData.linkedAccounts = _.orderBy(this.oneAccountData.linkedAccounts,
            [a => a.subsystemAccountDetailedInfo.accountStatus, a => a.displayToken.tokenInfo.lastUseDTM],
            ['asc', 'desc']);
    }

    /**
     * Assign a display token and determine last usage details
     * @param data
     */
    extractSubsystemData(data: Model.WSSubsystemStatusResponse) {
        this.subsystemData = data;
        if (!this.subsystemData.accountLastUsageDetails && !this.subsystemData.tokens) {
            return;
        }

        this.subsystemData.displayToken = this._cub_oneAccountService.getDisplayToken(this.subsystemData.tokens);

        // The subsystem account and its related tokens all have last usage details.
        // Display whichever is most recent.
        if (this.subsystemData.accountLastUsageDetails && this.subsystemData.displayToken) {
            const tokenLastUse = this.subsystemData.displayToken.tokenLastUsageDetails.transactionDateTime;
            const accountLastUse = this.subsystemData.accountLastUsageDetails.transactionDateTime;
            if (moment(accountLastUse).isAfter(tokenLastUse)) {
                this.subsystemData.displayToken.tokenLastUsageDetails = this.subsystemData.accountLastUsageDetails;
            }
        }
    }

    /**
     * TODO: remove once everything is implemented.
     * Placeholder method to use until something is implemented
     * @param event
     */
    unimplemented(event: Event) {
        event.preventDefault();
        alert('This function is unimplemented');
    }

    /**
     * Create a session record and load additional data based on its context
     */
    startSession() {
        const done = this.tryAddLoadingMessage('Starting new session');
        return this._cub_sessionService.createSession(this.currentEntityName, this.currentRecordId)
            .then(() => this.loadData(false))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    /**
     * End a session and update the session bar context to match the current page
     */
    endSession() {
        const done = this.tryAddLoadingMessage('Ending session');
        this._cub_sessionService.endSession()
            .then(() => {
                this.extractFormData();
                this.loadData(true);
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    /**
     * Navigate to the customer record if not already there
     */
    customerClicked() {
        if (!this.session.customerId) {
            this._cub_dataService.onApplicationError('Cannot navigate to customer record, no customer ID found!');
            return;
        }
        if (this._cub_dataService.guidsAreEqual(this.session.customerId, this.currentRecordId)) {
            return;
        }
        this._cub_dataService.openMsdForm('account', this.session.customerId);
    }

    /**
     * Navigate to the contact record if not already there
     */
    contactClicked() {
        if (!this.session.contactId) {
            this._cub_dataService.onApplicationError('Cannot navigate to contact record, no contact ID found!');
            return;
        }
        if (this._cub_dataService.guidsAreEqual(this.session.contactId, this.currentRecordId)) {
            return;
        }
        this._cub_dataService.openMsdForm('contact', this.session.contactId);
    }

    /**
     * Navigate to the subsystem account record if not already there
     * @param subsystem
     * @param subsystemAccountReference
     */
    subsystemAccountClicked(subsystem, subsystemAccountReference) {
        if (parent.Xrm.Page.data && this.currentEntityName === Model.cub_TransitAccountLogicalName) {
            const currentAccountRef = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
            const currentSubsystem = parent.Xrm.Page.getAttribute('cub_subsystemid').getValue();
            if (currentAccountRef === subsystemAccountReference
                && currentSubsystem === subsystem) {
                return;
            }
        }

        const done = this.tryAddLoadingMessage('Retrieving account');
        this._cub_transitAccountService.getTransitAccount(subsystemAccountReference, subsystem, this.session.customerId)
            .then(transitAccount => {
                if (transitAccount && !this._cub_dataService.guidsAreEqual(transitAccount.Id, this.currentRecordId)) {
                    this._cub_dataService.openMsdForm(Model.cub_TransitAccountLogicalName, transitAccount.Id);
                }
            })
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    /**
     * Navigate to Customer Registration if not already there or on Master Search
     */
    registerClicked() {
        if (!this.isStandalone) {
            return;
        }
        const data = encodeURIComponent(JSON.stringify({
            route: 'CustomerRegistration',
            queryParams: encodeURIComponent([
                'subsystem=', this.session.subsystem,
                '&subsystemAccountReference=', this.session.subsystemAccountReference
            ].join(''))
        }));
        const url = window.location.origin + window.location.pathname + '?pagemode=iframe&Data=' + data;
        parent.location.href = url;
    }

    /**
     * Determine whether the session's context is relevant to the current MSD
     * page by checking the session and related records against the displaying record.
     * @returns false if the current page is unrelated to the current session, true otherwise.
     */
    checkSessionContextIsRelevant(): boolean {
        // Return early if within another component (like Master Search),
        // there is no active session, or the active session is not yet associated
        // with a registered customer or unregistered subsystem account.
        if (!this.isStandalone
            || !this.isActive
            || (!this.session.customerId
                && !this.session.transitAccountId)
        ) {
            return true;
        }

        if (this.currentEntityName === 'account') {
            return this.session.isRegistered
                && this._cub_dataService.guidsAreEqual(this.session.customerId, this.currentRecordId);
        }
        else if (this.currentEntityName === 'contact') {
            if (!this.session.isRegistered) {
                return false;
            }
            const customerControl = parent.Xrm.Page.getAttribute('parentcustomerid');
            if (!customerControl || !customerControl.getValue()) {
                return false;
            }
            const customerId = customerControl.getValue()[0].id;
            return this._cub_dataService.guidsAreEqual(this.session.customerId, customerId);
        }
        else if (this.currentEntityName = 'cub_transitaccount') {
            if (!this.session.isRegistered) {
                return this._cub_dataService.guidsAreEqual(this.session.transitAccountId, this.currentRecordId);
            } else {
                const customerControl = parent.Xrm.Page.getAttribute('cub_customer');
                if (!customerControl || !customerControl.getValue()) {
                    return false;
                }
                const customerId = customerControl.getValue()[0].id;
                return this._cub_dataService.guidsAreEqual(this.session.customerId, customerId);
            }
        }

        return true;
    }

    /**
     * If there is an active session, load data from session context.
     * If there is not an active session, try to start a new session
     * with the current record as context.
     * @param shouldAuthenticate if true, attempt to trigger contact authentication process
     */
    finishInit(shouldAuthenticate: boolean) {
        if (this.isActive) {
            this.loadData(false);
            if (shouldAuthenticate) {
                this.authenticateContact();
            }
            return;
        }

        if (this.currentEntityName === 'account'
            || this.currentEntityName === 'contact'
            || this.currentEntityName === 'cub_transitaccount'
        ) {
            this.startSession().then(() => {
                if (shouldAuthenticate) {
                    this.authenticateContact();
                }
            });
        }
    }

    /**
     * If the session bar is attached to another component like Master Search,
     * don't display any loading messages to reduce screen clutter.
     * @param message
     * @returns a function to remove the loading message or a no-op
     */
    tryAddLoadingMessage(message: string): () => void {
        if (!this.isStandalone) {
            return () => { }; // no-op
        } else {
            return this._loadingScreen.add(message);
        }
    }

    /**
     * Update the current session.
     * Invoked by MSD form scripts for contact and account.
     * @param session an object containing any MSD attributes to be updated
     */
    update(session) {
        session.Id = this.session.sessionId;
        const done = this.tryAddLoadingMessage('Updating');
        this._cub_sessionService.updateSession(session)
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }

    /**
     * Call a form script function to check if the current contact needs to be verified.
     */
    authenticateContact() {
        if (this.currentEntityName === 'account') {
            parent['Cubic'].account.authenticateContact();
        } else if (this.currentEntityName === 'contact') {
            parent['Cubic'].contact.authenticateContact();
        }
    }

    showSessionDetailsClicked(sessionId: string,) {
       
        let dialogOptions = new parent.Xrm['DialogOptions']();
        let data = encodeURIComponent(JSON.stringify({
            route: 'SessionDetails',
            queryParams: [
                'sessionId=', sessionId
                //'sessionId=DAEB341B-292D-E811-80F9-005056814569'
            ].join('')
        }));

        let modalUrl = window.location.origin + window.location.pathname + '?Data=' + data;
        dialogOptions.width = 1280;
        dialogOptions.height = 780;

        parent.Xrm['Internal'].openDialog(modalUrl, dialogOptions, null, null, (response) => {
            if (response == null) {
                return;
            }
            try {
                parent['Cubic'].account.subsystemAccounts.zone.run(() => {
                    parent['Cubic'].account.subsystemAccounts.component.onModalClose(response);
                });
            } catch (e) {
                console.warn('Unable to handle modal close callback', e);
            }
        });
    }
}