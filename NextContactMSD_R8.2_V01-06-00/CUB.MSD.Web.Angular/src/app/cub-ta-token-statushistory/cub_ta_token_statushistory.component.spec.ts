import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cub_TA_Token_StatusHistoryComponent } from './cub_ta_token_statushistory.component';

describe('Cub_TA_Token_StatusHistoryComponent', () => {
    let component: Cub_TA_Token_StatusHistoryComponent;
    let fixture: ComponentFixture<Cub_TA_Token_StatusHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [Cub_TA_Token_StatusHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Cub_TA_Token_StatusHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
