import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { DataTableModule, SharedModule } from 'primeng/primeng';
import { TextMaskModule } from 'angular2-text-mask';
import { NguiDatetimePickerModule, NguiDatetime } from '@ngui/datetime-picker';

import { Cub_CustomizableGridComponent } from '../controls/cub-customizablegrid/cub_customizablegrid.component'
import { Cub_TA_TripsHistoryComponent } from '../cub-ta-tripshistory/cub_ta_tripshistory.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import { Cub_TextboxComponent } from '../controls/cub-textbox/cub_textbox.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_OptionSelectComponent } from '../controls/cub-optionselect/cub_optionselect.component';
import { Cub_HelpComponent } from '../controls/cub-help/cub_help.component';
import { Cub_ButtonComponent } from '../controls/cub-button/cub_button.component';
import { Cub_MultiDropdown, } from '../controls/cub-multi-dropdown/cub-multi-dropdown.component';
import { Cub_DateRangeComponent } from '../controls/cub-date-range/cub-date-range.component';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_TA_TripsHistoryService } from '../services/cub_ta_tripshistory.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { MomentDatePipe } from '../pipes/moment-date.pipe';
import { MoneyPipe } from '../pipes/money.pipe';
import { PhonePipe } from '../pipes/phone.pipe';

import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';
import { Cub_MenuToggleDirective } from '../cub-menu-toggle/cub-menu-toggle.directive';

import { WSGetCustomerOrderHistoryResponse } from '../model';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_TA_TripsHistoryServiceStub } from '../testutilities/cub_ta_tripshistory.service.stub';
import { ActivatedRouteStub } from '../testutilities/route.stub';


import * as moment from 'moment';

describe('Cub_TransitAccountTripsHistoryComponent', () => {
    pending();
    const TRIPS_HISTORY_RESPONSE_BODY: any = {
        Body: '{"totalCount":10,"lineItems":[{"tripId":"200","journeyId":"300","startDateTime":"2017-11-13T18:25:43.511Z","endDateTime":"2017-11-13T18:25:43.511Z","startLocationDescription":"Roosevekt Ave & 74th St.","endLocationDescription":"Roosevekt Ave & 74th St.","travelMode":"Rail","travelModeDescription":"Rail","token":{"tokenType":"Bankcard","maskedPAN":"1111"},"productDescription":"Weekly Pass","totalFare":275,"unpaidFare":50,"tripType":"Trip Type","tripTypeDescription":"Trip Type Description","tripStatus":"Finalized","tripStatusDescription":"Finalized","tripStatusDateTime":"2017-11-13T18:25:43.511Z","travelPresenceIndicator":"Manual-Filled","isCorrectable":false,"riderClass":"Adult","riderClassDescription":"Adult","timeCategory":"Off-Peak","timeCategoryDescription":"Off-Peak","concession":"Full Fare","concessionDescription":"Full Fare"}]}'
    };

    let component: Cub_TA_TripsHistoryComponent;
    let fixture: ComponentFixture<Cub_TA_TripsHistoryComponent>;
    let service: Cub_TA_TripsHistoryService;
    let dataService: Cub_DataService;
    let root: DebugElement;
    let table;

    // frequently-invoked sequence of component initialization steps
    let INIT = () => {
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                DataTableModule,
                TextMaskModule,
                NguiDatetimePickerModule
            ],
            declarations: [
                Cub_MenuToggleDirective,
                Cub_TA_TripsHistoryComponent,
                Cub_CustomizableGridComponent,
                Cub_DropdownComponent,
                Cub_TextboxComponent,
                Cub_ControlLabelComponent,
                Cub_OptionSelectComponent,
                Cub_HelpComponent,
                Cub_ButtonComponent,
                Cub_DateRangeComponent,
                MomentDatePipe,
                MoneyPipe,
                PhonePipe,
                Cub_MultiDropdown,
                Cub_TooltipDirective
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                AppLoadingScreenService,
                { provide: ActivatedRoute, useClass: ActivatedRouteStub }
            ]
        }).overrideComponent(Cub_TA_TripsHistoryComponent, {
            set: {
                providers: [
                    { provide: Cub_TA_TripsHistoryService, useClass: Cub_TA_TripsHistoryServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_TA_TripsHistoryComponent);
        component = fixture.componentInstance;
        root = fixture.debugElement;
        service = root.injector.get(Cub_TA_TripsHistoryService);
        dataService = TestBed.get(Cub_DataService);
        table = root.nativeElement;
        Cub_TA_TripsHistoryServiceStub._tripsHistoryResponse = TRIPS_HISTORY_RESPONSE_BODY;
    });

    it('should be created', fakeAsync(() => {
        INIT();
        expect(component).toBeTruthy();
    }));

    it('initial load of data', fakeAsync(() => {
        component.subsystemId = '123';
        component.transitAccountId = '456';
        INIT();
        tick();
        expect(component.indicatorControl).toBeDefined();
        expect(component.showControl).toBeDefined();
        expect(component.viewTypeControl).toBeDefined();
        expect(component.travelModeControl).toBeDefined();
        expect(component.statusControl).toBeDefined();
        fixture.whenStable().then(() => {
            expect(component.sortedTripsHistory).toEqual(JSON.parse(TRIPS_HISTORY_RESPONSE_BODY.Body).lineItems);
            expect(component.totalRecordCount).toEqual(JSON.parse(TRIPS_HISTORY_RESPONSE_BODY.Body).totalCount);
        });

    }));
});
