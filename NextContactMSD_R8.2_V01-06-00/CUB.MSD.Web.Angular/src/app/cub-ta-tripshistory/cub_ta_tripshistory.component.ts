import { Component, Input, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as Model from '../model';
import { Cub_DataService } from '../services/cub_data.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_TA_TripsHistoryService } from '../services/cub_ta_tripshistory.service';
import { Cub_CustomizableGridComponent } from '../controls/cub-customizablegrid/cub_customizablegrid.component';

import * as moment from 'moment';
import * as lodash from 'lodash';
import { LazyLoadEvent } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'cub_ta_tripshistory',
    templateUrl: './cub_ta_tripshistory.component.html',
    providers: [Cub_TA_TripsHistoryService]
})
export class Cub_TA_TripsHistoryComponent implements OnInit {

    @Input() subsystemId: string = "";
    @Input() transitAccountId: string = "";

    customerId: string = null;

    @ViewChild(Cub_CustomizableGridComponent)
    grid: Cub_CustomizableGridComponent;

    tripsHistory: Array<object> = [];
    sortedTripsHistory: Array<object> = [];
    dateOffset: number = 0;

    travelModeControl: {
        value: string,
        options: Array<Model.Cub_MultiDropdownOption>
    };
    statusControl: {
        value: string,
        options: Array<Model.Cub_MultiDropdownOption>
    };
    indicatorControl: {
        value: string,
        options: Array<Model.Cub_MultiDropdownOption>
    };
    showControl: {
        value: string,
        options: Array<Model.Cub_MultiDropdownOption>
    };
    viewTypeControl: {
        value: string,
        options: Array<Model.Cub_MultiDropdownOption>
    };

    disableSearch: boolean = true;

    initialPass: boolean = true;
    rowsPerPage: number = 5; //default
    initComplete: boolean = false;

    /* Filter values */
    startDateFilter: string;
    endDateFilter: string;

    /*Pagination and Sorting params*/
    sortBy: string;
    pagingOffset: number = 0;
    totalRecordCount: number = 0;

    showDetail: boolean = false;
    selectedTripId: any = null;
    showVoidTrip: boolean = false;

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cub_TA_TripsHistoryService: Cub_TA_TripsHistoryService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private _changeDetector: ChangeDetectorRef
    ) { }

    ngOnInit() {
        if (!this.transitAccountId) {
            this.transitAccountId = parent.Xrm.Page.data.entity.getPrimaryAttributeValue();
        }
        if (!this.subsystemId) {
            const subsystemControl = parent.Xrm.Page.getAttribute('cub_subsystemid');
            if (subsystemControl) {
                this.subsystemId = subsystemControl.getValue();
            }
        }

        if (!this.transitAccountId) {
            this._cub_dataService.onApplicationError('no acct ref');
            return;
        }
        if (!this.subsystemId) {
            this._cub_dataService.onApplicationError('no subsystem');
            return;
        }

        this.travelModeControl = {
            value: '',
            options: []
        };

        this.statusControl = {
            value: '',
            options: []
        };

        this.indicatorControl = {
            value: '',
            options: []
        };

        this.showControl = {
            value: '',
            options: []
        };

        this.viewTypeControl = {
            value: '',
            options: []
        };

        //Load globals for the page
        const globals$ = this._cub_dataService.Globals.first(globals => !!globals['CUB.MSD.Web.Angular']);
        const grids$ = this._cub_dataService.GridConfigs.first(configs => !!configs['TA.TripsHistory']);
        const done = this._loadingScreen.add();
        Observable.forkJoin(globals$, grids$)
            .finally(() => done())
            .subscribe(([globals, grids]) => {
                this.dateOffset = globals['CUB.MSD.Web.Angular']['TA.TripsHistory.StartDateOffset'] as number;

                var travelModeOptionsString = globals['CUB.MSD.Web.Angular']['TA.TripsHistory.TravelModeOptions'] as string;
                var travelModeOptions = travelModeOptionsString ? travelModeOptionsString.split(';') : [];
                for (var i = 0; i < travelModeOptions.length; i++) {
                    if (travelModeOptions[i].indexOf(':') === -1) continue;

                    var keyValuePair = travelModeOptions[i].split(':');
                    if (keyValuePair.length !== 2) continue;
                    this.travelModeControl.options.push({
                        label: keyValuePair[0],
                        value: keyValuePair[1],
                        checked: true
                    } as Model.Cub_MultiDropdownOption);
                }

                var statusOptionsString = globals['CUB.MSD.Web.Angular']['TA.TripsHistory.StatusOptions'] as string;
                var statusOptions = statusOptionsString ? statusOptionsString.split(';') : [];
                for (var i = 0; i < statusOptions.length; i++) {
                    if (statusOptions[i].indexOf(':') === -1) continue;

                    var keyValuePair = statusOptions[i].split(':');
                    if (keyValuePair.length !== 2) continue;
                    this.statusControl.options.push({
                        label: keyValuePair[0],
                        value: keyValuePair[1],
                        checked: true
                    } as Model.Cub_MultiDropdownOption);
                }

                var indicatorOptionsString = globals['CUB.MSD.Web.Angular']['TA.TripsHistory.IndicatorOptions'] as string;
                var indicatorOptions = indicatorOptionsString ? indicatorOptionsString.split(';') : [];
                for (var i = 0; i < indicatorOptions.length; i++) {
                    if (indicatorOptions[i].indexOf(':') === -1) continue;

                    var keyValuePair = indicatorOptions[i].split(':');
                    if (keyValuePair.length !== 2) continue;
                    this.indicatorControl.options.push({
                        label: keyValuePair[0],
                        value: keyValuePair[1],
                        checked: true
                    } as Model.Cub_MultiDropdownOption);
                }

                var showOptionsString = globals['CUB.MSD.Web.Angular']['TA.TripsHistory.ShowOptions'] as string;
                var showOptions = showOptionsString ? showOptionsString.split(';') : [];
                for (var i = 0; i < showOptions.length; i++) {
                    if (showOptions[i].indexOf(':') === -1) continue;

                    var keyValuePair = showOptions[i].split(':');
                    if (keyValuePair.length !== 2) continue;
                    this.showControl.options.push({
                        label: keyValuePair[0],
                        value: keyValuePair[1],
                        checked: true
                    } as Model.Cub_MultiDropdownOption);
                }

                var viewTypeOptionsString = globals['CUB.MSD.Web.Angular']['TA.TripsHistory.ViewTypeOptions'] as string;
                var viewTypeOptions = viewTypeOptionsString ? viewTypeOptionsString.split(';') : [];
                for (var i = 0; i < viewTypeOptions.length; i++) {
                    if (viewTypeOptions[i].indexOf(':') === -1) continue;

                    var keyValuePair = viewTypeOptions[i].split(':');
                    if (keyValuePair.length !== 2) continue;
                    this.viewTypeControl.options.push({
                        label: keyValuePair[0],
                        value: keyValuePair[1],
                        checked: true
                    } as Model.Cub_MultiDropdownOption);
                }

                const grid = grids['TA.TripsHistory'];
                this.rowsPerPage = grid.rowsToDisplay;
                this.sortBy = grid.defaultSort;
                this.getTripsHistory();
            });
    }

    //Handle grid loading on paging
    onLazyLoad(event: LazyLoadEvent) {
        if (this.initComplete) {
            this.pagingOffset = event.first;
            this.sortBy = this._cub_dataService.formatSortQueryParam(event.multiSortMeta);
            this.disableSearch = false;
            this.getTripsHistory();
        }
    }

    //Retrieve paged-portion of tap history data
    getTripsHistory() {
        let subSystemId = this.subsystemId;
        let accountId = this.transitAccountId;
        let startDateTime = this.startDateFilter;
        let endDateTime = this.endDateFilter;
        let travelMode = this.travelModeControl.value || null;
        let tripCategory = this.indicatorControl.value || null;
        let viewType = this.viewTypeControl.value || null;
        let tripStatus = this.statusControl.value || null;
        let sortBy = this.sortBy || null;
        let offset = (this.pagingOffset > 0) ? this.pagingOffset : 0;
        let limit = this.rowsPerPage;

        //TODO: Show filter and Indicator Filter multiselect dropdown have no matching NIS filter

        const done = this._loadingScreen.add('Loading Trips History...');

        this._cub_TA_TripsHistoryService.getTripsHistory({
            subSystemId: subSystemId,
            accountId: accountId,
            startDateTime: startDateTime,
            endDateTime: endDateTime,
            tripStatus: tripStatus,
            travelMode: travelMode,
            tripCategory: tripCategory,
            viewType: viewType,
            sortBy: sortBy,
            offset: offset,
            limit: limit
        })
            .then((data) => {
                if (data) {
                    var tripsHistory = JSON.parse(data.Body);
                    this.tripsHistory = tripsHistory.lineItems;
                    this.sortedTripsHistory = tripsHistory.lineItems;
                    this.totalRecordCount = tripsHistory.totalCount;
                    this.disableSearch = true;
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting trips history');
            })
            .then(() => this.initComplete = true)
            .then(() => done());
    }

    /**
     * Fires when the start date text is modified or a date is selected using the
     * date picker. This sets the start date filter used in getBalanceHistory()
     * and determines whether the end date control should be disabled.
     */
    onStartDateChanged(startDate) {
        this.startDateFilter = startDate;
        this.disableSearch = false;
    }

    /**
     * Fires when the end date text is modified or a date is selected using the
     * date picker. This sets the end date filter used in getBalanceHistory().
     */
    onEndDateChanged(endDate) {
        this.endDateFilter = endDate;
        this.disableSearch = false;
    }

    /**
     * Fires when the reset button is clicked.
     * This resets filters/sort and then calls getTapsHistory().
     */
    onResetClicked() {
        this.indicatorControl.options = [].concat(this.indicatorControl.options);
        this.indicatorControl.value = null;
        this.showControl.options = [].concat(this.showControl.options);
        this.showControl.value = null;
        this.statusControl.options = [].concat(this.statusControl.options);
        this.statusControl.value = null;
        this.travelModeControl.options = [].concat(this.travelModeControl.options);
        this.travelModeControl.value = null;
        this.viewTypeControl.options = [].concat(this.viewTypeControl.options);
        this.viewTypeControl.value = null;

        const tempOffset = this.dateOffset;
        this.dateOffset = null;
        this._changeDetector.detectChanges(); // necessary to reset date filter for now
        this.dateOffset = tempOffset;

        this.sortBy = this.grid.control.defaultSort;
        this.grid.reset()
        this.getTripsHistory();
    }

    /**
    * Fires when the refresh button is clicked.
    * This then calls getTapsHistory().
    */
    onRefreshClicked() {
        this.getTripsHistory();
    }

    /**
     * Fires when the enter key is pressed while any of search, start date, or
     * end date controls are in focus. Calls getTapsHistory() and shifts focus
     * to the search control.
     */
    onEnterKey() {
        if (!this.disableSearch) {
            this.getTripsHistory();
            this.disableSearch = true;
        }
    }

    /**
    * Fires when the search button is clicked.
    * This validates and then calls getTapsHistory().
    */
    onSearchClicked() {
        this.getTripsHistory();
    }

    /**
    * Generic onChange function that enables the search button. Fires when a
    * dropdown control changes.
    */
    onMultiDropdownChanged(event, control) {
        this[control].value = event;
        this.disableSearch = false;
    }

    /**
    * Fires when a row is clicked within the taps history table. This
    * calls the service to get the select trip history's detail, prepares
    * the data to display within the detail modal window, and resets the resend
    * modal window controls.
    * @param event event.data is the selected balance record
    */
    onRowDblClick(event: any) {
        this.selectedTripId = event.data.tripId;
        this.showDetail = true;
    }

    hideDetail() {
        this.showDetail = false;
    }

    hideVoidTrip() {
        this.showVoidTrip = false;
        //this.getTripsHistory();
    }

    onVoidTripActionClicked(tripId) {
        if (parent.Xrm.Page.getAttribute('cub_customer').getValue()) {
            this.customerId = parent.Xrm.Page.getAttribute('cub_customer').getValue()[0].id;
        }
        this.selectedTripId = tripId;
        this.showVoidTrip = true;
    }
}