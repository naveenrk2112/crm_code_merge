﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as Model from '../../model';

@Component({
    selector: '[cub-checkboxlist]',
    templateUrl: './cub_checkboxlist.component.html'
})

export class Cub_CheckboxlistComponent {
    @Input() control: Model.Cub_Checkboxlist = {
        id: '',
        controls: {}, //Properties vary based on the control type, look at actual control components for details
        controlsOrder: [], //Order to show controls
        label: '', //Label for the control group
        noSubClass: false, // if true, don't add a class to each cub-checkbox <li>
    };
    //Emits
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>(); //When input changes

    inputChange(data: any) {
        this.onChange.emit(data);
    }

    startsWithFormFieldWidth(key: string) {
        return (this.control.controls[key].checkboxClass.startsWith("form-field-width"));
    }
}