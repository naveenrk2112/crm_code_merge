﻿import { Component, EventEmitter, Input, OnInit, OnChanges, Output, SimpleChanges } from '@angular/core';

import { Cub_Textbox } from '../../model';

import * as moment from 'moment';

/**
 * Use Case - automatically update component date filters:
 * <cub-date-range
 *      [(start)]="myStartDateString"
 *      [(end)]="myEndDateString"
 *      [offset]="myOffset"></cub-date-range>
 * 
 * -> this.myStartDateString and this.myEndDateString are always valid NIS date
 * strings or null
 *
 * Use Case - perform additional logic when date filters change:
 * <cub-date-range
 *      (startChange)="startDateChanged($event)"
 *      (endChange)="endDateChanged($event)"
 *      [offset]="myOffset"></cub-date-range>
 * 
 * -> $event is the updated valid NIS date string or null
 */

@Component({
    selector: 'cub-date-range',
    templateUrl: './cub-date-range.component.html'
})
export class Cub_DateRangeComponent implements OnInit, OnChanges {

    /**
     * Notifies parent of changes to start date. Can be monitored using either
     * [(start)]="myStartDate" to update parent value directly or
     * (startChange)="onDateChanged($event)" to notify parent of updated value.
     */
    @Input() start: string;
    @Output() startChange = new EventEmitter<string>();

    /**
     * Notifies parent of changes to end date. Can be monitored using either
     * [(end)]="myEndDate" to update parent value directly or
     * (endChange)="onDateChanged($event)" to notify parent of updated value.
     */
    @Input() end: string;
    @Output() endChange = new EventEmitter<string>();

    /**
     * Optional start date offset. If supplied, the end date defaults to the
     * current date, and the start date defaults to offset days before end date.
     */
    @Input() offset: number;

    /**
     * If true, user cannot enter only one date. Clearing one field clears
     * the other. If both are cleared, entering one field defaults the other
     * to the same date.
     */
    @Input() requireBoth: boolean = false;

    /**
     * Optional ENTER key event listener.
     */
    @Output() onEnterKey = new EventEmitter<void>();

    startDateControl: Cub_Textbox = {
        placeholder: 'MM/DD/YYYY',
        formFieldClass: 'form-field-text',
        fieldWidthClass: 'form-field-width-100',
        isVerticalStackedLabel: false,
        id: 'start-date-control',
        mask: [/[01]/, /\d/, '/', /[0-3]/, /\d/, '/', /[12]/, /\d/, /\d/, /\d/],
        placeholderChar: '_',
        dateConfiguration: {
            showDatePicker: true,
            dateFormat: 'MM/DD/YYYY'
        },
    };

    endDateControl: Cub_Textbox = {
        placeholder: 'MM/DD/YYYY',
        formFieldClass: 'form-field-text',
        fieldWidthClass: 'form-field-width-100',
        isVerticalStackedLabel: false,
        id: 'end-date-control',
        mask: [/[01]/, /\d/, '/', /[0-3]/, /\d/, '/', /[12]/, /\d/, /\d/, /\d/],
        placeholderChar: '_',
        dateConfiguration: {
            showDatePicker: true,
            dateFormat: 'MM/DD/YYYY'
        }
    };

    ngOnInit() {
        if (this.offset) {
            this.reset();
        } 
    }

    /**
     * The start date offset is typically retrieved asynchronously from globals.
     * This lifecycle event fires whenever offset changes and resets the range.
     * @param changes will contain offset if it changed
     */
    ngOnChanges(changes: SimpleChanges) {
        if (changes.offset) {
            this.reset();
        }
    }

    /**
     * Fires when the start date text is modified or a date is selected using the
     * date picker. This emits the new start date string and determines whether
     * the end date should change.
     */
    onStartDateChanged() {
        if (!this.startDateControl.value) {
            this.startChange.emit(null);
            if (this.requireBoth) {
                this.endDateControl.value = null;
                this.endChange.emit(null);
            }
        } else if (this.startDateControl.value.indexOf(this.startDateControl.placeholderChar) === -1) {
            let startDate = moment(this.startDateControl.value, 'MM/DD/YYYY');
            if (!startDate.isValid()) {
                return;
            }

            this.startChange.emit(startDate.toISOString());

            if ((!this.endDateControl.value && this.requireBoth)
                || (this.endDateControl.value && moment(this.endDateControl.value, 'MM/DD/YYYY').isBefore(startDate, 'day'))) {
                this.endDateControl.value = this.startDateControl.value;
                this.onEndDateChanged();
            }
        }
    }

    /**
     * Fires when the end date text is modified or a date is selected using the
     * date picker. This emits the new end date string and determines whether
     * the start date should change.
     */
    onEndDateChanged() {
        if (!this.endDateControl.value) {
            this.endChange.emit(null);
            if (this.requireBoth) {
                this.startDateControl.value = null;
                this.endChange.emit(null);
            }
        } else if (this.endDateControl.value.indexOf(this.endDateControl.placeholderChar) === -1) {
            let endDate = moment(this.endDateControl.value, 'MM/DD/YYYY');
            if (!endDate.isValid()) {
                return;
            }

            // effectively setting timestamp to 23:59:59.999
            this.endChange.emit(endDate.add(1, 'd').subtract(1, 'ms').toISOString());

            // endDate2 is necessary because add()/subtract() above manipulate endDate object
            const endDate2 = moment(this.endDateControl.value, 'MM/DD/YYYY');
            if ((!this.startDateControl.value && this.requireBoth)
                || (this.startDateControl.value && moment(this.startDateControl.value, 'MM/DD/YYYY').isAfter(endDate2, 'day'))) {
                this.startDateControl.value = this.endDateControl.value;
                this.onStartDateChanged();
            }
        }
    }

    reset() {
        if (this.offset) {
            const startDate = moment().startOf('day').subtract(this.offset, 'days');
            const endDate = moment().startOf('day');
            this.startDateControl.value = startDate.format('MM/DD/YYYY');
            this.endDateControl.value = endDate.format('MM/DD/YYYY');
            this.startChange.emit(startDate.toISOString());
            this.endChange.emit(endDate.add(1, 'd').subtract(1, 'ms').toISOString());
        } else {
            this.startDateControl.value = null;
            this.endDateControl.value = null;
            this.startChange.emit(null);
            this.endChange.emit(null);
        }
    }
}