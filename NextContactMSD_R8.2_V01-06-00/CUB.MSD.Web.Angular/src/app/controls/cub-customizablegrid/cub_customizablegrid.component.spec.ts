﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { DataTableModule } from 'primeng/primeng';

import { Cub_CustomizableGridComponent } from './cub_customizablegrid.component';

import { Cub_ActionsMenuDirective } from '../../cub-actions-menu/cub-actions-menu.directive';
import { Cub_MenuToggleDirective } from '../../cub-menu-toggle/cub-menu-toggle.directive';
import { Cub_TooltipDirective } from '../../cub-tooltip/cub-tooltip.directive';

import { MoneyPipeModule } from '../../pipes/money.pipe';
import { MomentDatePipe } from '../../pipes/moment-date.pipe';
import { PhonePipe } from '../../pipes/phone.pipe';

import { Cub_DataService } from '../../services/cub_data.service';
import { Cub_DataServiceStub } from '../../testutilities/cub_data.service.stub';

import * as Model from '../../model';

describe('Cub_CustomizableGridComponent', () => {
    let component: Cub_CustomizableGridComponent;
    let fixture: ComponentFixture<Cub_CustomizableGridComponent>;
    let root: DebugElement;

    const GRID_NAME = 'GRID_NAME';

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                DataTableModule,
                MoneyPipeModule
            ],
            declarations: [
                Cub_CustomizableGridComponent,
                Cub_ActionsMenuDirective,
                Cub_MenuToggleDirective,
                Cub_TooltipDirective,
                MomentDatePipe,
                PhonePipe
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_CustomizableGridComponent);
        component = fixture.componentInstance;
        root = fixture.debugElement;

        Cub_DataServiceStub._gridConfig = {
            [GRID_NAME]: {}
        };
        component.gridName = GRID_NAME;
    });

    it('Should render', () => {
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });

    it('Control is defined', async(() => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(component.control).toBeDefined();
        });
    }));

    it('should be able to render without data', () => {
        component.data = [];
        fixture.detectChanges();
        expect(component.data).toEqual([]);
    });

    it('should render all visible columns', async(() => {
        const COLUMN_HEADER = 'Column';
        Cub_DataServiceStub._gridConfig[GRID_NAME].columns = [
            {
                displayName: COLUMN_HEADER,
                fieldName: 'field',
                visible: true
            }
        ];
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let columnHeadings = root.nativeElement.querySelectorAll('th');
            expect(columnHeadings).toBeDefined();
            expect(columnHeadings.length).toBe(1);
        });
    }));

    it('should not render an actions column if none specified', async(() => {
        Cub_DataServiceStub._gridConfig[GRID_NAME].actions = null;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let menus = root.nativeElement.querySelector('.actions-menu');
            expect(menus).toBeNull();
            let actionsColumn = root.nativeElement.querySelector('.actions');
            expect(actionsColumn).toBeNull();
        });
    }));

    it('should render an actions menu if actions are specified', async(() => {
        Cub_DataServiceStub._gridConfig[GRID_NAME].actions = [
            {
                label: 'Action',
                action: 'fakeActionFunctionName'
            }
        ];
        component.data = [{}]; // single record with no actual data
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let actionsColumn = root.nativeElement.querySelector('.actions');
            expect(actionsColumn).toBeDefined();
            let menus = root.nativeElement.querySelectorAll('.actions-menu');
            expect(menus).toBeDefined();
            expect(menus.length).toBe(1);
        });
    }));

    function getCustomizableGridData(gridname: string): any[] {
        var data = [
            {
                id: 0,
                name: 'Jenny Smith',
                age: '42',
                address: '1234 Nowhere Street Nowheresville, IL 60000',
                email: 'j.smith@nowhere.net',
                phone: '5558675309',
                accountBalance: 403.20,
                startDate: '4/15/2011'
            },
            {
                id: 1,
                name: 'John Smith',
                age: '55',
                address: '1234 Nowhere Street Nowheresville, IL 60000',
                email: 'john.smith@nowhere.net',
                phone: '5551111111',
                accountBalance: 40.20,
                startDate: '4/15/2011'
            },
            {
                id: 2,
                name: 'Benny Smith',
                age: '23',
                address: '1234 Nowhere Street Nowheresville, IL 60000',
                email: 'b.smith@nowhere.net',
                phone: '5552222222',
                accountBalance: 3.20,
                startDate: '5/4/2012'
            },
            {
                id: 3,
                name: 'Lenny Smith',
                age: '12',
                address: '1234 Nowhere Street Nowheresville, IL 60000',
                email: 'l.smith@nowhere.net',
                phone: '5553333333',
                accountBalance: 4.20,
                startDate: '8/1/2014'
            },
            {
                id: 4,
                name: 'Rob Montague',
                age: '35',
                address: '525 W. Monroe St. Chicago, IL 60661',
                email: 'rmontague@sonomapartners.com',
                phone: '5551234567',
                accountBalance: 43.20,
                startDate: '10/5/2013 4:34:00 PM'
            },
            {
                id: 5,
                name: 'Logan M',
                age: '3',
                address: '555 Nowhere street Nowheresville, IL',
                email: 'loganm@nowhere.com',
                phone: '5553333333',
                accountBalance: 55.20,
                startDate: '10/26/2014 8:00:00 AM'
            },
            {
                id: 6,
                name: 'Luke Skywalker',
                age: '55',
                address: '123 Jedi Lane Jedi Academy Coruscant',
                email: 'lskywalker@jedi.com',
                phone: '9876543210',
                accountBalance: 999.99,
                startDate: '05/25/1977 8:30:00 PM'
            },
            {
                id: 7,
                name: 'Yoda',
                age: '850',
                address: '123 Jedi Lane Jedi Academy STE Afterlife Coruscant',
                email: 'yoda@jedi.com',
                phone: '5558545555',
                accountBalance: 100000000000.09,
                startDate: '05/21/1980 8:30:00 PM'
            }
        ];
        return data;
    }

    function getCustomizableGridColumns(gridname: string): Model.Cub_CustomizableGridWebserviceColumn[] {
        var columns = [
            {
                columnSize: 'S',
                displayName: 'Full Name',
                fieldName: 'name',
                format: 'String',
                sortable: true,
                visible: true                 
            },
            {
                columnSize: 'XXS',
                displayName: 'Age',
                fieldName: 'age',
                format: 'String',
                sortable: true,
                visible: true
            },
            {
                columnSize: 'XXL',
                displayName: 'Address',
                fieldName: 'address',
                format: 'String',
                sortable: false,
                visible: true
            },
            {
                columnSize: 'L',
                displayName: 'Email',
                fieldName: 'email',
                format: 'String',
                sortable: true,
                visible: true
            },
            {
                columnSize: 'M',
                displayName: 'Phone',
                fieldName: 'phone',
                format: 'Phone',
                sortable: false,
                visible: true
            },
            {
                columnSize: 'S',
                displayName: 'Balance',
                fieldName: 'accountBalance',
                format: 'Currency',
                sortable: true,
                visible: true
            },
            {
                columnSize: 'L',
                displayName: 'Start Date',
                fieldName: 'startDate',
                format: 'DateTime',
                sortable: true,
                visible: true
            }] as Model.Cub_CustomizableGridWebserviceColumn[];
        return columns;
    }
});
