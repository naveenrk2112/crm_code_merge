﻿import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import * as Model from '../../model';
import { Cub_DataService } from '../../services/cub_data.service';

import { LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';

@Component({
    selector: 'cub-customizablegrid',
    templateUrl: './cub_customizablegrid.component.html',
})
export class Cub_CustomizableGridComponent implements OnInit {

    constructor(private dataService: Cub_DataService) { }

    private _control: Model.Cub_CustomizableGrid;

    @Input() gridName: string;
    @Input() scope: any; // optionally assign `this` scope for custom action execution contexts

    set control(value: Model.Cub_CustomizableGrid) {
        this._control = Object.assign({
            actions: null,
            columns: [],
            formattedColumns: [],
            displayPaginator: false
        }, value);
    }
    get control() {
        return this._control;
    }

    @Input() data: any;
    @Input() totalCount: number;
    @Input() lazy: boolean = false;

    private _multiSortMeta: SortMeta[] = [];
    @Input()
    set multiSortMeta(value: SortMeta[]) {
        this._multiSortMeta = value || [];
        this.syncSortColumns();
    }
    get multiSortMeta() { return this._multiSortMeta; }

    readonly _columnFormats = Model.Cub_CustomizableGridColumnFormat;
    readonly _columnSizes = Model.Cub_CustomizableGridColumnSize;

    @Output() onLazyLoad: EventEmitter<any> = new EventEmitter();
    @Output() onRowDblClick: EventEmitter<any> = new EventEmitter();

    ngOnInit() {
        this.reset();
    }

    reset(): Promise<void> {
        return this.setupCustomizableGrid()
            .catch(err => console.error(err));
    }

    _onLazyLoad(event: LazyLoadEvent) {
        this.multiSortMeta = event.multiSortMeta;
        this.onLazyLoad.emit(event);
    }

    /**
     * Update each formatted column's sort order to match PrimeNG's multiSortMeta
     */
    private syncSortColumns() {
        if (!this.control || !this.control.formattedColumns) {
            return;
        }
        const len = this.control.formattedColumns.length;
        for (let i = 0; i < len; i++) {
            let column = this.control.formattedColumns[i];
            const columnSort = _.find(this.multiSortMeta, s => s.field === column.sortField);
            if (columnSort == null) {
                column.sortOrder = 0;
            } else {
                column.sortOrder = columnSort.order;
            }
        }
    }

    private setupCustomizableGrid() {
        return new Promise<void>((resolve, reject) => {
            if (!this.gridName) {
                reject('Cannot initialize grid, no grid name specified.');
                return;
            }
            this.dataService.GridConfigs
                .first((configs) => !!configs[this.gridName])
                .subscribe((configs) => {
                    this.control = configs[this.gridName];
                    if (this.control.maxRecords) {
                        this.totalCount = this.control.maxRecords;
                    }
                    this.parseDefaultSort();
                    this.formatColumnsFromWebservice();
                    resolve();
                });
        });
    }

    /**
     * This function converts the columns into a format that the grid knows how to parse.
     * It checks to make sure that the column size field and the field Format are able to convert to the right Enum value. If it isn't known,
     * the default value will be applied
     */
    private formatColumnsFromWebservice() {
        const colLength = this.control.columns.length;
        let columns = [] as Model.Cub_CustomizableGridColumn[];
        for (let i = 0; i < colLength; i++) {
            const webColumn = this.control.columns[i];
            let column = {
                displayName: webColumn.displayName,
                fieldName: webColumn.fieldName,
                sortable: webColumn.sortable,
                visible: webColumn.visible
            } as Model.Cub_CustomizableGridColumn;

            if (column.sortable) {
                column.sortField = webColumn.sortField || webColumn.fieldName;
            }

            // Convert columnSize to enum value
            try {
                column.columnSize = this._columnSizes[webColumn.columnSize];
            } catch (e) {
                console.warn(`unknown column size '${webColumn.columnSize}', defaulting to small (S)`);
                column.columnSize = this._columnSizes.S;
            }

            // Convert format field to enum value
            try {
                column.format = this._columnFormats[webColumn.format.toUpperCase()];
            } catch (e) {
                console.warn(`Unknown column format '${webColumn.format}', defaulting to STRING`);
                column.format = this._columnFormats.STRING;
            }
            columns.push(column);
        }
        this.control.formattedColumns = columns;
        this.syncSortColumns();
    }

    hasValue(row: any, fieldName: string) {
        return _.has(row, fieldName);
    }

    getValue(row: any, fieldName: string) {
        return _.get(row, fieldName, '\u2014');
    }

    populateCell(row: any, template: string) {
        let result = '';
        let variableValuesOnly = _.flatten(_.compact(
            _.map(
                template.match(/\{\{([^}]*)\}\}/g), (v) => {
                    return v.replace(/\}|\{/g, '').split(',');
                }
            )
        ));

        let values = _.compact(template.split(/\{\{([^}]*)\}\}/g));
        if (values.length === 1) {
            return this.getValue(row, values[0]);
        }

        for (let i = 0; i < values.length; i++) {
            var eachValue = values[i].split(',');
            for (let l = 0; l < eachValue.length; l++) {
                //If determined to be a variable, show value if found, otherwise blank if not
                //If not a varaible, show text "as is"
                var test = eachValue[l];
                if (this.hasValue(row, eachValue[l]) && variableValuesOnly.indexOf(eachValue[l]) !== -1) {
                    result += this.getValue(row, eachValue[l])
                }
                else if (variableValuesOnly.indexOf(eachValue[l]) == -1) {
                    result += eachValue[l];
                }
            }
        }

        return result;
    }

    parseDefaultSort() {
        if (!this.control.defaultSort) {
            this.multiSortMeta = [];
            return;
        }
        this.multiSortMeta = _.chain(this.control.defaultSort.split(','))
            .map(sort => sort.split('.'))
            .filter(sort => sort.length === 2)
            .map(sort => [sort[0].trim(), sort[1].trim()])
            .filter(sort => sort[0] && sort[1])
            .map(sort => ({
                field: sort[0],
                order: sort[1] === 'desc' ? -1 : 1
            }))
            .value();
    }

    /**
     * Attempt to execute a custom action on a particular record.
     * @param action {string} either a global function name or a stringified function
     * @param record {any} the record to perform the action against
     */
    executeAction(action: string, record: any) {
        if (action == null) {
            return;
        }

        let actionFn;
        let winScope = window;
        // check each window object for a globally-defined function name
        do {
            if (_.has(winScope, action)) {
                actionFn = _.get(winScope, action);
            }
            winScope = winScope.parent;
        } while (actionFn == null && winScope !== winScope.parent);

        // assume action is a stringified function
        if (actionFn == null) {
            actionFn = eval('(' + action + ')');
        }

        if (typeof actionFn === 'function') {
            actionFn.call(this.scope, record);
        } else {
            console.warn('Unable to execute custom action. Custom actions should be functions defined in a web resource or specified inline');
        }
    }
}