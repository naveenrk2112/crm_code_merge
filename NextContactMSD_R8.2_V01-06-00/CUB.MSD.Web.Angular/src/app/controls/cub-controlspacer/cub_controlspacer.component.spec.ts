﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Cub_ControlSpacerComponent } from './cub_controlspacer.component';

describe('CubControlSpacerComponent', () => {

    let comp: Cub_ControlSpacerComponent;
    let fixture: ComponentFixture<Cub_ControlSpacerComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule
            ],
            declarations: [
                Cub_ControlSpacerComponent
            ],
        });

        fixture = TestBed.createComponent(Cub_ControlSpacerComponent);
        comp = fixture.componentInstance;
    });

    it('Should create the component', async(() => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    }));

    it(`"fieldWidth" should be equal to form-field-width-25"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("li").getAttribute("class")).toEqual('form-field-width-25');
    }));

    it(`"fieldWidth" should be equal to "form-field-width-50"`, fakeAsync(() => {
        comp.fieldWidth = "50"
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("li").getAttribute("class")).toEqual('form-field-width-50');
    }));
});
