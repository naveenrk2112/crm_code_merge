﻿import {Component, Input} from '@angular/core';

@Component({
    selector: '[cub-textboxlabel]',
    template: '<li [attr.class]="generateFieldWidth(fieldWidth)"></li>'
})

//Used if a control in a column is not using 100% of it's width to take up the rest of the space (for styling purposes)
export class Cub_ControlSpacerComponent {
    @Input() fieldWidth: string = "25";

    //Helpers for templates
    generateFieldWidth(width: string) {
        return 'form-field-width-' + width;
    }
}