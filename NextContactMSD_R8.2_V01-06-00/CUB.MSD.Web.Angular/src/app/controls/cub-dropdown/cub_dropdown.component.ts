﻿import { Component, Input, Output, EventEmitter, OnInit, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'; 
import { defaults } from 'lodash';
import * as Model from '../../model';

@Component({
    selector: '[cub-dropdown]',
    templateUrl: './cub_dropdown.component.html'
})
export class Cub_DropdownComponent implements OnInit {
    @Input() control: Model.Cub_Dropdown;

    //Emits
    @Output() onChange: EventEmitter<any> = new EventEmitter();

    ngOnInit() {
        // set defaults for any unspecified fields
        defaults(this.control, <Model.Cub_Dropdown>{
            formFieldClass: '',
            labelWidthClass: '',
            fieldWidthClass: '',
            spacerWidthClass: '',
            isDisabled: false,
            isRequired: false,
            isTableControl: false,
            isSmall: false,
            errorMessage: '',
            validationMessage: '',
            showErrorMessage: false,
            id: '',
            label: '',
            value: '',
            options: [],
            isErrored: false,
            isInvalid: false
        });
    }

    //Anytime the dropdown values changes (ie. user picks an option from the list)
    dropdownChange(newValue: any): void {
        this.control.value = newValue.target.value;
        if (newValue.target.selectedIndex >= 0) {
            this.control.text = newValue.target.options[newValue.target.selectedIndex].text;
        }
        this.onChange.emit([newValue.target.value, this.control]);
    }

    //Helpers for templates
    generateFieldClass(control: any) {
        var errorClass = control.showErrorMessage
            ? 'error show-error-message'
            : control.isErrored
                ? 'error'
                : '';
        return (errorClass + ' form-field-drop-down');
    }
}

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [Cub_DropdownComponent],
    exports: [Cub_DropdownComponent]
})
export class Cub_DropdownModule { }