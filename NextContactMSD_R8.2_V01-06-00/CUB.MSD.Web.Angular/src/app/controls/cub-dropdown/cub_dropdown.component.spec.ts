﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Cub_DropdownComponent } from './cub_dropdown.component';

describe('CubDropdownComponent', () => {

    let comp: Cub_DropdownComponent;
    let fixture: ComponentFixture<Cub_DropdownComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule
            ],
            declarations: [
                Cub_DropdownComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_DropdownComponent);
        comp = fixture.componentInstance;

        //defaults
        comp.control = {
            id: '',
            isRequired: false,
            formFieldClass: '',
            labelWidthClass: '',
            fieldWidthClass: '',
            spacerWidthClass: '',
            isDisabled: false,
            isTableControl: false,
            isSmall: false,
            showErrorMessage: false,
            errorMessage: '',
            validationMessage: '',
            label: '',
            value: '',
            options: [],
            isErrored: false,
            isInvalid: false
        };
    })

    //Where isTableControl = true and isSmall = false
    it('Should create the component', async(() => {
        comp.control.isTableControl = true;
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    }));

    it(`"id" should be equal to ''"`, fakeAsync(() => {
        comp.control.isTableControl = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").id).toEqual('');
    }));

    it(`"id" should be equal to 'dropdown1'"`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.id = "dropdown1";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").id).toEqual('dropdown1');
    }));

    it(`"isRequired" set to false`, fakeAsync(() => {
        comp.control.isTableControl = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("required")).toEqual(null);
    }));

    it(`"isRequired" set to true`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        comp.control.isRequired = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("required")).toEqual("");
    }));

    it(`confirm number of options is 3`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            },
            {
                name: "Test3",
                value: "Test3"
            }
        ];
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").querySelectorAll("option").length).toEqual(3);
    }));

    it(`"Error message" set to "Test" but not visible`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.errorMessage = "Test";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p")).toBeNull();
    }));

    it(`"Error message" set to "Test" and visible`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.errorMessage = "Test";
        comp.control.isErrored = true;
        comp.control.showErrorMessage = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p").innerHTML).toEqual("Test");
    }));

    it(`No "fieldWidthClass"`, fakeAsync(() => {
        comp.control.isTableControl = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("ul li").getAttribute("class")).toEqual("");
    }));

    it(`"fieldWidthClass" to be "form-field-width-100"`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.fieldWidthClass = "form-field-width-100";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("ul li").getAttribute("class")).toEqual("form-field-width-100");
    }));

    it(`No "spacerWidthClass"`, fakeAsync(() => {
        comp.control.isTableControl = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelectorAll("ul li").length).toEqual(1);
    }));

    it(`"spacerWidthClass" defined`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.spacerWidthClass = "form-field-width-50";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelectorAll("ul li").length).toEqual(2);
    }));

    it(`No "formFieldClass"`, fakeAsync(() => {
        comp.control.isTableControl = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("ul").getAttribute("class")).toEqual("");
    }));

    it(`"formFieldClass" defined`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.formFieldClass = "form-fields-siblings";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("ul").getAttribute("class")).toEqual("form-fields-siblings");
    }));

    it(`Not disabled (with two options present)`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual(null);
    }));

    it(`disabled defined (with two options present)`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        comp.control.isDisabled = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual("");
    }));

    it(`disabled defined (with one option present)`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            }
        ];
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual("");
    }));

    it(`"value" set to "Test2"`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        comp.control.value = "Test2";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select option[value='Test2']").getAttribute("checked")).toBeDefined();
    }));

    it(`"Validation message" set to "Test" but not visible`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.validationMessage = "Test";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p")).toBeNull();
    }));

    it(`"Validation message" set to "Test" and visible`, fakeAsync(() => {
        comp.control.isTableControl = true;
        comp.control.validationMessage = "Test";
        comp.control.isInvalid = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p").innerHTML).toEqual("Test");
    }));


    //Where isTableControl = false and isSmall = false
    it('Should create the component', async(() => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    }));

    it(`"id" should be equal to ''"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").id).toEqual('');
    }));

    it(`"id" should be equal to 'dropdown1'"`, fakeAsync(() => {
        comp.control.id = "dropdown1";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").id).toEqual('dropdown1');
    }));

    it(`"label" should be null"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("label")).toBeFalsy();
    }));

    it(`"label" should be equal to 'Dropdown'"`, fakeAsync(() => {
        comp.control.label = "Dropdown";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("label").innerHTML).toEqual('Dropdown');
    }));

    it(`"isRequired" set to false`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("required")).toEqual(null);
    }));

    it(`"isRequired" set to true`, fakeAsync(() => {
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        comp.control.isRequired = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("required")).toEqual("");
    }));

    it(`confirm number of options is 3`, fakeAsync(() => {
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            },
            {
                name: "Test3",
                value: "Test3"
            }
        ];
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").querySelectorAll("option").length).toEqual(3);
    }));

    it(`"Error message" set to "Test" but not visible`, fakeAsync(() => {
        comp.control.errorMessage = "Test";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p")).toBeNull();
    }));

    it(`"Error message" set to "Test" and visible`, fakeAsync(() => {
        comp.control.errorMessage = "Test";
        comp.control.isErrored = true;
        comp.control.showErrorMessage = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p").innerHTML).toEqual("Test");
    }));

    it(`Not disabled (with two options present)`, fakeAsync(() => {
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual(null);
    }));

    it(`disabled defined (with two options present)`, fakeAsync(() => {
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        comp.control.isDisabled = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual("");
    }));

    it(`disabled defined (with one option present)`, fakeAsync(() => {
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            }
        ];
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual("");
    }));

    it(`"value" set to "Test2"`, fakeAsync(() => {
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        comp.control.value = "Test2";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select option[value='Test2']").getAttribute("checked")).toBeDefined();
    }));

    it(`"Validation message" set to "Test" but not visible`, fakeAsync(() => {
        comp.control.validationMessage = "Test";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p")).toBeNull();
    }));

    it(`"Validation message" set to "Test" and visible`, fakeAsync(() => {
        comp.control.validationMessage = "Test";
        comp.control.isInvalid = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p").innerHTML).toEqual("Test");
    }));


    //Where isTableControl = false and isSmall = true
    it('Should create the component', async(() => {
        comp.control.isSmall = true;
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    }));

    it(`"id" should be equal to ''"`, fakeAsync(() => {
        comp.control.isSmall = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").id).toEqual('');
    }));

    it(`"id" should be equal to 'dropdown1'"`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.id = "dropdown1";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").id).toEqual('dropdown1');
    }));

    it(`"isRequired" set to false`, fakeAsync(() => {
        comp.control.isSmall = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("required")).toEqual(null);
    }));

    it(`"isRequired" set to true`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        comp.control.isRequired = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("required")).toEqual("");
    }));

    it(`confirm number of options is 3`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            },
            {
                name: "Test3",
                value: "Test3"
            }
        ];
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").querySelectorAll("option").length).toEqual(3);
    }));

    it(`"Error message" set to "Test" but not visible`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.errorMessage = "Test";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p")).toBeNull();
    }));

    it(`"Error message" set to "Test" and visible`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.errorMessage = "Test";
        comp.control.isErrored = true;
        comp.control.showErrorMessage = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p").innerHTML).toEqual("Test");
    }));

    it(`Not disabled (with two options present)`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual(null);
    }));

    it(`disabled defined (with two options present)`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        comp.control.isDisabled = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual("");
    }));

    it(`disabled defined (with one option present)`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            }
        ];
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual("");
    }));

    it(`"value" set to "Test2"`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.options = [
            {
                name: "Test1",
                value: "Test1"
            },
            {
                name: "Test2",
                value: "Test2"
            }
        ];
        comp.control.value = "Test2";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("select option[value='Test2']").getAttribute("checked")).toBeDefined();
    }));

    it(`"Validation message" set to "Test" but not visible`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.validationMessage = "Test";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p")).toBeNull();
    }));

    it(`"Validation message" set to "Test" and visible`, fakeAsync(() => {
        comp.control.isSmall = true;
        comp.control.validationMessage = "Test";
        comp.control.isInvalid = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p").innerHTML).toEqual("Test");
    }));
});
