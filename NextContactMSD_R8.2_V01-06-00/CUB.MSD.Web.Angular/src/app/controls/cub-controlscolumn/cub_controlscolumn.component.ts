﻿import {Component, Input, Output, EventEmitter} from '@angular/core';

//A column that can contain various controls
@Component({
    selector: '[cub-controlscolumn]',
    templateUrl: './cub_controlscolumn.component.html'
})

export class Cub_ControlsColumnComponent {
    @Input() controls = {}; //Properties vary based on the control type, look at actual control components for details
    @Input() controlsOrder: Array<string>; //Order to show controls
    @Input() labelWidthClass;

    //Emits
    @Output() onChange: EventEmitter<any> = new EventEmitter(); //When any input changes
    @Output() onClick: EventEmitter<any> = new EventEmitter(); //When a button is clicked
    @Output() onRemove: EventEmitter<any> = new EventEmitter(); //When a 'X' is clicked next to certain controls
    @Output() onFocusOut: EventEmitter<any> = new EventEmitter(); //when a textbox loses focus
    @Output() onEnterPress: EventEmitter<any> = new EventEmitter(); //when a focused control has the enter key pressed

    //When on focus of a text box, emit this event when the 'Enter' key is pressed
    inputEnterPress(data: any) {
        this.onEnterPress.emit([data[0], data[1]]);
    }

    //When dealing with controls that can be removed, emit this event when an attempt to remove is performed
    removedClicked(event: any) {
        this.onRemove.emit([event.currentTarget.id]);
    }

    //Emit when any input control changes values
    inputChange(data: any) {
        this.onChange.emit([data[0], data[1]]);
    }

    //Emit when any text box loses focus
    inputFocusOut(data: any) {
        this.onFocusOut.emit([data[0], data[1]]);
    }

    //Emit when a button control is clicked
    buttonClicked(data: any) {
        this.onClick.emit([data[0], data[1]]);
    }
}