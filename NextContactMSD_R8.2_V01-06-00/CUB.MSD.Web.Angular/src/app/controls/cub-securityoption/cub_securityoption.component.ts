﻿// Display a verification option if it has an answer.
// In order to add proper spacing to address option, address answer is passed
// in as an array, and HTML <br /> tags are inserted between array members.
// All other answers are passed in as string.

import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as Model from '../../model'

@Component({
    selector: 'cub-securityoption',
    templateUrl: './cub_securityoption.component.html'
})

export class Cub_SecurityOptionComponent {
    @Input() securityOption: Model.Cub_SecurityOption;
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    inputChange(newValue: boolean) {
        this.onChange.emit([newValue, this.securityOption]);
    }

    // Returns true if array, false if string
    answerIsArray(): boolean {
        return this.securityOption.answer instanceof Array;
    }

    // These verification options must be able to wrap to a new line after
    // any character to preserve styling (rather than just spaces, hyphens, etc.)
    isUnwrappable(): boolean {
        return this.securityOption.name === 'email-verified'
            || this.securityOption.name === 'username-verified';
    }
}