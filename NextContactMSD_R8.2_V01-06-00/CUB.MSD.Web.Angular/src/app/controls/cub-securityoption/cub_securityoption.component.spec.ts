﻿import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Cub_SecurityOptionComponent } from './cub_securityoption.component';

describe('Cub_SecurityOptionComponent', () => {
    let comp: Cub_SecurityOptionComponent;
    let fixture: ComponentFixture<Cub_SecurityOptionComponent>;
    let root: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [Cub_SecurityOptionComponent],
        }).compileComponents();

        fixture = TestBed.createComponent(Cub_SecurityOptionComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
    });

    it('should not render if it has no answer', () => {
        comp.securityOption = {
            name: 'name-verified',
            question: 'Name',
            answer: '',
            verified: false
        };
        fixture.detectChanges();
        expect(root.nativeElement.querySelector('li')).toBeNull();
    });

    it('should have no <br> if answer is a string', () => {
        comp.securityOption = {
            name: 'name-verified',
            question: 'Name',
            answer: 'Jonathan Jacob Anderson',
            verified: false
        };
        fixture.detectChanges();
        expect(root.nativeElement.querySelectorAll('li.answer p br').length).toBe(0);
    });

    it('should separate lines with <br> tags if answer is an array', () => {
        comp.securityOption = {
            name: 'address-verified',
            question: 'Address',
            answer: ['123 Diners Dr', 'Suite 1B', 'Flavortown, USA'],
            verified: false
        };
        fixture.detectChanges();
        expect(root.nativeElement.querySelectorAll('li.answer p br').length).toBe(2);
    });

    it('should have class long-line-wrap if answer is email or username only', () => {
        comp.securityOption = {
            name: 'email-verified',
            question: 'Email',
            answer: 'ckent@thedailyplanet.com',
            verified: false
        };
        fixture.detectChanges();
        let answerEl = root.nativeElement.querySelector('li.answer');
        expect(answerEl.className).toMatch('long-line-wrap');

        comp.securityOption = {
            name: 'name-verified',
            question: 'Name',
            answer: 'Clark Kent',
            verified: false
        };
        fixture.detectChanges();
        expect(answerEl.className).not.toMatch('long-line-wrap');

        comp.securityOption = {
            name: 'username-verified',
            question: 'Username',
            answer: 'DefNotSuperman',
            verified: false
        };
        fixture.detectChanges();
        expect(answerEl.className).toMatch('long-line-wrap');
    });

    it('should emit an event when clicked', () => {
        spyOn(comp.onChange, 'emit');
        comp.securityOption = {
            name: 'name-verified',
            question: 'Name',
            answer: 'Trogdor the Burninator',
            verified: false
        };

        fixture.detectChanges();
        let label = fixture.nativeElement.querySelector('label');
        label.click();
        expect(comp.onChange.emit).toHaveBeenCalledWith([true, comp.securityOption]);
        label.click();
        expect(comp.onChange.emit).toHaveBeenCalledWith([false, comp.securityOption]);
    });
});