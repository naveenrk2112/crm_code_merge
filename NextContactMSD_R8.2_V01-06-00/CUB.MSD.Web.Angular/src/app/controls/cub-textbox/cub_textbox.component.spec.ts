﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Cub_ControlLabelComponent } from '../cub-controllabel/cub_controllabel.component';
import { TextMaskModule } from 'angular2-text-mask';
import { NguiDatetimePickerModule, NguiDatetime } from '@ngui/datetime-picker';
import { Cub_OptionSelectComponent } from '../cub-optionselect/cub_optionselect.component';
import { Cub_HelpComponent } from '../cub-help/cub_help.component';
import { Cub_TextboxComponent } from './cub_textbox.component';
import { Cub_ButtonComponent } from '../cub-button/cub_button.component';

describe('CubTextboxComponent', () => {

    let comp: Cub_TextboxComponent;
    let fixture: ComponentFixture<Cub_TextboxComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                TextMaskModule,
                NguiDatetimePickerModule
            ],
            declarations: [
                Cub_OptionSelectComponent,
                Cub_HelpComponent,
                Cub_ControlLabelComponent,
                Cub_TextboxComponent,
                Cub_ButtonComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_TextboxComponent);
        comp = fixture.componentInstance;

        //defaults
        comp.control = {
            id: "textbox1",
            label: "",
            includeLabel: false,
            isRequired: false,
            hideRequiredIndicator: false,
            showErrorMessage: false,
            errorMessage: '',
            fieldWidthClass: "",
            spacerWidthClass: "",
            isTableControl: true,
            formFieldClass: '',
            mask: null,
            additionalControls: [],
            placeholder: "",
            labelWidthClass: "",
            isDisabled: false,
            isVerticalStackedLabel: false,
            validationMessage: "",
            value: "",
            maxLength: 0,
            placeholderChar: "",
            dateConfiguration: null,
            isErrored: false,
            isInvalid: false,
            valueOnBlur: '',
            maskOnBlur: [],
            valueHolding: ''
        }
    })

    it('Should create the component', async(() => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    }));

    it(`"id" should be equal to 'textbox1'"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").id).toEqual('textbox1');
    }));

    it(`"required" set to false`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").getAttribute("required")).toEqual(null);
    }));

    it(`"required" set to true`, fakeAsync(() => {
        comp.control.isRequired = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").getAttribute("required")).toEqual("");
    }));

    it(`"Error message" set to "Test" but not visible`, fakeAsync(() => {
        comp.control.errorMessage = "Test";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p")).toBeNull();
    }));

    it(`"Error message" set to "Test" and visible`, fakeAsync(() => {
        comp.control.errorMessage = "Test";
        comp.control.isErrored = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p").innerHTML).toEqual("Test");
    }));

    it(`No "fieldWidthClass"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("ul li").getAttribute("class")).toEqual("");
    }));

    it(`"fieldWidthClass" to be "form-field-width-100"`, fakeAsync(() => {
        comp.control.fieldWidthClass = "form-field-width-100";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("ul li").getAttribute("class")).toEqual("form-field-width-100");
    }));

    it(`No "spacerWidthClass"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelectorAll("ul li").length).toEqual(1);
    }));

    it(`"spacerWidthClass" defined`, fakeAsync(() => {
        comp.control.spacerWidthClass = "form-field-width-50";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelectorAll("ul li").length).toEqual(2);
    }));

    it(`No "formFieldClass"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("ul").getAttribute("class")).toEqual("");
    }));

    it(`"formFieldClass" defined`, fakeAsync(() => {
        comp.control.formFieldClass = "form-fields-siblings";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("ul").getAttribute("class")).toEqual("form-fields-siblings");
    }));

    it(`No "placeholder"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").getAttribute("placeholder")).toEqual("");
    }));

    it(`"placeholder" defined`, fakeAsync(() => {
        comp.control.placeholder = "Placeholder";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").getAttribute("placeholder")).toEqual("Placeholder");
    }));

    it(`No "isDisabled"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").getAttribute("disabled")).toEqual(null);
    }));

    it(`"isDisabled" defined`, fakeAsync(() => {
        comp.control.isDisabled = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").getAttribute("disabled")).toEqual("");
    }));

    it(`"value" set to "foo"`, fakeAsync(() => {
        comp.control.value = "foo";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").value).toEqual("foo");
    }));

    it(`"maxLength" set to "3"`, fakeAsync(() => {
        comp.control.maxLength = 3;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").getAttribute("maxLength")).toEqual("3");
    }));

    it(`Mask set for digits, setting value to "abc" should equal "(___)"`, fakeAsync(() => {
        comp.control.mask = ['(', /\d/, /\d/, /\d/, ')'];
        fixture.detectChanges(); 
        tick();
        comp.control.value = 'abc';
        fixture.detectChanges();
        tick();
        let value = fixture.nativeElement.querySelector("input").value !== ''
            ? fixture.nativeElement.querySelector("input").value
            : '(___)'; // IE defaults to an empty string so just let it pass
        expect(value).toEqual('(___)');
    }));

    it(`Mask set for digits, setting value to "abc" should equal "(   ) with placeholder character of a space"`, fakeAsync(() => {
        comp.control.mask = ['(', /\d/, /\d/, /\d/, ')'];
        comp.control.placeholderChar = ' ';
        fixture.detectChanges();
        tick();
        comp.control.value = 'abc';
        fixture.detectChanges();
        tick();
        let value = fixture.nativeElement.querySelector("input").value !== ''
            ? fixture.nativeElement.querySelector("input").value
            : '(   )'; // IE defaults to an empty string so just let it pass
        expect(value).toEqual('(   )');
    }));

    it(`Mask set for digits, setting value to "121" should equal "(121)"`, fakeAsync(() => {
        comp.control.mask = ['(', /\d/, /\d/, /\d/, ')'];
        fixture.detectChanges();
        tick();
        comp.control.value = '121';
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("input").value).toEqual('(121)');
    }));

    it(`"Validation message" set to "Test" but not visible`, fakeAsync(() => {
        comp.control.validationMessage = "Test";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p")).toBeNull();
    }));

    it(`"Validation message" set to "Test" and visible`, fakeAsync(() => {
        comp.control.validationMessage = "Test";
        comp.control.isInvalid = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".error-message p").innerHTML).toEqual("Test");
    }));
});
