﻿import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';

import * as moment from 'moment';

import * as Model from '../../model';

//Can be inserted into a control column
@Component({
    selector: '[cub-textbox]',
    templateUrl: './cub_textbox.component.html'
})
export class Cub_TextboxComponent implements OnInit {
    private arrayOfAdditionalControlKeys: string[] = [];
    private maskHolding: Model.TextMask | Model.TextMaskFn = [];
    private _mask: Model.TextMaskConfig

    dateFormGroup: FormGroup;

    @Input() overrideFieldWidthClass: string;

    @Input()
    set mask(mask: Model.TextMaskConfig) {
        this._mask = mask;
        if (!!mask
            && (this.maskHolding == false
                || (Array.isArray(this.maskHolding)
                    && (this.maskHolding as Array<any>).length == 0))
            && (!mask.mask
                || typeof mask.mask === 'function'
                || mask.mask.length > 0)) {
            this.maskHolding = mask.mask;
        }
    }
    get mask(): Model.TextMaskConfig {
        return this._mask;
    }

    constructor(private fb: FormBuilder) { }

    ngOnInit() {
        if (!this.mask) {
            this.mask = {
                mask: this.control.mask || false,
                placeholderChar: this.control.placeholderChar || '_'
            };
        }
        this.dateFormGroup = this.fb.group({
            date: [null, [Validators.required]]
        });
        this.arrayOfAdditionalControlKeys = this.control.additionalControls ? Object.keys(this.control.additionalControls) : [];
    }

    @Input() control: Model.Cub_Textbox;

    //Emits
    @Output() onChange: EventEmitter<any> = new EventEmitter();
    @Output() onFocus: EventEmitter<any> = new EventEmitter();
    @Output() onFocusOut: EventEmitter<any> = new EventEmitter();
    @Output() onEnterPress: EventEmitter<any> = new EventEmitter();

    inputKeyPress(event: any) {
        if (event.keyCode == 13) {
            this.onEnterPress.emit([event.keyCode, this.control]);
        }
    }

    inputChange(event: any): void {
        if (event == null) {
            return;
        }

        let newValue;
        if (!!event.target) {
            newValue = event.target.value;
        } else if (typeof event === 'string') {
            newValue = event;
        } else {
            // dont allow this.control.value to be reset
            return;
        }

        if (newValue !== this.control.value) {
            this.control.value = newValue;
            this.onChange.emit([newValue, this.control]);
        }
    }

    inputFocusOut(event: any): void {
        this.control.value = this.control.value ? this.control.value.trim() : this.control.value;

        if (this.control.valueOnBlur && this.control.maskOnBlur) {
            this.control.valueHolding = this.control.value;
            this.control.value = this.control.valueOnBlur;
            this.mask = { mask: this.control.maskOnBlur, placeholderChar: this.control.placeholderChar };
        }
        this.onFocusOut.emit([this.control.value , this.control]);
    }

    inputFocus(event: any): void {
        if (this.control.valueOnBlur && this.control.maskOnBlur) {
            this.control.value = this.control.valueHolding
        }
        
        this.mask = { mask: this.maskHolding, placeholderChar: this.control.placeholderChar };
        this.onFocus.emit([event.target.value, this.control]);
    }

    additionalInputChange(data: any) {
        this.control.additionalControls[data[1].id].value = data[0];
        this.onChange.emit([data[0], data[1]]);
    }
}
