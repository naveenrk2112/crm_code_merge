﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as Model from '../../model';

@Component({
    selector: '[cub-checkbox]',
    templateUrl: './cub_checkbox.component.html'
})
export class Cub_CheckboxComponent {
    @Input() control: Model.Cub_Checkbox = {
        checkboxClass: '',
        isDisabled: false, //Disable component
        id: '', //unique control id
        name: '', //
        label: '', //Label for control
        value: '', //Value for control
        partial: false
    };
    @Input() dataInput: any;
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    inputChange(control: Model.Cub_Checkbox) {
        if (!control.value) {
            this.control.partial = false;
        }
        this.control.value = !control.value;
        this.onChange.emit(this.control);
    }

    startsWithFormFieldWidth() {
        return (this.control.checkboxClass.startsWith("form-field-width"));
    }
}