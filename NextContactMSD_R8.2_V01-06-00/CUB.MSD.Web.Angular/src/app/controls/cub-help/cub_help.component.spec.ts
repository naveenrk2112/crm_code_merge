﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Cub_HelpComponent } from './cub_help.component';

describe('CubHelpComponent', () => {

    let comp: Cub_HelpComponent;
    let fixture: ComponentFixture<Cub_HelpComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule
            ],
            declarations: [
                Cub_HelpComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_HelpComponent);
        comp = fixture.componentInstance;

        //defaults
        comp.control = {
            helpText: '',
            popoverText: ''
        };
    });

    it('Should create the component', async(() => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    }));

    it(`"helpText" should be equal to ''`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".form-field-help p").innerHTML).toEqual('');
    }));

    it(`"helpText" should be equal to 'Help!'"`, fakeAsync(() => {
        comp.control.helpText = "Help!";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".form-field-help p").innerHTML).toEqual('Help!');
    }));

    it(`"popoverText" should be equal to ''`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".help-message p").innerHTML).toEqual('');
    }));

    it(`"popoverText" should be equal to 'Popover!'`, fakeAsync(() => {
        comp.control.popoverText = "Popover!";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector(".help-message p").innerHTML).toEqual('Popover!');
    }));
});