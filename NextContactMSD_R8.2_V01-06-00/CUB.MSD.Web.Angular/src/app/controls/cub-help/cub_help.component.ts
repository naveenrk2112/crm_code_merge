﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as Model from '../../model';

@Component({
    selector: '[cub-help]',
    templateUrl: './cub_help.component.html'
})

//Help text with a hover over effect to show more information (combined with another control)
export class Cub_HelpComponent {
    @Input() control: Model.Cub_Help = {
        helpText: "Help", //Label for button on screen
        popoverText: "" //Text displayed when hovered over label
    };
}