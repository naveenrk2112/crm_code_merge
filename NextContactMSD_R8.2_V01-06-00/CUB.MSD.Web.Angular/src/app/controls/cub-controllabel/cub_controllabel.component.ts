﻿import {Component, Input} from '@angular/core';

@Component({
    selector: '[cub-controllabel]',
    template: '<label [attr.for]="id">{{label}}</label>'
})

//Used to display labels next to some input components
export class Cub_ControlLabelComponent {
    @Input() id: string = ""; //id of label
    @Input() label: string = ""; //text to display
}