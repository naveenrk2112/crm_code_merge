﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Cub_ControlLabelComponent } from './cub_controllabel.component';

describe('CubControlLabelComponent', () => {

    let comp: Cub_ControlLabelComponent;
    let fixture: ComponentFixture<Cub_ControlLabelComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule
            ],
            declarations: [
                Cub_ControlLabelComponent
            ],
        });

        fixture = TestBed.createComponent(Cub_ControlLabelComponent);
        comp = fixture.componentInstance;

        //defaults
        comp.id = "";
        comp.label = "";
    });

    it('Should create the component', async(() => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    }));

    it(`"id" should be equal to ''"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("label").getAttribute("for")).toEqual('');
    }));

    it(`"id" should be equal to 'label1'"`, fakeAsync(() => {
        comp.id = "label1"
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("label").getAttribute("for")).toEqual('label1');
    }));

    it(`"label" should be equal to ''"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("label").innerHTML).toEqual('');
    }));

    it(`"label" should be equal to 'Test Label'"`, fakeAsync(() => {
        comp.label = "Test Label";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("label").innerHTML).toEqual('Test Label');
    }));
});
