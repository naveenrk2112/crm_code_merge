﻿import { Component, Input, Output, EventEmitter, OnChanges, SimpleChange, AfterViewChecked } from '@angular/core';
import * as Model from '../../model';

@Component({
    selector: '[cub-optionselect]',
    templateUrl: './cub_optionselect.component.html'
})

//A special controls that shows two radio buttons (the first two options in the list), with the remainder of the options in a dropdown.
export class Cub_OptionSelectComponent implements OnChanges, AfterViewChecked {
    private fieldWidthClass: string;

    @Input() value: string;

    @Input() control: Model.Cub_OptionSelect;

    //After view loaded/checked
    ngAfterViewChecked() {
        this.dropdownChange(this.control.value);
    }

    //Used to make determinations on when to change the controls
    ngOnChanges(changes: { [propName: string]: SimpleChange }) {
        if (changes && changes.value && changes.value.currentValue) {
            this.dropdownChange(changes.value.currentValue);
        }
    }

    @Output() onChange: EventEmitter<any> = new EventEmitter();

    dropdownChange(newValue: any): void {
        //TODO: DOM manipulation, but should be in sync with value (due to some HTML/JS limitations it seems), maybe a better way?
        if (document.getElementById(this.control.option1.id)) {
            if (newValue == this.control.option1.value) {
                document.getElementById(this.control.option1.id)['checked'] = true;
                document.getElementById(this.control.option2.id)['checked'] = false;
                if (document.getElementById(this.control.selectId)) {
                    document.getElementById(this.control.selectId)['selectedIndex'] = 0;
                }
            }
            else if (newValue == this.control.option2.value) {
                document.getElementById(this.control.option1.id)['checked'] = false;
                document.getElementById(this.control.option2.id)['checked'] = true;
                if (document.getElementById(this.control.selectId)) {
                    document.getElementById(this.control.selectId)['selectedIndex'] = 0;
                }
            }
            else {
                document.getElementById(this.control.option1.id)['checked'] = false;
                document.getElementById(this.control.option2.id)['checked'] = false;
                if (document.getElementById(this.control['selectId'])) {
                    document.getElementById(this.control['selectId'])['value'] = newValue;
                }
            }
        }

        this.control.value = newValue;
        if (newValue != this.control.value) {
            this.onChange.emit([newValue, this.control]);
        }
    }
}
