﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Cub_OptionSelectComponent } from './cub_optionselect.component';

describe('CubOptionSelectComponent', () => {

    let comp: Cub_OptionSelectComponent;
    let fixture: ComponentFixture<Cub_OptionSelectComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule
            ],
            declarations: [
                Cub_OptionSelectComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_OptionSelectComponent);
        comp = fixture.componentInstance;
        comp.control = {
            formFieldClass: 'form-fields-siblings',
            labelWidthClass: 'form-field-width-50',
            fieldWidthClass: 'form-field-width-50',
            isDisabled: false,
            isTableControl: false,
            errorMessage: '',
            id: 'test-option-select',
            selectId: 'option-1',
            label: 'Test Option Select',
            dropdownFieldWidthClass: '',
            option1: {
                id: "Option1",
                name: "Option1",
                value: "1"
            },
            option2: {
                id: "Option2",
                name: "Option2",
                value: "2"
            },
            additionalOptions: [
                {
                    name: "Option3",
                    value: "Option3"
                },
                {
                    name: "Option4",
                    value: "Option4"
                }
            ]
        };
        comp.control.additionalOptions = [
            {
                name: "Option3",
                value: "Option3"
            },
            {
                name: "Option4",
                value: "Option4"
            }
        ];
    });

    it('Should create the component', () => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    });

    it('No formFieldClass', () => {
        comp.control.formFieldClass = '';
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector("ul").getAttribute("class")).toEqual("");
    });

    it(`"formFieldClass" defined`, () => {
        comp.control.formFieldClass = "form-fields-siblings";
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector("ul").getAttribute("class")).toEqual("form-fields-siblings");
    });

    it(`No "fieldWidthClass"`, () => {
        comp.control.fieldWidthClass = '';
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector("ul li").getAttribute("class")).toEqual("");
    });

    it(`"fieldWidthClass" to be "form-field-width-100"`, () => {
        comp.control.fieldWidthClass = "form-field-width-100";
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector("ul li").getAttribute("class")).toEqual("form-field-width-100");
    });

    it(`"isDisabled" set to false`, () => {
        comp.control.option1 = {
            id: "Option1",
            name: "Option1",
            value: "1"
        };
        comp.control.option2 = {
            id: "Option2",
            name: "Option2",
            value: "2"
        };

        comp.control.additionalOptions = [
            {
                name: "Option3",
                value: "3"
            },
            {
                name: "Option4",
                value: "4"
            }
        ];
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector("input[id='Option1']").getAttribute("disabled")).toEqual(null);
        expect(fixture.nativeElement.querySelector("input[id='Option2']").getAttribute("disabled")).toEqual(null);
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual(null);
    });

    it(`"isDisabled" set to true`, () => {
        comp.control.option1 = {
            id: "Option1",
            name: "Option1",
            value: "1"
        };
        comp.control.option2 = {
            id: "Option2",
            name: "Option2",
            value: "2"
        };

        comp.control.additionalOptions = [
            {
                name: "Option3",
                value: "3"
            },
            {
                name: "Option4",
                value: "4"
            }
        ];
        comp.control.isDisabled = true;
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector("input[id='Option1']").getAttribute("disabled")).toEqual("");
        expect(fixture.nativeElement.querySelector("input[id='Option2']").getAttribute("disabled")).toEqual("");
        expect(fixture.nativeElement.querySelector("select").getAttribute("disabled")).toEqual("");
    });

    it(`"labesl" should be equal to associated labels"`, () => {
        comp.control.option1 = {
            id: "Option1",
            name: "Option1",
            value: "1"
        };
        comp.control.option2 = {
            id: "Option2",
            name: "Option2",
            value: "2"
        };

        comp.control.additionalOptions = [
            {
                name: "Option3",
                value: "Option3"
            },
            {
                name: "Option4",
                value: "Option4"
            }
        ];
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector("label[for='Option1']").innerHTML.trim()).toEqual("Option1");
        expect(fixture.nativeElement.querySelector("label[for='Option2']").innerHTML.trim()).toEqual("Option2");
        expect(fixture.nativeElement.querySelector("option[value='Option3']").innerHTML.trim()).toEqual("Option3");
    });
});
