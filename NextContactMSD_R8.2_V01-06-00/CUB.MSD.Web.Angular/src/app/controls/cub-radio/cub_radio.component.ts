﻿import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Model from '../../model';
import { defaults } from 'lodash';

@Component({
    selector: '[cub-radio]',
    templateUrl: './cub_radio.component.html'
})
export class Cub_RadioComponent implements OnInit{
    @Input() control: Model.Cub_Radio;
    @Input() checkedValue: string = null;
    @Input() group: string = '';
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    inputChange(event: any) {
        this.onChange.emit(this.control.id);
    }

    ngOnInit() {
        // set defaults for any unspecified fields
        defaults(this.control, {
            labelClass: '',
            radioClass: '',
            isDisabled: false,
            id: '',
            label: ''
        } as Model.Cub_Radio);
    }
}