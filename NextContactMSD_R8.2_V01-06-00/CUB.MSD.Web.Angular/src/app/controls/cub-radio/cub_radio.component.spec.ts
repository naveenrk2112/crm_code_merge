﻿import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Cub_RadioComponent } from './cub_radio.component';

describe('Cub_RadioComponent', () => {
    let comp: Cub_RadioComponent;
    let fixture: ComponentFixture<Cub_RadioComponent>;
    let root: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [Cub_RadioComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_RadioComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
        comp.control = {
            id: 'test-radio',
            label: 'Test Radio'
        };
    });

    it('should create the component', () => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    });

    it('should set default values for optional control properties', () => {
        fixture.detectChanges();
        expect(comp.control).toBeDefined('Control is undefined');
        expect(comp.control.isDisabled).toEqual(false, 'isDisabled not set to default');
        expect(comp.control.labelClass).toEqual('', 'Label class not set to default');
        expect(comp.control.radioClass).toEqual('', 'Radio class not set to default');
    });

    it('should default to unchecked', () => {
        fixture.detectChanges();
        expect(root.nativeElement.querySelector('input').checked).toBeFalsy();
    });

    it('should be able to be checked on init', () => {
        comp.checkedValue = comp.control.id;
        fixture.detectChanges();
        expect(root.nativeElement.querySelector('input').checked).toBeTruthy();
    })

    it('should emit an event with the ID of the input', (done) => {
        const ID = 'fancy-test-radio-id';
        comp.control = {
            id: ID,
            label: 'Test Radio',
        };
        comp.onChange.subscribe(val => {
            expect(val).toEqual(ID);
            done();
        });
        fixture.detectChanges();
        root.nativeElement.querySelector('input').click();
    });
});