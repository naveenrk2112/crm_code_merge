﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as Model from '../../model';

//Can be inserted into a control column
@Component({
    selector: '[cub-flydown]',
    templateUrl: './cub_flydown.component.html'
})

export class Cub_FlydownComponent {
    @Input() control: Model.Cub_Flydown = {
        id: '', //unique control id
        label:'', //Label for control
        value: '', //Value for control
        options: [] //Array of objects: {name: "Label Text", value: "Value"}
    };

    @Output() onChange: EventEmitter<any> = new EventEmitter();

    flydownChange(newValue: any): void {
        this.control.value = newValue.target.value;
        this.onChange.emit([newValue.target.value, this.control]);
    }

    inputChange(newValue: string): void {
        if (newValue != this.control.value) {
            this.control.value = newValue;
            this.onChange.emit([newValue, this.control]);
        }
    }
}