﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { chain, filter, map } from 'lodash';
import { Cub_MultiDropdownOption } from '../../model';

@Component({
    selector: '[cub-multi-dropdown]',
    templateUrl: './cub-multi-dropdown.component.html'
})
export class Cub_MultiDropdown {
    
    @Input() controlId: string;
    @Input() label: string;

    /**
     * Use get/set for options to set all as checked by default. This
     * way, components implementing this control do not need to bother with it.
     */
    private _options: Cub_MultiDropdownOption[] = [];
    @Input('options')
    set options(list: Cub_MultiDropdownOption[]) {
        if (list == null) {
            return;
        }
        for (let i = 0; i < list.length; ++i) {
            list[i].checked = true;
        }
        this._options = list;
        this.allSelected = true;
    }
    get options() {
        return this._options;
    }

    allSelected: boolean = true;

    get isDisabled(): boolean {
        return this.options.length < 2;
    }

    /**
     * Displays total number selected to the user
     */
    get numSelected(): string {
        if (this.allSelected) {
            return 'All';
        }

        let totalSelected = filter(this.options, o => o.checked).length;
        if (totalSelected === 1) {
            return '1 Type';
        } else {
            return totalSelected + ' Types';
        }
    }

    /**
     * Fires when an option is selected or unselected and emits a list of all
     * selected values. Emits an empty list when both none and all are selected.
     */
    @Output() onChange = new EventEmitter<string[]>();

    /**
     * Sets all options to match 'All' option.
     */
    onAllChange() {
        this.allSelected = !this.allSelected;
        for (let i = 0; i < this.options.length; ++i) {
            this.options[i].checked = this.allSelected;
        }
        this.alertChange();
    }

    /**
     * Also attempts to set 'All' option if possible.
     * @param option
     */
    onOptionChange(option: Cub_MultiDropdownOption) {
        option.checked = !option.checked;
        if (!option.checked) {
            this.allSelected = false;
        } else {
            let totalNotSelected = filter(this.options, o => !o.checked).length;
            if (totalNotSelected === 0) {
                this.allSelected = true;
            }
        }
        this.alertChange();
    }

    /**
     * Fires when any option is selected/unselected. This causes an event to be
     * emitted containing all selected option values, or [] if either all or
     * none are selected.
     */
    alertChange() {
        if (this.allSelected) {
            this.onChange.emit([]);
        } else {
            let selected = chain(this.options)
                .filter(o => o.checked)
                .map(o => o.value)
                .value();
            this.onChange.emit(selected);
        }
    }

    reset() {
        this.options = [].concat(this.options);
        this.alertChange();
    }
}
