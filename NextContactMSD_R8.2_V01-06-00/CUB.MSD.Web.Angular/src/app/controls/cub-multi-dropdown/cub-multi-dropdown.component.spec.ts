﻿import { TestBed, async, ComponentFixture } from '@angular/core/testing';

import { Cub_MultiDropdown } from './cub-multi-dropdown.component';
import { Cub_MenuToggleDirective } from '../../cub-menu-toggle/cub-menu-toggle.directive';

describe('Cub_MultiDropdown', () => {
    let fixture: ComponentFixture<Cub_MultiDropdown>;
    let component: Cub_MultiDropdown;

    const OPTION1 = 'option-1';
    const OPTION2 = 'option-2';

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                Cub_MultiDropdown,
                Cub_MenuToggleDirective
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_MultiDropdown);
        component = fixture.componentInstance;
        component.controlId = 'test-multi-dropdown';
        component.label = 'Test MultiDropdown';
        component.options = [
            {
                value: OPTION1,
                label: 'Option 1'
            }, {
                value: OPTION2,
                label: 'Option 2'
            }
        ];
    });

    it('should render', () => {
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });

    it('should be able to render without options set', () => {
        component.options = [];
        fixture.detectChanges();
        expect(component).toBeTruthy();
        expect(component.options).toEqual([]);
    });

    it('should select all by default', () => {
        fixture.detectChanges();
        expect(component.numSelected).toEqual('All');
        expect(component.allSelected).toBeTruthy();
        for (let i = 0; i < component.options.length; ++i) {
            expect(component.options[i].checked).toBeTruthy();
        }
    });

    it('should display number of options selected if less than all', () => {
        spyOn(component.onChange, 'emit');
        fixture.detectChanges();

        fixture.debugElement.nativeElement.querySelector(`#${component.controlId}-${OPTION1}`).click();
        fixture.detectChanges();
        expect(component.numSelected).toEqual('1 Type');
        expect(component.onChange.emit).toHaveBeenCalledWith([OPTION2]);

        fixture.debugElement.nativeElement.querySelector(`#${component.controlId}-${OPTION2}`).click();
        fixture.detectChanges();
        expect(component.numSelected).toEqual('0 Types');
        expect(component.onChange.emit).toHaveBeenCalledWith([]);
    });

    it('should toggle all options when All clicked', () => {
        spyOn(component.onChange, 'emit');
        fixture.detectChanges();

        fixture.debugElement.nativeElement.querySelector('#' + component.controlId + '-all').click();
        fixture.detectChanges();
        expect(component.onChange.emit).toHaveBeenCalledWith([]);
        expect(component.numSelected).toEqual('0 Types');
        expect(component.allSelected).toBeFalsy();
        for (let i = 0; i < component.options.length; ++i) {
            expect(component.options[i].checked).toBeFalsy();
        }

        fixture.debugElement.nativeElement.querySelector('#' + component.controlId + '-all').click();
        fixture.detectChanges();
        expect(component.onChange.emit).toHaveBeenCalledWith([]);
        expect(component.numSelected).toEqual('All');
        expect(component.allSelected).toBeTruthy();
        for (let i = 0; i < component.options.length; ++i) {
            expect(component.options[i].checked).toBeTruthy();
        }
    });
});