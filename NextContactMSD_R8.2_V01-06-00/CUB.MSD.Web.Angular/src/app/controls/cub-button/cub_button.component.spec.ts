﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Cub_ButtonComponent } from './cub_button.component';

describe('CubButtonComponent', () => {

    let comp: Cub_ButtonComponent;
    let fixture: ComponentFixture<Cub_ButtonComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule
            ],
            declarations: [
                Cub_ButtonComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_ButtonComponent);
        comp = fixture.componentInstance;

        //defaults
        comp.control = {
            id: "",
            class: "",
            isDisabled: false,
            label: ""
        };

    });

    it('Should create the component', async(() => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    }));

    it(`"id" should be equal to ''"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("button").getAttribute("id")).toEqual('');
    }));

    it(`"id" should be equal to 'Test'"`, fakeAsync(() => {
        comp.control.id = "button1"
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("button").getAttribute("id")).toEqual('button1');
    }));

    it(`"class" should be equal to ''"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("button").getAttribute("class")).toEqual('');
    }));

    it(`"class" should be equal to 'button-primary'"`, fakeAsync(() => {
        comp.control.class = "button-primary"
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("button").getAttribute("class")).toEqual('button-primary');
    }));

    it(`"label" should be equal to ''"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("button p").innerHTML).toEqual('');
    }));

    it(`"label" should be equal to 'A button'"`, fakeAsync(() => {
        comp.control.label = "A button";
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("button p").innerHTML).toEqual('A button');
    }));

    it(`"isDisabled" should be false"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("button").getAttribute("disabled")).toBeNull();
    }));

    it(`"isDisabled" should be true"`, fakeAsync(() => {
        comp.control.isDisabled = true;
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("button").getAttribute("disabled")).toEqual("");
    }));


    it(`Click of button should emit click event`, async(() => {
        spyOn(comp, 'click');
        var button = fixture.nativeElement.querySelector("button");
        button.click();
        fixture.whenStable().then(() => {
            expect(comp.click).toHaveBeenCalled();
        });
    }));
});
