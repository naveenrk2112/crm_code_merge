﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as Model from '../../model';

@Component({
    selector: '[cub-button]',
    templateUrl: './cub_button.component.html'
})

//Used to display certain buttons to the user
export class Cub_ButtonComponent {
    @Input() control: Model.Cub_Button = {
        id: '', //unique control id
        class: '', //Button's class
        isDisabled: false, //Disable component
        label: '' //Label for control
    };

    //Emits
    @Output() onClick: EventEmitter<any> = new EventEmitter();

    //On button click
    click(entry: Cub_ButtonComponent): void {
        if(this.onClick) {
            this.onClick.emit([entry, this.control]);
        }
    }
}