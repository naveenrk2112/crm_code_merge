﻿import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Cub_RadiolistComponent } from './cub_radiolist.component';
import { Cub_RadioComponent } from '../cub-radio/cub_radio.component';

describe('Cub_RadiolistComponent', () => {
    let comp: Cub_RadiolistComponent;
    let fixture: ComponentFixture<Cub_RadiolistComponent>;
    let root: DebugElement;

    const RADIO_CONTROLS = [
        {
            id: 'radio-1',
            label: 'Radio 1'
        }, {
            id: 'radio-2',
            label: 'Radio 2'
        }
    ];

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [
                Cub_RadiolistComponent,
                Cub_RadioComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_RadiolistComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
        comp.control = {
            id: 'test-radio-list',
            label: '',
            group: 'test-input-group',
            controls: RADIO_CONTROLS
        };
    });

    it('should create the component', () => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    });

    it('should be able to render as buttons', () => {
        comp.control.isButton = true;
        fixture.detectChanges();
        expect(root.nativeElement.querySelectorAll('input').length).toEqual(RADIO_CONTROLS.length);
    });

    it('should render a <p> element if label is set', () => {
        const LABEL = 'Test Radiolist Label';
        comp.control.label = LABEL;
        fixture.detectChanges();
        expect(root.nativeElement.querySelector('p').innerHTML).toEqual(LABEL);
    });

    it('should render a <label> element if label is set and rendering as buttons', () => {
        const LABEL = 'Test Radiolist Label';
        comp.control.label = LABEL;
        comp.control.isButton = true;
        fixture.detectChanges();
        expect(root.nativeElement.querySelector('label').innerHTML).toEqual(LABEL);
    });

    it('should set default values for optional control properties', () => {
        fixture.detectChanges();
        expect(comp.control.isButton).toEqual(false, 'isButton not set to default');
        expect(comp.control.value).toEqual('', 'Value not set to default');
    });

    it('should not check any option by default', () => {
        fixture.detectChanges();
        expect(comp.control.value).toBeFalsy('Control value should not be set');
        expect(root.nativeElement.querySelector('input:checked')).toBeFalsy('No <input> should be checked');
    });

    it('emit the selected input ID when one is clicked', (done) => {
        fixture.detectChanges();
        let el = root.nativeElement.querySelector('input');
        comp.onChange.subscribe(val => {
            expect(val).toEqual(el.id);
            expect(comp.control.value).toEqual(el.id);
            done();
        });
        el.click();
    });
});