﻿import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Model from '../../model'
import { defaults } from 'lodash';

@Component({
    selector: '[cub-radiolist]',
    templateUrl: './cub_radiolist.component.html'
})
export class Cub_RadiolistComponent implements OnInit {
    @Input() control: Model.Cub_Radiolist;
    //Emits
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>(); //When input changes

    inputChange(checkedId: string) {
        this.control.value = checkedId;
        this.onChange.emit(checkedId);
    }

    ngOnInit() {
        defaults(this.control, {
            controls: [],
            id: '',
            group: '',
            label: '',
            value: '',
            isButton: false
        } as Model.Cub_Radiolist);
    }
}