﻿import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Cub_AlertComponent } from './cub_alert.component';

describe('CubAlertComponent', () => {

    let comp: Cub_AlertComponent;
    let fixture: ComponentFixture<Cub_AlertComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule
            ],
            declarations: [
                Cub_AlertComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_AlertComponent);
        comp = fixture.componentInstance;

        //defaults
        comp.alertMessage = "";
    });

    it('Should create the component', async(() => {
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    }));

    it(`"alertMessage" should be equal to ''"`, fakeAsync(() => {
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("li p").innerHTML).toEqual('');
    }));

    it(`"alertMessage" should be equal to 'Test'"`, fakeAsync(() => {
        comp.alertMessage = "Test"
        fixture.detectChanges();
        tick();
        expect(fixture.nativeElement.querySelector("li p").innerHTML).toEqual('Test');
    }));
});
