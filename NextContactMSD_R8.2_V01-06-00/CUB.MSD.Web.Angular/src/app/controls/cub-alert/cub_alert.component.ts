﻿// Intended for use with cub_customerverification.component.ts to display contact alerts

import { Component, Input } from '@angular/core';

@Component({
    selector: 'cub-alert',
    templateUrl: './cub_alert.component.html'
})
export class Cub_AlertComponent {
    @Input() alertMessage: string;
}