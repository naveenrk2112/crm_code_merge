﻿import { HttpErrorResponse } from '@angular/common/http';

import * as moment from 'moment';

// MSD entity logical names
export const CustomerLogicalName = 'account';
export const ContactLogicalName = 'contact';
export const cub_TransitAccountLogicalName = 'cub_transitaccount';

/**
 * Use this interface if you need to add additional unique properties to an
 * existing class/interface, but said properties are irrelevant to other uses.
 * 
 * Example:
 * You're using the multiselect dropdown control and you want to add a field
 * 'isRequired' to your Cub_MultiDropdownOption list. Since this is a unique
 * case for the multiselect, rather than specifying 'isRequired' as an optional
 * field on the interface (i.e. isRequired?: boolean), instead make the
 * interface Cub_MultiDropdownOption extend Anything.
 *
 * Syntax:
 * export interface MyFakeInterface extends Anything {...}
 * export class MyFakeClass implements Anything {...}
 */
export interface Anything {
    [key: string]: any
}

export enum VerificationResult {
    VERIFIED,
    NOT_VERIFIED,
    CANCEL
}

/**
 * Class used by the Angular application to manage error handling. Error-handling
 * logic should immediately create a new CubError by passing in the error context
 * object. Set the handled property to TRUE as soon as an error is handled so that
 * rethrowing the error does not result in duplicate handling.
 * 
 * @example
 *  let e = new CubError(err);
 *  if (e.handled) {
 *      return;
 *  }
 *  // handle error, set e.handled = true, and optionally rethrow e.
 */
export class CubError {
    error: Error;
    handled: boolean;

    constructor(error: any) {
        if (error instanceof CubError) {
            this.error = error.error;
            this.handled = error.handled;
        } else if (error instanceof Error) {
            this.error = error;
            this.handled = false;
        } else if (error instanceof HttpErrorResponse) {
            this.error = (error.error instanceof Error)
                ? error.error
                : new Error(error.message);
            this.handled = false;
        } else if (typeof error === 'string') {
            this.error = new Error(error);
            this.handled = false;
        } else {
            this.error = new Error(JSON.stringify(error));
            this.handled = false;
        }
    }
}

//Controls
/**
 * Interface: Cub_Button
 * This interface is used by this file to generate the appropriate object properties
 * for any component that uses the cub_button control
 */
export interface Cub_Button {
    id: string, //unique control id
    class: string, //Button's class
    isDisabled?: boolean, //Disable component
    label: string //Label for control
}

export interface Cub_Checkbox {
    checkboxClass: string,
    isDisabled?: boolean, //Disable component
    id: string, //unique control id
    name?: string, //
    label: string, //Label for control
    value: string | boolean, //Value for control
    partial?: boolean
}
/**
 * Interface: Cub_Checkboxlist
 * This interface is used by this file to generate the appropriate object properties
 * for any component that uses the cub_checkboxlist control
 *
*/
export interface Cub_Checkboxlist {
    id: string,
    controls: { [value: string]: Cub_Checkbox }, //Properties vary based on the control type, look at actual control components for details
    controlsOrder: string[], //Order to show controls
    label: string; //Label for the control group
    noSubClass: boolean, // if true, don't add a class to each cub-checkbox <li>
}

export interface Cub_CustomizableGrid {
    actions: Cub_CustomizableGridAction[],
    columns: Cub_CustomizableGridWebserviceColumn[],
    formattedColumns?: Cub_CustomizableGridColumn[],
    displayPaginator: boolean,
    id: string,
    primaryKey: string,
    rowsToDisplay: number,
    maxRecords: number,
    defaultSort: string
}

export interface Cub_CustomizableGridColumn {
    columnSize: Cub_CustomizableGridColumnSize,
    displayName: string,
    fieldName: string,
    format: Cub_CustomizableGridColumnFormat,
    sortable: boolean,
    sortField?: string,
    visible: boolean,
    sortOrder: number
}

export interface Cub_CustomizableGridWebserviceColumn {
    columnSize: string,
    displayName: string,
    fieldName: string,
    format: string,
    sortable: boolean,
    sortField?: string,
    visible: boolean
}

export enum Cub_CustomizableGridColumnFormat {
    STRING,
    DATETIME,
    CURRENCY,
    PHONE
}

export enum Cub_CustomizableGridColumnSize {
    XXXS,   //24rem
    XXS,    //30rem
    XS,     //36rem
    S,      //42rem
    M,      //46rem
    L,      //50rem
    XL,     //56rem
    XXL,    //62rem
    XXXL,   //70rem
    XXXXL   //80rem
}

export interface Cub_CustomizableGridAction {
    label: string,
    action: string,
    condition?: string
}

/**
 * Interface: Cub_Dropdown
 * This interface is used by this file to generate the appropriate object properties
 * for any component that uses the cub_dropdown control
 */
export interface Cub_Dropdown {
    formFieldClass?: string, //Form field class to use, eg. form-fields-siblings
    labelWidthClass?: string, //Width of label field (eg. form-field-width-100) (Only used when isTableControl = false)
    fieldWidthClass?: string, //Width of field (eg. form-field-width-100)
    spacerWidthClass?: string, //Width of space to right of field (eg. form-field-width-100)
    isDisabled?: boolean, //Disable component (manually, if only one option, it's automatically disabled)
    isRequired?: boolean, //Require a value
    isTableControl?: boolean, //Should we display this as a table control layout
    isSmall?: boolean, //If dropdown is a smaller size or not
    errorMessage?: string, //Error message to display when required or validation not met
    validationMessage?: string, //Error message to display when validation not met
    showErrorMessage?: boolean, //Show the error message
    id: string, //unique control id
    label?: string, //Label for control
    value?: string, //Value for control
    text?: string, // Text of the control
    options: Cub_DropdownOption[], //Array of objects: {name: "Label Text", value: "Value", isDisabled: boolean (optional, defaults to false)}
    isErrored?: boolean, //Field is in an errored stated
    isInvalid?: boolean, //Field is in an invalid state
    type?: string //Used as an identifier for dynamic fields when building forms
}

export interface Cub_DropdownOption extends Anything{
    name: string,
    value: string,
    isDisabled?: boolean
}

/**
 * Interface: Cub_Flydown
 * This interface is used by this file to generate the appropriate object properties
 * for any component that uses the cub_flydown control
 */
export interface Cub_Flydown {
    id: string, //unique control id
    label: string, //Label for control
    value: string, //Value for control
    options: Cub_FlydownOption[] //Array of Cub_FlydownOption: {name: "Label Text", value: "Value"}
}

export interface Cub_FlydownOption {
    name: string,
    value: string
}

export interface Cub_Help {
    helpText: string,
    popoverText: string
}

export interface Cub_MultiDropdownOption {
    value: string,
    label: string,
    checked?: boolean
}

/**
 * Interface: Cub_OptionSelect
 * This interface is used by this file to generate the appropriate object properties
 * for any component that uses the cub_optionselect control
 */
export interface Cub_OptionSelect {
    formFieldClass: string, //Form field class to use, eg. form-fields-siblings
    labelWidthClass: string, //Width of label field (eg. form-field-width-100) (Only used when isTableControl = false)
    fieldWidthClass: string, //Width of field (eg. form-field-width-100)
    isDisabled: boolean, //Disable component
    isTableControl: boolean, //Should we display this as a table control layout
    errorMessage: string, //Error message to display when required or validation not met
    id: string, //unique control id
    selectId: string,
    label: string, //Label for control
    value?: string, //Value for control
    dropdownFieldWidthClass: string, //width of the additional dropdown control
    option1: Cub_OptionSelectOption, //option 1, object {value: '', id: '', name: '', fieldWidthClass: ''}
    option2: Cub_OptionSelectOption, //option 1, object {value: '', id: '', name: '', fieldWidthClass: ''}
    additionalOptions: Cub_OptionSelectAdditionalOption[], //Array of objects, {value: '', name: '', isDisabled: ''}
    type?: string //Used as an identifier for dynamic fields when building forms
};

export interface Cub_OptionSelectOption {
    id: string,
    name: string,
    value: string,
    fieldWidthClass?: string
}

export interface Cub_OptionSelectAdditionalOption {
    name: string,
    value: string,
    isDisabled?: boolean,
}

export interface Cub_Radio {
    radioClass?: string, //Width of field (eg. form-field-width-100)
    labelClass?: string, //Class for Label
    isDisabled?: boolean, //Disable component
    id: string, //unique control id
    label: string, //Label for control
}

export interface Cub_Radiolist {
    controls: Cub_Radio[], //list of controls in display order
    id: string, //Unique id name for control
    group: string, //Groupname of the radiolist component
    label?: string, //Label for the control group
    value?: string, //value of the selected radio button
    isButton?: boolean, //is this styled as a custom button control or native radio options?
}

/**
 * Interface: Cub_Textbox
 * This interface is used by this file to generate the appropriate object properties
 * for any component that uses the cub_textbox control
 */
export interface Cub_Textbox {
    additionalControls?: {},
    placeholder?: string,
    formFieldClass?: string, //Form field class to use, eg. form-fields-siblings
    labelWidthClass?: string, //Width of label field (eg. form-field-width-100) (Only used when isTableControl = false)
    fieldWidthClass?: string, //Width of field (eg. form-field-width-100)
    spacerWidthClass?: string, //Width of space to right of field (eg. form-field-width-100)
    isDisabled?: boolean, //Disable component
    isRequired?: boolean, //Require a value
    isTableControl?: boolean, //Should we display this as a table control layout
    isVerticalStackedLabel?: boolean, //Should we display this control with a label on top of the control?
    showErrorMessage?: boolean, //Show the error message
    errorMessage?: string, //Error message to display when required not met
    validationMessage?: string, //Error message to display when validation not met
    id: string, //unique control id
    label?: string, //Label for control
    includeLabel?: boolean, //set to TRUE if isTableControl == false && NOT using separate cub-controllabel
    value?: string, //Value for control
    maxLength?: number, //Max Length of value (DO NOT use with mask)
    mask?: any[] //Mask to use for formatting
    placeholderChar?: string, //default mask to show for empty spaces
    dateConfiguration?: {
        showDatePicker: boolean, //Shows a popup date picker
        dateFormat: string //Format of date popover input
    },
    isErrored?: boolean, //Field is in an errored stated
    isInvalid?: boolean, //Field is in an invalid state
    hideRequiredIndicator?: boolean, //If IsRequired is true and this is true, the user will not see the red bar to the left of the text field
    valueOnBlur?: string, //The temporary value to input into text box on blur (focus out)
    maskOnBlur?: TextMask | TextMaskFn, //The temporary mask value to use on the textbox on blur (focus out)
    valueHolding?: string, //The 'raw' value inputed when 'valueOnBlur' and 'maskOnBlur' are used
    type?: string, //Used as an identifier for dynamic fields when building forms
    validationRegEx?: RegExp //Used to validate filed
};

/**
 * Object passed to <input [textMask]="myTextMask" /> used by `angular2-text-mask`
 * package and cub-textbox control.
 * https://github.com/text-mask/text-mask/blob/master/componentDocumentation.md#text-mask-documentation
 */
export interface TextMaskConfig {
    /**
     * https://github.com/text-mask/text-mask/blob/master/componentDocumentation.md#mask
     */
    mask: TextMask | TextMaskFn,

    /**
     * defaults to true
     */
    guide?: boolean,

    /**
     * defaults to '_'
     */
    placeholderChar?: string,

    /**
     * defaults to false
     */
    keepCharPositions?: boolean,
    pipe?: TextMaskPipeFn,

    /**
     * defaults to false
     */
    showMask?: boolean
}

export type TextMask = false | (string | RegExp)[];

/**
 * Optional function used by `angular2-text-mask` package within cub-textbox
 * https://github.com/text-mask/text-mask/blob/master/componentDocumentation.md#mask-function
 */
export interface TextMaskFn {
    (rawInput: string): TextMask;
}
/**
 * https://github.com/text-mask/text-mask/blob/master/componentDocumentation.md#pipe
 */
export interface TextMaskPipeFn {
    (conformedValue: string, config: TextMaskConfig): false | string | TextMaskPipeOutput
}

export interface TextMaskPipeOutput {
    value: string,
    indexesOfPipedChars: number[]
}

/**
 * Interface: Cub_SubsystemAccount
 * This interface is used by this file to generate the appropriate object properties
 * for any component that uses the cub_subsystemaccount component
 */
export interface Cub_SubsystemAccount {
    subsystem: string,
    agency: string,
    subsystemId: string,
    status: string,
    balance: number,
    lastUsed: Date,
    tokens: number,
    accountId?: string | number
}

/**
 * Interface: Cub_SecurityOption
 * This interface is used by this file to generate the appropriate object properties
 * for any component that uses the cub_securityoption component
 */
export interface Cub_SecurityOption {
    name: string,
    question: string,
    answer: string | string[],
    verified: boolean
}

/**
 * Interface: Category
 * This interface is used by this file to help organize the output of the
 * NIS system into something more easily used
 * in UI Layout
 */
export interface Category {
    categoryName: string;
    categoryDescription: string;
    isExpanded: boolean;
    isNotification: boolean;
    subcategories: Array<SubCategory>;
    emailCheckbox?: Cub_Checkbox;
    pushnotificationCheckbox?: Cub_Checkbox;
    smsCheckbox?: Cub_Checkbox;
}

/** 
 * Interface: Channel
 * This interface is used by this file to help organize the output of the
 * NIS system into something more easily used
 * in UI Layout
 */
export interface Channel {
    channel?: string;
    enabled?: boolean;
    reqType?: string;
}

/**
 * Interface: Contact
 * This interface is used by this file to help organize the contacts returned from the
 * webservice into something more easily referenced.
 */
export interface Contact {
    contactId: string;
    firstName: string;
    lastName: string;
    selected?: boolean;
    contactCheckbox?: Cub_Checkbox;
}

/** 
 * Interface: NotificationPreference
 * This interface is used by this file to help organize the output of the
 * NIS system into something more easily used
 * in UI Layout
 */
export interface NotificationPreference {
    notificationType: string;
    notificationDescription: string;
    optIn: boolean;
    channels: Array<Channel>;
    emailCheckbox?: Cub_Checkbox;
    pushnotificationCheckbox?: Cub_Checkbox;
    smsCheckbox?: Cub_Checkbox;
}

/**
 * Data model returned by Cub_NotificationService.getCustomerNotifications()
 */
export interface NotificationRecord {
    notificationId: number,
    contactName: string,
    createDateTime: string,
    notificationReference: string,
    type: string,
    description: string,
    channel: string,
    recipient: string,
    subject: string,
    status: string,
    sendDateTime: string,
    failDateTime: string,
    allowResend: boolean
}

/**
 * Data model for request body of getCustomerNotifications()
 */
export interface NotificationSearchCriteria {
    customerId: string,
    contactId?: string,
    startDate?: string,
    endDate?: string,
    type?: string,
    status?: string,
    channel?: string,
    recipient?: string,
    notificationReference?: string,
    sortBy?: string,
    offset?: number,
    limit?: number
}

/**
 * Data model returned by getCustomerNotificationDetail()
 */
export interface NotificationDetail {
    alternateMessage: string;
    message: string;
    contactId: string;
}

/**
 * Data model for request body of getNotifications()
 */
export interface NotificationsFilters {
    recipient?: string,
    notificationReference?: string,
    globalTxnId?: string,
    startDate?: string,
    endDate?: string,
    type?: string,
    status?: string,
    channel?: string,
    sortBy?: string,
    offset?: number,
    limit?: number
}

/**
 * Interface: SubCategory
 * This interface is used by this file to help organize the output of the
 * NIS system into something more easily used
 * in UI Layout
 */
export interface SubCategory {
    subcategoryName: string;
    subcategoryDescription: string;
    isExpanded: boolean;
    isNotification: boolean;
    emailCheckbox?: Cub_Checkbox;
    pushnotificationCheckbox?: Cub_Checkbox;
    smsCheckbox?: Cub_Checkbox;
    notifications: Array<NotificationPreference>;
}

/**
 * Data model parsed from NISApiModel.WSTransitAccountPass with isActive added
 */
export interface TransitAccountPass {
    createdDateTime?: string,
    isActive: boolean,
    deactivatedDateTime?: string,
    deactivatedReason?: string,
    durationType?: string,
    endDateTime?: string,
    expirationDateTime: string,
    fareProductTypeId: string,
    passDescription: string,
    passSKU: string,
    passSerialNbr: string,
    remainingDuration?: number,
    startDateTime?: string,
    supportsAutoload: boolean
}

/**
 * Data model from NISApiModel.WSTransitAccountToken with passesControl added
 */
export interface TransitAccountToken {
    subsystemTokenType: string,
    subsystemTokenTypeDescription: string,
    tokenInfo: TravelTokenDisplay,
    status?: string,
    statusDescription?: string,
    statusChangeDateTime?: string,
    tokenLastUsageDetails?: LastUsageDetails,
    passesControl?: Cub_Radio
}

/**
 * Data model from NISApiModel.WSTravelTokenDisplay which implements
 * WSBankcardTravelTokenDisplay, WSSmartcardTravelTokenDisplay, and
 * WSMobileTravelTokenDisplay. These subclass fields must be marked optional
 * even if required.
 */
export interface TravelTokenDisplay {
    tokenType: string,
    tokenNickname?: string,
    lastUseDTM: string,

    /* Bankcard fields */
    maskedPAN?: string,
    cardExpiryMMYY?: string,

    /* Smartcard fields */
    serialNumber?: string,
    cardExpiryDate?: string,

    /* Mobile fields */
    serializedMobileToken?: string
}

/**
 * Data model from NISApiModel.WSLastUsageDetails
 */
export interface LastUsageDetails {
    transactionDateTime: string,
    device: string,
    status: string,
    statusDescription: string,
    location: string
}

/* Data model returned for Session Info*/
export interface Session {
    sessionId?: string;
    sessionNumber?: string;
    customerId?: string;
    customerFirstName?: string;
    customerLastName?: string;
    contactId?: string;
    contactFirstName?: string;
    contactLastName?: string;
    transitAccountId?: string;
    subsystem?: string;
    subsystemAccountReference?: string;
    isRegistered?: boolean;
    isVerified?: boolean;
    isPrimaryContact?: boolean;
    createdById?: string;
    modifiedById?: string;
    channelName?: string;
    transcriptUrl?: string;
    notes?: string;
    createdOnDateTime?: string;
    modifiedOnDateTime?: string;
    createdByUserFullName?: string;
    modifiedByUserFullName?: string;
    endDateTime?: string;
    startDateTime?: string;
    durationTime?: string;
    status?: string;   
    activities?: string;
    cases?: string;
    linkSessionsId?: string;
}

export interface SessionActivities {
    groupId?: string;
    groupNumber?: string;
    referenceNumber?: string;
    activityId?: string;
    customerId?: string;
    channelName?: string;
    subject?: string;
    status?: string;
    createdByName?: string;
    createdOnDateTime?: string;
    activityType?: string;
}

export interface SessionCases{
    createdOnDateTime?: string;
    caseNumber?: string;
    priority?: string;
    origin?: string;
    status?: string;
    customerName?: string;
    customerId?: string;
    ownerName?: string;
    ownerId?: string;
    title?: string;
    isEscalated?: boolean;
}

export class ActiveUsers {
    Error: string;
    Users: object
}

//DATA SERVICE TYPES
export interface Cub_Globals {
    [globalName: string]: {
        [detailName: string]: string | number
    }
}

export interface Cub_GridMap {
    [gridName: string]: Cub_CustomizableGrid
}

export interface BusyScreen {
    busyCount: number,
    busyText: string,
    buttonText: string,
    buttonRoute: string,
    icon: string,
    smallLoader?: boolean
}

export interface CancelScreen {
    show: boolean,
    cancelButtonRoute: string
}

export interface ErrorScreen {
    show: boolean,
    errors: string[],
    route: string,
    fatalState: boolean,
    errorTotal: number
}

export interface MasterSearchValues {
    contactType?: string,
    customerType?: string,
    firstName?: string,
    lastName?: string,
    phone?: string,
    email?: string,
    tokenType?: string,
    subSystem?: string,
    bankCardNumber?: string,
    expirationDate?: string,
    transitAccountNumber?: string
}

export interface VerifiedCache {
    start: moment.Moment,
    contacts: string[]
}

export interface WSAddress {
    address1: string,
    address2?: string,
    address3?: string
    city: string,
    state?: string,
    stateId?: string,
    country?: string,
    countryId?: string,
    postalCode?: string,
    postalCodeId?: string,
    addressId?: string,
    isExistingAddress?: boolean
}

export interface AddressDependency {
    key: string,
    value: string,
    type: string,
    update: boolean
}

/**
 * Used by Cub_TokenComponent
 */
export class Token {
    tokenInfo: string;
    nickname: string;
    status: string;
    lastUsed: Date | string;
    subsystemId: string;
    accountId: string
}

export interface TapsHistorySearchCriteria {
    accountId: string,
    subSystem: string,
    startDateTime?: string,
    endDateTime?: string,
    travelMode?: string,
    status?: string,
    eventList?: string,
    operatorList: string,
    sortBy?: string,
    offset?: number,
    limit?: number
}

export interface WSWebResponseHeader {
    result: string,
    uid?: string,
    fieldName?: string,
    errorKey?: string,
    errorMessage?: string
}

export interface WSGetCustomerOrderHistoryResponse {
    hdr: WSWebResponseHeader,
    orders: WSOrderHistoryLineItem[],
    totalCount: number
}

export interface WSOrderHistoryLineItem {
    orderId: number,
    orderDateTime: string,
    orderLastUpdate?: string,
    orderType: string,
    orderTypeDescription: string,
    orderStatus: string,
    orderStatusDescription: string,
    orderTotalAmount: number,
    paymentStatus: string,
    paymentStatusDescription: string
}

export class WSErrorResponse {
    Message: string;
    Origin: string;
    Data: string;
    CorrelationId: string;
    StackTrace: string;
}

export interface MasterSearchContactCache {
    customerType: string,
    firstName: string,
    lastName: string,
    phone: string,
    email: string
}

export interface ResolveBalanceArgs {
    customerId?: string,
    unregisteredEmail?: string,
    clientRefId?: string,
    subsystem: string,
    subsystemAccountReference: string,
    userId?: string
}

export interface DelinkCustomerArgs {
    subsystemId?: string,
    subsystemAccount?: string,
    customerId?: string,
    userId?: string
}

export interface BlockUnblockTokenArgs {
    subsystemId?: string,
    subsystemAccount?: string,
    customerId?: string,
    oneaccountId?: string,
    block?: boolean
}

export interface BlockUnblockAccountArgs {
    subsystemId?: string,
    subsystemAccount?: string,
    block?: boolean
}

export interface WSBlockUnblockAccountResponse {
    body: string,
    Header: WSWebResponseHeader    
}

export interface WSDebtCollectResponse {
    orderId: string,
    responseCode: string, //OrderAccepted, Completed, CompletedWithErrors, or FulfillmentFailed
    paymentRefNbr?: string,
    paymentResults: WSPaymentResult[],
    errors?: WSOrderError[]
}

export interface WSPaymentResult {
    responseCode: string,
    authRefNbr?: string,
    bankAuthCode?: string,
    authDateTime?: string,
    payment: any
}

export interface WSOrderError {
    errorKey?: string,
    errorMessage?: string
}

export interface WSDebtCollectionInfo {
    action: string,
    amountDue: number,
    bankcardCharged?: WSDebtCollectionBankcard,
    attemptInSeconds?: number,
    remainingAttemptsForDay?: number
}

export interface WSDebtCollectionBankcard {
    maskedPan: string,
    cardExpiryMMYY: string
}

export interface WSOneAccountSummaryResponse {
    hdr?: any,
    oneAccountId: number,
    accountStatus: any,
    purses: any[],
    linkedAccounts: WSSubsystemAccountInfo[]
}

export interface WSSubsystemAccountInfo extends Anything {
    nickname?: string,
    subsystemAccountReference: string,
    subsystemInfo: any,
    subsystemAccountDetailedInfo?: WSSubsystemAccountDetailedInfo
}

export interface WSSubsystemAccountDetailedInfo {
    accountStatus: string,
    accountStatusDescription: string,
    accountStatusCause?: string,
    accountType?: string,
    accountTypeDescription?: string,
    tokens: WSSubsystemAccountToken[],
    purses?: any[],
    accountAlerts?: string[],
    lastChangedStatusDTM: string,
    WSDebtCollectionInfo?: WSDebtCollectionInfo
}

export interface WSSubsystemAccountToken extends Anything {
    subsystemTokenType: string,
    subsystemTokenTypeDescription: string,
    tokenInfo: WSTravelTokenDisplay,
    status?: string,
    statusDescription: string,
    statusChangeDateTime?: string
}

export interface WSTravelTokenDisplay {
    tokenType: string,
    tokenNickname?: string,
    lastUseDTM: string
}

export interface WSBankcardTravelTokenDisplay extends WSTravelTokenDisplay {
    maskedPAN: string,
    cardExpiryMMYY: string
}

export interface WSSmartcardTravelTokenDisplay extends WSTravelTokenDisplay {
    serialNumber: string,
    cardExpiryDate: string
}

export interface WSMobileTravelTokenDisplay extends WSTravelTokenDisplay {
    serializedMobileToken: string
}

export interface WSSubsystemStatusResponse extends Anything {
    accountStatus: string,
    accountStatusDescription: string,
    accountStatusChangeDateTime?: string,
    riderClass: string,
    riderClassDescription: string,
    accountCreatedDateTime?: string,
    tokens: WSTransitAccountToken[],
    passes?: WSTransitAccountPass[],
    purses: WSSubsystemPurse[],
    accountLastUsageDetails?: WSLastUsageDetails,
    foreignCustomerId?: string,
    tapCorrectionInfo?: WSTapCorrectionInfo,
    debtCollectionInfo?: WSDebtCollectionInfo
}

export interface WSTransitAccountToken {
    subsystemTokenType: string,
    subsystemTokenTypeDescription: string,
    tokenInfo: WSTravelTokenDisplay,
    status?: string,
    statusDescription?: string,
    statusChangeDateTime?: string,
    tokenLastUsageDetails?: WSLastUsageDetails
}

export interface WSTransitAccountPass {
    passSKU: string,
    passDescriptionn: string,
    createdDateTime?: string,
    startDateTime?: string,
    endDateTime?: string,
    remainingDuration?: number,
    durationType?: string,
    expirationDateTime: string,
    deactivatedDateTime?: string,
    deactivatedReason?: string,
    passSerialNbr: string,
    fareProductTypeId: string,
    supportsAutoload: boolean
}

export interface WSSubsystemPurse {
    nickname?: string,
    balance: number,
    purseType: string,
    purseTypeDescription: string,
    purseRestriction: string,
    purseRestrictionDescription: string
}

export interface WSLastUsageDetails {
    transactionDateTime: string,
    device: string,
    status: string,
    statusDescription: string,
    location: string
}

export interface WSTapCorrectionInfo {
    totalIncompleteCount: number,
    provisionalIncompleteCount: number,
    manualFillCount: number,
    autoFillCount: number
}

export interface MvcGenericResponse {
    Header: {
        result: string,
        uid: string,
        fieldName: string,
        errorKey: string,
        errorMessage: string,
    },
    Body: string
}

export interface EntityReference {
    Id: string,
    Name: string,
    LogicalName: string
}

export interface WSCubTransitAccount {
    Id: string,
    TransitAccountId: string,
    SubsystemId: string,
    Customer?: EntityReference,
    Contact?: EntityReference
}

export interface WSCUbOrder {
    orderId: number,
    origin: string,
    orderDateTime: string,
    insertedBy: string,
    orderLastUpdate: string,
    lastUpdatedBy: string,

}