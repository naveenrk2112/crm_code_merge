﻿import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_SubsystemService } from '../services/cub_subsystem.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { BlockUnblockTokenArgs, WSBlockUnblockAccountResponse, CubError, Cub_DropdownOption } from '../model';

import * as _ from 'lodash';
import * as Model from '../model';

@Component({
    selector: 'cub-subsystem-acct-tkn-block-unblock',
    templateUrl: './cub_subsytemaccounttkn_block_unblock.component.html',
    providers: [Cub_SubsystemService]
})
export class Cub_SubsystemAccountTknBlockUnblockComponent implements OnInit {
    requestBody: BlockUnblockTokenArgs = {};
    responseBody: WSBlockUnblockAccountResponse = {} as any;
    reasonCodeControl: Model.Cub_Dropdown = {} as any;
    email: Model.Cub_Textbox = {} as any;
    notes: string = "";
    tokenAction: string = "";

    showSaveModal: boolean = false;
    showNotesError: boolean = false;
    showErrorsModal: boolean = false;
    showConfirmBlockTerminatePage: boolean = false;
    showTerminatePrompt: boolean = false;

    completeProcess: boolean = false;

    blockReasonCodeDesc: string = "";
    unblockReasonCodeDesc: string = "";
    terminateReasonCodeDesc: string = "";
    reasonCodeTypes: any = {};
    reasonCodeTypeId: any = {};
    reasonCodeSelected: any = {};
    reasonCodeDescription: string[] = [];

    @Input() subsystemAccount: string;
    @Input() subsystemId: string;
    @Input() customerId: string;
    @Input() tokenId: string = '';
    @Input() block: boolean;
    @Input() terminate: boolean;
    @Output() onClose = new EventEmitter<any>();

    constructor(
        public _cub_dataService: Cub_DataService,
        public _cub_subsystemService: Cub_SubsystemService,
        private _loadingScreen: AppLoadingScreenService,
        private _router: Router,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.blockReasonCodeDesc = globals['CUB.MSD.Web.Angular']['ReasonCode_BlockToken'] as string;
                this.unblockReasonCodeDesc = globals['CUB.MSD.Web.Angular']['ReasonCode_UnblockToken'] as string;
                this.terminateReasonCodeDesc = globals['CUB.MSD.Web.Angular']['ReasonCode_TerminateToken'] as string;

                if (!this.blockReasonCodeDesc) {
                    this.blockReasonCodeDesc = "BlockToken";
                }

                if (!this.unblockReasonCodeDesc) {
                    this.unblockReasonCodeDesc = "UnblockToken";
                }

                if (!this.terminateReasonCodeDesc) {
                    this.terminateReasonCodeDesc = "TerminateToken";
                }

                if (this.terminate) {
                    this.reasonCodeDescription = [this.terminateReasonCodeDesc];
                    this.tokenAction = 'Terminate';
                }
                else {
                    if (this.block) {
                        this.reasonCodeDescription = [this.blockReasonCodeDesc, this.terminateReasonCodeDesc];
                        this.tokenAction = 'Block';
                    }
                    else {
                        this.reasonCodeDescription = [this.unblockReasonCodeDesc];
                        this.tokenAction = 'Unblock';
                    }
                }


                this.getReasonCodeTypes();
            });

        //"Block" : "Unblock";
        this.reasonCodeControl = {
            id: 'reasonCodeControl',
            formFieldClass: 'form-field-drop-down',
            fieldWidthClass: 'form-field-width-L',
            showErrorMessage: true,
            errorMessage: 'Reason Code is required.',
            isRequired: true,
            value: '',
            options: []
        };

        this.email = {
            id: 'email',
            formFieldClass: 'form-field-text',
            fieldWidthClass: 'form-field-width-85',
            value: '',
            isDisabled: false,
            showErrorMessage: true,
            errorMessage: 'Email is required.',
            isRequired: !this.customerId ? true : false
        }

        this.showConfirmBlockTerminatePage = true;
    }

    //getContactInfo() {
    //    if (this.contactId) {
    //        const done = this._loadingScreen.add('Getting Contact information...');
    //        this._cub_SubsystemService.GetContactNameEmailById(this.contactId)
    //            .then((data) => {
    //                var contact = JSON.parse(data);
    //                this.contactName = `${contact.FirstName} ${contact.LastName}`;
    //                this.contactEmail = contact.Email;
    //            })
    //            .catch((error: any) => {
    //                this._cub_dataService.prependCurrentError('Error getting Contact info');
    //            })
    //            .then(() => done());
    //    }
    //}

    getReasonCodeTypes() {
        this._cub_subsystemService.getReasonCodes()
            .then((data) => {
                if (data) {
                    this.reasonCodeTypes = JSON.parse(data.Body);
                }
            })
            .then(() => {

                this.reasonCodeControl.options = [{
                    value: "0",
                    id: "0",
                    name: "Select...",
                    isDisabled: true,
                    notesRequired: false
                } as Cub_DropdownOption];

                const len = this.reasonCodeDescription.length;
                for (let i = 0; i < len; i++) {
                    const rtName = this.reasonCodeDescription[i];
                    this.reasonCodeControl.options = this.reasonCodeControl.options.concat(_.find<any>(
                        this.reasonCodeTypes.reasonCodeTypes,
                        rct => rct['name'] === rtName).reasonCodes.map(rc => ({
                            value: rc.reasonCodeId,
                            name: rc.description,
                            notesRequired: rc.notesMandatoryFlag,
                            typeName: rtName
                        })));
                }
            })
            .catch((error: any) => {
                this._cub_dataService.prependCurrentError('Error getting reason Codes');
            });
    }

    onReasonCodeChanged() {
        this.reasonCodeSelected = _.find<any>(this.reasonCodeControl.options, ['value', parseInt(this.reasonCodeControl.value)]);
        this.reasonCodeControl.isErrored = false;
        if (this.reasonCodeSelected.typeName === this.terminateReasonCodeDesc) {
            this.tokenAction = 'Terminate';
            this.showTerminatePrompt = true;
        }
        else
        {
            if (this.reasonCodeSelected.typeName === this.blockReasonCodeDesc) {
                this.tokenAction = 'Block';
            }

            this.showTerminatePrompt = false;
        }
    }

    submitClicked() {
        if (!this.validateSubmit()) return;
        
        if (this.reasonCodeSelected.typeName === this.terminateReasonCodeDesc) {
            this.showTerminatePrompt = true;
        }

        this.processRequest();
    }

    processRequest() {
        this._cub_subsystemService.tokenAction(this.subsystemAccount, this.subsystemId, this.tokenId, this.customerId, this.reasonCodeSelected.value, this.notes, this.tokenAction)
            .then(data => {
                if (data != null) {
                    this.responseBody = data;
                    if (data.Header.errorMessage) {
                        this.showErrorsModal = true;
                    }
                    else {
                        this.showConfirmBlockTerminatePage = false;
                        this.showSaveModal = true;
                    }  
                }
            })
            .catch(err => {
                if (err instanceof CubError && err.handled) {
                    return;
                }
                this._cub_dataService.onApplicationError(err);
            })
    }

    validateSubmit() {
        if (this.reasonCodeSelected == null) {
            this.reasonCodeControl.showErrorMessage = true;
            this.reasonCodeControl.isErrored = true;
            return false;
        }

        if (this.reasonCodeSelected.notesRequired && !this.notes) {
            this.showNotesError = true;
            return false
        }
        else {
            this.showNotesError = false;
            return true;
        }
    }

    okClicked() {
        if (this.responseBody == null) {
            let error = ['Something went wrong while attempting to ', this.reasonCodeDescription, ' transit token ', this.requestBody.subsystemAccount, '.'].join('');
            this._cub_dataService.onApplicationError(error);
            return;
        }

        this.showSaveModal = false;

        if (this.responseBody.Header.result === 'Successful') {
            this.closeDialog(true);
        }
        else {
            this.closeDialog(false);
        }
    }

    okErrors() {
        this.showErrorsModal = false;
        let shouldRefresh: boolean = false;

        window['Mscrm'].Utilities.setReturnValue({
            refresh: shouldRefresh
        });
        window['closeWindow']();
    }

    cancelClicked() {
        this.showConfirmBlockTerminatePage = false;
        this.closeDialog(false);
    }

    closeDialog(shouldRefresh: boolean) {
        const result = {
            refresh: shouldRefresh
        };
        if (this._router.url.match(/^\/SubsystemAccountTknBlockUnblock/)) {
            window['Mscrm'].Utilities.setReturnValue(result);
            window['closeWindow']();
        } else {
            this.onClose.emit(result);
        }
    }
}