import { Component, Input, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { Tam_TransactionsService } from '../services/tam_transactions.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { DataTableModule, SharedModule, LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-tam-transactions',
  templateUrl: './tam-transactions.component.html',
  styleUrls: ['./tam-transactions.component.css'],
  providers: [Tam_TransactionsService]
})
export class Tam_TransactionsComponent implements OnInit {

    transactions: any;
    rowsPerPage: number = 10;

    @Input() customerId: string;

    constructor(
        private _tam_transactionsservice: Tam_TransactionsService,
        private _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) { }


    ngOnInit() {
        const done = this._loadingScreen.add("Loading Transactions...");
        let queryParams = this._route.snapshot.queryParams;
        this.customerId = queryParams['customerId'];

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'])
            .subscribe(globals => {
                this.rowsPerPage = globals['CUB.MSD.Web.Angular']['TAM.Transponders.NumRows'] as number;
            });
        this._tam_transactionsservice.getTransactions(this.customerId)
            .then((data: any) => {
                if (data && data.trxnHistory) {
                    this.transactions = data.trxnHistory;
                }
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error loading transactions'))
            .then(() => done());
    }
}
