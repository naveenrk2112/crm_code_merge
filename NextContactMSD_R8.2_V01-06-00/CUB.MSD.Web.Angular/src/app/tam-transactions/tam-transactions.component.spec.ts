import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Tam_TransactionsComponent } from './tam-transactions.component';

describe('TamTransactionsComponent', () => {
    let component: Tam_TransactionsComponent;
    let fixture: ComponentFixture<Tam_TransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [Tam_TransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Tam_TransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
      pending();
    expect(component).toBeTruthy();
  });
});
