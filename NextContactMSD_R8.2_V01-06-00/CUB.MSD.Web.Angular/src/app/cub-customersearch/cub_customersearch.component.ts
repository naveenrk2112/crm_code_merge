﻿import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Cub_CustomerSearchService } from '../services/cub_customersearch.service';
import { Cub_DataService } from '../services/cub_data.service';
import { Cub_MasterSearchService } from '../services/cub_mastersearch.service';
import { Cub_SessionService } from '../services/cub-session.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { Cub_Button, Cub_Textbox, Cub_Dropdown, VerificationResult, Cub_Globals } from '../model';

import * as lodash from 'lodash';
import * as $ from 'jquery';

@Component({
    selector: 'cub-customersearch',
    templateUrl: './cub_customersearch.component.html',
    providers: [Cub_CustomerSearchService, Cub_MasterSearchService]
})
export class Cub_CustomerSearchComponent implements OnInit {
    constructor(
        private _cub_dataService: Cub_DataService,
        private _cub_customerSearchService: Cub_CustomerSearchService,
        private _cub_masterSearchService: Cub_MasterSearchService,
        private _cub_sessionService: Cub_SessionService,
        private _loadingScreen: AppLoadingScreenService,
        private route: ActivatedRoute,
        private router: Router,
        private _renderer: Renderer2
    ) { }

    searchControlsColumn: Array<string>;
    searchControls: any;
    defaultCustomerType: string;

    //For Duplicates modal
    sortBy: string;
    sortOrder: string;
    sortOrderOptions: Array<Object>;
    sortOrderControl: Cub_Dropdown;

    rowsOnDuplicateModal: number = 3;

    //The results of the search are stored here for the duplicates popover to show, if needed
    duplicateSearch = {
        results: [] as any[], //Duplicate results
        sortedResults: [] as any[], //The sorted order to display them in
        show: false as boolean
    };

    showVerification: boolean = false;
    get selectedContact() {
        return this._cub_dataService.selectedContactCustomerToVerify;
    }
    set selectedContact(value: any) {
        this._cub_dataService.selectedContactCustomerToVerify = value;
    }
    redirectEntity: string;

    ngOnInit() {
        let height = $('#navTabGroupDiv', top.document).css('height');
        $('#crmTopBar', top.document).hide();
        // NOTE: this currently gets overridden by MSD on initial page load
        $('#crmContentPanel', top.document).css('top', height);
        const bodyTag = document.getElementsByTagName('body')[0];
        this._renderer.addClass(bodyTag, 'grey-background');

        this.router.navigate([
            '/CustomerSearch',
            { outlets: { session: 'Session' } }
        ], { replaceUrl: true });

        //The controls for the page (Note that we can add service calls for any of these 'hard coded' values that load on page load)
        this.searchControls = {
            customerType: {
                type: 'Dropdown',
                id: 'customer-type',
                label: 'Customer Type',
                isRequired: true,
                errorMessage: 'Customer type is required',
                fieldWidthClass: 'form-field-width-100',
                isTableControl: true,
                formFieldClass: 'form-fields-siblings',
                options: [],
                value: null
            } as Cub_Dropdown,
            firstName: {
                type: "Textbox",
                id: "firstName",
                label: "First Name",
                isRequired: true,
                errorMessage: "First name is required",
                fieldWidthClass: "form-field-width-100",
                isTableControl: true,
                formFieldClass: 'form-fields-siblings',
                value: '',
            } as Cub_Textbox,
            lastName: {
                type: "Textbox",
                id: "lastName",
                label: "Last Name",
                isRequired: true,
                errorMessage: "Last name is required",
                fieldWidthClass: "form-field-width-100",
                isTableControl: true,
                formFieldClass: 'form-fields-siblings',
                value: '',
            } as Cub_Textbox,
            email: {
                type: "Textbox",
                id: "email",
                label: "Email",
                isRequired: true,
                errorMessage: 'Email is required',
                fieldWidthClass: "form-field-width-100",
                isTableControl: true,
                formFieldClass: 'form-fields-siblings',
                value: '',
            } as Cub_Textbox,
            phone: {
                type: "Textbox",
                id: "phone",
                label: "Phone",
                isRequired: true,
                errorMessage: 'Phone number is required',
                fieldWidthClass: 'form-field-width-60',
                spacerWidthClass: 'form-field-width-40',
                isTableControl: true,
                formFieldClass: 'form-fields-siblings',
                value: '',
                placeholderChar: '_',
                mask: []
            } as Cub_Textbox
        };

        //Initialize controls in this order on screen
        this.searchControlsColumn = ['customerType', 'firstName', 'lastName', 'email', 'phone'];

        //Used for Duplicates popover
        this.sortBy = "BestMatch";
        this.sortOrder = "asc";
        this.sortOrderOptions = [
            { value: "BestMatch", name: "Best Match" },
            { value: "CustomerName", name: "Customer Name" },
            { value: "FullName", name: "Contact Full Name" },
            { value: "FirstName", name: " Contact First Name" },
            { value: "LastName", name: "Contact Last Name" },
            { value: "Email", name: "Email" }
        ];
        this.sortOrderControl = {
            formFieldClass: 'form-fields-vertical', //Form field class to use, eg. form-fields-siblings
            labelWidthClass: 'form-field-width-S', //Width of label
            fieldWidthClass: "form-field-width-XL", //Width of Control
            isTableControl: false,
            isSmall: true,
            id: 'sortOrderControl', //unique control id
            label: 'Sort by', //Label for control
            value: "BestMatch", //Value for control
            options: this.sortOrderOptions //Array of objects: {name: "Label Text", value: "Value"}
        } as Cub_Dropdown;

        this._cub_dataService.Globals
            .first(globals => !!globals['CUB.MSD.Web.Angular'] && !!globals['CustomerRegistration'])
            .subscribe(globals => this.loadConstants(globals));

        this.searchControls.customerType.value = this._cub_dataService.searchValues['customerType'];
        this.searchControls.firstName.value = this._cub_dataService.searchValues['firstName'];
        this.searchControls.lastName.value = this._cub_dataService.searchValues['lastName'];
        this.searchControls.phone.value = this._cub_dataService.searchValues['phone'];
        this.searchControls.email.value = this._cub_dataService.searchValues['email'];
        for (let fieldKey in this.searchControls) {
            if (fieldKey === 'customerType') {
                continue;
            }
            if (this.searchControls[fieldKey].value) {
                this.validate();
                break;
            }
        }

        const done = this._loadingScreen.add();
        this._cub_masterSearchService.getAccountTypes()
            .then(value => this.processAccountTypes(value))
            .catch(error => this._cub_dataService.onApplicationError(error))
            .then(() => done());
    }

    loadConstants(globals: Cub_Globals) {
        this.rowsOnDuplicateModal = globals['CUB.MSD.Web.Angular']['MasterSearch.Duplicates.NumRows'] as number;

        //Customer Type configuration
        if (globals.MasterSearch.CustomerType_DefaultValue) {
            this.defaultCustomerType = globals.CustomerRegistration.CustomerType_DefaultValue as string;
            if (!this._cub_dataService.searchValues['customerType']) {
                this._cub_dataService.searchValues['customerType'] = this.defaultCustomerType;
            }
            if (!this.searchControls.customerType.value) {
                this.searchControls.customerType.value = this.defaultCustomerType;
            }
        }

        //First Name Configuration
        if (globals.CustomerRegistration.FirstName_MaxLength) {
            this.searchControls.firstName.maxLength = globals.CustomerRegistration.FirstName_MaxLength;
        }

        //Last Name Configuration
        if (globals.CustomerRegistration.LastName_MaxLength) {
            this.searchControls.lastName.maxLength = globals.CustomerRegistration.LastName_MaxLength;
        }

        //Phone Configuration
        if (globals.CustomerRegistration.Phone_Mask) {
            let value = globals.CustomerRegistration.Phone_Mask as string;
            const characterMaskList = value.split('|')
                .map(char => char.length > 1 ? new RegExp(char) : char);

            // For some reason the text-mask sets the input to "(___) ___-____"
            // if the first character doesn't match rather than leave it blank, so
            // we use this function to force a blank input if the first character is
            // unrelated to a phone number. This function fires when the input value 
            // changes. It accepts the updated input value and returns a list of character masks.
            // https://github.com/text-mask/text-mask/blob/master/componentDocumentation.md#mask-function
            const determineMask = (rawInput) => {
                if (!rawInput) {
                    return characterMaskList;
                }
                // first character should be 1-9 or satisfy the first character mask
                if (!rawInput[0].match(/[1-9]/)
                    && ((typeof characterMaskList[0] === 'string' && rawInput[0] !== characterMaskList[0])
                        || (characterMaskList[0] instanceof RegExp && !rawInput[0].match(characterMaskList[0])))
                ) {
                    return [/[1-9]/];
                }
                return characterMaskList;
            };
            // We have to reassign the object to update the mask field.
            this.searchControls.phone = Object.assign({}, this.searchControls.phone, { mask: determineMask });
        }

        //Email configuration
        if (globals.CustomerRegistration.Email_MaxLength) {
            this.searchControls.email.maxLength = globals.CustomerRegistration.Email_MaxLength;
        }
    }

    processAccountTypes(data: any) {
        //let options = [];
        //for (var i = 0; i < data.length; i++) {
        //    var accountTypes = data[i];
        //    options.push({
        //        value: accountTypes['Key'],
        //        name: accountTypes['Value']
        //    });
        //}
        this.searchControls.customerType.options = lodash.map(data, d => ({
            value: d['Key'],
            name: d['Value']
        }))//options;
        if (!this.searchControls.customerType.value && this.searchControls.customerType.options.length) {
            this.searchControls.customerType.value = this.searchControls.customerType.options[0]['Key'];
        }
    }

    //Handle sorting of duplicates popover
    onSortDropdownChanged(event: any) {
        if (!event || event.length <= 0) {
            return;
        }
        var selectedValue = event[0];
        this.sortResults(selectedValue, true);
    }

    sortResults(field: string, inAscendingOrder: boolean) {
        if (field.toLocaleLowerCase() === "BestMatch".toLocaleLowerCase()) {
            this.duplicateSearch.sortedResults = this.duplicateSearch.results;
            return;
        }
        if (!this.duplicateSearch.results ||
            !this.duplicateSearch.results[0] ||
            !this.duplicateSearch.results[0][field]) {
            console.log("Error while sorting results");
            return;
        }
        var sortOrder = 'desc'
        if (inAscendingOrder) {
            sortOrder = 'asc';
        }
        this.duplicateSearch.sortedResults = lodash.orderBy(this.duplicateSearch.results, [field], ['asc']);
    }

    //If a duplicate's 'verify' is clicked or the row is double clicked, load verification popover
    verifyContact(record: any, redirectEntity) {
        this.selectedContact = record;
        this.redirectEntity = redirectEntity;
        this.showVerification = true;
    }

    //Handle clicking ignore duplicates
    ignoreDuplicatesClicked(event: any) {
        this.duplicateSearch.show = false;
        this.router.navigate(['/CustomerRegistration']);
    }

    //Handle clicking cancel duplicates
    cancelDuplicatesClicked(event: any) {
        this.duplicateSearch.show = false;
    }

    //Handle clicking cancel on search form
    cancelButtonClicked() {
        if (this._cub_dataService.previousPage) {
            this.router.navigateByUrl(this._cub_dataService.previousPage);
        }
    }

    //When the enter button is pressed on a focused control
    onEnterPress(event: any) {
        this.searchButtonClicked();
    }

    //Handle clicking 'Create Customer' on search form
    searchButtonClicked() {
        var isValidated = this.validate();
        if (isValidated) {
            const done = this._loadingScreen.add('Searching for duplicates...');
            this._cub_customerSearchService.searchCustomer()
                .then((data: any[]) => {
                    if (data && data.length > 0) {
                        this.duplicateSearch.results = data;
                        this.duplicateSearch.sortedResults = data;
                        this.duplicateSearch.show = true;
                    }
                    else {
                        this._cub_dataService.busyScreen.busyText = 'No duplicates found';
                        this._cub_dataService.busyScreen.icon = 'check';
                        ++this._cub_dataService.busyScreen.busyCount;
                        setTimeout(() => {
                            --this._cub_dataService.busyScreen.busyCount;
                            this.router.navigate(['CustomerRegistration']);
                        }, 500);
                    }
                })
                .catch(error => {
                    this._cub_dataService.prependCurrentError("Error searching customers");
                })
                .then(() => done());
        }
    }

    onTextChange(data: any = null) {
        //Make sure to put search terms in a common object for other pages to grab and autopopulate their fields if needed when navigated to
        if (data) {
            var newValue = data[0];
            var textControl = data[1];
            this._cub_dataService.searchValues[textControl['id']] = newValue;
        }
        
        this.validate();
    }

    //Handle how required/validation erorrs are shown to the user
    validate() {
        var isValidated = true;
        
        for (let key in this.searchControls) {
            let control = this.searchControls[key];
            if (control.isRequired && !control.value) {
                control.isErrored = true;
                control.isInvalid = false;
                control.showErrorMessage = true;
                isValidated = false;
            }
            else if (control.validationRegEx && control.value && !control.validationRegEx.test(control.value)) {
                // Currently never reached, no fields have validation regex specified
                control.isErrored = false;
                control.isInvalid = true;
                control.showErrorMessage = true;
                isValidated = false;
            }
            else {
                control.isErrored = false;
                control.isInvalid = false;
                control.showErrorMessage = false;
            }
        }

        return isValidated;
    }

    resolveVerification(event: VerificationResult) {
        this.showVerification = false;
        if (event === VerificationResult.CANCEL) {
            return;
        }

        const done = this._loadingScreen.add('Loading session');
        const recordId = this.selectedContact[this.redirectEntity === 'contact' ? 'ContactId' : 'AccountId'];

        let optionalSessionPromise: Promise<any> = Promise.resolve();
        if (!this._cub_sessionService.isActive) {
            optionalSessionPromise = optionalSessionPromise.then(() =>
                this._cub_sessionService.createSession('contact', this.selectedContact['ContactId']));
        }
        else if (!this._cub_sessionService.session.isRegistered) {
            const updatedSession = {
                id: this._cub_sessionService.session.sessionId,
                cub_customerid: { id: this.selectedContact['AccountId'] },
                cub_contactid: { id: this.selectedContact['ContactId'] },
                cub_isregistered: true,
                cub_isverified: event === VerificationResult.VERIFIED,
                cub_isprimarycontact: true // TODO: is this handled server-side? if not, figure out how to determine
            }
            optionalSessionPromise = optionalSessionPromise.then(() =>this._cub_sessionService.updateSession(updatedSession));
        }

        optionalSessionPromise
            .then(() => this._cub_dataService.openMsdForm(this.redirectEntity, recordId))
            .catch(err => this._cub_dataService.onApplicationError(err))
            .then(() => done());
    }
}