﻿import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import { DebugElement } from '@angular/core';

import { DataTableModule } from 'primeng/primeng';
import { TextMaskModule } from 'angular2-text-mask';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';

import { Cub_CustomerSearchComponent } from './cub_customersearch.component';
import { Cub_ControlsColumnComponent } from '../controls/cub-controlscolumn/cub_controlscolumn.component';
import { Cub_ControlLabelComponent } from '../controls/cub-controllabel/cub_controllabel.component';
import { Cub_DropdownComponent } from '../controls/cub-dropdown/cub_dropdown.component';
import { Cub_TextboxComponent } from '../controls/cub-textbox/cub_textbox.component';
import { Cub_OptionSelectComponent } from '../controls/cub-optionselect/cub_optionselect.component';
import { Cub_ButtonComponent } from '../controls/cub-button/cub_button.component';
import { Cub_HelpComponent } from '../controls/cub-help/cub_help.component';
import { Cub_AlertComponent } from '../controls/cub-alert/cub_alert.component';
import { Cub_SecurityOptionComponent } from '../controls/cub-securityoption/cub_securityoption.component';
import { Cub_CustomerVerificationComponent } from '../cub-customerverification/cub_customerverification.component';
import { Cub_SessionModule } from '../cub-session/cub_session.module';

import { Cub_TooltipDirective } from '../cub-tooltip/cub-tooltip.directive';

import { PhonePipe } from '../pipes/phone.pipe';
import { HighlightPipe } from '../pipes/highlight.pipe';

import { Cub_DataService } from '../services/cub_data.service';
import { Cub_CustomerSearchService } from '../services/cub_customersearch.service';
import { Cub_SessionService } from '../services/cub-session.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';

import { AppLoadingScreenServiceStub } from '../testutilities/app-loading-screen.service.stub';
import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';
import { Cub_CustomerSearchServiceStub } from '../testutilities/cub-customer-search.service.stub';
import { Cub_SessionServiceStub } from '../testutilities/cub_session.service.stub';
import { ActivatedRouteStub, RouterStub } from '../testutilities/route.stub';

describe('CubCustomerSearchComponent', () => {
    let comp: Cub_CustomerSearchComponent;
    let fixture: ComponentFixture<Cub_CustomerSearchComponent>;
    let root: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                NguiDatetimePickerModule,
                TextMaskModule,
                DataTableModule,
                Cub_SessionModule,
                RouterModule.forRoot([{ path: 'CustomerSearch', component: Cub_CustomerSearchComponent }])
            ],
            declarations: [
                Cub_ControlsColumnComponent,
                Cub_ControlLabelComponent,
                Cub_DropdownComponent,
                Cub_TextboxComponent,
                Cub_OptionSelectComponent,
                Cub_ButtonComponent,
                Cub_HelpComponent,
                Cub_AlertComponent,
                Cub_CustomerVerificationComponent,
                Cub_SecurityOptionComponent,
                Cub_CustomerSearchComponent,
                Cub_TooltipDirective,
                PhonePipe,
                HighlightPipe
            ],
            providers: [
                { provide: Cub_DataService, useClass: Cub_DataServiceStub },
                { provide: Cub_SessionService, useClass: Cub_SessionServiceStub },
                { provide: AppLoadingScreenService, useClass: AppLoadingScreenServiceStub },
                { provide: Router, useClass: RouterStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub }
            ]
        }).overrideComponent(Cub_CustomerSearchComponent, {
            set: {
                providers: [
                    { provide: Cub_CustomerSearchService, useClass: Cub_CustomerSearchServiceStub }
                ]
            }
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Cub_CustomerSearchComponent);
        comp = fixture.componentInstance;
        root = fixture.debugElement;
    })

    it('Should create the component', (() => {
        expect(comp).toBeTruthy();
    }));
});