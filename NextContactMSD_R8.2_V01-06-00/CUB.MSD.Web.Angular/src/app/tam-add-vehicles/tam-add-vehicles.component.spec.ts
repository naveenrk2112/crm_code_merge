import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Tam_AddVehiclesComponent } from './tam-add-vehicles.component';

describe('Tam_AddVehiclesComponent', () => {
    let component: Tam_AddVehiclesComponent;
    let fixture: ComponentFixture<Tam_AddVehiclesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [Tam_AddVehiclesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Tam_AddVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
