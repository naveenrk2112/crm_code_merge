import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';

import { Tam_AccountInformationComponent } from './tam-accountinformation.component';
import { Cub_DataService } from '../services/cub_data.service';

import { Cub_DataServiceStub } from '../testutilities/cub_data.service.stub';

describe('Tam_AccountInformationComponent', () => {
    let component: Tam_AccountInformationComponent;
    let fixture: ComponentFixture<Tam_AccountInformationComponent>;

  beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
            HttpClientTestingModule
        ],
        declarations: [
            Tam_AccountInformationComponent
        ],
        providers: [
            { provide: Cub_DataService, useClass: Cub_DataServiceStub },
            { provide: ActivatedRoute, useValue: { snapshot: { queryParams: {} } } }
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(Tam_AccountInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

    //TODO: change "xit" to "it" once ready to start unit testing, disabled for now due to causing unit tests failures as this is still being developed
  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
