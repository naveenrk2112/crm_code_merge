import { Component, Input, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cub_DataService } from '../services/cub_data.service';
import { Tam_AccountInformationService } from '../services/tam_accountinformation.service';
import { AppLoadingScreenService } from '../app-loading-screen/app-loading-screen.service';
import { DataTableModule, SharedModule, LazyLoadEvent, SortMeta } from 'primeng/primeng';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'app-tam-accountinformation',
    templateUrl: './tam-accountinformation.component.html',
    providers: [Tam_AccountInformationService]
})
export class Tam_AccountInformationComponent implements OnInit {

    @Input() customerId: string;

    accountSummary: any;

    constructor(
        private _tam_accountinformationservice: Tam_AccountInformationService,
        private _cub_dataService: Cub_DataService,
        private _loadingScreen: AppLoadingScreenService,
        private _route: ActivatedRoute
    ) {}

    ngOnInit() {
        const done = this._loadingScreen.add("Loading Account Summary...");
        let queryParams = this._route.snapshot.queryParams;
        this.customerId = queryParams['customerId'];

        this._tam_accountinformationservice.getAccountSummary(this.customerId)
            .then((data: any) => {
                if (data && data.accountSummary) {
                    this.accountSummary = data.accountSummary;
                }
            })
            .catch(err => this._cub_dataService.prependCurrentError('Error loading account summary'))
            .then(() => done());
    }
}
