export const environment = {
    production: true,
    REQUEST_URL_WITHIN_MSD: true,
    CUB_MSD_WEB_URL: 'http://usdced-msdyn01.cts.cubic.cub:12663'
};
