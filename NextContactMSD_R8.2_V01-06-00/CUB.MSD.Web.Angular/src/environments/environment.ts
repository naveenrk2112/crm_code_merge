// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// HINT: right-click a property -> Find All References

export const environment = {
    production: false,
    // when true, apiServerUrl will be updated when loading within MSD
    REQUEST_URL_WITHIN_MSD: true,
    // apiServerUrl default value
    CUB_MSD_WEB_URL: 'http://usdced-msdyn01.cts.cubic.cub:12663'
};
