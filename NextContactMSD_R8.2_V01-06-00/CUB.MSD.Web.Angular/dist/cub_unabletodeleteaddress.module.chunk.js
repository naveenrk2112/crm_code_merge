webpackJsonp(["cub_unabletodeleteaddress.module"],{

/***/ "../../../../../src/app/cub-unabletodeleteaddress/cub_unabletodeleteaddress.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"hybrid modal-small unsupported\">\r\n    <ul class=\"window modal-width-S unable-to-delete\">\r\n        <li class=\"header center\">\r\n            <h1>Unable To Delete</h1>\r\n        </li>\r\n        <li class=\"body center\">\r\n            <div>\r\n                <p>The address cannot be deleted because it is currently used by a contact or funding source. To delete the address, first remove it from the contact or funding source:</p>\r\n                <ul class=\"list-update\">\r\n                    <li *ngFor=\"let record of dependencies\">\r\n                        <button type=\"button\">\r\n                            <ul>\r\n                                <li>\r\n                                    <p>{{record.title}}</p>\r\n                                </li>\r\n                                <li>\r\n                                    <p *ngIf=\"record.allowUpdate == 1\"><a (click)=\"updateClicked($event, record)\">Update</a></p>\r\n                                </li>\r\n                            </ul>\r\n                        </button>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n        </li>\r\n        <li class=\"footer\">\r\n            <ul class=\"buttons\">\r\n                <li>\r\n                    <button type=\"button\" class=\"button-primary\" (click)=\"closeModal($event)\">\r\n                        <p>OK</p>\r\n                    </button>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n    </ul>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/cub-unabletodeleteaddress/cub_unabletodeleteaddress.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Cub_UnableToDeleteAddressComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Cub_UnableToDeleteAddressComponent = (function () {
    function Cub_UnableToDeleteAddressComponent(route, router) {
        this.route = route;
        this.router = router;
        this.dependencies = []; //Each object contains type, id, title, allowUpdate
    }
    Cub_UnableToDeleteAddressComponent.prototype.updateClicked = function (event, record) {
        var data = {
            action: record.type == 'contact' ? 'openContact' : 'openFundingSource',
            id: record.id
        };
        parent.postMessage(JSON.stringify(data), '*');
    };
    Cub_UnableToDeleteAddressComponent.prototype.closeModal = function (event) {
        var data = {
            action: 'closeModal'
        };
        parent.postMessage(JSON.stringify(data), '*');
    };
    Cub_UnableToDeleteAddressComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.dependencies = [];
            var i = 1;
            while (true) {
                var record = params['record' + i];
                if (!record) {
                    break;
                }
                var details = record.split('|');
                if (details.length != 4) {
                    break;
                }
                _this.dependencies.push({
                    type: details[0],
                    id: details[1],
                    title: details[2],
                    allowUpdate: details[3]
                });
                i++;
            }
        });
    };
    return Cub_UnableToDeleteAddressComponent;
}());
Cub_UnableToDeleteAddressComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'cub-unabletodeleteaddress',
        template: __webpack_require__("../../../../../src/app/cub-unabletodeleteaddress/cub_unabletodeleteaddress.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]) === "function" && _b || Object])
], Cub_UnableToDeleteAddressComponent);

var _a, _b;
//# sourceMappingURL=cub_unabletodeleteaddress.component.js.map

/***/ }),

/***/ "../../../../../src/app/cub-unabletodeleteaddress/cub_unabletodeleteaddress.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cub_UnableToDeleteAddressModule", function() { return Cub_UnableToDeleteAddressModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cub_unabletodeleteaddress_component__ = __webpack_require__("../../../../../src/app/cub-unabletodeleteaddress/cub_unabletodeleteaddress.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var Cub_UnableToDeleteAddressModule = (function () {
    function Cub_UnableToDeleteAddressModule() {
    }
    return Cub_UnableToDeleteAddressModule;
}());
Cub_UnableToDeleteAddressModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild([
                { path: '', component: __WEBPACK_IMPORTED_MODULE_3__cub_unabletodeleteaddress_component__["a" /* Cub_UnableToDeleteAddressComponent */] }
            ])
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__cub_unabletodeleteaddress_component__["a" /* Cub_UnableToDeleteAddressComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]]
    })
], Cub_UnableToDeleteAddressModule);

//# sourceMappingURL=cub_unabletodeleteaddress.module.js.map

/***/ })

});
//# sourceMappingURL=cub_unabletodeleteaddress.module.chunk.js.map