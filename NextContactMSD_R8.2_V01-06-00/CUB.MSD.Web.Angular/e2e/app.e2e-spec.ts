import { CubicAngularPage } from './app.po';

describe('cubic-angular App', () => {
  let page: CubicAngularPage;

  beforeEach(() => {
    page = new CubicAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual("app works!");
  });
});
