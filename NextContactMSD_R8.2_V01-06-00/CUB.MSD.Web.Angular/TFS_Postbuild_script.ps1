#Script to run in TFS build to run npm

#test
# cd \Builds\58\CTSDev1\NextContact_MSD_CTFS01\src\NextContact-MSD\Dev\NextContactMSD\CUB.MSD.Web.Angular
#Must add nodejs to path
$env:Path += ";C:\Program Files\nodejs"

#Change to Angular project folder
cd NextContact-MSD\Dev\NextContactMSD\CUB.MSD.Web.Angular

#Location of node_modules folder (outside of build)
$sourceRoot = "D:\Source\NextContactMSD\CUB.MSD.Web.Angular\node_modules"

#Copy the node_modules folder into build folder
Copy-Item -Path $sourceRoot -Recurse -Container

try
{
	npm run distprod -- --env=Release --progress=false
	Write-Host "npm finished" 
	exit 0
}
Catch [ex]
{
  Write-Error "npm error = " $_
  exit 1
}

