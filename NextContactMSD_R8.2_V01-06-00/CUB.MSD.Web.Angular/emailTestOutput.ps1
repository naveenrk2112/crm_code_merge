#
# emailTestOutput.ps1
#
$sender = "James.Lusk@cubic.com"
$recipients = "James.Lusk@cubic.com", "Tony.Riak@cubic.com", "Saqib.Saadi@cubic.com", "Erik.Petersen@cubic.com", "Gor.Baghdasaryan@cubic.com", "Jose.Santos@cubic.com", "Paul.Lohmuller@cubic.com", "Miles.Hastings@cubic.com", "Rob.Montague@cubic.com"
$mail_subject = "Deployment Angular Unit Test Results"

$fileContents = [IO.File]::ReadAllText(".\test\results.html")
if ($fileContents) { 
	$fileTimeStamp = (Get-Item .\test\results.html).LastWriteTime
	if($fileTimeStamp -lt (Get-Date).AddMinutes(-1)) {
		echo ".\test\results.html is stale and will not be resent"
		exit 0
	}
    Send-MailMessage -From $sender -Subject $mail_subject -To $recipients -Body $fileContents -BodyAsHtml -SmtpServer "smtp.cubic.com"
	echo "Email sent"
	exit 0
} else {
	echo "Could not read .\test\results.html!"
	exit 1
}