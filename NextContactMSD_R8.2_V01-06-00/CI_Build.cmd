@echo off

:: get and save the arguments in named parameter variables now
::set TF_BUILD_SOURCESDIRECTORY=%1
set TF_BUILD_SOURCESDIRECTORY=.

:: set pertinent environment variables
set TF_BUILD_METRICS_DIR=C:\Apps\sonar-scanner-2.6.1
set TF_BUILD_LOG_FILE=%TF_BUILD_SOURCESDIRECTORY%\CI_Build.log
::set TF_BUILD_LOG_FILE=C:\Builds\CI_Build.log


:: copy outputs from build
:: xcopy %TF_BUILD_BINARIESDIRECTORY% %TF_BUILD_BIN_DIR% /E /Y

:: enable visual studio commands
:: echo enable visual studio commands >> %TF_BUILD_LOG_FILE%
::if not defined VSINSTALLDIR call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\vsvars32.bat" >> %TF_BUILD_LOG_FILE%

:: run the unit tests

:: run SonarQube
echo. > %TF_BUILD_LOG_FILE%
echo %DATE% >> %TF_BUILD_LOG_FILE%
echo %TIME% >> %TF_BUILD_LOG_FILE%
echo %TF_BUILD_SOURCESDIRECTORY% >> %TF_BUILD_LOG_FILE%
echo.Starting SonarQube Analysis >> %TF_BUILD_LOG_FILE%
set JAVA_HOME=C:\Apps\jdk1.8.0_71
set PATH=%JAVA_HOME%\bin;%PATH%

cd %TF_BUILD_SOURCESDIRECTORY%
dir >> %TF_BUILD_LOG_FILE%

echo Calling Sonar scanner for static analysis >> %TF_BUILD_LOG_FILE%
::call %TF_BUILD_METRICS_DIR%\bin\sonar-scanner.bat -Dsonar.language=cs -Dsonar.projectKey=cubic.cts.nextcontact.MSD.SonarKey -Dsonar.projectName=C#::NextContactMSD >> %TF_BUILD_LOG_FILE%
echo Calling Sonar scanner for SourceMeter analysis >> %TF_BUILD_LOG_FILE% 
call %TF_BUILD_METRICS_DIR%\bin\sonar-scanner.bat -Dsonar.language=csharp ^
-Dsonar.exclusions=CUB.MSD.Model/CUB.MSD.Model.cs,CUB.MSD.Model/CUB.MSD.OptionSets.cs,ConsoleApp/*,Database/*,CUB.MSD.Plugins.Test/*,CUB.MSD.UnitTest/*,CUB.MSD.WCF.Library.Tests/*,CUB.MSD.WCF.Tests/*,CUB.MSD.Web.Tests/*,WixCustomActions/*  ^
-Dsonar.projectKey=nextcontactsMSDSonarKey ^
-Dsonar.projectName=C#::NextContactMSD:sm -Dsonar.sources=. -Dsonar.projectVersion=1.0  -Dsm.csharp.input=NextContactMSD.sln -Dsm.csharp.configuration=Release -Dsm.csharp.platform=AnyCPU >> %TF_BUILD_LOG_FILE%

:: exit
echo End >> %TF_BUILD_LOG_FILE%
echo %DATE% >> %TF_BUILD_LOG_FILE%
echo %TIME% >> %TF_BUILD_LOG_FILE%
echo Review build log: %TF_BUILD_LOG_FILE% >> %TF_BUILD_LOG_FILE%
exit /b

