/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CUB.MSD.Web.Models.TransitAccount
{
    public class BalanceHistoryViewModel
    {
        public string AccountId { set; get; }
        public string Subsystem { set; get; }
        public string StartDate { set; get; }
        public string EndDate { set; get; }
    }
}
