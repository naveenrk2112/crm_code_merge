/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CUB.MSD.Web.Models.MasterSearch
{
    public class SearchCardActionModel
    {
        public string TokenType { set; get; }
        public string SubSystem { set; get; }
        public string BankCardNumber { set; get; }
        public string ExpirationYear { set; get; }
        public string ExpirationMonth { set; get; }

        /// <summary>
        /// (Required)  The encrypted card details. 
        /// �Cubic DirectPay format JWE Encrypted JSON payload�. See Cubic DirectPay specification for Details of creating the encrypted payload.
        /// </summary>
        public string encryptedToken { set; get; }

        /// <summary>
        /// (Required)  The name of the key used to encrypt the token. Valid values are project specific.
        /// e.g. �MobileKeyNameV1�
        /// </summary>
        public string keyName { set; get; }

        /// <summary>
        /// (Required)  The system generated security token.
        /// </summary>
        public string nonce { get; set; }
    }
}
