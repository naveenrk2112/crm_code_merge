﻿var TransitAccount = {}

TransitAccount._TransitAccountParams = {};

TransitAccount.SearchTransitAccount = function (accountId, subsystem, balanceStartDate, travelStartDate) {
    TransitAccount._TransitAccountParams = {
        "AccountId": accountId,
        "Subsystem": subsystem,
    };
    TransitAccountStatus.SearchTransitAccountStatus();
    TransitAccountBalanceHistory.SearchTransitAccountBalanceHistory(balanceStartDate);
    TransitAccountTravelHistory.SearchTransitAccountTravelHistory(travelStartDate);
}
