/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CUB.MSD.Core;
using Microsoft.AspNet.Identity;
using CUB.MSD.Logic;
using CUB.MSD.Model;
using static CUB.MSD.Model.CUBConstants;
using CUB.MSD.Web.Models.TransitAccount;
using System.Net.Http;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Web.Controllers
{
    public class TransitAccountController : BaseController
    {
        // GET: TransitAccount
        public ActionResult Index()
        {
            //if (User?.Identity != null && !User.Identity.IsAuthenticated)
            //{
            //    throw new Exception("User Not Authenticated");
            //}
            //string userName = User.Identity.Name;
            //string userName = "CARCGNT\\202421";
            //Guid? userId = null;

            //using (var connMgr = new CrmConnectionManager())
            //{
            //    UserLogic userLogic = new UserLogic(connMgr.Service);
            //    userId = userLogic.GetCrmUserId(userName);
            //}

            return View();
        }

        #region Account Status
        //http://localhost:12663/TransitAccount/AccountStatus?accountId=330000008624&subSystem=ABP
        [HttpGet]
        public ActionResult AccountStatus(string accountId, string subSystem)
        {
            AccountStatusViewModel viewModel = new AccountStatusViewModel();
            viewModel.AccountId = accountId;
            viewModel.Subsystem = subSystem;
            return View(viewModel);
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetAccountStatus()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Retrieve the status of an existing transit account associated to a subsystem
        /// </summary>
        /// <param name="accountID">Transit account ID</param>
        /// <param name="subSystem">Subssystem</param>
        /// <returns>Transit account status</returns>
        [HttpGet]
        public ActionResult GetAccountStatus(string accountId, string subSystem)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountStatusByID(accountId, subSystem);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Balance history
        //http://localhost:12663/TransitAccount/BalanceHistory?accountId=330000008624&subSystem=ABP
        [HttpGet]
        public ActionResult BalanceHistory(string accountId, string subSystem)
        {
            string startDate;
            string endDate = DateTime.Today.ToString("yyyy-MM-dd");
            using (var connMgr = new CrmConnectionManager())
            {
                int days = int.Parse(GlobalsCache.GetKeyFromCache(connMgr.Service, "MasterSearchConstants", "AccountBalanceHistoryStartDateDefault").ToString());
                startDate = DateTime.Today.AddDays(-days).ToString("yyyy-MM-dd");
            }

            BalanceHistoryViewModel viewModel = new BalanceHistoryViewModel();
            viewModel.AccountId = accountId;
            viewModel.Subsystem = subSystem;
            viewModel.StartDate = startDate;
            viewModel.EndDate = endDate;
            return View(viewModel);
        }

        // function used by MasterSearch and may not be needed
        [HttpGet]
        public ActionResult GetBalanceHistory(string accountId, string subSystem, string startDate, string endDate)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                string result = nis.GetTransitAccountBalanceHistory(accountId, subSystem, startDate, endDate);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        #region Transit Account Balance History

        [HttpOptions]
        public HttpResponseMessage GetTransitAccountBalanceHistory()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get the balance history for Transit Account. This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// NIS API call: 2.15.2 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/balancehistory GET
        /// </summary>
        /// <param name="accountId">(Required) Unique identifier for the Transit Account. </param>
        /// <param name="subSystem">(Required)  Unique identifier for the SubSystem</param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the transactions are required for, defaults to 7 days ago.  Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.  Required when startDateTime is given.</param>
        /// <param name="entryType">(Optional) The unique identifier of the entry type. </param>
        /// <param name="entrySubType">(Optional)  The unique identifier of the entry sub type (for charge it is the charge type, for loads it is the load type). </param>
        /// <param name="purseType">(Optional) Type of purse it is.</param>
        /// <param name="reversal">(Optional) Flag showing if the transaction is a reversal.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10.</param>
        /// <returns>One Account Travel History as JSON</returns>
        [HttpGet]
        public ActionResult GetTransitAccountBalanceHistory(string accountId, string subSystemId, string startDateTime, string endDateTime,
                                             string entryType, string entrySubType, string purseType, bool? reversal,
                                             string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountBalanceHistory(accountId, subSystemId, startDateTime, endDateTime, entryType, entrySubType, purseType, reversal, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Travel history
        //http://localhost:12663/TransitAccount/TravelHistory?accountId=330000008624&subSystem=ABP
        [HttpGet]
        public ActionResult TravelHistory(string accountId, string subSystem)
        {
            string startDate;
            string endDate = DateTime.Today.ToString("yyyy-MM-dd");
            using (var connMgr = new CrmConnectionManager())
            {
                int days = int.Parse(GlobalsCache.GetKeyFromCache(connMgr.Service, "MasterSearchConstants", "AccountTravelHistoryStartDateDefault").ToString());
                startDate = DateTime.Today.AddDays(-days).ToString("yyyy-MM-dd");
            }

            TravelHistoryViewModel viewModel = new TravelHistoryViewModel();
            viewModel.AccountId = accountId;
            viewModel.Subsystem = subSystem;
            viewModel.StartDate = startDate;
            viewModel.EndDate = endDate;
            return View(viewModel);
        }

        [HttpOptions]
        public HttpResponseMessage GetTravelHistory()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get the travel history for Transit Account. This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// NIS API call: 2.14.3 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/travelhistory GET
        /// </summary>
        /// <param name="accountId">(Required) Unique identifier for the Transit Account. </param>
        /// <param name="subSystemId">(Required)  Unique identifier for the SubSystem</param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the transactions are required for, defaults to 7 days ago.  Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.  Required when startDateTime is given.</param>
        /// <param name="travelMode">(Optional) Comma separated list of the travel mode IDs to return the history of, defaults to all travel modes. See section 2.6.9 for allowable values.</param>
        /// <param name="tripCategory">(Optional)  Comma separated list of the trip category enums to return the history of, defaults to all categories.
        ///                            Allowable values:
        ///                            - All
        ///                            - Correctable
        ///                            - Finalized
        ///                            - Incomplete
        ///                            - Auto-Filled
        ///                            - Manual-Filled
        ///                            - Complete
        ///    </param>
        /// <param name="viewType">(Optional) View type enum to return the history of, defaults to Simple type.</param>
        /// <param name="tripStatus">(Optional) The unique identifier of the trip status.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10.</param>
        /// <returns>One Account Travel History as JSON</returns>
        [HttpGet]
        public ActionResult GetTravelHistory(string accountId, string subSystemId, string startDateTime, string endDateTime,
                                             string travelMode, string tripCategory, string viewType, string tripStatus,
                                             string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountTravelHistory(accountId, subSystemId, startDateTime, endDateTime, 
                                                                travelMode, tripCategory, viewType, tripStatus,
                                                                sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        public ActionResult TransitAccount(string accountId, string subSystem)
        {
            TransitAccountViewModel viewModel = new TransitAccountViewModel();
            viewModel.AccountId = accountId;
            viewModel.Subsystem = subSystem;
            return View(viewModel);
        }

        [HttpOptions]
        public HttpResponseMessage GetTravelHistoryDetail()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/TransitAccount/GetTravelHistoryDetail?transitaccountId=440000001006&subsystemId=ABP&tripId=1118
        /// <summary>
        /// Get the travel history detail information for an account by transaction ID. 
        /// NIS API call: 2.16.4 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/transaction/<transaction-id>/travelhistorydetail GET
        /// </summary>
        /// <param name="transitaccountId">(Required) Unique identifier for the transit account. </param>
        /// <param name="subsystemId">(Required) Subsystem ID</param>
        /// <param name="tripId">(Required) Trip ID to return the details for.</param>
        /// <returns>Transit Account Travel history Detail as JSON</returns>
        /// 
        [HttpGet]
        public ActionResult GetTravelHistoryDetail(string transitaccountId, string subsystemId, string tripId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountTravelHistoryDetail(transitaccountId, subsystemId, tripId);
                var ret = Json(result, JsonRequestBehavior.AllowGet);
                return ret;
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetStatusHistory()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get the status history information for an account by transitaccount ID. 
        /// NIS API call: 2.14.8 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/statushistory GET
        /// </summary>
        /// <param name="transitaccountId">(Required) Unique identifier for the transit account. </param>
        /// <param name="subsystemId">(Required) Subsystem ID</param>
        /// <returns>Transit Account Status history as JSON</returns>
        /// 
        [HttpGet]
        public ActionResult GetStatusHistory(string transitaccountId, string subsystemId, string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountStatusHistory(transitaccountId, subsystemId, sortBy, offset, limit);
                var ret = Json(result, JsonRequestBehavior.AllowGet);
                return ret;
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetTokenStatusHistory()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get the token status history information for token by token ID. 
        /// NIS API call: 2.16.9 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/traveltoken/<token-id>/statushistory GET
        /// </summary>
        /// <param name="transitaccountId">(Required) Unique identifier for the transit account. </param>
        /// <param name="subsystemId">(Required) Subsystem ID</param>
        /// <param name="tokenId">(Required) Token ID</param>
        /// <returns>Token Status history as JSON</returns>
        /// 
        [HttpGet]
        public ActionResult GetTokenStatusHistory(string subsystemId, string tokenId, string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTokenStatusHistory(subsystemId, tokenId, sortBy, offset, limit);
                var ret = Json(result, JsonRequestBehavior.AllowGet);
                return ret;
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetActivities()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get MSD Activities associated with a Transit Account
        /// </summary>
        /// <param name="transitAccountId">Transit account ID</param>
        /// <returns>A list of Transit Account activities, in JSON format</returns>
        [HttpGet]
        public ActionResult GetActivities(string transitAccountId)
        {
            if (string.IsNullOrEmpty(transitAccountId))
            {
                throw new HttpException(400, "Transit Account ID unspecified");
            }
            using (var ccm = new CrmConnectionManager())
            {
                var logic = new ActivityLogic(ccm.Service);
                var activities = logic.GetByTransitAccount(transitAccountId);
                return Json(activities, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetTASubsystemActivities()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/TransitAccount/GetTASubsystemActivities?transitAccountId=4C167FB1-085F-4547-82F7-4C87B693F09F&subsystemId=4C167FB1-085F-4547-82F7-4C87B693F09F
        /// <summary>
        /// This API is used to retrieve activity history that was associated with the a transit account and a subsystem based upon filtering parameters.
        /// 2.8.21 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/activity GET
        /// </summary>
        /// <param name="transitAccountId">Transit Account Id</param>
        /// <param name="subsystemId">Subsystem Id</param>
        /// <param name="customerId">Customer Id - passed in but not used</param>
        /// <param name="contactId">(Optional) The contact Id for which the activity should be shown.</param>
        /// <param name="startDateTime">(Optional) The date and time that should be used to filter the starting date of the range. 
        ///     If not provided then the default should be configurable with a default value of now � 30 days.</param>
        /// <param name="endDateTime">(Optional)  The date and time that should be used to filter the ending date of the range.
        ///     If not provided then the default should be the beginning of next day. 
        ///     The maximum end date should be as number of days from the start date.</param>
        /// <param name="type">(Optional)  An identifier for the type of activity to return in the results.
        ///     The default is to return all activity types if no value is provided.</param>
        /// <param name="activitySubject">(Optional)  An identifier for the subject of activity to return in the results. 
        ///     Activity subject is a logical grouping of activity types. The default is to return all activity subject if no value is provided.</param>
        /// <param name="activityCategory">(Optional)  An identifier for the category of activity to return in the results. 
        ///     Category is used to distinguish between event and audit. The default is to return all activity category if no value is provided.</param>
        /// <param name="channel">(Optional) Used to filter the activities by channel.</param>
        /// <param name="origin">(Optional)  An identifier for the origin of activity to return in the results. 
        ///     Origin tells the component which initiated the action which caused the activities to be generated. The default is to return all activity origin if no value is provided.</param>
        /// <param name="globalTxnId">(Optional)  An identifier for the category of activity to return in the results. 
        ///     Global transaction id is a mechanism to tie multiple method being called as part of a single action. The default is to return all activity category if no value is provided.</param>
        /// <param name="userName">(Optional) Used to filter the activities by the username that performed the activity.
        ///     Allow for partial string with wild card �*�.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �type.asc,channel.desc� indicates sort by type ascending and status descending.
        ///     Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) Used to identify the starting index that should be provided within the page in the search result.
        ///     The default is to start at the first row of data (0) if no value is provided.</param>
        /// <param name="limit">(Optional) Used to identify the number of rows per page in the search result in the response.
        ///     The default is to return 20 rows if no value is provided.</param>
        /// <returns>Transit Account Subsystem Activities</returns>
        [HttpGet]
        public ActionResult GetTASubsystemActivities(string transitAccountId, string subsystemId, 
                                            string customerId, string contactId, string startDateTime, string endDateTime,
                                            string type, string activitySubject, string activityCategory, string channel, 
                                            string origin, string globalTxnId, string userName, string crmSessionId,
                                            string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTASubsystemActivities(transitAccountId, subsystemId, customerId, contactId, 
                                                        startDateTime, endDateTime, type, activitySubject, activityCategory, 
                                                        channel, origin, globalTxnId, userName, crmSessionId, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetTransitAccount()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Retrieve an MSD Transit Account record. If one does not yet exist,
        /// it will be created and then returned.
        /// </summary>
        /// <param name="transitAccountId">Transit account ID</param>
        /// <param name="subsystemId">Subsystem ID</param>
        /// <param name="customerId">MSD Customer GUID string, only used if record does not yet exist and transit account is linked to customer's OneAccount</param>
        /// <returns>WSCubTransitAccount</returns>
        [HttpPost]
        public ActionResult GetTransitAccount(string transitAccountId, string subsystemId, string customerId, Guid? userId)
        {
            if (string.IsNullOrEmpty(transitAccountId))
            {
                throw new ArgumentNullException("transitAccountId");
            }
            if (string.IsNullOrEmpty(subsystemId))
            {
                throw new ArgumentNullException("subsystemId");
            }

            using (var ccm = new CrmConnectionManager(userId))
            {
                var logic = new TransitAccountLogic(ccm.Service);
                var ta = logic.GetTransitAccount(transitAccountId, subsystemId);
                if (ta == null)
                {
                    Guid parsedId;
                    EntityReference customerRef = null;
                    if (!string.IsNullOrEmpty(customerId) && Guid.TryParse(customerId, out parsedId))
                    {
                        customerRef = new EntityReference(Model.Account.EntityLogicalName, parsedId);
                    }
                    ta = logic.CreateTransitAccount(transitAccountId, subsystemId, customerRef);
                }
                return Json(ta, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetTravelHistoryTaps()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        //http://localhost:12663/TransitAccount/GetTravelHistoryTaps?accountId=330000008624&subSystem=ABP
        /// <summary>
        /// 2.14.5 transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/travelhistory/taps GET
        /// The API to get the tap history for a transitaccount. This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// </summary>
        /// <param name="accountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystemId">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="startDateTime">(Conditionally-Required)  Starting date and time for the period the transactions are required for, defaults to 7 days ago.
        ///                             Required when endDateTime is given. Time when tap occurred.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.
        ///                           Required when startDateTime is given. Time when tap occurred.</param>
        /// <param name="travelMode">(Optional) Comma separated list of the travel mode IDs to return the history of, defaults to all travel modes. See section 2.6.9 for allowable values.</param>
        /// <param name="status">(Optional) Comma separated list of the status IDs to return the travel history of, defaults to all statuses.</param>
        /// <param name="eventList">(Optional) Comma separated list of the event IDs to return the travel history of, defaults to all events.</param>
        /// <param name="operatorList">(Required) Comma separated list of Unique identifier of the operators.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: 
        ///                      �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime. </param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10.</param>
        /// <returns>Travel History Taps as JSON</returns>
        [HttpGet]
        public ActionResult GetTravelHistoryTaps(string accountId, string subSystem, string startDateTime, string endDateTime,
                                                 string travelMode, string status, string eventList, string operatorList, 
                                                 string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountTravelHistoryTaps(accountId, subSystem, startDateTime, endDateTime, 
                                                                    travelMode, status, eventList, operatorList, 
                                                                    sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //http://localhost:12665/TransitAccount/getBankCardChargeHistory?accountId=330000008624&subSystem=ABP
        /// <summary>
        /// The API to get the bankcard charge payment history for a transitaccount. 
        /// This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// </summary>
        /// <param name="accountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystem">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the transactions are required for, defaults to 7 days ago.  Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.  Required when startDateTime is given.</param>
        /// <param name="dateType">(Optional) Type of date filter to apply on. Possible values are dateOpened and dateSettled. Defaults to dateOpened.</param>
        /// <param name="status">(Optional)  Comma separated list of the payment status IDs.  Defaults to all statuses.</param>
        /// <param name="response">(Optional) Comma separated list of the response IDs, Defaults to all responses.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional)  The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional)  The number of records per page, defaults to 10.</param>
        /// <returns>Card Charge History JSON response</returns>
        [HttpGet]
        public ActionResult getBankCardChargeHistory(string accountId, string subSystem, 
                                                     string startDateTime, string endDateTime, 
                                                     string dateType, string status, string response,
                                                     string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountCardChargeHistory(accountId, subSystem, startDateTime, endDateTime, 
                                                                    dateType, status, response, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage getBankCardChargeHistory()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }



        //http://localhost:12665/TransitAccount/getBankChargeAggregationDetails?accountId=330000008624&subSystem=ABP&paymentId=1
        /// <summary>
        /// The endpoint is to get the aggregated details such as bankcard charge details, journal entries and the trip details.
        /// This information is all clubbed together to help client get all the information they need to help answer questions related to 
        /// charge, payment and trip.
        /// </summary>
        /// <param name="accountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystem">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="paymentId">(Required)  Unique id for the bankcard charge payment transaction.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult getBankChargeAggregationDetails(string accountId, string subSystem, string paymentId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.getBankChargeAggregationDetails(accountId, subSystem, paymentId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage getBankChargeAggregationDetails()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        //http://localhost:12665/TransitAccount/getOrderHistory?transitAccountId=330000008624&subSystemId=ABP
        /// <summary>
        /// This API is used to retrieve the order history for a subsystem transit account.
        /// </summary>
        /// <param name="transitAccountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystemId">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the orders are required for.
        ///                             Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required)  Ending date and time for the period the orders are required for.
        ///                           Required when startDateTime is given.</param>
        /// <param name="orderType">(Optional) Filters by the order type.
        /// Valid types are:
        ///  - Sale
        ///  - Adjustment
        ///  - AutoloadEnrollment
        ///  - CloseAccount
        ///  - Autoload
        /// </param>
        /// <param name="orderStatus">(Optional) Filters by the status of the order:
        /// Valid statuses are:
        ///	 - OrderAccepted
        ///	 - PaymentFailed
        ///  - FulfillmentFailed
        ///  - Completed
        ///  - CompletedWithErrors
        ///  - PaymentAccepted
        /// </param>
        /// <param name="paymentStatus">(Optional) Filters by the payment status.
        /// Valid statuses are:
        ///  - Pending
        ///  - Authorized
        ///  - FailedAuthorization
        ///  - Confirmed
        ///  - FailedConfirmation
        ///  - PendingConfirmation
        ///  - ConfirmationExpired
        /// </param>
        /// <param name="orderId">(Optional) Filter by the order id entered.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: 
        /// �orderDateTime.asc,orderType.desc� indicates sort by orderDateTime ascending and orderType descending.
        /// Default is descending by orderDateTime.
        /// Supported sort fields: 
        ///  - orderDateTime
        ///  - orderType
        ///  - orderStatus
        ///  - paymentStatus
        ///  - orderId
        /// </param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10. Value should be between 1 and 9999, inclusive.</param>
        /// <returns>Order history</returns>
        [HttpGet]
        public ActionResult getOrderHistory(string transitAccountId, string subSystemId,
                                            string startDateTime, string endDateTime,
                                            string orderType, string orderStatus, string paymentStatus, int? orderId, bool? realtimeResults,
                                            string sortBy, int? offset, int? limit, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountOrderHistory(transitAccountId, subSystemId, startDateTime, endDateTime,
                                                               orderType, orderStatus, paymentStatus, orderId, realtimeResults,
                                                               sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage getOrderHistory()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }


        /// <summary>
        /// This API is used to request the the order history details for an order. This search provides an option to search in the realtime data source 
        /// by setting the optional filter realtimeResults to true. 
        /// This is to offer customer service the ability to troubleshoot orders that are not completely processed yet. 
        /// However, this option should be used in rare cases and some information will not be available in the search results.  
        /// </summary>
        /// <param name="transitAccountId">(Required) The unique identifier of the transit account.</param>
        /// <param name="subSystemId">(Required) Unique identifier for the subsystem to which transit account belongs.</param>
        /// <param name="orderid">(Required) Unique id for the order
        /// <param name="realtimeResults">(Optional)  Indicates if the results should be retrieved from a realtime source (i.e: OMS). By default this filter is set to false.
        [HttpGet]
        public ActionResult getOrderDetatail(string transitAccountId, string subSystemId,
                                           string orderid, bool? realtimeResults, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountOrderDetail(transitAccountId, subSystemId, orderid, realtimeResults);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage getOrderDetatail()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpOptions]
        public HttpResponseMessage GetTravelModes()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetTravelModes(string subsystem)
        {
            using (var conn = new CrmConnectionManager())
            {
                var nis = new NISApiLogic(conn.Service);
                var result = nis.GetTravelModesBySubsystem(subsystem);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetTravelOperators()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetTravelOperators(string subsystem)
        {
            using (var conn = new CrmConnectionManager())
            {
                var nis = new NISApiLogic(conn.Service);
                var result = nis.GetTravelOperatorsBySubsystem(subsystem);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetTapTypes()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetTapTypes(string subsystem)
        {
            using (var conn = new CrmConnectionManager())
            {
                var nis = new NISApiLogic(conn.Service);
                var result = nis.GetTapTypesBySubsystem(subsystem);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetTapStatuses()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetTapStatuses(string subsystem)
        {
            using (var conn = new CrmConnectionManager())
            {
                var nis = new NISApiLogic(conn.Service);
                var result = nis.GetTapStatusesBySubsystem(subsystem);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetLocations()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/TransitAccount/GetLocations?subsystem=ABP
        /// <summary>
        /// Return the location list for the given subsystem
        /// </summary>
        /// <param name="subsystem">subsystem</param>
        /// <returns>List of locations</returns>
        [HttpGet]
        public ActionResult GetLocations(string subsystem, string serviceOperator)
        {
            using (var conn = new CrmConnectionManager())
            {
                var nis = new NISApiLogic(conn.Service);
                var result = nis.GetLocationsBySubsystem(subsystem, serviceOperator);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
