/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Web.Models;
using CUB.MSD.Web.Models.MasterSearch;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class MasterSearchController : BaseController
    {
        // GET: MasterSearch
        [HttpGet]
        public ActionResult Index()
        {
            using (var connMgr = new CrmConnectionManager())
            {
                ViewBag.CRM_URL = GlobalsCache.GetKeyFromCache(connMgr.Service, "CUB.MSD.WEB", "CRM.URL");
                int balanceHistoryDays = int.Parse(GlobalsCache.GetKeyFromCache(connMgr.Service, "MasterSearchConstants", "AccountBalanceHistoryStartDateDefault").ToString());
                ViewBag.BalanceHistoryStartDate = DateTime.Today.AddDays(-balanceHistoryDays).ToString("yyyy-MM-dd");
                int TravelHistoryDays = int.Parse(GlobalsCache.GetKeyFromCache(connMgr.Service, "MasterSearchConstants", "AccountTravelHistoryStartDateDefault").ToString());
                ViewBag.TravelHistoryStartDate = DateTime.Today.AddDays(-TravelHistoryDays).ToString("yyyy-MM-dd");
            }
            return View();
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage SearchCard()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpPost]
        public ActionResult SearchCard(SearchCardActionModel model, Guid? userId)
        {
            CubLogger.InfoFormat("SearchCardActionModel: {0}", JsonConvert.SerializeObject(model));
            using (var connMgr = new CrmConnectionManager(userId))
            {
                CardLogic cardLogic = new CardLogic(connMgr.Service);
                string result = cardLogic.SearchToken(model.SubSystem, model.TokenType, model.BankCardNumber,
                                                      model.ExpirationMonth, model.ExpirationYear,
                                                      model.encryptedToken, model.nonce);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Retrieve the status of an existing transit account associated to a subsystem
        /// </summary>
        /// <param name="accountID">Transit account ID</param>
        /// <param name="subSystem">Subssystem</param>
        /// <returns>Transit account status</returns>
        [HttpPost]
        public ActionResult GetAccountStatus(string accountId, string subSystem, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetTransitAccountStatusByID(accountId, subSystem);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetBalanceHistory(string accountId, string subSystem, string startDate, string endDate, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                string result = nis.GetTransitAccountBalanceHistory(accountId, subSystem, startDate, endDate);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage SearchContact()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpPost]
        public ActionResult SearchContact(CustomerSearchViewModel contactSearchData, Guid? userId)
        {
            //string firstName, string lastName, string email, string phone
            using (var connMgr = new CrmConnectionManager(userId))
            {
                ContactLogic logic = new ContactLogic(connMgr.Service);
                var res = logic.SearchByFirstLastPhoneEmail(
                    contactSearchData.FirstName, 
                    contactSearchData.LastName, 
                    contactSearchData.Email, 
                    contactSearchData.Phone);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SearchCase(string caseNumber, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                CaseLogic caseLogic = new CaseLogic(connMgr.Service);
                var res = caseLogic.CastEntityCollectionToString(caseLogic.SearchByCaseId(caseNumber));
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
