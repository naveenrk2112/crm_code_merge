/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class CrmInteractionController : BaseController
    {
        // GET: CrmInteraction
        public ActionResult Index()
        {
            return View();
        }
    }
}
