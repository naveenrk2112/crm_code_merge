/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model;
using CUB.MSD.Model.NIS;
using Microsoft.Crm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class CustomerVerificationController : BaseController
    {
        // TODO: remove all HttpOptions

        [HttpOptions]
        public HttpResponseMessage GetSecurityQA()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetSecurityQA(Guid contactId)
        {
            using (var ccm = new CrmConnectionManager())
            {
                var logic = new ContactLogic(ccm.Service);
                var securityQAs = logic.GetSecurityAnswers(contactId);
                return Json(securityQAs, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetAddress()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetAddress(Guid contactId)
        {
            using (var ccm = new CrmConnectionManager())
            {
                var logic = new AddressLogic(ccm.Service);
                var address = logic.GetByContactId(contactId);
                return Json(address, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetBirthDate()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetBirthDate(Guid contactId)
        {
            using (var ccm = new CrmConnectionManager())
            {
                var logic = new ContactLogic(ccm.Service);
                var birthDate = logic.GetBirthDate(contactId);
                return Json(birthDate?.ToShortDateString() ?? "", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetUsernameAndPin()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetUsernameAndPin(Guid contactId)
        {
            using (var ccm = new CrmConnectionManager())
            {
                var logic = new ContactLogic(ccm.Service);
                var credential = logic.GetUsernameAndPin(contactId);
                if (credential == null)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }

                var usernameAndPin = new
                {
                    username = credential.cub_Username,
                    pin = credential.cub_Pin
                };
                return Json(usernameAndPin, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetRecentlyVerifiedContactIds()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetRecentlyVerifiedContactIds(Guid userId)
        {
            using (var ccm = new CrmConnectionManager(userId))
            {
                var logic = new UserLogic(ccm.Service);
                var verificationActivities = logic.GetRecentlyVerifiedContactIds(userId);
                return Json(verificationActivities, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage CreateVerificationActivity()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpPost]
        public ActionResult CreateVerificationActivity(Guid? userId, Guid contactId, string subject, string body)
        {
            using (var ccm = new CrmConnectionManager(userId))
            {
                var logic = new ContactLogic(ccm.Service);
                var vaId = logic.CreateVerificationActivity(contactId, subject, body);
                if (vaId == null)
                {
                    throw new MSDException("Unable to create verification activity");
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetCurrentVerificationStatus()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetCurrentVerificationStatus(Guid userId, Guid contactId)
        {
            using (var conn = new CrmConnectionManager(userId))
            {
                var logic = new ContactLogic(conn.Service);
                var result = logic.GetCurrentVerificationStatus(userId, contactId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
