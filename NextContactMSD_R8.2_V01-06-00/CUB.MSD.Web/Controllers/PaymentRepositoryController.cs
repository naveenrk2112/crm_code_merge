﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CUB.MSD.Core;
using Microsoft.AspNet.Identity;
using CUB.MSD.Logic;
using CUB.MSD.Model;
using static CUB.MSD.Model.CUBConstants;
using CUB.MSD.Web.Models.TransitAccount;
using System.Net.Http;
using Microsoft.Xrm.Sdk;


namespace CUB.MSD.Web.Controllers
{
    public class PaymentRepositoryController : Controller
    {
        // http://localhost:12665/PaymentRepository/GetNonce
        /// <summary>
        /// 2.18.1 payment/repository/nonce GET
        /// This API gets a repository nonce for handling a credit card in a PCI compliant manner.
        /// The API returns a unique value that acts as a security token for repository requests. 
        /// </summary>
        /// <returns>Nonce</returns>
        public ActionResult GetNonce()
        {
            using (var conn = new CrmConnectionManager())
            {
                var nis = new NISApiLogic(conn.Service);
                var result = nis.GetPaymentRepositoryNonce();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //public ActionResult GetEncryptionDetails()
        //{
        //    string publicKey = "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFpcmRBeTl3ZDFGTE1BL1A3b0pIUAprWHhqMVhaT0JZckttSmM1cmJDT2ZUTTkrazBuVzBQL0kwUi9SM0dlWmJhV2R3UTN2dDFSWDV6OHJhaVpjVFpOCm1EYTFxVGN2TmE4bTV3NVh1b1gzUFZtY3V0RXZqZU94ZE9wV3hpMTV4Y1RBb1lUZVVsTFk1ckRFaHh6QXVybUUKN2lLWHhhUXdlTFJ5dXNqTGFkVkNjVElKMzdMRmY2TzZ6VnJRck9lQkpGVDRFQ1FZYkxrNkJhdGltOTV5TnJCbwpvWVd6aEdESzZ1YWN5QWJic21aeEh4UUdUTjhPcWppNElXeDJweXVyTnBQbGVkaFhjRkpPWjlWSldOU3pXMGp5CmtEdzhpU3ViNDZwWUhIMlhRSml3cWJOV1RMclFvdkRHT1ZwU3NPT2JST1lCeWlDQTRQQVVxcU1iM1AxOHVvUWsKdndJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg==";
        //    var result = new
        //    {
        //        modulus = "modulus",
        //        exponent = "exponent",
        //        decodedString = publicKey
        //    };
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
    }
}