/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using CUB.MSD.Model;
using CUB.MSD.Web.Models;
using CUB.MSD.Web.Models.MasterSearch;
using Microsoft.Xrm.Sdk;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;


namespace CUB.MSD.Web.Controllers
{
    public class CustomerFundingSourceController : BaseController
    {
        [HttpPost]
        public ActionResult Index()
        {
            return View();
        }

        // The NIS Funding Source POST method is broken down into 4 actions below: credit card reference, credit card clear, direct debit reference, direct debit clear
        // Direct Debit is not yet supported by NIS but calls have been stubbed in.

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CustomerFundingSourcePostCCRef()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// CustomerFundingSourcePostCCRef
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="pgCardId"></param>
        /// <param name="maskedPan"></param>
        /// <param name="cardExpiryMMYY"></param>
        /// <param name="nameOnCard"></param>
        /// <param name="creditCardType"></param>
        /// <param name="billingAddressId"></param>
        /// <param name="setAsPrimary"></param>
        /// <param name="userId"></param>
        /// <returns>fundingSourceId</returns>
        //var result = nisApi.CustomerFundingSourcePostCCRef("2B9634F0-3F29-E711-80EA-005056813E0D", "1", "1111", "0120", "Bill Smith", "Visa", "1", false);
        [HttpPost]
        public ActionResult CustomerFundingSourcePostCCRef(string customerId, string pgCardId, string maskedPan, string cardExpiryMMYY, string nameOnCard, string creditCardType,
                                                           string billingAddressId, bool? setAsPrimary, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                    CubResponse<WSCustomerFundingSourcePostResponse> result = nis.CustomerFundingSourcePostCCRef(customerId, pgCardId, maskedPan, cardExpiryMMYY, nameOnCard, creditCardType, 
                                                                                                            billingAddressId, setAsPrimary);
         
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// CustomerFundingSourcePostCCClear
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="pan"></param>
        /// <param name="cardExpiryMMYY"></param>
        /// <param name="nameOnCard"></param>
        /// <param name="postalcode"></param>
        /// <param name="address1"></param>
        /// <param name="country"></param>
        /// <param name="billingAddressId"></param>
        /// <param name="setAsPrimary"></param>
        /// <parram name="userId"></parram>
        /// <returns>fundingSourceId</returns>
        //var result = nisApi.CustomerFundingSourcePostCCClear("2B9634F0-3F29-E711-80EA-005056813E0D", "Visa", "5555555555554447", "0120", "Bill Smith", "12345","First Street","US","1",false);
        [HttpPost]
        public ActionResult CustomerFundingSourcePostCCClear(string customerId, string cardType, string pan, string cardExpiryMMYY, string nameOnCard, string postalcode, string address1, string country,
                                                             string billingAddressId, bool? setAsPrimary, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                CubResponse<WSCustomerFundingSourcePostResponse> result = nis.CustomerFundingSourcePostCCClear(customerId, cardType, pan, cardExpiryMMYY, nameOnCard, postalcode, address1, country,
                                                                                                        billingAddressId, setAsPrimary);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage AddFundingSource()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }
        /// <summary>
        /// Add Funding Source with credit card information
        /// </summary>
        /// <param name="accountID">Account ID</param>
        /// <param name="cardNumber">Card number</param>
        /// <param name="cardExpiryMMYY">Card expiration date</param>
        /// <param name="nameOnCard">Name on card</param>
        /// <param name="billingAddressId">Billing address id. If null indicates a new address and the address fields will be used
        ///                                to create a new address</param>
        /// <param name="address1">Address 1</param>
        /// <param name="address2">Address 2</param>
        /// <param name="city">City</param>
        /// <param name="state">State</param>
        /// <param name="zipcode">Zipcode</param>
        /// <param name="country">Country</param>
        /// <param name="setAsPrimary">Set as primary funding source</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddFundingSource(string accountID, string cardNumber, string cardExpiryMMYY, string nameOnCard,
                                             string billingAddressId, bool isNewAddress,
                                             string address1, string address2, string city, string state, Guid? StateId, 
                                             string zipcode, Guid? zipcodeId, string country, Guid? countryId,
                                             bool setAsPrimary, Guid? userId)
        {
            //CubLogger.DebugFormat("CustomerID = {0}; Card # = {1}; Card Exp. = {2}; Name on Card = {3}",
            //                      accountID, cardNumber, cardExpiryMMYY, nameOnCard);
            //CubLogger.DebugFormat("Billing Address ID = {0}; isNewAddress = {1}", billingAddressId, isNewAddress);
            //CubLogger.DebugFormat("Address 1 = {0}; Address 2 = {1}; City = {2}; State = {3}; Zipcode = {4}; Country = {5}",
            //                      address1, address2, city, state, zipcode, country);
            //CubLogger.DebugFormat("Primary = {0}", setAsPrimary);

            using (var connMgr = new CrmConnectionManager(userId))
            {
                var addressLogic = new AddressLogic(connMgr.Service);
                if (!isNewAddress)
                {
                    var address = addressLogic.Retrieve<cub_Address>(new Guid(billingAddressId), "cub_street1", "cub_postalcodeid", "cub_countryid");
                    zipcode = address.cub_PostalCodeId.Name;
                    address1 = address.cub_Street1;
                }
                else
                {
                    Model.MSDApi.WSAddress newAddress = new Model.MSDApi.WSAddress();
                    newAddress.address1 = address1;
                    newAddress.address2 = address2;
                    newAddress.city = city;
                    newAddress.stateId = StateId.Value;
                    newAddress.postalCodeId = zipcodeId.Value;
                    newAddress.countryId = countryId.Value;
                    billingAddressId = addressLogic.CreateAddress(newAddress, new Guid(accountID)).ToString();
                }
                var nisLogic = new NISApiLogic(connMgr.Service);
                //@TODO: Credit card TYPE is missing from the UI and it is required by NIS.
                // We had a change on CustomerFundingSourcePostCCClear adding the type
                CubResponse<WSCustomerFundingSourcePostResponse> result =
                    nisLogic.CustomerFundingSourcePostCCClear(accountID, "", cardNumber, cardExpiryMMYY, nameOnCard, zipcode, address1, country,
                                                              billingAddressId, setAsPrimary);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        // NIS doesn't support direct debit yet so finish these when they add it.
        //[HttpPost]
        //public ActionResult PostCustomerFundingSourceDDRef(string customerId, string nameOnAccount, int bankAccountNumber, string bankRoutingNumber, string accountType, string financialInstitutionName,
        //                                                   string billingAddressId, bool? setAsPrimary)
        //{
        //    using (var connMgr = new CrmConnectionManager())
        //    {
        //        NISApiLogic nis = new NISApiLogic(connMgr.Service);
        //        CubResponse<WSCustomerFundingSourcePostResponse> result = nis.CustomerFundingSourcePost(customerId, 
        //          nameOnAccount, bankAccountNumber, bankRoutingNumber, accountType, financialInstitutionName,
        //          billingAddressId, setAsPrimary);

        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //[HttpPost]
        //public ActionResult PostCustomerFundingSourceDDClear(string customerId, string paymentType, int paymentAmount, string billingAddressId, bool? setAsPrimary)
        //{
        //    using (var connMgr = new CrmConnectionManager())
        //    {
        //        NISApiLogic nis = new NISApiLogic(connMgr.Service);
        //        CubResponse<WSCustomerFundingSourcePostResponse> result = nis.CustomerFundingSourcePost(customerId, paymentType, paymentAmount, billingAddressId, billingAddressId, setAsPrimary);

        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CustomerFundingSourceGet()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// CustomerFundingSourceGet - retrieves all funding sources for the customerId passed
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="inactive"></param>
        /// <returns>WSFundingSourcesResponse - list of funding sources</returns>
        //http://localhost:12663/CustomerFundingSource/CustomerFundingSourceGet?customerId=2B9634F0-3F29-E711-80EA-005056813E0D
        [HttpGet]
        public ActionResult CustomerFundingSourceGet(string customerId, bool? inactive)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var resultJson = nis.CustomerFundingSourceGet(customerId, inactive);
                var result = JsonConvert.DeserializeObject<WSGetFundingSourcesResponse>(resultJson);
                if(result?.fundingSources?.Count > 0)
                {
                    var logic = new AddressLogic(connMgr.Service);
                    foreach(var fs in result.fundingSources)
                    {
                        var addressResult = logic.IsAddressIdValid(fs.billingAddressId);
                        if (addressResult.Success && addressResult.ResponseObject != null)
                        {
                            var a = addressResult.ResponseObject;
                            var sb = new StringBuilder();
                            sb.Append(a.address1?.Trim());
                            sb.Append(", ");
                            sb.Append(a.city?.Trim());
                            sb.Append(", ");
                            sb.Append(a.state?.Trim());
                            sb.Append(" ");
                            sb.Append(a.postalCode?.Trim());
                            fs.billingAddress = sb.ToString();
                        }
                    }
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CustomerFundingSourceGetById()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// CustomerFundingSourceGetById - gets a funding source by customerId/fundingSourceId
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="fundingSourceId"></param>
        /// <returns>WSGetFundingSourceResponse</returns>
        //http://localhost:12663/CustomerFundingSource/CustomerFundingSourceGetById?customerId=2B9634F0-3F29-E711-80EA-005056813E0D&fundingSourceId=9
        [HttpGet]
        public ActionResult CustomerFundingSourceGetById(string customerId, string fundingSourceId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                AddressLogic logic = new AddressLogic(connMgr.Service);
                WSGetFundingSourceResponse result = nis.CustomerFundingSourceGetById(customerId, fundingSourceId);
                if (result.fundingSource != null)
                {
                    CubResponse<CUB.MSD.Model.MSDApi.WSAddress> addr = logic.IsAddressIdValid(result.fundingSource.billingAddressId);
                    if (addr.Success)
                    {
                        result.fundingSource.billingAddress =
                            addr.ResponseObject.address1.Trim() + ", " + addr.ResponseObject.city.Trim() + ", " +
                            addr.ResponseObject.state.Trim() + " " + addr.ResponseObject.postalCode.Trim();
                    }
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CustomerFundingSourceDelete()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// CustomerFundingSourceDelete
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="fundingSourceId"></param>
        /// <returns>WSGetFundingSourceResponse - no data</returns>
        //var result = nisApi.CustomerFundingSourceDelete("2B9634F0-3F29-E711-80EA-005056813E0D", "7");

        [HttpDelete]
        public ActionResult CustomerFundingSourceDelete(string customerId, string fundingSourceId, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSWebResponseHeader result = nis.CustomerFundingSourceDelete(customerId, fundingSourceId);
                if (result == null)
                {
                    HttpStatusCodeResult statusResult = new HttpStatusCodeResult(HttpStatusCode.NoContent);
                    return statusResult;
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CustomerFundingSourcePatchCC()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// CustomerFundingSourcePatchCC
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="fundingSourceId"></param>
        /// <param name="pgCardId"></param>
        /// <param name="cardExpiryMMYY"></param>
        /// <param name="nameOnCard"></param>
        /// <param name="address1"></param>
        /// <param name="country"></param>
        /// <param name="postalCode"></param>
        /// <param name="billingAddressId"></param>
        /// <param name="setAsPrimary"></param>
        /// <returns>WSWebResponseHeader - no data</returns>
        //var result = nisApi.CustomerFundingSourcePatchCC("2B9634F0-3F29-E711-80EA-005056813E0D", "1", "507508", "0120", "Johnny Smith", "11 Elm", "US", "92008", "1", true);

        [HttpPatch]
        public ActionResult CustomerFundingSourcePatchCC(string customerId, string fundingSourceId, string pgCardId, string cardExpiryMMYY, string nameOnCard, 
                                                         bool isNewAddress, string address1, string address2, string city, string State, Guid? StateId, string zipcode, Guid? zipcodeId, 
                                                         string country, Guid? countryId, string billingAddressId, string billingAddress, bool? setAsPrimary, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                var addressLogic = new AddressLogic(connMgr.Service);
                if (!isNewAddress)
                {
                    var address = addressLogic.Retrieve<cub_Address>(new Guid(billingAddressId), "cub_street1", "cub_postalcodeid", "cub_countryid");
                    zipcode = address.cub_PostalCodeId.Name;
                    address1 = address.cub_Street1;
                }
                else
                {
                    Model.MSDApi.WSAddress newAddress = new Model.MSDApi.WSAddress();
                    newAddress.address1 = address1;
                    newAddress.address2 = address2;
                    newAddress.city = city;
                    newAddress.stateId = StateId.Value;
                    newAddress.postalCodeId = zipcodeId.Value;
                    newAddress.countryId = countryId.Value;
                    billingAddress = addressLogic.CreateAddress(newAddress, new Guid(customerId)).ToString();
                }

                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSWebResponseHeader result = nis.CustomerFundingSourcePatchCC(customerId, fundingSourceId, pgCardId, cardExpiryMMYY, nameOnCard, address1, country, zipcode, billingAddress, setAsPrimary);
                if (result == null)
                {
                    HttpStatusCodeResult statusResult = new HttpStatusCodeResult(HttpStatusCode.NoContent);
                    return statusResult;
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CustomerFundingSourceSetAsPrimary()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpPatch]
        public ActionResult CustomerFundingSourceSetAsPrimary(string customerId, string fundingSourceId, string billingAddressId, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {

                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSWebResponseHeader result = nis.CustomerFundingSourcePatchCC(customerId, fundingSourceId, null, null, null, null, null, null, billingAddressId, true);
                if (result == null)
                {
                    HttpStatusCodeResult statusResult = new HttpStatusCodeResult(HttpStatusCode.NoContent);
                    return statusResult;
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// CustomerFundingSourcePatchDD
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="fundingSourceId"></param>
        /// <param name="pgCardId"></param>
        /// <param name="cardExpiryMMYY"></param>
        /// <param name="nameOnCard"></param>
        /// <param name="address1"></param>
        /// <param name="country"></param>
        /// <param name="postalCode"></param>
        /// <param name="billingAddressId"></param>
        /// <param name="setAsPrimary"></param>
        /// <returns>WSWebResponseHeader - no data</returns>
        // Finish below when NIS adds support for direct debit - convert cc to dd
        //http://localhost:12663/CustomerFundingSource/CustomerFundingSourcePatchDD?customerId=2B9634F0-3F29-E711-80EA-005056813E0D&fundingSourceId=9&pgCardId=1&cardExpiryMMYY=0120&nameOnCard=Johnny Smith&address1=Second Street&country=US&postalCode=6789&billingAddressId=1&billingAddressId=1&setAsPrimary=true
        [HttpPost]
        public ActionResult CustomerFundingSourcePatchDD(string customerId, string fundingSourceId, string pgCardId, string cardExpiryMMYY, string nameOnCard, string address1, string country, string postalCode,
                                                         string billingAddressId, bool? setAsPrimary, Guid? userId)
        {  // todo - convert this function from cc to dd
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSWebResponseHeader result = nis.CustomerFundingSourcePatchDD(customerId, fundingSourceId, pgCardId, cardExpiryMMYY, nameOnCard, address1, country, postalCode, billingAddressId, setAsPrimary);
                if (result == null)
                {
                    HttpStatusCodeResult statusResult = new HttpStatusCodeResult(HttpStatusCode.NoContent);
                    return statusResult;
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
