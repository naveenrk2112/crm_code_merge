﻿using CUB.MSD.Core;
using CUB.MSD.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class ErrorBody
    {
        public string Message { get; set; }
        public string Origin { get; set; }
        public string Data { get; set; }
        public Guid? CorrelationId { get; set; }
        public string StackTrace { get; set; }
    }

    static class Origin
    {
        public const string NIS = "NIS";
        public const string MSD = "MSD";
        public const string TOLLING = "Tolling";
        public const string INTERNAL = "Internal";
        public const string GATEWAY = "General Gatway";
        public const string HTTP = "HTTP";
    }

    public class BaseController : Controller
    {
        private Guid? _correlationId;

        public BaseController() : base()
        {
            using (var ccm = new CrmConnectionManager())
            {
                _correlationId = CubLogger.Init(ccm.Service);
            }
        }

        //Handle unhandled exceptions from a controller
        protected override void OnException(ExceptionContext exceptionContext)
        {
            var exception = exceptionContext.Exception;
            ErrorBody body;

            if (exception is NISException)
            {
                var nisException = exception as NISException;

                CubLogger.Error(nisException.Message, nisException, nisException.StackTrace);
                Response.StatusCode = (int)HttpStatusCode.BadGateway;
                body = new ErrorBody
                {
                    Origin = Origin.NIS,
                    Data = nisException.Hdr != null ? nisException.Hdr.SerializeObject() : null
                };
            }
            else if (exception is TollingException)
            {
                var tollingException = exception as TollingException;

                CubLogger.Error(tollingException.Message, tollingException, tollingException.StackTrace);
                Response.StatusCode = (int)HttpStatusCode.BadGateway;
                body = new ErrorBody
                {
                    Origin = Origin.NIS,
                    Data = tollingException.Hdr != null ? tollingException.Hdr.SerializeObject() : null
                };
            }
            else if (exception is GatewayException)
            {
                var gatewayException = exception as GatewayException;

                CubLogger.Error(gatewayException.Message, gatewayException, gatewayException.StackTrace);
                Response.StatusCode = (int)HttpStatusCode.BadGateway;
                body = new ErrorBody
                {
                    Origin = Origin.GATEWAY,
                    Data = gatewayException.Hdr != null ? gatewayException.Hdr.SerializeObject() : null
                };
            }
            //If an MSD organization service throws the exception
            else if (exception is FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>)
            {
                var orgException = exception as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
                var msdException = new MSDException(orgException);

                CubLogger.Error(orgException.Message, orgException, orgException.StackTrace);
                Response.StatusCode = (int)HttpStatusCode.BadGateway;
                body = new ErrorBody
                {
                    Origin = Origin.MSD
                };
            }
            //Manual throwing of this exception
            else if (exception is MSDException)
            {
                var msdException = exception as MSDException;

                CubLogger.Error(msdException.Message, msdException, msdException.StackTrace);
                Response.StatusCode = (int)HttpStatusCode.BadGateway;
                body = new ErrorBody
                {
                    Origin = Origin.MSD
                };
            }
            //Handled HTTP exception
            else if (exception is HttpException)
            {
                var httpException = exception as HttpException;

                CubLogger.Error(httpException.Message, httpException, httpException.StackTrace);
                Response.StatusCode = httpException.GetHttpCode();
                body = new ErrorBody
                {                  
                    Origin = Origin.HTTP
                };
            }
            //Unhandled error
            else
            {
                CubLogger.Error(exception.Message, exception, exception.StackTrace);
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                body = new ErrorBody
                {
                    Origin = Origin.INTERNAL
                };
            }

            body.CorrelationId = _correlationId;
            body.Message = exception.Message;
            //TODO: Is this a security issue?
            body.StackTrace = exception.StackTrace;

            exceptionContext.ExceptionHandled = true;
            exceptionContext.Result = Json(body, JsonRequestBehavior.AllowGet);
        }
    }
}