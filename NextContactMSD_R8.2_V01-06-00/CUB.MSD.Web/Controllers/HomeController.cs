﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Configuration;
using System.Net;
using System.Web.Http.Results;
using System.IO;
using System.Net.Http.Formatting;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using CUB.MSD.Model.NIS;

namespace CUB.MSD.Web.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        [Route("")]
        public ActionResult Index()
        {
            using (var ccm = new CrmConnectionManager())
            {
                var logic = new GlobalsLogic(ccm.Service);
                var globalKeys = logic.GetAttributeValue("AppInfo", "ClientGlobalKeys");
                var globals = globalKeys.Split(',').Select(key => new { Key = key, Value = logic.GetCachedDetailsByGlobal(key) }).ToList();
                var gridConfigs = logic.GetAllCustomGridConfigurations();
                ViewBag.MsdUrl = string.Format("{0}{1}", ConfigurationManager.AppSettings["CRM.IPURL"], ConfigurationManager.AppSettings["CRM.OrganizationUniqueName"]);
                ViewBag.WebAppUrl = logic.GetAttributeValue("CUB.MSD.WEB", "WEB.APP.URL");
                ViewBag.Globals = JsonConvert.SerializeObject(globals);
                ViewBag.GridConfigs = JsonConvert.SerializeObject(gridConfigs);

                return View("NgApp");
            }
        }

        [Route("trip")]
        [Route("location")]
        [Route("person")]
        public ActionResult AppBookmarkableRoutes()
        {
            return View();
        }

        // TODO: remove HttpOptions
        [HttpOptions]
        public HttpResponseMessage GetUrl(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetUrl()
        {

            if (String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CRM.IPURL"]))
            {
                using (var ccm = new CrmConnectionManager())
                {
                    return Json(new { url = ccm.WebApplicationUrl }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { url = ConfigurationManager.AppSettings["CRM.IPURL"] }, JsonRequestBehavior.AllowGet);
            }

        }

        // TODO: remove HttpOptions
        [HttpOptions]
        public HttpResponseMessage GetConstants()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetConstants(string key)
        {
            using (var ccm = new CrmConnectionManager())
            {
                var logic = new GlobalsLogic(ccm.Service);
                var results = logic.GetCachedDetailsByGlobal(key);
                return Json(results, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetAllConstants(string foo)
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetAllConstants()
        {
            using (var conn = new CrmConnectionManager())
            {
                var logic = new GlobalsLogic(conn.Service);
                var globalKeys = logic.GetAttributeValue("AppInfo", "ClientGlobalKeys");
                var globals = globalKeys.Split(',')
                    .Select(key => key.Trim())
                    .Select(key => new { Key = key, Value = logic.GetCachedDetailsByGlobal(key) })
                    .ToList();
                var response = new MvcGenericResponse();
                response.Body = JsonConvert.SerializeObject(globals);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetGridConfigurations(string foo)
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        /// <summary>
        /// Retrieves all customizable grid configuration records from MSD.
        /// The results returned are full of empty lists if not returned as a
        /// serialized string.
        /// </summary>
        /// <returns>A JSON string representing an array of grid configuration objects</returns>
        [HttpGet]
        public ActionResult GetGridConfigurations()
        {
            using (var ccm = new CrmConnectionManager())
            {
                var logic = new GlobalsLogic(ccm.Service);
                var results = logic.GetAllCustomGridConfigurations();
                return Json(results.SerializeObject(), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoggerTest()
        {
            using (var ccm = new CrmConnectionManager())
            {
                //ContactLogic logic = new ContactLogic(ccm.Service);
                CubLogger.Init(ccm.Service, true);
                string jsonData = "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}";
                CubLogger.Info("Info Log test");
                CubLogger.InfoFormat("Formated Info Test: {0} - {1} - {2}", "One", "Two", "Three");
                CubLogger.Info("Info with JSON Data", jsonData);

                // Request
                DateTime requestTime = DateTime.UtcNow;
                CubLogger.Info("Request", "Request", requestTime, null, null);

                // Response
                CubLogger.Info("Response", null, requestTime, "response", DateTime.UtcNow);

                CubLogger.Info("processing request/response with empty start and end time", "Request", null, "response", null);
                CubLogger.Debug("Info Log test");
                CubLogger.Warn("Info Log test");
                try
                {
                    if (true)
                    {
                        throw new Exception("Fake Exception to test the logger");
                    }
                }
                catch (Exception ex)
                {
                    CubLogger.Error(string.Empty, ex);
                    CubLogger.Error("Error test with data", ex, jsonData);
                }

                return View();
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetActivityTypes(string foo)
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        /// <summary>
        /// Query MSD metadata for all Activity type codes
        /// </summary>
        /// <returns>A JSON array of activity types where Value is the type code and Label is the display name</returns>
        [HttpGet]
        public ActionResult GetActivityTypes()
        {
            using (var ccm = new CrmConnectionManager())
            {
                var req = new RetrieveOptionSetRequest
                {
                    Name = "activitypointer_activitytypecode"
                };
                var res = (RetrieveOptionSetResponse)ccm.Service.Execute(req);
                var options = ((OptionSetMetadata)res.OptionSetMetadata).Options
                    .Where(o => IsCreatableActivity(o))
                    .Select(o => new
                    {
                        Value = o.Value,
                        Label = o.Label.UserLocalizedLabel.Label
                    })
                    .ToList();
                return Json(options, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Helper method for removing activities that users shouldn't be allowed
        /// to create outside of a BPF, plugin, or workflow
        /// </summary>
        /// <param name="option">An option set option metadata object</param>
        /// <returns>False if the activity is not creatable, true otherwise.</returns>
        private bool IsCreatableActivity(OptionMetadata option)
        {
            switch (option.Value)
            {
                // Exclude these activity types. Only specify NATIVE types here
                case BulkOperation.EntityTypeCode:
                case CampaignActivity.EntityTypeCode:
                case IncidentResolution.EntityTypeCode:
                case OpportunityClose.EntityTypeCode:
                case OrderClose.EntityTypeCode:
                case QuoteClose.EntityTypeCode:
                    return false;
                default:
                    return true;
            }
        }
    }
}
