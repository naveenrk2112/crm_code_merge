﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class ActivityController : BaseController
    {

        #region Create Activity of type 'Contact Verification - Contact Info'
        [HttpOptions]
        public HttpResponseMessage ContactVerificationByContactInfo()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Activity/ContactVerificationBySecurityQuestion?contactId=33CB941B-881C-49BD-BDB8-657BCC050B38
        /// <summary>
        /// Creates a New Activity of type Contact Verification - Contact Info
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <returns>cubResponse as a JSON object</returns>
        [HttpPost]
        public ActionResult ContactVerificationByContactInfo(Guid? userId, Guid? sessionId, Guid contactId, string subject, string notes)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                var logic = new ActivityLogic(connMgr.Service);

                // Get activity type value to lookup activity type id
                string activityNameFromGlobals = connMgr.CUBOrganizationServiceContext.cub_GlobalDetailsSet
                    .Where(gd => gd.cub_AttributeValue == GlobalsCache.ActivityTypeContactVerifyContactInfo(connMgr.Service))
                    .Select(gd => gd.cub_AttributeValue).Single();

                CreateMSDActivityData activityData = new CreateMSDActivityData();
                activityData.CreatedById = userId.HasValue ? userId.Value : Guid.Empty;
                activityData.ActivityTypeId = logic.GetActivityTypeId(activityNameFromGlobals);
                activityData.SessionId = sessionId.HasValue ? sessionId.Value : Guid.Empty;
                activityData.ContactId = contactId;
                activityData.Description = notes;
                CubResponse<WSCreateNewActivityResponse> cubResponse = logic.CreateNewActivity(activityData);
                return Json(cubResponse, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Create Activity of type 'Contact Verification - Security Question'
        [HttpOptions]
        public HttpResponseMessage ContactVerificationBySecurityQuestion()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Activity/ContactVerificationBySecurityQuestion?contactId=33CB941B-881C-49BD-BDB8-657BCC050B38
        /// <summary>
        /// Creates a New Activity of type Contact Verification - Security Question
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <returns>cubResponse as a JSON object</returns>
        [HttpPost]
        public ActionResult ContactVerificationBySecurityQuestion(Guid? userId, Guid? sessionId, Guid contactId, string subject, string notes)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                var logic = new ActivityLogic(connMgr.Service);

                // Get activity type value to lookup activity type id
                string activityNameFromGlobals = connMgr.CUBOrganizationServiceContext.cub_GlobalDetailsSet
                    .Where(gd => gd.cub_AttributeValue == GlobalsCache.ActivityTypeContactVerifySecurityQuestion(connMgr.Service))
                    .Select(gd => gd.cub_AttributeValue).Single();

                CreateMSDActivityData activityData = new CreateMSDActivityData();
                activityData.CreatedById = userId.HasValue ? userId.Value : Guid.Empty;
                activityData.ActivityTypeId = logic.GetActivityTypeId(activityNameFromGlobals);
                activityData.SessionId = sessionId.HasValue ? sessionId.Value : Guid.Empty;
                activityData.ContactId = contactId;
                activityData.Description = notes;
                CubResponse<WSCreateNewActivityResponse> cubResponse = logic.CreateNewActivity(activityData);
                return Json(cubResponse, JsonRequestBehavior.AllowGet);

            }
        }
        #endregion

        #region Create Activity of type 'Contact Verification - Unverified'
        [HttpOptions]
        public HttpResponseMessage ContactVerificationUnverified()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Activity/ContactVerificationUnverified?contactId=33CB941B-881C-49BD-BDB8-657BCC050B38
        /// <summary>
        /// Creates a New Activity of type Contact Verification - Security Question
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <returns>cubResponse as a JSON object</returns>
        [HttpPost]
        public ActionResult ContactVerificationUnverified(Guid? userId, Guid? sessionId, Guid contactId, string subject, string notes)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                var logic = new ActivityLogic(connMgr.Service);

                // Get activity type value to lookup activity type id
                string activityNameFromGlobals = connMgr.CUBOrganizationServiceContext.cub_GlobalDetailsSet
                    .Where(gd => gd.cub_AttributeValue == GlobalsCache.ActivityTypeContactVerificationUnverified(connMgr.Service))
                    .Select(gd => gd.cub_AttributeValue).Single();

                CreateMSDActivityData activityData = new CreateMSDActivityData();
                activityData.CreatedById = userId.HasValue ? userId.Value : Guid.Empty;
                activityData.ActivityTypeId = logic.GetActivityTypeId(activityNameFromGlobals);
                activityData.SessionId = sessionId.HasValue ? sessionId.Value : Guid.Empty;
                activityData.ContactId = contactId;
                activityData.Description = notes;
                CubResponse<WSCreateNewActivityResponse> cubResponse = logic.CreateNewActivity(activityData);
                return Json(cubResponse, JsonRequestBehavior.AllowGet);

            }
        }
        #endregion

        #region Get BO activity details

        [HttpOptions]
        public HttpResponseMessage GetActivityDetails()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetActivityDetails(string customerId, string transitaccountId, string subsystemId, string activityId, string contactId, 
            string contact, string actionType, string location, string device, string sourceIpAddress, string type, string activitySubject, string activityCategory, 
            string createDateTime, string channel, string origin, string globalTxnId, string subsystemDescription, string trackingInfo, string userName, string activityData,
            string getActivityDetailsFor)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetActivityDetails(customerId, transitaccountId, subsystemId, activityId,
                    contactId, contact, actionType, location, device, sourceIpAddress, type,
                    activitySubject, activityCategory, createDateTime, channel, origin, globalTxnId,
                    subsystemDescription, trackingInfo, userName, activityData, getActivityDetailsFor);
                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }

        #endregion
    }
}