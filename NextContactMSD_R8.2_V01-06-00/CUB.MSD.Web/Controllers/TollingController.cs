﻿using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;

namespace CUB.MSD.Web.Controllers
{
    public class TollingController : BaseController
    {
        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetTransponders()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Tolling/GetTransponders?customerId=1001
        /// <summary>
        /// Get Transponders
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <returns>Transponders</returns>
        [HttpGet]
        public ActionResult GetTransponders(string customerId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSTollingGetTranspondersResponse result = nis.GetTollingTransponders(customerId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetVehicles()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Tolling/GetVehicles?customerId=1001
        /// <summary>
        /// Get Vehicles
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <returns>Vehicles</returns>
        [HttpGet]
        public ActionResult GetVehicles(string customerId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSTollingGetVehiclesResponse result = nis.GetTollingVehicles(customerId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetTransactionHistory()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Tolling/GetTransactionHistory?customerId=1001
        /// <summary>
        /// Get Transactions History
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <returns>Transaction History</returns>
        [HttpGet]
        public ActionResult GetTransactionHistory(string customerId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSTollingGetTransactionHistoryResponse result = nis.GetTollingTransactionHistory(customerId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetAccountSummary()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Tolling/GetAccountSummary?customerId=12345
        /// <summary>
        /// Get Account Summary
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <returns>Account Summary</returns>
        [HttpGet]
        public ActionResult GetAccountSummary(string customerId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSTollingGetAccountSummaryResponse result = nis.GetTollingAccountSummary(customerId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}