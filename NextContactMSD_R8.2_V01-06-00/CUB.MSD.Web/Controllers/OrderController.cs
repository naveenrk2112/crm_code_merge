﻿using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class OrderController : Controller
    {
        public HttpResponseMessage GetOrderConfigDatatypes()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Order/GetOrderConfigDatatypes?returnOrderTypes=true&returnOrderStatuses=false&returnPaymentStatuses=true
        /// <summary>
        /// This API is used to get all configured order data types such as list of order types, list of order statuses and payment statuses.
        /// Optional filters indicate which order data types should be returned.
        /// </summary>
        /// <param name="returnOrderTypes">(Optional) Flag to indicate whether to retrieve order types Default value is ‘true’. </param>
        /// <param name="returnOrderStatuses">(Optional) Flag to indicate whether to retrieve order statuses Default value is ‘true. </param>
        /// <param name="returnPaymentStatuses">(Optional) Flag to indicate whether to retrieve order payment types Default value is ‘true. </param>
        /// <param name="userId">User ID</param>
        /// <returns>Order data types</returns>
        [HttpGet]
        public ActionResult GetOrderConfigDataTypes(bool? returnOrderTypes, bool? returnOrderStatuses, bool? returnPaymentStatuses, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetOrderConfigDataTypes(returnOrderTypes, returnOrderStatuses, returnPaymentStatuses);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpOptions]
        public HttpResponseMessage OrderPaymentReceipt()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        ///http://localhost:12665/Order/OrderPaymentReceipt?orderid=123&unregisteredEmail=test@test.com
        /// <summary>
        /// 2.15.12 order/<order-id>/paymentreceipt POST
        /// This API is used to generate and send the payment receipt for a given order. This is to be used when a payment receipt was not sent during initial order processing due to lack of an email address (unregistered customers), customer was opted out to receive payment receipts at the time of order processing or a system malfunction.
        /// Besides the order id, the request allows for an email address to be provided.The email is only used if the order was for an unregistered customer.In this case the order gets updated with the email address for future
        /// notifications. If the order was for a registered customer and an email was provided, the email is ignored and the customer primary contact email address is used.
        /// Payment receipts will be generated only if the order type supports payment receipts (Sale, Autoload, Debt Collection) and if payment was successful.
        /// If the customer needs to receive a notification with the current order status, another method is to be used.
        /// This method returns an empty response body in case of success or failure.The response header contains the result.
        /// </summary>
        /// <param name="orderid">(Required) The order the payment receipt should be created.</param>
        /// <param name="unregisteredEmail">(Conditionally-Required)  If order is for an unregistered customer, the email address to send order payment receipt.
        ///                                 If the order is for a registered customer, this field will be ignored and the notification will be sent to the registered contact information.
        ///                                 Required if order is for unregistered customer.
        ///</param>
        /// <returns>NIS Response  as JSON</returns>
        [HttpPost]
        public ActionResult OrderPaymentReceipt(string orderid, string unregisteredEmail, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.OrderPaymentReceipt(orderid, unregisteredEmail);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpOptions]
        public HttpResponseMessage ResendOrderNotification()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        ///http://localhost:12665/Order/ResendOrderNotification?notificationType=CustomerPaymentReceipt&notificationId=1234
        /// <summary>
        /// 2.13.1. POST notification/type/<notification-type>/resend POST
        /// This API method is used to resend a notification based on the provided notification type and identifier, the specified notification channels, email address and SMS phone. Only notification id and notification type are required. 
        /// If an original notification with the same notification id and notification type is not found, an error is returned. 
        /// If the original notification with the same notification id and notification type is found and notifications channel, email address or SMS phone are not specified, the notification is resent with the values from the original request.
        /// If Email is specified in the notification methods, an email address needs to be provided.
        /// If SMS is specified in the notification methods, SMS phone needs to be provided.
        /// If PushNotification is specified in the notification methods, no other information is required and the currently active push notification tokens for the patron account Id in the original request will be used. 
        /// </summary>
        /// <param name="notificationType">(Required)  An identifier for the type of notification to send.  Notification types are project specific. </param>
        /// <param name="clientRefId">(Required)  The client’s unique identifier for sending this notification.
        ///       This field will be used for detecting duplicate requests and therefore must be globally unique.This value should be specified as:
        ///"        <device-id>:<unique-id>".
        ///</param>
        /// <param name="notificationId">(Required) The notification identifier for the original notification that was sent. (Note this is NOT the notificationReference. This is the notificationId returned by notification history calls)</param>
        /// <param name="channels">(Optional)  A list of notification channels (Email, SMS, Push) that should be used for resending the notification.</param>
        /// <param name="email">(Optional)  If one of the channels is Email and if a value is provided, this is the email address to be used to resend the notification.</param>
        /// <param name="smsPhone">(Optional)  If one of the channels is SMS and if a value is provided, this is the phone number to be used to resend the notification.</param>
        /// <returns>NIS Response as JSON</returns>
        [HttpPost]
        public ActionResult ResendOrderNotification(string notificationType, string clientRefId, int notificationId, string channels, string email,
                                                       string smsPhone, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.ResendNotification(notificationType, clientRefId, notificationId, channels, email, smsPhone);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpOptions]
        public HttpResponseMessage GetOrderNotification()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// 2.15.11 : order/<order-id>/notification/g\ POST
        ///•	startDateTime: the start date time to search for notifications and it is based on when the notification was processed by the system.This value is configurable to allow the number of days in the history to start, but the default is current day minus 30 days.
        ///•	endDateTime: to allow historical filtering on the selected range type, The end date allows the user to limit the range to search.  If not provided, the end date defaults to beginning of next day.The maximum end date should be set to no more than a configurable number of days from the start date.
        ///•	type: to narrow down the results to only show a selected notification type.
        ///•	status: to narrow down the results based on the final status of a processed notification.  Valid values are:
        ///         o All
        ///         o Sent
        ///         o Failed
        ///•	channel: to narrow down the results to show notifications processed for a selected notification channel.
        ///•	recipient: to narrow down the results to show notifications processed for a specific recipient (email address, SMS phone number or mobile device nickname).
        ///•	notificationReference: used to narrow down the customer notifications in the result by the notification reference displayed in the outgoing message.
        ///•	sortBy: sort expression to be applied to the results.
        ///•	offset: to allow for pagination provides the starting point that should be pulled from the results. Defaults to 0.
        ///•limit: to allow for pagination provides the number of records that should be returned per page. Defaults to 20.
        /// it is ignored and the customer notification preferences are used to send notifications.
        /// </summary>
        /// <param name="orderId">(Required) The order the notification is related to.</param>
        /// <param name="startDateTime">Optional)  The date and time that should be used to filter the starting date of the range.  If not provided then the default should be configurable with a default value of now – 30 days.</param>
        ///<param name="endDateTime">(Optional)  The date and time that should be used to filter the ending date of the range.  If not provided then the default should be the beginning of next day.  The maximum end date should be as number of days from the start date.</param>
        ///<param name="type">(Optional)  An identifier for the type of notification to return in the results.  The default is to return all notification types if no value is provided.</param>
        ///<param name="status">(Required)  The final notification status should be returned.  The default is to return all notifications in final status if no value is provided.
        ///     • All
        ///     • Sent
        ///     • Failed</param>
        ///<param name="channel">(Optional)  Used to filter the notification types by channel. 
        ///        Valid values are:
        ///         • Email
        ///         • PushNotification
        ///         • SMS
        ///         These values are not case sensitive.The default is to return all channels if no value is provided.
        ///</param>
        ///<param name="recipient">(Optional)  Used to filter the notification types by recipient (email address, SMS phone number or mobile device nickname). </param>
        ///<param name="notificationReference">(Optional)  Used to filter the notification types by the notification reference displayed in the outgoing message. Allow for partial string with wild card ‘*’.</param>
        ///<param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: “type.asc,status.desc” indicates sort by type ascending and status  descending. Default is descending by startDateTime.</param>
        ///<param name="offset">(Optional) Used to identify the starting index that should be provided within the page in the search result.  The default is to start at the first row of data (0) if no value is provided.</param>
        ///<param name="limit">(Optional) Used to identify the number of rows per page in the search result in the response.  The default is to return 20 rows if no value is provided.</param>
        /// <returns>NIS Response List<WSOrderNotificationInfo> as JSON</returns>
        [HttpGet]
        public ActionResult GetOrderNotification(string orderId, DateTime? startDateTime, DateTime? endDateTime, string type, string status,
                                                        string channel, string recipient, string notificationReference, string sortBy, int? offset, int? limit,Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetOrderNotification(orderId, startDateTime, endDateTime, type, status, channel, recipient, notificationReference, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}