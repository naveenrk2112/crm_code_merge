﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CUB.MSD.Core;
using Microsoft.AspNet.Identity;
using CUB.MSD.Logic;
using CUB.MSD.Model;
using static CUB.MSD.Model.CUBConstants;
using CUB.MSD.Web.Models.TransitAccount;
using System.Net.Http;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Web.Controllers
{
    public class CustomerServiceController : Controller
    {
        [HttpOptions]
        public HttpResponseMessage GetReasonCodes()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/CustomerService/GetReasonCodes
        /// <summary>
        /// This API returns a mapping ofconfigured reason code types and reason codes that can be used by customer service.
        /// The mapping defines the type of codes and then set of reason codes associated with that type for use within the CSR interface.
        /// Depending on the reason code type, the mapping might include a list of financially responsible operators that are associated with
        /// specific reason codes.  If the reason codes are to be used for financial transactions, minimum and maximum amounts might be included.
        /// This method accepts optional filters by:
        ///  - typeId: to narrow down the results to only show a selected reason code type.
        ///  - active: to narrow down the results to only those reason codes that are active.
        /// </summary>
        /// <param name="typeId">
        /// (Optional) An identifier for the reason code types to return in the results.
        /// The default is to return all reason code types if no value is provided.
        /// </param>
        /// <param name="active">
        /// (Optional) An identifier for the active indicator of reason codes to return in the results.
        /// The default is to return all active reason codes if no value is provided.
        /// </param>
        /// <returns>Reason Codes</returns>
        [HttpGet]
        public ActionResult GetReasonCodes(int? typeId, bool? active)
        {
            using (var conn = new CrmConnectionManager())
            {
                var nis = new NISApiLogic(conn.Service);
                var result = nis.GetCustomerServiceReasonCodes(typeId, active);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}