/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Web.Models;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    /// <summary>
    /// Customer Registration Controller
    /// </summary>
    public class CustomerRegistrationController : BaseController
    {
        /// <summary>
        /// Action to initiate a new Customer Registration
        /// </summary>
        /// <param name="TokenId">TokenID</param>
        /// <returns>View Action Result</returns>
        public ActionResult Index(string TokenId)
        {
            ViewBag.TokenId = TokenId;
            ViewBag.PhoneTypes = GetPhoneTypes();
            using (var connMgr = new CrmConnectionManager())
            {
                CustomerRegistrationLogic logic = new CustomerRegistrationLogic(connMgr.Service);
                ViewBag.CRM_URL = GlobalsCache.GetKeyFromCache(connMgr.Service, "CUB.MSD.WEB", "CRM.URL");
                //ViewBag.AccountTypes = GetAccountTypes(connMgr.Service);
                //ViewBag.ContactTypes = GetContactTypes(connMgr.Service);
                //ViewBag.Countries = GetCountries(connMgr.Service);
                //ViewBag.States = GetStates(connMgr.Service);
                //ViewBag.SecurityQuestions = GetSecurityQuestions(connMgr.Service);
                ViewBag.UsernameRequireEmail = GlobalsCache.UsernameRequireEmail(connMgr.Service) ? "true" : "false";
            }
            return View();
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage SearchContact()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// POST action to search for contact
        /// </summary>
        /// <param name="contactSearchData">Search Data</param>
        /// <returns>Contacts found</returns>
        [HttpPost]
        public ActionResult SearchContact(CustomerSearchViewModel contactSearchData, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                CustomerRegistrationLogic logic = new CustomerRegistrationLogic(connMgr.Service);
                var result = logic.SearchContact(contactSearchData.FirstName, contactSearchData.LastName,
                                                 contactSearchData.Email, contactSearchData.Phone, contactSearchData.CustomerType);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage Register()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// POST action to Register an Account
        /// </summary>
        /// <param name="model">Action model</param>
        /// <returns>View Action Result</returns>
        [HttpPost]
        public ActionResult Register(CustomerRegistrationModel model, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                CustomerRegistrationLogic logic = new CustomerRegistrationLogic(connMgr.Service);
                List<KeyValuePair<string, string>> errors = logic.ValidateFieldsNewCustomerRegistration(model);

                if(errors.Count > 0)
                {
                    return Json(errors, JsonRequestBehavior.AllowGet);
                }

                var accountId = logic.RegisterCustomer(model);

                // todo: new code - we need to call NIS to complete registration and get back oneaccountId
                AccountLogic accountLogic = new AccountLogic(connMgr.Service);
                string oneAccountid = accountLogic.CompleteCustomerRegistration(accountId);

                // end new
                return Json(accountId, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CompleteRegistration()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// POST action to Register an Account
        /// </summary>
        /// <param name="model">Action model</param>
        /// <returns>View Action Result</returns>
        [HttpPost]
        public ActionResult CompleteRegistration(string accountId, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                AccountLogic accountLogic = new AccountLogic(connMgr.Service);
                // call NIS to complete registration and get back oneaccountId
                string oneAccountId = accountLogic.CompleteCustomerRegistration(new Guid(accountId));
                return Json(oneAccountId, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CheckZipPostalCode(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Post Action to check Zip/Postal Code
        /// </summary>
        /// <param name="code">Zip/Postal Code</param>
        /// <param name="country">MSD Country ID</param>
        /// <param name="state">MSD State ID</param>
        /// <returns>View Action Result</returns>
        [HttpPost]
        public ActionResult CheckZipPostalCode(string code, Guid? country, Guid? state, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                CustomerRegistrationLogic logic = new CustomerRegistrationLogic(connMgr.Service);
                var result = logic.GetZipPostalCode(code, country, state);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetPhoneTypes(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Return Phone Types to be used by UI components
        /// </summary>
        /// <returns>Phone Types</returns>
        [HttpGet]
        public ActionResult GetPhoneTypes()
        {
            UtilityLogic util = new UtilityLogic();
            var phoneTypes = util.GetOptionSetMetaData(typeof(cub_phonetype));
            /*
             * Moving the Mobile and Home to the top of the list
             */
            var mobile = (from m in phoneTypes
                          where m.Key.Equals(971880001)
                          select m).SingleOrDefault();
            var home = (from m in phoneTypes
                        where m.Key.Equals(971880000)
                        select m).SingleOrDefault();
            phoneTypes.Remove(mobile);
            phoneTypes.Remove(home);
            phoneTypes.Insert(0, home);
            phoneTypes.Insert(0, mobile);
            return Json(phoneTypes, JsonRequestBehavior.AllowGet);
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetAccountTypes(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Return Account Types to be used by UI components
        /// </summary>
        /// <returns>Account Types</returns>
        [HttpGet]
        public ActionResult GetAccountTypes()
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var accountTypes = GlobalsCache.GetAccountTypesForUI(connMgr.Service);
                return Json(accountTypes, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetContactTypes(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Return Contact Types  to be used by UI components
        /// </summary>
        /// <returns>Contact Types</returns>
        [HttpGet]
        public ActionResult GetContactTypes()
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var contactTypes = GlobalsCache.GetContactTypesForUI(connMgr.Service);
                return Json(contactTypes, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetCountries(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Return Countries to be used by UI components
        /// </summary>
        /// <returns>Countries</returns>
        /// 
        [HttpGet]
        public ActionResult GetCountries()
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var countries = GlobalsCache.GetCountryListForUI(connMgr.Service);
                /*
                 * Moving US and CA to the top of the list
                 */
                var us = (from c in countries
                          where c.Key.Equals(new Guid("61F3898E-D8C0-E611-80E2-005056814178"))
                          select new KeyValuePair<Guid, string>(c.Key, "USA")).FirstOrDefault();
                
                var ca = (from c in countries
                          where c.Key.Equals(new Guid("D9F1898E-D8C0-E611-80E2-005056814178"))
                          select c).FirstOrDefault();
             
                countries.Remove((from c in countries
                                  where c.Key.Equals(new Guid("61F3898E-D8C0-E611-80E2-005056814178"))
                                  select c).FirstOrDefault());
                countries.Remove(ca);
                countries.Insert(0, ca);
                countries.Insert(0, us);
                return Json(countries, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetStates(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Return States to be used by UI components
        /// </summary>
        /// <returns>States</returns>
        [HttpGet]
        public ActionResult GetStates()
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var states = GlobalsCache.GetStatesListForUI(connMgr.Service);
                return Json(states, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetSecurityQuestions(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Return Security Questions to be used by UI components
        /// </summary>
        /// <returns>States</returns>
        [HttpGet]
        public ActionResult GetSecurityQuestions()
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var data = GlobalsCache.GetSecurityQuestionsForUI(connMgr.Service);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Find duplicate contacts (Exactly match)
        /// </summary>
        /// <param name="firstName">First Name</param>
        /// <param name="lastName">Last Name</param>
        /// <param name="email">Email</param>
        /// <returns>View Action Result</returns>
        [HttpPost]
        public ActionResult FindDuplicates(string firstName,
                                           string lastName,
                                           string email,
                                           Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                CustomerRegistrationLogic logic = new CustomerRegistrationLogic(connMgr.Service);
                var result = logic.SearchContactExactMatch(firstName, lastName, email);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CheckUsername()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// POST action to validate a Username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>View Action Result with the List of errors as JSON. Empty means no errors</returns>
        [HttpPost]
        public ActionResult CheckUsername(string username, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                ContactLogic logic = new ContactLogic(connMgr.Service);
                //@todo: Should we add the check if the user is available to this method?
                CubResponse<WSUsernamePrevalidateResults> validateResults = logic.ValidateUsername(username);
                //var result = Newtonsoft.Json.JsonConvert.SerializeObject(validateResults.ResponseObject);
                return Json(validateResults.ResponseObject, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage ValidateAddress(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Post Action to check Zip/Postal Code
        /// </summary>
        /// <param name="code">Zip/Postal Code</param>
        /// <param name="country">MSD Country ID</param>
        /// <param name="state">MSD State ID</param>
        /// <returns>View Action Result</returns>
        [HttpGet]
        public ActionResult ValidateAddress(WSAddress address)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var logic = new AddressLogic(connMgr.Service);
                var result = logic.IsAddressValid(address);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Validate Fields
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Validation Errors</returns>
        /*[HttpPost]
        public ActionResult ValidateFields(CustomerRegistrationModel model)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                CustomerRegistrationLogic logic = new CustomerRegistrationLogic(connMgr.Service);
                List<KeyValuePair<string, string>> errors = logic.ValidateFieldsNewCustomerRegistration(model);
                var result = Newtonsoft.Json.JsonConvert.SerializeObject(errors);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }*/
    }
}
