/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using CUB.MSD.Web.Models;
using CUB.MSD.Web.Models.MasterSearch;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class SessionController : BaseController
    {
        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CreateSession()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Create a session for the current user.
        /// </summary>
        /// <param name="userId">The current user</param>
        /// <param name="recordId">Optional record ID to associate with the session</param>
        /// <param name="entityName">Optional record type to associate with the session</param>
        /// <returns>WSSessionResponse</returns>
        [HttpPost]
        public ActionResult CreateSession(Guid userId, Guid? recordId, string entityName, string channelName)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                var logic = new SessionLogic(connMgr.Service);
                var response = logic.CreateSession(userId, recordId, entityName, channelName);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage RetrieveSession()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Search for and return an active session for the current user.
        /// </summary>
        /// <param name="userId">The current user</param>
        /// <returns>WSSessionResponse</returns>
        [HttpGet]
        public ActionResult RetrieveSession(Guid userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                var logic = new SessionLogic(connMgr.Service);
                var response = logic.RetrieveSession(userId);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetSessionDetails()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get session detail by session number
        /// </summary>
        /// <param name="sessionId">Session Id</param>
        /// <returns>WSSessionResponse</returns>
        [HttpGet]
        public ActionResult GetSessionDetails(Guid sessionId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var logic = new SessionLogic(connMgr.Service);
                var response = logic.GetSessionDetail(sessionId);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        public HttpResponseMessage SearchSession()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get session detail by session number
        /// </summary>
        /// <param name="sessionId">Session Id</param>
        /// <returns>WSSessionResponse</returns>
        [HttpGet]
        public ActionResult SearchSession(string sessionNumber)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var logic = new SessionLogic(connMgr.Service);
                var response = logic.SearchSession(sessionNumber);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetLinkedSessions()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get session detail by session number
        /// </summary>
        /// <param name="sessionId">Session Id</param>
        /// <returns>WSSessionResponse</returns>
        [HttpGet]
        public ActionResult GetLinkedSessions(Guid sessionId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var logic = new SessionLogic(connMgr.Service);
                var response = logic.GetLinkedSessions(sessionId);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage RetrieveActiveUsers(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Retrieves a list of active users, excluding the user making this call
        /// </summary>
        /// <param name="userId">The Guid of the user making this call/param>
        /// <returns>ActionResults
        /// </returns>
        [HttpGet]
        public ActionResult RetrieveActiveUsers(Guid userId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var logic = new SessionLogic(connMgr.Service);
                var response = logic.RetrieveActiveUsers(userId);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage EndSession()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Closes an active session
        /// </summary>
        /// <param name="sessionId">The guid of the session record to close</param>
        /// <param name="userId">The Guid of the user record closing the session</param>
        /// <returns>Success code with no content</returns>
        [HttpPost]
        public HttpResponseMessage EndSession(Guid sessionId, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                var logic = new SessionLogic(connMgr.Service);
                logic.EndSession(sessionId);
                return new HttpResponseMessage(System.Net.HttpStatusCode.NoContent);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage UpdateNotes()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Update Session Notes
        /// </summary>
        /// <param name="sessionId">The guid of the session record to close</param>
        /// <param name="notes">Notes</param>
        /// <returns>Success code with no content</returns>
        [HttpPost]
        public HttpResponseMessage UpdateNotes(Guid sessionId, string notes)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var logic = new SessionLogic(connMgr.Service);
                logic.UpdateNotes(sessionId, notes);
                return new HttpResponseMessage(System.Net.HttpStatusCode.NoContent);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage TransferSession()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Tranfers an active session to another user
        /// </summary>
        /// <param name="sessionId">The guid of the session record to close</param>
        /// <param name="userId">The Guid of the user record transfering the session</param>
        /// <param name="transferToId">The Guid of the user record that you want to transfer the record to</param>
        /// <returns>ActionResult
        /// </returns>
        [HttpPost]
        public ActionResult TransferSession(Guid sessionId, Guid userId, Guid transferToId, Guid? callerId)
        {
            using (var connMgr = new CrmConnectionManager(callerId))
            {
                var logic = new SessionLogic(connMgr.Service);
                var response = logic.TransferSession(sessionId, userId, transferToId);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage Update()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Update the given session fields and return the updated session.
        /// It is only necessary to assign the fields that need upating to the
        /// session argument. Any other fields will remain as they were.
        /// </summary>
        /// <param name="userId">The calling user ID</param>
        /// <param name="session">An object containing the updated MSD session fields</param>
        /// <returns>WSSessionResponse</returns>
        [HttpPost]
        public ActionResult Update(Guid? userId, cub_CSRSession session)
        {
            using (var conn = new CrmConnectionManager(userId))
            {
                var logic = new SessionLogic(conn.Service);
                var result = logic.Update(session);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpOptions]
        public HttpResponseMessage LinkSessions()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Link child session to parent session
        /// </summary>
        /// <param name="userId">User Id making the request</param>
        /// <param name="parentSession">Parent session GUID</param>
        /// <param name="childSession">Session Id to be linked as child session</param>
        /// <returns>WSSessionResponse</returns>
        [HttpPost]
        public HttpResponseMessage LinkSessions(Guid userId, Guid parentSession, Guid childSession)
        {
            
            using (var conn = new CrmConnectionManager(userId))
            {
                var logic = new SessionLogic(conn.Service);
                logic.LinkSession(parentSession, childSession);
                return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
            }
            
        }

        [HttpOptions]
        public HttpResponseMessage RemoveLinkedSession()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Remove the link between two sessions
        /// </summary>
        /// <param name="userId">User Id making the request</param>
        /// <param name="linkSessionId">Link Session Id</param>
        /// <returns>WSSessionResponse</returns>
        [HttpDelete]
        public HttpResponseMessage RemoveLinkedSession(Guid userId, Guid linkSessionId)
        {
            using (var conn = new CrmConnectionManager(userId))
            {
                var logic = new SessionLogic(conn.Service);
                logic.RemoveLinkedSession(linkSessionId);
                return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
            }
        }
    }
}
