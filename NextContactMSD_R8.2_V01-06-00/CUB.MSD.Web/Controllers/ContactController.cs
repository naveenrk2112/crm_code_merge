﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class ContactController : BaseController
    {
        //http://localhost:12665/Contact/ForgotPassword?contactId=2B9634F0-3F29-E711-80EA-005056813E0D
        [HttpPost]
        public ActionResult ForgotPassword(Guid contactId, Guid customerId, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                ContactLogic logic = new ContactLogic(connMgr.Service);
                CubResponse<WSGenerateTokenResponse> cubResponse = new CubResponse<WSGenerateTokenResponse>();
                cubResponse = logic.GenerateToken(customerId, contactId, cub_verificationtokentype.Password);

                if (cubResponse.Success)
                {
                    if (cubResponse.ResponseObject != null)
                    {
                        DateTime tokenExpiry = cubResponse.ResponseObject.tokenExpiryTime.Value;
                        tokenExpiry = DateTime.SpecifyKind(tokenExpiry, DateTimeKind.Utc);
                        logic.RequestSendingOfVerificationToken(customerId, contactId, cubResponse.ResponseObject.token, tokenExpiry, cub_verificationtokentype.Password);
                    }
                }

                return Json(cubResponse, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetContactNameEmailById()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Contact/GetContactNameEmailById?contactId=33CB941B-881C-49BD-BDB8-657BCC050B38
        /// <summary>
        /// Get a contact First and Last names and Email by contact ID
        /// </summary>
        /// <param name="contactId">Contact ID</param>
        /// <returns>Contact First/Last Names and Email as a JSON object</returns>
        [HttpGet]
        public ActionResult GetContactNameEmailById(Guid contactId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                ContactLogic logic = new ContactLogic(connMgr.Service);
                string info = logic.GetContactNameEmailById(contactId);
                return Json(info, JsonRequestBehavior.AllowGet);
            }
        }
    }
}