/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using CUB.MSD.Model;
using CUB.MSD.Web.Models;
using CUB.MSD.Web.Models.MasterSearch;
using Microsoft.Xrm.Sdk;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Net.Http;

namespace CUB.MSD.Web.Controllers
{
    public class CustomerNotificationController : BaseController
    {
        // GET: CustomerNotification
        [HttpPost]
        public ActionResult Index()
        {
            return View();
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetCustomerNotification()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        //http://localhost:12665/CustomerNotification/GetCustomerNotification?customerId=2B9634F0-3F29-E711-80EA-005056813E0D
        [HttpGet]
        public ActionResult GetCustomerNotification(string customerId, string contactId, string startDate, string endDate, string type,
                                                    string status, string channel, string recipient, string notificationReference, 
                                                    string sortBy, int? offset, int? limit)
        {
            //string startDate;
            //string endDate = DateTime.Today.ToString("yyyy-MM-dd");

            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSGetCustomerNotificationResponse result = nis.GetCustomerNotification(customerId, contactId, startDate, endDate, type, status, channel, recipient, notificationReference, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetCustomerNotificationDetail()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        //http://localhost:12663/CustomerNotification/GetCustomerNotificationDetail?customerId=2B9634F0-3F29-E711-80EA-005056813E0D&notificationId=8685
        [HttpGet]
        public ActionResult GetCustomerNotificationDetail(string customerId, string notificationId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSGetCustomerNotificationDetailResponse result = nis.GetCustomerNotificationDetail(customerId, notificationId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CustomerNotificationResend()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // tried running post below from postman in debug but couldn't connect to test
        //http://localhost:12663/CustomerNotification/CustomerNotificationResend?customerId=2B9634F0-3F29-E711-80EA-005056813E0D&contactId=b5262c34-ce29-e711-80e6-005056814569&notificationId=CNG-8231&notificationType=NewCustomer&allowReformat=true&channel=EMAIL&clientRefId=b5262c34-ce29-e711-80e6-005056814569
        [HttpPost]
        public ActionResult CustomerNotificationResend(string customerId, string contactId, string notificationId, string notificationType,
                                                       bool allowReformat, string channel, string clientRefId, Guid? userId)
        { 
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                CubResponse<WSCustomerNotificationResendResponse> result = nis.CustomerNotificationResend(customerId, contactId, notificationId, notificationType, allowReformat, channel, clientRefId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetCustomerNotificationPreferences()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        //http://localhost:12663/CustomerNotification/GetCustomerNotificationPreferences?customerId=B08800FD-2BF2-E611-80E2-005056814569
        /// <summary>
        /// This API is used to retrieve notification preferences for a customerís contact.
        /// This method accepts optional filters by:
        /// contactId: to narrow down the results to a specific contact or if this filter is not provided, all contacts are returned.
        /// reqType: to narrow down the results to only show Mandatory or NotMandatory notification types and channels.
        /// channel: to narrow down the results to only show notification types that are set to be sent by a given channel.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="contactId">Contact ID (Optional)</param>
        /// <param name="reqType">Required Type (Optional): M</param>
        /// <param name="channel">Channel</param>
        /// <param name="type">Type</param>
        /// <returns>Notification Preferences</returns>
        [HttpGet]
        public ActionResult GetCustomerNotificationPreferences(string customerId, string contactId, string reqType, string channel, string type)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetCustomerNotificationPreferences(customerId, contactId, reqType, channel, type);
                JsonResult ret = Json(result, JsonRequestBehavior.AllowGet);
                
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage CustomerNotificationPreferencesPatch()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// update contact information and notification preferences for a list of customerís contacts.  
        /// It accepts partial updates for the notification preferences or for the contact information with the values in the request.
        /// If in the request, a preference by notification type and channel is disabling a mandatory notification type, all other updates in the request are processed but the disabling of the mandatory notification type is not applied and no errors are returned.
        /// </summary>
        /// <param name="requestData">Customer Notification Preference Patch Data</param>
        /// <returns>Customer Notification Preference Patch Response</returns>
        [HttpPost]
        public ActionResult CustomerNotificationPreferencesPatch(CustomerNotificationPreferencesRequest requestData, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.CustomerNotificationPreferencesPatch(requestData);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetCustomerContacts()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        //http://localhost:12665/CustomerNotification/GetCustomerContacts?customerId=2B9634F0-3F29-E711-80EA-005056813E0D
        /// <summary>
        /// This API is used to retrieve contacts related to a customer.
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <returns>Collection of Contacts</returns>
        [HttpGet]
        public ActionResult GetCustomerContacts(string customerId)
        {
            WSCustomerContact wsresp = new WSCustomerContact();

            using (var connMgr = new CrmConnectionManager())
            {
                AccountLogic acct = new AccountLogic(connMgr.Service);
                IEnumerable<WSContact> ret = acct.GetByAccountId(new Guid(customerId));
                return Json(ret, JsonRequestBehavior.AllowGet);
            }
        }
        
        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetConfigNotificationChannel()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        //http://localhost:12665/CustomerNotification/GetConfigNotificationChannel?enabled=true
        /// <summary>
        /// This API is used to retrieve config notification channels
        /// This method accepts optional filters by:
        /// enabled: to narrow down the results to a enabled/disabled notifications
        /// </summary>
        /// <returns>Config Notification Channel Response</returns>
        [HttpGet]
        public ActionResult GetConfigNotificationChannel(bool? enabled)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSGetConfigNotificationChannelResponse result = nis.GetConfigNotificationChannel(enabled);
                JsonResult ret = Json(result, JsonRequestBehavior.AllowGet);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
