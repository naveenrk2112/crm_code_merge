/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;

namespace CUB.MSD.Web.Controllers
{
    public class CustomerAddressController : BaseController
    {
        // GET: CustomerAddress
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CheckDuplicate(Guid accountId, string street1, string street2, string city, Guid stateId, Guid zipId, Guid countryId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                AddressLogic logic = new AddressLogic(connMgr.Service);
                CubResponse<WSAddress> cubResponse = new CubResponse<WSAddress>();
                cubResponse = logic.AddressPreCreateValidate(accountId, street1, street2, city, stateId, zipId, countryId);
                return Json(cubResponse, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpDelete]
        public ActionResult DeleteAddress(Guid customerId, Guid addressId, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                using (var nisApi = new NISApiLogic(connMgr.Service))
                {
                    var deleteResponse = nisApi.DeleteAddress(customerId, addressId);
                    return Json(deleteResponse, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult AddressPreUpdateValidate(string street1, string street2, string street3, string city,
            Guid stateId, Guid zipId, Guid countryId, Guid customerId, Guid addressId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                cub_Address cubAddress = new cub_Address();
                cubAddress.cub_Street1 = street1;
                cubAddress.cub_Street2 = street2;
                cubAddress.cub_Street3 = street3;
                cubAddress.cub_City = city;
                cubAddress.cub_CountryId = new EntityReference { LogicalName = cub_Country.EntityLogicalName, Id = countryId };
                cubAddress.cub_StateProvinceId = new EntityReference { LogicalName = cub_StateOrProvince.EntityLogicalName, Id = stateId };
                cubAddress.cub_PostalCodeId = new EntityReference { LogicalName = cub_ZipOrPostalCode.EntityLogicalName, Id = zipId };
                cubAddress.cub_AccountAddressId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = customerId };
                cubAddress.Id = addressId;

                using (AddressLogic logic = new AddressLogic(connMgr.Service))
                {
                    CubResponse<WSAddressDependenciesResponse> cubResponse = new CubResponse<WSAddressDependenciesResponse>();
                    cubResponse = logic.AddressPreUpdateValidate(cubAddress);
                    return Json(cubResponse, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetAddressByAddressId()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        //http://localhost:12663/CustomerAddress/GetAddressByAddressId?addressId=7FD6ADEC-CD29-E711-80E6-005056814569
        [HttpGet]
        public ActionResult GetAddressByAddressId(string addressId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                AddressLogic logic = new AddressLogic(connMgr.Service);
                CubResponse<WSAddress> cubResponse = new CubResponse<WSAddress>();
                cubResponse = logic.IsAddressIdValid(addressId);
                return Json(cubResponse, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage AddressUpdate()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Address Update makes a call to NIS Address PUT api
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="addressId"></param>
        /// <param name="requestBody"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddressUpdate(string customerId, string addressId, WSAddressUpdateRequest requestBody, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                using (AddressLogic logic = new AddressLogic(connMgr.Service))
                {
                    CubResponse<WSAddressUpdateResponse> cubResponse = new CubResponse<WSAddressUpdateResponse>();
                    cubResponse = logic.UpdateAddress(customerId, addressId, requestBody);
                    return Json(cubResponse, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
