﻿using CUB.MSD.Core;
using CUB.MSD.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class CustomerController : Controller
    {

        [HttpOptions]
        public HttpResponseMessage GetActivities()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12665/Customer/GetActivities?customerId=4C167FB1-085F-4547-82F7-4C87B693F09F
        /// <summary>
        /// This API is used to retrieve activity history that was associated with the customer based upon filtering parameters.
        /// 2.8.22 customer/<customer-id>/activity GET
        /// </summary>
        /// <param name="customerId">Customer MSD ID</param>
        /// <param name="contactId">(Optional) The contact Id for which the activity should be shown.</param>
        /// <param name="startDateTime">(Optional) The date and time that should be used to filter the starting date of the range. 
        ///  If not provided then the default should be configurable with a default value of now – 30 days.</param>
        /// <param name="endDateTime">(Optional)  The date and time that should be used to filter the ending date of the range.
        /// If not provided then the default should be the beginning of next day. 
        /// The maximum end date should be as number of days from the start date.</param>
        /// <param name="type">(Optional)  An identifier for the type of activity to return in the results.
        /// The default is to return all activity types if no value is provided.</param>
        /// <param name="activitySubject">(Optional)  An identifier for the subject of activity to return in the results. 
        ///     Activity subject is a logical grouping of activity types. The default is to return all activity subject if no value is provided.</param>
        /// <param name="activityCategory">(Optional)  An identifier for the category of activity to return in the results. 
        ///     Category is used to distinguish between event and audit. The default is to return all activity category if no value is provided.</param> 
        /// <param name="channel">(Optional) Used to filter the activities by channel.</param>
        /// <param name="origin">(Optional)  An identifier for the origin of activity to return in the results. 
        ///     Origin tells the component which initiated the action which caused the activities to be generated. The default is to return all activity origin if no value is provided.</param>
        /// <param name="globalTxnId">(Optional)  An identifier for the category of activity to return in the results. 
        ///     Global transaction id is a mechanism to tie multiple method being called as part of a single action. The default is to return all activity category if no value is provided.</param>
        /// <param name="userName">(Optional) Used to filter the activities by the username that performed the activity.
        /// Allow for partial string with wild card ‘*’.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: “type.asc,channel.desc” indicates sort by type ascending and status descending.
        /// Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) Used to identify the starting index that should be provided within the page in the search result.
        /// The default is to start at the first row of data (0) if no value is provided.</param>
        /// <param name="limit">(Optional) Used to identify the number of rows per page in the search result in the response.
        /// The default is to return 20 rows if no value is provided.</param>
        /// <returns>Customer Activities</returns>
        [HttpGet]
        public ActionResult GetActivities(string customerId, string contactId, string startDateTime, string endDateTime,
                                          string type, string activityCategory, string activitySubject, string channel,
                                          string origin, string globalTxnId, string userName, string crmSessionId,
                                          string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetCustomerActivities(customerId, contactId, startDateTime, endDateTime, type, 
                                            activityCategory, activitySubject,  channel, origin, globalTxnId,
                                            userName, crmSessionId, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetPrimaryContact()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetPrimaryContact(Guid customerId)
        {
            using (var conn = new CrmConnectionManager())
            {
                var logic = new AccountLogic(conn.Service);
                var result = logic.GetPrimaryContact(customerId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public HttpResponseMessage getContactsForVoidTrip()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult getContactsForVoidTrip(Guid customerId)
        {
            using (var conn = new CrmConnectionManager())
            {
                var logic = new AccountLogic(conn.Service);
                var result = logic.getContactsForVoidTrip(customerId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

    }
}