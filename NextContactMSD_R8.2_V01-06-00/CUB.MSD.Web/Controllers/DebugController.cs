﻿using CUB.MSD.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CUB.MSD.Web.Controllers
{
    public class DebugController : BaseController
    {
        [HttpPost]
        public HttpResponseMessage ClearGlobalsCache()
        {
            var success = GlobalsCache.Clear();
            if (success)
            {
                return new HttpResponseMessage { StatusCode = HttpStatusCode.NoContent };
            }
            else
            {
                return new HttpResponseMessage { StatusCode = HttpStatusCode.InternalServerError };
            }
        }

        /// <summary>
        /// Used to update the globals cache duration. This method is called by
        /// the plugin CUB.MSD.Plugins.CacheDurationUpdatePlugin.
        /// </summary>
        /// <param name="duration">a parsable double value</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage UpdateCacheDuration(string duration)
        {
            var success = GlobalsCache.UpdateCacheExpirationMinutes(duration) && GlobalsCache.Clear();
            if (success)
            {
                return new HttpResponseMessage { StatusCode = HttpStatusCode.NoContent };
            }
            else
            {
                return new HttpResponseMessage { StatusCode = HttpStatusCode.InternalServerError };
            }
        }
    }
}