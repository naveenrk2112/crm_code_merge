﻿using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    /// <summary>
    /// Customer Order Controller
    /// </summary>
    public class CustomerOrderController : BaseController
    {
        [HttpOptions]
        public HttpResponseMessage GetOrderHistory()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        //http://localhost:12663/CustomerOrder/GetOrderHistory?customerId=123
        //
        /// <summary>
        /// Get Customer Order History
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="orderType">Order Type</param>
        /// <param name="orderStatus">Order Status</param>
        /// <param name="paymentStatus">Payment Status</param>
        /// <param name="sortBy">Sort By</param>
        /// <param name="offset">Offset</param>
        /// <param name="limit">Limit</param>
        /// <returns>Order History</returns>
        [HttpGet]
        public ActionResult GetOrderHistory(string customerId, string startDateTime, string endDateTime, 
                                            string orderType, string orderStatus, string paymentStatus,
                                            int? orderId, bool? realTime, string sortBy, int? offset, int? limit,
                                            Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetCustomerOrderHistory(customerId, startDateTime, endDateTime, orderType, orderStatus, paymentStatus, orderId, realTime, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetOrderHistoryDetail()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12663/CustomerOrder/GetOrderHistoryDetail?customerId=123&orderid=456
        /// <summary>
        /// Get Customer Order History Detail
        /// </summary>
        /// <param name="customerId">Customer ID</param>
        /// <param name="orderId">Order ID</param>
        /// <returns>Order History Detail</returns>
        [HttpGet]
        public ActionResult GetOrderHistoryDetail(string customerId, string orderId, bool? realtimeResults)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetCustomerOrderHistoryDetail(customerId, orderId, realtimeResults);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetOrder()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // http://localhost:12663/CustomerOrder/GetOrder?customerId=123&orderid=456
        /// <summary>
        /// Get Order
        /// </summary>
        /// <param name="customerId">(Required) The unique identifier for the customer.</param>
        /// <param name="orderId">(Required) The unique identifier for the order.</param>
        /// <param name="closeAccountRefresh">(Optional) Update close account purse and pass data before returning.
        /// Default value is false.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetOrder(string customerId, string orderId, string closeAccountRefresh)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSGetCustomerOrderResponse result = nis.GetCustomerOrder(customerId, orderId, closeAccountRefresh);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetDebtCollection()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpPost]
        public ActionResult GetDebtCollection(string customerId, string unregisteredEmail, string clientRefId, string subsystem, string subsystemAccountReference, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetOrderDebtCollect(customerId, unregisteredEmail, clientRefId, subsystem, subsystemAccountReference);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpOptions]
        public HttpResponseMessage PostOrderAdjustment()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// 2.14.4 order/adjustment POST
        /// This API is used to submit (create) an adjustment order for a given registered or unregistered customer.
        /// The order is validated and accepted.Adjustment orders may require approval before being processed, if the order is approved, it is then processed immediately.
        /// If not, it stays in OrderAccepted status until the order is approved or rejected.
        /// When the adjustment is for a registered customer, the customerId in the request is required and if an anonymous email address is provided,
        /// it is ignored and the customer notification preferences are used to send notifications.
        ///This method assumes that the client has validated and provided the appropriate reason codes and operator financially responsible for the adjustment.
        /// </summary>
        /// <param name="customerId">(Conditionally-Required) Unique identifier for the customer. Required for adjustments on One Account and all registered customers.</param>
        /// <param name="unregisteredEmail">(Optional)  If the customer is not registered (i.e. customerId is not provided), the email address to send order notifications.
        /// If customerId is specified, this field will be ignored.
        /// </param>
        /// <param name="clientRefId">(Required)  The client’s unique identifier for sending this notification.
        /// This field will be used for detecting duplicate requests and therefore must be globally unique.
        /// This value should be specified as: "<device-id>:<unique-id>"
        ///</param>
        /// <param name="productLineItems">(Required) Line items to adjust product in the one account or subsystem account.
        /// When other types are are added, this will become Conditionally-Required.
        /// </param>
        /// <param name="reasonCode">(Required) Reason code for the adjustment.</param>
        /// <param name="notes">(Optional)  Notes for the adjustment.</param>
        /// <param name="financiallyResponsibleOperatorId">(Required) Operator impacted by the adjustment financially.</param>
        /// <param name="isApproved">(Optional) Specifies if the order is approved to be processed. Default is true. </param>
        /// <param name="userId">User ID</param>
        /// <returns>Order Adjustment response</returns>
        [HttpPost]
        public ActionResult PostOrderAdjustment(string customerId, string unregisteredEmail, string clientRefId,
                                                List<WSAdjustProductLineItem> productLineItems, string reasonCode,
                                                string notes, int financiallyResponsibleOperatorId, bool? isApproved,
                                                Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.PostOrderAdjustment(customerId, unregisteredEmail, clientRefId,
                                                     productLineItems, reasonCode,
                                                     notes, financiallyResponsibleOperatorId, isApproved,
                                                     userId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage PostTravelCorrection()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// 2.15.15 order/travelcorrection POST
        /// This API is used to submit (create) a travel correction order for a given registered or unregistered customer. 
        /// A travel correction order may include multiple line items and supports tap/trip corrections and voids. 
        ///The order is validated before it is accepted.Travel correction orders may require approval before being processed.
        ///If the order is approved, all the trip corrections and trip voids are immediately processed.
        ///The response contains information about the order and processing results.
        /// When the order is for a registered customer, the customerId in the request is required and if an anonymous email address is provided, 
        /// it is ignored and the customer notification preferences are used to send notifications.
        /// </summary>
        /// <param name="request">Request</param>
        /// <param name="userId">User ID</param>
        /// <returns>NIS Response as JSON</returns>
        [HttpPost]
        public ActionResult PostTravelCorrection(WSTripCorrection request, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.PostTravelCorrection(request, userId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}