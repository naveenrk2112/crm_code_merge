/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using CUB.MSD.Web.Models;
using CUB.MSD.Web.Models.MasterSearch;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class SubSystemController : BaseController
    {
        [HttpOptions]
        public HttpResponseMessage GetTokenTypeMapping(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        //http://localhost:12663/SubSystem/GetTokenTypeMapping
        public ActionResult GetTokenTypeMapping()
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                string result = nis.GetSubsystemTokenTypeMappings();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage GetCasesRelatedToSubsystemId()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetCasesRelatedToSubsystemId(string subsystemId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var subsystem = new SubsystemLogic(connMgr.Service);
                var result = subsystem.GetCasesRelatedToSubsystemId(subsystemId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage TravelTokenCanBeLinked()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// 2.11.3 subsystem/<subsystem-id>/traveltoken/canbelinked POST
        ///  This API is used to verify if the token exists in the subsystem and check if its eligible to be linked or not. 
        ///  If the token is not in the sub-system, based on the configuration if it is okay to automatically register in the subsystem, 
        ///  it will complete the token registration.
        ///  The API also returns information for the client to verify if the customer is indeed owner of the token.
        /// </summary>
        /// <param name="subsystemId">(Required) Unique identifier for the subsystem where the travel token is registered.</param>
        /// <param name="bankTravelToken">(Required) bank travel token.</param>
        /// <param name="oneAccountId">(Optional) Unique identifier of the OneAccount if the customer has a OneAccount by this point.</param>
        /// <param name="foreignCustomerId">(Optional) Unique identifier for the foreign customer.</param>
        /// <param name="foreignTokenSubsystem">(Optional) Unique identifier for the foreign token subsystem if the token is owned by a different 
        /// subsystem than the account-based subsystem.</param>
        /// <param name="autoCreateFlag">(Required) To indicate if the token should be auto created in the subsystem in case it is not yet created.
        /// For certain token types the system will not allow auto-create. In this case the autoCreateFlag will be ignored.</param>
        /// <returns>client to verify if the customer is indeed owner of the token.</returns>
        [HttpPost]
        public ActionResult TravelTokenCanBeLinked(string tokenType, string subsystemId, string encryptedToken, string nonce,
                                                   int? oneAccountId, string foreignCustomerId, string foreignTokenSubsystem, bool autoCreateFlag)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var nis = new SubsystemLogic(connMgr.Service);
                var result = nis.TravelTokenCanBeLinked(tokenType, subsystemId, encryptedToken, nonce,
                                                        oneAccountId, foreignCustomerId, foreignTokenSubsystem, autoCreateFlag);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    

        [HttpOptions]
        public HttpResponseMessage BlockSubsystemTransitAccount()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// 2.12.5 subsystem/<subsystem-id>/subsystemaccount/<account-ref>/block POST
        ///  This API is used to block the subsystem account.
        /// </summary>
        /// <param name="subsystemId">(Required) Unique identifier for the subsystem where the travel token is registered.</param>
        /// <param name="accountNumber">(Required) Account number of the travel token associated within the given subsystem.</param>
        /// <param name="reasonCode">(Required)  Reason code for the blocking.</param>
        /// <param name="notes">(Optional) Notes for the adjustment.</param>
        /// <returns>No data is returned by this method. Common headers and http response codes are returned.</returns>
        [HttpPost]
        public ActionResult BlockSubsystemTransitAccount(string subsystemId, string accountNumber,
                                                   string reasonCode, string notes)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var subsystem = new SubsystemLogic(connMgr.Service);
                var result = subsystem.BlockSubsystemTransitAccount(subsystemId, accountNumber,
                                                        reasonCode, notes);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

       
        public HttpResponseMessage UnblockSubsystemTransitAccount()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// 2.12.6 subsystem/<subsystem-id>/subsystemaccount/<account-ref>/unblock POST
        ///  This API is used to unblock the subsystem account.
        /// </summary>
        /// <param name="subsystemId">(Required) Unique identifier for the subsystem where the travel token is registered.</param>
        /// <param name="accountNumber">(Required) Account number of the travel token associated within the given subsystem.</param>
        /// <param name="reasonCode">(Required)  Reason code for the blocking.</param>    
        /// <returns>No data is returned by this method.Common headers and http response codes are returned.</returns>
        [HttpPost]
        public ActionResult UnblockSubsystemTransitAccount(string subsystemId, string accountNumber,
                                                   string reasonCode, string notes)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var subsystem = new SubsystemLogic(connMgr.Service);
                var result = subsystem.UnblockSubsystemTransitAccount(subsystemId, accountNumber,
                                                        reasonCode, notes);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpOptions]
        public HttpResponseMessage TokenAction()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// 2.12.5 subsystem/<subsystem-id>/subsystemaccount/<account-ref>/block POST
        ///  This API is used to block the subsystem account.
        /// </summary>
        /// <param name="subsystemId">(Required) Unique identifier for the subsystem where the travel token is registered.</param>
        /// <param name="accountNumber">(Required) Account number of the travel token associated within the given subsystem.</param>
        /// <param name="reasonCode">(Required)  Reason code for the blocking.</param>
        /// <param name="notes">(Optional) Notes for the adjustment.</param>
        /// <returns>No data is returned by this method. Common headers and http response codes are returned.</returns>
        [HttpPost]
        public ActionResult TokenAction(string transitAccountId, string subsystemId, string travelToken, string customerId, 
                                                   string reasonCode, string notes, string tokenAction)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var nis = new NISApiLogic(connMgr.Service);
                var result = nis.TokenAction(transitAccountId, subsystemId, travelToken, customerId,
                                                        reasonCode, notes, tokenAction);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

    }
}