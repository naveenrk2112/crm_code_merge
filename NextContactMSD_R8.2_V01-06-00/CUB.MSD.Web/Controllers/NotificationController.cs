﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class NotificationController : BaseController
    {

        [HttpOptions]
        public HttpResponseMessage GetNotifications()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetNotifications(string recipient, string notificationReference, string globalTxnId, string startDateTime, string endDateTime,
                                          string type, string status, string channel, string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetNotifications(recipient, notificationReference, globalTxnId, startDateTime, endDateTime,
                                          type, status, channel, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpOptions]
        public HttpResponseMessage GetNotificationDetails()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetNotificationDetails(string notificationId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetNotificationDetails(notificationId);
                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }
    }
}