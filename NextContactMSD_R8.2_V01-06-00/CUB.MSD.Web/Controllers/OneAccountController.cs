/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Core;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using CUB.MSD.Web.Models;
using CUB.MSD.Web.Models.MasterSearch;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace CUB.MSD.Web.Controllers
{
    public class OneAccountController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="foo"></param>
        /// <returns></returns>
        [HttpOptions]
        public HttpResponseMessage GetOneAccountSummary(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        // GET: OneAccount
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index()
        {
            return View();
        }

        //http://localhost:12663/OneAccount/GetOneAccountSummary?customerId=B08800FD-2BF2-E611-80E2-005056814569&returnSubsystemAccountDetailedInfo=true
        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="returnSubsystemAccountDetailedInfo"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetOneAccountSummary(string customerId, bool? returnSubsystemAccountDetailedInfo)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSOneAccountSummaryResponse result = nis.GetOneAccountSummary(customerId, returnSubsystemAccountDetailedInfo);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// TODO: For testing only
        /// </summary>
        /// <returns></returns>
        [HttpOptions]
        public HttpResponseMessage LinkSubsystemToCustomeraccount()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Link subsystem to one account
        /// </summary>
        /// <param name="oneaccountId">one account id</param>
        /// <param name="subsystemId">subssystem id</param>
        /// <param name="accountRef">account ref</param>
        /// <param name="subsystemAccountNickname">subsystem account nick name</param>
        /// <returns>Result as JSON</returns>
        [HttpPost]
        public ActionResult LinkSubsystemToCustomeraccount(string oneaccountId, string subsystemId, string accountRef, string subsystemAccountNickname, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSLinkSubssystemToCustomerAccountResponse result =
                    nis.LinkSubssystemToCustomerAccount(oneaccountId, subsystemId, accountRef, subsystemAccountNickname);
                if (result.hdr.result == RestConstants.SUCCESSFUL)
                {
                    // Create MSD association for rolling-up transit account activities
                    var logic = new TransitAccountLogic(connMgr.Service);
                    logic.AssociateWithCustomer(accountRef, subsystemId, oneaccountId);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// TODO: For testing only
        /// </summary>
        /// <returns></returns>
        [HttpOptions]
        public HttpResponseMessage CanBeDelinked()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// FDS 3.4.13 - Delink Subsystem Account from OneAccount
        /// API 2.12.11 - canbedelinked
        /// </summary>
        /// <param name="oneaccountId">one account id</param>
        /// <param name="subsystemId">subssystem id</param>
        /// <param name="subsystemAccountNickname">subsystem account nick name</param>
        /// <returns>Result as JSON</returns>
        [HttpGet]
        public ActionResult CanBeDelinked(string oneaccountId, string subsystemId, string subsystemAccount, string customerId, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                if (string.IsNullOrEmpty(oneaccountId) && !string.IsNullOrEmpty(customerId))
                {
                    AccountLogic accountLogic = new AccountLogic(connMgr.Service);
                    oneaccountId = accountLogic.GetAccountOneAccountId(customerId);
                }

                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSCanBeDelinkedSubssystemFromCustomerAccountResponse result = nis.CanBeDeLinkedCustomerAccountFromSubsystem(oneaccountId, subsystemId, subsystemAccount);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// TODO: For testing only
        /// </summary>
        /// <returns></returns>
        [HttpOptions]
        public HttpResponseMessage DeLinkSubsystemFromCustomeraccount()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Delink subsystem from one account
        /// </summary>
        /// <param name="oneaccountId">one account id</param>
        /// <param name="subsystemId">subssystem id</param>
        /// <param name="subsystemAccount">account ref</param>
        /// <param name="customerId">customer id</param>
        /// <returns>Result as JSON</returns>
        [HttpPost]
        public ActionResult DeLinkSubsystemFromCustomeraccount(string oneaccountId, string subsystemId, string subsystemAccount, string customerId, Guid? userId)
        {
            using (var connMgr = new CrmConnectionManager(userId))
            {
                if (string.IsNullOrEmpty(oneaccountId) && !string.IsNullOrEmpty(customerId))
                {
                    AccountLogic accountLogic = new AccountLogic(connMgr.Service);
                    oneaccountId = accountLogic.GetAccountOneAccountId(customerId);
                }
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                WSDelinkSubssystemFromCustomerAccountResponse result =
                    nis.DeLinkSubssystemFromCustomerAccount(oneaccountId, subsystemId, subsystemAccount);
                if (result.hdr.result == RestConstants.SUCCESSFUL)
                {
                    // Call the Create MSD association for rolling-up transit account activities passing an empty oneaccountid which will disassociate the customer with the transit account
                    var logic = new TransitAccountLogic(connMgr.Service);
                    logic.AssociateWithCustomer(subsystemAccount, subsystemId, "");
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        // http://localhost:12665/OneAccount/GetBalanceHistory?oneAccountId=2B9634F0-3F29-E711-80EA-005056813E0D
        /// <summary>
        /// Get the balance history of an one account. This API supports pagination so that it always returns the data of a given page. 
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// NIS API call: 2.10.2 oneaccount/<oneaccount-id>/balancehistory GET
        /// </summary>
        /// <param name="oneAccountId">(Required) Unique identifier for the OneAccount account. </param>
        /// <param name="startDateTime">(Conditionally Required)  Starting date and time for the period the transactions are required for. Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally Required)  Ending date and time for the period the transactions are required for. Required when startDateTime is given.</param>
        /// <param name="viewType">(Optional) This is to control filter of the journal entries that are details related auth, confirm, auto-reversal:
        /// Supported types are: Simple, Complete</param>
        /// <param name="purseId">(Optional) The unique identifier of the purse to get transactions for.</param>
        /// <param name="financialTxnType">(Optional) Financial Transaction type related journal entries to fetch.  If not specified all types are returned.
        /// Supported types are: PurseLoad, PurseLoadReversal, Charge(includes Charge, ChargeAuth, ChargeConfirm, ChargeAutoReversal), ChargeReversal, PurseAdjustment</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �transactionAmount.asc,transactionDateTime.desc� indicates sort by transactionAmount 
        /// ascending and trasactionDateTime descending. Default is descending by journal entry id.</param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10. Value should be between 1 and 9999, inclusive.</param>
        /// <returns>Once Account transctions as JSON</returns>
        [HttpGet]
        public ActionResult GetBalanceHistory(string oneAccountId, string startDateTime, string endDateTime, string viewType,
                                              string purseId, string financialTxnType, string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetOneAccountBalanceHistory(oneAccountId, startDateTime, endDateTime, viewType,
                                                          purseId, financialTxnType, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: TESTING ONLY
        [HttpOptions]
        public HttpResponseMessage GetBalanceHistory(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpOptions]
        public HttpResponseMessage GetBalanceHistoryDetail(string foo)
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }
        /// <summary>
        /// Get the balance history details of an account based on a  journal entry ID. 
        /// NIS API call: 2.10.3 oneaccount/<oneaccount-id>/journalentry/<journalentry-id>/balancehistorydetail GET
        /// </summary>
        /// <param name="oneAccountId">One account id</param>
        /// <param name="journalEntryId">Journal entry id</param>
        /// <returns>Balance history details as JSON</returns>
        [HttpGet]
        public ActionResult GetBalanceHistoryDetail(string oneAccountId, string journalEntryId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetOneAccountBalanceHistoryDetail(oneAccountId, journalEntryId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetTravelHistory()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get the travel history for an account. This API supports pagination so that it always returns the data of a given page.
        /// It accepts sorting criteria so that the results are sorted by given sort field and order.
        /// NIS API call: 2.10.4 oneaccount/<oneaccount-id>/travelhistory GET
        /// </summary>
        /// <param name="oneAccountId">(Required) Unique identifier for the OneAccount account. </param>
        /// <param name="startDateTime">(Conditionally-Required) Starting date and time for the period the transactions are required for, defaults to 7 days ago.  Required when endDateTime is given.</param>
        /// <param name="endDateTime">(Conditionally-Required) Ending date and time for the period the transactions are required for, defaults to now.  Required when startDateTime is given.</param>
        /// <param name="authorityId">(Optional) Comma separated list of the authority IDs to get the travel history of, defaults to all authorities.</param>
        /// <param name="travelMode">(Optional) Comma separated list of the travel mode IDs to return the history of, defaults to all travel modes. See section 2.6.9 for allowable values.</param>
        /// <param name="correctable">(Optional) Flag to indicate if only correctable trips should be returned or not.</param>
        /// <param name="sortBy">(Optional) The comma separated list of fields with sort order e.g: �location.asc,authority.desc� indicates sort by location ascending and authority descending. Default is descending by startDateTime.</param>
        /// <param name="offset">(Optional) The index of the first  record in the page, starts with 0 and defaults to 0.</param>
        /// <param name="limit">(Optional) The number of records per page, defaults to 10.</param>
        /// <returns>One Account Travel History as JSON</returns>
        [HttpGet]
        public ActionResult GetTravelHistory(string oneAccountId, string startDateTime, string endDateTime,
                                             string authorityId, string travelMode, string tripCategory,
                                             string viewType, string sortBy, int? offset, int? limit)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetOneAccountTravelHistory(oneAccountId, startDateTime, endDateTime, authorityId, travelMode,
                                                         tripCategory, viewType, sortBy, offset, limit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetTravelHistoryDetail()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        /// <summary>
        /// Get the travel history detail information for an account by transaction ID.
        /// NIS API call: 2.10.5 oneaccount/<oneaccount-id>/transaction/<transaction-id>/travelhistorydetail GET
        /// </summary>
        /// <param name="oneAccountId">One account id</param>
        /// <param name="tripId">Transaction ID</param>
        /// <returns>One account travel history detail as JSON</returns>
        [HttpGet]
        public ActionResult GetTravelHistoryDetail(string oneAccountId, string tripId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                NISApiLogic nis = new NISApiLogic(connMgr.Service);
                var result = nis.GetOneAccountTravelHistoryDetail(oneAccountId, tripId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetAddressForDropdownList()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetAddressForDropdownList(Guid accountId)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                AccountLogic logic = new AccountLogic(connMgr.Service);
                var result = logic.GetAddressForDropdownList(accountId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //TODO: Testing only
        [HttpOptions]
        public HttpResponseMessage GetStates()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }

        [HttpGet]
        public ActionResult GetStates(string countryAlpha2 = null)
        {
            using (var connMgr = new CrmConnectionManager())
            {
                var states = GlobalsCache.GetStatesListForUI(connMgr.Service, countryAlpha2);
                return Json(states, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
