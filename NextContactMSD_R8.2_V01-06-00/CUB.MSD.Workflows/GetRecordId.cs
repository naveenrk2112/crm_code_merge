/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace CUB.MSD.Workflows
{
    public class GetRecordId : CodeActivity
    {
        #region Output Parameters
        [Output("Record Id")]
        public OutArgument<string> RecordId { get; set; }
        #endregion

        //in the future the CodeActivityContext will need to be mocked so that the context.GetExtension will be testable
        protected override void Execute(CodeActivityContext context)
        {
            // Create the tracing service
            ITracingService tracingService = context.GetExtension<ITracingService>();

            //Create the IWorkflowContext and the
            //IOrganizationService for communication with CRM
            IWorkflowContext workflowContext = context.GetExtension<IWorkflowContext>(); //mock this up to be serialized

            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();

            IOrganizationService service = serviceFactory.CreateOrganizationService(workflowContext.UserId);

            RecordId.Set(context, workflowContext.PrimaryEntityId.ToString().ToUpper());
        }
    }
}
