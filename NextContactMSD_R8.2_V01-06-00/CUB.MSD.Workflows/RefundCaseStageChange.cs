/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using CUB.MSD.Model;
using Microsoft.Crm.Sdk.Messages;

namespace CUB.MSD.Workflows
{
    public class RefundCaseStageChange : CodeActivity
    {
        IOrganizationService _orgService;

        //Stages
        //Could make configurable if somehow the stage ids change on the BPF
        private readonly Guid STAGE_1_PROPOSE = new Guid("e5eabd36-f0c3-4ece-ad13-d81489e44369");
        private readonly Guid STAGE_2_APPROVAL = new Guid("37484eb5-1056-43ce-97d4-10d00e9efeef");
        private readonly Guid STAGE_3_RESOLVE = new Guid("fbbd4be8-f9cb-4a22-8360-6d2a838c177e");

        #region Input Parameters

        [Input("Tier 1 Queue and Approval")]
        [ReferenceTarget(Queue.EntityLogicalName)]
        public InArgument<EntityReference> Tier1Queue { get; set; }

        [Input("Tier 2 Queue and Approval")]
        [ReferenceTarget(Queue.EntityLogicalName)]
        public InArgument<EntityReference> Tier2Queue { get; set; }

        [Input("Amount too high")]
        public InArgument<bool> AmountTooHigh { get; set; }

        [Input("Max Amount")]
        public InArgument<Money> MaxAmount { get; set; }
        #endregion

        #region Output Parameters

        #endregion

        protected override void Execute(CodeActivityContext context)
        {
            ITracingService tracingService = context.GetExtension<ITracingService>();
            IWorkflowContext workflowContext = context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
            _orgService = serviceFactory.CreateOrganizationService(workflowContext.UserId);
            var _systemOrgService = serviceFactory.CreateOrganizationService(null);

            var preImage = workflowContext.PreEntityImages.Contains("PreBusinessEntity")
                ? workflowContext.PreEntityImages["PreBusinessEntity"].ToEntity<Incident>()
                : null;

            if(preImage == null)
            {
                throw new InvalidPluginExecutionException("Preimage is unavailable")
;           }

            if(AmountTooHigh.Get(context) == true)
            {
                var err = "The refund amount entered is too high.\r\n";
                if(MaxAmount.Get(context) != null)
                {
                    err += String.Format("Max amount: ${0}\r\n", MaxAmount.Get(context).Value.ToString());

                }
                throw new InvalidPluginExecutionException(err);
            }

            var fromStage = preImage.StageId;

            var toStage = ((Entity)workflowContext.InputParameters["Target"]).Contains("stageid")
                ? ((Entity)workflowContext.InputParameters["Target"])["stageid"] as Guid?
                : null;

            if (fromStage == null || toStage == null)
            {
                return;
            }

            var caseRecord = ((Entity)workflowContext.InputParameters["Target"]).ToEntity<Incident>();

            if (fromStage == STAGE_1_PROPOSE && toStage == STAGE_2_APPROVAL)
            {
                if (Tier2Queue.Get(context) != null)
                {
                    AddToQueue(_systemOrgService, caseRecord.Id, Tier2Queue.Get(context).Id);
                }
                else if (Tier1Queue.Get(context) != null)
                {
                    AddToQueue(_systemOrgService, caseRecord.Id, Tier1Queue.Get(context).Id);
                }

                if (caseRecord.cub_CaseOwnerApproval == null || caseRecord.cub_CaseOwnerApproval.Id != workflowContext.UserId)
                {
                    caseRecord.cub_CaseOwnerApproval = new EntityReference(SystemUser.EntityLogicalName, workflowContext.UserId);
                }
            }
            else if (fromStage == STAGE_2_APPROVAL && toStage != STAGE_1_PROPOSE) //From Stage 2 to any stage after
            {
                var tier1Needed = preImage.cub_Team1Approval == null && Tier1Queue.Get(context) != null;
                var tier2Needed = preImage.cub_Team2Approval == null && Tier2Queue.Get(context) != null;

                if(caseRecord.cub_CaseOwnerApproval == null || caseRecord.cub_CaseOwnerApproval.Id != workflowContext.UserId)
                {
                    caseRecord.cub_CaseOwnerApproval = new EntityReference(SystemUser.EntityLogicalName, workflowContext.UserId);
                }

                CheckApprovalsError(tier1Needed, tier2Needed);
            }
        }

        private void CheckApprovalsError(bool tier1, bool tier2)
        {
            if(!tier1 && !tier2)
            {
                return;
            }

            var error = "You cannot continue the refund process without the following approvals:\r\n";

            if (tier1 == true) error += "Tier 1\r\n";
            if (tier2 == true) error += "Tier 2\r\n";

            throw new InvalidPluginExecutionException(error);
        }

        private void AddToQueue(IOrganizationService systemOrgService, Guid caseId, Guid queue)
        {
            var addtoQueueRequest = new AddToQueueRequest
            {
                Target = new EntityReference(Incident.EntityLogicalName, caseId),
                DestinationQueueId = queue
            };

            systemOrgService.Execute(addtoQueueRequest);
        }
    }
}
