/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace CUB.MSD.Workflows
{
    public class CalculateAverage : CodeActivity
    {
        #region Input Parameters
        [Input("Amount 1 Integer")]
        public InArgument<int> Amount1Int { get; set; }

        [Input("Amount 2 Integer")]
        public InArgument<int> Amount2Int { get; set; }

        [Input("Amount 3 Integer")]
        public InArgument<int> Amount3Int { get; set; }

        [Input("Amount 1 Decimal")]
        public InArgument<decimal> Amount1Dec { get; set; }

        [Input("Amount 2 Decimal")]
        public InArgument<decimal> Amount2Dec { get; set; }

        [Input("Amount 3 Decimal")]
        public InArgument<decimal> Amount3Dec { get; set; }
        #endregion

        #region Output Parameters
        [Output("Average Amount Integer")]
        public OutArgument<int> AverageAmtInt { get; set; }

        [Output("Average Amount Decimal")]
        public OutArgument<decimal> AverageAmtDec { get; set; }
        #endregion

        protected override void Execute(CodeActivityContext context)
        {
            CalcAverage(context);
        }

        private void CalcAverage(CodeActivityContext context)
        {
            int totalInt = 0;
            decimal totalDec = 0;
            int count = 0;

            GetSum(Amount1Int.Get(context), Amount1Dec.Get(context), ref totalInt, ref totalDec, ref count);
            GetSum(Amount2Int.Get(context), Amount2Dec.Get(context), ref totalInt, ref totalDec, ref count);
            GetSum(Amount3Int.Get(context), Amount3Dec.Get(context), ref totalInt, ref totalDec, ref count);

            if (count <= 0) return;

            if (totalInt > 0)
            {
                AverageAmtInt.Set(context, totalInt / count);
            }
            else if (totalDec > 0)
            {
                AverageAmtDec.Set(context, totalDec / count);
            }
        }

        private void GetSum(int intVal, decimal decimalVal, ref int totalInt, ref decimal totalDecimal, ref int count)
        {
            if (intVal > 0)
            {
                totalInt += intVal;
                count += 1;
            }
            else if (decimalVal > 0)
            {
                totalDecimal += decimalVal;
                count += 1;
            }
        }
    }
}
