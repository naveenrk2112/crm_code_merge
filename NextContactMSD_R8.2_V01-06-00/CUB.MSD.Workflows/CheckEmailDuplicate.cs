/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using CUB.MSD.Logic;

namespace CUB.MSD.Workflows
{
    public class CheckEmailDuplicate : CodeActivity
    {
        #region Input Parameters
        [RequiredArgument]
        [ReferenceTarget("contact")]
        [Input("Email Address")]
        public InArgument<string> EmailAddress { get; set; }
        #endregion

        #region Output Parameters
        [Output("Contact Id")]
        public OutArgument<string> ContactId { get; set; }
        #endregion

        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                //  ITracingService tracingService = context.GetExtension<ITracingService>();
                IWorkflowContext workflowContext = context.GetExtension<IWorkflowContext>(); //mock this up to be serialized
                IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
                IOrganizationService service = serviceFactory.CreateOrganizationService(workflowContext.UserId);


                string email = EmailAddress.Get(context);
                EmailLogic emailLogic = new EmailLogic(service);
                string ret = emailLogic.ValidateEmailAddressNotInUseOnOtherContact(email, "00000000-0000-0000-0000-000000000000");
                ContactId.Set(context, ret);
            }

            catch (Exception ex)
            {
                ContactId.Set(context, ex.Message);
            }
        }
    }
}
