/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using CUB.MSD.Model;
using Microsoft.Crm.Sdk.Messages;

namespace CUB.MSD.Workflows
{
    public class GetMaxRefundAmounts : CodeActivity
    {
        IOrganizationService _orgService;

        //Queues
        private readonly Guid TIER_1_QUEUE = new Guid("B7C3E063-78BA-E711-80F8-005056810D65");
        private readonly Guid TIER_2_QUEUE = new Guid("BE257CAF-ECBC-E711-80F8-005056810D65");

        #region Input Parameters

        [Input("Request Tier 1")]
        [RequiredArgument]
        public InArgument<bool> RequestTier1 { get; set; }

        [Input("Request Tier 2")]
        [RequiredArgument]
        public InArgument<bool> RequestTier2 { get; set; }
        #endregion

        #region Output Parameters
        [Output("Tier 1 max amount")]
        public OutArgument<Money> Tier1MaxAmount { get; set; }
        [Output("Tier 2 max amount")]
        public OutArgument<Money> Tier2MaxAmount { get; set; }
        #endregion

        protected override void Execute(CodeActivityContext context)
        {
            ITracingService tracingService = context.GetExtension<ITracingService>();
            IWorkflowContext workflowContext = context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
            _orgService = serviceFactory.CreateOrganizationService(workflowContext.UserId);

            if(RequestTier1.Get(context) == true)
            {
                //Instead of below code, could make a call to CRM, another service, etc, etc before setting/returning.
                Tier1MaxAmount.Set(context, new Money(10));
            }
            else
            {
                Tier1MaxAmount.Set(context, new Money(0));
            }

            if (RequestTier2.Get(context) == true)
            {
                //Instead of below code, could make a call to CRM, another service, etc, etc before setting/returning.
                Tier2MaxAmount.Set(context, new Money(100));
            }
            else
            {
                Tier2MaxAmount.Set(context, new Money(0));
            }
        }
    }
}
