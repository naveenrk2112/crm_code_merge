/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Activities;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace CUB.MSD.Workflows
{
    class WorkflowHelpers
    {
        
        public static IOrganizationService GetService(CodeActivityContext context)
        {
            IWorkflowContext workflowContext = context.GetExtension<IWorkflowContext>();

     

            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();


            IOrganizationService service = serviceFactory.CreateOrganizationService(workflowContext.UserId);

            return service;

        }

        public static IWorkflowContext GetWorkflowContext(CodeActivityContext context)
        {
           
            IWorkflowContext workflowContext = context.GetExtension<IWorkflowContext>();

            return workflowContext;

        }

        //untested
        public static ITracingService GetTracingService(IServiceProvider serviceProvider)
        {
            return (ITracingService)serviceProvider.GetService(typeof(ITracingService));
        }

        //public static void LogEvent(string orgName, string message, LogLevel logLevel, Exception exception = null)
        //{
        //    try
        //    {
        //        // if the org isn't set up to log at this level, get out
        //        if (!Logger.DoLog(orgName, logLevel))
        //            return;

        //        MethodBase method = null;
        //        try
        //        {
        //            var stacktrace = new StackTrace();
        //            method = stacktrace.GetFrame(1).GetMethod();
        //        }
        //        catch (Exception)
        //        {
        //            method = null;
        //        }

        //        try
        //        {
        //            Logger.Log(logLevel, message, orgName, exception, method, "workflow");
        //        }
        //        catch (Exception e)
        //        {
        //            Trace.WriteLine(e.ToString());
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}
    }
}
