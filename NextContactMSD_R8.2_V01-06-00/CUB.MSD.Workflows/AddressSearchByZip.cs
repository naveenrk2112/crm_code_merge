/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using CUB.MSD.Logic;
using CUB.MSD.Model;

namespace CUB.MSD.Workflows
{
    public class AddressSearchByZip : CodeActivity
    {
        #region Input Parameters
        [RequiredArgument]
        [ReferenceTarget("cub_ZipOrPostalCode")]
        [Input("Zip Code ID")]
        public InArgument<EntityReference> ZipCodeId { get; set; }
        #endregion

        #region Output Parameters
        [Output("City")]
        public OutArgument<string> City { get; set; }
        [Output("Country")]
        public OutArgument<string> Country { get; set; }
        [Output("State")]
        public OutArgument<string> State { get; set; }
        #endregion

        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                ITracingService tracingService = context.GetExtension<ITracingService>();
                IWorkflowContext workflowContext = context.GetExtension<IWorkflowContext>(); //mock this up to be serialized
                IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
                IOrganizationService service = serviceFactory.CreateOrganizationService(workflowContext.UserId);

                ZipCodeLogic zipLogic = new ZipCodeLogic(service);
                cub_ZipOrPostalCode zipEntity = zipLogic.GetById(ZipCodeId.Get(context).Id);
                City.Set(context, zipEntity.cub_City);
                Country.Set(context, zipEntity.cub_Country.Name.ToString());
                State.Set(context, zipEntity.cub_State.Name.ToString());
            }

            catch (Exception ex)
            {
                City.Set(context, ex.Message);  // use city or add an error output parm?
            }
        }
    }
}
