rem FOR 2016 WORKFLOW MERGE/BUILD
rem This file needs to run the CUB.MSD.Workflows folder

..\SolutionFiles\ILMerge /t:libraries /targetplatform:"v4,C:\Windows\Microsoft.NET\Framework64\v4.0.30319" /copyattrs /keyfile:"workflowsKey.snk" /out:"bin\debug\CUB.MSD.Workflows.dll" "bin\debug\CUB.MSD.Workflows99.dll" "bin\debug\CUB.MSD.Logic.dll" "bin\debug\$(Configuration)\CUB.MSD.Model.dll" "bin\debug\CUB.MSD.Logging.dll" "bin\debug\log4net.dll"
xcopy /E/Y/D/R "bin\debug\CUB.MSD.Workflows.dll" "..\CrmPackage\bin\debug\CUB.MSD.Workflows.dll"

rem ..\SolutionFiles\ILMerge /t:libraries /targetplatform:"v4,C:\Windows\Microsoft.NET\Framework64\v4.0.30319" /copyattrs /keyfile:"pluginsKey.snk" /out:"bin\debug\CUB.MSD.Plugins.dll" "bin\debug\CUB.MSD.Plugins99.dll" "bin\debug\Newtonsoft.Json.dll" "bin\debug\CUB.MSD.Logic.dll" "bin\debug\CUB.MSD.Model.dll" 
rem xcopy /E/Y/D/R "bin\debug\CUB.MSD.Plugins.dll" "..\CrmPackage\bin\debug\CUB.MSD.Plugins.dll"

pause