﻿using CUB.MSD.Core;
using CUB.MSD.Logic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CubLoggerStressTest
{
    public class Program
    {
        private bool Running { set; get; }
        private readonly int THREAD_COUNT;
        private readonly int EXECUTION_COUNT;
        private readonly int SLEEP_TIME_IN_MILISECONDS;

        public static void Main(string[] args)
        {
            Program program = new Program();
            program.Start();
        }

        public Program()
        {
            THREAD_COUNT = int.Parse(ConfigurationManager.AppSettings["THREAD_COUNT"]);
            EXECUTION_COUNT = int.Parse(ConfigurationManager.AppSettings["EXECUTION_COUNT"]);
            SLEEP_TIME_IN_MILISECONDS = int.Parse(ConfigurationManager.AppSettings["SLEEP_TIME_IN_MILISECONDS"]);
        }

        public void Start()
        {
            var watch = Stopwatch.StartNew();
            Console.WriteLine("CubLogger Stress Test Started!");
            List<Task> list = new List<Task>();
            for (int i = 0; i < THREAD_COUNT; i++)
            {
                Task task = new Task(() => LoggingTest());
                list.Add(task);
                task.Start();
            }
            Console.WriteLine("{0} Threads started", THREAD_COUNT);
            Console.WriteLine("{0} Executions", EXECUTION_COUNT);
            Task.WaitAll(list.ToArray());
            watch.Stop();
            Console.WriteLine("\nCubLogger Stress Test Finished!");
            Console.WriteLine("Execution time = {0} seconds", watch.ElapsedMilliseconds / 1000);
        }

        private void LoggingTest()
        {
            try
            {
                using (var ccm = new CrmConnectionManager())
                {
                    for (int i = 0; i < EXECUTION_COUNT; i++)
                    {
                        string jsonData = "{{\"result\":\"Successful\",\"uid\":\"{0}\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}}";
                        jsonData = string.Format(jsonData, Guid.NewGuid().ToString());
                        Console.Write(".");
                        CubLogger.Init(ccm.Service, true);
                        CubLogger.Info("Y1 - Info Log test");
                        CubLogger.Info("Y2 - Logging with data", jsonData);
                        CubLogger.InfoFormat("Y3 - Thread ID = {0}", Thread.CurrentThread.ManagedThreadId);
                        try
                        {
                            throw new Exception("Logging test exception");
                        }
                        catch (Exception ex)
                        {
                            CubLogger.Error("4 - Logging exception", ex, jsonData);
                        }
                        //Thread.Sleep(SLEEP_TIME_IN_MILISECONDS);
                    }
                }
            } catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }
    }
}
