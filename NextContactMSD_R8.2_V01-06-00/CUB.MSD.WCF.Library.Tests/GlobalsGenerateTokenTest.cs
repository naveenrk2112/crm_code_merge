/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGenerateTokenTest
    {
        [TestMethod]
        public void GenerateTokenSuccessTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                Account acc = new Account();
                acc.AccountId = Guid.NewGuid();
                acc.CustomerTypeCode = (object)"Individual";
                acc.cub_AccountTypeId = new EntityReference();
                acc.cub_AccountTypeId.Id = Guid.NewGuid();
                acc.StatusCode = new OptionSetValue((int)account_statuscode.Active);

                Contact c = new Contact();
                c.ContactId = Guid.NewGuid();
                c.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };
                c.StatusCode = new OptionSetValue((int)contact_statuscode.Active);

                cub_Credential cc = new cub_Credential();
                cc.cub_CredentialId = Guid.NewGuid();
                cc.cub_Username = "myUsername";
                cc.cub_ContactId = new EntityReference { LogicalName = c.LogicalName, Id = c.ContactId.Value };
                cc.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);
                cc.cub_Password = "myPa$sw0rd";

                List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
                data.AddRange(globalpwddata);

                data.Add(acc);
                data.Add(c);
                data.Add(cc);

                fakedContext.Initialize(data);

                JsonModels.WSVerificationToken tokenData = new JsonModels.WSVerificationToken();
                tokenData.verificationType = "Password";

                string customerId = acc.AccountId.Value.ToString();
                string contactId = c.ContactId.Value.ToString();

                JsonModels.WSGenerateTokenResponse response = global.GenerateToken(tokenData, customerId, contactId);

                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.IsNull(exActMgrFault);
            }
        }

        [TestMethod]
        public void GenerateTokenInActiveAccountTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                Account acc = new Account();
                acc.AccountId = Guid.NewGuid();
                acc.CustomerTypeCode = (object)"Individual";
                acc.cub_AccountTypeId = new EntityReference();
                acc.cub_AccountTypeId.Id = Guid.NewGuid();
                acc.StatusCode = new OptionSetValue((int)account_statuscode.Active);

                Contact c = new Contact();
                c.ContactId = Guid.NewGuid();
                c.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };
                c.StatusCode = new OptionSetValue((int)contact_statuscode.Active);

                cub_Credential cc = new cub_Credential();
                cc.cub_CredentialId = Guid.NewGuid();
                cc.cub_Username = "myUsername";
                cc.cub_ContactId = new EntityReference { LogicalName = c.LogicalName, Id = c.ContactId.Value };
                cc.statuscode = new OptionSetValue((int)cub_credential_statuscode.Inactive);
                cc.cub_Password = "myPa$sw0rd";

                List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
                data.AddRange(globalpwddata);

                data.Add(acc);
                data.Add(c);
                data.Add(cc);

                fakedContext.Initialize(data);

                JsonModels.WSVerificationToken tokenData = new JsonModels.WSVerificationToken();
                tokenData.verificationType = "Password";

                string customerId = acc.AccountId.Value.ToString();
                string contactId = c.ContactId.Value.ToString();

                JsonModels.WSGenerateTokenResponse response = global.GenerateToken(tokenData, customerId, contactId);

                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                Assert.AreEqual(e.Detail.fieldName, "Account");
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_ACCNT_INACTIVE);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_LOGIN_INACTIVE);
            }
        }

        [TestMethod]
        public void GenerateTokenNullCustomerIdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSVerificationToken tokenData = new JsonModels.WSVerificationToken();
                tokenData.verificationType = "Password";

                string customerId = null;
                string contactId = null;

                JsonModels.WSGenerateTokenResponse response = global.GenerateToken(tokenData, customerId, contactId);

                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }

        [TestMethod]
        public void GenerateTokenInvalidCustomerIdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSVerificationToken tokenData = new JsonModels.WSVerificationToken();
                tokenData.verificationType = "Password";

                string customerId = "3831F0B3-B26F-4540-B4D2-012371803BFW";
                string contactId = null;

                JsonModels.WSGenerateTokenResponse response = global.GenerateToken(tokenData, customerId, contactId);

                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }

        [TestMethod]
        public void GenerateTokenNullContactIdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSVerificationToken tokenData = new JsonModels.WSVerificationToken();
                tokenData.verificationType = "Password";

                string customerId = "3831F0B3-B26F-4540-B4D2-012371803BF5";
                string contactId = null;

                JsonModels.WSGenerateTokenResponse response = global.GenerateToken(tokenData, customerId, contactId);

                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "ContactId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_CONTACT_NOT_FOUND);
            }
        }

        [TestMethod]
        public void GenerateTokenInvalidContactIdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSVerificationToken tokenData = new JsonModels.WSVerificationToken();
                tokenData.verificationType = "Password";

                string customerId = "3831F0B3-B26F-4540-B4D2-012371803BF5";
                string contactId = "3831F0B3-B26F-4540-B4D2-012371803BFW";

                JsonModels.WSGenerateTokenResponse response = global.GenerateToken(tokenData, customerId, contactId);

                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "ContactId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_CONTACT_NOT_FOUND);
            }
        }
        [TestMethod]
        public void GenerateTokenNullTokenDataTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSVerificationToken tokenData = null;

                string customerId = "3831F0B3-B26F-4540-B4D2-012371803BF5";
                string contactId = "3831F0B3-B26F-4540-B4D2-012371803BF4";

                JsonModels.WSGenerateTokenResponse response = global.GenerateToken(tokenData, customerId, contactId);

                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "Verification Token");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }
        }

        [TestMethod]
        public void GenerateTokenNullVerificationTypeTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSVerificationToken tokenData = new JsonModels.WSVerificationToken();
                tokenData.verificationType = null;

                string customerId = "3831F0B3-B26F-4540-B4D2-012371803BF5";
                string contactId = "3831F0B3-B26F-4540-B4D2-012371803BF4";

                JsonModels.WSGenerateTokenResponse response = global.GenerateToken(tokenData, customerId, contactId);

                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "Verification Token");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }
        }
    }
}
