/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsAuthenticateCallerTest
    {
        [TestMethod]
        public void AuthenticateCallerGoodTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            cub_ApiAuthorization ca = new cub_ApiAuthorization();
            ca.cub_ApiAuthorizationId = Guid.NewGuid();
            ca.cub_Department = "testDepartment";
            ca.cub_Username = "testUsername";
            ca.cub_Password = "testPassword";
            ca.statuscode = contact_statuscode.Active;
            string caller = ca.cub_Department;
            string username = ca.cub_Username;
            string password = ca.cub_Password;
            data.Add(ca);
            fakedContext.Initialize(data);
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            bool testAuth = global.AuthenticateCaller(caller, username, password);
            Assert.IsTrue(testAuth);
        }

        [TestMethod]
        public void AuthenticateCallerBadTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            cub_ApiAuthorization ca = new cub_ApiAuthorization();
            ca.cub_ApiAuthorizationId = Guid.NewGuid();
            ca.cub_Department = "testDepartment";
            ca.cub_Username = "testUsername";
            ca.cub_Password = "testPassword";
            ca.statuscode = contact_statuscode.Active;
            string caller = ca.cub_Department;
            string username = ca.cub_Username;
            string password = "testbadPassword";
            data.Add(ca);
            fakedContext.Initialize(data);
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            bool testAuth = global.AuthenticateCaller(caller, username, password);
            Assert.IsFalse(testAuth);
        }

        [TestMethod]
        public void AuthenticateCallerNullTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();
            cub_ApiAuthorization ca = new cub_ApiAuthorization();
            ca.cub_ApiAuthorizationId = Guid.NewGuid();
            ca.cub_Department = "testDepartment";
            ca.cub_Username = "testUsername";
            ca.cub_Password = "testPassword";
            ca.statuscode = contact_statuscode.Active;
            string caller = null;
            string username = null;
            string password = null;
            data.Add(ca);
            fakedContext.Initialize(data);
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            bool testAuth = global.AuthenticateCaller(caller, username, password);
            Assert.IsFalse(testAuth);
        }
    }
}
