/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class UtilityTests
    {
        [TestMethod]
        public void Base64EncryptedStringToStringNullTest()
        {
            string result = Utility.Base64EncryptedStringToString(null);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void Base64EncryptedStringToStringTest()
        {
            string startValue = "ThisIsMyString";
            byte[] ba = Utility.StringToByteArray(startValue);
            string encryptedValue = Convert.ToBase64String(ba);
            string result = Utility.Base64EncryptedStringToString(encryptedValue);
            Assert.AreEqual(result, startValue);
        }

        [TestMethod]
        public void ByteArrayToStringTest()
        {
            string startValue = "ThisIsMyString";
            byte[] ba = Utility.StringToByteArray(startValue);
            string result = Utility.ByteArrayToString(ba);
            Assert.AreEqual(result, startValue);
        }

        [TestMethod]
        public void StringToByteArrayNullTest()
        {
            string startValue = null;
            byte[] ba = Utility.StringToByteArray(startValue);
            Assert.IsNull(ba);
        }

        [TestMethod]
        public void StringToByteArrayTest()
        {
            string startValue = "ThisIsMyString";
            byte[] ba = Utility.StringToByteArray(startValue);
            string result = Utility.ByteArrayToString(ba);
            Assert.AreEqual(result, startValue);
        }

        [TestMethod]
        public void IsGUIDNullTest()
        {
            string startValue = null;
            bool IsGuid = Utility.IsGUID(startValue);
            Assert.IsFalse(IsGuid);
        }

        [TestMethod]
        public void IsGUIDTest()
        {
            string startValue = "7F5416B6-2750-401A-9322-92559D241A91";
            bool IsGuid = Utility.IsGUID(startValue);
            Assert.IsTrue(IsGuid);
        }

        //[TestMethod]
        //public void NulltoStringTest()
        //{
        //    string startValue = null;
        //    string retStr = Utility.NulltoString(ref startValue);
        //    Assert.IsNotNull(retStr);
        //}

        //[TestMethod]
        //public void UpperCaseCountTest()
        //{
        //    string startValue = "7F5416B6-2750-401A-9322-92559D241A91";
        //    int count = Utility.UpperCaseCount(startValue);
        //    Assert.AreEqual(count, 5);
        //}

        //[TestMethod]
        //public void LowerCaseCountTest()
        //{
        //    string startValue = "7F5416B6-2750-401a-9322-92559D241A91";
        //    int count = Utility.LowerCaseCount(startValue);
        //    Assert.AreEqual(count, 1);
        //}

        //[TestMethod]
        //public void NumericCountTest()
        //{
        //    string startValue = "7F5416B6-2750-401a-9322-92559D241A91";
        //    int count = Utility.NumericCount(startValue);
        //    Assert.AreEqual(count, 27);
        //}

        //[TestMethod]
        //public void NonAlphaCountTest()
        //{
        //    string startValue = "@7F5416B6-2750-401a-9322-92559$2!1A*1";
        //    int count = Utility.NonAlphaCount(startValue);
        //    Assert.AreEqual(count, 8);
        //}

        //[TestMethod]
        //public void ValidateUsernameNullTest()
        //{
        //    string startValue = null;
        //    CommonSettings.UsernameMaxLength = 0;
        //    CommonSettings.UsernameMinLength = 0;
        //    CommonSettings.UsernameRequireEmail = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidateUsername(startValue);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.general.value.toosmall");
        //    Assert.AreEqual(errorList[0].message, "Minimum length not met");
        //}

        //[TestMethod]
        //public void ValidateUsernameTooShortTest()
        //{
        //    CommonSettings.UsernameMaxLength = 0;
        //    CommonSettings.UsernameMinLength = 42;
        //    CommonSettings.UsernameRequireEmail = 0;
        //    string startValue = "@7F5416B6-2750-401a-9322-92559$2!1A*1";
        //    List<JsonModels.WSError> errorList = Utility.ValidateUsername(startValue);
        //    Assert.AreEqual(errorList.Count, 2);
        //    Assert.AreEqual(errorList[0].key, "errors.general.value.toosmall");
        //    Assert.AreEqual(errorList[0].message, "Minimum length not met");
        //}

        //[TestMethod]
        //public void ValidateUsernameTooLongTest()
        //{
        //    CommonSettings.UsernameMinLength = 2;
        //    CommonSettings.UsernameMaxLength = 10;
        //    CommonSettings.UsernameRequireEmail = 0;
        //    string startValue = "@7F5416B6-2750-401a-9322-92559$2!1A*1";
        //    List<JsonModels.WSError> errorList = Utility.ValidateUsername(startValue);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.general.value.toolong");
        //    Assert.AreEqual(errorList[0].message, "Maximum length exceeded");
        //}

        //[TestMethod]
        //public void ValidateUsernameIsEmailFailTest()
        //{
        //    CommonSettings.UsernameMinLength = 2;
        //    CommonSettings.UsernameMaxLength = 60;
        //    CommonSettings.UsernameRequireEmail = 1;
        //    string startValue = "@UserNameAtHome.com";
        //    List<JsonModels.WSError> errorList = Utility.ValidateUsername(startValue);
        //    Assert.AreEqual(errorList.Count, 2);
        //    Assert.AreEqual(errorList[0].key, "errors.general.email.cannot.start.with.special.characters");
        //    Assert.AreEqual(errorList[0].message, "Cannot start with special characters");
        //}

        //[TestMethod]
        //public void ValidateUsernameIsEmailFail2Test()
        //{
        //    CommonSettings.UsernameMinLength = 2;
        //    CommonSettings.UsernameMaxLength = 60;
        //    CommonSettings.UsernameRequireEmail = 1;
        //    string startValue = "UserNameAtHome.com";
        //    List<JsonModels.WSError> errorList = Utility.ValidateUsername(startValue);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.general.email.invalid.format");
        //    Assert.AreEqual(errorList[0].message, "Must be in a standard valid email format");
        //}

        //[TestMethod]
        //public void ValidateUsernameIsEmailGoodTest()
        //{
        //    CommonSettings.UsernameMinLength = 2;
        //    CommonSettings.UsernameMaxLength = 60;
        //    CommonSettings.UsernameRequireEmail = 1;
        //    string startValue = "UserName@Home.com";
        //    List<JsonModels.WSError> errorList = Utility.ValidateUsername(startValue);
        //    Assert.IsNull(errorList);
        //}

        //[TestMethod]
        //public void ValidatePasswordNullTest()
        //{
        //    string username = null;
        //    string password = null;
        //    CommonSettings.PasswordMinLength = 2;
        //    CommonSettings.PasswordMaxLength = 100;
        //    CommonSettings.PasswordNumeric = 0;
        //    CommonSettings.PasswordLowerAlpha = 0;
        //    CommonSettings.PasswordUpperAlpha = 0;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.general.value.toosmall");
        //    Assert.AreEqual(errorList[0].message, "Minimum length not met");
        //}

        //[TestMethod]
        //public void ValidatePasswordTooShortTest()
        //{
        //    string username = null;
        //    string password = "p";
        //    CommonSettings.PasswordMinLength = 2;
        //    CommonSettings.PasswordMaxLength = 100;
        //    CommonSettings.PasswordNumeric = 0;
        //    CommonSettings.PasswordLowerAlpha = 0;
        //    CommonSettings.PasswordUpperAlpha = 0;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.general.value.toosmall");
        //    Assert.AreEqual(errorList[0].message, "Minimum length not met");
        //}

        //[TestMethod]
        //public void ValidatePasswordTooLongTest()
        //{
        //    string username = null;
        //    string password = "paswordofthelongtype";
        //    CommonSettings.PasswordMinLength = 2;
        //    CommonSettings.PasswordMaxLength = 10;
        //    CommonSettings.PasswordNumeric = 0;
        //    CommonSettings.PasswordLowerAlpha = 0;
        //    CommonSettings.PasswordUpperAlpha = 0;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.general.value.toolong");
        //    Assert.AreEqual(errorList[0].message, "Maximum length exceeded");
        //}

        //[TestMethod]
        //public void ValidatePasswordContainsUsernameTest()
        //{
        //    string username = "username";
        //    string password = "usernamepasword";
        //    CommonSettings.PasswordMinLength = 6;
        //    CommonSettings.PasswordMaxLength = 50;
        //    CommonSettings.PasswordNumeric = 0;
        //    CommonSettings.PasswordLowerAlpha = 0;
        //    CommonSettings.PasswordUpperAlpha = 0;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.password.cannot.contain.username");
        //    Assert.AreEqual(errorList[0].message, "Password can not contain username in it");
        //}

        //[TestMethod]
        //public void ValidatePasswordHasConsecutiveCharsTest()
        //{
        //    string username = "username";
        //    string password = "password";
        //    CommonSettings.PasswordMinLength = 6;
        //    CommonSettings.PasswordMaxLength = 50;
        //    CommonSettings.PasswordNumeric = 0;
        //    CommonSettings.PasswordLowerAlpha = 0;
        //    CommonSettings.PasswordUpperAlpha = 0;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.password.cannot.contain.same.char.consecutively");
        //    Assert.AreEqual(errorList[0].message, "Password can not contain the same character consecutively");
        //}

        //[TestMethod]
        //public void ValidatePasswordHasContiguousCharsTest()
        //{
        //    string username = "username";
        //    string password = "pa$swordword";
        //    CommonSettings.PasswordMinLength = 6;
        //    CommonSettings.PasswordMaxLength = 50;
        //    CommonSettings.PasswordNumeric = 0;
        //    CommonSettings.PasswordLowerAlpha = 0;
        //    CommonSettings.PasswordUpperAlpha = 0;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.password.cannot.contain.contiguous.chars");
        //    Assert.AreEqual(errorList[0].message, "Password can not contain contiguous characters");
        //}

        //[TestMethod]
        //public void ValidatePasswordNumericCountTest()
        //{
        //    string username = "username";
        //    string password = "pa$sword";
        //    CommonSettings.PasswordMinLength = 6;
        //    CommonSettings.PasswordMaxLength = 50;
        //    CommonSettings.PasswordNumeric = 2;
        //    CommonSettings.PasswordLowerAlpha = 0;
        //    CommonSettings.PasswordUpperAlpha = 0;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.password.must.contain.atleast.digits");
        //    Assert.AreEqual(errorList[0].message, "Password must contain digit(s)");
        //}

        //[TestMethod]
        //public void ValidatePasswordLowerAlphaCountOnlyTest()
        //{
        //    string username = "username";
        //    string password = "pa$sword";
        //    CommonSettings.PasswordMinLength = 6;
        //    CommonSettings.PasswordMaxLength = 50;
        //    CommonSettings.PasswordNumeric = 0;
        //    CommonSettings.PasswordLowerAlpha = 10;
        //    CommonSettings.PasswordUpperAlpha = 0;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 2);
        //    Assert.AreEqual(errorList[0].key, "errors.password.must.contain.mixed.case.chars");
        //    Assert.AreEqual(errorList[0].message, "Password must contain mixed case letters");
        //}

        //[TestMethod]
        //public void ValidatePasswordAlphaCountMixedTest()
        //{
        //    string username = "username";
        //    string password = "pa$sword";
        //    CommonSettings.PasswordMinLength = 6;
        //    CommonSettings.PasswordMaxLength = 50;
        //    CommonSettings.PasswordLowerAlpha = 4;
        //    CommonSettings.PasswordUpperAlpha = 5;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 2);
        //    Assert.AreEqual(errorList[0].key, "errors.password.must.contain.mixed.case.chars");
        //    Assert.AreEqual(errorList[0].message, "Password must contain mixed case letters");
        //}

        //[TestMethod]
        //public void ValidatePasswordUpperAlphaCountOnlyTest()
        //{
        //    string username = "username";
        //    string password = "pa$sword";
        //    CommonSettings.PasswordMinLength = 6;
        //    CommonSettings.PasswordMaxLength = 50;
        //    CommonSettings.PasswordNumeric = 0;
        //    CommonSettings.PasswordLowerAlpha = 0;
        //    CommonSettings.PasswordUpperAlpha = 10;
        //    CommonSettings.PasswordNonAlphanumeric = 0;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 2);
        //    Assert.AreEqual(errorList[0].key, "errors.password.must.contain.mixed.case.chars");
        //    Assert.AreEqual(errorList[0].message, "Password must contain mixed case letters");
        //}

        //[TestMethod]
        //public void ValidatePasswordNonAlphaCountOnlyTest()
        //{
        //    string username = "username";
        //    string password = "pa$sword";
        //    CommonSettings.PasswordMinLength = 6;
        //    CommonSettings.PasswordMaxLength = 50;
        //    CommonSettings.PasswordNumeric = 0;
        //    CommonSettings.PasswordLowerAlpha = 0;
        //    CommonSettings.PasswordUpperAlpha = 0;
        //    CommonSettings.PasswordNonAlphanumeric = 10;
        //    List<JsonModels.WSError> errorList = Utility.ValidatePassword(password, username);
        //    Assert.AreEqual(errorList.Count, 1);
        //    Assert.AreEqual(errorList[0].key, "errors.password.must.contain.at.least.special.chars");
        //    Assert.AreEqual(errorList[0].message, "Password must contain special character(s)");
        //}
    }
}
