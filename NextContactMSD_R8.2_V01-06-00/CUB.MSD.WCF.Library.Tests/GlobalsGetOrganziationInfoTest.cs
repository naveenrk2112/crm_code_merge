/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGetOrganziationInfoTest
    {
        [TestMethod]
        public void GetOrganziationInfoTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals globals = new Globals(ci);
            JsonModels.WSMSDOrganizationInfo info = globals.GetOrganziationInfo();
            Assert.IsNotNull(info.OrganizationName);
            Assert.IsNotNull(info.OrganzationId);
        }
    }
}
