/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsCustomerAddressAddTest
    {
        [TestMethod]
        public void CustomerAddressAddCustomerNullTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = null;
                Model.MSDApi.WSAddress addressAddData = null;

                global.CustomerAddressAdd(customerId, addressAddData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "CustomerId is null");
            }
        }

        [TestMethod]
        public void CustomerAddressAddCustomerIsBadValueTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDZ";
                Model.MSDApi.WSAddress addressAddData = null;

                global.CustomerAddressAdd(customerId, addressAddData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "Customer not found");
            }
        }

        [TestMethod]
        public void CustomerAddressAddressIsNullTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                Model.MSDApi.WSAddress addressAddData = null;

                global.CustomerAddressAdd(customerId, addressAddData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "Address");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }
        }

        [TestMethod]
        public void CustomerAddressAddServiceIsNullTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                Model.MSDApi.WSAddress addressAddData = new Model.MSDApi.WSAddress();

                global.CustomerAddressAdd(customerId, addressAddData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "Country");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_COUNTRY_REQUIRED);
            }
        }
    }
}
