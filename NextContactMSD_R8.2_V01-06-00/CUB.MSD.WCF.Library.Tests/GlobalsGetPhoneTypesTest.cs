/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using System;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGetPhoneTypeTest
    {
        [TestMethod]
        public void GetPhoneTypesTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            fakedContext.Initialize(PrepareData());
            var fakedService = fakedContext.GetOrganizationService();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            var phoneTypes = global.GetClientPhoneTypes();
            foreach(var item in phoneTypes.phoneTypes)
            {
                // The phone type key is the first letter of the description
                Assert.AreEqual(item.key, item.value.Substring(0,1));
            }
        }

        private static List<Entity> PrepareData()
        {
            List<Entity> data = new List<Entity>();
            Guid translationID = Guid.NewGuid();
            data.Add(new cub_OptionSetTranslation { cub_OptionSetTranslationId = translationID, cub_OptionSetName = "cub_phonetype" });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "H",
                cub_OptionSetValue = 971880000
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "M",
                cub_OptionSetValue = 971880001
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "F",
                cub_OptionSetValue = 971880002
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "W",
                cub_OptionSetValue = 971880003
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "D",
                cub_OptionSetValue = 971880004
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "E",
                cub_OptionSetValue = 971880005
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "O",
                cub_OptionSetValue = 971880006
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "P",
                cub_OptionSetValue = 971880007
            });
            return data;
        }
    }
}
