﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using CUB.MSD.WCF;
using Microsoft.QualityTools.Testing.Fakes;
using System.ServiceModel.Web.Fakes;
using System.Web.Fakes;
using System.Net;
using System.Web;
using System.Collections.Specialized;
using Microsoft.Xrm.Sdk.Fakes;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGetClientCustomerSettingsTest
    {
        [TestMethod]
        public void GetClientCustomerSettingsHappyPathTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            GlobalsGetClientCustomerSettingsTest.PrepareData(data, false);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            JsonModels.WSClientCustomerSettings response = global.GetClientCustomerSettings();
            Assert.IsNotNull(response.customerSettings);
        }

        [TestMethod]
        public void GetClientCustomerSettingsTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            GlobalsGetClientCustomerSettingsTest.PrepareData(data, false);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            JsonModels.WSClientCustomerSettings response = global.GetClientCustomerSettings();
            Assert.AreEqual(response.customerSettings.Count, 2);
        }

        public static void PrepareData(List<Entity> data, bool noSubject)
        {
            /*
             * Globals
             */
            EntityReference UsernameValidationGlobalID = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());
            data.Add(new cub_Globals
            {
                cub_GlobalsId = UsernameValidationGlobalID.Id,
                cub_Name = "UsernameValidation",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = UsernameValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UsernameMaxLength",
                cub_AttributeValue = "100",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = UsernameValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UsernameMinLength",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = UsernameValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UsernameRequireEmail",
                cub_AttributeValue = "false",
                cub_Enabled = true
            });
            EntityReference PasswordValidationGlobalID = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());
            data.Add(new cub_Globals
            {
                cub_GlobalsId = PasswordValidationGlobalID.Id,
                cub_Name = "PasswordValidation",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordAllowConsecutiveChars",
                cub_AttributeValue = "false",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordAllowContiguousChars",
                cub_AttributeValue = "false",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordAllowdictionaryWords",
                cub_AttributeValue = "true",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordAllowUsername",
                cub_AttributeValue = "true",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordLowerAlpha",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordMaxLength",
                cub_AttributeValue = "64",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordMinLength",
                cub_AttributeValue = "8",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordNonAlpha",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordNumeric",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = PasswordValidationGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordUpperAlpha",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });

            /*
             * Customer Type
             */
            data.Add(new cub_AccountType
            {
                cub_AccountTypeId = new Guid("2AFFC4CA-787E-424F-BB2D-2C2DFD81EB22"),
                statuscode = cub_accounttype_statuscode.Active,
                cub_AccountName = "Traveler"
            });

            data.Add(new cub_AccountType
            {
                cub_AccountTypeId = new Guid("7BC5D5B0-C5A0-4E49-8CA2-20C94821C3FF"),
                statuscode = cub_accounttype_statuscode.Active,
                cub_AccountName = "Retailer"
            });
        }
    }
}
