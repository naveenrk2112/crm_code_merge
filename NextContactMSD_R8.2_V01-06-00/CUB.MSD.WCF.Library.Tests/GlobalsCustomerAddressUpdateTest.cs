/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsCustomerAddressUpdateTest
    {
        [TestMethod]
        public void CustomerAddressUpdateCustomerIsNullTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = null;
                string addressId = null;
                Model.MSDApi.WSAddress addressUpdateData = null;

                global.CustomerAddressUpdate(customerId, addressId, addressUpdateData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "CustomerId is null");
            }
        }

        [TestMethod]
        public void CustomerAddressUpdateCustomerIsBadValueTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDZ";
                string addressId = null;
                Model.MSDApi.WSAddress addressUpdateData = null;

                global.CustomerAddressUpdate(customerId, addressId, addressUpdateData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "Customer not found");
            }
        }

        [TestMethod]
        public void CustomerAddressUpdateAddressIsNullTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string addressId = null;
                Model.MSDApi.WSAddress addressUpdateData = null;

                global.CustomerAddressUpdate(customerId, addressId, addressUpdateData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "AddressId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "AddressId is null");
            }
        }

        [TestMethod]
        public void CustomerAddressUpdateAddressIsBadValueTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string addressId = "7F754EAB-9545-4EEF-8063-A53453DD2FDZ";
                Model.MSDApi.WSAddress addressUpdateData = null;

                global.CustomerAddressUpdate(customerId, addressId, addressUpdateData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "AddressId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "AddressId not found");
            }
        }

        [TestMethod]
        public void CustomerAddressAddressIsNullTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string addressId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                Model.MSDApi.WSAddress addressUpdateData = null;

                global.CustomerAddressUpdate(customerId, addressId, addressUpdateData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "Address");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }
        }

        [TestMethod]
        public void CustomerAddressServiceIsNullTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string addressId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                Model.MSDApi.WSAddress addressUpdateData = new Model.MSDApi.WSAddress();

                global.CustomerAddressUpdate(customerId, addressId, addressUpdateData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "MSD");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, "CUB.MSD.Logic");
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "Object reference not set to an instance of an object.");
            }
        }
    }
}
