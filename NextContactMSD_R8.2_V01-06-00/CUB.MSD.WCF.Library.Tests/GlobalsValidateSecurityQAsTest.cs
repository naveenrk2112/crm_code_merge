/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsValidateSecurityQAsTest
    {
        [TestMethod]
        public void ValidateSecurityQAsSuccessTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                Account acc = new Account();
                acc.AccountId = Guid.NewGuid();
                acc.CustomerTypeCode = (object)"Individual";
                acc.cub_AccountTypeId = new EntityReference();
                acc.cub_AccountTypeId.Id = Guid.NewGuid();
                acc.StatusCode = account_statuscode.Active;

                Contact c = new Contact();
                c.ContactId = Guid.NewGuid();
                c.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };

                cub_Credential cc = new cub_Credential();
                cc.cub_CredentialId = Guid.NewGuid();
                cc.cub_Username = "myUsername";
                cc.cub_ContactId = new EntityReference { LogicalName = c.LogicalName, Id = c.ContactId.Value };

                cub_SecurityQuestion csq1 = new cub_SecurityQuestion();
                csq1.cub_SecurityQuestionId = Guid.NewGuid();
                csq1.cub_SecurityQuestionDesc = "What is your favorite color?";

                cub_SecurityAnswer csa1 = new cub_SecurityAnswer();
                csa1.cub_SecurityAnswerId = Guid.NewGuid();
                csa1.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa1.cub_securityquestion_securityanswer = csq1;
                csa1.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq1.cub_SecurityQuestionId };
                csa1.cub_SecurityAnswerDesc = "Rosa";

                cub_SecurityQuestion csq2 = new cub_SecurityQuestion();
                csq2.cub_SecurityQuestionId = Guid.NewGuid();
                csq2.cub_SecurityQuestionDesc = "What is your favorite car marque?";

                cub_SecurityAnswer csa2 = new cub_SecurityAnswer();
                csa2.cub_SecurityAnswerId = Guid.NewGuid();
                csa2.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa2.cub_securityquestion_securityanswer = csq2;
                csa2.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq2.cub_SecurityQuestionId };
                csa2.cub_SecurityAnswerDesc = "Ferrari";

                data.Add(acc);
                data.Add(c);
                data.Add(cc);
                data.Add(csq1);
                data.Add(csa1);
                data.Add(csq2);
                data.Add(csa2);

                List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
                data.AddRange(globalpwddata);

                fakedContext.Initialize(data);

                JsonModels.WSPasswordForgotRequest validateSecurityQAData = new JsonModels.WSPasswordForgotRequest();
                validateSecurityQAData.username = cc.cub_Username;
                validateSecurityQAData.securityQuestionAnswers = new System.Collections.Generic.List<JsonModels.WSSecurityQA>();

                JsonModels.WSSecurityQA qa1 = new JsonModels.WSSecurityQA();
                qa1.securityQuestion = "What is your favorite color?";
                qa1.securityAnswer = "Rosa";

                validateSecurityQAData.securityQuestionAnswers.Add(qa1);

                JsonModels.WSSecurityQA qa2 = new JsonModels.WSSecurityQA();
                qa2.securityQuestion = "What is your favorite car marque?";
                qa2.securityAnswer = "Ferrari";

                validateSecurityQAData.securityQuestionAnswers.Add(qa2);

                JsonModels.WSPasswordForgotResponse response = global.validateSecurityQAs(validateSecurityQAData);

                Assert.AreEqual(response.contactId, c.ContactId.Value.ToString().ToUpper());
                Assert.AreEqual(response.customerId, c.ParentCustomerId.Id.ToString().ToUpper());
                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.IsNull(exActMgrFault);
            }
        }

        [TestMethod]
        public void ValidateSecurityQAsNullUsernameTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSPasswordForgotRequest validateSecurityQAData = new JsonModels.WSPasswordForgotRequest();
                validateSecurityQAData.username = null;
                validateSecurityQAData.securityQuestionAnswers = new System.Collections.Generic.List<JsonModels.WSSecurityQA>();

                JsonModels.WSSecurityQA qa1 = new JsonModels.WSSecurityQA();
                qa1.securityQuestion = "What is your favorite color?";
                qa1.securityAnswer = "Rosa";

                validateSecurityQAData.securityQuestionAnswers.Add(qa1);

                JsonModels.WSSecurityQA qa2 = new JsonModels.WSSecurityQA();
                qa2.securityQuestion = "What is your favorite car marque?";
                qa2.securityAnswer = "Ferrari";

                validateSecurityQAData.securityQuestionAnswers.Add(qa2);

                JsonModels.WSPasswordForgotResponse response = global.validateSecurityQAs(validateSecurityQAData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "Username");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }
        }

        [TestMethod]
        public void ValidateSecurityQAsNullQAsTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSPasswordForgotRequest validateSecurityQAData = new JsonModels.WSPasswordForgotRequest();
                validateSecurityQAData.username = "myUsername";
                validateSecurityQAData.securityQuestionAnswers = null;

                JsonModels.WSPasswordForgotResponse response = global.validateSecurityQAs(validateSecurityQAData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "SecurityQuestionAnswers");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }
        }

        [TestMethod]
        public void ValidateSecurityQAsCredsNotFoundTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                Account acc = new Account();
                acc.AccountId = Guid.NewGuid();
                acc.CustomerTypeCode = (object)"Individual";
                acc.cub_AccountTypeId = new EntityReference();
                acc.cub_AccountTypeId.Id = Guid.NewGuid();
                acc.StatusCode = account_statuscode.Active;

                Contact c = new Contact();
                c.ContactId = Guid.NewGuid();
                c.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };

                cub_Credential cc = new cub_Credential();
                cc.cub_CredentialId = Guid.NewGuid();
                cc.cub_Username = "myUsername";
                cc.cub_ContactId = new EntityReference { LogicalName = c.LogicalName, Id = c.ContactId.Value };

                cub_SecurityQuestion csq1 = new cub_SecurityQuestion();
                csq1.cub_SecurityQuestionId = Guid.NewGuid();
                csq1.cub_SecurityQuestionDesc = "What is your favorite color?";

                cub_SecurityAnswer csa1 = new cub_SecurityAnswer();
                csa1.cub_SecurityAnswerId = Guid.NewGuid();
                csa1.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa1.cub_securityquestion_securityanswer = csq1;
                csa1.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq1.cub_SecurityQuestionId };
                csa1.cub_SecurityAnswerDesc = "Rosa";

                cub_SecurityQuestion csq2 = new cub_SecurityQuestion();
                csq2.cub_SecurityQuestionId = Guid.NewGuid();
                csq2.cub_SecurityQuestionDesc = "What is your favorite car marque?";

                cub_SecurityAnswer csa2 = new cub_SecurityAnswer();
                csa2.cub_SecurityAnswerId = Guid.NewGuid();
                csa2.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa2.cub_securityquestion_securityanswer = csq2;
                csa2.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq2.cub_SecurityQuestionId };
                csa2.cub_SecurityAnswerDesc = "Ferrari";

                data.Add(acc);
                data.Add(c);
                data.Add(cc);
                data.Add(csq1);
                data.Add(csa1);
                data.Add(csq2);
                data.Add(csa2);

                JsonModels.WSPasswordForgotRequest validateSecurityQAData = new JsonModels.WSPasswordForgotRequest();
                validateSecurityQAData.username = "myUsername";
                validateSecurityQAData.securityQuestionAnswers = new System.Collections.Generic.List<JsonModels.WSSecurityQA>();

                JsonModels.WSSecurityQA qa1 = new JsonModels.WSSecurityQA();
                qa1.securityQuestion = "What is your favorite color?";
                qa1.securityAnswer = "Rosa";

                validateSecurityQAData.securityQuestionAnswers.Add(qa1);

                JsonModels.WSSecurityQA qa2 = new JsonModels.WSSecurityQA();
                qa2.securityQuestion = "What is your favorite car marque?";
                qa2.securityAnswer = "Ferrari";

                validateSecurityQAData.securityQuestionAnswers.Add(qa2);

                JsonModels.WSPasswordForgotResponse response = global.validateSecurityQAs(validateSecurityQAData);

                Assert.AreEqual(response.contactId, c.ContactId.ToString().ToUpper());
                Assert.AreEqual(response.customerId, c.ParentCustomerId.Id.ToString());
                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault>)
            {
            }
        }

        [TestMethod]
        public void ValidateSecurityQAsQuestionNotFoundTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                Account acc = new Account();
                acc.AccountId = Guid.NewGuid();
                acc.CustomerTypeCode = (object)"Individual";
                acc.cub_AccountTypeId = new EntityReference();
                acc.cub_AccountTypeId.Id = Guid.NewGuid();
                acc.StatusCode = account_statuscode.Active;

                Contact c = new Contact();
                c.ContactId = Guid.NewGuid();
                c.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };

                cub_Credential cc = new cub_Credential();
                cc.cub_CredentialId = Guid.NewGuid();
                cc.cub_Username = "myUsername";
                cc.cub_ContactId = new EntityReference { LogicalName = c.LogicalName, Id = c.ContactId.Value };

                cub_SecurityQuestion csq1 = new cub_SecurityQuestion();
                csq1.cub_SecurityQuestionId = Guid.NewGuid();
                csq1.cub_SecurityQuestionDesc = "What is your favorite color?";

                cub_SecurityAnswer csa1 = new cub_SecurityAnswer();
                csa1.cub_SecurityAnswerId = Guid.NewGuid();
                csa1.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa1.cub_securityquestion_securityanswer = csq1;
                csa1.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq1.cub_SecurityQuestionId };
                csa1.cub_SecurityAnswerDesc = "Rosa";

                cub_SecurityQuestion csq2 = new cub_SecurityQuestion();
                csq2.cub_SecurityQuestionId = Guid.NewGuid();
                csq2.cub_SecurityQuestionDesc = "What is your favorite car marque?";

                cub_SecurityAnswer csa2 = new cub_SecurityAnswer();
                csa2.cub_SecurityAnswerId = Guid.NewGuid();
                csa2.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa2.cub_securityquestion_securityanswer = csq2;
                csa2.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq2.cub_SecurityQuestionId };
                csa2.cub_SecurityAnswerDesc = "Ferrari";

                data.Add(acc);
                data.Add(c);
                data.Add(cc);
                data.Add(csq1);
                data.Add(csa1);
                data.Add(csq2);
                data.Add(csa2);

                List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
                data.AddRange(globalpwddata);

                fakedContext.Initialize(data);

                JsonModels.WSPasswordForgotRequest validateSecurityQAData = new JsonModels.WSPasswordForgotRequest();
                validateSecurityQAData.username = cc.cub_Username;
                validateSecurityQAData.securityQuestionAnswers = new System.Collections.Generic.List<JsonModels.WSSecurityQA>();

                JsonModels.WSSecurityQA qa1 = new JsonModels.WSSecurityQA();
                qa1.securityQuestion = "What is your favorite color?";
                qa1.securityAnswer = "Rosa";

                validateSecurityQAData.securityQuestionAnswers.Add(qa1);

                JsonModels.WSSecurityQA qa2 = new JsonModels.WSSecurityQA();
                qa2.securityQuestion = "What is your mother's maiden name?";
                qa2.securityAnswer = "Smith";

                validateSecurityQAData.securityQuestionAnswers.Add(qa2);

                JsonModels.WSPasswordForgotResponse response = global.validateSecurityQAs(validateSecurityQAData);

                Assert.AreEqual(response.contactId, c.ContactId.Value.ToString().ToUpper());
                Assert.AreEqual(response.customerId, c.ParentCustomerId.Id.ToString());
                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "SecurityQuestionAnswers");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, "errors.security.answer.mismatch");
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "One or more of the provided security answers did not match the answers on the account");
            }
        }

        [TestMethod]
        public void ValidateSecurityQAsWrongAnswerTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                Account acc = new Account();
                acc.AccountId = Guid.NewGuid();
                acc.CustomerTypeCode = (object)"Individual";
                acc.cub_AccountTypeId = new EntityReference();
                acc.cub_AccountTypeId.Id = Guid.NewGuid();
                acc.StatusCode = account_statuscode.Active;

                Contact c = new Contact();
                c.ContactId = Guid.NewGuid();
                c.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };

                cub_Credential cc = new cub_Credential();
                cc.cub_CredentialId = Guid.NewGuid();
                cc.cub_Username = "myUsername";
                cc.cub_ContactId = new EntityReference { LogicalName = c.LogicalName, Id = c.ContactId.Value };

                cub_SecurityQuestion csq1 = new cub_SecurityQuestion();
                csq1.cub_SecurityQuestionId = Guid.NewGuid();
                csq1.cub_SecurityQuestionDesc = "What is your favorite color?";

                cub_SecurityAnswer csa1 = new cub_SecurityAnswer();
                csa1.cub_SecurityAnswerId = Guid.NewGuid();
                csa1.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa1.cub_securityquestion_securityanswer = csq1;
                csa1.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq1.cub_SecurityQuestionId };
                csa1.cub_SecurityAnswerDesc = "Rosa";

                cub_SecurityQuestion csq2 = new cub_SecurityQuestion();
                csq2.cub_SecurityQuestionId = Guid.NewGuid();
                csq2.cub_SecurityQuestionDesc = "What is your favorite car marque?";

                cub_SecurityAnswer csa2 = new cub_SecurityAnswer();
                csa2.cub_SecurityAnswerId = Guid.NewGuid();
                csa2.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa2.cub_securityquestion_securityanswer = csq2;
                csa2.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq2.cub_SecurityQuestionId };
                csa2.cub_SecurityAnswerDesc = "Ferrari";

                data.Add(acc);
                data.Add(c);
                data.Add(cc);
                data.Add(csq1);
                data.Add(csa1);
                data.Add(csq2);
                data.Add(csa2);

                List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
                data.AddRange(globalpwddata);

                fakedContext.Initialize(data);

                JsonModels.WSPasswordForgotRequest validateSecurityQAData = new JsonModels.WSPasswordForgotRequest();
                validateSecurityQAData.username = cc.cub_Username;
                validateSecurityQAData.securityQuestionAnswers = new System.Collections.Generic.List<JsonModels.WSSecurityQA>();

                JsonModels.WSSecurityQA qa1 = new JsonModels.WSSecurityQA();
                qa1.securityQuestion = "What is your favorite color?";
                qa1.securityAnswer = "Rosa";

                validateSecurityQAData.securityQuestionAnswers.Add(qa1);

                JsonModels.WSSecurityQA qa2 = new JsonModels.WSSecurityQA();
                qa2.securityQuestion = "What is your favorite car marque?";
                qa2.securityAnswer = "Lotus";

                validateSecurityQAData.securityQuestionAnswers.Add(qa2);

                JsonModels.WSPasswordForgotResponse response = global.validateSecurityQAs(validateSecurityQAData);

                Assert.AreEqual(response.contactId, c.ContactId.Value.ToString().ToUpper());
                Assert.AreEqual(response.customerId, c.ParentCustomerId.Id.ToString());
                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "SecurityQuestionAnswers");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, "errors.security.answer.mismatch");
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "One or more of the provided security answers did not match the answers on the account");
            }
        }

        [TestMethod]
        public void ValidateSecurityQAsQAsNotFoundTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                Account acc = new Account();
                acc.AccountId = Guid.NewGuid();
                acc.CustomerTypeCode = (object)"Individual";
                acc.cub_AccountTypeId = new EntityReference();
                acc.cub_AccountTypeId.Id = Guid.NewGuid();
                acc.StatusCode = account_statuscode.Active;

                Contact c = new Contact();
                c.ContactId = Guid.NewGuid();
                c.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };

                cub_Credential cc = new cub_Credential();
                cc.cub_CredentialId = Guid.NewGuid();
                cc.cub_Username = "myUsername";
                cc.cub_ContactId = new EntityReference { LogicalName = c.LogicalName, Id = Guid.NewGuid() };

                cub_SecurityQuestion csq1 = new cub_SecurityQuestion();
                csq1.cub_SecurityQuestionId = Guid.NewGuid();
                csq1.cub_SecurityQuestionDesc = "What is your favorite color?";

                cub_SecurityAnswer csa1 = new cub_SecurityAnswer();
                csa1.cub_SecurityAnswerId = Guid.NewGuid();
                csa1.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa1.cub_securityquestion_securityanswer = csq1;
                csa1.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq1.cub_SecurityQuestionId };
                csa1.cub_SecurityAnswerDesc = "Rosa";

                cub_SecurityQuestion csq2 = new cub_SecurityQuestion();
                csq2.cub_SecurityQuestionId = Guid.NewGuid();
                csq2.cub_SecurityQuestionDesc = "What is your favorite car marque?";

                cub_SecurityAnswer csa2 = new cub_SecurityAnswer();
                csa2.cub_SecurityAnswerId = Guid.NewGuid();
                csa2.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa2.cub_securityquestion_securityanswer = csq2;
                csa2.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq2.cub_SecurityQuestionId };
                csa2.cub_SecurityAnswerDesc = "Ferrari";

                data.Add(acc);
                data.Add(c);
                data.Add(cc);
                data.Add(csq1);
                data.Add(csa1);
                data.Add(csq2);
                data.Add(csa2);

                List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
                data.AddRange(globalpwddata);

                fakedContext.Initialize(data);

                JsonModels.WSPasswordForgotRequest validateSecurityQAData = new JsonModels.WSPasswordForgotRequest();
                validateSecurityQAData.username = cc.cub_Username;
                validateSecurityQAData.securityQuestionAnswers = new System.Collections.Generic.List<JsonModels.WSSecurityQA>();

                JsonModels.WSSecurityQA qa1 = new JsonModels.WSSecurityQA();
                qa1.securityQuestion = "What is your favorite color?";
                qa1.securityAnswer = "Rosa";

                validateSecurityQAData.securityQuestionAnswers.Add(qa1);

                JsonModels.WSSecurityQA qa2 = new JsonModels.WSSecurityQA();
                qa2.securityQuestion = "What is your favorite car marque?";
                qa2.securityAnswer = "Lotus";

                validateSecurityQAData.securityQuestionAnswers.Add(qa2);

                JsonModels.WSPasswordForgotResponse response = global.validateSecurityQAs(validateSecurityQAData);

                Assert.AreEqual(response.contactId, c.ContactId.Value.ToString().ToUpper());
                Assert.AreEqual(response.customerId, c.ParentCustomerId.Id.ToString());
                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "SecurityQuestionAnswers");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, "errors.security.not.possible");
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "Account has no security question answer pairs on file");
            }
        }

        [TestMethod]
        public void ValidateSecurityQAsContactNotFoundTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                Account acc = new Account();
                acc.AccountId = Guid.NewGuid();
                acc.CustomerTypeCode = (object)"Individual";
                acc.cub_AccountTypeId = new EntityReference();
                acc.cub_AccountTypeId.Id = Guid.NewGuid();
                acc.StatusCode = account_statuscode.Active;

                Contact c = new Contact();
                c.ContactId = Guid.NewGuid();

                cub_Credential cc = new cub_Credential();
                cc.cub_CredentialId = Guid.NewGuid();
                cc.cub_Username = "myUsername";
                cc.cub_ContactId = new EntityReference { LogicalName = c.LogicalName, Id = c.ContactId.Value };

                cub_SecurityQuestion csq1 = new cub_SecurityQuestion();
                csq1.cub_SecurityQuestionId = Guid.NewGuid();
                csq1.cub_SecurityQuestionDesc = "What is your favorite color?";

                cub_SecurityAnswer csa1 = new cub_SecurityAnswer();
                csa1.cub_SecurityAnswerId = Guid.NewGuid();
                csa1.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa1.cub_securityquestion_securityanswer = csq1;
                csa1.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq1.cub_SecurityQuestionId };
                csa1.cub_SecurityAnswerDesc = "Rosa";

                cub_SecurityQuestion csq2 = new cub_SecurityQuestion();
                csq2.cub_SecurityQuestionId = Guid.NewGuid();
                csq2.cub_SecurityQuestionDesc = "What is your favorite car marque?";

                cub_SecurityAnswer csa2 = new cub_SecurityAnswer();
                csa2.cub_SecurityAnswerId = Guid.NewGuid();
                csa2.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa2.cub_securityquestion_securityanswer = csq2;
                csa2.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq2.cub_SecurityQuestionId };
                csa2.cub_SecurityAnswerDesc = "Ferrari";

                data.Add(acc);
                data.Add(c);
                data.Add(cc);
                data.Add(csq1);
                data.Add(csa1);
                data.Add(csq2);
                data.Add(csa2);

                List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
                data.AddRange(globalpwddata);

                fakedContext.Initialize(data);

                JsonModels.WSPasswordForgotRequest validateSecurityQAData = new JsonModels.WSPasswordForgotRequest();
                validateSecurityQAData.username = cc.cub_Username;
                validateSecurityQAData.securityQuestionAnswers = new System.Collections.Generic.List<JsonModels.WSSecurityQA>();

                JsonModels.WSSecurityQA qa1 = new JsonModels.WSSecurityQA();
                qa1.securityQuestion = "What is your favorite color?";
                qa1.securityAnswer = "Rosa";

                validateSecurityQAData.securityQuestionAnswers.Add(qa1);

                JsonModels.WSSecurityQA qa2 = new JsonModels.WSSecurityQA();
                qa2.securityQuestion = "What is your favorite car marque?";
                qa2.securityAnswer = "Ferrari";

                validateSecurityQAData.securityQuestionAnswers.Add(qa2);

                JsonModels.WSPasswordForgotResponse response = global.validateSecurityQAs(validateSecurityQAData);

                Assert.AreEqual(response.contactId, c.ContactId.Value.ToString().ToUpper());
                Assert.AreEqual(response.customerId, c.ParentCustomerId.Id.ToString());
                Assert.IsNotNull(response.tokenExpiryDateTime);
                Assert.IsNotNull(response.verificationToken);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "Username");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "Username not found");
            }
        }
    }
}
