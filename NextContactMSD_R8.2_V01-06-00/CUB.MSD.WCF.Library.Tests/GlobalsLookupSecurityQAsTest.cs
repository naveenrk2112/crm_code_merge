/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsLookupSecurityQAsTest
    {
        [TestMethod]
        public void LookupSecurityQAsSuccessTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                Contact c = new Contact();
                c.ContactId = Guid.NewGuid();

                cub_Credential cc = new cub_Credential();
                cc.cub_CredentialId = Guid.NewGuid();
                cc.cub_Username = "myUsername";
                cc.cub_ContactId = new EntityReference { LogicalName = c.LogicalName, Id = c.ContactId.Value };

                cub_SecurityQuestion csq1 = new cub_SecurityQuestion();
                csq1.cub_SecurityQuestionId = Guid.NewGuid();
                csq1.cub_SecurityQuestionDesc = "What is your favorite color?";

                cub_SecurityQuestion csq2 = new cub_SecurityQuestion();
                csq2.cub_SecurityQuestionId = Guid.NewGuid();
                csq2.cub_SecurityQuestionDesc = "What is your favorite car marque?";

                cub_SecurityAnswer csa1 = new cub_SecurityAnswer();
                csa1.cub_SecurityAnswerId = Guid.NewGuid();
                csa1.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa1.cub_securityquestion_securityanswer = csq1;
                csa1.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq1.cub_SecurityQuestionId };
                csa1.cub_SecurityAnswerDesc = "Rosa";

                cub_SecurityAnswer csa2 = new cub_SecurityAnswer();
                csa2.cub_SecurityAnswerId = Guid.NewGuid();
                csa2.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)c.ContactId };
                csa2.cub_securityquestion_securityanswer = csq2;
                csa2.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq2.cub_SecurityQuestionId };
                csa2.cub_SecurityAnswerDesc = "Ferrari";

                data.Add(c);
                data.Add(cc);
                data.Add(csq1);
                data.Add(csq2);
                data.Add(csa1);
                data.Add(csa2);

                fakedContext.Initialize(data);

                JsonModels.WSSecurityQuestionsRetreiveRequest usernameData = new JsonModels.WSSecurityQuestionsRetreiveRequest();

                usernameData.username = cc.cub_Username;

                JsonModels.WSSecurityQuestionsWithStrings response = global.LookupSecurityQAs(usernameData);

                Assert.IsNotNull(response.securityQuestions);
                Assert.AreEqual(response.securityQuestions.Count, 2);
                Assert.AreEqual(response.securityQuestions[0], "What is your favorite color?");
                Assert.AreEqual(response.securityQuestions[1], "What is your favorite car marque?" );
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.IsNull(exActMgrFault);
            }
        }

        [TestMethod]
        public void LookupSecurityQAsNullUsernameTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSSecurityQuestionsRetreiveRequest usernameData = new JsonModels.WSSecurityQuestionsRetreiveRequest();

                usernameData.username = null;

                JsonModels.WSSecurityQuestionsWithStrings response = global.LookupSecurityQAs(usernameData);

                Assert.IsNotNull(response.securityQuestions);
                Assert.AreEqual(response.securityQuestions.Count, 2);
                Assert.AreEqual(response.securityQuestions[0], "What is your favorite color?");
                Assert.AreEqual(response.securityQuestions[1], "What is your favorite car marque?");
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                Assert.AreEqual(e.Detail.fieldName, "Username");
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }
        }

        [TestMethod]
        public void LookupSecurityQAsNullServiceTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                JsonModels.WSSecurityQuestionsRetreiveRequest usernameData = new JsonModels.WSSecurityQuestionsRetreiveRequest();

                usernameData.username = "myUsername";

                global.ConnectionInfo = null;

                JsonModels.WSSecurityQuestionsWithStrings response = global.LookupSecurityQAs(usernameData);

                Assert.IsNotNull(response.securityQuestions);
                Assert.AreEqual(response.securityQuestions.Count, 2);
                Assert.AreEqual(response.securityQuestions[0], "What is your favorite color?");
                Assert.AreEqual(response.securityQuestions[1], "What is your favorite car marque?");
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                Assert.AreEqual(e.Detail.fieldName, "MSD");
                Assert.AreEqual(e.Detail.errorKey, "CUB.MSD.WCF.Library");
                Assert.AreEqual(e.Detail.errorMessage, "Object reference not set to an instance of an object.");
            }
        }
    }
}
