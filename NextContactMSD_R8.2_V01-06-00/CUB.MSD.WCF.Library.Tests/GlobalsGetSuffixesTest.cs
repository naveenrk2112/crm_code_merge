/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using System;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGetSuffixesTest
    {
        [TestMethod]
        public void GetSuffixesTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            fakedContext.Initialize(PrepareData());
            var fakedService = fakedContext.GetOrganizationService();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            var suffixes = global.GetClientSuffixes();
            foreach(var item in suffixes.nameSuffixes)
            {
                // The suffix key is the first letter of the description
                switch (item.value)
                {
                    case "Junior":
                        Assert.AreEqual(item.key, "1");
                        break;
                    case "Senior":
                        Assert.AreEqual(item.key, "2");
                        break;
                    case "Esquire":
                        Assert.AreEqual(item.key, "3");
                        break;
                }
            }
        }

        private static List<Entity> PrepareData()
        {
            List<Entity> data = new List<Entity>();
            Guid translationID = Guid.NewGuid();
            data.Add(new cub_OptionSetTranslation { cub_OptionSetTranslationId = translationID, cub_OptionSetName = "cub_contactsuffix" });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "1",
                cub_OptionSetValue = 971880000
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "2",
                cub_OptionSetValue = 971880001
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, translationID),
                cub_Translation = "3",
                cub_OptionSetValue = 971880002
            });
            return data;
        }
    }
}
