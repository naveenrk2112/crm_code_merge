﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using CUB.MSD.WCF;
using Microsoft.QualityTools.Testing.Fakes;
using System.ServiceModel.Web.Fakes;
using System.Web.Fakes;
using System.Net;
using System.Web;
using System.Collections.Specialized;
using Microsoft.Xrm.Sdk.Fakes;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsClientFeedbackTest
    {
        [TestMethod]
        public void FeedbackHappyPathTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            GlobalsClientFeedbackTest.PrepareData(data, false);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            JsonModels.WSCustomerFeedbackRequest request = new JsonModels.WSCustomerFeedbackRequest();
            request.feedbackType = "GeneralFeedback";
            request.feedbackMessage = "This is a test feedback from a imaginary customer";
            request.customerId = "2AFFC4CA-787E-424F-BB2D-2C2DFD81EB22";
            request.contactId = "33CB941B-881C-49BD-BDB8-657BCC050B38";
            request.channel = "Web";

            JsonModels.WSCustomerFeedbackResponse response = global.createCase(request);
            Assert.IsNotNull(response.caseId);
        }

        [TestMethod]
        public void FeedbackFeedBackEmptyTypeTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            GlobalsClientFeedbackTest.PrepareData(data, false);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            JsonModels.WSCustomerFeedbackRequest request = new JsonModels.WSCustomerFeedbackRequest();
            request.feedbackMessage = "This is a test feedback from a imaginary customer";
            request.customerId = "2AFFC4CA-787E-424F-BB2D-2C2DFD81EB22";
            request.contactId = "33CB941B-881C-49BD-BDB8-657BCC050B38";
            request.channel = "Web";

            using (ShimsContext.Create())
            {
                var fakeResponse = new ShimOutgoingWebResponseContext();
                var fakeRequest = new ShimIncomingWebRequestContext();

                var ctx_hd = new WebHeaderCollection();
                ctx_hd.Add("myCustomHeader", "XXXX");
                fakeRequest.HeadersGet = () => ctx_hd;
                fakeResponse.HeadersGet = () => ctx_hd;

                var ctx = new ShimWebOperationContext
                {
                    OutgoingResponseGet = () => fakeResponse,
                    IncomingRequestGet = () => fakeRequest
                };
                ShimWebOperationContext.CurrentGet = () => ctx;

                RestService restService = new RestService();
                JsonModels.WSCustomerFeedbackResponse wfcResponse = restService.clientFeedback(request);
                string responseheader = ctx.Instance.OutgoingResponse.Headers.Get("x-cub-hdr");
                Assert.AreEqual(responseheader, "{\"result\":\"DataValidationError\",\"uid\":null,\"fieldName\":\"feedbackType\",\"errorKey\":\"errors.general.value.required\",\"errorMessage\":\"Field is blank.\"}");
            }
        }

        [TestMethod]
        public void FeedbackFeedMessageToLongTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            GlobalsClientFeedbackTest.PrepareData(data, false);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            JsonModels.WSCustomerFeedbackRequest request = new JsonModels.WSCustomerFeedbackRequest();
            request.feedbackType = "GeneralFeedback";
            request.feedbackMessage = "This is a test feedback from a imaginary customer".PadRight(2001, '-');
            request.customerId = "2AFFC4CA-787E-424F-BB2D-2C2DFD81EB22";
            request.contactId = "33CB941B-881C-49BD-BDB8-657BCC050B38";
            request.channel = "Web";

            using (ShimsContext.Create())
            {
                var fakeResponse = new ShimOutgoingWebResponseContext();
                var fakeRequest = new ShimIncomingWebRequestContext();

                var ctx_hd = new WebHeaderCollection();
                ctx_hd.Add("myCustomHeader", "XXXX");
                fakeRequest.HeadersGet = () => ctx_hd;
                fakeResponse.HeadersGet = () => ctx_hd;

                var ctx = new ShimWebOperationContext
                {
                    OutgoingResponseGet = () => fakeResponse,
                    IncomingRequestGet = () => fakeRequest
                };
                ShimWebOperationContext.CurrentGet = () => ctx;

                RestService restService = new RestService();
                JsonModels.WSCustomerFeedbackResponse wfcResponse = restService.clientFeedback(request);
                string responseheader = ctx.Instance.OutgoingResponse.Headers.Get("x-cub-hdr");
                Assert.AreEqual(responseheader, "{\"result\":\"DataValidationError\",\"uid\":null,\"fieldName\":\"feedbackMessage\",\"errorKey\":\"errors.general.value.toolong\",\"errorMessage\":\"Maximum length exceeded.\"}");
            }
        }

        [TestMethod]
        public void FeedbackSubjectNotFoundTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            GlobalsClientFeedbackTest.PrepareData(data, true);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            JsonModels.WSCustomerFeedbackRequest request = new JsonModels.WSCustomerFeedbackRequest();
            request.feedbackType = "GeneralFeedback";
            request.feedbackMessage = "This is a test feedback from an imaginary customer";
            request.customerId = "2AFFC4CA-787E-424F-BB2D-2C2DFD81EB22";
            request.contactId = "33CB941B-881C-49BD-BDB8-657BCC050B38";
            request.channel = "Web";
            try
            {
                JsonModels.WSCustomerFeedbackResponse response = global.createCase(request);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Subject not found");
            }

            //using (ShimsContext.Create())
            //{
            //    /*
            //     * Fake operation context
            //     */
            //    var fakeResponse = new ShimOutgoingWebResponseContext();
            //    var fakeRequest = new ShimIncomingWebRequestContext();

            //    var ctx_hd = new WebHeaderCollection();
            //    ctx_hd.Add("x-cub-audit", "{\"sourceIp\":\"172.21.17.18\",\"userId\":\"\",\"channel\":\"Web\",\"location\":\"MSD WEB\"}");
            //    fakeRequest.HeadersGet = () => ctx_hd;
            //    fakeResponse.HeadersGet = () => ctx_hd;

            //    var ctx = new ShimWebOperationContext
            //    {
            //        OutgoingResponseGet = () => fakeResponse,
            //        IncomingRequestGet = () => fakeRequest
            //    };
            //    ShimWebOperationContext.CurrentGet = () => ctx;

            //    /*
            //     * Fake HTTP Context
            //     */
            //    var fakeHttpRequest = new ShimHttpRequest();
            //    fakeHttpRequest.HeadersGet = () => new NameValueCollection { { "x-cub-audit", "{\"sourceIp\":\"172.21.17.18\",\"userId\":\"\",\"channel\":\"Web\",\"location\":\"MSD WEB\"}" } };
            //    var fakeHttpContext = new ShimHttpContext
            //    {
            //        RequestGet = () => fakeHttpRequest
            //    };
            //    ShimHttpContext.CurrentGet = () => { return fakeHttpContext; };

            //    /*
            //     * SDK
            //     */
            //    var service = new StubIOrganizationService();
            //    CUBOrganizationServiceContext orgContext = new CUBOrganizationServiceContext(service);
            //    var fakeContext = new Microsoft.Xrm.Sdk.Client.Fakes.ShimOrganizationServiceContext(orgContext);

            //    RestService restService = new RestService();
            //    JsonModels.WSCustomerFeedbackResponse wfcResponse = restService.clientFeedback(request);
            //    string responseheader = ctx.Instance.OutgoingResponse.Headers.Get("x-cub-hdr");
            //    Assert.AreEqual(responseheader, "{\"result\":\"DataValidationError\",\"uid\":null,\"fieldName\":\"feedbackMessage\",\"errorKey\":\"errors.general.value.toolong\",\"errorMessage\":\"Maximum length exceeded.\"}");
            //}
        }

        public static void PrepareData(List<Entity> data, bool noSubject)
        {
            /*
             * Globals
             */
            EntityReference nisApiGlobalID = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());
            data.Add(new cub_Globals
            {
                cub_GlobalsId = nisApiGlobalID.Id,
                cub_Name = "AppInfo",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CustomerFeedbackTitleTemplate",
                cub_AttributeValue = "{firstName} {lastName}; Email: {unregisteredEmail}; Phone: {phoneType}-{phoneNumber}; Card #: {cardNumber}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CustomerCommunicationCaseSubjectID",
                cub_AttributeValue = "51C77204-2E23-E811-80F9-005056814569",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UnregisteredCustomerID",
                cub_AttributeValue = "9CEF1AB6-F6B8-E711-80F6-005056814569",
                cub_Enabled = true
            });

            /*
             * Customer
             */
            Account customer = new Account();
            customer.AccountId = new Guid("2AFFC4CA-787E-424F-BB2D-2C2DFD81EB22");
            data.Add(customer);

            /*
             * Contact
             */
            Contact contact = new Contact();
            contact.Id = new Guid("33CB941B-881C-49BD-BDB8-657BCC050B38");
            contact.ParentCustomerId = customer.ToEntityReference();
            data.Add(contact);

            /*
             * Case Subject: Customer Communication
             */
            Subject customerCommunicationSubject = new Subject();
            customerCommunicationSubject.SubjectId = new Guid("51C77204-2E23-E811-80F9-005056814569");
            customerCommunicationSubject.Title = "Customer Communication";
            data.Add(customerCommunicationSubject);

            if (!noSubject)
            {
                /*
                 * Case Subject: GeneralFeedback
                 */
                Subject generalFeedBackSubject = new Subject();
                generalFeedBackSubject.SubjectId = Guid.NewGuid();
                generalFeedBackSubject.Title = "General Feedback";
                generalFeedBackSubject.ParentSubject = customerCommunicationSubject.ToEntityReference();
                data.Add(generalFeedBackSubject);
            }

            /*
             * OptionSet Transaltion
             */
            cub_OptionSetTranslation translation = new cub_OptionSetTranslation();
                translation.cub_OptionSetTranslationId = Guid.NewGuid();
                translation.cub_OptionSetName = "CaseOriginCode";
                translation.cub_EntityName = "case";
                data.Add(translation);

            /*
             * Optionset transaltion Detail
             */
            cub_OptionSetTranslationDetail translationDetail = new cub_OptionSetTranslationDetail();
            translationDetail.cub_OptionSetTranslationId = translation.ToEntityReference();
            translationDetail.cub_OptionSetTranslationDetailId = Guid.NewGuid();
            translationDetail.cub_Translation = "Web";
            translationDetail.cub_OptionSetValue = 3;
            data.Add(translationDetail);
        }
    }
}
