﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using CUB.MSD.WCF;
using Microsoft.QualityTools.Testing.Fakes;
using System.ServiceModel.Web.Fakes;
using System.Web.Fakes;
using System.Net;
using System.Web;
using System.Collections.Specialized;
using Microsoft.Xrm.Sdk.Fakes;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsCredentialsValidateTest
    {
        [TestMethod]
        public void HappyPathTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Account account = new Account();
            account.AccountId = Guid.NewGuid();
            account.StateCode = AccountState.Active;
            account.StatusCode = account_statuscode.Active;
            data.Add(account);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            contact.ParentCustomerId = account.ToEntityReference();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            global.CredentialsValidate(account.AccountId.ToString(), contact.ContactId.Value.ToString(), "1234");
        }

        [TestMethod]
        public void InvalidPinTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Account account = new Account();
            account.AccountId = Guid.NewGuid();
            account.StateCode = AccountState.Active;
            account.StatusCode = account_statuscode.Active;
            data.Add(account);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            contact.ParentCustomerId = account.ToEntityReference();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            try
            {
                global.CredentialsValidate(account.AccountId.ToString(), contact.ContactId.Value.ToString(), "1235");
            } catch (FaultException ex)
            {
                Assert.AreEqual(CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD, ((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail.errorKey);
                Assert.AreEqual(CUBConstants.Error.ERR_INVALID_PIN, (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).errorMessage);
                Assert.AreEqual("pin", (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).fieldName);
            }
        }

        [TestMethod]
        public void CredentialsInactiveTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Account account = new Account();
            account.AccountId = Guid.NewGuid();
            account.StateCode = AccountState.Active;
            account.StatusCode = account_statuscode.Active;
            data.Add(account);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            contact.ParentCustomerId = account.ToEntityReference();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.statecode = cub_CredentialState.Inactive;
            credentials.statuscode = cub_credential_statuscode.Inactive;
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            try
            {
                global.CredentialsValidate(account.AccountId.ToString(), contact.ContactId.Value.ToString(), "1234");
            }
            catch (FaultException ex)
            {
                Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_UNEXPECTED, ((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail.errorKey);
                Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_INVALID_STATUS, (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).errorMessage);
                Assert.AreEqual("pin", (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).fieldName);
            }
        }

        [TestMethod]
        public void NoCustomerIdTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Account account = new Account();
            account.AccountId = Guid.NewGuid();
            account.StateCode = AccountState.Active;
            account.StatusCode = account_statuscode.Active;
            data.Add(account);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            contact.ParentCustomerId = account.ToEntityReference();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.statecode = cub_CredentialState.Inactive;
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            try
            {
                global.CredentialsValidate("", contact.ContactId.Value.ToString(), "1234");
            }
            catch (FaultException ex)
            {
                Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, ((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail.errorKey);
                Assert.AreEqual(CUBConstants.Error.MSG_FIELD_IS_BLANK, (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).errorMessage);
                Assert.AreEqual("customerId", (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).fieldName);
            }
        }

        [TestMethod]
        public void NoContactIdTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Account account = new Account();
            account.AccountId = Guid.NewGuid();
            account.StateCode = AccountState.Active;
            account.StatusCode = account_statuscode.Active;
            data.Add(account);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            contact.ParentCustomerId = account.ToEntityReference();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.statecode = cub_CredentialState.Inactive;
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            try
            {
                global.CredentialsValidate(account.AccountId.ToString(), "", "1234");
            }
            catch (FaultException ex)
            {
                Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, ((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail.errorKey);
                Assert.AreEqual(CUBConstants.Error.MSG_FIELD_IS_BLANK, (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).errorMessage);
                Assert.AreEqual("contactId", (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).fieldName);
            }
        }

        [TestMethod]
        public void NoPinTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.statecode = cub_CredentialState.Inactive;
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            try
            {
                global.CredentialsValidate(Guid.NewGuid().ToString(), contact.ContactId.Value.ToString(), "");
            }
            catch (FaultException ex)
            {
                Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED, ((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail.errorKey);
                Assert.AreEqual(CUBConstants.Error.MSG_FIELD_IS_BLANK, (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).errorMessage);
                Assert.AreEqual("pin", (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).fieldName);
            }
        }

        [TestMethod]
        public void CustomerNotFoundTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.statecode = cub_CredentialState.Inactive;
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            try
            {
                global.CredentialsValidate(Guid.NewGuid().ToString(), contact.ContactId.Value.ToString(), "1234");
            }
            catch (FaultException ex)
            {
                Assert.AreEqual(CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND, ((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail.errorKey);
                Assert.AreEqual(CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND, (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).errorMessage);
                Assert.AreEqual("customer-id", (((System.ServiceModel.FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).fieldName);
            }
        }

        [TestMethod]
        public void AccountClosedOrPendingCloseTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Account account = new Account();
            account.AccountId = Guid.NewGuid();
            account.StateCode = AccountState.Inactive;
            account.StatusCode = account_statuscode.PendingClose;
            data.Add(account);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            contact.ParentCustomerId = account.ToEntityReference();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.statecode = cub_CredentialState.Inactive;
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            try
            {
                global.CredentialsValidate(account.AccountId.ToString(), contact.ContactId.Value.ToString(), "1234");
            }
            catch (FaultException ex)
            {
                Assert.AreEqual(CUBConstants.Error.ERR_ACCNT_CLOSED_OR_PENDING_CLOSURE, ((FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail.errorKey);
                Assert.AreEqual(CUBConstants.Error.MSG_ACCNT_CLOSED_OR_PENDING, (((FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).errorMessage);
                Assert.AreEqual("customer-id", (((FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).fieldName);
            }
        }

        [TestMethod]
        public void LoginLockedTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Account account = new Account();
            account.AccountId = Guid.NewGuid();
            account.StateCode = AccountState.Active;
            account.StatusCode = account_statuscode.Active;
            data.Add(account);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            contact.ParentCustomerId = account.ToEntityReference();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.statecode = cub_CredentialState.Inactive;
            credentials.statuscode = cub_credential_statuscode.Locked;
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            try
            {
                global.CredentialsValidate(account.AccountId.ToString(), contact.ContactId.Value.ToString(), "1234");
            }
            catch (FaultException ex)
            {
                Assert.AreEqual(CUBConstants.Error.ERR_ACCNT_LOCKED, ((FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail.errorKey);
                Assert.AreEqual(CUBConstants.Error.MSG_LOGIN_LOCKED, (((FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).errorMessage);
                Assert.AreEqual("pin", (((FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).fieldName);
            }
        }

        [TestMethod]
        public void AccountPendingActivationTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();
            PrepareData(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Account account = new Account();
            account.AccountId = Guid.NewGuid();
            account.StateCode = AccountState.Active;
            account.StatusCode = account_statuscode.Active;
            data.Add(account);

            Contact contact = new Contact();
            contact.ContactId = Guid.NewGuid();
            contact.ParentCustomerId = account.ToEntityReference();
            data.Add(contact);

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = contact.ToEntityReference();
            credentials.statecode = cub_CredentialState.Inactive;
            credentials.statuscode = cub_credential_statuscode.PendingActivation;
            credentials.cub_Pin = "1234";
            data.Add(credentials);

            fakedContext.Initialize(data);
            try
            {
                global.CredentialsValidate(account.AccountId.ToString(), contact.ContactId.Value.ToString(), "1234");
            }
            catch (FaultException ex)
            {
                Assert.AreEqual(CUBConstants.Error.ERR_ACCNT_PENDING_ACTIVATION, ((FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail.errorKey);
                Assert.AreEqual(CUBConstants.Error.MSG_LOGIN_LOCKED, (((FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).errorMessage);
                Assert.AreEqual("pin", (((FaultException<CUB.MSD.Model.MSDApi.WSMSDFault>)ex).Detail).fieldName);
            }
        }

        private static void PrepareData(List<Entity> data)
        {
            var pinValidationGlobal = new cub_Globals
            {
                cub_GlobalsId = Guid.NewGuid(),
                cub_Name = "PinValidation",
                cub_Enabled = true
            };
            data.Add(pinValidationGlobal);
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = pinValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PinMaxLength",
                cub_AttributeValue = "4",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = pinValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PinMinLength",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
        }
    }
}
