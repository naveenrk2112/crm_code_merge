/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsIsAvailableTest
    {
        [TestMethod]
        public void IsAvailableTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals globals = new Globals(ci);
            JsonModels.WSAppInfo info = globals.IsAvailable();
            Assert.IsNotNull(info.NextContactMSD);
            Assert.IsTrue(info.status);
        }
    }

    [TestClass]
    public class GlobalsGetCredentialsNullTest
    {
        [TestMethod]
        public void getCredentialsTest()
        {
            CommonSettings.Domain = null;
            CommonSettings.UserName = null;
            CommonSettings.Password = null;

            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals globals = new Globals(ci);
            System.ServiceModel.Description.ClientCredentials creds = globals.getCredentials();
        }
    }
}
