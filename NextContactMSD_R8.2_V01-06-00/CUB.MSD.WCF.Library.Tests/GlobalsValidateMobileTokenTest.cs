﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using CUB.MSD.Logic;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsValidateMobileTokenTest
    {
        [TestMethod]
        public void ValidateMobileTokenSuccessTest()
        {
            Guid contactId = Guid.NewGuid();
            Guid customerId = Guid.NewGuid();
            Guid credId = Guid.NewGuid();
            Guid verifTokenId = Guid.NewGuid();
            string verificationToken = "jD4U!Y@k%";
            string mobileDeviceToken = "123Sumsung";

            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);


            Contact con = new Contact();
            con.ContactId = contactId;
            con.ParentCustomerId =new EntityReference(Account.EntityLogicalName, customerId);

            cub_Credential cred = new cub_Credential();
            cred.cub_CredentialId = credId;
            cred.cub_ContactId= new EntityReference(Contact.EntityLogicalName, contactId);
            cred.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            cub_VerificationToken verToken = new cub_VerificationToken();
            verToken.cub_CredentialsId = new EntityReference(cub_Credential.EntityLogicalName, credId);
            verToken.cub_Name = UtilityLogic.EncryptTripleDES(verificationToken);
            verToken.cub_MobileDeviceToken = UtilityLogic.EncryptTripleDES(mobileDeviceToken);
            verToken.cub_TokenType = new OptionSetValue((int)cub_verificationtokentype.Mobile);
            verToken.cub_VerificationTokenId = verifTokenId ;
            verToken.cub_TokenExpirationDate = DateTime.Now.Date.AddYears(2);
            verToken.cub_RedemptionIndicator = false;

            data.Add(con);
            data.Add(cred);
            data.Add(verToken);

            //List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
            //data.AddRange(globalpwddata);

            fakedContext.Initialize(data);

            JsonModels.WSVerifyMobileTokenRequest req = new JsonModels.WSVerifyMobileTokenRequest();
            req.contactId = contactId.ToString();
            req.customerId = customerId.ToString();
            req.verificationToken = verificationToken;
            JsonModels.WSVerifyMobileTokenResponse response = global.ValidateMobileToken(req);

            Assert.IsNotNull(response.mobileToken);
            Assert.AreEqual(mobileDeviceToken,response.mobileToken);


        }

        [TestMethod]
        public void ValidateMobileTokenFailureTest()
        {
            Guid contactId = Guid.NewGuid();
            Guid customerId = Guid.NewGuid();
            Guid credId = Guid.NewGuid();
            Guid verifTokenId = Guid.NewGuid();
           

            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            JsonModels.WSVerifyMobileTokenRequest req = new JsonModels.WSVerifyMobileTokenRequest();

            try
            {
                JsonModels.WSVerifyMobileTokenResponse response = global.ValidateMobileToken(null);
            }

            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND);
            }

            
            try
            {
                JsonModels.WSVerifyMobileTokenResponse response = global.ValidateMobileToken(req);
            }

            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {

                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }

            req.customerId = "test";
            try
            {
                JsonModels.WSVerifyMobileTokenResponse response = global.ValidateMobileToken(req);
            }

            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {

                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_CUSTOMER_NOT_FOUND);
            }

            req.customerId =customerId.ToString();
            try
            {
                JsonModels.WSVerifyMobileTokenResponse response = global.ValidateMobileToken(req);
            }

            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {

                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }

            req.contactId = "test";
            try
            {
                JsonModels.WSVerifyMobileTokenResponse response = global.ValidateMobileToken(req);
            }

            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {

                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_CONTACT_NOT_FOUND);
            }

            req.contactId = contactId.ToString();
            try
            {
                JsonModels.WSVerifyMobileTokenResponse response = global.ValidateMobileToken(req);
            }

            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {

                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }


            //Contact con = new Contact();
            //con.ContactId = contactId;
            //con.ParentCustomerId = new EntityReference(Account.EntityLogicalName, customerId);

            //cub_Credential cred = new cub_Credential();
            //cred.cub_CredentialId = credId;
            //cred.cub_ContactId = new EntityReference(Contact.EntityLogicalName, contactId);
            //cred.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            //cub_VerificationToken verToken = new cub_VerificationToken();
            //verToken.cub_CredentialsId = new EntityReference(cub_Credential.EntityLogicalName, credId);
            //verToken.cub_Name = UtilityLogic.EncryptTripleDES(verificationToken);
            //verToken.cub_MobileDeviceToken = UtilityLogic.EncryptTripleDES(mobileDeviceToken);
            //verToken.cub_TokenType = new OptionSetValue((int)cub_verificationtokentype.Mobile);
            //verToken.cub_VerificationTokenId = verifTokenId;

            //data.Add(con);
            //data.Add(cred);
            //data.Add(verToken);

            ////List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
            ////data.AddRange(globalpwddata);

            //fakedContext.Initialize(data);

            //JsonModels.WSVerifyMobileTokenRequest req = new JsonModels.WSVerifyMobileTokenRequest();
            //req.contactId = contactId.ToString();
            //req.customerId = customerId.ToString();
            //req.verificationToken = verificationToken;
            //JsonModels.WSVerifyMobileTokenResponse response = global.ValidateMobileToken(req);

            //Assert.IsNotNull(response.mobileToken);
            //Assert.AreEqual(mobileDeviceToken, response.mobileToken);


        }
    }



}
