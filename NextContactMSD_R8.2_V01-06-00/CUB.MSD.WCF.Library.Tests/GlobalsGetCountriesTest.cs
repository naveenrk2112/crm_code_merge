/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using System;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGetCountriesTest
    {
        [TestMethod]
        public void GetCountriesTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            fakedContext.Initialize(PrepareData());
            var fakedService = fakedContext.GetOrganizationService();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            var countries = global.GetClientCountries();
            Assert.AreEqual(2, countries.countries.Count);
            Assert.AreEqual(countries.countries[0].country, "US");
            Assert.AreEqual(6, countries.countries[0].states.Count);
            Assert.AreEqual("CA", countries.countries[0].states[0].key);
            Assert.AreEqual("California", countries.countries[0].states[0].value);
            Assert.AreEqual("OR", countries.countries[0].states[5].key);
            Assert.AreEqual("Oregon", countries.countries[0].states[5].value);
            Assert.AreEqual("1", countries.countries[0].dialingCode);
            Assert.AreEqual("REQ", countries.countries[0].config.address2);
            Assert.AreEqual("OPT", countries.countries[0].config.address3);
            Assert.AreEqual("REQ", countries.countries[0].config.city);
            Assert.AreEqual("REQ", countries.countries[0].config.state);
            Assert.AreEqual("REQ", countries.countries[0].config.postalCode);

            Assert.AreEqual(countries.countries[1].country, "CA");
            Assert.AreEqual(2, countries.countries[1].states.Count);
            Assert.AreEqual("BC", countries.countries[1].states[0].key);
            Assert.AreEqual("British Columbia", countries.countries[1].states[0].value);
            Assert.AreEqual("ON", countries.countries[1].states[1].key);
            Assert.AreEqual("Ontario", countries.countries[1].states[1].value);
        }

        [TestMethod]
        public void GetCountriesWithNullOptionsTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            fakedContext.Initialize(PrepareDataForNullOptions());
            var fakedService = fakedContext.GetOrganizationService();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            var countries = global.GetClientCountries();
            Assert.IsNull(countries.countries[0].dialingCode);
            Assert.IsNull(countries.countries[0].config.address2);
            Assert.IsNull(countries.countries[0].config.address3);
            Assert.IsNull(countries.countries[0].config.city);
            Assert.IsNull(countries.countries[0].config.state);
            Assert.IsNull(countries.countries[0].config.postalCode);
            Assert.AreEqual(0, countries.countries[0].states.Count);
        }

        private static List<Entity> PrepareData()
        {
            List<Entity> data = new List<Entity>();
            cub_Country us = new cub_Country {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "United States of America",
                cub_Alpha2 = "US",
                cub_Address2 = new OptionSetValue((int) cub_Countrycub_Address2.REQ),
                cub_Address3 = new OptionSetValue((int)cub_Countrycub_Address3.OPT),
                cub_City = new OptionSetValue((int)cub_Countrycub_City.REQ),
                cub_State = new OptionSetValue((int)cub_Countrycub_State.REQ),
                cub_PostalCode = new OptionSetValue((int)cub_Countrycub_PostalCode.REQ),
                cub_DialingCode = "1"
            };
            data.Add(us);
            data.Add(new cub_StateOrProvince {
                            cub_StateOrProvinceId = Guid.NewGuid(),
                            cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                            cub_Abbreviation = "CA",
                            cub_StateName = "California"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "FL",
                cub_StateName = "Florida"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "WA",
                cub_StateName = "Washington"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "CO",
                cub_StateName = "Colorado"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "TX",
                cub_StateName = "Texas"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, us.cub_CountryId.Value),
                cub_Abbreviation = "OR",
                cub_StateName = "Oregon"
            });
            cub_Country ca = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "Canada",
                cub_Alpha2 = "CA",
                cub_Address2 = new OptionSetValue((int)cub_Countrycub_Address2.REQ),
                cub_Address3 = new OptionSetValue((int)cub_Countrycub_Address3.OPT),
                cub_City = new OptionSetValue((int)cub_Countrycub_City.REQ),
                cub_State = new OptionSetValue((int)cub_Countrycub_State.REQ),
                cub_PostalCode = new OptionSetValue((int)cub_Countrycub_PostalCode.REQ),
                cub_DialingCode = "1"
            };
            data.Add(ca);
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, ca.cub_CountryId.Value),
                cub_Abbreviation = "BC",
                cub_StateName = "British Columbia"
            });
            data.Add(new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = new EntityReference(cub_Country.EntityLogicalName, ca.cub_CountryId.Value),
                cub_Abbreviation = "ON",
                cub_StateName = "Ontario"
            });
            return data;
        }

        private static List<Entity> PrepareDataForNullOptions ()
        {
            List<Entity> data = new List<Entity>();
            cub_Country us = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "United States of America",
                cub_Alpha2 = "US"
            };
            data.Add(us);
            return data;
        }
    }
}
