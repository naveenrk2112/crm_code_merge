/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsContactChangePasswordTest
    {
        [TestMethod]
        public void ContactChangePasswordBadDataTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string contactId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                JsonModels.WSPasswordRequest changePasswordData = null;

                global.ContactChangePassword(changePasswordData, customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }

        [TestMethod]
        public void ContactChangePasswordNullCustomerIdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = null;
                string contactId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                JsonModels.WSPasswordRequest changePasswordData = new JsonModels.WSPasswordRequest();

                global.ContactChangePassword(changePasswordData, customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }

        [TestMethod]
        public void ContactChangePasswordBadCustomerIdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDZ";
                string contactId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                JsonModels.WSPasswordRequest changePasswordData = new JsonModels.WSPasswordRequest();

                global.ContactChangePassword(changePasswordData, customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }


        [TestMethod]
        public void ContactChangePasswordNullContactIdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                string contactId = null;
                JsonModels.WSPasswordRequest changePasswordData = new JsonModels.WSPasswordRequest();

                global.ContactChangePassword(changePasswordData, customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "ContactId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_CONTACT_NOT_FOUND);
            }
        }

        [TestMethod]
        public void ContactChangePasswordBadContactIdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string contactId = "7F754EAB-9545-4EEF-8063-A53453DD2FDZ";
                JsonModels.WSPasswordRequest changePasswordData = new JsonModels.WSPasswordRequest();

                global.ContactChangePassword(changePasswordData, customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "ContactId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_CONTACT_NOT_FOUND);
            }
        }

        [TestMethod]
        public void ContactChangePasswordNullOldPwdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string contactId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                JsonModels.WSPasswordRequest changePasswordData = new JsonModels.WSPasswordRequest();

                global.ContactChangePassword(changePasswordData, customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "Old");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }
        }

        [TestMethod]
        public void ContactChangePasswordNullNewPwdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string contactId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                JsonModels.WSPasswordRequest changePasswordData = new JsonModels.WSPasswordRequest();
                changePasswordData.oldpassword = "OldPassword";

                global.ContactChangePassword(changePasswordData, customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "New");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_VALUE_REQUIRED);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_FIELD_IS_BLANK);
            }
        }

        [TestMethod]
        public void ContactChangePasswordNewPwdTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string contactId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                JsonModels.WSPasswordRequest changePasswordData = new JsonModels.WSPasswordRequest();
                changePasswordData.oldpassword = "OldPassword";
                changePasswordData.newpassword = "NewPas$w0rd";

                global.ContactChangePassword(changePasswordData, customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "contact");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_CONTACT_NOT_FOUND);
            }
        }

        [TestMethod]
        public void ContactChangePasswordNullTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);
                string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
                string contactId = "7F754EAB-9545-4EEF-8063-A53453DD2FDB";
                JsonModels.WSPasswordRequest changePasswordData = new JsonModels.WSPasswordRequest();
                changePasswordData.oldpassword = "OldPassword";
                changePasswordData.newpassword = "NewPas$w0rd";
                global.ConnectionInfo = null;

                global.ContactChangePassword(changePasswordData, customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "MSD");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, "CUB.MSD.WCF.Library");
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "Object reference not set to an instance of an object.");
            }
        }
    }
}
