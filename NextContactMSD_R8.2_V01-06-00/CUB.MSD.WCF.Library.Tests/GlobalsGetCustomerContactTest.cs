/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using System.ServiceModel;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGetCustomerContactTest
    {
        [TestMethod]
        public void GetCustomerContactSuccessTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            Account acc = new Account();
            acc.AccountId = Guid.NewGuid();
            acc.CustomerTypeCode = (object)"Individual";
            acc.cub_AccountTypeId = new EntityReference();
            acc.cub_AccountTypeId.Id = Guid.NewGuid();
            acc.StatusCode = account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = acc.cub_AccountTypeId.Id;

            cub_ContactType cct = new cub_ContactType();
            cct.Id = Guid.NewGuid();
            cct.cub_Name = "CubName";

            cub_StateOrProvince csp = new cub_StateOrProvince();
            csp.cub_StateOrProvinceId = Guid.NewGuid();
            csp.cub_StateName = "Tennessee";

            Contact con = new Contact();
            con.ContactId = Guid.NewGuid();
            con.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };
            con.cub_ContactTypeId = new EntityReference { LogicalName = cub_ContactType.EntityLogicalName, Id = cct.Id };
            con.Salutation = "Mr.";
            con.FirstName = "Cubic";
            con.LastName = "Tester";
            con.MiddleName = "W";
            con.Telephone1 = "9314541408";
            con.Telephone2 = "9314541409";
            con.Telephone3 = "9314541410";
            con.cub_Phone1Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880003);
            con.cub_Phone2Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880001);
            con.cub_Phone3Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880000);
            con.cub_AddressId = new EntityReference { LogicalName = cub_Address.EntityLogicalName, Id = Guid.NewGuid() };
            con.Suffix = "Jr.";
            con.EMailAddress1 = "MrTester@cubic.com";
            con.BirthDate = DateTime.Now;
            con.cub_PersonalIdentifier = "Drivers License";
            con.cub_PersonalIdentifierType = "1";
            con.StatusCode = contact_statuscode.Active;

            cub_Address ca = new cub_Address();
            ca.cub_AddressId = con.cub_AddressId.Id;
            ca.cub_AccountAddressId = new EntityReference();
            ca.cub_AccountAddressId.Id = (Guid)acc.AccountId;
            ca.cub_Name = "street";
            ca.cub_Street1 = "1308 South Wsahington Street";
            ca.cub_Street2 = "Building 2, Cubicle - The Dude";
            ca.cub_City = "Tullahoma";
            ca.cub_StateProvinceId = new EntityReference();
            ca.cub_StateProvinceId.Name = "TN";
            ca.cub_PostalCodeId = new EntityReference();
            ca.cub_PostalCodeId.Name = "37388";
            ca.cub_CountryId = new EntityReference();
            ca.cub_CountryId.Id = Guid.NewGuid();

            cub_Country cctry = new cub_Country();
            cctry.cub_CountryId = ca.cub_AddressId;
            cctry.cub_Alpha2 = "US";

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_Username = "the_user_name";
            cc.cub_Pin = "1234";
            cc.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };

            cub_SecurityQuestion csq = new cub_SecurityQuestion();
            csq.cub_SecurityQuestionId = Guid.NewGuid();
            csq.cub_SecurityQuestionDesc = "What is the answer to the ultimate question?";

            cub_SecurityAnswer csa = new cub_SecurityAnswer();
            csa.cub_SecurityAnswerId = Guid.NewGuid();
            csa.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };
            csa.cub_securityquestion_securityanswer = csq;
            csa.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq.cub_SecurityQuestionId };
            csa.cub_SecurityAnswerDesc = "42";

            data.Add(acc);
            data.Add(cat);
            data.Add(cct);
            data.Add(cat);
            data.Add(csp);
            data.Add(cctry);
            data.Add(ca);
            data.Add(con);
            data.Add(cc);
            data.Add(csq);
            data.Add(csa);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = acc.AccountId.Value.ToString();
            string contactId = con.ContactId.Value.ToString();

            JsonModels.WSCustomerContactInfo custResponse = global.GetCustomerContact(customerId, contactId);

            Assert.AreEqual(custResponse.contactId, con.ContactId.ToString().ToUpper());
            Assert.IsNotNull(custResponse.address);
            Assert.AreEqual(custResponse.address.addressId, ca.cub_AddressId.ToString().ToUpper());
            Assert.AreEqual(custResponse.contactType, con.cub_ContactTypeId.Name);
            Assert.IsNotNull(custResponse.dateOfBirth);
            Assert.AreEqual(custResponse.email, con.EMailAddress1);
            Assert.IsNotNull(custResponse.name);
            if (custResponse.name != null)
            {
                Assert.AreEqual(custResponse.name.firstName, con.FirstName);
                Assert.AreEqual(custResponse.name.lastName, con.LastName);
                Assert.AreEqual(custResponse.name.middleInitial, con.MiddleName);
                Assert.AreEqual(custResponse.name.nameSuffix, con.Suffix);
                Assert.AreEqual(custResponse.name.title, con.Salutation);
            }
            //Assert.AreEqual(custResponse.personalIdentifierInfo.personalIdentifier, con.cub_personalidentifier);
            //Assert.AreEqual(custResponse.personalIdentifierInfo.personalIdentifierType, con.cub_personalidentifiertype);
            Assert.IsNotNull(custResponse.phone);
            if (custResponse.phone != null)
            {
                Assert.AreEqual(custResponse.phone[0].number, con.Telephone1);
                Assert.AreEqual(custResponse.phone[0].type, ((cub_phonetype)((OptionSetValue)con.cub_Phone1Type).Value).ToString("G"));
                Assert.AreEqual(custResponse.phone[1].number, con.Telephone2);
                Assert.AreEqual(custResponse.phone[1].type, ((cub_phonetype)((OptionSetValue)con.cub_Phone2Type).Value).ToString("G"));
                Assert.AreEqual(custResponse.phone[2].number, con.Telephone3);
                Assert.AreEqual(custResponse.phone[2].type, ((cub_phonetype)((OptionSetValue)con.cub_Phone3Type).Value).ToString("G"));
            }
            Assert.AreEqual(custResponse.pin, cc.cub_Pin);
            Assert.IsNotNull(custResponse.securityQAs);
            if (custResponse.securityQAs != null)
            {
                Assert.AreEqual(custResponse.securityQAs[0].securityAnswer, csa.cub_SecurityAnswerDesc);
                Assert.AreEqual(custResponse.securityQAs[0].securityQuestion, csq.cub_SecurityQuestionDesc);
            }
            Assert.AreEqual(custResponse.username, cc.cub_Username);
        }

        [TestMethod]
        public void GetCustomerContactNullCustomer()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = null;
            string contactId = null;

            try
            {
                JsonModels.WSCustomerContactInfo custResponse = global.GetCustomerContact(customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }

        [TestMethod]
        public void GetCustomerContactBadCustomerId()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDZ";
            string contactId = null;

            try
            {
                JsonModels.WSCustomerContactInfo custResponse = global.GetCustomerContact(customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }

        [TestMethod]
        public void GetCustomerContactNullContact()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
            string contactId = null;

            try
            {
                JsonModels.WSCustomerContactInfo custResponse = global.GetCustomerContact(customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "ContactId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The contact not found");
            }
        }

        [TestMethod]
        public void GetCustomerContactBadContactId()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDA";
            string contactId = "7F754EAB-9545-4EEF-8063-A53453DD2FDZ";

            try
            {
                JsonModels.WSCustomerContactInfo custResponse = global.GetCustomerContact(customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "ContactId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The contact not found");
            }
        }

        [TestMethod]
        public void GetCustomerContactContactNotFound()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            Account acc = new Account();
            acc.AccountId = Guid.NewGuid();
            acc.CustomerTypeCode = (object)"Individual";
            acc.cub_AccountTypeId = new EntityReference();
            acc.cub_AccountTypeId.Id = Guid.NewGuid();
            acc.StatusCode = account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = acc.cub_AccountTypeId.Id;

            cub_ContactType cct = new cub_ContactType();
            cct.Id = Guid.NewGuid();
            cct.cub_Name = "CubName";

            cub_StateOrProvince csp = new cub_StateOrProvince();
            csp.cub_StateOrProvinceId = Guid.NewGuid();
            csp.cub_StateName = "Tennessee";

            cub_Address ca = new cub_Address();
            ca.cub_AddressId = Guid.NewGuid();
            ca.cub_AccountAddressId = new EntityReference();
            ca.cub_AccountAddressId.Id = (Guid)acc.AccountId;
            ca.cub_Name = "street";
            ca.cub_Street1 = "1308 South Wsahington Street";
            ca.cub_Street2 = "Building 2, Cubicle - The Dude";
            ca.cub_City = "Tullahoma";
            ca.cub_StateProvinceId = new EntityReference();
            ca.cub_StateProvinceId.Name = "TN";
            ca.cub_PostalCodeId = new EntityReference();
            ca.cub_PostalCodeId.Name = "37388";
            ca.cub_CountryId = new EntityReference();
            ca.cub_CountryId.Id = Guid.NewGuid();

            cub_Country cctry = new cub_Country();
            cctry.cub_CountryId = ca.cub_AddressId;
            cctry.cub_Alpha2 = "US";

            Contact con = new Contact();
            con.ContactId = Guid.NewGuid();
            con.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };
            con.cub_ContactTypeId = new EntityReference { LogicalName = cub_ContactType.EntityLogicalName, Id = cct.Id };
            con.Salutation = "Mr.";
            con.FirstName = "Cubic";
            con.LastName = "Tester";
            con.MiddleName = "W";
            con.Telephone1 = "9314541408";
            con.Telephone2 = "9314541409";
            con.Telephone3 = "9314541410";
            con.cub_Phone1Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880003);
            con.cub_Phone2Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880001);
            con.cub_Phone3Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880000);
            con.cub_AddressId = new EntityReference { LogicalName = cub_Address.EntityLogicalName, Id = (Guid)ca.cub_AddressId };
            con.Suffix = "Jr.";
            con.EMailAddress1 = "MrTester@cubic.com";
            con.BirthDate = DateTime.Now;
            con.cub_PersonalIdentifier = "Drivers License";
            con.cub_PersonalIdentifierType = "1";
            con.StatusCode = contact_statuscode.Active;

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_Username = "the_user_name";
            cc.cub_Pin = "1234";
            cc.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };

            cub_SecurityQuestion csq = new cub_SecurityQuestion();
            csq.cub_SecurityQuestionId = Guid.NewGuid();
            csq.cub_SecurityQuestionDesc = "What is the answer to the ultimate question?";

            cub_SecurityAnswer csa = new cub_SecurityAnswer();
            csa.cub_SecurityAnswerId = Guid.NewGuid();
            csa.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };
            csa.cub_securityquestion_securityanswer = csq;
            csa.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq.cub_SecurityQuestionId };
            csa.cub_SecurityAnswerDesc = "42";

            data.Add(acc);
            data.Add(cat);
            data.Add(cct);
            data.Add(cat);
            data.Add(csp);
            data.Add(cctry);
            data.Add(ca);
            data.Add(con);
            data.Add(cc);
            data.Add(csq);
            data.Add(csa);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = acc.AccountId.Value.ToString();
            string contactId = con.ContactId.Value.ToString();

            try
            {
                JsonModels.WSCustomerContactInfo custResponse = global.GetCustomerContact(customerId, contactId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "ContactId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The contact not found");
            }
        }

        [TestMethod]
        public void CreateCustomerContactHappyPath()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            Guid customerId = Guid.NewGuid();
            fakedContext.Initialize(PrepareMSDData(customerId));
            var fakedService = fakedContext.GetOrganizationService();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);                     
            Model.MSDApi.WSCustomerContactRequest request = PrepareRequest();
            Guid? guid = global.CreateCustomerContact(request.contact.customerId.Value.ToString(), request);
            Assert.IsNotNull(guid);
        }

        private IEnumerable<Entity> PrepareMSDData(Guid customerId)
        {
            List<Entity> data = new List<Entity>();

            /*
             * User Vaidation Globals
             */
            var usernameValidationGlobal = new cub_Globals
            {
                cub_GlobalsId = Guid.NewGuid(),
                cub_Name = "UsernameValidation",
                cub_Enabled = true
            };
            data.Add(usernameValidationGlobal);
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = usernameValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UsernameMinLength",
                cub_AttributeValue = "5",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = usernameValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UsernameMaxLength",
                cub_AttributeValue = "50",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = usernameValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UsernameRequireEmail",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });

            /*
             * Password Globals
             */
            var passwordValidationGlobal = new cub_Globals
            {
                cub_GlobalsId = Guid.NewGuid(),
                cub_Name = "PasswordValidation",
                cub_Enabled = true
            };
            data.Add(passwordValidationGlobal);
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = passwordValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "NumberOfPasswordsToArchive",
                cub_AttributeValue = "5",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = passwordValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordLowerAlpha",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = passwordValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordMaxLength",
                cub_AttributeValue = "64",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = passwordValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordMinLength",
                cub_AttributeValue = "8",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = passwordValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordNonAlpha",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = passwordValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordNumeric",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = passwordValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordUpperAlpha",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = passwordValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TempPasswordDuartionDays",
                cub_AttributeValue = "2",
                cub_Enabled = true
            });

            /*
             * PIN Validation Globals
             */
            var pinValidationGlobal = new cub_Globals
            {
                cub_GlobalsId = Guid.NewGuid(),
                cub_Name = "PinValidation",
                cub_Enabled = true
            };
            data.Add(pinValidationGlobal);
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = pinValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PinMaxLength",
                cub_AttributeValue = "4",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = pinValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PinMinLength",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = pinValidationGlobal.ToEntityReference(),
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PinNumericOnly",
                cub_AttributeValue = "1",
                cub_Enabled = true
            });

            /*
             * Phone Tyoe OptionSet Translations
             */
            var optionSetTranslationPhoneType = new cub_OptionSetTranslation
            {
                cub_OptionSetTranslationId = Guid.NewGuid(),
                cub_OptionSetName = "cub_phonetype"
            };
            data.Add(optionSetTranslationPhoneType);
            data.Add(new cub_OptionSetTranslationDetail {
                cub_OptionSetTranslationId = optionSetTranslationPhoneType.ToEntityReference(),
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_Translation = "M",
                cub_OptionSetValue = 971880001
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationId = optionSetTranslationPhoneType.ToEntityReference(),
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_Translation = "W",
                cub_OptionSetValue = 971880003
            });

            /*
             * Personal Identifier Type Option Set Translation
             */
            var optionSetTranslationPersonalIdentifier = new cub_OptionSetTranslation
            {
                cub_OptionSetTranslationId = Guid.NewGuid(),
                cub_OptionSetName = "Contactcub_PersonalIdentifierType"
            };
            data.Add(optionSetTranslationPersonalIdentifier);
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationId = optionSetTranslationPersonalIdentifier.ToEntityReference(),
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_Translation = "DriversLicense",
                cub_OptionSetValue = 971880000
            });
            data.Add(new cub_ContactType {
                    cub_ContactTypeId = Guid.NewGuid(),
                    cub_Name = "Primary"
            });

            /*
             * Security Questions
             */
            data.Add(new cub_SecurityQuestion {
                 cub_SecurityQuestionId = Guid.NewGuid(),
                 cub_SecurityQuestionDesc = "What is your favorite color?"
            });
            data.Add(new cub_SecurityQuestion
            {
                cub_SecurityQuestionId = Guid.NewGuid(),
                cub_SecurityQuestionDesc = "Have you ever seen the rain?"
            });

            // Country
            var country = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "USA",
                cub_Alpha2 = "US",
                cub_CountryCode = "99"
            };
            data.Add(country);

            // State/Province
            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = country.ToEntityReference(),
                cub_StateName = "California",
                cub_Abbreviation = "CA"
            };
            data.Add(state);

            // ZipOrPostal Code
            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "92008",
                cub_City = "San Diego",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };
            data.Add(zip);
            return data;
        }

        private Model.MSDApi.WSCustomerContactRequest PrepareRequest()
        {
            Model.MSDApi.WSCustomerContactRequest data = new Model.MSDApi.WSCustomerContactRequest();
            data.contact = new Model.MSDApi.WSCustomerContact();
            data.contact.customerId = Guid.NewGuid();
            data.contact.address = new Model.MSDApi.WSAddress {
                address1 = "301-5955 Birney Av.",
                city = "San Diego",
                country = "US",
                postalCode = "92008",
                state = "California"
            };
            data.contact.contactType = "Primary";
            data.contact.dateOfBirth = "1980-05-01T00:00:00.0000000Z";
            data.contact.email = "email@gmail.com";
            data.contact.name = new Model.MSDApi.WSName
            {
                firstName = "Jose",
                lastName = "Santos",
            };
            data.contact.personalIdentifierInfo = new Model.MSDApi.WSPersonalIdentifier
            {
                personalIdentifierType = "DriversLicense",
                personalIdentifier = "123456"
            };
            data.contact.phone = new Model.MSDApi.WSPhone[2];
            data.contact.phone[0] = new Model.MSDApi.WSPhone
            {
                type = "M",
                number = "7777777777"
            };
            data.contact.phone[1] = new Model.MSDApi.WSPhone
            {
                type = "W",
                number = "8888888888"
            };
            data.contact.pin = "3210";
            data.contact.securityQAs = new Model.MSDApi.WSSecurityQA[2];
            data.contact.securityQAs[0] = new Model.MSDApi.WSSecurityQA
            {
                securityQuestion = "What is your favorite color?",
                securityAnswer = "Turquoise"
            };
            data.contact.securityQAs[1] = new Model.MSDApi.WSSecurityQA
            {
                securityQuestion = "Have you ever seen the rain?",
                securityAnswer = "Yes"
            };
            data.contact.username = "email@gmail.com";
            data.contact.password = "Defa$lt1";
            return data;
        }
    }
}
