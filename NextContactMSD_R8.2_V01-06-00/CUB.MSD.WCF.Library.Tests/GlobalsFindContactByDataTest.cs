/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;
using CUB.MSD.UnitTest.CUB.MSD.Logic;
using System.Net.Http;
using System.Net;
using System.ServiceModel;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsFindContactByDataTest
    {
        [TestMethod]
        public void FindContactByDataFullCallTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            cub_AccountType accountType = new cub_AccountType();
            accountType.cub_AccountTypeId = Guid.NewGuid();
            accountType.cub_AccountName = "Personal";

            Account account = new Account();
            account.cub_AccountTypeId = accountType.ToEntityReference();
            account.AccountId = Guid.NewGuid();
            account.StatusCode = account_statuscode.Active;

            var country = new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_CountryName = "USA",
                cub_CountryCode = "99"
            };

            var state = new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_Country = country.ToEntityReference(),
                cub_StateName = "Tennessee",
                cub_Abbreviation = "TN"
            };

            var zip = new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "37388",
                cub_City = "Tullahoma",
                cub_State = state.ToEntityReference(),
                cub_Country = country.ToEntityReference()
            };

            var address = new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_Street1 = "1308 South Wsahington Street",
                cub_Street2 = "Building 2, Cubicle - The Dude",
                cub_City = "Tullahoma",
                cub_CountryId = country.ToEntityReference(),
                cub_StateProvinceId = state.ToEntityReference(),
                cub_PostalCodeId = new EntityReference(zip.LogicalName, zip.cub_ZipOrPostalCodeId.Value),
                cub_AccountAddressId = account.ToEntityReference()
            };

            Contact contact = new Contact();
            contact.ContactId = credentials.cub_ContactId.Id;
            contact.ParentCustomerId = account.ToEntityReference();
            contact.EMailAddress1 = "TheDude@Cubic.com";
            contact.FirstName = "The";
            contact.LastName = "Dude";
            contact.Telephone1 = "9314546408";
            contact.cub_AddressId = address.ToEntityReference();

            data.Add(credentials);
            data.Add(accountType);
            data.Add(account);
            data.Add(country);
            data.Add(state);
            data.Add(zip);
            data.Add(address);
            data.Add(contact);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSForgotUsername contactData = new JsonModels.WSForgotUsername();
            contactData.email = "TheDude@Cubic.com";
            contactData.firstName = "The";
            contactData.lastName = "Dude";
            contactData.phone = "9314546409";
            contactData.pin ="5432";
            contactData.postalCode = "37389";

            NisAPIGlobalsHelper.GetNisApiGlobals(data);
            fakedContext.Initialize(data);
            NISApiLogic logic = new NISApiLogic(fakedService);

            try
            {
                global.FindContactByData(contactData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                Assert.AreEqual(e.Detail.fieldName, "Account");
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT);
            }
        }

        [TestMethod]
        public void FindContactByDataFailContactDataIsNullTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            JsonModels.WSForgotUsername contactData = null;
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            try
            {
               global.FindContactByData(contactData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                Assert.AreEqual(e.Detail.fieldName, "Email/Phone");
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_EMAIL_OR_PHONE_REQUIRED);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_EMAIL_OR_PHONE_REQUIRED);
            }
        }

        [TestMethod]
        public void FindContactByDataFailEmailOrPhoneRequiredTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            JsonModels.WSForgotUsername contactData = new JsonModels.WSForgotUsername();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            contactData.email = "";
            contactData.firstName = "The";
            contactData.lastName = "Dude";
            contactData.phone = "";
            contactData.pin = "1234";
            contactData.postalCode = "37388";

            try
            {
                global.FindContactByData(contactData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                Assert.AreEqual(e.Detail.fieldName, "Email/Phone");
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_EMAIL_OR_PHONE_REQUIRED);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_EMAIL_OR_PHONE_REQUIRED);
            }
        }

        [TestMethod]
        public void FindContactByDataFailFirstNameTooLongTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            JsonModels.WSForgotUsername contactData = new JsonModels.WSForgotUsername();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            contactData.email = "TheDude@Cubic.com";
            contactData.firstName = "Hereiswherewehavetohaveareallyreallysuperduperverylongfirstname";
            contactData.lastName = "Dude";
            contactData.phone = "9314546408";
            contactData.pin = "1234";
            contactData.postalCode = "37388";

            try
            {
                global.FindContactByData(contactData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                Assert.AreEqual(e.Detail.fieldName, "");
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT);
            }
        }

        [TestMethod]
        public void FindContactByDataFailLastNameTooLongTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            JsonModels.WSForgotUsername contactData = new JsonModels.WSForgotUsername();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            contactData.email = "TheDude@Cubic.com";
            contactData.firstName = "The";
            contactData.lastName = "Hereiswherewehavetohaveareallyreallysuperduperverylonglastname";
            contactData.phone = "9314546408";
            contactData.pin = "1234";
            contactData.postalCode = "37388";

            try
            {
                global.FindContactByData(contactData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                //Assert.AreEqual(e.Detail.fieldName, "");
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT);
            }
        }

        [TestMethod]
        public void FindContactByDataFailPINTooLongTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            JsonModels.WSForgotUsername contactData = new JsonModels.WSForgotUsername();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            contactData.email = "TheDude@Cubic.com";
            contactData.firstName = "The";
            contactData.lastName = "Dude";
            contactData.phone = "9314546408";
            contactData.pin = "123456789012345679";
            contactData.postalCode = "37388";

            try
            {
                global.FindContactByData(contactData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                //Assert.AreEqual(e.Detail.fieldName, "");
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT);
            }
        }

        [TestMethod]
        public void FindContactByDataFailPostalCodeTooLongTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            JsonModels.WSForgotUsername contactData = new JsonModels.WSForgotUsername();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            contactData.email = "TheDude@Cubic.com";
            contactData.firstName = "The";
            contactData.lastName = "Dude";
            contactData.phone = "9314546408";
            contactData.pin = "1234";
            contactData.postalCode = "37388-78282";

            try
            {
                global.FindContactByData(contactData);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                //Assert.AreEqual(e.Detail.fieldName, "");
                Assert.AreEqual(e.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(e.Detail.errorMessage, CUBConstants.Error.MSG_UNABLE_FIND_ACCOUNT);
            }
        }
    }
}
