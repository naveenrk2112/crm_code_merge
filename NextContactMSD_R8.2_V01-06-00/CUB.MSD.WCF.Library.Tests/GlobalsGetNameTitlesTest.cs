/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using System;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGetNameTitlesTest
    {
        [TestMethod]
        public void GetNameTitlesTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            fakedContext.Initialize(PrepareData());
            var fakedService = fakedContext.GetOrganizationService();
            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            var nameTitles = global.GetClientNameTitles();
            Assert.AreNotSame(0, nameTitles.nameTitles.Count);
            Assert.AreEqual(nameTitles.nameTitles[0].key, "Mr");
            Assert.AreEqual(nameTitles.nameTitles[1].key, "Mrs");
            Assert.AreEqual(nameTitles.nameTitles[2].key, "Miss");
            Assert.AreEqual(nameTitles.nameTitles[3].key, "Ms");
            Assert.AreEqual(nameTitles.nameTitles[4].key, "Dr");
        }

        private static List<Entity> PrepareData()
        {
            List<Entity> data = new List<Entity>();
            Guid id = Guid.NewGuid();
            data.Add(new cub_OptionSetTranslation { cub_OptionSetTranslationId = id, cub_OptionSetName = "cub_nametitles" });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, id),
                cub_Translation = "Mr",
                cub_OptionSetValue = 971880000
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, id),
                cub_Translation = "Mrs",
                cub_OptionSetValue = 971880001
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, id),
                cub_Translation = "Miss",
                cub_OptionSetValue = 971880002
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, id),
                cub_Translation = "Ms",
                cub_OptionSetValue = 971880003
            });
            data.Add(new cub_OptionSetTranslationDetail
            {
                cub_OptionSetTranslationDetailId = Guid.NewGuid(),
                cub_OptionSetTranslationId = new EntityReference(cub_OptionSetTranslation.EntityLogicalName, id),
                cub_Translation = "Dr",
                cub_OptionSetValue = 971880004
            });
            return data;
        }
    }
}
