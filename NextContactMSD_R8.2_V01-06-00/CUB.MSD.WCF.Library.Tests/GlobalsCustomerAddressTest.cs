/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using System.ServiceModel;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsCustomerAddressTest
    {
        [TestMethod]
        public void GetCustomerAddress()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Address add = new cub_Address();
            add.cub_AddressId = Guid.NewGuid();
            add.cub_CountryId = new EntityReference(); 
            add.cub_CountryId.Id = Guid.NewGuid();
            add.cub_AccountAddressId = new EntityReference();
            add.cub_AccountAddressId.Id = Guid.NewGuid();
            add.cub_Street1 = "1308 South Wsahington Street";
            add.cub_Street2 = "Building 2, Cubicle - The Dude";
            add.cub_City = "Tullahoma";
            add.cub_StateProvinceId = new EntityReference();
            add.cub_StateProvinceId.Name = "TN";
            add.cub_PostalCodeId = new EntityReference();
            add.cub_PostalCodeId.Name = "37388";

            cub_Country country = new cub_Country();
            country.cub_CountryId = add.cub_CountryId.Id;
            country.cub_Alpha2 = "US";

            string customerId = add.cub_AccountAddressId.Id.ToString();
            string addressId = add.cub_AddressId.ToString();

            data.Add(add);
            data.Add(country);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            Model.NIS.WSAddress addResponse = global.GetCustomerAddress(customerId, addressId);

            Assert.AreEqual(addResponse.address1, add.cub_Street1);
            Assert.AreEqual(addResponse.address2, add.cub_Street2);
            Assert.AreEqual(addResponse.city, add.cub_City);
            Assert.AreEqual(addResponse.state, add.cub_StateProvinceId.Name);
            Assert.AreEqual(addResponse.country, country.cub_Alpha2);
            Assert.AreEqual(addResponse.postalCode, add.cub_PostalCodeId.Name);
        }

        [TestMethod]
        public void GetCustomerAddressNullCustomer()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Address add = new cub_Address();
            add.cub_AddressId = Guid.NewGuid();
            add.cub_CountryId = new EntityReference();
            add.cub_CountryId.Id = Guid.NewGuid();
            add.cub_AccountAddressId = new EntityReference();
            add.cub_AccountAddressId.Id = Guid.NewGuid();
            add.cub_Street1 = "1308 South Wsahington Street";
            add.cub_Street2 = "Building 2, Cubicle - The Dude";
            add.cub_City = "Tullahoma";
            add.cub_StateProvinceId = new EntityReference();
            add.cub_StateProvinceId.Name = "TN";
            add.cub_PostalCodeId = new EntityReference();
            add.cub_PostalCodeId.Name = "37388";

            cub_Country country = new cub_Country();
            country.cub_CountryId = add.cub_CountryId.Id;
            country.cub_Alpha2 = "US";

            string customerId = null;
            string addressId = add.cub_AddressId.ToString();

            data.Add(add);
            data.Add(country);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            try
            {
                Model.NIS.WSAddress addResponse = global.GetCustomerAddress(customerId, addressId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, "errors.general.value.required");
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "CustomerId is null");
            }
        }

        [TestMethod]
        public void GetCustomerAddressCustomerIdNotGuid()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Address add = new cub_Address();
            add.cub_AddressId = Guid.NewGuid();
            add.cub_CountryId = new EntityReference();
            add.cub_CountryId.Id = Guid.NewGuid();
            add.cub_AccountAddressId = new EntityReference();
            add.cub_AccountAddressId.Id = Guid.NewGuid();
            add.cub_Street1 = "1308 South Wsahington Street";
            add.cub_Street2 = "Building 2, Cubicle - The Dude";
            add.cub_City = "Tullahoma";
            add.cub_StateProvinceId = new EntityReference();
            add.cub_StateProvinceId.Name = "TN";
            add.cub_PostalCodeId = new EntityReference();
            add.cub_PostalCodeId.Name = "37388";

            cub_Country country = new cub_Country();
            country.cub_CountryId = add.cub_CountryId.Id;
            country.cub_Alpha2 = "US";

            string customerId = "W593A6CE-3929-40D1-A8D4-41B9C28F23A3";
            string addressId = add.cub_AddressId.ToString();

            data.Add(add);
            data.Add(country);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            try
            {
                Model.NIS.WSAddress addResponse = global.GetCustomerAddress(customerId, addressId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "Customer not found");
            }
        }

        [TestMethod]
        public void GetCustomerAddressNullAddressId()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Address add = new cub_Address();
            add.cub_AddressId = Guid.NewGuid();
            add.cub_CountryId = new EntityReference();
            add.cub_CountryId.Id = Guid.NewGuid();
            add.cub_AccountAddressId = new EntityReference();
            add.cub_AccountAddressId.Id = Guid.NewGuid();
            add.cub_Street1 = "1308 South Wsahington Street";
            add.cub_Street2 = "Building 2, Cubicle - The Dude";
            add.cub_City = "Tullahoma";
            add.cub_StateProvinceId = new EntityReference();
            add.cub_StateProvinceId.Name = "TN";
            add.cub_PostalCodeId = new EntityReference();
            add.cub_PostalCodeId.Name = "37388";

            cub_Country country = new cub_Country();
            country.cub_CountryId = add.cub_CountryId.Id;
            country.cub_Alpha2 = "US";

            string customerId = add.cub_AccountAddressId.Id.ToString();
            string addressId = null;

            data.Add(add);
            data.Add(country);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            try
            {
                Model.NIS.WSAddress addResponse = global.GetCustomerAddress(customerId, addressId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "AddressId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, "errors.general.value.required");
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "AddressId is null");
            }
        }

        [TestMethod]
        public void GetCustomerAddressAddressIdNotGuid()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Address add = new cub_Address();
            add.cub_AddressId = Guid.NewGuid();
            add.cub_CountryId = new EntityReference();
            add.cub_CountryId.Id = Guid.NewGuid();
            add.cub_AccountAddressId = new EntityReference();
            add.cub_AccountAddressId.Id = Guid.NewGuid();
            add.cub_Street1 = "1308 South Wsahington Street";
            add.cub_Street2 = "Building 2, Cubicle - The Dude";
            add.cub_City = "Tullahoma";
            add.cub_StateProvinceId = new EntityReference();
            add.cub_StateProvinceId.Name = "TN";
            add.cub_PostalCodeId = new EntityReference();
            add.cub_PostalCodeId.Name = "37388";

            cub_Country country = new cub_Country();
            country.cub_CountryId = add.cub_CountryId.Id;
            country.cub_Alpha2 = "US";

            string customerId = add.cub_AccountAddressId.Id.ToString();
            string addressId = "W593A6CE-3929-40D1-A8D4-41B9C28F23A3";

            data.Add(add);
            data.Add(country);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            try
            {
                Model.NIS.WSAddress addResponse = global.GetCustomerAddress(customerId, addressId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "AddressId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "Address not found");
            }
        }

        [TestMethod]
        public void GetCustomerAddressAddressNotFound()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Address add = new cub_Address();
            add.cub_AddressId = Guid.NewGuid();
            add.cub_CountryId = new EntityReference();
            add.cub_CountryId.Id = Guid.NewGuid();
            add.cub_AccountAddressId = new EntityReference();
            add.cub_AccountAddressId.Id = Guid.NewGuid();
            add.cub_Street1 = "1308 South Wsahington Street";
            add.cub_Street2 = "Building 2, Cubicle - The Dude";
            add.cub_City = "Tullahoma";
            add.cub_StateProvinceId = new EntityReference();
            add.cub_StateProvinceId.Name = "TN";
            add.cub_PostalCodeId = new EntityReference();
            add.cub_PostalCodeId.Name = "37388";

            cub_Country country = new cub_Country();
            country.cub_CountryId = add.cub_CountryId.Id;
            country.cub_Alpha2 = "US";

            string customerId = add.cub_AccountAddressId.Id.ToString();
            string addressId = Guid.NewGuid().ToString();

            data.Add(add);
            data.Add(country);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            try
            {
                Model.NIS.WSAddress addResponse = global.GetCustomerAddress(customerId, addressId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "AddressId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, CUBConstants.Error.MSG_ADDRESS_NOT_FOUND);
            }
        }
    }
}
