/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsCustomerAuthenticateTest
    {
        [TestMethod]
        public void CustomerAuthenticateSuccessTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            Contact c = new Contact();
            c.ContactId = credentials.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();
            
            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            a.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(credentials);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = credentials.cub_Username;
            creds.password = "testPassword";
            creds.customerType = cat.cub_AccountName;

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authCode, CUBConstants.Error.SUCCESS);
            Assert.IsNull(authResponse.authErrors);
            Assert.AreEqual(authResponse.contactId, c.ContactId.Value.ToString().ToUpper());
            Assert.AreEqual(authResponse.customerId, c.ParentCustomerId.Id.ToString().ToUpper());
            Assert.AreEqual(authResponse.customerType, cat.cub_AccountName);
        }

        [TestMethod]
        public void GlobalsCustomerAuthenticateCredentialsNullTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            Contact c = new Contact();
            c.ContactId = credentials.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();

            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            a.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(credentials);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(null);

            Assert.AreEqual(authResponse.authCode, CUBConstants.Error.FAILURE);
            Assert.IsNotNull(authResponse.authErrors);
            Assert.AreEqual(authResponse.authErrors[0].key, CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD);
            Assert.AreEqual(authResponse.authErrors[0].message, CUBConstants.Error.MSG_USERNAME_AND_PASSWORD_INVALID);
            Assert.IsNull(authResponse.contactId);
            Assert.IsNull(authResponse.customerId);
            Assert.IsNull(authResponse.customerType);
        }

        [TestMethod]
        public void CustomerAuthenticateContactNotFoundTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_ContactId = new EntityReference();
            cc.cub_ContactId.Id = Guid.NewGuid();
            cc.cub_Username = "testUsername";
            cc.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            cc.cub_LockoutDtm = null;
            cc.cub_ForceChangePassword = false;
            cc.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            Contact c = new Contact();
            c.ContactId = cc.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();

            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            a.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(cc);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = "testUsername";
            creds.password = "testWrongPassword";
            creds.customerType = cat.cub_AccountName;

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authCode, CUBConstants.Error.FAILURE);
            Assert.IsNotNull(authResponse.authErrors);
            Assert.AreEqual(authResponse.authErrors[0].key, CUBConstants.Error.ERR_INVALID_USER_OR_PASSWORD);
            Assert.AreEqual(authResponse.authErrors[0].message, CUBConstants.Error.MSG_USERNAME_AND_PASSWORD_INVALID);
            Assert.IsNull(authResponse.contactId);
            Assert.IsNull(authResponse.customerId);
            Assert.IsNull(authResponse.customerType);
        }

        [TestMethod]
        public void CustomerAuthenticateContactNoAccountTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_ContactId = new EntityReference();
            cc.cub_ContactId.Id = Guid.NewGuid();
            cc.cub_Username = "testUsername";
            cc.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            cc.cub_LockoutDtm = null;
            cc.cub_ForceChangePassword = false;
            cc.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            Contact c = new Contact();
            c.ContactId = cc.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();

            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            a.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = Guid.NewGuid();//a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(cc);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = cc.cub_Username;
            creds.password = "testPassword";

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authCode, CUBConstants.Error.SUCCESS);
            Assert.IsNull(authResponse.authErrors);
            Assert.AreEqual(authResponse.contactId, c.ContactId.Value.ToString().ToUpper());
            Assert.AreEqual(authResponse.customerId, c.ParentCustomerId.Id.ToString().ToUpper());
            Assert.IsNull(authResponse.customerType);
        }

        [TestMethod]
        public void CustomerAuthenticateStatusCodeNotActiveTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_ContactId = new EntityReference();
            cc.cub_ContactId.Id = Guid.NewGuid();
            cc.cub_Username = "testUsername";
            cc.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            cc.cub_LockoutDtm = null;
            cc.cub_ForceChangePassword = false;
            cc.statuscode = new OptionSetValue((int)cub_credential_statuscode.Inactive);

            Contact c = new Contact();
            c.ContactId = cc.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();

            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            OptionSetValue value = new OptionSetValue();
            value.Value = 2;
            a.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(cc);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = cc.cub_Username;
            creds.password = "testPassword";
            creds.customerType = cat.cub_AccountName;

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authCode, CUBConstants.Error.FAILURE);
            Assert.IsNotNull(authResponse.authErrors);
            Assert.AreEqual(authResponse.authErrors[0].key, CUBConstants.Error.ERR_ACCNT_INACTIVE);
            Assert.AreEqual(authResponse.authErrors[0].message, CUBConstants.Error.MSG_LOGIN_INACTIVE);
            Assert.IsNull(authResponse.contactId);
            Assert.IsNull(authResponse.customerId);
            Assert.IsNull(authResponse.customerType);
        }

        [TestMethod]
        public void CustomerAuthenticateLockedOutTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = DateTime.Now.AddDays(-2);
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Locked);

            Contact c = new Contact();
            c.ContactId = credentials.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();

            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            a.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(credentials);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = credentials.cub_Username;
            creds.password = "testPassword";
            creds.customerType = cat.cub_AccountName;

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authCode, CUBConstants.Error.FAILURE);
            Assert.IsNotNull(authResponse.authErrors);
            Assert.AreEqual(authResponse.authErrors[0].key, CUBConstants.Error.ERR_ACCOUNT_LOCKED);
            Assert.AreEqual(authResponse.authErrors[0].message, CUBConstants.Error.MSG_LOGIN_LOCKED);
            Assert.IsNull(authResponse.contactId);
            Assert.IsNull(authResponse.customerId);
            Assert.IsNull(authResponse.customerType);
        }

        //[TestMethod]
        //public void CustomerAuthenticateTempPwdExpiredTest()
        //{
        //    var fakedContext = new XrmFakedContext();
        //    fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
        //    var fakedService = fakedContext.GetOrganizationService();
        //    List<Entity> data = new List<Entity>();

        //    cub_credential cc = new cub_credential();
        //    cc.cub_credentialId = Guid.NewGuid();
        //    cc.cub_contactId = new EntityReference();
        //    cc.cub_contactId.Id = Guid.NewGuid();
        //    cc.cub_username = "testUsername";
        //    cc.cub_password = UtilityLogic.EncryptTripleDES("testPassword");
        //    cc.cub_lockoutdtm = null;
        //    cc.cub_forcechangepassword = false;
        //    //cc.cub_temppasswordexpdtm = (DateTime.Now).AddDays(-2);

        //    Contact c = new Contact();
        //    c.ContactId = cc.cub_contactId.Id;
        //    c.ParentCustomerId = new EntityReference();
        //    c.ParentCustomerId.Id = Guid.NewGuid();

        //    Account a = new Account();
        //    a.cub_AccountTypeId = new EntityReference();
        //    a.cub_AccountTypeId.Id = Guid.NewGuid();
        //    a.AccountId = c.ParentCustomerId.Id;
        //    OptionSetValue value = new OptionSetValue();
        //    value.Value = 1;
        //    a.StatusCode = value; //account_statuscode.Active;

        //    cub_accounttype cat = new cub_accounttype();
        //    cat.cub_accounttypeId = a.cub_AccountTypeId.Id;
        //    cat.cub_accountname = "Personal";

        //    data.Add(cc);
        //    data.Add(c);
        //    data.Add(a);
        //    data.Add(cat);
        //    fakedContext.Initialize(data);

        //    ConnectionInformation ci = new ConnectionInformation();
        //    ci.CrmService = fakedService;
        //    Globals global = new Globals(ci);
        //    JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
        //    creds.username = cc.cub_username;
        //    creds.password = "testPassword";
        //    creds.customerType = cat.cub_accountname;

        //    JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

        //    Assert.AreEqual(authResponse.authCode, "Failure");
        //    Assert.IsNotNull(authResponse.authErrors);
        //    Assert.AreEqual(authResponse.authErrors[0].key, "errors.password.is.expired");
        //    Assert.AreEqual(authResponse.authErrors[0].message, "Temporary password is expired");
        //    Assert.IsNull(authResponse.contactId);
        //    Assert.IsNull(authResponse.customerId);
        //    Assert.IsNull(authResponse.customerType);
        //}

        [TestMethod]
        public void CustomerAuthenticateForceChangePwdTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = true;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            Contact c = new Contact();
            c.ContactId = credentials.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();

            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            a.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(credentials);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = credentials.cub_Username;
            creds.password = "testPassword";
            creds.customerType = cat.cub_AccountName;

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authCode, CUBConstants.Error.SUCCESS_CHANGE_PASSWORD);
            Assert.IsNull(authResponse.authErrors);
            Assert.AreEqual(authResponse.contactId, c.ContactId.Value.ToString().ToUpper());
            Assert.AreEqual(authResponse.customerId, c.ParentCustomerId.Id.ToString().ToUpper());
            Assert.AreEqual(authResponse.customerType, cat.cub_AccountName);
        }

        [TestMethod]
        public void CustomerAuthenticateContactNoAccountAndLockedOutTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_ContactId = new EntityReference();
            cc.cub_ContactId.Id = Guid.NewGuid();
            cc.cub_Username = "testUsername";
            cc.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            cc.cub_LockoutDtm = (DateTime.Now).AddDays(-2);
            cc.cub_ForceChangePassword = false;
            cc.statuscode = new OptionSetValue((int)cub_credential_statuscode.Locked);

            Contact c = new Contact();
            c.ContactId = cc.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();

            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            a.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = Guid.NewGuid();//a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(cc);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = cc.cub_Username;
            creds.password = "testPassword";

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authCode, CUBConstants.Error.FAILURE);
            Assert.IsNotNull(authResponse.authErrors);
            Assert.AreEqual(authResponse.authErrors[0].key, CUBConstants.Error.ERR_ACCOUNT_LOCKED);
            Assert.AreEqual(authResponse.authErrors[0].message, CUBConstants.Error.MSG_LOGIN_LOCKED);
            Assert.IsNull(authResponse.contactId);
            Assert.IsNull(authResponse.customerId);
            Assert.IsNull(authResponse.customerType);
        }

        //[TestMethod]
        //public void CustomerAuthenticateContactNoAccountAndTempPwdExpiredTest()
        //{
        //    var fakedContext = new XrmFakedContext();
        //    fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
        //    var fakedService = fakedContext.GetOrganizationService();
        //    List<Entity> data = new List<Entity>();

        //    cub_credential cc = new cub_credential();
        //    cc.cub_credentialId = Guid.NewGuid();
        //    cc.cub_contactId = new EntityReference();
        //    cc.cub_contactId.Id = Guid.NewGuid();
        //    cc.cub_username = "testUsername";
        //    cc.cub_password = UtilityLogic.EncryptTripleDES("testPassword");
        //    cc.cub_lockoutdtm = null;
        //    cc.cub_forcechangepassword = false;
        //    //cc.cub_temppasswordexpdtm = (DateTime.Now).AddDays(-2);

        //    Contact c = new Contact();
        //    c.ContactId = cc.cub_contactId.Id;
        //    c.ParentCustomerId = new EntityReference();
        //    c.ParentCustomerId.Id = Guid.NewGuid();

        //    Account a = new Account();
        //    a.cub_AccountTypeId = new EntityReference();
        //    a.cub_AccountTypeId.Id = Guid.NewGuid();
        //    a.AccountId = c.ParentCustomerId.Id;
        //    OptionSetValue value = new OptionSetValue();
        //    value.Value = 1;
        //    a.StatusCode = value; //account_statuscode.Active;

        //    cub_accounttype cat = new cub_accounttype();
        //    cat.cub_accounttypeId = Guid.NewGuid();//a.cub_AccountTypeId.Id;
        //    cat.cub_accountname = "Personal";

        //    data.Add(cc);
        //    data.Add(c);
        //    data.Add(a);
        //    data.Add(cat);
        //    fakedContext.Initialize(data);

        //    ConnectionInformation ci = new ConnectionInformation();
        //    ci.CrmService = fakedService;
        //    Globals global = new Globals(ci);
        //    JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
        //    creds.username = cc.cub_username;
        //    creds.password = "testPassword";
        //    creds.customerType = cat.cub_accountname;

        //    JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

        //    Assert.AreEqual(authResponse.authCode, CUBConstants.Error.FAILURE);
        //    Assert.IsNotNull(authResponse.authErrors);
        //    Assert.AreEqual(authResponse.authErrors[0].key, "errors.password.is.expired");
        //    Assert.AreEqual(authResponse.authErrors[0].message, "Temporary password is expired");
        //    Assert.IsNull(authResponse.contactId);
        //    Assert.IsNull(authResponse.customerId);
        //    Assert.IsNull(authResponse.customerType);
        //}

        [TestMethod]
        public void CustomerAuthenticateContactNoAccountAndChangePwdTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_ContactId = new EntityReference();
            cc.cub_ContactId.Id = Guid.NewGuid();
            cc.cub_Username = "testUsername";
            cc.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            cc.cub_LockoutDtm = null;
            cc.cub_ForceChangePassword = true;
            cc.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            Contact c = new Contact();
            c.ContactId = cc.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();

            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            a.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = Guid.NewGuid();//a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(cc);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = cc.cub_Username;
            creds.password = "testPassword";

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authCode, CUBConstants.Error.SUCCESS_CHANGE_PASSWORD);
            Assert.IsNull(authResponse.authErrors);
            Assert.AreEqual(authResponse.contactId, c.ContactId.Value.ToString().ToUpper());
            Assert.AreEqual(authResponse.customerId, c.ParentCustomerId.Id.ToString().ToUpper());
            Assert.IsNull(authResponse.customerType);
        }

        [TestMethod]
        public void CustomerAuthenticateAccountClosedOrPedingClosed()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.Active);

            Contact c = new Contact();
            c.ContactId = credentials.cub_ContactId.Id;
            c.ParentCustomerId = new EntityReference();
            c.ParentCustomerId.Id = Guid.NewGuid();

            Account a = new Account();
            a.cub_AccountTypeId = new EntityReference();
            a.cub_AccountTypeId.Id = Guid.NewGuid();
            a.AccountId = c.ParentCustomerId.Id;
            a.StatusCode = new OptionSetValue((int)account_statuscode.PendingClose);

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = a.cub_AccountTypeId.Id;
            cat.cub_AccountName = "Personal";

            data.Add(credentials);
            data.Add(c);
            data.Add(a);
            data.Add(cat);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = credentials.cub_Username;
            creds.password = "testPassword";
            creds.customerType = cat.cub_AccountName;

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authErrors[0].key, CUBConstants.Error.ERR_ACCNT_CLOSED_OR_PENDING_CLOSURE);
            Assert.AreEqual(authResponse.authErrors[0].message, CUBConstants.Error.MSG_ACCNT_CLOSED_OR_PENDING);
        }

        [TestMethod]
        public void CustomerAuthenticateTemporaryPasswordExpired()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            cub_Credential credentials = new cub_Credential();
            credentials.cub_CredentialId = Guid.NewGuid();
            credentials.cub_ContactId = new EntityReference();
            credentials.cub_ContactId.Id = Guid.NewGuid();
            credentials.cub_Username = "theDude";
            credentials.cub_Password = UtilityLogic.EncryptTripleDES("testPassword");
            credentials.cub_LockoutDtm = null;
            credentials.cub_Pin = "1234";
            credentials.cub_ForceChangePassword = false;
            credentials.statuscode = new OptionSetValue((int)cub_credential_statuscode.PendingActivation);

            cub_VerificationToken verificationToken = new cub_VerificationToken();
            verificationToken.cub_VerificationTokenId = Guid.NewGuid();
            verificationToken.cub_CredentialsId = credentials.ToEntityReference();
            verificationToken.cub_TokenExpirationDate = DateTime.Today.AddDays(-1);
            verificationToken.statuscode = new OptionSetValue((int)cub_verificationtoken_statuscode.Active);

            Contact contact = new Contact();
            contact.ContactId = credentials.cub_ContactId.Id;
            contact.ParentCustomerId = new EntityReference();
            contact.ParentCustomerId.Id = Guid.NewGuid();

            Account account = new Account();
            account.cub_AccountTypeId = new EntityReference();
            account.cub_AccountTypeId.Id = Guid.NewGuid();
            account.AccountId = contact.ParentCustomerId.Id;
            account.StatusCode = new OptionSetValue((int)account_statuscode.Active);

            cub_AccountType accountType = new cub_AccountType();
            accountType.cub_AccountTypeId = account.cub_AccountTypeId.Id;
            accountType.cub_AccountName = "Personal";

            data.Add(credentials);
            data.Add(verificationToken);
            data.Add(contact);
            data.Add(account);
            data.Add(accountType);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            JsonModels.WSAuthenticateRequest creds = new JsonModels.WSAuthenticateRequest();
            creds.username = credentials.cub_Username;
            creds.password = "testPassword";
            creds.customerType = accountType.cub_AccountName;

            JsonModels.WSAuthenticateResponse authResponse = global.CustomerAuthenticate(creds);

            Assert.AreEqual(authResponse.authErrors[1].key, CUBConstants.Error.ERR_ACCNT_IS_EXPIRED);
            Assert.AreEqual(authResponse.authErrors[1].message, CUBConstants.Error.MSG_TEMPORARY_PASSWORD_IS_EXPIRED);
        }
    }
}
