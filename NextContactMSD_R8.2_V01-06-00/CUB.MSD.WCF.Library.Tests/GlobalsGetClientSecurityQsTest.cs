/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using CUB.MSD.Model;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGetClientSecurityQsTest
    {
        [TestMethod]
        public void GetClientSecurityQsSuccessTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                cub_SecurityQuestion csq1 = new cub_SecurityQuestion();
                csq1.cub_SecurityQuestionId = Guid.NewGuid();
                csq1.cub_SecurityQuestionDesc = "What is your favorite color?";

                cub_SecurityQuestion csq2 = new cub_SecurityQuestion();
                csq2.cub_SecurityQuestionId = Guid.NewGuid();
                csq2.cub_SecurityQuestionDesc = "What is your favorite car marque?";

                data.Add(csq1);
                data.Add(csq2);

                List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
                data.AddRange(globalpwddata);

                fakedContext.Initialize(data);

                JsonModels.WSSecurityQuestions response = global.GetClientSecurityQuestions();

                Assert.IsNotNull(response.securityQuestions);
                Assert.AreEqual(response.securityQuestions.Count, 2);
                Assert.AreEqual(response.securityQuestions[0].name, "What is your favorite color?");
                Assert.AreEqual(response.securityQuestions[1].name, "What is your favorite car marque?");
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.IsNull(exActMgrFault);
            }
        }

        [TestMethod]
        public void GetClientSecurityQsNullServiceTest()
        {
            try
            {
                var fakedContext = new XrmFakedContext();
                fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
                var fakedService = fakedContext.GetOrganizationService();
                List<Entity> data = new List<Entity>();

                ConnectionInformation ci = new ConnectionInformation();
                ci.CrmService = fakedService;
                Globals global = new Globals(ci);

                cub_SecurityQuestion csq1 = new cub_SecurityQuestion();
                csq1.cub_SecurityQuestionId = Guid.NewGuid();
                csq1.cub_SecurityQuestionDesc = "What is your favorite color?";

                cub_SecurityQuestion csq2 = new cub_SecurityQuestion();
                csq2.cub_SecurityQuestionId = Guid.NewGuid();
                csq2.cub_SecurityQuestionDesc = "What is your favorite car marque?";

                data.Add(csq1);
                data.Add(csq2);

                List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
                data.AddRange(globalpwddata);

                fakedContext.Initialize(data);

                global.ConnectionInfo = null;

                JsonModels.WSSecurityQuestions response = global.GetClientSecurityQuestions();

                Assert.IsNotNull(response.securityQuestions);
                Assert.AreEqual(response.securityQuestions.Count, 2);
                Assert.AreEqual(response.securityQuestions[0].name, "What is your favorite color?");
                Assert.AreEqual(response.securityQuestions[0].value, 1);
                Assert.AreEqual(response.securityQuestions[1].name, "What is your favorite car marque?");
                Assert.AreEqual(response.securityQuestions[1].value, 2);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> e)
            {
                Assert.AreEqual(e.Detail.fieldName, "MSD");
                Assert.AreEqual(e.Detail.errorKey, "CUB.MSD.WCF.Library");
                Assert.AreEqual(e.Detail.errorMessage, "Object reference not set to an instance of an object.");
            }
        }
    }
}
