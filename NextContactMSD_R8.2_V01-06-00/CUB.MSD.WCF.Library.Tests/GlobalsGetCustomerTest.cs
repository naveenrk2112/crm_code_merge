/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using System.ServiceModel;

namespace CUB.MSD.WCF.Library.Tests
{
    [TestClass]
    public class GlobalsGetCustomerTest
    {
        [TestMethod]
        public void GetCustomerSuccessTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            Account acc = new Account();
            acc.AccountId = Guid.NewGuid();
            acc.CustomerTypeCode = (object)"Individual";
            acc.cub_AccountTypeId = new EntityReference();
            acc.cub_AccountTypeId.Id = Guid.NewGuid();
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            acc.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = acc.cub_AccountTypeId.Id;

            cub_ContactType cct = new cub_ContactType();
            cct.Id = Guid.NewGuid();
            cct.cub_Name = "CubName";

            cub_Address ca = new cub_Address();
            ca.cub_AddressId = Guid.NewGuid();
            ca.cub_AccountAddressId = new EntityReference();
            ca.cub_AccountAddressId.Id = (Guid)acc.AccountId;
            ca.cub_Name = "street";
            ca.cub_Street1 = "1308 South Wsahington Street";
            ca.cub_Street2 = "Building 2, Cubicle - The Dude";
            ca.cub_City = "Tullahoma";
            ca.cub_StateProvinceId = new EntityReference();
            ca.cub_StateProvinceId.Name = "TN";
            ca.cub_PostalCodeId = new EntityReference();
            ca.cub_PostalCodeId.Name = "37388";
            ca.cub_CountryId = new EntityReference();
            ca.cub_CountryId.Id = Guid.NewGuid();
            ca.cub_CountryId.Name = "US";

            cub_Country cctry = new cub_Country();
            cctry.cub_CountryId = ca.cub_CountryId.Id;
            cctry.cub_Alpha2 = "US";

            Contact con = new Contact();
            con.ContactId = Guid.NewGuid();
            con.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };
            con.cub_ContactTypeId = new EntityReference { LogicalName = cub_ContactType.EntityLogicalName, Id = cct.Id };
            con.Salutation = "Mr.";
            con.FirstName = "Cubic";
            con.LastName = "Tester";
            con.MiddleName = "W";
            con.Telephone1 = "9314541408";
            con.Telephone2 = "9314541409";
            con.Telephone3 = "9314541410";
            con.cub_Phone1Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880003);
            con.cub_Phone2Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880001);
            con.cub_Phone3Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880000);
            con.cub_AddressId = new EntityReference { LogicalName = cub_Address.EntityLogicalName, Id = (Guid)ca.cub_AddressId };
            con.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };
            con.Suffix = "Jr.";
            con.EMailAddress1 = "MrTester@cubic.com";
            con.BirthDate = DateTime.Now;
            con.cub_PersonalIdentifier = "Drivers License";
            con.cub_PersonalIdentifierType = "1";

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_Username = "the_user_name";
            cc.cub_Pin = "1234";
            cc.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };

            cub_SecurityQuestion csq = new cub_SecurityQuestion();
            csq.cub_SecurityQuestionId = Guid.NewGuid();
            csq.cub_SecurityQuestionDesc = "What is the answer to the ultimate question?";

            cub_SecurityAnswer csa = new cub_SecurityAnswer();
            csa.cub_SecurityAnswerId = Guid.NewGuid();
            csa.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };
            csa.cub_securityquestion_securityanswer = csq;
            csa.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq.cub_SecurityQuestionId };
            csa.cub_SecurityAnswerDesc = "42";

            data.Add(acc);
            data.Add(cat);
            data.Add(cct);
            data.Add(ca);
            data.Add(cctry);
            data.Add(con);
            data.Add(cc);
            data.Add(csq);
            data.Add(csa);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = acc.AccountId.Value.ToString();
            JsonModels.WSCustomer custResponse = global.GetCustomer(customerId);

            Assert.AreEqual(custResponse.customerId, con.ParentCustomerId.Id.ToString().ToUpper());
            Assert.IsNotNull(custResponse.contacts);
            if (custResponse.contacts.Count > 0)
                Assert.AreEqual(custResponse.contacts[0].contactId, con.ContactId.ToString().ToUpper());
            Assert.IsNotNull(custResponse.addresses);
            if (custResponse.addresses.Count > 0)
                Assert.AreEqual(custResponse.addresses[0].addressId, ca.cub_AddressId.ToString().ToUpper());
        }

        [TestMethod]
        public void GetCustomerNullCustomer()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = null;

            try
            {
                JsonModels.WSCustomer custResponse = global.GetCustomer(customerId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }

        [TestMethod]
        public void GetCustomerBadCustomerId()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = "7F754EAB-9545-4EEF-8063-A53453DD2FDZ";

            try
            {
                JsonModels.WSCustomer custResponse = global.GetCustomer(customerId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }

        [TestMethod]
        public void GetCustomerNotFoundTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            Account acc = new Account();
            acc.AccountId = Guid.NewGuid();
            acc.CustomerTypeCode = (object)"Individual";
            acc.cub_AccountTypeId = new EntityReference();
            acc.cub_AccountTypeId.Id = Guid.NewGuid();
            OptionSetValue value = new OptionSetValue();
            value.Value = 1;
            acc.StatusCode = value; //account_statuscode.Active;

            cub_AccountType cat = new cub_AccountType();
            cat.cub_AccountTypeId = acc.cub_AccountTypeId.Id;

            cub_ContactType cct = new cub_ContactType();
            cct.Id = Guid.NewGuid();
            cct.cub_Name = "CubName";

            cub_Address ca = new cub_Address();
            ca.cub_AddressId = Guid.NewGuid();
            ca.cub_AccountAddressId = new EntityReference();
            ca.cub_AccountAddressId.Id = (Guid)acc.AccountId;
            ca.cub_Name = "street";
            ca.cub_Street1 = "1308 South Wsahington Street";
            ca.cub_Street2 = "Building 2, Cubicle - The Dude";
            ca.cub_City = "Tullahoma";
            ca.cub_StateProvinceId = new EntityReference();
            ca.cub_StateProvinceId.Name = "TN";
            ca.cub_PostalCodeId = new EntityReference();
            ca.cub_PostalCodeId.Name = "37388";
            ca.cub_CountryId = new EntityReference();
            ca.cub_CountryId.Id = Guid.NewGuid();
            ca.cub_CountryId.Name = "US";

            cub_Country cctry = new cub_Country();
            cctry.cub_CountryId = ca.cub_CountryId.Id;
            cctry.cub_Alpha2 = "US";

            Contact con = new Contact();
            con.ContactId = Guid.NewGuid();
            con.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };
            con.cub_ContactTypeId = new EntityReference { LogicalName = cub_ContactType.EntityLogicalName, Id = cct.Id };
            con.Salutation = "Mr.";
            con.FirstName = "Cubic";
            con.LastName = "Tester";
            con.MiddleName = "W";
            con.Telephone1 = "9314541408";
            con.Telephone2 = "9314541409";
            con.Telephone3 = "9314541410";
            con.cub_Phone1Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880003);
            con.cub_Phone2Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880001);
            con.cub_Phone3Type = new Microsoft.Xrm.Sdk.OptionSetValue(971880000);
            con.cub_AddressId = new EntityReference { LogicalName = cub_Address.EntityLogicalName, Id = (Guid)ca.cub_AddressId };
            con.ParentCustomerId = new EntityReference { LogicalName = Account.EntityLogicalName, Id = (Guid)acc.AccountId };
            con.Suffix = "Jr.";
            con.EMailAddress1 = "MrTester@cubic.com";
            con.BirthDate = DateTime.Now;
            con.cub_PersonalIdentifier = "Drivers License";
            con.cub_PersonalIdentifierType = "1";

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_Username = "the_user_name";
            cc.cub_Pin = "1234";
            cc.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };

            cub_SecurityQuestion csq = new cub_SecurityQuestion();
            csq.cub_SecurityQuestionId = Guid.NewGuid();
            csq.cub_SecurityQuestionDesc = "What is the answer to the ultimate question?";

            cub_SecurityAnswer csa = new cub_SecurityAnswer();
            csa.cub_SecurityAnswerId = Guid.NewGuid();
            csa.cub_ContactId = new EntityReference { LogicalName = Contact.EntityLogicalName, Id = (Guid)con.ContactId };
            csa.cub_securityquestion_securityanswer = csq;
            csa.cub_SecurityQuestionId = new EntityReference { LogicalName = cub_SecurityAnswer.EntityLogicalName, Id = (Guid)csq.cub_SecurityQuestionId };
            csa.cub_SecurityAnswerDesc = "42";

            data.Add(acc);
            data.Add(cat);
            data.Add(cct);
            data.Add(ca);
            data.Add(cctry);
            data.Add(con);
            data.Add(cc);
            data.Add(csq);
            data.Add(csa);
            fakedContext.Initialize(data);

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);
            string customerId = acc.AccountId.Value.ToString();

            try
            {
                JsonModels.WSCustomer custResponse = global.GetCustomer(customerId);
            }
            catch (FaultException<Model.MSDApi.WSMSDFault> exActMgrFault)
            {
                Assert.AreEqual(exActMgrFault.Detail.fieldName, "CustomerId");
                Assert.AreEqual(exActMgrFault.Detail.errorKey, CUBConstants.Error.ERR_GENERAL_RECORD_NOT_FOUND);
                Assert.AreEqual(exActMgrFault.Detail.errorMessage, "The customer not found");
            }
        }
    }
}
