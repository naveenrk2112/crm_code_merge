/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;

namespace CUB.MSD.WCF.Library.Tests
{
    public class GlobalsHelperForPasswordAndUsername
    {
        public static List<Entity> GetEmailFormatConstants()
        {
            List<Entity> data = new List<Entity>();

            cub_Globals cg = (new cub_Globals
            {
                cub_GlobalsId = Guid.NewGuid(),
                cub_Name = "ValidateEmailFormat",
                cub_Enabled = true
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UseEmailForUsername",
                cub_AttributeValue = "1",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(cg);
            return data;
        }

        public static List<Entity> GetUsernameConstants()
        {
            List<Entity> data = new List<Entity>();

            cub_Globals cg = (new cub_Globals
            {
                cub_GlobalsId = Guid.NewGuid(),
                cub_Name = "UsernameValidation",
                cub_Enabled = true
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UsernameMaxLength",
                cub_AttributeValue = "60",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UsernameMinLength",
                cub_AttributeValue = "6",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "UsernameRequireEmail",
                cub_AttributeValue = "0",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(cg);
            return data;
        }

        public static List<Entity> GetPasswordConstants()
        {
            List<Entity> data = new List<Entity>();

            cub_Globals cg = (new cub_Globals
            {
                cub_GlobalsId = Guid.NewGuid(),
                cub_Name = "PasswordValidation",
                cub_Enabled = true
            });

            data.Add(new cub_GlobalDetails
            { 
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordMinLength",
                cub_AttributeValue = "8",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordMaxLength",
                cub_AttributeValue = "60",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordNumeric",
                cub_AttributeValue = "0",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordLowerAlpha",
                cub_AttributeValue = "0",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordUpperAlpha",
                cub_AttributeValue = "0",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "PasswordNonAlpha",
                cub_AttributeValue = "0",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TempPasswordDuartionDays",
                cub_AttributeValue = "2",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });

            data.Add(new cub_GlobalDetails
            {
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "NumberOfPasswordsToArchive",
                cub_AttributeValue = "5",
                cub_Enabled = true,
                cub_GlobalId = new EntityReference { LogicalName = cg.LogicalName, Id = cg.cub_GlobalsId.Value }
            });
            
            data.Add(cg);
            return data;
        }
    }

    [TestClass]
    public class GlobalsPrevalidateTest
    {
        [TestMethod]
        public void PrevalidateTestDuplicateUsername()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_Username = "myUsername";

            data.Add(cc);

            List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
            data.AddRange(globalpwddata);

            List<Entity> globalusrdata = GlobalsHelperForPasswordAndUsername.GetUsernameConstants();
            data.AddRange(globalusrdata);

            List<Entity> globalemailfmtdata = GlobalsHelperForPasswordAndUsername.GetEmailFormatConstants();
            data.AddRange(globalemailfmtdata);

            fakedContext.Initialize(data);

            JsonModels.WSCredentials creds = new JsonModels.WSCredentials();
            creds.username = cc.cub_Username;
            creds.password = "password";

            JsonModels.WSCredentailsPrevalidateResults results = global.CustomerPrevalidate(creds);

            Assert.IsFalse(results.isPasswordValid);
            Assert.IsFalse(results.isUsernameValid);
            Assert.AreEqual(results.usernameErrors.Count, 1);
            Assert.AreEqual(results.usernameErrors[0].key, "errors.general.value.duplicate");
            Assert.AreEqual(results.usernameErrors[0].message, "Value already exists");
            Assert.AreEqual(results.passwordErrors.Count, 1);
            Assert.AreEqual(results.passwordErrors[0].key, CUBConstants.Error.ERR_SAME_CONSECUTIVE_CHARS);
            Assert.AreEqual(results.passwordErrors[0].message, CUBConstants.Error.MSG_SAME_CONSECUTIVE_CHARS);
        }

        [TestMethod]
        public void PrevalidateTestGoodUsername()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_Username = "myUsername";

            data.Add(cc);

            List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
            data.AddRange(globalpwddata);

            List<Entity> globalusrdata = GlobalsHelperForPasswordAndUsername.GetUsernameConstants();
            data.AddRange(globalusrdata);

            List<Entity> globalemailfmtdata = GlobalsHelperForPasswordAndUsername.GetEmailFormatConstants();
            data.AddRange(globalemailfmtdata);

            fakedContext.Initialize(data);

            JsonModels.WSCredentials creds = new JsonModels.WSCredentials();
            creds.username = "TheUserName";
            creds.password = "Pa$sword";

            JsonModels.WSCredentailsPrevalidateResults results = global.CustomerPrevalidate(creds);

            Assert.IsTrue(results.isPasswordValid);
            Assert.IsTrue(results.isUsernameValid);
            Assert.IsNull(results.passwordErrors);
            Assert.IsNull(results.usernameErrors);
        }

        [TestMethod]
        public void PrevalidateTestBadUsername()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = new List<Entity>();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            string org = global.GetOrgName();

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_Username = "myUsername";

            data.Add(cc);

            List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
            data.AddRange(globalpwddata);

            List<Entity> globalusrdata = GlobalsHelperForPasswordAndUsername.GetUsernameConstants();
            data.AddRange(globalusrdata);

            List<Entity> globalemailfmtdata = GlobalsHelperForPasswordAndUsername.GetEmailFormatConstants();
            data.AddRange(globalemailfmtdata);

            fakedContext.Initialize(data);

            JsonModels.WSCredentials creds = new JsonModels.WSCredentials();
            creds.username = "UName";
            creds.password = "Pa$sword";

            JsonModels.WSCredentailsPrevalidateResults results = global.CustomerPrevalidate(creds);

            Assert.IsTrue(results.isPasswordValid);
            Assert.IsFalse(results.isUsernameValid);
            Assert.AreEqual(results.usernameErrors[0].key, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL);
            Assert.AreEqual(results.usernameErrors[0].message, CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET);
            Assert.IsNull(results.passwordErrors);
        }

        [TestMethod]
        public void PrevalidateTestNullCredentials()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            CommonSettings.OrgName = "TestOrg";
            string org = CommonSettings.OrgName;
            CommonSettings.LogTable = "TestLog";
            string loggingTable = CommonSettings.LogTable;
            CommonSettings.UserName = "testUser";
            string username = CommonSettings.UserName;
            CommonSettings.Password = "testPassword";
            string password = CommonSettings.Password;
            CommonSettings.Domain = "testDomain";
            string domain = CommonSettings.Domain;
            var fakedService = fakedContext.GetOrganizationService();

            List<Entity> data = new List<Entity>();

            ConnectionInformation ci = new ConnectionInformation();
            ci.CrmService = fakedService;
            Globals global = new Globals(ci);

            cub_Credential cc = new cub_Credential();
            cc.cub_CredentialId = Guid.NewGuid();
            cc.cub_Username = "myUsername";

            data.Add(cc);

            List<Entity> globalpwddata = GlobalsHelperForPasswordAndUsername.GetPasswordConstants();
            data.AddRange(globalpwddata);

            List<Entity> globalusrdata = GlobalsHelperForPasswordAndUsername.GetUsernameConstants();
            data.AddRange(globalusrdata);

            List<Entity> globalemailfmtdata = GlobalsHelperForPasswordAndUsername.GetEmailFormatConstants();
            data.AddRange(globalemailfmtdata);

            fakedContext.Initialize(data);

            JsonModels.WSCredentailsPrevalidateResults results = global.CustomerPrevalidate(null);
            Assert.IsFalse(results.isPasswordValid);
            Assert.IsFalse(results.isUsernameValid);
            Assert.AreEqual(results.usernameErrors.Count, 1);
            Assert.AreEqual(results.usernameErrors[0].key, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL);
            Assert.AreEqual(results.usernameErrors[0].message, CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET);
            Assert.AreEqual(results.passwordErrors.Count, 1);
            Assert.AreEqual(results.passwordErrors[0].key, CUBConstants.Error.ERR_GENERAL_VALUE_TOO_SMALL);
            Assert.AreEqual(results.passwordErrors[0].message, CUBConstants.Error.MSG_MINIMUM_LENGTH_NOT_MET);
        }
    }
}
