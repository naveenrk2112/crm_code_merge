select * from APP_RESOURCE order by APP_RESOURCE_ID;
select * from APP_RESOURCE_SCRIPT_MAPPING;
/*
 * INSERT APP_RESOURCE
 */
DEFINE VAR_APP_RESOURCE_ID = '5';
DEFINE VAR_APP_RESOURCE_TYPE_ID = '2';
DEFINE VAR_URI = '/javascript/getTrxnHistory.js'
DEFINE VAR_DESCRIPTION = 'Tolling: getTrxnHistory fake call'

INSERT into APP_RESOURCE (APP_RESOURCE_ID, APP_RESOURCE_TYPE_ID, URI, ACTIVE_FLAG, DESCRIPTION)
VALUES (&&VAR_APP_RESOURCE_ID, &&VAR_APP_RESOURCE_TYPE_ID, '&&VAR_URI', 1, '&&VAR_DESCRIPTION');

/*
 * UPDATE TEXT_DATA (JavaScript Function)
 * This is a separated statement because it will be executed multiple times
 * before it get done correctly.
 * PLEASE REMEMBER TO SET THE CORRECT 'APP_RESOURCE_ID'
 */
UPDATE APP_RESOURCE 
  SET TEXT_DATA = '
var TollingGetTrxnHistory = function(req) {
var json = [
     {
        "tollAmount": 0.5,
        "tagAgency": "026",
        "tagSno": "9B065033",
        "lpType": "PASS",
        "lpNum": "7911691",
        "origin": " ",
        "updatedDate": "Sep 19, 2017 4:18:01 AM",
        "imagePresent": "Y",
        "paidStatus": "NOT_PAID",
        "licensePlate": "NH 7911691 PASS",
        "paidDate": "NA",
        "violationType": "0",
        "ticketNo": "***********",
        "entryLane": "Rochester 04",
        "tollPaid": 0,
        "tollDue": 0.5,
        "id": 644989,
        "actualAxles": 0,
        "exitLane": "Hooksett Ramp 05",
        "uidUpdated": 100,
        "validationStatus": "S",
        "exitDate": 20170919,
        "entryDate": 20170919,
        "confidence": " ",
        "lpState": "NH",
        "tollDate": 20170919,
        "tollFare": 0.5
    },
     {
        "tollAmount": 0.5,
        "tagAgency": "026",
        "tagSno": "9B065033",
        "lpType": "PASS",
        "lpNum": "7911691",
        "origin": " ",
        "updatedDate": "Sep 19, 2017 4:18:01 AM",
        "imagePresent": "Y",
        "paidStatus": "NOT_PAID",
        "licensePlate": "NH 7911691 PASS",
        "paidDate": "NA",
        "violationType": "0",
        "ticketNo": "***********",
        "entryLane": "Rochester 04",
        "tollPaid": 0,
        "tollDue": 0.5,
        "id": 644989,
        "actualAxles": 0,
        "exitLane": "Hooksett Ramp 05",
        "uidUpdated": 100,
        "validationStatus": "S",
        "exitDate": 20170919,
        "entryDate": 20170919,
        "confidence": " ",
        "lpState": "NH",
        "tollDate": 20170919,
        "tollFare": 0.5
    },
     {
        "tollAmount": 0.5,
        "tagAgency": "026",
        "tagSno": "9B065033",
        "lpType": "PASS",
        "lpNum": "7911691",
        "origin": " ",
        "updatedDate": "Sep 19, 2017 4:18:01 AM",
        "imagePresent": "Y",
        "paidStatus": "NOT_PAID",
        "licensePlate": "NH 7911691 PASS",
        "paidDate": "NA",
        "violationType": "0",
        "ticketNo": "***********",
        "entryLane": "Rochester 04",
        "tollPaid": 0,
        "tollDue": 0.5,
        "id": 644989,
        "actualAxles": 0,
        "exitLane": "Hooksett Ramp 05",
        "uidUpdated": 100,
        "validationStatus": "S",
        "exitDate": 20170919,
        "entryDate": 20170919,
        "confidence": " ",
        "lpState": "NH",
        "tollDate": 20170919,
        "tollFare": 0.5
    },
];
var mock = req.createMockResponse();
mock.setBody(JSON.stringify(json));
mock.setContentType("application/json");
mock.setResponseCode(200);
};' 
WHERE APP_RESOURCE_ID = 5; -- ATTENTION: REMEMBER TO SET THIS CORRECTLY

/*
 * INSERT APP_RESOURCE_SCRIPT_MAPPING
 */
DEFINE VAR_PRIORITY = 1;
DEFINE VAR_IN_OUT_FLAG = 1;
DEFINE VAR_REQ_RSP_FLAG = 1;
DEFINE VAR_INBOUND_HTTP_METHOD = 'GET'
DEFINE VAR_INBOUND_URI = '*/csapi/v1/getTrxnHistory';
DEFINE VAR_FUNCTION_NAME = 'TollingGetTrxnHistory';
DEFINE VAR_DESCRIPTION = 'Tolling: GetTrxnHistory fake call';

INSERT INTO APP_RESOURCE_SCRIPT_MAPPING (PRIORITY, IN_OUT_FLAG, REQ_RSP_FLAG, INBOUND_HTTP_METHOD, INBOUND_URI, FUNCTION_NAME, ACTIVE_FLAG, DESCRIPTION, INSERTED_DTM)
VALUES (&&VAR_PRIORITY, &&VAR_IN_OUT_FLAG, &&VAR_REQ_RSP_FLAG, '&&VAR_INBOUND_HTTP_METHOD', '&&VAR_INBOUND_URI', '&&VAR_FUNCTION_NAME', 1, '&&VAR_DESCRIPTION', SYSDATE);
