select * from APP_RESOURCE order by APP_RESOURCE_ID;
select * from APP_RESOURCE_SCRIPT_MAPPING;
/*
 * INSERT APP_RESOURCE
 */
DEFINE VAR_APP_RESOURCE_ID = '9';
DEFINE VAR_APP_RESOURCE_TYPE_ID = '2';
DEFINE VAR_URI = '/javascript/CustomerOrderDebtCollect.js'
DEFINE VAR_DESCRIPTION = 'CustomerOrder: DebtCollect fake call'

INSERT into APP_RESOURCE (APP_RESOURCE_ID, APP_RESOURCE_TYPE_ID, URI, ACTIVE_FLAG, DESCRIPTION)
VALUES (&&VAR_APP_RESOURCE_ID, &&VAR_APP_RESOURCE_TYPE_ID, '&&VAR_URI', 1, '&&VAR_DESCRIPTION');

/*
 * UPDATE TEXT_DATA (JavaScript Function)
 * This is a separated statement because it will be executed multiple times
 * before it get done correctly.
 * PLEASE REMEMBER TO SET THE CORRECT 'APP_RESOURCE_ID'
 */
UPDATE APP_RESOURCE 
  SET TEXT_DATA = '
var CustomerOrderGetDebtCollect = function(req) {
var json =
{
	"orderId": "2740",
	"responseCode": "FulfullmentFailed",
	"paymentResults": [
	    {
		   "responseCode": "Successful",
		   "authRefNbr": "12345678",
		   "bankAuthCode": "123456",
		   "authDateTime": "2017-11-07T22:09:12.747Z",
		   "payment": {
		      "paymentType": "CreditCardNotPresent",
			  "paymentAmount": 1000,
			  "creditCardType": "Visa",
			  "maskedPan": "0010"
	        }
	    }
    ],
	"error": []
};
var mock = req.createMockResponse();
mock.setBody(JSON.stringify(json));
mock.setContentType("application/json");
mock.setResponseCode(200);
};' 
WHERE APP_RESOURCE_ID = 9; -- ATTENTION: REMEMBER TO SET THIS CORRECTLY

/*
 * INSERT APP_RESOURCE_SCRIPT_MAPPING
 */
DEFINE VAR_PRIORITY = 1;
DEFINE VAR_IN_OUT_FLAG = 1;
DEFINE VAR_REQ_RSP_FLAG = 1;
DEFINE VAR_INBOUND_HTTP_METHOD = 'POST'
DEFINE VAR_INBOUND_URI = '*order/debtcollect';
DEFINE VAR_FUNCTION_NAME = 'CustomerOrderGetDebtCollect';
DEFINE VAR_DESCRIPTION = 'order/debtcollect fake call';

INSERT INTO APP_RESOURCE_SCRIPT_MAPPING (PRIORITY, IN_OUT_FLAG, REQ_RSP_FLAG, INBOUND_HTTP_METHOD, INBOUND_URI, FUNCTION_NAME, ACTIVE_FLAG, DESCRIPTION, INSERTED_DTM)
VALUES (&&VAR_PRIORITY, &&VAR_IN_OUT_FLAG, &&VAR_REQ_RSP_FLAG, '&&VAR_INBOUND_HTTP_METHOD', '&&VAR_INBOUND_URI', '&&VAR_FUNCTION_NAME', 1, '&&VAR_DESCRIPTION', SYSDATE);
