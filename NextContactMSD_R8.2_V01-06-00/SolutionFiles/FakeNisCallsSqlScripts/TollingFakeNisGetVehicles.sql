select * from APP_RESOURCE order by APP_RESOURCE_ID;
select * from APP_RESOURCE_SCRIPT_MAPPING;
/*
 * INSERT APP_RESOURCE
 */
DEFINE VAR_APP_RESOURCE_ID = '4';
DEFINE VAR_APP_RESOURCE_TYPE_ID = '2';
DEFINE VAR_URI = '/javascript/TollingGetVehicles.js'
DEFINE VAR_DESCRIPTION = 'Tolling: GetVehicles fake call'

INSERT into APP_RESOURCE (APP_RESOURCE_ID, APP_RESOURCE_TYPE_ID, URI, ACTIVE_FLAG, DESCRIPTION)
VALUES (&&VAR_APP_RESOURCE_ID, &&VAR_APP_RESOURCE_TYPE_ID, '&&VAR_URI', 1, '&&VAR_DESCRIPTION');

/*
 * UPDATE TEXT_DATA (JavaScript Function)
 * This is a separated statement because it will be executed multiple times
 * before it get done correctly.
 * PLEASE REMEMBER TO SET THE CORRECT 'APP_RESOURCE_ID'
 */
UPDATE APP_RESOURCE 
  SET TEXT_DATA = '
var TollingGetVehicles = function(req) {
var json = [
    {
        "color": "Blue",
        "year": 2017,
        "uidCreated": 100,
        "origin": "D",
        "axles": "4",
        "plateNum": "6007251",
        "type": "PASS",
        "isActive": "Y",
        "tsUpdated": "Sep 18, 2017 7:28:31 PM",
        "customerId": "1003",
        "tsCreated": "Sep 18, 2017 7:28:31 PM",
        "model": "Rav4",
        "id": 11442,
        "state": "NH",
        "licensePlateNo": "NH 6007251 PASS",
        "uidUpdated": 100,
        "make": "Toyota"
    },
    {
        "color": "Black",
        "year": 2017,
        "uidCreated": 100,
        "origin": "D",
        "axles": "4",
        "plateNum": "6007252",
        "type": "PASS",
        "isActive": "Y",
        "tsUpdated": "Sep 18, 2017 7:28:31 PM",
        "customerId": "1003",
        "tsCreated": "Sep 18, 2017 7:28:31 PM",
        "model": "Thundra",
        "id": 11442,
        "state": "NH",
        "licensePlateNo": "NH 6007252 PASS",
        "uidUpdated": 100,
        "make": "Toyota"
    },
    {
        "color": "Silver",
        "year": 2017,
        "uidCreated": 100,
        "origin": "D",
        "axles": "4",
        "plateNum": "6007253",
        "type": "PASS",
        "isActive": "Y",
        "tsUpdated": "Sep 18, 2017 7:28:31 PM",
        "customerId": "1003",
        "tsCreated": "Sep 18, 2017 7:28:31 PM",
        "model": "F1000",
        "id": 11442,
        "state": "NH",
        "licensePlateNo": "NH 6007253 PASS",
        "uidUpdated": 100,
        "make": "Ford"
    },
];
var mock = req.createMockResponse();
mock.setBody(JSON.stringify(json));
mock.setContentType("application/json");
mock.setResponseCode(200);
};' 
WHERE APP_RESOURCE_ID = 4; -- ATTENTION: REMEMBER TO SET THIS CORRECTLY

/*
 * INSERT APP_RESOURCE_SCRIPT_MAPPING
 */
DEFINE VAR_PRIORITY = 1;
DEFINE VAR_IN_OUT_FLAG = 1;
DEFINE VAR_REQ_RSP_FLAG = 1;
DEFINE VAR_INBOUND_HTTP_METHOD = 'GET'
DEFINE VAR_INBOUND_URI = '*/csapi/v1/getVehicles';
DEFINE VAR_FUNCTION_NAME = 'TollingGetVehicles';
DEFINE VAR_DESCRIPTION = 'Tolling: GetVehicles fake call';

INSERT INTO APP_RESOURCE_SCRIPT_MAPPING (PRIORITY, IN_OUT_FLAG, REQ_RSP_FLAG, INBOUND_HTTP_METHOD, INBOUND_URI, FUNCTION_NAME, ACTIVE_FLAG, DESCRIPTION, INSERTED_DTM)
VALUES (&&VAR_PRIORITY, &&VAR_IN_OUT_FLAG, &&VAR_REQ_RSP_FLAG, '&&VAR_INBOUND_HTTP_METHOD', '&&VAR_INBOUND_URI', '&&VAR_FUNCTION_NAME', 1, '&&VAR_DESCRIPTION', SYSDATE);
