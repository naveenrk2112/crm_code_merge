select * from APP_RESOURCE order by APP_RESOURCE_ID;
select * from APP_RESOURCE_SCRIPT_MAPPING;
/*
 * INSERT APP_RESOURCE
 */
DEFINE VAR_APP_RESOURCE_ID = '11';
DEFINE VAR_APP_RESOURCE_TYPE_ID = '2';
DEFINE VAR_URI = '/javascript/TransitAccountStatusHistory.js'
DEFINE VAR_DESCRIPTION = 'TransitAccount: Status History fake call'

INSERT into APP_RESOURCE (APP_RESOURCE_ID, APP_RESOURCE_TYPE_ID, URI, ACTIVE_FLAG, DESCRIPTION)
VALUES (&&VAR_APP_RESOURCE_ID, &&VAR_APP_RESOURCE_TYPE_ID, '&&VAR_URI', 1, '&&VAR_DESCRIPTION');

/*
 * UPDATE TEXT_DATA (JavaScript Function)
 * This is a separated statement because it will be executed multiple times
 * before it get done correctly.
 * PLEASE REMEMBER TO SET THE CORRECT 'APP_RESOURCE_ID'
 */
UPDATE APP_RESOURCE 
  SET TEXT_DATA = '
var TransitAccountGetStatusHistory = function(req) {
var json =
{
	"totalCount": 10,
	"lineItems": [
		{
			"statusId": "1582019",
			"updatedDateTime": "2017-11-13T18:25:43.511Z",
			"status": "Active",
			"source": "CSR",
			"reason": "Reason here.  Limited to 80 characters",
			"token": "Token here"
        },
	{
			"statusId": "1582020",
			"updatedDateTime": "2017-12-01T18:25:43.511Z",
			"status": "Suspended",
			"source": "Web",
			"reason": "Lost card.",
			"token": "Token here"
        },
		{
			"statusId": "1582021",
			"updatedDateTime": "2017-12-02T18:25:43.511Z",
			"status": "Active",
			"source": "CSR",
			"reason": "Found card.",
			"token": "Token here"
        }
	]
};
var mock = req.createMockResponse();
mock.setBody(JSON.stringify(json));
mock.setContentType("application/json");
mock.setResponseCode(200);
};' 
WHERE APP_RESOURCE_ID = 11; -- ATTENTION: REMEMBER TO SET THIS CORRECTLY

/*
 * INSERT APP_RESOURCE_SCRIPT_MAPPING
 */
DEFINE VAR_PRIORITY = 1;
DEFINE VAR_IN_OUT_FLAG = 1;
DEFINE VAR_REQ_RSP_FLAG = 1;
DEFINE VAR_INBOUND_HTTP_METHOD = 'GET'
DEFINE VAR_INBOUND_URI = '*/transitaccount*/statushistory';
DEFINE VAR_FUNCTION_NAME = 'TransitAccountGetStatusHistory';
DEFINE VAR_DESCRIPTION = 'transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/statushistory fake call';

INSERT INTO APP_RESOURCE_SCRIPT_MAPPING (PRIORITY, IN_OUT_FLAG, REQ_RSP_FLAG, INBOUND_HTTP_METHOD, INBOUND_URI, FUNCTION_NAME, ACTIVE_FLAG, DESCRIPTION, INSERTED_DTM)
VALUES (&&VAR_PRIORITY, &&VAR_IN_OUT_FLAG, &&VAR_REQ_RSP_FLAG, '&&VAR_INBOUND_HTTP_METHOD', '&&VAR_INBOUND_URI', '&&VAR_FUNCTION_NAME', 1, '&&VAR_DESCRIPTION', SYSDATE);
