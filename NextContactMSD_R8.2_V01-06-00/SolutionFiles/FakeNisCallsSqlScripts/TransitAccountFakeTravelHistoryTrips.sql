select * from APP_RESOURCE order by APP_RESOURCE_ID;
select * from APP_RESOURCE_SCRIPT_MAPPING;
/*
 * INSERT APP_RESOURCE
 */
DEFINE VAR_APP_RESOURCE_ID = '8';
DEFINE VAR_APP_RESOURCE_TYPE_ID = '2';
DEFINE VAR_URI = '/javascript/TransitAccountTravelHistoryTrips.js'
DEFINE VAR_DESCRIPTION = 'TransitAccount: Travel History Trips fake call'

INSERT into APP_RESOURCE (APP_RESOURCE_ID, APP_RESOURCE_TYPE_ID, URI, ACTIVE_FLAG, DESCRIPTION)
VALUES (&&VAR_APP_RESOURCE_ID, &&VAR_APP_RESOURCE_TYPE_ID, '&&VAR_URI', 1, '&&VAR_DESCRIPTION');

/*
 * UPDATE TEXT_DATA (JavaScript Function)
 * This is a separated statement because it will be executed multiple times
 * before it get done correctly.
 * PLEASE REMEMBER TO SET THE CORRECT 'APP_RESOURCE_ID'
 */
UPDATE APP_RESOURCE 
  SET TEXT_DATA = '
var TransitAccountGetTravelHistoryTrips = function(req) {
var json =
{
	"totalCount": 10,
	"lineItems": [
		{
			"tripId": "200",
			"journeyId": "300",
			"startDateTime": "2017-11-13T18:25:43.511Z",
			"endDateTime": "2017-11-13T18:25:43.511Z",
			"startLocationDescription": "Roosevekt Ave & 74th St.",
			"endLocationDescription": "Roosevekt Ave & 74th St.",
			"travelMode": "Rail",
			"travelModeDescription": "Rail",
			"token": {
				"tokenType": "Bankcard",
				"maskedPAN": "1111"
			},
			"productDescription": "Weekly Pass",
			"totalFare": 275,
			"unpaidFare": 50,
			"tripType": "Trip Type",
			"tripTypeDescription": "Trip Type Description",
			"tripStatus": "Finalized",
			"tripStatusDescription": "Finalized",
			"tripStatusDateTime": "2017-11-13T18:25:43.511Z",
			"travelPresenceIndicator": "Manual-Filled",
			"isCorrectable": false,
			"riderClass": "Adult",
			"riderClassDescription": "Adult",
			"timeCategory": "Off-Peak",
			"timeCategoryDescription": "Off-Peak",
			"concession": "Full Fare",
			"concessionDescription": "Full Fare"
                },
		{
			"tripId": "200",
			"journeyId": "300",
			"startDateTime": "2017-11-13T18:25:43.511Z",
			"endDateTime": "2017-11-13T18:25:43.511Z",
			"startLocationDescription": "Roosevekt Ave & 74th St.",
			"endLocationDescription": "Roosevekt Ave & 74th St.",
			"travelMode": "Rail",
			"travelModeDescription": "Rail",
			"token": {
				"tokenType": "Bankcard",
				"maskedPAN": "1111"
			},
			"productDescription": "Weekly Pass",
			"totalFare": 275,
			"unpaidFare": 50,
			"tripType": "Trip Type",
			"tripTypeDescription": "Trip Type Description",
			"tripStatus": "Finalized",
			"tripStatusDescription": "Finalized",
			"tripStatusDateTime": "2017-11-13T18:25:43.511Z",
			"travelPresenceIndicator": "Manual-Filled",
			"isCorrectable": false,
			"riderClass": "Adult",
			"riderClassDescription": "Adult",
			"timeCategory": "Off-Peak",
			"timeCategoryDescription": "Off-Peak",
			"concession": "Full Fare",
			"concessionDescription": "Full Fare"
                },
		{
			"tripId": "200",
			"journeyId": "300",
			"startDateTime": "2017-11-13T18:25:43.511Z",
			"endDateTime": "2017-11-13T18:25:43.511Z",
			"startLocationDescription": "Roosevekt Ave & 74th St.",
			"endLocationDescription": "Roosevekt Ave & 74th St.",
			"travelMode": "Rail",
			"travelModeDescription": "Rail",
			"token": {
				"tokenType": "Bankcard",
				"maskedPAN": "1111"
			},
			"productDescription": "Weekly Pass",
			"totalFare": 275,
			"unpaidFare": 50,
			"tripType": "Trip Type",
			"tripTypeDescription": "Trip Type Description",
			"tripStatus": "Finalized",
			"tripStatusDescription": "Finalized",
			"tripStatusDateTime": "2017-11-13T18:25:43.511Z",
			"travelPresenceIndicator": "Manual-Filled",
			"isCorrectable": false,
			"riderClass": "Adult",
			"riderClassDescription": "Adult",
			"timeCategory": "Off-Peak",
			"timeCategoryDescription": "Off-Peak",
			"concession": "Full Fare",
			"concessionDescription": "Full Fare"
                },
		{
			"tripId": "200",
			"journeyId": "300",
			"startDateTime": "2017-11-13T18:25:43.511Z",
			"endDateTime": "2017-11-13T18:25:43.511Z",
			"startLocationDescription": "Roosevekt Ave & 74th St.",
			"endLocationDescription": "Roosevekt Ave & 74th St.",
			"travelMode": "Rail",
			"travelModeDescription": "Rail",
			"token": {
				"tokenType": "Bankcard",
				"maskedPAN": "1111"
			},
			"productDescription": "Weekly Pass",
			"totalFare": 275,
			"unpaidFare": 50,
			"tripType": "Trip Type",
			"tripTypeDescription": "Trip Type Description",
			"tripStatus": "Finalized",
			"tripStatusDescription": "Finalized",
			"tripStatusDateTime": "2017-11-13T18:25:43.511Z",
			"travelPresenceIndicator": "Manual-Filled",
			"isCorrectable": false,
			"riderClass": "Adult",
			"riderClassDescription": "Adult",
			"timeCategory": "Off-Peak",
			"timeCategoryDescription": "Off-Peak",
			"concession": "Full Fare",
			"concessionDescription": "Full Fare"
                },
		{
			"tripId": "200",
			"journeyId": "300",
			"startDateTime": "2017-11-13T18:25:43.511Z",
			"endDateTime": "2017-11-13T18:25:43.511Z",
			"startLocationDescription": "Roosevekt Ave & 74th St.",
			"endLocationDescription": "Roosevekt Ave & 74th St.",
			"travelMode": "Rail",
			"travelModeDescription": "Rail",
			"token": {
				"tokenType": "Bankcard",
				"maskedPAN": "1111"
			},
			"productDescription": "Weekly Pass",
			"totalFare": 275,
			"unpaidFare": 50,
			"tripType": "Trip Type",
			"tripTypeDescription": "Trip Type Description",
			"tripStatus": "Finalized",
			"tripStatusDescription": "Finalized",
			"tripStatusDateTime": "2017-11-13T18:25:43.511Z",
			"travelPresenceIndicator": "Manual-Filled",
			"isCorrectable": false,
			"riderClass": "Adult",
			"riderClassDescription": "Adult",
			"timeCategory": "Off-Peak",
			"timeCategoryDescription": "Off-Peak",
			"concession": "Full Fare",
			"concessionDescription": "Full Fare"
                }
	]
};
var mock = req.createMockResponse();
mock.setBody(JSON.stringify(json));
mock.setContentType("application/json");
mock.setResponseCode(200);
};' 
WHERE APP_RESOURCE_ID = 8; -- ATTENTION: REMEMBER TO SET THIS CORRECTLY

/*
 * INSERT APP_RESOURCE_SCRIPT_MAPPING
 */
DEFINE VAR_PRIORITY = 1;
DEFINE VAR_IN_OUT_FLAG = 1;
DEFINE VAR_REQ_RSP_FLAG = 1;
DEFINE VAR_INBOUND_HTTP_METHOD = 'GET'
DEFINE VAR_INBOUND_URI = '*/travelhistory/trips';
DEFINE VAR_FUNCTION_NAME = 'TransitAccountGetTravelHistoryTrips';
DEFINE VAR_DESCRIPTION = 'transitaccount/<transitaccount-id>/subsystem/<subsystem-id>/travelhistory/taps fake call';

INSERT INTO APP_RESOURCE_SCRIPT_MAPPING (PRIORITY, IN_OUT_FLAG, REQ_RSP_FLAG, INBOUND_HTTP_METHOD, INBOUND_URI, FUNCTION_NAME, ACTIVE_FLAG, DESCRIPTION, INSERTED_DTM)
VALUES (&&VAR_PRIORITY, &&VAR_IN_OUT_FLAG, &&VAR_REQ_RSP_FLAG, '&&VAR_INBOUND_HTTP_METHOD', '&&VAR_INBOUND_URI', '&&VAR_FUNCTION_NAME', 1, '&&VAR_DESCRIPTION', SYSDATE);
