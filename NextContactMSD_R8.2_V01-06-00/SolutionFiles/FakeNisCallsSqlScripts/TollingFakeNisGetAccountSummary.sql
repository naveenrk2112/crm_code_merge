select * from APP_RESOURCE order by APP_RESOURCE_ID;
select * from APP_RESOURCE_SCRIPT_MAPPING;
/*
 * INSERT APP_RESOURCE
 */
DEFINE VAR_APP_RESOURCE_ID = '6';
DEFINE VAR_APP_RESOURCE_TYPE_ID = '2';
DEFINE VAR_URI = '/javascript/getAccountSummary.js'
DEFINE VAR_DESCRIPTION = 'Tolling: getAccountSummary fake call'

INSERT into APP_RESOURCE (APP_RESOURCE_ID, APP_RESOURCE_TYPE_ID, URI, ACTIVE_FLAG, DESCRIPTION)
VALUES (&&VAR_APP_RESOURCE_ID, &&VAR_APP_RESOURCE_TYPE_ID, '&&VAR_URI', 1, '&&VAR_DESCRIPTION');

/*
 * UPDATE TEXT_DATA (JavaScript Function)
 * This is a separated statement because it will be executed multiple times
 * before it get done correctly.
 * PLEASE REMEMBER TO SET THE CORRECT 'APP_RESOURCE_ID'
 */
UPDATE APP_RESOURCE 
  SET TEXT_DATA = '
var TollingGetAccountSummary = function(req) {
var json = {
    "postapaidBalance": 10,
    "lastInvPayments": 12,
    "lastTripPost": "Sep 19, 2017 4:23:51 AM",
    "currentBalance": 100,
    "lastInvBalance": 25,
    "prepaidBalance": 100,
    "lastInvTolls": 10,
    "paymentPlanBalance": 8,
    "lastInvAdjustments": 10,
    "transponderDeposit": 15,
    "tagCount": 5,
    "plateCount": 8,
    "collectionBalance": 3,
    "lastPaymentPost": "Sep 12, 2017 2:21:40 AM",
    "violationBalance": 10
};
var mock = req.createMockResponse();
mock.setBody(JSON.stringify(json));
mock.setContentType("application/json");
mock.setResponseCode(200);
};' 
WHERE APP_RESOURCE_ID = 6; -- ATTENTION: REMEMBER TO SET THIS CORRECTLY

/*
 * INSERT APP_RESOURCE_SCRIPT_MAPPING
 */
DEFINE VAR_PRIORITY = 1;
DEFINE VAR_IN_OUT_FLAG = 1;
DEFINE VAR_REQ_RSP_FLAG = 1;
DEFINE VAR_INBOUND_HTTP_METHOD = 'GET'
DEFINE VAR_INBOUND_URI = '*/csapi/v1/getAccountSummary';
DEFINE VAR_FUNCTION_NAME = 'TollingGetAccountSummary';
DEFINE VAR_DESCRIPTION = 'Tolling: GetAccountSummary fake call';

INSERT INTO APP_RESOURCE_SCRIPT_MAPPING (PRIORITY, IN_OUT_FLAG, REQ_RSP_FLAG, INBOUND_HTTP_METHOD, INBOUND_URI, FUNCTION_NAME, ACTIVE_FLAG, DESCRIPTION, INSERTED_DTM)
VALUES (&&VAR_PRIORITY, &&VAR_IN_OUT_FLAG, &&VAR_REQ_RSP_FLAG, '&&VAR_INBOUND_HTTP_METHOD', '&&VAR_INBOUND_URI', '&&VAR_FUNCTION_NAME', 1, '&&VAR_DESCRIPTION', SYSDATE);
