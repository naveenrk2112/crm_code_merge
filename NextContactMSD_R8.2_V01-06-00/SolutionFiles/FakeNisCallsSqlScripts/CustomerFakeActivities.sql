/*
 * INSERT APP_RESOURCE
 */
DEFINE VAR_APP_RESOURCE_ID = '10';
DEFINE VAR_APP_RESOURCE_TYPE_ID = '2';
DEFINE VAR_URI = '/javascript/CustomerActivities.js'
DEFINE VAR_DESCRIPTION = 'Customer: Activities fake call'

INSERT into APP_RESOURCE (APP_RESOURCE_ID, APP_RESOURCE_TYPE_ID, URI, ACTIVE_FLAG, DESCRIPTION)
VALUES (&&VAR_APP_RESOURCE_ID, &&VAR_APP_RESOURCE_TYPE_ID, '&&VAR_URI', 1, '&&VAR_DESCRIPTION');

/*
 * UPDATE TEXT_DATA (JavaScript Function)
 * This is a separated statement because it will be executed multiple times
 * before it get done correctly.
 * PLEASE REMEMBER TO SET THE CORRECT 'APP_RESOURCE_ID'
 */
TEXT_DATA =>
var CustomerGetFakeActivities = function(req) {
    var json =
{
    "activities": [
        {
            "activityId": 52,
            "type": "linkSubsystemAccount",
            "actionType": "Create",
            "createDateTime": "2017-11-30T22:15:06.000Z",
            "channel": "NIS00001",
            "userName": " ",
            "activityData": {
                "content": [
                    {
                        "key": "subsystemName",
                        "value": "NYCT ABP"
                    },
                    {
                        "key": "authorityName",
                        "value": "NYCT"
                    }
                ],
                "contentMap": [
                    {
                        "key": "email",
                        "oldValue": "bob.marley@gmail",
                        "newValue": "Bob.Marley@gmail.com"
                    },
                    {
                        "key": "phone",
                        "oldValue": "604-3188645",
                        "newValue": "+1 604 3188645"
                    },
                ],
                "targetDetails": [
                    {
                        "key": "SMS",
                        "value": "858. 123.1234"
                    },
                    {
                        "key": "SMS",
                        "value": "858.123.6789"
                    },
                    {
                        "key": "Email",
                        "value": "bob.marley@gmail.com"
                    }
                ],
            }
        },
        {
            "activityId": 37,
            "type": "createOneAccount",
            "actionType": "Create",
            "createDateTime": "2017-11-30T21:23:10.000Z",
            "channel": "NIS00001",
            "userName": " ",
            "activityData": {
                "content": []
            }
        }
    ],
    "totalCount": 2
};
    var mock = req.createMockResponse();
    mock.setBody(JSON.stringify(json));
    mock.setContentType('application/json');
    mock.setResponseCode(200);
};

/*
 * INSERT APP_RESOURCE_SCRIPT_MAPPING
 */
DEFINE VAR_PRIORITY = 1;
DEFINE VAR_IN_OUT_FLAG = 1;
DEFINE VAR_REQ_RSP_FLAG = 1;
DEFINE VAR_INBOUND_HTTP_METHOD = 'GET'
DEFINE VAR_INBOUND_URI = '*/activity';
DEFINE VAR_FUNCTION_NAME = 'CustomerGetFakeActivities';
DEFINE VAR_DESCRIPTION = 'customer/<customer-id>/activity fake call';

INSERT INTO APP_RESOURCE_SCRIPT_MAPPING (PRIORITY, IN_OUT_FLAG, REQ_RSP_FLAG, INBOUND_HTTP_METHOD, INBOUND_URI, FUNCTION_NAME, ACTIVE_FLAG, DESCRIPTION, INSERTED_DTM)
VALUES (&&VAR_PRIORITY, &&VAR_IN_OUT_FLAG, &&VAR_REQ_RSP_FLAG, '&&VAR_INBOUND_HTTP_METHOD', '&&VAR_INBOUND_URI', '&&VAR_FUNCTION_NAME', 1, '&&VAR_DESCRIPTION', SYSDATE);
