
Project Setup: (see SP site for more detail)
- Source Location: $/CTSDev1/NextContact-MSD/Dev/NextContactMSD
- Add the Microsoft Dynamics 365 Developer Toolkit from Tools/Extensions
- Download the Dynamics 2016 (365) sdk and install
- Goto Tools/Options/Dynamics 365 Developer Toolkit and set locations for the pluginreg tool (..sdk\tools\pluginregistration and the sdk (..sdk\bin)
   - Uncheck Use MSBuild if checked
- Run the BuildPlugins.bat post build script in the CUB.MSD.Plugins folder the first time - you may get a prompt for File or Directory only the first time

Deploy Plugins:
- checkout CrmPackage/RegisterFile.crmregister (updated by deploy)
- Tools/Connect to Dynamics 365 Server 
  - server: USDCED-MSDYN01, etc
- right click CrmPackage and deploy or select plugin and deploy
- The plugin post-build script uses ilMerge to merge dependent assemblies into plugin dll
- NOTE!!! There is a bug in the Developers Toolkit 365 where it does NOT deploy plugin step images.  You have to add these manually using the pluginRegistration program.

Plugin Context Logging:
- Set global LogPluginContext to <entityname>, all or none to log context to server
- Execute plugin and check c:\windows\temp\context\<orgname> for context file

Plugin Debugging:
- Use plugin profiling - see this post - https://msdn.microsoft.com/en-us/library/hh372952.aspx
- Use remote debugging - run VS on CRM server, copy pdb files to \server\bin\assembly, attach to wp3.exe process

Deploy Web Resources:
- connect to server as with plugins
- right click Web Resource or CrmPackage and select Deploy

Deploy Workflow Assemblies:
- same procedure as plugins for workflows 

Generate EF Model: (see SP for more detail)
- Get latest source from $/CTSDev1/NextContact-MSD/Dev/ModelGen
- Run modelgen.bat 
  - enter your AD credentials
  - runs several minutes
- Copy CUB.MSD.Model.cs, CUB.MSD.ModelConstants.cs, CUB.MSD.Optionsets.cs  into your source\CUB.MSD.Model folder
- Rebuild solution -deploy plugins if needed
- Todo: add a filtering service to only include entities needed

Generate Constants for EF Model:
- Get latest source from $/CTSDev1/NextContact-MSD/Dev/ModelGen
- Run modelgenConstants.bat (add your credentials first)
- Copy CUB.MSD.ModelConstants into your source\CUB.MSD.Model folder 

Generate OptionSets for EF Model:
- Get latest source from $/CTSDev1/NextContact-MSD/Dev/ModelGen
- Run modelgenOptionset.bat (add your credentials first)
- Copy CUB.MSD.Optionsets.cs to your source\CUB.MSD.Model folder

Console App:
- set credentials in App.config
- use for logic, etc testing

LinqPad:
http://www.linqpad.net/Download.aspx


Client Error Handling Options:
- SDK (Form display): Xrm.Page.ui.setFormNotification(result.DuplicateID, 'ERROR', '1')
                      Xrm.Page.ui.clearFormNotification('1');
- SDK (Field display): Xrm.Page.getControl("fax").setNotification("This is a notification about the Fax field")
- Notify.js (Form display with buttons/links)
   http://www.magnetismsolutions.com/blog/paulnieuwelaar/2015/08/12/Dynamics-CRM-2015-JavaScript-Form-Notifications-on-Steroids
       Notify.add(errorMessage, "ERROR", "emailError",
         [{
             type: "Button",
             text: "View Duplicate",
             callback: function () {
                 //  Notify.add("Contact Opened.", "SUCCESS", "emailError", null, 3);
                 //                     var windowOptions = {
                 //                         openInNewWindow: true
                 //                     };
                 // openEntityForm forces an onSave which throws the dirty warning so can't use
                 //Xrm.Utility.openEntityForm("contact", contactID, windowOptions);
                 var url = Xrm.Page.context.getClientUrl() + "/main.aspx?etn=contact&pagetype=entityrecord&id=" + contactID;
                 window.open(url, '_blank', 'width=750,height=900,status=1,resizable=1,location=no,toolbar=no');
             }
         },
         {
             type: "link",
             text: "Close",
             callback: function () {
                 Notify.remove("emailError");
             }
         }]);

