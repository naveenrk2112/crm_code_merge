/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Fakes;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.QualityTools.Testing.Fakes;
using FakeXrmEasy;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;
using static CUB.MSD.Model.CUBConstants;


namespace CUB.MSD.Plugins.Test
{
    [TestClass]
    public class GlobalGetUserInfoTest
    {
        [TestMethod]
        public void CreateUserInfoTestEasyFake()
        {
            IPlugin fakedPlugin;

            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();

            // fake systemuser entity fetch    
            Entity UserData = UserHelper.GetUser();
            
            // fake roles entity fetch
            List<Entity> data = UserHelper.GetUserRoles();
            data.Add(UserData);

            var context = new XrmFakedContext();

            fakedContext.AddRelationship("systemuserroles_association", new XrmFakedRelationship
            {
                IntersectEntity = "systemuserroles",
                Entity1LogicalName = SystemUser.EntityLogicalName,
                Entity1Attribute = "systemuserid",
                Entity2LogicalName = Role.EntityLogicalName,
                Entity2Attribute = "roleid"
            });

            UserHelper.AssociateUserRoles(fakedService);

            List<Entity> globaldata = GlobalsHelper.GetGlobals();
            data.AddRange(globaldata);

            fakedContext.Initialize(data);
            
            var PlugCtx = fakedContext.GetDefaultPluginContext();
            PlugCtx.UserId = ((SystemUser)UserData).SystemUserId.Value;

            PlugCtx.Stage = MessageProcessingStage.PostOperation;
            PlugCtx.MessageName = GlobalGetUserInfoConstants.ACTION_NAME;

            fakedPlugin = fakedContext.ExecutePluginWith<GlobalGetUserInfo>(PlugCtx);

            var domainName = PlugCtx.OutputParameters[GlobalGetUserInfoConstants.PARAM_DOMAINNAME];
            Assert.AreEqual(domainName, "CARCGNT\\202066");

            var appversion = PlugCtx.OutputParameters[GlobalGetUserInfoConstants.PARAM_APPVERSION];
            Assert.AreEqual(appversion, "PN.8500.99183-1.01.01.0500.Next.Contact");
           // note some data in this plugin cannot be faked (bizid, currency, user fullname...)
        }
    }
}
