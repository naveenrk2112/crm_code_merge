/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using CUB.MSD.Model;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Plugins.Test
{
    public class GlobalsHelper
    {
        public static List<Entity> GetGlobals()
        {
            List<Entity> data = new List<Entity>();
            EntityReference GlobalID = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());
            data.Add(new cub_Globals
            {
                cub_GlobalsId = GlobalID.Id,
                cub_Name = "AppInfo",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = GlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "AppVersion",
                cub_AttributeValue = "PN.8500.99183-1.01.01.0500.Next.Contact",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = GlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "ClientName",
                cub_AttributeValue = "NY",
                cub_Enabled = true
            });

            return data;
        }
    }
}
