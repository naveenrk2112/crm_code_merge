/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CUB.MSD.Logic;
using System.Net.Http;
using FakeXrmEasy;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.QualityTools.Testing.Fakes;
using System.Net.Http.Fakes;
using System.Net;
using static CUB.MSD.Model.CUBConstants;
using CUB.MSD.Plugins.MasterSearch;

namespace CUB.MSD.Plugins.Test
{
    [TestClass]
    public class TransitAccountStatusByIdPluginTest
    {
        [TestMethod]
        public void TransitAccountStatusByIdPluginTestMainTest()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = NisAPIGlobalsHelper.GetNisApiGlobals();
            fakedContext.Initialize(data);
            NISApiLogic nisApi = new NISApiLogic(fakedService);

            var PlugCtx = fakedContext.GetDefaultPluginContext();
            PlugCtx.Stage = MessageProcessingStage.PostOperation;
            PlugCtx.MessageName = TransitAccountConstants.STATUS_ACTION_NAME;
            PlugCtx.InputParameters[TransitAccountConstants.ACCOUNT_ID] = "123456789";
            PlugCtx.InputParameters[TransitAccountConstants.SUBSYSTEM] = "MSD";

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.GetAsyncString = (client, str) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var fakedPlugin = fakedContext.ExecutePluginWith<TransitAccountStatusByIdPlugin>(PlugCtx);
                var result = PlugCtx.OutputParameters[TransitAccountConstants.ACCOUNT_STATUS_OUTPUT_PARAM];
                Assert.AreEqual(result, message);
            }
        }
    }
}
