/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Fakes;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.QualityTools.Testing.Fakes;
using FakeXrmEasy;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;

namespace CUB.MSD.Plugins.Test
{
    [TestClass]
    public class AddressPluginTest
    {
        [TestMethod]
        public void UpdateDependenciesTestEasyFake()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = NisAPIGlobalsHelper.GetNisApiGlobals();
            List<Entity> addressData = AddressHelper.GetAddress();
            data.AddRange(addressData);
            fakedContext.Initialize(data);
            NISApiLogic nisApi = new NISApiLogic(fakedService);

            var PlugCtx = fakedContext.GetDefaultPluginContext();
            PlugCtx.PrimaryEntityName = cub_Address.EntityLogicalName;
            PlugCtx.Stage = MessageProcessingStage.PostOperation;
            PlugCtx.MessageName = ContextMessageName.Update;
            PlugCtx.InputParameters["Target"] = addressData[0];

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var fakedPlugin = fakedContext.ExecutePluginWith<AddressPlugin>(PlugCtx);
            }
        }

        [TestMethod]
        public void AddressPreCreateValidate()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = NisAPIGlobalsHelper.GetNisApiGlobals();
            List<Entity> addressData = AddressHelper.GetAddress();
            data.AddRange(addressData);
            fakedContext.Initialize(data);

            var PlugCtx = fakedContext.GetDefaultPluginContext();
            PlugCtx.PrimaryEntityName = cub_Address.EntityLogicalName;
            PlugCtx.Stage = MessageProcessingStage.PreValidation;
            PlugCtx.MessageName = ContextMessageName.Update;

            #region Duplicate Address
            cub_Address cubAddress0 = addressData[0].ToEntity<cub_Address>();
            cub_Address cubAddress1 = addressData[4].ToEntity<cub_Address>();
            cub_Address modifiedAddress = new cub_Address();

            //set address 0 same as address 1 and update to cause the plugin to fire
            modifiedAddress.cub_Street1 = cubAddress1.cub_Street1;
            modifiedAddress.cub_Street2 = cubAddress1.cub_Street2;
            modifiedAddress.cub_Street3 = cubAddress1.cub_Street3;
            modifiedAddress.cub_City = cubAddress1.cub_City;
            modifiedAddress.cub_StateProvinceId = cubAddress1.cub_StateProvinceId;
            modifiedAddress.cub_address_postalcodeid = cubAddress1.cub_address_postalcodeid;
            modifiedAddress.cub_CountryId = cubAddress1.cub_CountryId;
            modifiedAddress.statecode = cubAddress1.statecode;
            modifiedAddress.cub_AccountAddressId = cubAddress1.cub_AccountAddressId;

            //unmodified version of addressData[0]
            PlugCtx.PreEntityImages["PreUpdateImage"] = cubAddress0;

            //modified version of address to cause a duplicate with addressData[4]
            PlugCtx.InputParameters["Target"] = modifiedAddress;

            try
            {
                var fakedPlugin = fakedContext.ExecutePluginWith<AddressPlugin>(PlugCtx);
            }
            catch (System.Exception ex)
            {
                Assert.AreEqual("Error on AddressPlugin. 10: Update: This address already exists", ex.Message);
            }
            #endregion

            #region Non-duplicate address
            
            //unmodified version of addressData[0]
            PlugCtx.PreEntityImages["PreUpdateImage"] = cubAddress0;

            modifiedAddress.cub_Street1 = "Modified non dup";
            modifiedAddress.cub_Street2 = cubAddress1.cub_Street2;
            modifiedAddress.cub_Street3 = cubAddress1.cub_Street3;
            modifiedAddress.cub_City = cubAddress1.cub_City;
            modifiedAddress.cub_StateProvinceId = cubAddress1.cub_StateProvinceId;
            modifiedAddress.cub_address_postalcodeid = cubAddress1.cub_address_postalcodeid;
            modifiedAddress.cub_CountryId = cubAddress1.cub_CountryId;
            modifiedAddress.statecode = cubAddress1.statecode;
            modifiedAddress.cub_AccountAddressId = cubAddress1.cub_AccountAddressId;

            //modified version of address to cause a duplicate with addressData[4]
            PlugCtx.InputParameters["Target"] = modifiedAddress;

            try
            {
                var fakedPlugin = fakedContext.ExecutePluginWith<AddressPlugin>(PlugCtx);

            }
            catch (System.Exception ex)
            {
                // this should not happen as the address is not a duplicate. 
                // If we end up here for some reason, the assert should fail as the message
                // will be different
                Assert.AreEqual(string.Empty, ex.Message);
            }

            #endregion


        }
    }
}
