/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using CUB.MSD.Model;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Plugins.Test
{
    class AddressHelper
    {
        public static List<Entity> GetAddress()
        {
            List<Entity> data = new List<Entity>();

            cub_Country cntry = (new cub_Country
            {
                cub_CountryId = Guid.NewGuid(),
                cub_Alpha2 = "US"
            });

            cub_ZipOrPostalCode zip = (new cub_ZipOrPostalCode
            {
                cub_ZipOrPostalCodeId = Guid.NewGuid(),
                cub_Code = "60098"
            });

            cub_StateOrProvince state = (new cub_StateOrProvince
            {
                cub_StateOrProvinceId = Guid.NewGuid(),
                cub_StateName = "IL"
            });


            cub_Address addy = (new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_AccountAddressId = new EntityReference(Account.EntityLogicalName, Guid.NewGuid()),
                cub_Street1 = "337 Vine Street",
                cub_Street2 = null,
                cub_City = "Woodstock",
                statecode = cub_AddressState.Active,
                cub_CountryId = new EntityReference(cub_Country.EntityLogicalName, cntry.cub_CountryId.Value),
            });

            addy.cub_StateProvinceId = new EntityReference(cub_StateOrProvince.EntityLogicalName, state.cub_StateOrProvinceId.Value);
            addy.cub_StateProvinceId.Name = state.cub_StateName;
            addy.cub_PostalCodeId = new EntityReference(cub_ZipOrPostalCode.EntityLogicalName, zip.cub_ZipOrPostalCodeId.Value);
            addy.cub_PostalCodeId.Name = zip.cub_Code;

            data.Add(addy);
            data.Add(cntry);
            data.Add(zip);
            data.Add(state);

            //add another address

            addy = (new cub_Address
            {
                cub_AddressId = Guid.NewGuid(),
                cub_AccountAddressId = new EntityReference(Account.EntityLogicalName, Guid.NewGuid()),
                cub_Street1 = "339 Vine Street",
                cub_Street2 = null,
                cub_City = "Woodstock",

                cub_CountryId = new EntityReference(cub_Country.EntityLogicalName, cntry.cub_CountryId.Value),
            });

            addy.cub_StateProvinceId = new EntityReference(cub_StateOrProvince.EntityLogicalName, state.cub_StateOrProvinceId.Value);
            addy.cub_StateProvinceId.Name = state.cub_StateName;
            addy.cub_PostalCodeId = new EntityReference(cub_ZipOrPostalCode.EntityLogicalName, zip.cub_ZipOrPostalCodeId.Value);
            addy.cub_PostalCodeId.Name = zip.cub_Code;

            data.Add(addy);

            return data;
        }
    }
}
