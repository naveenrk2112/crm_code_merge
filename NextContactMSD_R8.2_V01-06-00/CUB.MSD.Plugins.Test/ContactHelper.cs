/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using CUB.MSD.Model;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Plugins.Test
{
    public class ConatctsHelper
    {
        public static List<Entity> GetContacts()
        {

          

            List<Entity> data = new List<Entity>();
           
            data.Add(new Contact
            {
                ContactId = Guid.NewGuid(),
                FirstName = "John",
                LastName = "Smith",
                EMailAddress1 = "John.Smith@msn.com",
                Telephone1 = "(508)415-8569",
                Telephone2 = "(798)512-8965",
                Telephone3= "(785)986-5236"
                 
            });

            data.Add(new Contact
            {
                ContactId = Guid.NewGuid(),
                FirstName = "Johnathan",
                LastName = "Trump",
                EMailAddress1 = "Johnathan.Trump@hotmail.com",
                Telephone1 = "(508)256-8520",
                Telephone2 = "(798)123-7458"
            });

            data.Add(new Contact
            {
                ContactId = Guid.NewGuid(),
                FirstName = "John",
                LastName = "Dowe",
                EMailAddress1 = "JDowe@hotmail.com",
                Telephone1 = "(125)856-8523",
                Telephone2 = "(508)853-8520",
                Telephone3 = "(798)123-7458"
            });
            return data;
        }

        public static Entity GetContact()
        {
            Entity data = (new Contact
            {
                ParentCustomerId = new EntityReference(Account.EntityLogicalName, new Guid("4682d8af-b4aa-e611-80de-005056814569")),
                ContactId = new Guid("5ee2976d-6ab6-e611-80e0-005056813f0c"),
                FirstName = "Tony",
                MiddleName = "S",
                LastName = "Riak",
                EMailAddress1 = "tonyriak@cubic.com",

                Telephone1 = "(508)415-8569",
                cub_Phone1Type = new OptionSetValue(971880001),
                Telephone2 = "(798)512-8965",
                cub_Phone2Type = new OptionSetValue(971880002),
                Telephone3 = "(785)986-5236",
                cub_Phone3Type = new OptionSetValue(971880000)
            });
            return data;
        }
    }
}
