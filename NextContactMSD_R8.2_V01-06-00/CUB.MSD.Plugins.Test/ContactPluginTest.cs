/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Fakes;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.QualityTools.Testing.Fakes;
using FakeXrmEasy;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;

namespace CUB.MSD.Plugins.Test
{
    [TestClass]
    public class ContactPluginTest
    {
        [TestMethod]
        public void CreateTelephoneTestEasyFake()
        {
            var fakedContext = new XrmFakedContext();

            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            List<Entity> data = NisAPIGlobalsHelper.GetNisApiGlobals();
            Entity contactData = ConatctsHelper.GetContact();
            data.Add(contactData);

            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(data);

            var inputParameters = new ParameterCollection();
            inputParameters.Add("Target", contactData);

            var PlugCtx = fakedContext.GetDefaultPluginContext();
            PlugCtx.Stage = MessageProcessingStage.PreOperation;
            PlugCtx.MessageName = ContextMessageName.Create;
            PlugCtx.PrimaryEntityName = Contact.EntityLogicalName;
            PlugCtx.PrimaryEntityId = contactData.Id;
            PlugCtx.InputParameters = inputParameters;

            var fakedPlugin = fakedContext.ExecutePluginWith<ContactPlugin>(PlugCtx);

            Assert.AreEqual(contactData.Attributes[Contact.telephone1Attribute], "5084158569");
            Assert.AreEqual(contactData.Attributes[Contact.telephone2Attribute], "7985128965");
            Assert.AreEqual(contactData.Attributes[Contact.telephone3Attribute], "7859865236");
        }

        [TestMethod]
        public void ReportChangeTestEasyFake()
        {
            var fakedContext = new XrmFakedContext();
            fakedContext.ProxyTypesAssembly = Assembly.Load("CUB.MSD.Model");
            var fakedService = fakedContext.GetOrganizationService();
            List<Entity> data = NisAPIGlobalsHelper.GetNisApiGlobals();
            Entity contactData = ConatctsHelper.GetContact();
            data.Add(contactData);
            fakedContext.Initialize(data);
            NISApiLogic nisApi = new NISApiLogic(fakedService);

            var PlugCtx = fakedContext.GetDefaultPluginContext();
            PlugCtx.PrimaryEntityName = Contact.EntityLogicalName;
            PlugCtx.Stage = MessageProcessingStage.PostOperation;
            PlugCtx.MessageName = ContextMessageName.Update;
            PlugCtx.InputParameters["Target"] = contactData;

            string message = "{\"message\":\"FAKE MESSAGE\"}";
            using (ShimsContext.Create())
            {
                ShimHttpClient.AllInstances.PostAsyncStringHttpContent = (str, cntn, hdr) =>
                {
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(message),
                    };
                    httpResponseMessage.Headers.Add("x-cub-hdr", "{\"result\":\"Successful\",\"uid\":\"uid\",\"fieldName\":\"fieldName\",\"errorKey\":\"errorKey\",\"errorMessage\":\"errorMessage\"}");
                    var task = System.Threading.Tasks.Task.FromResult(httpResponseMessage);
                    return task;
                };
                var fakedPlugin = fakedContext.ExecutePluginWith<ContactPlugin>(PlugCtx);
            }
        }
    }
}
