/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using CUB.MSD.Model;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Plugins.Test
{
    public class CaseHelper
    {
        public static List<Entity> GetCases()
        {

            List<Entity> data = new List<Entity>();

            data.Add(new Incident
            {
                IncidentId = Guid.NewGuid(),
                TicketNumber = "CUB-45212-3874L4",
                Title = "Pass is not working",

            });
            data.Add(new Incident
            {
                IncidentId = Guid.NewGuid(),
                TicketNumber = "CUB-45212-8215L4",
                Title = "Pass is not working",

            });
            data.Add(new Incident
            {
                IncidentId = Guid.NewGuid(),
                TicketNumber = "CUB-45212-3256L4",
                Title = "Pass Replacement",

            });
            data.Add(new Incident
            {
                IncidentId = Guid.NewGuid(),
                TicketNumber = "CUB-45212-8325L4",
                Title = "Loaded value on pass disappeared",

            });
            return data;
        }

        public static Entity GetCase()
        {

            
            return new Incident
            {
                IncidentId = Guid.NewGuid(),
                TicketNumber = "CUB-749321-3L4L4",
                Title = "Your system is perfect",
                

            };
            
        }

    }
}
