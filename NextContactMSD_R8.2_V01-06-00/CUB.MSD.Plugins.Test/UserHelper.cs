/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using CUB.MSD.Model;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;

namespace CUB.MSD.Plugins.Test
{
    public class UserHelper
    {
        public static Entity GetUser()
        {
            // can't set UserName, Currency or BusinessUnit so using first/last name and IDs in test
            Entity data = (new SystemUser
            { 
                Id = new Guid("98F4919E-CDBB-E611-80E0-005056814569"),
                FirstName = "Tim",
                LastName = "Klooster",
                DomainName = "CARCGNT\\202066",
                TransactionCurrencyId = new EntityReference(TransactionCurrency.EntityLogicalName, new Guid("85DAB853-A0A7-E611-80DE-005056814569")),
                BusinessUnitId = new EntityReference(BusinessUnit.EntityLogicalName, new Guid("FF890AE4-9FA7-E611-80DE-005056814569"))
            });
            return data;
        }

        public static List<Entity> GetUserRoles()
        {
            List<Entity> data = new List<Entity>();

            data.Add(new Role
            {
                Id = new Guid("778E0AE4-9FA7-E611-80DE-005056814569"),
                Name = "System Administrator",
            });

            data.Add(new Role
            {
                Id = new Guid("2EA20AE4-9FA7-E611-80DE-005056814569"),
                Name = "System Customizer"
            });

            return data;
        }

        public static Logic.UserInfo GetUserInfo()
        {
            Logic.UserInfo ui = new Logic.UserInfo();
            ui.AppVersion = "PN.8500.99183-1.01.01.0500.Next.Contact";
            ui.ClientName = "NY";
            ui.SystemName = "NextContact-DEV";

            return ui;
        }

        public static void AssociateUserRoles(IOrganizationService fakedService)
        {
            var roleToUser = new AssociateRequest
            {
                Target = new EntityReference("systemuser", new Guid("98F4919E-CDBB-E611-80E0-005056814569")),

                RelatedEntities = new EntityReferenceCollection
                {
                    new EntityReference("role", new Guid("778E0AE4-9FA7-E611-80DE-005056814569"))
                },
                Relationship = new Relationship("systemuserroles_association")
            };

            fakedService.Execute(roleToUser);

            var role2ToUser = new AssociateRequest
            {
                Target = new EntityReference("systemuser", new Guid("98F4919E-CDBB-E611-80E0-005056814569")),

                RelatedEntities = new EntityReferenceCollection
                {
                    new EntityReference("role", new Guid("2EA20AE4-9FA7-E611-80DE-005056814569"))
                },
                Relationship = new Relationship("systemuserroles_association")
            };

            fakedService.Execute(role2ToUser);

        }
    }
}
