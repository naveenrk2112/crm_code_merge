/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using CUB.MSD.Model;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Plugins.Test
{
    public class NisAPIGlobalsHelper
    {
        public static List<Entity> GetNisApiGlobals()
        {
            List<Entity> data = new List<Entity>();
            EntityReference nisApiGlobalID = new EntityReference(cub_Globals.EntityLogicalName, Guid.NewGuid());
            data.Add(new cub_Globals
            {
                cub_GlobalsId = nisApiGlobalID.Id,
                cub_Name = "NISApi",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "BaseUrl",
                cub_AttributeValue = "http://172.23.4.196:8201/nis",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "Device",
                cub_AttributeValue = "MSD",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "Username",
                cub_AttributeValue = "username",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "Password",
                cub_AttributeValue = "password",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "TransitAccount",
                cub_AttributeValue = "/nwapi/v2/transitaccount/{0}/subsystem/{1}",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "CompleteRegistrationPath",
                cub_AttributeValue = "/nwapi/v2/customer/{0}/completeregistration",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = "SearchTokenPath",
                cub_AttributeValue = "/csapi/v1/subsystem/{0}/traveltoken/search",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = CUBConstants.Globals.NIS_API_REPORT_CHANGE,
                cub_AttributeValue = "/nwapi/v2/customer/{0}/contact/{1}/reportchange",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = CUBConstants.Globals.NIS_API_UPDATE_DEPENDANCIES,
                cub_AttributeValue = "/nwapi/v2/customer/{0}/address/{1}/updatedependencies",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_BALANCE_HISTORY,
                cub_AttributeValue = "/nwapi/v2/transitaccount/{0}/subsystem/{1}/balancehistory?startDateTime={2}T00:00:00.000Z&endDateTime={3}T23:59:59.999Z",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = CUBConstants.Globals.NIS_API_TRANSIT_ACCOUNT_TRAVEL_HISTORY,
                cub_AttributeValue = "/nwapi/v2/transitaccount/{0}/subsystem/{1}/travelhistory?startDateTime={2}T00:00:00.000Z&endDateTime={3}T23:59:59.999Z",
                cub_Enabled = true
            });
            data.Add(new cub_GlobalDetails
            {
                cub_GlobalId = nisApiGlobalID,
                cub_GlobalDetailsId = Guid.NewGuid(),
                cub_Name = CUBConstants.Globals.NIS_API_ADDRESS_DEPENDENCIES,
                cub_AttributeValue = "/csapi/v1/customer/{0}/address/{1}/dependencies",
                cub_Enabled = true
            });
            return data;
        }
    }
}
