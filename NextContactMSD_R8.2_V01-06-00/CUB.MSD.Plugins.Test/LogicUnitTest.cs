/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Collections.Generic;
using CUB.MSD.Logic;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;

namespace CUB.MSD.Plugins.Test
{
    [TestClass]
    public class LogicUnitTest
    {
        [TestMethod]
        public void RetrieveMethod()
        {
            var fakedContext = new XrmFakedContext();

            cub_Country country = new cub_Country();
            country.cub_CountryId = Guid.NewGuid();
            country.cub_CountryName = "United States";
            country.cub_Alpha2 = "US";

            cub_StateOrProvince state = new cub_StateOrProvince();
            state.cub_StateOrProvinceId = Guid.NewGuid();
            state.cub_Abbreviation = "CA";

            cub_ZipOrPostalCode zip = new cub_ZipOrPostalCode();
            zip.cub_ZipOrPostalCodeId = Guid.NewGuid();
            zip.cub_Code = "1234567";

            List<Entity> data = new List<Entity>();
            data.Add(country);
            data.Add(state);
            data.Add(zip);

            var fakedService = fakedContext.GetOrganizationService();
            fakedContext.Initialize(data);

            LogicBase logic = new LogicBase(fakedService);
            Assert.AreEqual(logic.Retrieve<cub_Country>(country.cub_CountryId.Value, cub_Country.cub_alpha2Attribute).cub_Alpha2, country.cub_Alpha2);
            Assert.AreEqual(logic.Retrieve<cub_StateOrProvince>(state.cub_StateOrProvinceId.Value, cub_StateOrProvince.cub_abbreviationAttribute).cub_Abbreviation, state.cub_Abbreviation);
            Assert.AreEqual(logic.Retrieve<cub_ZipOrPostalCode>(zip.cub_ZipOrPostalCodeId.Value, cub_ZipOrPostalCode.cub_codeAttribute).cub_Code, zip.cub_Code);
        }
    }
}
