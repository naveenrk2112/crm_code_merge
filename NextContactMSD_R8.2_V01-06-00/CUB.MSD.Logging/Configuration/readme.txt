﻿Logging Setup



Directories and Configuration

The log configuration file is located in c:\CUBMSDLogging\log.config. There is inline documentation within this config file; more information about log4net configuration is at http://logging.apache.org/log4net/release/manual/configuration.html. Logs are written to c:\CUBMSDLogging\logs\CUBMSDLog.txt. The log file will roll to CUBMSDLog.x.txt after a configurable size threshold is reached (10MB by default). The size threshold and number of rolled files are configurable in log.config. log.config must be located in c:\CUBMSDLogging.



NOTE: plugins are not able to use log4net's remotingAppender. For some reason, plugins don't seem to be calling the tcp sink, so the windows service at the end of the sink never gets the log entries. Until a solution is found, do not use the Windows Service and remotingAppender. This may be related to a similar problem faced by the CRM 2011 Appender for log4net (http://crm2011appender.codeplex.com/) -- see their note at the top of the page: "CRM Appender currently has issues when used inside of plugins. The CRM Appender needs to be instantiated per each plugin to get the correct organization service each time. This causes an issue with duplicate log messages due to the way log4net works."

Until this issue is resolved a possible workaround is to create separate FileAppenders for each process (1 for plugins & workflows, 1 for IIS). The Windows Service and environment variable are not necessary to enable logging using FileAppenders.



Windows Service

A windows service is required to allow multiple processes to write to a single log file without conflict. 
The service uses a tcp port (default is 8085, which is configurable in the service's config file). The firewall must allow traffic to this port. 
Use the setup program to install the service. Once installed, the service and its config file are located in %program files (x86)%\CUBMSD\Log4net Remoting Server Service.

Development environments don't need the Windows Service. To enable logging without the service, edit c:\CUBMSDlogging\log.config. 
Comment out the remotingAppender and un-comment the FileAppender:
	<!-- <appender-ref ref="remotingAppender" /> -->
	<appender-ref ref="FileAppender" />



Environment Variable

The windows service requires an environment variable, REMOTELOG4NETLOGDIR, to be set to the location of the log directory. 
The variable must include the trailing backslash: c:\CUBMSDLogging\logs\



SUMMARY: SETTING UP LOGGING USING REMOTE APPENDER AND WINDOWS SERVICE

1) Create environment variable REMOTELOG4NETLOGDIR and set to c:\CUBMSDLogging\logs\  (include the trailing backslash)
2) copy log.config to c:\CUBMSDLogging
3) Install the Windows Service
4) Configure Windows Firewall to allow traffic on port 8085 (if the port needs to be changed, set it in the service's config file and restart the service).
5) Ensure that log processes have read/write access to c:\CUBMSDLogging and c:\CUBMSDLogging\logs

SUMMARY: SETTING UP LOGGING USING FILE APPENDER

1) copy log.config to c:\CUBMSDLogging
2) Ensure that log processes have read/write access to c:\CUBMSDLogging and c:\CUBMSDLogging\logs
