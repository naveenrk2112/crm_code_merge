/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

[assembly: CLSCompliant(true)]

namespace CUB.MSD.Logging
{
    [CLSCompliant(true)]
    public enum LogLevel
    {
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5
    }

    public class Utility
    {
        private static Configuration _configFile; 
        //private static XmlDocument GetLogConfigDocument()
        //{
        //    const string configFile = "c:\\CUBMSDLogging\\log.config";
        //    var configXml = new XmlDocument();

        //    try
        //    {
        //        if (!System.IO.File.Exists(configFile))
        //            return configXml;
        //    }
        //    catch (Exception)
        //    {
        //        return configXml;
        //    }

        //    try
        //    {
        //        configXml.Load(configFile);
        //    }
        //    catch (Exception)
        //    {
        //        return configXml;
        //    }

        //    return configXml;
        //}

        internal static void LoadConfigFile(string configFile)
        {
            ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
            configMap.ExeConfigFilename = configFile;
            _configFile = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
        }

        /// <summary>
        /// Returns the org's log level from the log config file (c:\CUBMSDLogging\log.config)
        /// </summary>
        /// <param name="orgName"></param>
        /// <returns></returns>
        //public static LogLevel GetOrgLogLevel(string orgName = "")
        //{
        //    const LogLevel defaultLogLevel = LogLevel.Info;

        //    if (string.IsNullOrEmpty(orgName)) orgName = "default";

        //    var configXml = GetLogConfigDocument();

        //    try
        //    {
        //        var node = configXml.SelectSingleNode(@"configuration/orgLoggingLevels/org[@name='" + orgName + "']");

        //        if (null == node || null == node.Attributes || null == node.Attributes["value"])
        //        {
        //            node = configXml.SelectSingleNode(@"configuration/orgLoggingLevels/org[@name='default']");
        //            if (null == node || null == node.Attributes || null == node.Attributes["value"])
        //                return defaultLogLevel;
        //        }

        //        LogLevel orgLogLevel;
        //        if (!LogLevel.TryParse(node.Attributes["value"].Value, true, out orgLogLevel))
        //        {
        //            var sb = new StringBuilder();
        //            foreach (LogLevel logLevel in (LogLevel[])Enum.GetValues(typeof(LogLevel)))
        //            {
        //                sb.Append(((sb.Length == 0) ? "" : ", ") + logLevel.ToString());
        //            }

        //            var warning = string.Format("Organization '{0}' contains an invalid log level: '{1}'. Valid log levels are {2}.", orgName, node.Attributes["value"].Value, sb.ToString());

        //            Logger.Log(LogLevel.Warn, warning);
        //            WriteToEventLog(warning, EventLogEntryType.Warning);

        //            return defaultLogLevel;
        //        }
        //        return orgLogLevel;
        //    }
        //    catch
        //    {
        //        return defaultLogLevel;
        //    }
        //}

        internal static LogLevel GetConfigLogLevel()
        {
            LogLevel returnLogLevel = LogLevel.Info;
            if (log4net.GlobalContext.Properties["ConfigLogLevel"] != null)
            {
                returnLogLevel = (LogLevel)log4net.GlobalContext.Properties["ConfigLogLevel"];
            }
            else if (!string.IsNullOrEmpty(Utility.LogLevelString))
            {
                if (Enum.TryParse(Utility.LogLevelString, out returnLogLevel))
                {
                    log4net.GlobalContext.Properties["ConfigLogLevel"] = returnLogLevel;
                }
            }

            return returnLogLevel;

        }

        /// <summary>
        /// Removes all old files in the log directory
        /// </summary>
        //public static void DeleteOldLogs()
        //{
        //    var configXml = GetLogConfigDocument();

        //    var filenodes = configXml.SelectNodes(@"configuration/log4net/appender/file");

        //    if (null == filenodes || filenodes.Count == 0)
        //        return;

        //    var daysToKeep = 30;

        //    var node = configXml.SelectSingleNode(@"configuration/maxDays");

        //    if (null != node && null != node.Attributes && node.Attributes["value"] != null && !string.IsNullOrEmpty(node.Attributes["value"].Value))
        //    {
        //        if (!int.TryParse(node.Attributes["value"].Value, out daysToKeep))
        //            daysToKeep = 30;
        //    }

        //    // get directory for each file appender and enumerate log files, deleting files as necessary
        //    foreach (XmlNode filenode in filenodes)
        //    {
        //        try
        //        {
        //            if (null == filenode.Attributes)
        //                continue;

        //            if (filenode.Attributes["value"] == null || string.IsNullOrEmpty(filenode.Attributes["value"].Value))
        //                continue;

        //            var filename = filenode.Attributes["value"].Value;

        //            // shouldn't use built-in path parsing functions because paths can have log4net placeholders
        //            //      that will break those functions
        //            var lastSlash = filename.LastIndexOf('\\');

        //            if (lastSlash < 0)
        //                continue;

        //            var directory = filename.Substring(0, lastSlash);

        //            if (!System.IO.Directory.Exists(directory))
        //                continue;

        //            var files = System.IO.Directory.GetFiles(directory);

        //            foreach (var file in files)
        //            {
        //                var lastWriteTime = System.IO.File.GetLastWriteTime(file);
        //                if (new TimeSpan(DateTime.Now.Ticks - lastWriteTime.Ticks).TotalDays <= daysToKeep)
        //                    continue;

        //                try
        //                {
        //                    System.IO.File.Delete(file);
        //                }
        //                catch
        //                {
        //                }
        //            }

        //        }
        //        catch
        //        {
        //        }
        //    }
        //}

        public static void WriteToEventLog(string message, System.Diagnostics.EventLogEntryType entryType = EventLogEntryType.Information)
        {
            try
            {
                // requires registry key HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\eventlog\Application\Cubic
                if (!EventLog.SourceExists("Cubic")) //change this
                {
                    EventLog.CreateEventSource("Cubic", "Application");
                }
                EventLog.WriteEntry("Cubic", message, entryType);
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
            }
        }

        //public static string GetConnectionString()
        //{
        //    var configXml = GetLogConfigDocument();

        //    try
        //    {
        //        var node = configXml.SelectSingleNode(@"configuration/connectionstring");

        //        if (null == node || null == node.Attributes || null == node.Attributes["value"])
        //        {
        //            return "";
        //        }

        //        var connectionString = node.Attributes["value"].Value;

        //        // If username and password are configured, unencrypt them and add them to the connection string
        //        if (node.Attributes["username"] != null && !string.IsNullOrEmpty(node.Attributes["username"].Value))
        //        {
        //            if (!connectionString.EndsWith(";")) connectionString += ";";
        //            connectionString += "User Id=" + StringCipher.Decrypt(node.Attributes["username"].Value, "weckl");
        //        }

        //        if (node.Attributes["password"] != null && !string.IsNullOrEmpty(node.Attributes["password"].Value))
        //        {
        //            if (!connectionString.EndsWith(";")) connectionString += ";";
        //            connectionString += "password=" + StringCipher.Decrypt(node.Attributes["password"].Value, "weckl");
        //        }

        //        return connectionString;
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //}

        #region encryption

        // added this class to decrypt user name and password from config file. Copied from LogicBase since I don't want to create a circular reference between logging and CRM2011Logic
        // usages:
        //
        // var encrypt = LogicBase.StringCipher.Encrypt("somepassword", "weckl");  
        // var decrypt = LogicBase.StringCipher.Decrypt(encrypt, "weckl");
        public static class StringCipher
        {
            // This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
            // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
            // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.

            // This constant is used to determine the keysize of the encryption algorithm.
            private const int keysize = 256;
            private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");

            public static string Encrypt(string plainText, string passPhrase)
            {
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                using (var password = new PasswordDeriveBytes(passPhrase, null))
                {
                    byte[] keyBytes = password.GetBytes(keysize / 8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.Mode = CipherMode.CBC;
                        using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                        {
                            using (var memoryStream = new MemoryStream())
                            {
                                using (
                                    var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
                                    )
                                {
                                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                    cryptoStream.FlushFinalBlock();
                                    byte[] cipherTextBytes = memoryStream.ToArray();
                                    return Convert.ToBase64String(cipherTextBytes);
                                }
                            }
                        }
                    }
                }
            }

            public static string Decrypt(string cipherText, string passPhrase)
            {
                byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
                using (var password = new PasswordDeriveBytes(passPhrase, null))
                {
                    byte[] keyBytes = password.GetBytes(keysize / 8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.Mode = CipherMode.CBC;
                        using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                        {
                            using (var memoryStream = new MemoryStream(cipherTextBytes))
                            {
                                using (
                                    var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                {
                                    var plainTextBytes = new byte[cipherTextBytes.Length];
                                    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                    return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        internal static string LogDBConnection
        {
            get
            {
                string returnValue = string.Empty;
                if (log4net.GlobalContext.Properties["LogDBConnection"] != null)
                {
                    returnValue = log4net.GlobalContext.Properties["LogDBConnection"].ToString();
                }
                else
                {
                    log4net.GlobalContext.Properties["LogDBConnection"] = _configFile.ConnectionStrings.ConnectionStrings["LogDBConnection"].ConnectionString;
                    returnValue = log4net.GlobalContext.Properties["LogDBConnection"].ToString();
                }
                return returnValue;
            }
        }
        internal static string OrgName
        {
            get
            {
                return GetConfigAppSetting("OrgName");
            }
        }
        internal static string LogLevelString
        {
            get
            {
                return GetConfigAppSetting("LogLevel");
            }
        }

        internal static string LogTable
        {
            get
            {
                return GetConfigAppSetting("LogTable");
            }
        }

        internal static string GetConfigAppSetting(string settingName)
        {
            return _configFile.AppSettings.Settings[settingName].Value;
            //return ConfigurationManager.AppSettings[settingName];
        }

        internal static string GetLoggingConfigFilePath(string configFilePath)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "log4net.config");
            //check if file exists
            if (!File.Exists(path)) path = configFilePath;
            return path;
        }
    }

    public class DataToLog
    {
        private Exception _exception = null;

        public MethodBase method { get; set; }
        public LogLevel level { get; set; } = LogLevel.Error;
        public object message { get; set; }
        public string orgName { get; set; }
        public string entityName { get; set; }
        public Exception exception
        {
            get { return _exception; }
            set
            {
                _exception = value;
                exceptionString = JsonConvert.SerializeObject(_exception);
            }
        }
        public string request { get; set; }
        public string response { get; set; }
        public DateTime? requestTime { get; set; }
        public DateTime? responseTime { get; set; }
        public string exceptionString { get; set; }
        public string machineName { get { return Environment.MachineName; } }
        public string userName { get { return Environment.UserName; } }
        public string runDuration
        {
            get
            {
                string returnValue = string.Empty;
                if (requestTime.HasValue && responseTime.HasValue)
                {
                    try
                    {
                        returnValue = (responseTime.Value - requestTime.Value).Milliseconds.ToString();
                    }
                    catch (Exception)
                    {}
                }
                return returnValue;
            }
        }



    }


}
