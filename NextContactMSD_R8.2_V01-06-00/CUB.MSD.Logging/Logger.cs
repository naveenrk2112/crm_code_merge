/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using log4net;
using log4net.Appender;
using System.Configuration;

namespace CUB.MSD.Logging
{
    public static class Logger
    {
        private static string _configFile = @"c:\CUBMSDLogging\log4net.config";

        static Logger()
        {
            // SS - NOTE: this may not get the correct log file as in case of executing from the plug-in registraiton tool
            _configFile = Utility.GetLoggingConfigFilePath(_configFile);

            log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(_configFile));
            Utility.LoadConfigFile(_configFile);
        }

        //private static readonly object InitializedLoggerLock = new object();

        //private static Boolean isLoggerInititalized = false;

        //private const string _mutexGuidId = "DE848DD1-5C4C-4DC8-8B53-D8BAE664B547";

        //private static Mutex _mutex = null;

        //private static string _mutexId = "";

        //private static void InitMutex()
        //{
        //    if (null != _mutex) return;
        //    _mutexId = new Guid(_mutexGuidId).ToString();
        //    _mutex = new Mutex(false, "Global\\CUBMSDLogging-" + _mutexId);

        //    var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
        //    var securitySettings = new MutexSecurity();
        //    securitySettings.AddAccessRule(allowEveryoneRule);
        //    _mutex.SetAccessControl(securitySettings);

        //}

        //private static ILog GetLogger(string source, string orgName)
        //{
        //    var typeName = "unknownType";
        //    try
        //    {
        //        typeName = MethodBase.GetCurrentMethod().DeclaringType.Name;
        //    }
        //    catch (Exception)
        //    {
        //        //throw;
        //    }

        //    source = (string.IsNullOrEmpty(source)) ? "SourceUnknown" : source;
        //    orgName = (string.IsNullOrEmpty(orgName)) ? "OrgUnknown" : orgName;

        //    var logId = string.Format("{0}{1}{2}.{3}", Process.GetCurrentProcess().ProcessName,
        //                              string.IsNullOrEmpty(source) ? "" : "." + source,
        //                              string.IsNullOrEmpty(orgName) ? "" : "." + orgName,
        //                              Process.GetCurrentProcess().Id.ToString());

        //    //log4net.Util.SystemInfo.NullText = "";
        //    //ThreadContext.Properties["source"] = source;
        //    //ThreadContext.Properties["orgname"] = orgName;
        //    //ThreadContext.Properties["processname"] = Process.GetCurrentProcess().ProcessName;

        //    ILog logger = null;

        //    #region OLD Logging

        //    //if (LogManager.GetCurrentLoggers().Length == 0 || LogManager.Exists(logId) == null)
        //    //{
        //    //    // load logger config with XmlConfigurator
        //    //    log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(_configFile));
        //    //}

        //    //logger = LogManager.GetLogger(logId);

        //    #endregion

        //    #region NEW Logging

        //    var loggerExists = LogManager.Exists(logId);

        //    if (LogManager.GetCurrentLoggers().Length == 0 || loggerExists == null)
        //    {
        //        log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(_configFile));
        //    }

        //    logger = LogManager.GetLogger(logId);
        //    var iLogger = ((log4net.Repository.Hierarchy.Logger)logger.Logger);
        //    iLogger.Additivity = false;

        //    var logAppender = FindAppender(logId);

        //    if ((logAppender == null && iLogger.Appenders.Count <= 0) || loggerExists == null)
        //    {
        //        // add logger. If connection string exists, add db logger, otherwise add file appender logger
        //        iLogger.RemoveAllAppenders();

        //        var connectionString = Utility.GetConnectionString();
        //        if (!string.IsNullOrEmpty(connectionString))
        //        {
        //            var dbAppender = Logger.CreateDatabaseAppender(logId, connectionString, orgName);
        //            iLogger.AddAppender(dbAppender);
        //        }
        //        else
        //        {
        //            var fileAppender = Logger.CreateFileAppender(logId, orgName, source,
        //                Process.GetCurrentProcess().ProcessName, Process.GetCurrentProcess().Id.ToString());
        //            iLogger.AddAppender(fileAppender);
        //        }

        //    }

        //    #endregion


        //    return logger;
        //}

        private static ILog GetLogger(DataToLog logData)
        {

            ILog logger = LogManager.GetLogger(logData.method.DeclaringType.FullName);

            #region Add DB Appender

            //NOTE: Unable to append DB logger from the config file, adding it in the code for now
            var iLogger = ((log4net.Repository.Hierarchy.Logger)logger.Logger);
            var dbAppender = FindAppender(logData.entityName);
            if (dbAppender == null)     // if not in the repository
            {
                iLogger.Additivity = false;
                var connectionString = Utility.LogDBConnection;
                if (!string.IsNullOrEmpty(connectionString))
                {
                    dbAppender = Logger.CreateDatabaseAppender(logData, connectionString);
                    iLogger.AddAppender(dbAppender);
                }
            }
            else
            {
                //in the repository but may not be in the current logger list of appenders
                if (!iLogger.Appenders.Contains(dbAppender))
                    iLogger.AddAppender(dbAppender);
            }

            #endregion Add DB Appender

            return logger;
        }


        #region Interfaces and classes for encapsulating logging for each log level
        private interface ILogLevelRule
        {
            bool IsMatch(LogLevel logLevel);
            //void Log(string message, Exception exception = null, string source = "", string orgName = "");
            void Log(DataToLog logData);
        }

        private class DebugLogLevelRule : ILogLevelRule
        {
            public bool IsMatch(LogLevel logLevel)
            {
                return logLevel == LogLevel.Debug;
            }

            //public void Log(string message, Exception exception = null, string source = "", string orgName = "")
            //{
            //    // Wait until it is safe to enter.
            //    //InitMutex();
            //    //_mutex.WaitOne();
            //    GetLogger(source, orgName).Debug(message, exception);
            //    //_mutex.ReleaseMutex();
            //}

            public void Log(DataToLog logData)
            {
                GetLogger(logData).Debug(logData.message, logData.exception);
            }

        }

        private class InfoLogLevelRule : ILogLevelRule
        {
            public bool IsMatch(LogLevel logLevel)
            {
                return logLevel == LogLevel.Info;
            }

            //public void Log(string message, Exception exception = null, string source = "", string orgName = "")
            //{
            //    // Wait until it is safe to enter.
            //    //InitMutex();
            //    //_mutex.WaitOne();
            //    GetLogger(source, orgName).Info(message, exception);
            //    //_mutex.ReleaseMutex();
            //}
            public void Log(DataToLog logData)
            {
                GetLogger(logData).Info(logData.message, logData.exception);
            }
        }

        private class WarnLogLevelRule : ILogLevelRule
        {
            public bool IsMatch(LogLevel logLevel)
            {
                return logLevel == LogLevel.Warn;
            }

            //public void Log(string message, Exception exception = null, string source = "", string orgName = "")
            //{
            //    // Wait until it is safe to enter.
            //    //InitMutex();
            //    //_mutex.WaitOne();
            //    GetLogger(source, orgName).Warn(message, exception);
            //    //_mutex.ReleaseMutex();
            //}
            public void Log(DataToLog logData)
            {
                GetLogger(logData).Warn(logData.message, logData.exception);
            }
        }

        private class ErrorLogLevelRule : ILogLevelRule
        {
            public bool IsMatch(LogLevel logLevel)
            {
                return logLevel == LogLevel.Error;
            }

            //public void Log(string message, Exception exception = null, string source = "", string orgName = "")
            //{
            //    // Wait until it is safe to enter.
            //    //InitMutex();
            //    //_mutex.WaitOne();
            //    GetLogger(source, orgName).Error(message, exception);
            //    //_mutex.ReleaseMutex();
            //}
            public void Log(DataToLog logData)
            {
                GetLogger(logData).Error(logData.message, logData.exception);
            }
        }

        private class FatalLogLevelRule : ILogLevelRule
        {
            public bool IsMatch(LogLevel logLevel)
            {
                return logLevel == LogLevel.Fatal;
            }

            //public void Log(string message, Exception exception = null, string source = "", string orgName = "")
            //{
            //    // Wait until it is safe to enter.
            //    //InitMutex();
            //    //_mutex.WaitOne();
            //    GetLogger(source, orgName).Fatal(message, exception);
            //    //_mutex.ReleaseMutex();
            //}
            public void Log(DataToLog logData)
            {
                GetLogger(logData).Fatal(logData.message, logData.exception);
            }
        }

        #endregion

        #region helper functions to add Org Name and code source (assembly, namespace, method name) to the message
        /// <summary>
        /// Adds code source (assembly, namespace, method name) to the front of the message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private static string PrependSource(string message, MethodBase callingMethod = null)
        {
            var stacktrace = new StackTrace();
            if (stacktrace.FrameCount < 3)
                return message;

            if (null == callingMethod)
                callingMethod = stacktrace.GetFrame(2).GetMethod();
            return string.Format("{0}.{1}\t{2}", callingMethod.ReflectedType.FullName, callingMethod.Name, message);
        }

        /// <summary>
        /// Adds the orgname to the front of the message
        /// </summary>
        /// <param name="orgName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private static string PrependOrgName(string orgName, string message)
        {
            if (string.IsNullOrEmpty(orgName))
                return message;

            if (string.IsNullOrEmpty(message))
                return orgName;

            return string.Format("{0}\t{1}", orgName, message);
        }

        #endregion

        //public static bool DoLog(string orgName, LogLevel logLevel)
        //{
        //    var orgLogLevel = Utility.GetOrgLogLevel(orgName);
        //    return ((int)orgLogLevel <= (int)logLevel);
        //}

        internal static bool DoLog(LogLevel logLevel)
        {
            var orgLogLevel = Utility.GetConfigLogLevel();
            return ((int)orgLogLevel <= (int)logLevel);
        }

        /// <summary>
        /// Creates a log entry using log4net
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        /// <param name="orgName">If provided, the orgname will be included in the tab-delimited entry, and the org's log level will be evaluated to determine whether to log the message.</param>
        /// <param name="exception">If provided, the exception will be logged according to log4net's conversion pattern in its config file</param>
        /// <param name="callingMethod">If not provided, the default calling method is assumed to be the method that calls this function. The default method may not be useful if it's a generic method like "LogEvent", so callingMethod allows generic log handlers to provide the method which was originally called.</param>
        /// <param name="source">If provided, the log file name can include this identifier by using the %property{source} decorator in the log4net file's pattern string. This avoids logging conflicts that occur between workflows, plugins, and the IIS process</param>
        //public static void Log(LogLevel level, string message, string orgName = "", Exception exception = null, MethodBase callingMethod = null, string source = "")
        //{
        //    try
        //    {
        //        if (!DoLog(orgName, level))
        //            return;

        //        var loggingRules = new List<ILogLevelRule>
        //        {
        //            new DebugLogLevelRule(),
        //            new InfoLogLevelRule(),
        //            new WarnLogLevelRule(),
        //            new ErrorLogLevelRule(),
        //            new FatalLogLevelRule()
        //        };

        //        var logger = loggingRules.FirstOrDefault(lr => lr.IsMatch(level));
        //        //if (null == logger)
        //        //    return;

        //        Utility.DeleteOldLogs();

        //        message = PrependSource(PrependOrgName(orgName, message), callingMethod);

        //        logger.Log(message, exception, source, orgName);
        //    }
        //    catch (Exception ex)
        //    {
        //        //throw;
        //    }
        //}

        internal static void Log(DataToLog logData)
        {

            if (!DoLog(logData.level))
                return;

            var loggingRules = new List<ILogLevelRule>
            {
                new DebugLogLevelRule(),
                new InfoLogLevelRule(),
                new WarnLogLevelRule(),
                new ErrorLogLevelRule(),
                new FatalLogLevelRule()
            };

            var logger = loggingRules.FirstOrDefault(lr => lr.IsMatch(logData.level));

            logger.Log(logData);

        }

        //public static IAppender CreateFileAppender(string name, string orgname, string source, string processname, string processid)
        //{
        //    var fileName =
        //        String.Format("c:\\CUBMSDLogging\\logs\\CUBMSD-{0}-{1}-{2}-{3}.log", orgname, source, processname, processid);

        //    var appender = new RollingFileAppender {Name = name, File = fileName, AppendToFile = true};
        //    var layout = new
        //  log4net.Layout.PatternLayout { ConversionPattern = "%utcdate\t%date\t%level\t%message\t%exception%newline" };
        //    //layout.Header = "UTC Date \tLocal Date\tLevel\tSource\n";
        //    layout.ActivateOptions();
  
        //    // Appender Settings
        //    appender.PreserveLogFileNameExtension = true;
        //    appender.AppendToFile = true;
        //    appender.RollingStyle = RollingFileAppender.RollingMode.Size;
        //    appender.MaxSizeRollBackups = 100;
        //    appender.MaxFileSize = 10000000;
        //    appender.StaticLogFileName = true;
        //    appender.Layout = layout;
        //    appender.ActivateOptions();

        //    return appender;
        //}

        //public static IAppender CreateDatabaseAppender(string name, string connectionString, string orgName = "")
        //{
        //    var appender = new AdoNetAppender {
        //        Name = name,
        //        BufferSize = 1,
        //        ConnectionType = "System.Data.SqlClient.SqlConnection, System.Data, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089",
        //        ConnectionString = connectionString,
        //        CommandText = "INSERT INTO cubmsdlog ([date],[thread],[level],[logger],[orgname],[message]) VALUES (@log_date, @thread, @log_level, @logger, @orgname, @message)"
        //    };

        //    // Appender Settings

        //    // getting sql error on bad date format
        //    appender.AddParameter(new AdoNetAppenderParameter()
        //    {
        //        ParameterName = "@log_date",
        //        DbType = System.Data.DbType.DateTime,
        //        Size = 255,
        //        Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%date{yyyy'-'MM'-'dd HH':'mm':'ss'.'fff}"))
        //    });

        //    appender.AddParameter(new AdoNetAppenderParameter()
        //    {
        //        ParameterName = "@thread",
        //        DbType = System.Data.DbType.String,
        //        Size = 255,
        //        Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%thread"))
        //    });

        //    appender.AddParameter(new AdoNetAppenderParameter()
        //    {
        //        ParameterName = "@log_level",
        //        DbType = System.Data.DbType.String,
        //        Size = 20,
        //        Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%level"))
        //    });

        //    appender.AddParameter(new AdoNetAppenderParameter()
        //    {
        //        ParameterName = "@logger",
        //        DbType = System.Data.DbType.String,
        //        Size = 255,
        //        Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%logger"))
        //    });

        //    appender.AddParameter(new AdoNetAppenderParameter()
        //    {
        //        ParameterName = "@orgname",
        //        DbType = System.Data.DbType.String,
        //        Size = 255,
        //        Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout(orgName))
        //    });

        //    appender.AddParameter(new AdoNetAppenderParameter()
        //    {
        //        ParameterName = "@message",
        //        DbType = System.Data.DbType.String,
        //        Size = 4000,
        //        Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%message"))
        //    });

        //    appender.ActivateOptions();

        //    return appender;
        //}

        internal static IAppender CreateDatabaseAppender(DataToLog logData, string connectionString)
        {
            var appender = new AdoNetAppender
            {
                Name = logData.entityName,
                BufferSize = 1,
                ConnectionType = "System.Data.SqlClient.SqlConnection, System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089",
                ConnectionString = connectionString,
                CommandText = string.Format("INSERT INTO {0} ([Create_Time],[Thread],[Log_Level],[Logger],[OrgName],[Log_Message],[Log_Exception],[Method_Name],[Request_Time],[Request],[Response_Time],[Response],[Machine_Name],[User_Name],[Correlation_Id], [Run_Duration]) VALUES (@log_date, @thread, @log_level, @logger, @orgname, @message, @Log_Exception, @methodname, @Request_Time, @ApiRequest, @Response_Time, @ApiResponse, @Machine_Name, @User_Name, @CUB_Correlation_Id, @Run_Duration)",
                                logData.entityName)
            };

            // Appender Settings

            // getting sql error on bad date format
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@log_date",
                DbType = System.Data.DbType.DateTime,
                Size = 255,
                Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%date{yyyy'-'MM'-'dd HH':'mm':'ss'.'fff}"))
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@thread",
                DbType = System.Data.DbType.String,
                Size = 255,
                Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%thread"))
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@log_level",
                DbType = System.Data.DbType.String,
                Size = 20,
                Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%level"))
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@logger",
                DbType = System.Data.DbType.String,
                Size = 255,
                Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%logger"))
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@orgname",
                DbType = System.Data.DbType.String,
                Size = 255,
                Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout(logData.orgName))
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@message",
                DbType = System.Data.DbType.String,
                Size = 4000,
                Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout("%message"))
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@Log_Exception",
                DbType = System.Data.DbType.String,
                Size = 4000,
                Layout = new log4net.Layout.RawPropertyLayout { Key = "Log_Exception" }
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@methodname",
                DbType = System.Data.DbType.String,
                Size = 255,
                Layout = new log4net.Layout.Layout2RawLayoutAdapter(new log4net.Layout.PatternLayout(logData.method.Name))
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@Request_Time",
                DbType = System.Data.DbType.DateTime,
                Size = 255,
                Layout = new log4net.Layout.RawPropertyLayout { Key = "Request_Time" }
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@ApiRequest",
                DbType = System.Data.DbType.String,
                Size = 4000,
                Layout = new log4net.Layout.RawPropertyLayout { Key = "ApiRequest" }
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@Response_Time",
                DbType = System.Data.DbType.DateTime,
                Size = 255,
                Layout = new log4net.Layout.RawPropertyLayout { Key = "Response_Time" }
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@ApiResponse",
                DbType = System.Data.DbType.String,
                Size = 4000,
                Layout = new log4net.Layout.RawPropertyLayout { Key = "ApiResponse" }
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@Machine_Name",
                DbType = System.Data.DbType.String,
                Size = 100,
                Layout = new log4net.Layout.RawPropertyLayout { Key = "Machine_Name" }
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@User_Name",
                DbType = System.Data.DbType.String,
                Size = 100,
                Layout = new log4net.Layout.RawPropertyLayout { Key = "User_Name" }
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@CUB_Correlation_Id",
                DbType = System.Data.DbType.String,
                Size = 40,
                Layout = new log4net.Layout.RawPropertyLayout { Key = "CUB_Correlation_Id" }
            });

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@Run_Duration",
                DbType = System.Data.DbType.String,
                Size = 20,
                Layout = new log4net.Layout.RawPropertyLayout { Key = "Run_Duration" }
            });
            
            appender.ActivateOptions();

            return appender;
        }

        internal static IAppender FindAppender(string
        appenderName)
        {
            IAppender returnAppender = LogManager.GetRepository().GetAppenders().FirstOrDefault(a => a.Name.Equals(appenderName));

            return returnAppender;
        }

        public static void SetExecutionContext(string orgName, string logTable)
        {
            log4net.ThreadContext.Properties["OrgName"] = orgName;
            log4net.ThreadContext.Properties["LogTable"] = logTable;
            SetThreadContextGuid();
        }

        private static void SetThreadContextGuid()
        {
            if (log4net.ThreadContext.Properties["CUB_Correlation_Id"] == null)
                log4net.ThreadContext.Properties["CUB_Correlation_Id"] = Guid.NewGuid().ToString();
        }


        public static void LogMethodEntered(LogLevel logLevel)
        {
            DoLogEvent("Method entered", logLevel);

        }
        public static void LogMethodExit(LogLevel logLevel)
        {
            DoLogEvent("Method exit", logLevel);
        }

        public static void LogEvent(object message, LogLevel logLevel)
        {
            DoLogEvent(message, logLevel);
        }

        public static void LogEvent(object message, LogLevel logLevel, Exception exception)
        {
            DoLogEvent(message, logLevel, exception);
        }

        public static void LogEvent(object message, LogLevel logLevel, Exception exception,
            string request = null, DateTime? requestTime = null,
            string response = null, DateTime? responseTime = null)
        {
            DoLogEvent(message, logLevel, exception, request, requestTime, response, responseTime);
        }



        private static void DoLogEvent(object message, LogLevel logLevel, Exception exception = null,
            string request = null, DateTime? requestTime = null,
            string response = null, DateTime? responseTime = null)
        {
            try
            {

                MethodBase method = null;
                try
                {
                    var stacktrace = new StackTrace();
                    method = stacktrace.GetFrame(2).GetMethod();
                }
                catch (Exception)
                {
                    method = null;
                }

                try
                {
                    if (log4net.ThreadContext.Properties["OrgName"] == null)
                    {
                        log4net.ThreadContext.Properties["OrgName"] = Utility.OrgName == null ? "OrgUnknown": Utility.OrgName;
                    }
                    if (log4net.ThreadContext.Properties["LogTable"] == null)
                    {
                        log4net.ThreadContext.Properties["LogTable"] = Utility.LogTable == null ? "General" : Utility.LogTable;
                    }
                    if (log4net.ThreadContext.Properties["CUB_Correlation_Id"] == null)
                    {
                        SetThreadContextGuid();
                    }

                    DataToLog logData = new DataToLog()
                    {
                        method = method,
                        level = logLevel,
                        message = message,
                        orgName = log4net.ThreadContext.Properties["OrgName"].ToString(),
                        entityName = log4net.ThreadContext.Properties["LogTable"].ToString(),
                        exception = exception,
                        request = request,
                        requestTime = requestTime,
                        response = response,
                        responseTime = responseTime
                    };

                    #region Set log4net Custom properties
                    log4net.ThreadContext.Properties["Request_Time"] = logData.requestTime;
                    log4net.ThreadContext.Properties["ApiRequest"] = logData.request;
                    log4net.ThreadContext.Properties["Response_Time"] = logData.responseTime;
                    log4net.ThreadContext.Properties["ApiResponse"] = logData.response;
                    log4net.ThreadContext.Properties["Log_Exception"] = logData.exceptionString;
                    log4net.ThreadContext.Properties["Machine_Name"] = logData.machineName;
                    log4net.ThreadContext.Properties["User_Name"] = logData.userName;
                    log4net.ThreadContext.Properties["Run_Duration"] = logData.runDuration;
                    #endregion  Set log4net Custom properties

                    Logger.Log(logData);

                    logData = null;
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.ToString());
                }
            }
            catch (Exception)
            {
            }
        }

    }
}
