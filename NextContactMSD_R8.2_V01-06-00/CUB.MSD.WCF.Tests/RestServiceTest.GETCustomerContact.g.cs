/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CUB.MSD.WCF;
using CUB.MSD.WCF.Library;
using System;
using System.ServiceModel;
using CUB.MSD.Model;

namespace CUB.MSD.WCF.Tests
{
    public partial class RestServiceTest
    {
        [TestMethod]
        public void GETCustomerContactThrowsObjectRefException220()
        {
            JsonModels.WSCustomerContact wSCustomerContact;
            RestService s0 = new RestService();
            
            try
            {
                wSCustomerContact = this.GETCustomerContact(s0, (string)null, (string)null);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Object reference not set to an instance of an object.");
            }
        }
    }
}
