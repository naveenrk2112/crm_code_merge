/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CUB.MSD.WCF;
using System.Collections.Generic;
using System;

namespace CUB.MSD.WCF.Tests
{
    public partial class RestServiceTest
    {

[TestMethod]
public void GetRequestInfo851()
{
    RestService s0 = new RestService();
    Dictionary<string, string> dictionary = (Dictionary<string, string>)null;
    string s = (string)null;
    string s1 = (string)null;
    this.GetRequestInfo(s0, ref dictionary, ref s, ref s1);
    Assert.IsNotNull((object)s0);
    Assert.IsNull((object)dictionary);
    Assert.AreEqual<string>((string)null, s);
    Assert.AreEqual<string>((string)null, s1);
}

[TestMethod]
public void GetRequestInfo595()
{
    Dictionary<string, string> dictionary;
    dictionary = new Dictionary<string, string>(0);
    dictionary[""] = (string)null;
    RestService s0 = new RestService();
    Dictionary<string, string> dictionary1 = dictionary;
    string s = (string)null;
    string s1 = (string)null;
    this.GetRequestInfo(s0, ref dictionary1, ref s, ref s1);
    Assert.IsNotNull((object)s0);
    Assert.IsNotNull((object)dictionary1);
    Assert.IsNotNull(dictionary1.Comparer);
    Assert.AreEqual<int>(3, dictionary1.Count);
    Assert.AreEqual<string>((string)null, s);
    Assert.AreEqual<string>((string)null, s1);
}
    }
}
