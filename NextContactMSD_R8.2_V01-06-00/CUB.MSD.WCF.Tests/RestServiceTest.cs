/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.WCF.Library;
using System.Collections.Specialized;
using System.Reflection;
using System.Collections.Generic;
using System.Net.Http;
using System;
using CUB.MSD.WCF;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CUB.MSD.WCF.Tests
{
    [TestClass]
    public partial class RestServiceTest
    {

        public void AddSuccessHeader(RestService target, string requestGuid)
        {
            target.AddSuccessHeader(requestGuid);
            // TODO: add assertions to method RestServiceTest.AddSuccessHeader(RestService, String)
        }

        internal void GetRequestInfo(
            RestService target,
            ref Dictionary<string, string> dictForm,
            ref string requestGuid,
            ref string requestDevice
        )
        {
            object[] args = new object[3];
            args[0] = (object)dictForm;
            args[1] = (object)requestGuid;
            args[2] = (object)requestDevice;
            Type[] parameterTypes = new Type[3];
            parameterTypes[0] = typeof(Dictionary<string, string>).MakeByRefType();
            parameterTypes[1] = typeof(string).MakeByRefType();
            parameterTypes[2] = typeof(string).MakeByRefType();
            object result = null;
            try
            {
                result = ((MethodBase)(typeof(RestService).GetMethod("GetRequestInfo",
                                                                        BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic, (Binder)null,
                                                                        CallingConventions.HasThis, parameterTypes, (ParameterModifier[])null)))
                                .Invoke((object)target, args);

            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Exception has been thrown by the target of an invocation.");
            }

            System.Web.HttpRequest req = new System.Web.HttpRequest("fileName", "http://test.cts.cub", "//MSD//AccountMgr.svc//customer//CF23729C-E7AC-E611-80DE-00505681512F//contact//F9D3514E-8BC1-E611-80E0-005056814569");
            System.IO.TextWriter writer = null; //new System.IO.StreamWriter("C:\\testwriter.txt");
            System.Web.HttpResponse resp = new System.Web.HttpResponse(writer);
            //req.Headers.Add("Authorization", "Basic dXNlcjpwYXNzd29yZA==");
            //req.Headers.Add("x-cub-hdr", "uid=0A1EBC2F-5CBE-4B0D-8FA0-24902E7E773D,device=SoapUI");
            System.Web.HttpContext testWeb = new System.Web.HttpContext(req, resp);
            System.Web.HttpContext.Current = testWeb;

            try
            {
                result = ((MethodBase)(typeof(RestService).GetMethod("GetRequestInfo",
                                                                        BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic, (Binder)null,
                                                                        CallingConventions.HasThis, parameterTypes, (ParameterModifier[])null)))
                                .Invoke((object)target, args);

                int i = unchecked(0);
                dictForm = args[0] as Dictionary<string, string>;
                requestGuid = args[1] as string;
                requestDevice = args[2] as string;

            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Exception has been thrown by the target of an invocation.");
                //Assert.IsInstanceOfType(e, typeof(System.NullReferenceException));
            }

            System.Web.HttpContext.Current = null;
        }

        public string[] ParseCUBHeader(string cubHdr)
        {
            string[] result = RestService.ParseCUBHeader(cubHdr);
            return result;
            // TODO: add assertions to method RestServiceTest.ParseCUBHeader(String)
        }

        public JsonModels.WSAppInfo IsAvailable(RestService target)
        {
            JsonModels.WSAppInfo result = target.IsAvailable();
            return result;
            // TODO: add assertions to method RestServiceTest.IsAvailable(RestService)
        }

        public JsonModels.WSMSDOrganizationInfo GetOrganizationInfo(RestService target)
        {
            JsonModels.WSMSDOrganizationInfo result = target.GetOrganizationInfo();
            return result;
            // TODO: add assertions to method RestServiceTest.IsAvailable(RestService)
        }

        public JsonModels.WSCustomerContact GETCustomerContact(
            RestService target,
            string customerId,
            string contactId
        )
        {
            JsonModels.WSCustomerContact result = target.customercontact(customerId, contactId);
            return result;
            // TODO: add assertions to method RestServiceTest.GETCustomerContact(RestService, String, String)
        }
    }
}
