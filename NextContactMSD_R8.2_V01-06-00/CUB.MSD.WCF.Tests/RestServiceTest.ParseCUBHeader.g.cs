/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CUB.MSD.WCF.Tests
{
    public partial class RestServiceTest
    {

        [TestMethod]
        public void ParseCUBHeader33()
        {
            string[] ss;
            ss = this.ParseCUBHeader((string)null);
            Assert.IsNotNull((object)ss);
            Assert.AreEqual<int>(2, ss.Length);
            Assert.AreEqual<string>((string)null, ss[0]);
            Assert.AreEqual<string>((string)null, ss[1]);
        }

        [TestMethod]
        public void ParseCUBHeader3301()
        {
            string[] ss;
            ss = this.ParseCUBHeader("{\"uid\":\"0A1EBC2F-5CBE-4B0D-8FA0-24902E7E773D\",\"device\":\"SoapUI\"}");
            Assert.IsNotNull((object)ss);
            Assert.AreEqual<string>("0A1EBC2F-5CBE-4B0D-8FA0-24902E7E773D", ss[0]);
            Assert.AreEqual<string>("SoapUI", ss[1]);
        }

        [TestMethod]
        public void ParseCUBHeader3302()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{\"uid\":,\"device\":\"SoapUI\"}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeader3303()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{\"uid\":\"12345678901234567890123456789012345678901\",\"device\":\"SoapUI\"}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid too long");
            }
        }

        [TestMethod]
        public void ParseCUBHeader3304()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{\"uid\":\"0A1EBC2F-5CBE-4B0D-8FA0-24902E7E773D\",\"using\":\"SoapUI\"}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - device missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeader3305()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{\"uid\":\"0A1EBC2F-5CBE-4B0D-8FA0-24902E7E773D\",\"device\":}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - device missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeader3306()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{\"uid\":\"0A1EBC2F-5CBE-4B0D-8FA0-24902E7E773D\",:\"SoapUI\"}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - device missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeader3307()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{\"uid\":\"0A1EBC2F-5CBE-4B0D-8FA0-24902E7E773D\",\"device\":\"12345678901234567890123456789012345678901\"}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - device too long");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsInvalidHeaderException514()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("?");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsIndexOutOfRangeException263()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{\0}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Index was outside the bounds of the array.");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsException575()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{,}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsIndexOutOfRangeException196()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{\0\0}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Index was outside the bounds of the array.");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsInvalidHeaderException300()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("?{\0}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsInvalidHeaderException731()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("\0 ");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsException825()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{,\0}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsException872()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{,,}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsInvalidHeaderException302()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("?{\0\0\0}??");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsInvalidHeaderException690()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("? ");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsException82501()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{?\0,}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsException177()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("?{\"\0?\0,\"}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsException565()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{:,}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsException748()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{:\0,}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsException795()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{?\0\0:\0,}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid missing");
            }
        }

        [TestMethod]
        public void ParseCUBHeaderThrowsException115()
        {
            string[] ss;

            try
            {
                ss = this.ParseCUBHeader("{\" \0\0\0:\0\0,\"}");
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Invalid Header - uid missing");
            }
        }
    }
}
