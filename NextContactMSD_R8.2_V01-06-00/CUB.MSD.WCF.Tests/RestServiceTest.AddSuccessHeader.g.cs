/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CUB.MSD.WCF;
using System;

namespace CUB.MSD.WCF.Tests
{
    public partial class RestServiceTest
    {

        [TestMethod]
        [ExpectedException(typeof(InvalidProgramException))]
        public void AddSuccessHeaderThrowsInvalidProgramException241()
        {
            RestService s0 = new RestService();
            this.AddSuccessHeader(s0, (string)null);
        }
    }
}
