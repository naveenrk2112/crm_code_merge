/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CUB.MSD.WCF;
using CUB.MSD.WCF.Library;
using System;

namespace CUB.MSD.WCF.Tests
{
    public partial class RestServiceTest
    {
        [TestMethod]
        public void IsAvailableThrowsInvalidProgramException226()
        {
            JsonModels.WSAppInfo wSAppInfo;
            RestService s0 = new RestService();
            try
            {
                wSAppInfo = this.IsAvailable(s0);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Object reference not set to an instance of an object.");
            }
        }
    }
}
