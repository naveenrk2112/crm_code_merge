/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;

namespace WixCustomActions
{
    public class CustomActions
    {
        /// <summary>
        /// EncryptValues will encrypt the username, password, and domain for the web service configuration files
        ///   so that plain text values can be entered when filling in the WiX installer yet have encrypted values
        ///   stored within the configuration file itself
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult EncryptValues(Session session)
        {
            try
            {
                session.Log("Begin Encrypt Values Custom Action");

                //Encrypt the Username
                session.Log("Session value for Username = '{0}'", session["USERNAME"]);
                byte[] b_username_word = Encoding.ASCII.GetBytes(session["USERNAME"]);
                session["USERNAME"] = Convert.ToBase64String(b_username_word);
                session.Log("Session new value for Username = '{0}'", session["USERNAME"]);

                //Encrypt the Password
                session.Log("Session value for Password = '{0}'", session["PASSWORD"]);
                byte[] b_password_word = Encoding.ASCII.GetBytes(session["PASSWORD"]);
                session["PASSWORD"] = Convert.ToBase64String(b_password_word);
                session.Log("Session new value for Password = '{0}'", session["PASSWORD"]);

                //Encrypt the Domain
                session.Log("Session value for Domain = '{0}'", session["DOMAIN"]);
                byte[] b_doamin_word = Encoding.ASCII.GetBytes(session["DOMAIN"]);
                session["DOMAIN"] = Convert.ToBase64String(b_doamin_word);
                session.Log("Session new value for Domain = '{0}'", session["DOMAIN"]);

                session.Log("End Encrypt Values Custom Action");
            }
            catch (Exception ex)
            {
                session.Log("ERROR in custom action EncryptValues {0}", ex.ToString());
                return ActionResult.Failure;
            }

            return ActionResult.Success;
        }

        /// <summary>
        /// This function will create the import file with the given parameters
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult CreateConfigImportFile(Session session)
        {
            try
            {
                if (!string.IsNullOrEmpty(session["ORGNAME"]))
                {

                    session.Log("Begin Create Config Import File Custom Action");

                    string line1 = "ConfigDataCopy.exe sourceorgname=\"file\" targetorgnames=\"" + session["ORGNAME"] + "\"";
                    string line2 = " entitynames=\"" + session["ENTITIES"] + "\" action=\"" + session["MSD_ACTION"] +"\"";
                    string line3 = string.Concat(line1, line2);
                    string[] lines = { line3 };
                    string location = session["FOLDERLOCATION"] + session["ORGNAME"] + "_Import.bat";
                    session.Log("Session value for ORGNAME = '{0}'", session["ORGNAME"]);
                    session.Log("Session value for ENTITIES = '{0}'", session["ENTITIES"]);
                    session.Log("Session value for FOLDERLOCATION = '{0}'", session["FOLDERLOCATION"]);
                    session.Log("Session location = '{0}'", location);
                    session.Log("Session lin3 = '{0}'", line3);

                    System.IO.File.WriteAllLines(@location, lines);

                    session.Log("End Create Config Import File Custom Action");
                }
            }
            catch (Exception ex)
            {
                session.Log("ERROR in custom action CreateConfigImportFile {0}", ex.ToString());
                return ActionResult.Failure;
            }

            return ActionResult.Success;
        }


        /// <summary>
        /// This function will create the export solution file with the given parameters
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult CreateImportSolutionFile(Session session)
        {
            try
            {
                if (!string.IsNullOrEmpty(session["ORGNAME"]))
                {
                    session.Log("Begin Create Import Solution File Custom Action");

                    session.Log("Session value for ORGNAME = '{0}'", session["ORGNAME"]);
                    session.Log("Session value for SOLUTION_ZIP = '{0}'", session["SOLUTION_ZIP"]);
                    session.Log("Session value for FOLDER2LOCATION = '{0}'", session["FOLDER2LOCATION"]);
                    string newfilename = session["FOLDER2LOCATION"] + "Conn_" + session["ORGNAME"] + ".xml";
                    session.Log("newfilename value = '{0}'", newfilename);
                    string oldfilename = session["FOLDER2LOCATION"] + "Conn_NextContactOrg.xml";
                    session.Log("oldfilename value = '{0}'", oldfilename);

                    if (System.IO.File.Exists(newfilename))
                    {
                        session.Log("Removing existing copy of '{0}'", newfilename);
                        System.IO.File.Delete(newfilename);
                    }

                    if (System.IO.File.Exists(oldfilename))
                    {
                        session.Log("Rename file '{0}' to '{1}'", oldfilename, newfilename);
                        System.IO.File.Move(oldfilename, newfilename);
                    }

                    string line1 = "Crm.Utilities.SolutionsDeploymentTool.Console.exe targetconnectionxml=\"Conn_" + session["ORGNAME"] + ".xml\"";
                    string line2 = " solutionnames=\"Solutions\\" + session["SOLUTION_ZIP"] + "\" mainoperation=\"" + session["MSD_MAIN_OPERATION"] + "\" abortonerror=\"false\"";
                    string line3 = " targetorgnames=\"" + session["ORGNAME"] + "\" publishoption=\"" + session["PUBLISH_OPTION"] + "\" deletepluginassemblies=\"" + session["DELETE_PLUGIN_ASSEMBLIES"] + "\"";
                    string line4 = string.Concat(line1, line2);
                    line4 = string.Concat(line4, line3);
                    string[] lines = { line4 };
                    string location = session["FOLDER2LOCATION"] + session["ORGNAME"] + "_Import.bat";
                    session.Log("Session location = '{0}'", location);
                    session.Log("Session lin3 = '{0}'", line4);

                    System.IO.File.WriteAllLines(@location, lines);

                    session.Log("End Create Import Solution File Custom Action");
                }
            }
            catch (Exception ex)
            {
                session.Log("ERROR in custom action CreateImportSolutionFile {0}", ex.ToString());
                return ActionResult.Failure;
            }

            return ActionResult.Success;
        }
    }
}
