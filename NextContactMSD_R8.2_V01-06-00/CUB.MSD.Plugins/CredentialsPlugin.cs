/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System;

namespace CUB.MSD.Plugins
{
    public class CredentialsPlugin : Plugin
    {
        public CredentialsPlugin() : base(typeof(CredentialsPlugin))
        {
            // Need to register events for the Plugin class
            base.RegisteredEvents.Add(new Tuple<int, string, string, 
                Action<LocalPluginContext>>(MessageProcessingStage.PreOperation, ContextMessageName.Create, cub_Credential.EntityLogicalName, new Action<LocalPluginContext>(ExecutePreCredentialsPlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, 
                Action<LocalPluginContext>>(MessageProcessingStage.PreOperation, ContextMessageName.Update, cub_Credential.EntityLogicalName, new Action<LocalPluginContext>(ExecutePreCredentialsPlugin)));
        }

        public void ExecutePreCredentialsPlugin(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(CUBConstants.Exceptions.LOCAL_CONTEXT_NOT_PASSED);
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, this.GetType().Name, service);
            Entity credentials = PluginHelpers.GetTargetEntity(context);
            cub_Credential cubCredentials = credentials?.ToEntity<cub_Credential>();

            if (cubCredentials == null) throw new InvalidPluginExecutionException(CUBConstants.Exceptions.UNABLE_TO_CONVERT_ENTITY_AS_SPECIFIED_TYPE);

            try
            {
                switch (context.Stage)
                {
                    case MessageProcessingStage.PreValidation:
                        break;

                    case MessageProcessingStage.PreOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                    if (cubCredentials.cub_Password != null)
                                    {
                                        try
                                        {
                                            UtilityLogic.DecryptTripleDES(cubCredentials.cub_Password);
                                        }
                                        catch (SystemException)
                                        {
                                            cubCredentials.cub_Password = UtilityLogic.EncryptTripleDES(cubCredentials.cub_Password);
                                        }
                                    }
                                    break;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Error on CredentialsPlugin. {0}: {1}: {2}",
                           context.Stage.ToString("G"), context.MessageName, ex.Message);
                throw new InvalidPluginExecutionException(msg, ex);
            }
        }
    }
}
