/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using Microsoft.Xrm.Sdk;
using System;
using System.Linq;
using System.Net.Http;
using static CUB.MSD.Model.CUBConstants;

namespace CUB.MSD.Plugins.MasterSearch
{
    public class GlobalTravelHistoryAction : Plugin
    {
        public GlobalTravelHistoryAction()
            : base(typeof(GlobalTravelHistoryAction))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, TransitAccountConstants.TRAVEL_HISTORY_ACTION_NAME, null, new Action<LocalPluginContext>(ExecuteGlobalTravelHistoryAction)));
        }

        public void ExecuteGlobalTravelHistoryAction(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(TransitAccountConstants.PLUGIN_CONTEXT_NOT_PASSED);
            }
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            if (context.Depth > PluginHelpers.Depth)
                return;
            if (context.MessageName == TransitAccountConstants.TRAVEL_HISTORY_ACTION_NAME)
            {
                if (localContext.PluginExecutionContext.InputParameters.Contains(TransitAccountConstants.ACCOUNT_ID) &&
                    localContext.PluginExecutionContext.InputParameters.Contains(TransitAccountConstants.SUBSYSTEM) &&
                    localContext.PluginExecutionContext.InputParameters.Contains(TransitAccountConstants.START_DATE_INPUT_PARAM) &&
                    localContext.PluginExecutionContext.InputParameters.Contains(TransitAccountConstants.END_DATE_INPUT_PARAM))
                {
                    NISApiLogic nisApi = new NISApiLogic(service);
                    string accountID = (string)localContext.PluginExecutionContext.InputParameters[TransitAccountConstants.ACCOUNT_ID];
                    string subsystem = (string)localContext.PluginExecutionContext.InputParameters[TransitAccountConstants.SUBSYSTEM];
                    string startDate = (string)localContext.PluginExecutionContext.InputParameters[TransitAccountConstants.START_DATE_INPUT_PARAM];
                    string endDate = (string)localContext.PluginExecutionContext.InputParameters[TransitAccountConstants.END_DATE_INPUT_PARAM];
                    var data = nisApi.GetTransitAccountTravelHistory(accountID, subsystem, startDate, endDate, null, null, null, null, null, null, null);
                    localContext.PluginExecutionContext.OutputParameters[TransitAccountConstants.TRAVEL_HISTORY_OUTPUT_PARAM] = data;
                }
            }
        }
    }
}
