/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using Microsoft.Xrm.Sdk;
using System;
using System.Linq;
using System.Net.Http;
using static CUB.MSD.Model.CUBConstants;

namespace CUB.MSD.Plugins.MasterSearch
{
    public class TransitAccountStatusByIdPlugin : Plugin
    {
        public TransitAccountStatusByIdPlugin()
            : base(typeof(TransitAccountStatusByIdPlugin))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, TransitAccountConstants.STATUS_ACTION_NAME, null, new Action<LocalPluginContext>(ExecuteTransitAccountStatusByAccountId)));
        }

        public void ExecuteTransitAccountStatusByAccountId(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(TransitAccountConstants.PLUGIN_CONTEXT_NOT_PASSED);
            }
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            CallSerializeContext(context, TransitAccountConstants.ACCOUNT_STATUS_PLUGIN_NAME, service);
            if (context.Depth > PluginHelpers.Depth)
                return;
            if (context.MessageName == TransitAccountConstants.STATUS_ACTION_NAME)
            {
                if (localContext.PluginExecutionContext.InputParameters.Contains(TransitAccountConstants.ACCOUNT_ID) &&
                    localContext.PluginExecutionContext.InputParameters.Contains(TransitAccountConstants.SUBSYSTEM))
                {
                    string accountID = (string)localContext.PluginExecutionContext.InputParameters[TransitAccountConstants.ACCOUNT_ID];
                    string subsystem = (string)localContext.PluginExecutionContext.InputParameters[TransitAccountConstants.SUBSYSTEM];
                    NISApiLogic nisApi = new NISApiLogic(service);
                    var data = nisApi.GetTransitAccountStatusByID(accountID, subsystem);
                    localContext.PluginExecutionContext.OutputParameters[TransitAccountConstants.ACCOUNT_STATUS_OUTPUT_PARAM] = data.ToString();
                }
            }
        }
    }
}
