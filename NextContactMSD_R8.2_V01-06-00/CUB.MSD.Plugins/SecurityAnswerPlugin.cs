﻿/***********************************************************************************
* Copyright (c) 2018 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/

using CUB.MSD.Logic;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System;

namespace CUB.MSD.Plugins
{
    public class SecurityAnswerPlugin : Plugin
    {
        public SecurityAnswerPlugin() : base(typeof(SecurityAnswerPlugin))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(
                MessageProcessingStage.PreValidation,
                ContextMessageName.Create,
                cub_SecurityAnswer.EntityLogicalName,
                new Action<LocalPluginContext>(ExecuteOnPreCreate)));
        }

        public void ExecuteOnPreCreate(LocalPluginContext localContext)
        {
            var context = localContext.PluginExecutionContext;
            var orgService = localContext.OrganizationService;

            CallSerializeContext(context, GetType().Name, orgService);

            var securityAnswer = ((Entity)context.InputParameters["Target"])?.ToEntity<cub_SecurityAnswer>();
            if (securityAnswer?.cub_ContactId?.Id == null)
            {
                return;
            }

            var logic = new ContactLogic(orgService, context);
            var isMaxReached = logic.MaxSecurityQAsReached(securityAnswer.cub_ContactId.Id);
            if (isMaxReached)
            {
                throw new InvalidPluginExecutionException("Maximum number of security questions already created for this contact. Cannot add more answers.");
            }
        }
    }
}
