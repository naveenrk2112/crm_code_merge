/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CUB.MSD.Model.CUBConstants;

namespace CUB.MSD.Plugins
{
    public class GlobalGetGlobalsAttributeValueAction : Plugin
    {
        public GlobalGetGlobalsAttributeValueAction()
            : base(typeof(GlobalGetGlobalsAttributeValueAction))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, GlobalGetGlobalsAttributeValueConstants.ACTION_NAME, null, new Action<LocalPluginContext>(ExecuteGlobalGetGlobalsAttribueValue)));
        }

        public void ExecuteGlobalGetGlobalsAttribueValue(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(TransitAccountConstants.PLUGIN_CONTEXT_NOT_PASSED);
            }
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            if (context.MessageName == GlobalGetGlobalsAttributeValueConstants.ACTION_NAME)
            {
                if (localContext.PluginExecutionContext.InputParameters.Contains(GlobalGetGlobalsAttributeValueConstants.GLOBAL_PARAM) &&
                    localContext.PluginExecutionContext.InputParameters.Contains(GlobalGetGlobalsAttributeValueConstants.GLOBAL_DETAIL_PARAM))
                {
                    string global = (string)localContext.PluginExecutionContext.InputParameters[GlobalGetGlobalsAttributeValueConstants.GLOBAL_PARAM];
                    string detail = (string)localContext.PluginExecutionContext.InputParameters[GlobalGetGlobalsAttributeValueConstants.GLOBAL_DETAIL_PARAM];
                    string value = GlobalsCache.GetKeyFromCache(service, global, detail).ToString();
                    localContext.PluginExecutionContext.OutputParameters[GlobalGetGlobalsAttributeValueConstants.ATTRIBUTE_VALUE_PARAM] = value;
                }
            }
        }
    }
}
