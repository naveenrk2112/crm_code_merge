/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;

// unfinished plugin - trying to remove formatting from phone in postop
namespace CUB.MSD.Plugins
{

    public class PhoneUpdatePlugin: Plugin
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneUpdatePlugin"/> class.
        /// </summary>
        /// <param name="unsecure">Contains public (unsecured) configuration information.</param>
        /// <param name="secure">Contains non-public (secured) configuration information. 
        /// When using Microsoft Dynamics 365 for Outlook with Offline Access, 
        /// the secure string is not passed to a plug-in that executes while the client is offline.</param>
        private readonly string postImageAlias = "Target";
        public PhoneUpdatePlugin()
            : base(typeof(PhoneUpdatePlugin))
        {
            // Need to register events for the Plugin class
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PreValidation, ContextMessageName.Create, "cub_phone", new Action<LocalPluginContext>(ExecutePhoneUpdatePlugin)));
            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        public void ExecutePhoneUpdatePlugin(LocalPluginContext localContext)
        {

            // Obtain the execution context from the service provider.
            if (localContext == null)
            {
                throw new ArgumentNullException("Plugin Error: LocalContext not passed.");
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, "PhoneUpdatePlugin", service);

            Entity cub_phone = PluginHelpers.GetTargetEntity(context);
            //Entity cub_phone = (context.PostEntityImages != null && context.PostEntityImages.Contains(this.postImageAlias)) ? context.PostEntityImages[this.postImageAlias] : null;

            if (cub_phone.Contains("cub_phonedescription") && cub_phone.Attributes["cub_phonedescription"] != null)
            {
                string tmp = ((string)cub_phone.Attributes["cub_phonedescription"]);
                string tmp2 = tmp.Replace("/[^ 0 - 9, A - Z, a - z] / g", "");
                cub_phone.Attributes["cub_phonedescription"] = tmp2;
            }
        }
    }
}
