﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace CUB.MSD.Plugins
{
    public class ActivityPointerPlugin : Plugin
    {
        public ActivityPointerPlugin() : base(typeof(ActivityPointerPlugin))
        {
            #region activitypointer events
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PreOperation, ContextMessageName.RetrieveMultiple, ActivityPointer.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePluginAction)));

            #endregion

        }

        private void ExecutePluginAction(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(CUBConstants.Exceptions.LOCAL_CONTEXT_NOT_PASSED);
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, this.GetType().Name, service);

            try
            {
                switch (context.Stage)
                {
                    case MessageProcessingStage.PreValidation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                case ContextMessageName.Delete:
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PreOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                    break;
                                case ContextMessageName.Update:
                                    break;
                                case ContextMessageName.RetrieveMultiple:
                                    var qe = context.InputParameters["Query"] as QueryExpression;
                                    if (qe != null)
                                    {
                                        ConditionExpression contactActivitiesRollUpToCustomerTriggerCondition = null;
                                        ConditionExpression regardingCondition = null;
                                        Guid regardingId = Guid.Empty;

                                        foreach (ConditionExpression cond in qe.Criteria.Conditions)
                                        {
                                            if (cond.AttributeName == "subject" && cond.Operator == ConditionOperator.Equal && cond.Values.Count == 1 && cond.Values.Contains("ContactActivitiesRollUpToCustomer"))
                                            {
                                                CubLogger.Debug("Found triggering condition");
                                                contactActivitiesRollUpToCustomerTriggerCondition = cond;
                                            }
                                            else if (cond.AttributeName == "regardingobjectid" && cond.Operator == ConditionOperator.Equal && cond.Values.Count == 1 && cond.Values[0] is Guid)
                                            {
                                                CubLogger.Debug("Found condition for regardingobjectid");
                                                regardingCondition = cond;

                                                regardingId = (Guid)regardingCondition.Values[0];
                                                CubLogger.Debug($"Found regarding id: {regardingId}");
                                            }
                                            else
                                            {
                                                CubLogger.Debug($"Disregarding condition for {cond.AttributeName}");
                                            }
                                        }

                                        if (contactActivitiesRollUpToCustomerTriggerCondition != null && regardingCondition != null && !regardingId.Equals(Guid.Empty))
                                        {
                                            //Roll up only if it's enabled in the org
                                            if (GlobalsCache.RollupLinkedEntityActivitiesToAccount(service))
                                            {

                                                CubLogger.Debug("Removing triggering conditions");
                                                qe.Criteria.Conditions.Remove(contactActivitiesRollUpToCustomerTriggerCondition);
                                                qe.Criteria.Conditions.Remove(regardingCondition);
                                                //This will modify the qe (query expression)
                                                ActivityPointerLogic.CreateCustomerRollUpActivitiesQueryExpression(qe, regardingId);
                                            }
                                            else
                                            {
                                                // if roll-up is not enabled, remove the subject filter otherwise the view will not return any results
                                                qe.Criteria.Conditions.Remove(contactActivitiesRollUpToCustomerTriggerCondition);
                                            }
                                        }
                                        else
                                        {
                                            // remove the subject filter otherwise the view will not return any results
                                            qe.Criteria.Conditions.Remove(contactActivitiesRollUpToCustomerTriggerCondition);
                                        }

                                    }
                                    
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PostOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                    break;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                string msg = $"Error on ActivityPointerPlugin. {context.Stage.ToString("G")}: {context.MessageName}: {ex.Message}";
                throw new InvalidPluginExecutionException(msg, ex);
            }

        }

    }
}
