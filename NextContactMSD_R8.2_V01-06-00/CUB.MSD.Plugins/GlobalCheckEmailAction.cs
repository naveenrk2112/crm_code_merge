/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;

using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;

namespace CUB.MSD.Plugins
{
    public class GlobalCheckEmailAction : Plugin
    {
        /// <summary>
        /// An action that validates the contact email address
        /// </summary>
        /// <remarks>Register this plug-in on the action cub_GlobalCheckEmailAction
        /// and postoperation stage.
        public GlobalCheckEmailAction()
            : base(typeof(GlobalCheckEmailAction))
        {
            // Need to register events for the Plugin class
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, "cub_GlobalCheckEmailAction", "none", new Action<LocalPluginContext>(ExecuteGlobalCheckEmailAction)));
            // Note:  Bug in Dev toolkit 365 with deploying actions - need to register this step using the RegisterPlugins tool

        }

        public void ExecuteGlobalCheckEmailAction(LocalPluginContext localContext)
        {
            // Obtain the execution context from the service provider.
            if (localContext == null)
            {
                throw new ArgumentNullException("Plugin Error: LocalContext not passed.");
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, "GlobalCheckEmailAction", service);

            string ret = "";

            #region guards

            if (context.Depth > PluginHelpers.Depth)
                return;

            #endregion

            switch (context.MessageName)
            {
                case "cub_GlobalCheckEmailAction": // custom action
                    {
                        // Note the inputparameters on an action do not contain a Target, only the input parms
                        //EntityReference contactRef = (EntityReference)localContext.PluginExecutionContext.InputParameters["EmailAddress"];

                        string emailAddressParm = (string)localContext.PluginExecutionContext.InputParameters["EmailAddress"];
                        string contextID = "";
                        if (localContext.PluginExecutionContext.InputParameters.Contains("ID") && ((string)localContext.PluginExecutionContext.InputParameters["ID"]) != "")
                        {
                            contextID = ((string)localContext.PluginExecutionContext.InputParameters["ID"]);
                        }

                        if (emailAddressParm != "")
                        {
                            
                            EmailLogic emailLogic = new EmailLogic(service);
                            ret = emailLogic.ValidateEmailAddressNotInUseOnOtherContact(emailAddressParm, contextID);
                            localContext.PluginExecutionContext.OutputParameters["DuplicateID"] = ret;
                            // if output if entityref
                            //EntityReference retEntity = new EntityReference;
                            //localContext.PluginExecutionContext.OutputParameters["DuplicateID"] = retEntity.Entities[0].GetAttributeValue<EntityReference>(“assigneduserid”).Name.ToString();

                        }
                    }
                    break;
            }
        }
    }
}
