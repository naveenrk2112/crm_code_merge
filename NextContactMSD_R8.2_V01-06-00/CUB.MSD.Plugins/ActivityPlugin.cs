﻿/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using Microsoft.Xrm.Sdk;
using System;

namespace CUB.MSD.Plugins
{
    public class ActivityPlugin : Plugin
    {
        public ActivityPlugin() : base(typeof(ActivityPlugin))
        {
            #region cub_CustomActivity events
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PreOperation, ContextMessageName.Create, cub_CustomActivity.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePluginAction)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, ContextMessageName.Create, cub_CustomActivity.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePluginAction)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PreOperation, ContextMessageName.Update, cub_CustomActivity.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePluginAction)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, ContextMessageName.Update, cub_CustomActivity.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePluginAction)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PreValidation, ContextMessageName.Create, cub_CustomActivity.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePluginAction)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PreValidation, ContextMessageName.Update, cub_CustomActivity.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePluginAction)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PreValidation, ContextMessageName.Update, cub_CustomActivity.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePluginAction)));
            #endregion

        }

        private void ExecutePluginAction(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(CUBConstants.Exceptions.LOCAL_CONTEXT_NOT_PASSED);
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, this.GetType().Name, service);
            ActivityLogic activityLogic = new ActivityLogic(service, context);

            cub_CustomActivity cubCustomActivity = null;
            Entity customActivity = null;

            if (context.MessageName == ContextMessageName.Delete)
            {
                EntityReference entRef = PluginHelpers.GetTargetEntityReference(context);
                if (entRef != null)
                {
                    customActivity = service.Retrieve(cub_CustomActivity.EntityLogicalName, entRef.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet() { AllColumns = true });

                }
            }
            else
            {
                customActivity = PluginHelpers.GetTargetEntity(context);
            }

            cubCustomActivity = customActivity?.ToEntity<cub_CustomActivity>();

            if (cubCustomActivity == null) throw new InvalidPluginExecutionException(CUBConstants.Exceptions.UNABLE_TO_CONVERT_ENTITY_AS_SPECIFIED_TYPE);


            try
            {
                switch (context.Stage)
                {
                    case MessageProcessingStage.PreValidation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                case ContextMessageName.Delete:
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PreOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                    // Set Group Id if blank
                                    activityLogic.SetGroupId(cubCustomActivity);
                                    break;
                                case ContextMessageName.Update:
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PostOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                    break;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                string msg = $"Error on ActivityPlugin. {context.Stage.ToString("G")}: {context.MessageName}: {ex.Message}";
                throw new InvalidPluginExecutionException(msg, ex);
            }

        }

    }
}
