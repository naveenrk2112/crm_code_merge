﻿/***********************************************************************************
* Copyright (c) 2018 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace CUB.MSD.Plugins
{
    public class CacheDurationUpdatePlugin : Plugin
    {
        public CacheDurationUpdatePlugin() : base(typeof(CacheDurationUpdatePlugin))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(
                MessageProcessingStage.PostOperation,
                ContextMessageName.Update,
                cub_GlobalDetails.EntityLogicalName,
                new Action<LocalPluginContext>(ExecuteOnPostUpdate)));
        }

        /// <summary>
        /// After global detail WEB.APP.CACHE.DURATION.MINUTES (CUB.MSD.WEB) successfully updates,
        /// alerts the web service to update its cache duration setting. Rather than throw an
        /// exception (which would revert the update), this method should log errors and return early.
        /// </summary>
        /// <param name="localContext"></param>
        public void ExecuteOnPostUpdate(LocalPluginContext localContext)
        {
            var context = localContext.PluginExecutionContext;
            var orgService = localContext.OrganizationService;
            var tracer = localContext.TracingService;

            // Only perform this operation for this specific global detail
            var global = context.PostEntityImages["Target"]?.ToEntity<cub_GlobalDetails>();
            if (global == null
                || global.cub_GlobalId.Name != CUBConstants.Globals.CUB_MSD_WEB
                || global.cub_Name != CUBConstants.Globals.WEB_APP_CACHE_DURATION_MINUTES)
            {
                return;
            }

            CubLogger.Init(orgService);
            CubLogger.Debug("Enter plugin");
            CallSerializeContext(context, GetType().Name, orgService);

            var url = GetGlobalDetailValue(orgService, CUBConstants.Globals.CUB_MSD_WEB, CUBConstants.Globals.WEB_APP_URL);
            var path = GetGlobalDetailValue(orgService, CUBConstants.Globals.CUB_MSD_WEB, CUBConstants.Globals.WEB_APP_URL_UPDATECACHEDURATION);
            if (string.IsNullOrEmpty(url) || string.IsNullOrEmpty(path))
            {
                var message = $"Invalid URL route: {url}{path}";
                tracer.Trace(message);
                CubLogger.Warn(message);
                return;
            }

            var body = new
            {
                duration = global.cub_AttributeValue
            };
            var httpContent = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            var route = url + path;
            CubLogger.Debug($"Executing POST request to {route}");
            using (var client = new HttpClient())
            {
                var response = client.PostAsync(route, httpContent).Result;
                if (!response.IsSuccessStatusCode)
                {
                    var message = $"Web service returned status code {response.StatusCode} {response.ReasonPhrase}";
                    tracer.Trace(message);
                    CubLogger.Warn(message);
                }
            }
            CubLogger.Debug("Exit plugin");
        }

        /// <summary>
        /// Helper method to retrieve the web service controller route configs for updating the web service cache
        /// </summary>
        /// <param name="orgService"></param>
        /// <param name="global"></param>
        /// <param name="globalDetail"></param>
        /// <returns></returns>
        private string GetGlobalDetailValue(IOrganizationService orgService, string global, string globalDetail)
        {
            var fetchXml = $@"
                <fetch>
                    <entity name='cub_globaldetails'>
                        <attribute name='cub_attributevalue' />
						<filter type='and'>
							<condition attribute='cub_enabled' operator='eq' value='1' />
							<condition attribute='cub_name' operator='eq' value='{globalDetail}' />
						</filter>
                        <link-entity name='cub_globals' from='cub_globalsid' to='cub_globalid'>
							<filter type='and'>
								<condition attribute='cub_enabled' operator='eq' value='1' />
								<condition attribute='cub_name' operator='eq' value='{global}' />
							</filter>
						</link-entity>
					</entity>
                </fetch>";
            return orgService.RetrieveMultiple(new FetchExpression(fetchXml))
                .Entities.FirstOrDefault()?.ToEntity<cub_GlobalDetails>()?.cub_AttributeValue;
        }
    }
}
