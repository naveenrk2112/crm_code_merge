/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;

using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;
using System.Linq;

namespace CUB.MSD.Plugins
{
    // Note: inheriting from base class plugin (added) instead of IPlugin
    public class ContactPlugin : Plugin
    {
        /// <summary>
        /// A plug-in that deals with teh contact entity
        /// </summary>
        /// <remarks>Register this plug-in on the contact entity</remarks>

        public ContactPlugin()
            : base(typeof(ContactPlugin))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, ContextMessageName.Update, Contact.EntityLogicalName, new Action<LocalPluginContext>(ExecutePlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PreOperation, ContextMessageName.Create, Contact.EntityLogicalName, new Action<LocalPluginContext>(ExecutePlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PreOperation, ContextMessageName.Update, Contact.EntityLogicalName, new Action<LocalPluginContext>(ExecutePlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PreValidation, ContextMessageName.Create, Contact.EntityLogicalName, new Action<LocalPluginContext>(ExecutePlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PreValidation, ContextMessageName.Update, Contact.EntityLogicalName, new Action<LocalPluginContext>(ExecutePlugin)));
        }

        public void ExecutePlugin(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(CUBConstants.Exceptions.LOCAL_CONTEXT_NOT_PASSED);
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, this.GetType().Name, service);
            ContactLogic contactLogic = new ContactLogic(service, context);
            Entity contact = PluginHelpers.GetTargetEntity(context);

            if (contact == null)
                throw new InvalidPluginExecutionException(String.Format(CUBConstants.Exceptions.GET_TARGET_ENTITY_FAILED, CUBConstants.Contact.CONTACT_PLUGIN));
            
            #region guards
            if (context.Depth > PluginHelpers.Depth) return;
            #endregion

            try
            {
                switch (context.Stage)
                {
                    case MessageProcessingStage.PreValidation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                    if (context.MessageName == ContextMessageName.Create)
                                    {
                                        Contact createContact = contact.ToEntity<Contact>();
                                        string err = string.Empty;
                                        if (!contactLogic.MultipleContactsAllowedOnCustomer(createContact, out err))
                                        {
                                            throw new InvalidPluginExecutionException(err);
                                        }
                                    }
                                    contactLogic.UpdateGmailEmail(contact);
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PreOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                    contactLogic.UpdateTelephone(contact, Contact.telephone1Attribute);
                                    contactLogic.UpdateTelephone(contact, Contact.telephone2Attribute);
                                    contactLogic.UpdateTelephone(contact, Contact.telephone3Attribute);
                                    // SS - NCTC-3435 - Check if a phone is added and create activity
                                    if (context.MessageName == ContextMessageName.Update)
                                    {
                                        if (context.PreEntityImages.Count > 0)
                                        {
                                            var contactPreImage = context.PreEntityImages["PreUpdateImage"] as Entity;
                                            if (contactPreImage != null)
                                            {
                                                Contact preUpdateContact = contactPreImage.ToEntity<Contact>();
                                                Contact updateContact = contact.ToEntity<Contact>();
                                                contactLogic.DoPreUpdateTasks(preUpdateContact, updateContact);
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PostOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                    break;
                                case ContextMessageName.Update:
                                    contactLogic.ReportChange(contact.Id);
                                    break;
                            }
                        }
                        break; 
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Error on ContactPlugin. {0}: {1}: {2}",
                           context.Stage.ToString("G"), context.MessageName, ex.Message);
                throw new InvalidPluginExecutionException(msg, ex);
            }
        }
    }
}
