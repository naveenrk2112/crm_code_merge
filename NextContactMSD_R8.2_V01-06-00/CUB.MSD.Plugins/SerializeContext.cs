/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Plugins
{
    public class CUBSerializePluginContext
    {
        /// <summary>
        ///     An implementation of IPluginExecutionContext designed to be serializable using a DataContractSerializer
        /// </summary>
        [DataContract]
        public class CUBPluginContext : IPluginExecutionContext
        {
         

            #region IPluginExecutionContext Members
            [DataMember]
            public Guid BusinessUnitId { get; set; }

            [DataMember]
            public Guid CorrelationId { get; set; }

            [DataMember]
            public int Depth { get; set; }

            [DataMember]
            public Guid InitiatingUserId { get; set; }

            [DataMember]
            public ParameterCollection InputParameters { get; set; }

            [DataMember]
            public bool IsExecutingOffline { get; set; }

            [DataMember]
            public bool IsInTransaction { get; set; }

            [DataMember]
            public bool IsOfflinePlayback { get; set; }

            [DataMember]
            public int IsolationMode { get; set; }

            [DataMember]
            public string MessageName { get; set; }

            [DataMember]
            public int Mode { get; set; }

            [DataMember]
            public DateTime OperationCreatedOn { get; set; }

            [DataMember]
            public Guid OperationId { get; set; }

            [DataMember]
            public Guid OrganizationId { get; set; }

            [DataMember]
            public string OrganizationName { get; set; }

            [DataMember]
            public ParameterCollection OutputParameters { get; set; }

            [DataMember]
            public EntityReference OwningExtension { get; set; }

            [DataMember]
            public CUBPluginContext Parent { get; set; }

            [IgnoreDataMember]
            [XmlIgnore()]  
            public IPluginExecutionContext ParentContext { get; set; }

       

            [DataMember]
            public EntityImageCollection PostEntityImages { get; set; }

            [DataMember]
            public EntityImageCollection PreEntityImages { get; set; }

            [DataMember]
            public Guid PrimaryEntityId { get; set; }

            [DataMember]
            public string PrimaryEntityName { get; set; }

            public Guid? RequestId { get; set; }

            [DataMember]
            public string SecondaryEntityName { get; set; }

            [DataMember]
            public ParameterCollection SharedVariables { get; set; }

            [DataMember]
            public int Stage { get; set; }

            [DataMember]
            public Guid UserId { get; set; }

            #endregion

            public CUBPluginContext()
            {
            }


            private CUBPluginContext(IPluginExecutionContext context)
            {
                BusinessUnitId = context.BusinessUnitId;
                CorrelationId = context.CorrelationId;
                Depth = context.Depth;
                InitiatingUserId = context.InitiatingUserId;
                InputParameters = context.InputParameters;
                IsExecutingOffline = context.IsExecutingOffline;
                IsInTransaction = context.IsInTransaction;
                IsOfflinePlayback = context.IsOfflinePlayback;
                IsolationMode = context.IsolationMode;
                MessageName = context.MessageName;
                Mode = context.Mode;
                OperationCreatedOn = context.OperationCreatedOn;
                OperationId = context.OperationId;
                OrganizationId = context.OrganizationId;
                OrganizationName = context.OrganizationName;
                OutputParameters = context.OutputParameters;
                OwningExtension = context.OwningExtension;
                PostEntityImages = context.PostEntityImages;
                PreEntityImages = context.PreEntityImages;
                PrimaryEntityId = context.PrimaryEntityId;
                PrimaryEntityName = context.PrimaryEntityName;
                RequestId = context.RequestId;
                SecondaryEntityName = context.SecondaryEntityName;
                SharedVariables = context.SharedVariables;
                Stage = context.Stage;
                UserId = context.UserId;
            }

            public static CUBPluginContext Copy(IPluginExecutionContext context)
            {
                var context2 = new CUBPluginContext(context);
                if (context.ParentContext != null)
                {
                    context2.Parent = Copy(context.ParentContext);
                }
                return context2;
            }
        }
    }
}
