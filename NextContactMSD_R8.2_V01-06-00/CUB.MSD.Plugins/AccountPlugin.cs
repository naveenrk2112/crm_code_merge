/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System;

namespace CUB.MSD.Plugins
{
    public class AccountPlugin : Plugin
    {
        public AccountPlugin() : base(typeof(AccountPlugin))
        {
            // Need to register events for the Plugin class
            //base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PreOperation, ContextMessageName.Create, Account.EntityLogicalName, new Action<LocalPluginContext>(ExecuteAccountPlugin)));
            //base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PreOperation, ContextMessageName.Update, Account.EntityLogicalName, new Action<LocalPluginContext>(ExecuteAccountPlugin)));
            //base.RegisteredEvents.Add(new Tuple<int, string, string, 
            //    Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, ContextMessageName.Update, Account.EntityLogicalName, 
            //        new Action<LocalPluginContext>(ExecutePostAccountPlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, 
                Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, ContextMessageName.Create, Account.EntityLogicalName, 
                    new Action<LocalPluginContext>(ExecutePostAccountPlugin)));
            
        }

        public void ExecutePostAccountPlugin(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(CUBConstants.Exceptions.LOCAL_CONTEXT_NOT_PASSED);
            }
            
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, this.GetType().Name, service);
            AccountLogic accountLogic = new AccountLogic(service, context);
            Entity account = PluginHelpers.GetTargetEntity(context);
            if (account == null)
                throw new InvalidPluginExecutionException(String.Format(CUBConstants.Exceptions.GET_TARGET_ENTITY_FAILED, CUBConstants.Account.ACCOUNT_PLUGIN));

            #region guards

            if (context.Depth > PluginHelpers.Depth)
                return;

            #endregion

            try
            {
                switch (context.Stage)
                {
                    case MessageProcessingStage.PreValidation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PreOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PostOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                    //SS - when an accout is created from the UI, we don't have the related records
                                    // therefore we can't complete the registration as NIS will throw an 
                                    // error when it calls back and doesn't find the data it it needs
                                    //accountLogic.CompleteCustomerRegistration(account.Id);
                                    break;
                                case ContextMessageName.Update:
                                    break;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Error on AccountPlugin. {0}: {1}: {2}",
                           context.Stage.ToString("G"), context.MessageName, ex.Message);
                throw new InvalidPluginExecutionException(msg, ex);
            }
        }
    }
}
