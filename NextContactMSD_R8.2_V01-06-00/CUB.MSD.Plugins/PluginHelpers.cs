/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using Microsoft.Xrm.Sdk;

namespace CUB.MSD.Plugins
{

    public class ContextMessageName
    {
        public const string Create = "Create";
        public const string Delete = "Delete";
        public const string Merge = "Merge";
        public const string Update = "Update";
        public const string RetrieveExchangeRate = "RetrieveExchangeRate";
        public const string RetrieveMultiple = "RetrieveMultiple";
        public const string SetStateDynamicEntity = "SetStateDynamicEntity";
        //TODO finish the enumeration or find it in the sdk
    }

    public class MessageProcessingMode
    {
        public const int Synchronous = 0;
        public const int Asynchronous = 1;
    }

    public class MessageProcessingStage
    {
        public const int PreValidation = 10;
        public const int PreOperation = 20;
        public const int MainOperation = 30; // crm only
        public const int PostOperation = 40;
        public const int PostOperationCRM4 = 50; // deprecated
    }

    public static class PluginHelpers
    {

        #region TestForEntity
        //we need to guard against the wrong entity of the plugin

        //if the entity name doesn't match then return from the plugin
        

        #endregion


        public const int Depth = 4;

        //untested
        public static IPluginExecutionContext GetContext(IServiceProvider serviceProvider)
        {
             return (IPluginExecutionContext) serviceProvider.GetService(typeof (IPluginExecutionContext));
        }


        //overload this to pass in the context
        public static Entity GetTargetEntity(IPluginExecutionContext pluginContext)  //will this always be reliable
        {
             // Delete, PreOperation contains an EntityReference instead of an Entity
            if (pluginContext.MessageName == ContextMessageName.Delete && pluginContext.Stage == MessageProcessingStage.PreOperation)
            {
             //   throw new Exception("Delete PreOperation stage does not contain a target entity. Did you mean to call GetTargetEntityReference instead?");
            }

            if (pluginContext.MessageName == ContextMessageName.SetStateDynamicEntity || pluginContext.MessageName == ContextMessageName.Delete) 
                return null;

            if (pluginContext.InputParameters.Contains("Target") &&
                pluginContext.InputParameters["Target"] is Entity)
            {
                return (Entity)pluginContext.InputParameters["Target"];
            }
            else
            {
                throw new Exception("No Target Entity Image found");
            }
        }

        public static EntityReference GetTargetEntityReference(IPluginExecutionContext pluginContext)  //will this always be reliable
        {
            // todo: need to find out which pipelines contain EntityReference in the target. Delete, PreOperation is one; Delete, PreValidation is another.
            //  SetState has an EntityReference in EntityMoniker
            if (
                pluginContext.MessageName != ContextMessageName.SetStateDynamicEntity &&
                (pluginContext.MessageName != ContextMessageName.Delete ||
                (pluginContext.MessageName == ContextMessageName.Delete && (pluginContext.Stage != MessageProcessingStage.PreValidation && pluginContext.Stage != MessageProcessingStage.PreOperation && pluginContext.Stage != MessageProcessingStage.PostOperation))
                ))
                return null;

            if (pluginContext.InputParameters.Contains("Target") &&
                pluginContext.InputParameters["Target"] is EntityReference)
            {
                return (EntityReference)pluginContext.InputParameters["Target"];
            }
            else if (pluginContext.InputParameters.Contains("EntityMoniker") &&
                pluginContext.InputParameters["EntityMoniker"] is EntityReference)
            {
                return (EntityReference)pluginContext.InputParameters["EntityMoniker"];
            }
            else
            {
                throw new Exception("No Target or EntityMoniker EntityReference found");
            }
        }

        public static Entity GetTargetEntityOutput(IServiceProvider serviceProvider)  //will this always be reliable
        {

            var pluginContext =
                (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (pluginContext.OutputParameters.Contains("Target") &&
                pluginContext.OutputParameters["Target"] is Entity)
            {
                return (Entity)pluginContext.OutputParameters["Target"];
            }
            else
            {
                throw new Exception("No Target Entity Image found");
            }
        }
        
        public static Entity GetTargetEntityPostEntityImages(IPluginExecutionContext pluginContext)  //will this always be reliable
        {
            if (pluginContext.PostEntityImages.Contains("Target") &&
                pluginContext.PostEntityImages["Target"] != null &&
                pluginContext.PostEntityImages["Target"] is Entity)
            {
                return (Entity)pluginContext.PostEntityImages["Target"];
            }
            else
            {
                throw new Exception("No Target Entity Image found");
            }
        }

        public static IOrganizationService GetService(IServiceProvider serviceProvider)
        {
            var pluginContext =
                (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            var factory =
                (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            return factory.CreateOrganizationService(pluginContext.UserId);
        }

        //untested
        public static ITracingService GetTracingService(IServiceProvider serviceProvider)
        {
           return (ITracingService)serviceProvider.GetService(typeof(ITracingService));
        }

    }
}
