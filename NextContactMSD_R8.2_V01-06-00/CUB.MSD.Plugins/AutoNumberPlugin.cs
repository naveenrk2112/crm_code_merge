﻿/***********************************************************************************
* Copyright (c) 2018 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using CUB.MSD.Model;
using Microsoft.Xrm.Sdk;
using System;

namespace CUB.MSD.Plugins
{
    public class AutoNumberPlugin : Plugin
    {
        public const string AUTO_NUMBER_KEY = "AutoNumberId"; //Key for the SharedVariables collection

        //Error message Text
        public const string NO_AUTO_NUMBER_ERROR = "No Auto Number record could be found for this entity.";
        public const string AUTO_NUMBER_EXCEEDED_ERROR = "Auto Number has reached its maximum value.";
        public const string INVALID_FIELD_NAME_ERROR = "The entity field specified for the related Auto Number is invalid.";

        /// <summary>
        /// If using AutoNumber on an entity, you must add registeredevents for both prevalidation and preoperation
        /// </summary>
        public AutoNumberPlugin() : base(typeof(AutoNumberPlugin))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(
                MessageProcessingStage.PreValidation,
                ContextMessageName.Create,
                cub_CSRSession.EntityLogicalName,
                new Action<LocalPluginContext>(ExecuteOnCreate)));

            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(
                MessageProcessingStage.PreOperation,
                ContextMessageName.Create,
                cub_CSRSession.EntityLogicalName,
                new Action<LocalPluginContext>(ExecuteOnCreate)));
        }

        public void ExecuteOnCreate(LocalPluginContext localContext)
        {
            var context = localContext.PluginExecutionContext;
            var orgService = localContext.OrganizationService;

            CallSerializeContext(context, GetType().Name, orgService);

            var entity = (Entity)context.InputParameters["Target"];

            if (context.MessageName == ContextMessageName.Create)
            {
                var logic = new AutoNumberLogic(orgService, context);

                // What plugin step we are on
                if (context.Stage == MessageProcessingStage.PreValidation)
                {
                    // Retrieving auto number id outside of transaction to prevent deadlock
                    // Retrieving places a 'shared lock' on the autonumber record, updates are prevented 
                    // if someone else has a shared lock on the record as well
                    try
                    {
                        var autoNumberId = logic.GetAutoNumberIdByEntity(context.PrimaryEntityName);
                        context.SharedVariables[AUTO_NUMBER_KEY] = autoNumberId;
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidPluginExecutionException(ex.Message, ex);
                    }
                }
                else if (context.Stage == MessageProcessingStage.PreOperation)
                {
                    // Assign and increment auto number based on id
                    if (context.ParentContext.SharedVariables.Contains(AUTO_NUMBER_KEY))
                    {
                        var id = (Guid)context.ParentContext.SharedVariables[AUTO_NUMBER_KEY];
                        var newAutoNumber = logic.GetNextAutoNumberById(id);

                        if (string.IsNullOrEmpty(newAutoNumber))
                        {
                            throw new InvalidPluginExecutionException(NO_AUTO_NUMBER_ERROR);
                        }

                        var autoNumEntity = logic.GetAutoNumberById(id);
                        var entityAutoFieldName = autoNumEntity.cub_EntityField;
                        var fieldValue = string.Empty;

                        if (!string.IsNullOrEmpty(autoNumEntity.cub_Prefix))
                        {
                            fieldValue += autoNumEntity.cub_Prefix;
                        }

                        fieldValue += newAutoNumber;

                        if (!string.IsNullOrEmpty(autoNumEntity.cub_Suffix))
                        {
                            fieldValue += autoNumEntity.cub_Suffix;
                        }

                        try
                        {
                            entity.Attributes[entityAutoFieldName] = fieldValue;
                        }
                        catch
                        {
                            throw new InvalidPluginExecutionException(INVALID_FIELD_NAME_ERROR);
                        }
                    }
                }
            }
        }
    }
}
