/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;

//using Newtonsoft.Json;

namespace CUB.MSD.Plugins
{
    public partial class Plugin
    {
        //added this partial class in order to include the serialization logic

        public void CallSerializeContext(IPluginExecutionContext context, string className, IOrganizationService service)
        {
            try
            {
                if (GetSerializeContext(context.OrganizationName, context.PrimaryEntityName, service))
                {
                    SerializeContextXMLFix(context, className);
                }
            }
            catch (Exception ex)
            {
                // NEED TO ADD LOG4NET or something
                //LogEvent(context.OrganizationName, "CallSerializeContext has failed: " + ex.Message, LogLevel.Error, ex);
            }
        }


        public static void SerializeContextXMLFix(IPluginExecutionContext context, string className)
        {
            var formatter = new DataContractSerializer(typeof (CUBSerializePluginContext.CUBPluginContext));

            CUBSerializePluginContext.CUBPluginContext myContext =
                CUBSerializePluginContext.CUBPluginContext.Copy(context);

            string directoryString;
            if (context.OrganizationName != null)
            {
                directoryString = @"C:\windows\temp\context\" + context.OrganizationName.Replace(" ", string.Empty) +
                                  @"\"; //might need to escape chars in the orgname that would interfere with the file
            }
            else
            {
                directoryString = @"C:\windows\temp\context\";
            }

            if (!Directory.Exists(directoryString))
            {
                Directory.CreateDirectory(directoryString);
            }

            string filename = string.Format("{0}{1}-{2}-{3}-----{4}", directoryString, context.PrimaryEntityName,
                context.MessageName, className, Guid.NewGuid());

            string suffix = string.Empty;
            int i = 0;
            while (File.Exists(filename + suffix))
            {
                suffix = i.ToString(CultureInfo.InvariantCulture);
                i++;
            }
            suffix += ".xml";
            using (FileStream file = File.Create(filename + suffix))
            {
                formatter.WriteObject(file, myContext);
                file.Flush();
                file.Close();
            }
        }

        public static bool GetSerializeContext(string orgName, string entityName, IOrganizationService service)
        {
            if (orgName == null)
            {
                //throw new ArgumentNullException("orgName");
                //log the issue and return
                return false;
            }
            if (entityName == null)
            {
                //throw new ArgumentNullException("entityName");
                //log the issue and return
                return false;
            }
            if (service == null)
            {
                //throw new ArgumentNullException("service");
                //log the issue and return
                return false;
            }


            try
            {
                using (var osc = new CUBOrganizationServiceContext(service))
                {
                    cub_Globals global = (from g in osc.cub_GlobalsSet
                                          where g.cub_Name.Equals("LogPluginContext")
                                          select new cub_Globals { cub_AttributeValue = g.cub_AttributeValue }).FirstOrDefault();

                    bool serializeContext = false;
                    if (global != null)
                    {
                        if (global.cub_AttributeValue != null)
                        {
                            //parse the value into an array and search for the entity name, if it exists then set the flag to true

                            string[] entityNames = global.cub_AttributeValue.Split(',');

                            serializeContext =
                                entityNames.Any(s => s.ToLower() == "all" || s.ToLower() == entityName.ToLower());
                        }
                    }
                    return serializeContext;
                }
            }
            catch (Exception ex)
            {
                //LogEvent(orgName, "GetSerializeContext has failed around global fetch: " + ex.Message, LogLevel.Error,
                //    ex);
                return false;
            }
        }

        //public void LogEvent(string message, EventLogEntryType type, int eventId)
        //{
        //    var logLevel = 2; // info

        //    switch (type)
        //    {
        //        case EventLogEntryType.Warning:
        //            logLevel = 3;
        //            break;
        //        case EventLogEntryType.Error:
        //            logLevel = 4;
        //            break;
        //    }

        //    LogEvent("", message, logLevel);
        //}

        // need to fix
        // New logging using log4net
        //public void LogEvent(string orgName, string message, LogLevel logLevel, Exception exception = null)
        //{
        //    try
        //    {
        //        // if the org isn't set up to log at this level, get out
        //        if (!Logger.DoLog(orgName, logLevel))
        //            return;

        //        MethodBase method = null;
        //        try
        //        {
        //            var stacktrace = new StackTrace();
        //            method = stacktrace.GetFrame(1).GetMethod();
        //        }
        //        catch (Exception)
        //        {
        //            method = null;
        //        }

        //        try
        //        {
        //            Logger.Log(logLevel, message, orgName, exception, method, "plugin");
        //        }
        //        catch (Exception e)
        //        {
        //            Trace.WriteLine(e.ToString());
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}


        public static void CallSerializeContextStatically(IPluginExecutionContext context, string className, IOrganizationService service)
        {
            //throwing an ArgumentNullException will likely cancel the plugin transaction so I'm going to log instead.
            if (context == null)
            {
                //throw new ArgumentNullException("context");
                //log issue
                return;
            }
            if (className == null)
            {
                //throw new ArgumentNullException("className");
                //log issue
                return;
            }
            if (service == null)
            {
                //throw new ArgumentNullException("service");
                //log issue
                return;
            }


            try
            {
                if (GetSerializeContext(context.OrganizationName, context.PrimaryEntityName, service))
                {
                    SerializeContextXMLFix(context, className);
                }
            }
            catch (Exception ex)
            {
                //LogEvent(context.OrganizationName, "CallSerializeContextStatically has failed: " + ex.Message, LogLevel.Error, ex);
                //log statically
            }
        }
    }
}
