/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using CUB.MSD.Logic;
using CUB.MSD.Model;
using CUB.MSD.Model.MSDApi;
using CUB.MSD.Model.NIS;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CUB.MSD.Plugins
{
    public class AddressPlugin : Plugin
    {
        public AddressPlugin() : base(typeof(AddressPlugin))
        {
            // Need to register events for the Plugin class

            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, ContextMessageName.Update, cub_Address.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePostAddressPlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PreValidation, ContextMessageName.Create, cub_Address.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePostAddressPlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PreValidation, ContextMessageName.Delete, cub_Address.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePostAddressPlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PreValidation, ContextMessageName.Update, cub_Address.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePostAddressPlugin)));
            base.RegisteredEvents.Add(new Tuple<int, string, string,
                Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, ContextMessageName.Create, cub_Address.EntityLogicalName,
                    new Action<LocalPluginContext>(ExecutePostAddressPlugin)));
        }

        public void ExecutePostAddressPlugin(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(CUBConstants.Exceptions.LOCAL_CONTEXT_NOT_PASSED);
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, this.GetType().Name, service);
            AddressLogic addressLogic = new AddressLogic(service, context);

            cub_Address cubAddress = null;
            Entity address = null;

            if (context.MessageName == ContextMessageName.Delete)
            {
                EntityReference entRef = PluginHelpers.GetTargetEntityReference(context);
                if (entRef != null)
                {
                    address = service.Retrieve(cub_Address.EntityLogicalName, entRef.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet() { AllColumns = true });

                }
            }
            else
            {
                address = PluginHelpers.GetTargetEntity(context);
            }

            cubAddress = address?.ToEntity<cub_Address>();

            if (cubAddress == null) throw new InvalidPluginExecutionException(CUBConstants.Exceptions.UNABLE_TO_CONVERT_ENTITY_AS_SPECIFIED_TYPE);

            try
            {
                switch (context.Stage)
                {
                    case MessageProcessingStage.PreValidation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                    CubResponse<Model.MSDApi.WSAddress> respPreValAddValidate = addressLogic.AddressPreCreateValidate(cubAddress);
                                    if (!respPreValAddValidate.Success)
                                    {
                                        throw new InvalidPluginExecutionException(string.Join(Environment.NewLine, respPreValAddValidate.Errors.Select(e => e.errorMessage)));
                                    }
                                    break;
                                case ContextMessageName.Update:
                                    #region Combine updated fields with non updated fields in one object
                                    //CRM sends only the update fields (in cubAddress) and at the pre stage we don't have post values, so we need to get the pre updated fields and merge
                                    // them with the updated fields, keeping the non updated fields from pre-image and updated fields from cubAddress
                                    var addressPreImage = context.PreEntityImages["PreUpdateImage"] as Entity;
                                    if (addressPreImage == null) throw new InvalidPluginExecutionException(CUBConstants.Address.Error.ERR_PLUGIN_STEP_IMAGE_NOT_FOUND);
                                    cub_Address preUpdateAdress = addressPreImage.ToEntity<cub_Address>();
                                    // Creating new object so we don't end up sending the extra fields to pre op and core operation if when they are not modified
                                    cub_Address combinedAddress = new cub_Address();
                                    combinedAddress.cub_Street1 = cubAddress.cub_Street1 ?? preUpdateAdress.cub_Street1;
                                    combinedAddress.cub_Street2 = cubAddress.cub_Street2 ?? preUpdateAdress.cub_Street2;
                                    combinedAddress.cub_Street3 = cubAddress.cub_Street3 ?? preUpdateAdress.cub_Street3;
                                    combinedAddress.cub_City = cubAddress.cub_City ?? preUpdateAdress.cub_City;
                                    combinedAddress.cub_StateProvinceId = cubAddress.cub_StateProvinceId ?? preUpdateAdress.cub_StateProvinceId;
                                    combinedAddress.cub_CountryId = cubAddress.cub_CountryId ?? preUpdateAdress.cub_CountryId;
                                    combinedAddress.cub_PostalCodeId = cubAddress.cub_PostalCodeId ?? preUpdateAdress.cub_PostalCodeId;
                                    combinedAddress.cub_AccountAddressId = cubAddress.cub_AccountAddressId ?? preUpdateAdress.cub_AccountAddressId;
                                    combinedAddress.statecode = cubAddress.statecode ?? preUpdateAdress.statecode;
                                    combinedAddress.Id = cubAddress.Id;
                                    #endregion
                                    CubResponse<Model.MSDApi.WSAddressDependenciesResponse> respPreValUpdateValidate = addressLogic.AddressPreUpdateValidate(combinedAddress);
                                    if (!respPreValUpdateValidate.Success)
                                    {
                                        throw new InvalidPluginExecutionException(string.Join(Environment.NewLine, respPreValUpdateValidate.Errors.Select(e => e.errorMessage)));
                                    }
                                    break;
                                case ContextMessageName.Delete:
                                    CubResponse<WSAddressDependenciesResponse> cubResponse = new CubResponse<WSAddressDependenciesResponse>();
                                    cubResponse = addressLogic.AddressPreDeleteValidation(cubAddress);
                                    if (!cubResponse.Success)
                                    {
                                        throw new InvalidPluginExecutionException(string.Join(Environment.NewLine, cubResponse.Errors.Select(e => e.errorMessage)));
                                    }
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PreOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                case ContextMessageName.Update:
                                    break;
                            }
                        }
                        break;
                    case MessageProcessingStage.PostOperation:
                        {
                            switch (context.MessageName)
                            {
                                case ContextMessageName.Create:
                                    addressLogic.DoAddressPostCreateTasks(cubAddress);
                                    break;
                                case ContextMessageName.Update:
                                    addressLogic.UpdateDependencies(cubAddress.Id);
                                    break;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Error on AddressPlugin. {0}: {1}: {2}",
                           context.Stage.ToString("G"), context.MessageName, ex.Message);
                throw new InvalidPluginExecutionException(msg, ex);
            }
        }
    }
}
