/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;

using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;
using System.Collections.Generic;
using static CUB.MSD.Model.CUBConstants;
using Newtonsoft.Json;
using CUB.MSD.Model.MSDApi;

namespace CUB.MSD.Plugins
{
    /// <summary>
    /// Plugin to save all Customer Registration Data as a ACID transaction.
    /// This plugin is associated with to the cub_GlobalCustomerRegistration MSD action
    /// The input parameter is a JSON object with the CustomerRegistration Data
    /// The output parameter is the Customer ID (MSD Account ID)
    /// </summary>
    public class GlobalCustomerRegistration : Plugin
    {
        public GlobalCustomerRegistration()
            : base(typeof(GlobalCustomerRegistration))
        {
            // Need to register events for the Plugin class
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, GlobalCustomerRegistrationConstants.ACTION_NAME, null, new Action<LocalPluginContext>(ExecuteGlobalCustomerRegistration)));

        }

        /// <summary>
        /// Execute the plugin logic
        /// </summary>
        /// <param name="localContext">Plugin context</param>
        public void ExecuteGlobalCustomerRegistration(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException(Exceptions.LOCAL_CONTEXT_NOT_PASSED);
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, GlobalCustomerRegistrationConstants.PLUGIN_NAME, service);

            #region guards

            if (context.Depth > PluginHelpers.Depth)
                return;

            #endregion

            if (context.MessageName.Equals(GlobalCustomerRegistrationConstants.ACTION_NAME))
            {
                string input = null;
                if (localContext.PluginExecutionContext.InputParameters.Contains(GlobalCustomerRegistrationConstants.INPUT_PARAMETER_NAME))
                {
                    input = (string)localContext.PluginExecutionContext.InputParameters[GlobalCustomerRegistrationConstants.INPUT_PARAMETER_NAME];
                    var contactLogic = new AccountLogic(service);
                    WSCustomer customer = JsonConvert.DeserializeObject<WSCustomer>(input);
                    contactLogic.RegisterCusomterSaveData(customer);
                    localContext.PluginExecutionContext.OutputParameters[GlobalCustomerRegistrationConstants.OUTPUT_PARAMETER_CUSTOMER_ID_NAME] 
                        = customer.customerId.Value;
                    localContext.PluginExecutionContext.OutputParameters[GlobalCustomerRegistrationConstants.OUTPUT_PARAMETER_CONTACT_ID_NAME]
                        = customer.contact.contactId.Value;
                }
            }
        }
    }
}
