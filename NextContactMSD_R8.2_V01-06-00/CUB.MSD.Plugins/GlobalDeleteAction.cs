/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;

using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;

namespace CUB.MSD.Plugins
{
    public class GlobalDeleteAction : Plugin
    {
        /// <summary>
        /// An action that validates the contact email address
        /// </summary>
        /// <remarks>Register this plug-in on the action cub_GlobalCheckEmailAction
        /// and postoperation stage.
        public GlobalDeleteAction()
            : base(typeof(GlobalDeleteAction))
        {
            // Need to register events for the Plugin class
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, 
                "cub_GlobalDeleteAction", "none", new Action<LocalPluginContext>(ExecuteGlobalDeleteRecordAction)));
            // Note:  Bug in Dev toolkit 365 with deploying actions - need to register this step using the RegisterPlugins tool

        }

        public void ExecuteGlobalDeleteRecordAction(LocalPluginContext localContext)
        {
            // Obtain the execution context from the service provider.
            if (localContext == null)
            {
                throw new ArgumentNullException("Plugin Error: LocalContext not passed.");
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, "GlobalDeleteAction", service);
            string ret = "";

            #region guards

            if (context.Depth > PluginHelpers.Depth)
                return;

            #endregion

            switch (context.MessageName)
            {
                case "cub_GlobalDeleteAction": // custom action
                    {
                        string recordId = (string)localContext.PluginExecutionContext.InputParameters["RecordId"];
                        string entityName = (string)localContext.PluginExecutionContext.InputParameters["EntityName"];

                        switch (entityName)
                        {
                            case cub_Address.EntityLogicalName:
                                CubResponse<WSAddressDeleteResponse> cubResponse = new CubResponse<WSAddressDeleteResponse>();
                                try
                                {
                                    cub_Address cubAddress = null;
                                    Entity address = null;

                                    address = service.Retrieve(cub_Address.EntityLogicalName, Guid.Parse(recordId), new Microsoft.Xrm.Sdk.Query.ColumnSet() { AllColumns = true });

                                    cubAddress = address?.ToEntity<cub_Address>();

                                    if (cubAddress == null)
                                        cubResponse.Errors.Add(new WSMSDFault(cub_Address.EntityLogicalName, cub_Address.cub_addressidAttribute,
                                            CUBConstants.Exceptions.UNABLE_TO_CONVERT_ENTITY_AS_SPECIFIED_TYPE));

                                    if (cubResponse.Success)
                                    {
                                        using (var nisApi = new NISApiLogic(service))
                                        {
                                            cubResponse.ResponseObject = nisApi.DeleteAddress(cubAddress.cub_AccountAddressId.Id, cubAddress.Id);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    cubResponse.Errors.Add(new WSMSDFault(cub_Address.EntityLogicalName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                                }

                                localContext.PluginExecutionContext.OutputParameters["CubResponse"] = cubResponse.SerializeObject();
                                
                                break;
                            default:
                                break;
                        }
                    }
                    break;
            }
        }
    }
}
