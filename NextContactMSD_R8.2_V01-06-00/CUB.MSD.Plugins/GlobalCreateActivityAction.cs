/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;

using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;
using CUB.MSD.Model.MSDApi;

namespace CUB.MSD.Plugins
{
    public class GlobalCreateActivityAction : Plugin
    {
        /// <summary>
        /// An action that validates the contact email address
        /// </summary>
        /// <remarks>Register this plug-in on the action cub_GlobalCheckEmailAction
        /// and postoperation stage.
        public GlobalCreateActivityAction()
            : base(typeof(GlobalCreateActivityAction))
        {
            // Need to register events for the Plugin class
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PostOperation,
                "cub_GlobalCreateActivityAction", "none", new Action<LocalPluginContext>(ExecuteCustomAction)));
        }

        private void ExecuteCustomAction(LocalPluginContext localContext)
        {
            // Obtain the execution context from the service provider.
            if (localContext == null)
            {
                throw new ArgumentNullException("Plugin Error: LocalContext not passed.");
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracer = localContext.TracingService;

            CallSerializeContext(context, this.GetType().Name, service);
            ActivityLogic activityLogic = new ActivityLogic(service, context);

            #region guards

            if (context.Depth > PluginHelpers.Depth)
                return;

            #endregion

            switch (context.MessageName)
            {
                case "cub_GlobalCreateActivityAction": // custom action
                    {
                        CubResponse<WSCreateNewActivityResponse> cubResponse = new CubResponse<WSCreateNewActivityResponse>();
                        try
                        {
                            CreateMSDActivityData activityData = new CreateMSDActivityData();
                            EntityReference er = null;
                            er = context.InputParameters["ActivityTypeId"] as EntityReference;
                            activityData.ActivityTypeId = er != null ? er.Id : Guid.Empty;
                            er = context.InputParameters["ActivitySubjectId"] as EntityReference;
                            activityData.ActivitySubjectId = er != null ? er.Id : Guid.Empty;
                            er = context.InputParameters["SessionId"] as EntityReference;
                            activityData.SessionId = er != null ? er.Id : Guid.Empty;
                            er = context.InputParameters["SubsystemId"] as EntityReference;
                            activityData.SubsystemId = er != null ? er.Id : Guid.Empty;
                            er = context.InputParameters["CustomerId"] as EntityReference;
                            activityData.CustomerId = er != null ? er.Id : Guid.Empty;
                            er = context.InputParameters["ContactId"] as EntityReference;
                            activityData.ContactId = er != null ? er.Id : Guid.Empty;
                            er = context.InputParameters["CaseId"] as EntityReference;
                            activityData.CaseId = er != null ? er.Id : Guid.Empty;
                            er = context.InputParameters["TaskId"] as EntityReference;
                            activityData.TaskId = er != null ? er.Id : Guid.Empty;
                            er = context.InputParameters["CreatedById"] as EntityReference;
                            activityData.CreatedById = er != null ? er.Id : Guid.Empty;
                            activityData.Category = context.InputParameters["Category"] as string;
                            activityData.Channel = context.InputParameters["Channel"] as string;
                            activityData.Description = context.InputParameters["Description"] as string;

                            cubResponse = activityLogic.CreateNewActivity(activityData);

                        }
                        catch (Exception ex)
                        {
                            cubResponse.Errors.Add(new WSMSDFault(context.MessageName, CUBConstants.Error.ERR_GENERAL_EXCEPTION, ex.Message));
                        }

                        if (cubResponse.Errors.Count > 0)
                        {
                            string err = string.Empty;
                            foreach (var item in cubResponse.Errors)
                            {
                                err += $"{Environment.NewLine}Key: {item.errorKey} Field: {item.fieldName} Message: {item.errorMessage}";
                            }
                            tracer.Trace(err);
                            CubLogger.Error(err);
                            throw new InvalidPluginExecutionException(err);
                        }

                        localContext.PluginExecutionContext.OutputParameters["CubResponse"] = cubResponse.SerializeObject();

                    }
                    break;
            }
        }
    }
}
