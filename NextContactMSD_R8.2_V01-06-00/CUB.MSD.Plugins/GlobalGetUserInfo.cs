/***********************************************************************************
* Copyright (c) 2017 CUBIC Transportation Systems. All rights reserved.
* CUBIC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
***********************************************************************************/
using System;

using Microsoft.Xrm.Sdk;
using CUB.MSD.Model;
using CUB.MSD.Logic;
using static CUB.MSD.Model.CUBConstants;

namespace CUB.MSD.Plugins
{
    public class GlobalGetUserInfo : Plugin
    {
        /// <summary>
        /// An action that returns user info
        /// </summary>
        /// <remarks>Register this plug-in on the action cub_GlobalGetUserInfoAction
        /// and postoperation stage.
        public GlobalGetUserInfo()
            : base(typeof(GlobalGetUserInfo))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(MessageProcessingStage.PostOperation, GlobalGetUserInfoConstants.ACTION_NAME, null, new Action<LocalPluginContext>(ExecuteGlobalGetUserInfo)));
        }

        public void ExecuteGlobalGetUserInfo(LocalPluginContext localContext)
        {
            // Obtain the execution context from the service provider.
            if (localContext == null)
            {
                throw new ArgumentNullException(Exceptions.LOCAL_CONTEXT_NOT_PASSED);
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;

            CallSerializeContext(context, GlobalGetUserInfoConstants.PLUGIN_NAME, service);

            #region guards

            if (context.Depth > PluginHelpers.Depth)
                return;

            #endregion

            switch (context.MessageName)
            {
                case GlobalGetUserInfoConstants.ACTION_NAME: // custom action
                    {
                        try
                        {
                            string userid = null;
                            userid = context.UserId.ToString();

                            UserLogic userlogic = new UserLogic(service);
                            UserInfo ui = userlogic.GetUserInfo(userid);

                            context.OutputParameters[GlobalGetUserInfoConstants.PARAM_USERNAME] = ui.UserName;
                            context.OutputParameters[GlobalGetUserInfoConstants.PARAM_ROLES] = ui.Roles;
                            context.OutputParameters[GlobalGetUserInfoConstants.PARAM_APPVERSION] = ui.AppVersion;
                            context.OutputParameters[GlobalGetUserInfoConstants.PARAM_SYSTEMNAME] = context.OrganizationName;
                            context.OutputParameters[GlobalGetUserInfoConstants.PARAM_CLIENTNAME] = ui.ClientName;
                            context.OutputParameters[GlobalGetUserInfoConstants.PARAM_BUSINESSUNIT] = ui.BusinessUnit;
                            context.OutputParameters[GlobalGetUserInfoConstants.PARAM_BUSINESSUNIT] = ui.BusinessUnit;
                            context.OutputParameters[GlobalGetUserInfoConstants.PARAM_CURRENCY] = ui.Currency;
                            context.OutputParameters[GlobalGetUserInfoConstants.PARAM_DOMAINNAME] = ui.DomainName;
                            // future
                            //context.OutputParameters[GlobalGetUserInfoConstants.PARAM_RECORDID] = ui.RecordID;
                            //context.OutputParameters[GlobalGetUserInfoConstants.PARAM_SVCVERSION] = ui.ServiceVersion;
                        }
                        catch (Exception ex)
                        {
                            string msg = string.Format("Error on GlobalGetUserInfo. {0}: {1}: ", context.MessageName, ex.Message);
                            throw new InvalidPluginExecutionException(msg, ex);
                        }
                    }

                    break;
            }
        }
    }
}
